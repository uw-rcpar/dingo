#!/bin/bash

# rsync AWS dev env to Acquia dev env
#
# RUN THIS FROM: Acquia dev server, it pulls

# src requires the trailing slash, "get all files inside the folder"
src_local_path='/var/www/dingo.rogercpareview.com/public_html/sites/default/files/'

# dest requires NOT to have trailing slash, "all files from dest go into this folder
dest_remote_server='rogercpa.prod@web-18483.prod.hosting.acquia.com'
dest_remote_folder="/mnt/gfs/home/rogercpa/prod/sites/default/files"

echo ""
echo "Rsync from: $src_local_path"
echo "Rsync into:  $dest_remote_server:$dest_remote_folder"
echo ""

# add --dry-run to test

echo "rsync --dry-run -avzr --delete -e 'ssh -i id_rsa_acquia_chris' $src_local_path $dest_remote_server:$dest_remote_folder"
