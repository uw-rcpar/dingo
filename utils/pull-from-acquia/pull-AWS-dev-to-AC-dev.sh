#!/bin/bash

# rsync AWS dev env to Acquia dev env
#
# RUN THIS FROM: Acquia dev server, it pulls

src_remote_server='ec2-user@dingo.rogercpareviewdev.com'
src_remote_path='/var/www/dingo.rogercpareview.com/public_html/sites/default/files/'
dest_local_folder="/mnt/gfs/home/rogercpa/dev/sites/default/files"

echo ""
echo "Rsync from: $src_remote_server:$src_remote_path"
echo "Rsync into:  $dest_local_folder"
echo ""

# add --dry-run to test
rsync --dry-run -avzr --delete -e 'ssh -i keys/id_rsa_acquia_servers' $src_remote_server:$src_remote_path $dest_local_folder
