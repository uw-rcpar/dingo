#!/bin/bash

# usage:
# 1. edit variables in the file below, destination, server etc...
# 2. run the script to send --soursedir to acquia
#
# results:
# given the sourcedir, the entire directory will be archived / zipped and scp-sent to acquia server

SCPDEST='rogercpa.dev@staging-18487.prod.hosting.acquia.com:/mnt/gfs/rogercpa.dev/import'

# @see: http://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
# Use > 1 to consume two arguments per pass in the loop (e.g. each
# argument has a corresponding value to go with it).
# Use > 0 to consume one or more arguments per pass in the loop (e.g.
# some arguments don't have a corresponding value to go with it such
# as in the --default example).
# note: if this is set to > 0 the /etc/hosts part is not recognized ( may be a bug )
while [[ $# > 1 ]]
do
key="$1"

case $key in

    # require the -s dir
    -s|--sourcedir)
    SOURCEDIR="$2"
    shift # past argument
    ;;

    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

$echo SOURCE DIR = "${SOURCEDIR}"
echo "Archiving $SOURCEDIR"

# clean up source dir
TARNAME=$(echo "$SOURCEDIR.tgz" | sed 's/\//\_/g')

echo "Creating $TARNAME"
rm -f ./$TARNAME
tar -czf ./$TARNAME $SOURCEDIR

echo "SCP sync files to the remote ($SCPDEST)"
scp ./$TARNAME $SCPDEST
