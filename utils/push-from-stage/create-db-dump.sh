#!/bin/bash

# usage:
# 1. cd into the directory containing 'public_html'
# 2. make sure to update/edit env vars below
#
# result:
# 1. it will create a database dump file _schema.sql with the schema only
# 2. it will run sed on that file, doing a find/replace on the semaphore table, replacing the primary key
#

# @next steps: look to send-archive.sh to send files to Acquia

# mysqldump -u rcpar -p --result-file=dingo_dev_ipq.$NOW.sql --skip-extended-insert --quick dingo_dev_ipq

NOW=$(date +"%Y-%m-%d-%H-%M-%S")

DBUSER="root"
DBPASS="CPAdev2013"
DATABASES_TO_DUMP='dingo_dev_ipq'

DATABASES=$(mysql -u $DBUSER -p$DBPASS -e 'SHOW DATABASES;' | grep "$DATABASES_TO_DUMP")
# DATABASES=$(mysql -u $DBUSER -p$DBPASS -e 'SHOW DATABASES;' | grep "database_core\|user_records")

mkdir -p exports
pushd exports

for DB in $DATABASES
do
  echo "Dumping $DB to $DB-$NOW"
  mkdir $DB-$NOW
  pushd $DB-$NOW

  echo "Dumping $DB sechema"
  mysqldump -u $DBUSER -p$DBPASS --no-data --result-file='_schema.sql' $DB

  echo "## PATCH ## Roll back to use just name for primary key in the semaphore table"
  sed 's/PRIMARY KEY (`name`,`value`) USING BTREE/PRIMARY KEY (`name`) USING BTREE/g' "_schema.sql" > _new-schema.sql
  mv -f _new-schema.sql _schema.sql

  # all tables
  mysqldump -u $DBUSER -p$DBPASS --skip-extended-insert --no-create-info --quick --result-file="_data.sql" $DB

  # each table separate
#  TABLES=$(mysql -u $DBUSER -p$DBPASS $DB -e 'SHOW TABLES;')
#
#  for TABLE in $TABLES
#  do
#    echo "Dumping database.table $DB.$TABLE"
#    mysqldump -u $DBUSER -p$DBPASS --skip-extended-insert --result-file="$TABLE.sql" $DB $TABLE
#  done

  popd
done
