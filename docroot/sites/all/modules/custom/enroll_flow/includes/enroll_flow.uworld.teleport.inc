<?php

/**
 * Access callback for user/%user/platform-migrate and user/%user/platform-migrate/confirm
 * @param object $user_to_migrate
 * - A loaded Drupal user object
 * @return bool
 */
function enroll_flow_platform_migrate_access($user_to_migrate) {
  global $user;
  if ($user_to_migrate->uid == $user->uid || user_access('access customercare')) {
    return true;
  }
  else {
    return false;
  }
}

/**
 * Page callback for user/%user/platform-migrate
 * @param object $user_to_migrate
 * - A loaded Drupal user object
 */
function enroll_flow_platform_migrate($user_to_migrate) {
  drupal_add_js(drupal_get_path('theme', 'bootstrap_rcpar') . '/js/QueryString.js');
  drupal_add_css(drupal_get_path('module', 'enroll_flow') . '/css/platform-migrate.css');
  drupal_add_js(drupal_get_path('module', 'enroll_flow') . '/js/platform-migrate.js');
  return rcpar_mods_render_block('block','platform_migrate');
}

/**
 * Page callback for user/%user/platform-migrate/error
 * @param object $user_to_migrate
 * - A loaded Drupal user object
 */
function enroll_flow_platform_migrate_error($user_to_migrate) {
  drupal_add_css(drupal_get_path('module', 'enroll_flow') . '/css/platform-migrate.css');
  drupal_add_js(drupal_get_path('theme', 'bootstrap_rcpar') . '/js/QueryString.js');
  drupal_add_js(drupal_get_path('module', 'enroll_flow') . '/js/platform-migrate.js');

  drupal_set_message('Oops!  An error has occurred during your course upgrade, and we recommend restarting the upgrade 
    process from your course dashboard. If you continue to encounter errors, please contact our Customer Support 
    Team from our <a href="/contact">contact page</a> for assistance.', 'error');

  drupal_set_message('Click <a href="/dashboard">here</a> to return to your dashboard.');

  return '
    <div class="platform-migration">
      <header id="navbar-pm" role="banner">
        <a class="logo navbar-btn pull-left step-one" title="Back"><img src="/sites/all/modules/custom/enroll_flow/css/img/logo-back.svg" alt="Roger CPA Review - Click to go to back" width="292"></a>
      </header>
    </div>';
}

/**
 * Page callback for user/%user/platform-migrate/confirm
 * @param object $user_to_migrate
 * - A loaded Drupal user object
 */
function enroll_flow_platform_migrate_confirm($user_to_migrate) {
  // Instantiates the UserTeleportation
  $t = new \RCPAR\UW\UserTeleportation($user_to_migrate->uid);

  // Make sure user's haven't already teleported. This can happen if they click multiple times or whatever.
  if($t->isUserMigrated()) {
    drupal_goto("user/{$user_to_migrate->uid}/platform-migrate", ['query' => ['step' => 'confirmation']]);
  }

  // Phase 1 users
  if(empty($_GET['data'])) {
    if($t->teleportPhase1()) {
      drupal_goto("user/{$user_to_migrate->uid}/platform-migrate", ['query' => ['step' => 'confirmation']]);
    }
  }
  // Phase 2 users
  else {
    if($t->teleportPhase2()) {
      $options = array('query' => array('data' => '1', 'step' => 'in-progress'));
      drupal_goto("/user/{$user_to_migrate->uid}/platform-migrate", $options);
    }
  }

  // Failure - if we got here, something must have failed. show the user the API's error message and redirect
  // to the error page.
  drupal_set_message($t->getErrorMessage(), 'error');
  drupal_goto("user/{$user_to_migrate->uid}/platform-migrate/error");
}

/**
 * Page callback for user/%user/notify
 * Sends an email to the user notifying them they can retry migrating to UWorld.
 * @param object $user
 * - A loaded Drupal user object
 */
function enroll_flow_user_notify($user) {
  // Only send email once the user has clicked the 'execute' link
  if(empty($_GET['execute'])) {
    $markup = '<div class="dashboard-wrapper welcome-wrapper" style="padding: 20px;">';
    $markup .= '<h2 style="padding-bottom: 15px;">Confirm send email to user:</h2>';
    $markup .= l('Send email', current_path(), ['query' => ['execute' => 1], 'attributes' => ['class' => ['btn-primary']]]);
    $markup .= '</div>';
    return $markup;
  }

  // Email user and redirect back to customer care screen
  $emailto = $user->mail;
  $emailfrom = variable_get('site_mail', '');
  $first_name = field_get_items('user', $user, 'field_first_name');
  $body = "Hi {$first_name[0]['safe_value']},\n\n";
  $body .= "We're happy to report that we've resolved the issue with your course platform upgrade. You can now reattempt the upgrade process via the prompts in your course.\n\n";
  $body .= "Apologies for the interruption, and please reach out to our Customer Care team if you require any further help.\n\n";
  $body .= "Our best,\n\n";
  $body .= "The Roger CPA Review Team";

  drupal_mail('system', 'uworld_teleportation', $emailto, language_default(), array('context' => [
    'message' => $body,
    'subject' => "You're all set to upgrade!",
  ]), $emailfrom);

  drupal_set_message("Notification email has been sent to {$emailto}." , 'alert alert-success');
  drupal_goto('customercare-screen/' . $user->uid);
}

/**
 * Returns true if the currently logged in user is eligible for phase 1 teleportation.
 *
 * @return bool
 */
function enroll_flow_is_user_phase1_eligible() {
  // Global kill switch for phase1 teleport
  if (!variable_get('uw_phase1_teleport', FALSE)) {
    return FALSE;
  }

  // User can't be eligible if they're not logged in.
  if(!user_is_logged_in()) {
    return FALSE;
  }

  return \RCPAR\UW\UserTeleportation::instance($GLOBALS['user']->uid)->isUserEligible('phase1');
}

/**
 * Returns true if the currently logged in user is eligible for phase 2 or 3 teleportation.
 *
 * @return bool
 */
function enroll_flow_is_user_phase2_eligible() {
  // Global kill switch for phase1 teleport
  if (!variable_get('uw_phase2_teleport', FALSE)) {
    return FALSE;
  }

  // User can't be eligible if they're not logged in.
  if(!user_is_logged_in()) {
    return FALSE;
  }

  // Check for phase 2 eligibility
  $is_eligible = \RCPAR\UW\UserTeleportation::instance($GLOBALS['user']->uid)->isUserEligible('phase2');

  // A little counter intuitive here, but if ineligible for phase2 we still need to check for phase 3
  if (variable_get('uw_phase3_teleport', FALSE) && $is_eligible != 1) {
    $is_eligible = \RCPAR\UW\UserTeleportation::instance($GLOBALS['user']->uid)->isUserEligible('phase3');
  }

  return $is_eligible;
}

