<?php

/**
 * Menu callback for /admin/settings/rcpar/system_settings/enroll_flow/re-import-coupon
 * Push all existing users to Salesforces
 */
function enroll_flow_reimport_coupon_orders_batch_form($form, &$form_state) {
  $form['instructions'] = array(
    '#markup' => '<p>Orders that contain a line item of the specified type and line item able as seen in the ' .
      'commerce_line_item database table will be re-imported from UWorld</p>',
  );
  
  $form['line_item_type'] = array(
    '#title' => 'Line Item Type',
    '#description' => 'Examples of valid types: "product", "avatax", "commerce_coupon". Do not include quotations.',
    '#type' => 'textfield',
    '#required' => TRUE,
  );

  $form['line_item_label'] = array(
    '#title' => 'Line Item Label',
    '#description' => 'Examples of valid labels: "FAR", "FREETRIAL-BUNDLE", "commerce_coupon_fixed: ED79R-42086-ME2Y6-968". Do not include quotations.',
    '#type' => 'textfield',
    '#required' => TRUE,
  );

  $form['resumeat'] = array(
    '#title' => 'Resume the batch at the given order ID and above.',
    '#description' => 'Leave blank to start from the beginning.<br>' .
      'The last order ID marked as processed was: ' . variable_get('enroll_flow_reimport_coupon_orders_resume', 0),
    '#type' => 'textfield',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Execute Batch',
  );

  return $form;
}


/**
 * Form callback for enroll_flow_reimport_coupon_orders_batch_form
 * Initiates the batch job.
 */
function enroll_flow_reimport_coupon_orders_batch_form_submit($form, &$form_state) {
  $line_item_type = $form_state['values']['line_item_type'];
  $line_item_label = $form_state['values']['line_item_label'];
  $resumeat = !empty($form_state['values']['resumeat']) ? $form_state['values']['resumeat'] : 0;

  enroll_flow_reimport_coupon_orders_batch($line_item_type, $line_item_label, $resumeat);
}

/**
 * The batch callback. Build batch operations.
 * @param string $coupon_code
 * - A commerce coupon code
 */
function enroll_flow_reimport_coupon_orders_batch($line_item_type, $line_item_label, $resumeat = 0) {
  $batch = array(
    'operations' => array(),
    'finished' => 'enroll_flow_reimport_coupon_orders_batch_finished',
    'title' => t('Re-import orders from UWorld with a specific coupon code'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'enroll_flow') . '/includes/enroll_flow.reimport.couponorders.batch.inc'
  );

  // Get a list of user IDs where there is some non-empty value in field_college_name
  $order_ids = db_query("
    SELECT DISTINCT order_id FROM commerce_line_item
    WHERE type = :type AND line_item_label = :label AND order_id > :resume
    ORDER BY order_id ASC", [':type' => $line_item_type, ':label' => $line_item_label, ':resume' => $resumeat])->fetchCol();

  // Set up batches of x number of users per operation
  foreach (array_chunk($order_ids, variable_get('enroll_flow_reimport_coupon_orders_batch_size', 10)) as $order_ids_chunk) {
    $batch['operations'][] = array('enroll_flow_reimport_coupon_orders_batch_process', array($order_ids_chunk));
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/rcpar/system_settings/enroll_flow/re-import-coupon'); // The path to redirect to when done.
  }
}


/**
 * Batch operation callback.
 * @param int[] $order_ids
 * - Array of order ids to process.
 * @param $context
 * - The system's batch information.
 */
function enroll_flow_reimport_coupon_orders_batch_process($order_ids, &$context) {
  foreach ($order_ids as $order_id) {
    enroll_flow_reimport_coupon_orders_batch_process_order($order_id, $context);
  }
  
  // Save our resume-at marker at the last processed uid
  variable_set('enroll_flow_reimport_coupon_orders_resume', $order_id);
}

/**
 * Process an individual user and convert their field_college_name into a sf_account university reference.
 * @param int $uid
 * - The user id to process.
 */
function enroll_flow_reimport_coupon_orders_batch_process_order($order_id, &$context) {
  try {
    // Load the order - throw an exception if we can't.
    $order = commerce_order_load($order_id);
    if (!$order) {
      throw new Exception('Order could not be loaded.');
    }
    
    // Get the UWorld order ID. Exception if we can't.
    $o = new \RCPAR\WrappersDelight\CommerceOrderWrapper($order);
    $uwoid = $o->getUwOrderId();
    if(empty($uwoid)) {
      throw new Exception('Order does not have a UWorld order ID.');
    }

    // Import from UWorld
    if(!\RCPAR\Import\UWorldOrderProcessor::importSingleOrder($uwoid)) {
      throw new Exception('Failed to process');
    };
  }
  catch (Exception $e) {
    $context['results']['failed_order_ids'][] = $order_id;
    return;
  }
}

/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function enroll_flow_reimport_coupon_orders_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Batch completed!');
    drupal_set_message('Failed Order IDs: <pre>' . print_r($results['failed_order_ids'], TRUE) . '</pre>', 'error');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}