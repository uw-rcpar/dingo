<?php

/**
 * Implements hook_commerce_payment_method_info().
 */
function enroll_flow_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['enroll_flow_uworld_payment'] = array(
    'title' => t('UWorld Payment'),
    'description' => t('Payment imported via API from UWorld.'),
    'active' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: submit form.
 */
function enroll_flow_uworld_payment_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form = array();
  return $form;
}

/**
 * Payment method callback: submit form validation.
 */
function enroll_flow_uworld_payment_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  if ($payment_method['title'] != "UWorld Payment") {
    return;
  }
}

/**
 * Payment method callback: submit form submission.
 */
function enroll_flow_uworld_payment_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order->data['enroll_flow_uworld_payment'] = $pane_values;

  $transaction = commerce_payment_transaction_new('enroll_flow_uworld_payment', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->message = '';
  $transaction->message_variables = array();

  commerce_payment_transaction_save($transaction);
}
