<?php
/**
 * @file
 * Page callbacks for the enroll_flow module registration pages.
 */

function enroll_flow_reg_page($type='default') {
  $redir = $type.'_register';
  $output = null;

  if (user_is_logged_in()) {
    global $user;
    if (!in_array(array('professor','student'), $user->roles)) {
      // User Needs the proper role
      $redir = $type.'_update';
    } 
  }

  // Set title
  drupal_set_title(t(ucwords($type) . ' Registration'));
  
  /*
    Display Proper Form (full reg or update profile)
  */
  switch($redir) {
    case 'professor_update':
      $output .= t('Custom Professor profile Fields for Update.');
      $output .= drupal_render(drupal_get_form('enroll_flow_reg_profupdate_form'));
    break;
    
    case 'professor_register';
      $output .= drupal_render(drupal_get_form('user_register_form'));
    break;
  
    case 'student_update':
      $output .= t('Custom Student profile Fields for Update.');
      $output .= drupal_render(drupal_get_form('enroll_flow_reg_studupdate_form'));
    break;

    case 'student_register':
      $output .= drupal_render(drupal_get_form('user_register_form'));    
    break;

    default:
      drupal_set_title(t('Registration Error'));
      $output .= t('Registration Type is not specified or not allowed.');
    break;
  }
  
  return $output;
}


function enroll_flow_reg_profupdate_form() {
  global $user;
  $account = user_load($user->uid);
  
  $form = array();
  
  $form['reg_update']['destination'] = array(
    '#type' => 'hidden',
    '#title' => t('destination'),
    '#default_value' => check_plain($_GET['destination']),
  );
  
  $form['reg_update']['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#weight' => 1,
    '#default_value' => '',
    '#description' => 'Enter a valid College email address.',
    '#required' => TRUE,
  );

  $form['reg_update']['campus'] = array(
    '#type' => 'textfield',
    '#title' => t('Campus'),
    '#weight' => 2,
    '#default_value' => $account->field_campus['und']['0']['value'],
    '#required' => TRUE,
  );
  
  $course_taught = array('advanced'=>'Advanced Accounting',
                         'audit'=>'Auditing',
                          'cost'=>'Cost Accounting',
                          'gov'=>'Gov/Non Profit',
                          'intermediate'=>'Intermediate 1,2 or 3',
                          'tax'=>'Tax',
                          'nothing'=>'Nothing currently',
                          );
  $form['reg_update']['courses_taught'] = array(
    '#type' => 'select',
    '#title' => t('What Course(s) do you teach? (select all that apply)'),
    '#weight' => 3,
    '#options' => $course_taught,
    '#default_value' => $account->field_courses_taught['und']['0']['value'],
    '#required' => TRUE,
  );

  $interests = array('supplements'=>'Curriculum Supplements',
                      'discounts'=>'Discounts',
                      'information'=>'Information on the CPA Exam',
                      'partnership'=>'Partnership Information',
                      'campus'=>'Offering Roger CPA Review on campus',
                      'credit'=>'Roger CPA Review for Credit Programs',
                      'exam'=>'Taking the CPA Exam',
                      );
  $form['reg_update']['interests'] = array(
    '#type' => 'select',
    '#title' => t('Interested in (select all that apply)'),
    '#weight' => 4,
    '#options' => $interests,
    '#default_value' => $account->field_interests['und']['0']['value'],
    '#required' => TRUE,
  );

  $form['reg_update']['other_cpa_programs'] = array(
    '#type' => 'radios',
    '#title' => t('Are you currently utilizing or partnered with another CPA Review program?'),
    '#weight' => 5,
    '#options' => array('Yes'=>'Yes','No'=>'No'),
    '#default_value' => $account->field_other_cpa_group['und']['0']['value'],
    '#required' => TRUE,
  );  
  
  $form['reg_update']['submit'] = array(
    '#type' => 'submit',
    '#title' => t('Update Profile'),
    '#value' => 'Update Profile',
    '#weight' => 10,
    '#required' => TRUE,
  );
  
  $form['#submit'][] = 'enroll_flow_profupdate_submit';
  $form['#validate'][] = 'enroll_flow_email_form_validate';
  
  return $form;
}

function enroll_flow_reg_studupdate_form() {
  global $user;
  $account = user_load($user->uid);

  $form = array();
  
  $form['reg_update']['destination'] = array(
    '#type' => 'hidden',
    '#title' => t('destination'),
    '#default_value' => check_plain($_GET['destination']),
  );

  $form['reg_update']['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#weight' => 1,
    '#default_value' => '',
    '#description' => 'Enter a valid College email address.',
    '#required' => TRUE,
  );

  $form['reg_update']['campus'] = array(
    '#type' => 'textfield',
    '#title' => t('Campus'),
    '#weight' => 2,
    '#default_value' => $account->field_campus['und']['0']['value'],
    '#required' => TRUE,
  );

$state_list = array('AL'=>"Alabama",  
			'AK'=>"Alaska",  
			'AZ'=>"Arizona",  
			'AR'=>"Arkansas",  
			'CA'=>"California",  
			'CO'=>"Colorado",  
			'CT'=>"Connecticut",  
			'DE'=>"Delaware",  
			'DC'=>"District Of Columbia",  
			'FL'=>"Florida",  
			'GA'=>"Georgia",  
			'HI'=>"Hawaii",  
			'ID'=>"Idaho",  
			'IL'=>"Illinois",  
			'IN'=>"Indiana",  
			'IA'=>"Iowa",  
			'KS'=>"Kansas",  
			'KY'=>"Kentucky",  
			'LA'=>"Louisiana",  
			'ME'=>"Maine",  
			'MD'=>"Maryland",  
			'MA'=>"Massachusetts",  
			'MI'=>"Michigan",  
			'MN'=>"Minnesota",  
			'MS'=>"Mississippi",  
			'MO'=>"Missouri",  
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",  
			'OK'=>"Oklahoma",  
			'OR'=>"Oregon",  
			'PA'=>"Pennsylvania",  
			'RI'=>"Rhode Island",  
			'SC'=>"South Carolina",  
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",  
			'TX'=>"Texas",  
			'UT'=>"Utah",  
			'VT'=>"Vermont",  
			'VA'=>"Virginia",  
			'WA'=>"Washington",  
			'WV'=>"West Virginia",  
			'WI'=>"Wisconsin",  
			'WY'=>"Wyoming");
			
  $form['reg_update']['state'] = array(
    '#type' => 'select',
    '#title' => t('State'),
    '#weight' => 3,
    '#options' => $state_list,
    '#default_value'=> $account->field_student_state['und']['0']['value'],
    '#required' => TRUE,
  );

  $form['reg_update']['grad_year'] = array(
    '#type' => 'textfield',
    '#title' => t('Graduation Year'),
    '#weight' => 4,
    '#default_value' => $account->field_graduation_year['und']['0']['value'],
    '#required' => TRUE,
  );

  $form['reg_update']['firm'] = array(
    '#type' => 'textfield',
    '#title' => t('Firm'),
    '#weight' => 5,
    '#default_value' => $account->field_firm['und']['0']['value'],
    '#required' => TRUE,
  );

  $form['reg_update']['submit'] = array(
    '#type' => 'submit',
    '#title' => t('Update Profile'),
    '#value' => t('Update Profile'),
    '#weight' => 10,
    '#required' => TRUE,
  );

  $form['#submit'][] = 'enroll_flow_studupdate_submit';
  $form['#validate'][] = 'enroll_flow_email_form_validate';
  
  return $form;
}


function enroll_flow_studupdate_submit($form, &$form_state) {
  global $user;
  $account = user_load($user->uid);

  $update = array();
  if (!empty($form_state['values']['campus'])) { $update['field_campus']['und']['0']['value'] = $form_state['values']['campus']; }
  if (!empty($form_state['values']['state'])) { $update['field_student_state']['und']['0']['value'] = $form_state['values']['state']; }
  if (!empty($form_state['values']['grad_year'])) { $update['field_graduation_year']['und']['0']['value'] = $form_state['values']['grad_year']; }
  if (!empty($form_state['values']['firm'])) { $update['field_firm']['und']['0']['value'] = $form_state['values']['firm']; }

  // Add a role
  $roles = array_map('trim', explode("\n", variable_get('ef_reg_studupdate_roles', '')));
  watchdog('enroll_flow', 'User Added ' . variable_get('ef_reg_studupdate_roles', '') .' <pre>'. print_r($roles, TRUE) .'</pre>');
  foreach($roles AS $k=>$role_name) {
    if ($role = user_role_load_by_name($role_name)) {
      user_multiple_role_edit(array($account->uid), 'add_role', $role->rid);
    }
  }

  // Save updated fields
  user_save($account, $update);
  // Dispaly a message to user.
  drupal_set_message(variable_get('ef_reg_studupdate_msg', ''), 'status');

  // Redirect if we have a destination.
  if (!empty($form_state['values']['destination'])) { 
    $form_state['redirect'] = $form_state['values']['destination'];
  }
}

/*
  Add/Modify forms per enrollment type.
*/
function enroll_flow_reg_form_mods(&$form, &$form_state, $form_id) {
  $type = check_plain(arg(2));
  if (empty($type)) {
    $type = isset($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : "default";
  }
  // Hide the default field since it's automatically populated.
  $form['field_registration_type']['und'][0]['value']['#type'] = 'hidden';

  if (!empty($type)) {
    $fields_professor = array_map('trim', explode("\n", variable_get('ef_professor_fields', '')));
    $fields_student = array_map('trim', explode("\n", variable_get('ef_student_fields', '')));
    $fields_webcast = array_map('trim', explode("\n", variable_get('ef_webcast_fields', '')));
   if(in_array($type,array("professor","student","webcast"))){
      $form['field_registration_type']['und'][0]['value']['#default_value'] = $type;
      if ($type != "webcast") {
        $form['#validate'][] = 'enroll_flow_email_form_validate'; 
      }
    }
    switch($type) {      
      case 'professor':
        $field_totals = explode(",",implode(",",$fields_student) . "," . implode(",",$fields_webcast));        
        $field_totals =  array_diff($field_totals, $fields_professor);
        $form = _enroll_flow_unset_fields($field_totals, $form);
      break;
      
      case 'student':
        $field_totals = explode(",",implode(",",$fields_professor) . "," . implode(",",$fields_webcast));        
        $field_totals =  array_diff($field_totals, $fields_student);
        $form = _enroll_flow_unset_fields($field_totals, $form);
      break;
      
      case 'webcast':        
        // Get fields to remove from registration flow 
        $field_totals = explode(",",implode(",",$fields_professor) . "," . implode(",",$fields_student));        
        $field_totals =  array_diff($field_totals, $fields_webcast);
        $form = _enroll_flow_unset_fields($field_totals, $form);
        
        $form['#submit'][]= 'enroll_flow_webcast_redirect';
        $form['field_country']['#access'] = 1;
        $form['field_student_state']['#states'] = array(
            'visible' => array(
              'select[name="field_country[und]"]' => array('value' => 'US'),
            ),
          );
        

      break;
      
      case 'default':
        // Regular registration form. The following lines remove all the fields that only show for student or professor registrations.
        $field_totals = explode(",", implode(",", $fields_professor) . "," . implode(",", $fields_student) . "," . implode(",", $fields_webcast));
        $form = _enroll_flow_unset_fields($field_totals, $form);
        
      break;
    }
  }
}

/*
function enroll_flow_reg_form($form, &$form_state, $account) {
  $form['#account'] = $account;
  field_attach_form('user', $account, $form, $form_state);
}

function enroll_flow_reg_form_submit($form, &$form_state) {
  field_attach_submit('user', $form['#account'], $form, $form_state);
}
*/

function _enroll_flow_unset_fields($fields, $form) {
  if (!empty($fields)) {
    foreach ($fields AS $k => $v) {
      if (isset($form[$v])) {
        unset($form[$v]);
      }
    }
  }
  return $form;
}
