<?php

/**
 * Batch form used to teleport specific users or users associated with partners to the UWorld system
 * @param $form
 * @param $form_state
 * @return mixed
 */
function enroll_flow_migrate_users_batch_form($form, &$form_state) {

  //////// Queue Users for teleporation ////////
  $form['queue'] = array(
    '#title' => 'Queue Users for Teleportation',
    '#type' => 'fieldset',
  );

  $form['queue']['migrate_uids'] = array(
    '#type' => 'textarea',
    '#title' => t('Migration UID List'),
    '#description' => 'Enter a list of user uids you wish to migrate each on a new line. All users migrated with course data.',
  );

  $form['queue']['migrate_emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Migration Email List'),
    '#description' => 'Enter a list of user emails you wish to migrate each on a new line. All users migrated with course data.',
  );

  $form['queue']['migrate_partners'] = array(
    '#type' => 'textarea',
    '#title' => t('Migration Partners List'),
    '#description' => 'A user who has any active qualifying entitlements originating from these partner IDs will be' .
      ' migrated to UWorld. NOTE: overrides disqualifying partners. All users migrated with course data.',
  );

  // Submit button
  $form['queue']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Queue Users',
    '#submit' => array('enroll_flow_migrate_users_queue_form_submit'),
  );

  //////// Batch migrate users ////////
  $form['batch'] = array(
    '#title' => 'Batch Migrate Users',
    '#type' => 'fieldset',
  );

  $form['batch']['queued'] = array(
    '#title' => 'Migrate queued users',
    '#type' => 'checkbox',
    '#description' => 'When checked users marked queued in the status field will be migrated.',
  );

  $form['batch']['failed'] = array(
    '#title' => 'Migrate failed users',
    '#type' => 'checkbox',
    '#description' => 'When checked users marked failed in the status field will be migrated.',
  );

  $form['batch']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Migrate Users',
    '#submit' => array('enroll_flow_migrate_users_batch'),
  );

  $form['batch']['info'] = array(
    '#markup' => '<p>Current number of queued users: ' . count(enroll_flow_migrate_users_get_queued_uids('queued')) . '<br>' .
      'Current number of failed users: ' . count(enroll_flow_migrate_users_get_queued_uids('failed')) . '<br>' .
      'Users in progress: ' . count(enroll_flow_migrate_users_get_queued_uids('in progress')) . '<br>' .
      'Completed batch migrations: ' . count(enroll_flow_migrate_users_get_queued_uids('completed')) . '<br>' .
      'Number of ineligible users in batch queue: ' . count(enroll_flow_migrate_users_get_queued_uids('ineligible')) . '<br>' .
      'Last run time: ' . format_date(variable_get('enroll_flow_migrate_users_last', 0)) . '</p>'
  );

  return $form;

}

/**
 * Get uids from the enroll_flow_uw_migrate_batch_queue table based on their status
 * @param $status
 * - Accepted values are 'queued', 'in progress', 'completed', 'failed', 'ineligible'
 * @param $limit
 *   if true will limit the records to only 100 [restriction needed for uworld api]
 * @return mixed
 */
function enroll_flow_migrate_users_get_queued_uids($status = 'queued', $limit = FALSE) {
  $limit_text = $limit ? ' LIMIT ' . variable_get('enroll_flow_migrate_users_limit', 100) : '';
  return db_query("SELECT uid FROM enroll_flow_uw_migrate_batch_queue WHERE status = :status {$limit_text}", array('status' => $status))->fetchAll();
}

function enroll_flow_migrate_users_update_queued_status($uid, $status, $error = null) {
  $fields = array(
    'uid' => $uid,
    'status' => $status,
    'updated' => time(),
  );
  if (!is_null($error)) {
    $fields['error_message'] = $error;
  }
  db_update('enroll_flow_uw_migrate_batch_queue')
    ->fields($fields)
    ->condition('uid', $uid)
    ->execute();
}

/**
 * Form callback for enroll_flow_migrate_users_batch
 * Inserts uids into enroll_flow_uw_migrate_batch_queue table.
 */
function enroll_flow_migrate_users_queue_form_submit($form, &$form_state) {

  $uids = array();

  // Validate blank fields
  if (empty($form_state['values']['migrate_emails'])
    && empty($form_state['values']['migrate_partners'])
    && empty($form_state['values']['migrate_uids'])) {
    drupal_set_message("Please enter email(s) and/or Partner ID(s) and/or UID(s)",'error');
    return FALSE;
  }

  // Check for uids
  if (!empty($form_state['values']['migrate_uids'])) {
    $uids = explode("\r\n", $form_state['values']['migrate_uids']);
  }

  // Convert emails to UIDs
  If (!empty($form_state['values']['migrate_emails'])) {
    $emails = explode("\r\n", $form_state['values']['migrate_emails']);
    foreach ($emails as $email) {
      $user = user_load_by_mail($email);
      $uids[] = $user->uid;
    }
  }

  // Get uids from partners
  If (!empty($form_state['values']['migrate_partners'])) {
    $partner_ids = str_replace("\r\n", ",", $form_state['values']['migrate_partners']);
    $results = db_query("SELECT users.uid
                FROM 
                field_data_field_expiration_date 
                INNER JOIN node ON node.nid = field_data_field_expiration_date.entity_id
                INNER JOIN users ON users.uid = node.uid
                INNER JOIN field_data_field_partner_id ON node.nid = field_data_field_partner_id.entity_id
                INNER JOIN field_data_field_course_type_ref ON node.nid = field_data_field_course_type_ref.entity_id
                WHERE  (field_data_field_expiration_date.field_expiration_date_value > UNIX_TIMESTAMP()) 
                AND (node.status = 1) 
                AND (field_data_field_expiration_date.entity_type = 'node') 
                AND (field_data_field_expiration_date.bundle = 'user_entitlement_product')
                AND (field_data_field_course_type_ref.bundle = 'user_entitlement_product')
                AND (field_data_field_course_type_ref.field_course_type_ref_tid <> 1466)
                AND (field_data_field_partner_id.field_partner_id_value IN (:pids))
                GROUP BY users.uid", array(':pids' => $partner_ids));
  }

  if ($results) {
    foreach ($results as $result) {
      $uids[] = $result->uid;
    }
  }

  // No uids then we're done
  if (empty($uids)) {
    drupal_set_message("No valid users eligible for migration.",'error');
    return FALSE;
  }

  // Make sure our uids are unique to prevent duplicate migrations
  $uids = array_unique($uids);

  // Queue up the uids for the batch job
  foreach ($uids as $uid) {
    try {
      // Status values can be queued, in progress, completed, failed, ineligible.
      $fields = array(
        'uid' => $uid,
        'status' => 'queued',
        'created' => REQUEST_TIME,
      );
      db_insert('enroll_flow_uw_migrate_batch_queue')->fields($fields)->execute();
    }
    catch (Exception $e) {
      // TODO can search $e for 'Integrity constraint violation' or 'Duplicate entry'
      // But we're not going to get too worked up about it. If the UID exists, let it slide
    }
  }
  drupal_set_message(count($uids) ." users successfully queued for migration.");
}

/**
 * Build the batch operations
 * @param $uids
 * - Array of user ids to process.
 */
function enroll_flow_migrate_users_batch($form, &$form_state) {
  //if one hour has passed we can process the other users
  if (variable_get('enroll_flow_migrate_users_last', 0) > time()) {
    drupal_set_message("Please wait an hour before running batch again");
    return;
  }

  $next_execution = variable_get('enroll_flow_migrate_users_batch_time_to_execute', 'next hour');
  variable_set('enroll_flow_migrate_users_last', strtotime($next_execution));
  $uids = array();
  if ($form_state["values"]["queued"]) {
    $records = enroll_flow_migrate_users_get_queued_uids('queued', TRUE);
    foreach ($records as $row) {
      $uids[] = $row->uid;
    }
  }

  if ($form_state['values']['failed']) {
    $records = enroll_flow_migrate_users_get_queued_uids('failed', TRUE);
    foreach ($records as $row) {
      $uids[] = $row->uid;
    }
  }

  // No uids then we're done
  if (empty($uids)) {
    drupal_set_message("No valid users eligible for migration.",'error');
    return FALSE;
  }

  // Set up the batch
  $batch = array(
    'operations' => array(),
    'finished' => 'enroll_flow_migrate_users_batch_finished',
    'title' => t('Batch migrate users to UWorld'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'enroll_flow') . '/includes/enroll_flow.uworld.migrate.users.batch.inc'
  );

  // Set up batches of x number of users per operation
  foreach (array_chunk($uids, variable_get('enroll_flow_migrate_users_batch_size', 25)) as $uids_chunk) {
    $batch['operations'][] = array('enroll_flow_migrate_users_batch_process', array($uids_chunk));
  }

  batch_set($batch);
  batch_process('admin/settings/rcpar/system_settings/enroll_flow/migrate-users-batch'); // The path to redirect to when done

}

/**
 * Process an individual user and calculate their migration eligibility.
 * @param array $uids
 * - The user id to process.
 * @param $context
 * - The system's batch information.
 */
function enroll_flow_migrate_users_batch_process($uids, &$context) {

  foreach ($uids as $uid) {
    try {
      $t = new \RCPAR\UW\UserTeleportation($uid);

      // If the user has already migrated or is a testing user nothing to see here
      if ($t->isUserMigrated() || strstr($t->u->getEmail(),'@rcparlabs.com')) {
        enroll_flow_migrate_users_update_queued_status($uid, 'completed');
        continue;
      }

      // If the user has no data then just use phase 1 to teleport
      if ($t->calculateZeroProgressEligibility()) {
        if (!$t->teleportPhase1()) {
          // If we're here, there's an error
          throw new Exception($t->getErrorMessage());
        }
        enroll_flow_migrate_users_update_queued_status($uid, 'completed');
        continue;
      }

      // If the user is either phase 1 or 2 they get teleported
      if ($t->calculateEligibility('phase2') || $t->calculateEligibility('phase3')) {
        if (!$t->teleportPhase2()) {
          // If we're here, there's an error
          throw new Exception($t->getErrorMessage());
        }
        enroll_flow_migrate_users_update_queued_status($uid, 'in progress');
        continue;
      }

      // If we made it this far we have an ineligible uid. (ACT, PAL, expired entitlements, etc.)
      $context['results']['ineligible_uids'][] = $uid;
      enroll_flow_migrate_users_update_queued_status($uid, 'ineligible');
    } catch (Exception $e) {
      $context['results']['failed_uids'][] = $uid;
      enroll_flow_migrate_users_update_queued_status($uid, 'failed', $e->getMessage());
    }
  }
}

/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function enroll_flow_migrate_users_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Batch completed!');
    drupal_set_message('Failed UIDs: <pre>' . print_r($results['failed_uids'], TRUE) . '</pre>', 'error');
    drupal_set_message('Ineligible UIDs: <pre>' . print_r($results['ineligible_uids'], TRUE) . '</pre>', 'error');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}

