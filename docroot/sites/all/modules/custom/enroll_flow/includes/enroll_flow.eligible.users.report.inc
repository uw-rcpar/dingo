<?php

/**
 * Generates a downloadable csv report of all students currently eligible for migration.
 * @param $offset int sql offset
 * @param $limit int sql limit
 * @throws Exception
 */
function enroll_flow_eligible_users_report($offset = 0, $limit = 100) {

  // Get all users with active entitlements
  $results = db_query_range("SELECT users.uid, users.mail as email, field_data_field_first_name.field_first_name_value as firstname
                    FROM 
                    field_data_field_expiration_date field_data_field_expiration_date
                    INNER JOIN node ON node.nid = field_data_field_expiration_date.entity_id
                    INNER JOIN users ON users.uid = node.uid
                    INNER JOIN field_data_field_first_name ON users.uid = field_data_field_first_name.entity_id
                    WHERE  (field_data_field_expiration_date.field_expiration_date_value > UNIX_TIMESTAMP()) 
                    AND (node.status = 1) 
                    AND (field_data_field_expiration_date.entity_type = 'node') 
                    AND (field_data_field_expiration_date.bundle = 'user_entitlement_product')
                    GROUP BY users.uid, users.mail, field_data_field_first_name.field_first_name_value", $offset, $limit);

  // Setup the csv file download in the browser
  drupal_add_http_header('Content-Type', 'text/csv; utf-8');
  drupal_add_http_header('Content-Disposition', 'attachment; filename = eligible_students.csv');

  // Instead of writing down to a file we write to the output stream
  $fh = fopen('php://output', 'w');

  // Column headers
  fputcsv($fh, array(t('Email'), t('Name')));

  if ($results) {
    foreach ($results as $result) {

      // Get the teleportation record
      $t = new \RCPAR\UW\UserTeleportation($result->uid);

      // If the user has already migrated nothing to see here
      if ($t->isUserMigrated()) {
        continue;
      }

      // Any phase is eligible
      if ($t->calculateEligibility('phase1') || $t->calculateEligibility('phase2') || $t->calculateEligibility('phase3')) {

        // Write the data in the csv format
        fputcsv($fh, array($result->email, $result->firstname));
      }
    }
  }

  // Close the stream
  fclose($fh);

}