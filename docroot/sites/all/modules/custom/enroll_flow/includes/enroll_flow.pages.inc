<?php

/**
 * @file
 * Defines enrollment flow pages and display templates
 */

/**
 * Starting point in enroll flow
 */
function enroll_flow_start() {
  drupal_add_js(drupal_get_path('module', 'enroll_flow') .'/js/enroll_flow.js');
  
  $output = '';

  $product_list_form = drupal_render(drupal_get_form('enroll_flow_product_list_form'));
  $output .= theme('enroll_flow_start', array('product_list_form' => $product_list_form));

  return $output;
}

function enroll_flow_product_list_form($form, $form_state) {
  setlocale(LC_MONETARY, 'en_US');
  
  global $user;
  
  $account_uid = (!empty($_REQUEST['account_uid'])) ? check_plain($_REQUEST['account_uid']) : '';
  $product_list = enroll_flow_get_product_list();
  $form = array();

  if (count($product_list) > 0) {
    // Allow the user to add products to another users cart if they have perms.
    if (in_array('administrator', array_values($user->roles)) && !empty($account_uid)) {
      $form['enroll_form']['account_uid'] = array(
          '#type' => 'hidden',
          '#value' => $account_uid,
      );
    }

    foreach ($product_list AS $p) {
      $amount = (module_exists('commerce')) ? commerce_currency_format($p->commerce_price['und'][0]['amount'], 'USD', '', $convert = TRUE) : $p->commerce_price['und'][0]['amount'] ;
      $options[$p->product_id] = $p->sku . ' ' . $amount;
    }
    $form['enroll_form']['product_list'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Course Options'),
        '#options' => $options,
        //'#attributes' => array('class' => array($p->sku))
        //'#attributes' => array('onchange' => 'window.location="'. $_GET['q'] .'?report_group_id="+$(this).val();')
    );
    
    $form['submit'] = array (
        '#type' => 'submit',
        '#value' => 'Add to cart',
    );
  }

  return $form;
}

/**
 * Submit handler for product list form
 */
function enroll_flow_product_list_form_validate($form, &$form_state) {
  $product_selected = false;

  foreach($form_state['values']['product_list'] AS $value) {
    if ($value > 0) {
      $product_selected = true;
    }
  }

  if ($product_selected === false) {
    form_set_error('product_list', 'You must select one of the course options to continue.');
  }
}

function enroll_flow_addto_cart_form_mods(&$form, &$form_state, $form_id) {
  // Get item IDs to look for from admin settings.
  $item_ids = variable_get('ef_items_ontop', array());
  // clone FULL item  
  foreach($item_ids AS $id) {
    if (isset($form['product_id']['#options'][$id])) {
      $full[$id] = $form['product_id']['#options'][$id];
      // Remove and place it in front
      unset($form['product_id']['#options'][$id]);
      $form['product_id']['#options'] = $full + $form['product_id']['#options'];

      // If there is no values being submitted, make this be the default value
      if(empty($form_state['input']) && empty($_POST["product_id"])) {
        $form['product_id']['#default_value'] = $id;
        $form_state['default_product'] = commerce_product_load($id);
      }
    }
  }

  // Check if partner prices disable the price display.
  $hide_prices = false;
  $hide_per_part_pricing = false;
  if (module_exists('rcpar_partners') && isset($_SESSION['rcpar_partner'])) {
    $hide_prices = rcpar_partners_hide_prices();
    $hide_per_part_pricing = rcpar_partners_hide_per_part_pricing();
  }
  
    /// Force format for only one item  
  if (isset($form['product_id']['#value']) && !isset($form['product_id']['#options'])) {
    $product_id = $form['product_id']['#value'];
   
    $product = commerce_product_load($product_id);    
    if(!in_array($product->type, array('course_extension'))){         
      $form['product_id']['#type'] = 'select';
      $form['product_id']['#options']= array(
          $product_id => $product->title
      );
      $form['product_id']['#default_value'] = $product_id;
      $form['product_id']['#ajax'] = array(
          'callback' => "commerce_cart_add_to_cart_form_attributes_refresh"
      );   
    }   
        
  }    
   
  // Add pricing to the display in form selections
  if (isset($form['product_id']['#options'])) {
    foreach($form['product_id']['#options'] AS $k=>$v) {
      if($k == 'partner_package'){
        // The products that are identified with this id, are not actual products and require that we handle
        // them in a special way
        continue;
      }
      $product = commerce_product_load($k);

      $sku = entity_metadata_wrapper('commerce_product', $product)->sku->value();
      $price = entity_metadata_wrapper('commerce_product', $product)->commerce_price->value();
      $saleprice = entity_metadata_wrapper('commerce_product', $product)->field_commerce_saleprice->value();
      $price_display = ' (' . commerce_currency_format($price['amount'], $price['currency_code'], $product) . ')';
      $sale_price_display = ' (' . commerce_currency_format($saleprice['amount'], $saleprice['currency_code'], $product) . ')';
      $final_price_display = '';
  
      // We may not show prices if partner flag set.
      if (!$hide_prices) {
        // If per part is false AND not a FULL product don't use prices
        if ($hide_per_part_pricing && !strstr($form['product_id']['#options'][$k], 'Full')) {
          $final_price_display = '';
        } else {
          // Do we have Partner Pricing?        
          $partner_pricing = rcpar_partners_get_partner_course_prices();
          if (isset($partner_pricing[$sku]) && rcpar_partners_display_firm_bundle_price()) {
            //$final_price_display = (empty($sale_price_display) || $sale_price_display == " ($0.00)") ? $price_display . t(' - Discount Price: ') . commerce_currency_format(str_ireplace('.', '', $partner_pricing[$sku]), $price['currency_code'], $product) : $sale_price_display ; 
            $final_price_display = (empty($sale_price_display) || $sale_price_display == " ($0.00)") ? ' (' . commerce_currency_format(str_ireplace('.', '', $partner_pricing[$sku]), $price['currency_code'], $product) . ')' : $sale_price_display ; 
          } else {
            $final_price_display = (empty($sale_price_display) || $sale_price_display == " ($0.00)") ? $price_display : $sale_price_display ;
          }
        }
      }

//      DEV-1074 overrides \ DEV-287 to re set Premier product display
//      //for PREMIERE we will not display price on products
//      if(in_array(66, array_keys($form['product_id']['#options']))){
//        $final_price_display = '';
//      }

      // Display only products that partner allows, if none defined display should be normal output
      if (module_exists('rcpar_partners')) {
        // Remove FLASH product is already in the users cart.
        if (enroll_flow_match_flash_in_cart($k)) {
          unset($form['product_id']['#options'][$k]);
          continue;
        }
      }
  
      $form['product_id']['#options'][$k] = $form['product_id']['#options'][$k] . $final_price_display;                  
    }
    $form['product_id']['#ajax']['callback'] = "enroll_flow_add_to_cart_form_attributes_refresh" ;
  }
 
}


/**
 * Submit handler for product list form
 */
function enroll_flow_product_list_form_submit($form, &$form_state) {
  global $user;
  
  // Set uid if we are passing it in --- for instance an admin user placing order for another user.
  (!empty($form_state['values']['account_uid'])) ? $uid = $form_state['values']['account_uid'] : $uid = $user->uid ;
  
  // 1) First clear cart and start again
  if (!empty($user->uid)) {
    $order = commerce_cart_order_load($user->uid);
    commerce_cart_order_empty($order);
  }
  
  // Now load new selections since the user has made it through step 1.
  foreach($form_state['values']['product_list'] AS $product_id) {
    if ($product_id != 0) {
      commerce_cart_product_add_by_id($product_id, '1', $uid);
    }
  }

  // Store for use later if needed
  $_SESSION['product_list_form'] = $form_state['values'];
  $form_state['redirect'] = 'cart';
}

function enroll_flow_user_pass_reset($form, &$form_state, $uid, $timestamp, $hashed_pass, $action = NULL) {
  global $user;

  // When processing the one-time login link, we have to make sure that a user
  // isn't already logged in.
  if ($user->uid) {
    // The existing user is already logged in.
    if ($user->uid == $uid) {
      drupal_set_message(t('You are logged in as %user. <a href="!user_edit">Change your password.</a>', array('%user' => $user->name, '!user_edit' => url("user/$user->uid/edit"))));
    }
    // A different user is already logged in on the computer.
    else {
      $reset_link_account = user_load($uid);
      if (!empty($reset_link_account)) {
        drupal_set_message(t('Another user (%other_user) is already logged into the site on this computer, but you tried to use a one-time link for user %resetting_user. Please <a href="!logout">logout</a> and try using the link again.',
          array('%other_user' => $user->name, '%resetting_user' => $reset_link_account->name, '!logout' => url('user/logout'))));
      } else {
        // Invalid one-time link specifies an unknown user.
        drupal_set_message(t('The one-time login link you clicked is invalid.'));
      }
    }
    drupal_goto();
  }
  else {
    // Time out, in seconds, until login URL expires. Defaults to 24 hours =
    // 86400 seconds.
    $timeout = variable_get('user_password_reset_timeout', 86400);
    $current = REQUEST_TIME;
    // Some redundant checks for extra security ?
    $users = enroll_flow_load_multiple($uid);
    if ($timestamp <= $current && $account = reset($users)) {
      // No time out for first time login.
      if ($account->login && $current - $timestamp > $timeout) {
        drupal_set_message(t('You have tried to use a one-time login link that has expired. Please request a new one using the form below.'));
        drupal_goto('user/password');
      }
      elseif ($account->uid && $timestamp >= $account->login && $timestamp <= $current && $hashed_pass == user_pass_rehash($account->pass, $timestamp, $account->login, $account->uid)) {
        // First stage is a confirmation form, then login
        if ($action == 'login') {
          // Set the new user.
          $user = $account;
          // user_login_finalize() also updates the login timestamp of the
          // user, which invalidates further use of the one-time login link.
          user_login_finalize();
          watchdog('user', 'User %name used one-time login link at time %timestamp.', array('%name' => $account->name, '%timestamp' => $timestamp));
          drupal_set_message(t('You have just used your one-time login link. It is no longer necessary to use this link to log in. Please change your password.'));
          // Let the user's password be changed without the current password check.
          $token = drupal_random_key();
          $_SESSION['pass_reset_' . $user->uid] = $token;
          drupal_goto('user/' . $user->uid . '/edit', array('query' => array('pass-reset-token' => $token)));
        }
        else {
          $form['message'] = array('#markup' => t('<p>This is a one-time login for %user_name and will expire on %expiration_date.</p><p>Click on this button to log in to the site and change your password.</p>', array('%user_name' => $account->name, '%expiration_date' => format_date($timestamp + $timeout))));
          $form['help'] = array('#markup' => '<p>' . t('This login can be used only once.') . '</p>');
          $form['actions'] = array('#type' => 'actions');
          $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Log in'));
          $form['#action'] = url("user/reset/$uid/$timestamp/$hashed_pass/login");
          return $form;
        }
      }
      else {
        drupal_set_message(t('You have tried to use a one-time login link that has either been used or is no longer valid. Please request a new one using the form below.'));
        drupal_goto('user/password');
      }
    }
    else {
      // Deny access, no more clues.
      // Everything will be in the watchdog's URL for the administrator to check.
      drupal_access_denied();
      drupal_exit();
    }
  }
}

function enroll_flow_load_multiple($uid) {
  db_update('users')
          ->fields(array('status' => 1))
          ->condition('uid', $uid)
          ->execute();
  
  return user_load_multiple(array($uid), array('status' => '1'));
}


/**
 * Ajax callback: returns AJAX commands when an attribute widget is changed.
 */
function enroll_flow_add_to_cart_form_attributes_refresh($form, $form_state) {
  $commands = array();

  // Render the form afresh to capture any changes to the available widgets
  // based on the latest selection.
  $commands[] = ajax_command_replace('.' . drupal_html_class($form['#form_id']), drupal_render($form));

  // Then render and return the various product fields that might need to be
  // updated on the page.
  if (!empty($form_state['context'])) {
    $product = commerce_product_load($form_state['default_product_id']);

    if(isset($product->field_product_image[LANGUAGE_NONE][0]['filename'])){
      $filename= $product->field_product_image[LANGUAGE_NONE][0]['filename'];
      $str = str_replace('.png', '', str_replace('-', ' ', $filename));    
      $product->field_product_image[LANGUAGE_NONE][0]['alt'] = $str;
      $form_state['default_product'] = $product;
      $product->display_context = $form_state['context']; 
    }

    // First render the actual fields attached to the referenced product.
    foreach (field_info_instances('commerce_product', $product->type) as $product_field_name => $product_field) {
      // Rebuild the same array of classes used when the field was first rendered.
      $replacement_class = drupal_html_class(implode('-', array($form_state['context']['class_prefix'], 'product', $product_field_name)));

      $classes = array(
        'commerce-product-field',
        drupal_html_class('commerce-product-field-' . $product_field_name),
        drupal_html_class('field-' . $product_field_name),
        $replacement_class,
      );

      $element = field_view_field('commerce_product', $product, $product_field_name, $form_state['context']['view_mode']);


      // Add an extra class to distinguish empty product fields.
      if (empty($element)) {
        $classes[] = 'commerce-product-field-empty';
      }

      // Append the prefix and suffix around existing values if necessary.
      $element += array('#prefix' => '', '#suffix' => '');
      $element['#prefix'] = '<div class="' . implode(' ', $classes) . '">' . $element['#prefix'];
      $element['#suffix'] .= '</div>';

      $commands[] = ajax_command_replace('.' . $replacement_class, drupal_render($element));
    }

    // Then render the extra fields defined for the referenced product.
    foreach (field_info_extra_fields('commerce_product', $product->type, 'display') as $product_extra_field_name => $product_extra_field) {
      $display = field_extra_fields_get_display('commerce_product', $product->type, $form_state['context']['view_mode']);

      // Only include extra fields that specify a theme function and that
      // are visible on the current view mode.
      if (!empty($product_extra_field['theme']) &&
        !empty($display[$product_extra_field_name]['visible'])) {
        
        // Rebuild the same array of classes used when the field was first rendered.
        $replacement_class = drupal_html_class(implode('-', array($form_state['context']['class_prefix'], 'product', $product_extra_field_name)));

        $classes = array(
          'commerce-product-extra-field',
          drupal_html_class('commerce-product-extra-field-' . $product_extra_field_name),
          $replacement_class,
        );

        // Theme the product extra field to $element.
        $variables = array(
          $product_extra_field_name => $product->{$product_extra_field_name},
          'label' => $product_extra_field['label'] . ':',
          'product' => $product,
        );
                 
          $element = array(
          '#markup' => theme($product_extra_field['theme'], $variables),
          '#attached' => array(
            'css' => array(drupal_get_path('module', 'commerce_product') . '/theme/commerce_product.theme.css'),
          ),
          '#prefix' => '<div class="' . implode(' ', $classes) . '">',
          '#suffix' => '</div>',
        );

        // Add an extra class to distinguish empty fields.
        if (empty($element['#markup'])) {
          $classes[] = 'commerce-product-extra-field-empty';
        }
        
        $commands[] = ajax_command_replace('.' . $replacement_class, drupal_render($element));
      }
    }
  }
  // On phones, the price in the Select product drop down is not visible because of the length of the product name.
  // Currently, the price of the FULL Package is displayed in a few divs with a class of "static-price" in the node content.
  // For the Select product, we are going to replace those divs when a choice is selected with the price of the part selected
  // from the product dropdown so the price will be visible for phones. See DEV-1100.
  if (stristr($form['#form_id'], 'commerce_cart_add_to_cart') && empty(array_diff(array_keys($form['product_id']['#options']), array(12,13,14,15,16)))) {
    // Product variation pages that use the page builder do not have $form_state['context'], so load the product for those pages here.
    if (!$product) {
      $product = commerce_product_load($form_state['default_product_id']);
    }
    $package = '';
    $markup_full_price = '';
    if ($product->sku == 'FULL') {
      $package = ' package';  
      //show sales price when we see a full product.
      if(isset($product->field_commerce_saleprice['und'][0]['amount'])){
        $partner = new RCPARPartner();
        if(!$partner->isPartnerPageLoaded()){
          $full_price = commerce_currency_format($product->field_commerce_saleprice['und'][0]['amount'], 'USD');
          $markup_full_price = '<del style="color:red">' . $full_price . '</del>';          
        }
      }
    }
    // Get the price and format it correctly.
    $price = commerce_currency_format($product->commerce_price['und'][0]['amount'], 'USD');
    // Create our markup.
    $markup = '<div class="static-price">' . $markup_full_price .  $price . ' for ' . $product->sku . $package . '</div>';

    // There is a jquery bug that doesn't allow replacing multiple elements with the same class
    // via one command, and we need three separate divs replaced. (They display in three different
    // fields for variations in layout and screen sizes.)
    $commands[] = ajax_command_replace('.field-name-field-details .static-price', $markup);
    $commands[] = ajax_command_replace('.field-name-field-right-content .static-price', $markup);
    $commands[] = ajax_command_replace('.field-name-field-specs .static-price', $markup);

    // Product variation pages that use a flexslider image display need the product images to swap out when the form changes.
    // Get the product image.
    $product_img = entity_metadata_wrapper('commerce_product', $product)->field_product_image->value();
    // Get the path of the file.
    $file_url = file_create_url($product_img[0]['uri']);
    // Create markup for the larger image display.
    $large_img = '<img src="'. image_style_url('product_image_carousel', $product_img[0]['uri']) . '" draggable="false">';
    // Create markup for the thumbnail image display.
    $small_img = '<img src="'. image_style_url('product_image_thumbnail', $product_img[0]['uri']) . '" draggable="false">';
    // Replace them via ajax.
    $commands[] = ajax_command_replace('.flex-viewport li:first-child img', $large_img);
    $commands[] = ajax_command_replace('.flex-control-nav li:first-child img', $small_img);
  }



    // Allow other modules to add arbitrary AJAX commands on the refresh.
  drupal_alter('commerce_cart_attributes_refresh', $commands, $form, $form_state);

  return array('#type' => 'ajax', '#commands' => $commands);
}