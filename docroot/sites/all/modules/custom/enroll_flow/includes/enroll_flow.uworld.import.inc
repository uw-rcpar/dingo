<?php

use RCPAR\Import\UWorldImport;
use RCPAR\Import\UWorldOrderProcessor;
use RCPAR\Import\UWorldUserProcessor;

/**
 * Implements hook_cron().
 */
function enroll_flow_cron() {
  //process in queue the remaining users daily for teleportation
  enroll_flow_cron_teleportation_elegibility();
  // Run API import from UWorld, if enabled
  if (variable_get('enable_uworld_configurations', FALSE) && variable_get('uworld_import_order_user_cron', TRUE)) {
    enroll_flow_cron_uworld_import();
  }
}

/**
 * UWorld order/user import management form.
 * Menu callback for admin/settings/rcpar/system_settings/enroll_flow/imports
 */
function enroll_flow_order_user_form($form, &$form_state) {
  // Perform actions from the log table, if specified
  if (!empty($_GET['action'])) {
    switch ($_GET['action']) {
      case 'delete':
        db_query("DELETE FROM enroll_flow_uw_import_log WHERE id = :id", [':id' => $_GET['id']]);
        drupal_set_message("Deleted log entry: " . $_GET['id']);
        break;
      case 'rerun':
        UWorldImport::rerun($_GET['id']);
        drupal_set_message("Re-ran import for log entry: " . $_GET['id']);
        break;
    }
    // Redirect in order to clear URL params
    drupal_goto('admin/settings/rcpar/system_settings/enroll_flow/imports', ['query' => ['page' => $_GET['page']]]);
  }

  $date_format = 'M jS, Y g:ia';

  ///////////////// Settings ///////////////
  $form['settings'] = array(
    '#title' => 'Settings',
    '#type' => 'fieldset',
  );
  $form['settings']['width'] = array(
    '#title' => 'Import width (seconds)',
    '#type' => 'textfield',
    '#default_value' => UWorldImport::getImportWidth(),
  );
  $form['settings']['settings_submit'] = array(
    '#type' => 'submit',
    '#value' => 'Change settings',
    '#submit' => array('enroll_flow_order_user_settings_form_submit'),
  );

  ///////////////// Info /////////////
  // Invoke scheduled import
  $form['normal_import'] = array(
    '#title' => 'Invoke next scheduled import',
    '#type' => 'fieldset',
  );
  $form['normal_import']['info'] = array(
    '#markup' => '<p>Last successful automated import time: ' . date($date_format, UWorldImport::getLastImportTime()) . '</p>' .
      '<p>Import range width: ' . UWorldImport::getImportWidth() / 60 . ' minutes<p>' .
      '<p>Next scheduled import range: ' .
      date($date_format, UWorldImport::getLastImportTime()) . ' to ' .
      date($date_format, UWorldImport::getLastImportTime() + UWorldImport::getImportWidth()) .
      '</p>',
  );

  // Submit button
  $form['normal_import']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#submit' => array('enroll_flow_order_user_form_submit'),
  );

  ///////////////// API testing utility /////////////
  $form['api_test'] = array(
    '#title' => 'Run test import(orders and users)',
    '#type' => 'fieldset',
  );
  $form['api_test']['from'] = array(
    '#title' => 'From time',
    '#type' => 'textfield',
    '#description' => 'Specifies the beginning of the date range for which to request API data. <br>
      Formatted as UNIX timestamp.',
    '#default_value' => $_GET['from'],
  );
  $form['api_test']['to'] = array(
    '#title' => 'To time',
    '#type' => 'textfield',
    '#description' => 'Specifies the end of the date range for which to request API data. <br>
      Formatted as UNIX timestamp.',
    '#default_value' => $_GET['to'],
  );
  $form['api_test']['order_id'] = array(
    '#title' => 'UWorld Order ID',
    '#type' => 'textfield',
    '#description' => 'Specify a specific order ID to query. When this parameter is present, only the order importer will be run and "from" and "to" parameters will be ignored.',
    '#default_value' => $_GET['order_id'],
  );
  $form['api_test']['user_id'] = array(
    '#title' => 'UWorld User ID',
    '#type' => 'textfield',
    '#description' => 'Specify a specific user ID to query. When this parameter is present, only the user importer will be run and "from" and "to" parameters will be ignored.',
    '#default_value' => $_GET['user_id'],
  );
  $form['api_test']['strtotime'] = array(
    '#title' => 'Use strtotime()',
    '#type' => 'checkbox',
    '#description' => 'When checked, dates entered in from/to will be converted to timestamps using strtotime() for your convenience.',
  );
  $form['api_test']['update_last'] = array(
    '#title' => 'Update last run time',
    '#type' => 'checkbox',
    '#description' => 'When checked, the last run time will be updated to the value of the "to" field, which will cause
    cron runs to resume at this time/date.',
  );
  $form['api_test']['users_only'] = array(
    '#title' => 'Users only',
    '#type' => 'checkbox',
    '#description' => 'When checked, only the users will be imported, orders will be ignored.',
  );
  $form['api_test']['email_errors'] = array(
    '#title' => 'Email errors',
    '#type' => 'checkbox',
    '#description' => 'When checked, an email will be sent when any errors are encountered in the job.',
  );
  $form['api_test']['debug_output'] = array(
    '#title' => 'Show API response only (dry run)',
    '#type' => 'checkbox',
    '#description' => 'When checked, the API response will be displayed on the screen for debugging purposes, no data will be saved.',
  );

  $form['api_test']['api_submit'] = array(
    '#type' => 'submit',
    '#value' => 'Import',
    '#validate' => array('enroll_flow_api_test_form_validate'),
    '#submit' => array('enroll_flow_api_test_form_submit'),
  );

  // Build a log table
  $header = array(
    'ID',
    'Type',
    'Records available',
    'Records Successful',
    'Start Time',
    'End time',
    'Message',
    'Has Failures',
    'Failed Ids',
    'Attempts',
    'First attempted',
    'Last attempted',
    'Completed',
    'Actions',
  );

  // Build a pager query
  $query = db_select('enroll_flow_uw_import_log', 'l')->extend('PagerDefault');
  $query->fields('l');
  
  // Errors only?
  if(!empty($_GET['errors'])) {
    $query->condition('has_failures', '1');
  }

  // Change the number of rows with the limit() call.
  $limit = !empty($_GET['limit']) ? $_GET['limit'] : 20;
  $result = $query
    ->limit($limit)
    ->orderBy('l.id', 'DESC')
    ->execute();

  // Go through the query results and build a table
  $rows = array();
  foreach ($result as $row) {
    // Build the table row and add it.
    $table_row = array(
      $row->id,
      $row->type,
      $row->records_available,
      $row->records_successful,
      format_date($row->start_time, 'custom', '' . $date_format . ''),
      format_date($row->end_time, 'custom', $date_format),
      $row->message,
      $row->has_failures ? '<strong style="color:red">Yes</strong>' : 'No',
      implode(', ', json_decode($row->failed_ids)),
      $row->attempts,
      format_date($row->first_attempted, 'custom', $date_format),
      format_date($row->last_attempted, 'custom', $date_format),
      format_date($row->completed, 'custom', $date_format),
      l('Delete', current_path(), array('query' => ['action' => 'delete', 'id' => $row->id, 'page' => $_GET['page']])) . ' | ' .
      l('Re-run', current_path(), array('query' => ['action' => 'rerun', 'id' => $row->id, 'page' => $_GET['page']]))
    );

    $rows[] = $table_row;
  }

  // Create a render array ($build) which will be themed as a table with a pager.
  $build['pager_table'] = array(
    '#caption' => 'Import log',
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There were no log records found.'),
  );

  // Attach the pager theme.
  $build['pager_pager'] = array('#theme' => 'pager');

  // Add the log table back into the render array
  $form['log'] = $build;

  return $form;
}

/**
 * Form submit function for enroll_flow_order_user_form
 * Imports a JSON file of MCQ data
 */
function enroll_flow_order_user_form_submit(&$form, &$form_state) {
  enroll_flow_cron_uworld_import();
}

/**
 * Form submit function for enroll_flow_order_user_form
 * Imports a JSON file of MCQ data
 */
function enroll_flow_order_user_settings_form_submit(&$form, &$form_state) {
  variable_set('uworld_order_range_width', $form_state['values']['width']);
  drupal_set_message('Updated import width.');
}

/**
 * Validate that our API dates are parsed correctly.
 */
function enroll_flow_api_test_form_validate(&$form, &$form_state) {
  // When order ID or user ID is used, we don't need to validate from/to
  if(!empty($form_state['values']['order_id'])) {
    return;
  }
  if(!empty($form_state['values']['user_id'])) {
    return;
  }

  // Check that time conversion is working before trying to run an update
  $from = $form_state['values']['from'];
  $to = $form_state['values']['to'];
  $strtotime = $form_state['values']['strtotime'];


  if ($strtotime) {
    $from = strtotime($from);
    $to = strtotime($to);
  }
  else {
    if (!is_numeric($from)) {
      form_set_error('from', '"From" needs to be a numeric UNIX timestamp');
    }
    if (!is_numeric($to)) {
      form_set_error('to', '"To" needs to be a numeric UNIX timestamp');
    }
  }

  if(!$from) {
    form_set_error('from', 'Could not convert the "from" field into a timestamp. Try a different format.');
  }
  if(!$to) {
    form_set_error('to', 'Could not convert the "to" field into a timestamp. Try a different format.');
  }
}

/**
 * Form submit function for API testing from enroll_flow_order_user_form
 * Gets data from the UWorld API and optionally saves it to the database as well.
 */
function enroll_flow_api_test_form_submit(&$form, &$form_state) {
  $from = $form_state['values']['from'];
  $to = $form_state['values']['to'];
  $strtotime = $form_state['values']['strtotime'];
  $debug_output = $form_state['values']['debug_output'];
  $update_last = $form_state['values']['update_last'];
  $email_errors = $form_state['values']['email_errors'];
  $order_id = $form_state['values']['order_id'];
  $user_id = $form_state['values']['user_id'];
  $users_only = $form_state['values']['users_only'];

  // Convert date/time from human readable to timestamp
  if ($strtotime) {
    $from = strtotime($from);
    $to = strtotime($to);
  }

  // Debugging only
  if($debug_output) {
    // Query the API - order only
    if(!empty($order_id)) {
      $order_data = UWorldOrderProcessor::apiRequest(NULL, NULL, $order_id);
    }
    // Query the API - user only
    elseif(!empty($user_id)) {
      $user_data = UWorldUserProcessor::apiRequest(NULL, NULL, $user_id);
    }
    // Users only
    elseif(!empty($users_only)) {
      $user_data = UWorldUserProcessor::apiRequest($from, $to);
    }
    // Users and orders
    else {
      $order_data = UWorldOrderProcessor::apiRequest($from, $to);
      $user_data = UWorldUserProcessor::apiRequest($from, $to);
    }


    // Output
    if (function_exists('dpm')) {
      dpm(json_decode($order_data), 'Decoded Order Data');
      dpm(json_decode($user_data), 'Decoded User Data');
    }
    else {
      drupal_set_message('<strong>Decoded Order Data</strong><pre>' . print_r(json_decode($order_data), TRUE) . '</pre><hr>');
      drupal_set_message('<strong>Decoded User Data</strong><pre>' . print_r(json_decode($user_data), TRUE) . '</pre>');
    }

    return;
  }

  // When order ID is specified, run only the order importer
  if(!empty($order_id)) {
    $order_data = UWorldOrderProcessor::import(NULL, NULL, $order_id);
  }
  // When user ID is specified, run only the user importer
  elseif(!empty($user_id)) {
    $user_data = UWorldUserProcessor::import(NULL, NULL, $user_id);
  }
  // Users only
  elseif(!empty($users_only)) {
    $user_data = UWorldUserProcessor::import($from, $to);
  }
  // Otherwise, run both the user and order importer
  else {
    UWorldImport::import($from, $to, $update_last, $email_errors);
  }
}


/**
 * Import order and user data from the UWorld API.
 */
function enroll_flow_cron_uworld_import() {
  // Get the last known time that an import completed.
  $from = UWorldImport::getLastImportTime();

  // Add 15 minutes to our last import time, so we can get the next increment worth of data
  $to = $from + UWorldImport::getImportWidth();

  // Don't run the import unless the current time is actually later than the time we're requesting data for,
  // otherwise we may never get data for that time.
  if (time() > $to) {
    UWorldImport::import($from, $to, TRUE, TRUE);
  }
  else {
    drupal_set_message('API "to" time is in the future - did not run.');
  }
}


/**
 * process users elegible to new platform
 */
function enroll_flow_cron_teleportation_elegibility() {
  $queue = DrupalQueue::get('teleportation_elegibility');
   //if current time is older than last time we process
   if (time() > enroll_flow_get_last_eligibility_check()) {
     //next time to process will be tomorrow
     enroll_flow_set_last_import_time(strtotime("tomorrow"));
     
     $result = db_query('SELECT uid FROM enroll_flow_uw_teleport WHERE '
         . 'teleported = 0 and (phase1_eligible = 1 OR phase2_eligible = 1 '
         . 'OR phase3_eligible = 1 or zero_progress_eligible = 1)');
     
     foreach ($result as $record) {
       $queue->createItem($record);   
     }     
   }
}

/**
 * Process a single user for teleportation this on the backend
 * @param <int> $uid unique identifier of the user to process
 */
function enroll_flow_user_teleportation_elegibility($data) {
  try {
    $t = new \RCPAR\UW\UserTeleportation($data->uid);
    // If the user has already migrated then move along
    if ($t->isUserMigrated()) {
      return;
    }
    $t->calculateZeroProgressEligibility();
    $t->calculateEligibility('phase1');
    $t->calculateEligibility('phase2');
    $t->calculateEligibility('phase3');
  }
  catch (Exception $ex) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . 'Procesing uid: ' . $data->uid . 'Yells error: '. $ex->getMessage());  
  }
}

/**
 * The last recorded time that an eligibility Check hapends.
 * @return int
 * - UNIX timestamp.
 */
function enroll_flow_get_last_eligibility_check() {
  return variable_get('uworld_eligibility_check', strtotime('January 1st, 2020'));
}

/**
 * Set the time that the eligibility Check hapends.
 * @param int $time
 * - UNIX timestamp.
 */
function enroll_flow_set_last_import_time($time) {
  return variable_set('uworld_eligibility_check', $time);
}

/*
 * Implements hook_cron_queue_info().
 */
function enroll_flow_cron_queue_info(){
  $queues['teleportation_elegibility'] = array(
    'worker callback' => 'enroll_flow_user_teleportation_elegibility',
	'time' => 120,
  );
  return $queues;
}