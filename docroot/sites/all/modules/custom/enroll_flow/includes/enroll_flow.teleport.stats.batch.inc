<?php

/**
 * Batch form with swoopy built-in resume field. Will import all users with active entitlements into the enroll_flow_uw_teleport
 * @param $form
 * @param $form_state
 * @return mixed
 */
function enroll_flow_import_unmigrated_users_batch_form($form, &$form_state) {

  $form['resumeat'] = array(
    '#title' => 'Resume the batch at the given UID and above.',
    '#description' => 'Leave blank to start from the beginning.<br>' .
      'The last UID marked as processed was: ' . variable_get('enroll_flow_import_users_resume', 0),
    '#type' => 'textfield',
  );

  $form['markup'] = array(
    '#markup' => '<strong>DO NOT RUN ON PRODUCTION</strong> until eligibility thresholds have been lifted and all users can migrate at will.<br>',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Execute Batch',
  );

  return $form;

}

/**
 * Form callback for enroll_flow_import_unmigrated_users_batch_form
 * Initiates the batch job.
 */
function enroll_flow_import_unmigrated_users_batch_form_submit($form, &$form_state) {

  $resumeat = 0;

  // We will use the pre_phase3_count variable to calculate phase 3 users by running the batch once with the phase3 flaff off and once with it on
  if (!variable_get('uw_phase3_teleport', FALSE)) {
    // Reset the pre phase3 count variable to 0
    variable_set('pre_phase3_count', 0);
  }

  if (!empty($form_state['values']['resumeat'])) {
    $resumeat = $form_state['values']['resumeat'];
  }

  enroll_flow_import_unmigrated_users_batch($resumeat);
}

/**
 * The batch callback. Build batch operations.
 * @param int $resumeat
 * - A user ID to start from. Allows us to resume a previously failed job.
 */
function enroll_flow_import_unmigrated_users_batch($resumeat = 0) {
  $batch = array(
    'operations' => array(),
    'finished' => 'enroll_flow_import_unmigrated_users_batch_finished',
    'title' => t('Import un-migrated users to teleport table'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'enroll_flow') . '/includes/enroll_flow.teleport.stats.batch.inc'
  );

  // Get a list of user IDs with active, non-expired entitlements
  $uids = db_query("SELECT users.uid
                            FROM 
                            field_data_field_expiration_date field_data_field_expiration_date
                            INNER JOIN node ON node.nid = field_data_field_expiration_date.entity_id
                            INNER JOIN users ON users.uid = node.uid
                            WHERE  (field_data_field_expiration_date.field_expiration_date_value > UNIX_TIMESTAMP()) 
                            AND (node.status = 1) 
                            AND (field_data_field_expiration_date.entity_type = 'node') 
                            AND (field_data_field_expiration_date.bundle = 'user_entitlement_product')
                            AND  users.uid >= :res
                            GROUP BY users.uid", [':res' => $resumeat])->fetchCol();

  // Set up batches of x number of users per operation
  foreach (array_chunk($uids, variable_get('enroll_flow_import_teleport_batch_size', 100)) as $uids_chunk) {
    $batch['operations'][] = array('enroll_flow_import_unmigrated_users_batch_process', array($uids_chunk));
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/rcpar/system_settings/enroll_flow/teleport-batch'); // The path to redirect to when done.
  }
}

/**
 * Batch operation callback.
 * @param int[] $uids
 * - Array of user ids to process.
 * @param $context
 * - The system's batch information.
 */
function enroll_flow_import_unmigrated_users_batch_process($uids, &$context) {
  foreach ($uids as $uid) {
    enroll_flow_import_unmigrated_users_batch_process_uid($uid, $context);
  }

  // Save our resume-at marker at the last processed uid
  variable_set('enroll_flow_import_users_resume', $uid);
}

/**
 * Process an individual user and calculate their migration eligibility.
 * @param int $uid
 * - The user id to process.
 */
function enroll_flow_import_unmigrated_users_batch_process_uid($uid, &$context) {
  try {
    $t = new \RCPAR\UW\UserTeleportation($uid);

    // If the user has already migrated then move along
    if ($t->isUserMigrated()) {
      return;
    }
    $t->calculateZeroProgressEligibility();
    $t->calculateEligibility('phase1');
    $t->calculateEligibility('phase2');
    $t->calculateEligibility('phase3');
  }
  catch (Exception $e) {
    $context['results']['failed_uids'][] = $uid;
    return;
  }
}

/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function enroll_flow_import_unmigrated_users_batch_finished($success, $results, $operations) {
  if ($success) {

    // If phase 3 is off, store the row count so we can compare to another batch with phase3 on
    if (!variable_get('uw_phase3_teleport', FALSE)) {
      // Reset the pre phase3 count variable to the total number of eligible, unmigrated users
      variable_set('pre_phase3_count', \RCPAR\UW\UserTeleportation::countEligibleUnmigrated());
    }

    drupal_set_message('Batch completed!');
    drupal_set_message('Failed UIDs: <pre>' . print_r($results['failed_uids'], TRUE) . '</pre>', 'error');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}