<?php

const SALES_FUNNEL_WIDGET_FIELDS = array(
  'field_did_you_graduate_college', // "Did you or are you currently attending college in the USA?"
  'field_college_state_list', // "What state is your campus located in?"
  'field_college_state', // "University / College"
  'field_college_name', // "College name"
  'field_when_do_you_plan_to_start', // "When do you plan to start studying for the CPA Exam?"
  'field_graduation_month_and_year',// "Graduation Date" (Note that despite of its name, it also includes the day)
  'field_college_sf_account', // Hidden entity reference to an sf_account for the corresponding university
);

define('COLLEGE_NOT_IN_LIST_OPT_ID', 158);

/**
 * This function appends the sales funnel widget fields (a couple of user profile fields wrapped into a container)
 *  note that the widget will be container within a fieldset named 'sales_funnel_widget'
 * @param $form
 *  Form to which we want to append our widget
 * @param $form_state
 *  Form state variable related to the form to which we want to append our widget
 * @param $account
 *  User account that we want to update with the fields collected information
 * @param string $parent_element
 *  There are cases where we don't want to place the widget container at the root level of the $form
 *  to place it inside for example of $form['dummy_element'], you need to send $parent_element = "dummy_element"
 *  IMPORTANT NOTE: dummy_element must have '#tree' property set as TRUE
 * @return mixed
 */
function sales_funnel_widget_append_sales_funnel_widget(&$form, &$form_state, $account, $parent_element = NULL) {
  // We are going to need this on the submit function
  $account_id = NULL;
  // node variable will be use to save if is a node object.
  $node = false;
  if(isset($form['#node'])){
    // Save the node data.
    $node = $form['#node'];
  }
  if (isset($account->uid)) {
    $form_state['storage']['sales_funnel_widget']['account_id'] = $account->uid;
  }
  $form_state['storage']['sales_funnel_widget']['parent_element'] = $parent_element;

  // sales_funnel_widget is a placeholder element
  // we are going to put all the sales funnel widget there
  if ($parent_element){
    $form[$parent_element]['sales_funnel_widget'] = array(
      '#type'        => 'fieldset',
      '#collapsible' => FALSE,
      '#tree'        => TRUE,
      '#element_validate' => array('sales_funnel_widget_container_validate'),
    );
    // If we want to place this widget inside of a parent element we will have to include on the set of
    // parents of the elements that we are going to add
    $tmpform['#parents'] = array($parent_element,'sales_funnel_widget');
    $our_form = &$form[$parent_element]['sales_funnel_widget'];
  }
  else {
    $form['sales_funnel_widget'] = array(
      '#type'        => 'fieldset',
      '#collapsible' => FALSE,
      '#tree'        => TRUE,
      '#element_validate' => array('sales_funnel_widget_container_validate'),
    );
    $tmpform['#parents'] = array('sales_funnel_widget');
    $our_form = &$form['sales_funnel_widget'];
  }

  // Here we pick some fields of the user profile and put them into the container
  foreach (SALES_FUNNEL_WIDGET_FIELDS as $field_name) {
    // Create the field widget using field module API
    $field = field_info_field($field_name);
    if($node){
      // If is a node, get the form structure in the correct format.
      $instance = field_info_instance('node',$field_name, $form['type']['#value']);

      // If is a node we need to generate a empty field
      if(isset($node->$field_name)){
        $node->$field_name[LANGUAGE_NONE][]['value'] = "";
      }

      $my_field = field_default_form('node', $node, $field, $instance,
      LANGUAGE_NONE, field_get_items("node", $node, $field_name), $tmpform, $form_state);
    }
    else{
      $instance = field_info_instance('user', $field_name, 'user');

      $my_field = field_default_form('user', $account, $field, $instance,
        'und', $account->{$field_name}['und'],
        $tmpform, $form_state);
    }
    // Add it to the sales_funnel_widget tree
    $our_form += (array) $my_field;

    // Delete the original values (in case they existed, like on the user edit form)
    unset($form_state['field'][$field_name]);
    // Now, we delete the original fields, in case their existed already in the form
    if ($parent_element) {
      // Here we are assuming that if the elements exists in the form, the are placed inside $parent_element
      unset($form[$parent_element][$field_name]);
    }
    else {
      unset($form[$field_name]);
    }
  }

  $our_form['field_college_name'][LANGUAGE_NONE][0]['value']['#title'] = 'Enter your college name';

  $our_form['field_college_state'][LANGUAGE_NONE]['#title'] = 'College (legacy taxonomy)';
  $our_form['field_college_state']['#attributes']['class'][] = 'hidden';

  // If there are two lead forms on the same page, the ajax gets screwed up in Chrome.
  // Give a unique ID to the college wrapper to prevent.
  // When the form option list is being rebuilt via AJAX, we want to ensure the same wrapper ID is used that submitted
  // the form, because otherwise the form builder keeps appending to it and the ajax response will have a different ID.
  if (isset($form_state['triggering_element'])
      && isset($form_state['triggering_element']['#ajax'])
      && strstr($form_state['triggering_element']['#ajax']['wrapper'], 'college-wrapper')) {
    $college_wrapper_id = $form_state['triggering_element']['#ajax']['wrapper'];
  }
  else {
    $college_wrapper_id = drupal_html_id('college_wrapper');
  }
  $our_form['field_college_state_list'][LANGUAGE_NONE]['#ajax'] = array(
    'event'    => 'change',
    'wrapper'  => $college_wrapper_id,
    'callback' => 'sales_funnel_widget_college_ajax_callback',
    'method'   => 'replace',
  );
  $our_form['field_college_state_list'][LANGUAGE_NONE]['#required'] = TRUE;
  $our_form['field_college_state_list']['#required'] = TRUE;

  // Go to the ticket DEV-608
  // in order to get more information about this changes
  $our_form['#title'] = t("Please let us know where you attended college so that we can provide you with a more optimized learning experience.");
  $our_form['field_did_you_graduate_college']['und']['#default_value'] = 1;
  $our_form['field_did_you_graduate_college']['#attributes']['class'][] = 'hidden';

  // These items were not deleted directly in the user field because there were some users
  // that  have this values selected previously, so we rather just remove them in the form
  // and in the fututre we can roll them back if needed.
  $items_to_remove = array('AS', 'FM', 'MH', 'MP', 'PW');
  foreach ($items_to_remove as $key) {
    unset($our_form['field_college_state_list']['und']['#options'][$key]);
  }

  // Grab the submitted form values. Location differs based on the use of a parent element.
  if($parent_element) {
    $values = $form_state['values'][$parent_element]['sales_funnel_widget'];
  }
  else {
    $values = $form_state['values']['sales_funnel_widget'];
  }

  if (isset($values['field_did_you_graduate_college'][LANGUAGE_NONE][0]['value'])) {
    $did_you_grad = $values['field_did_you_graduate_college'][LANGUAGE_NONE][0]['value'];
  }
  else {
    $did_you_grad = $our_form['field_did_you_graduate_college'][LANGUAGE_NONE]['#default_value'][0];
  }

  // Determine the selected state either from the form values or default form value
  if (isset($values['field_college_state_list'][LANGUAGE_NONE][0])) {
    // Note that when '_none' is selected, $values['field_college_state_list'][LANGUAGE_NONE][0]['value'] is actually
    // set to NULL, so an isset() test for that variable returns false. However, if no post value is there at all,
    // its other array tree structure will not be present and we'll fall back on the form default value
    $state = $values['field_college_state_list'][LANGUAGE_NONE][0]['value'];
  }
  else {
    $state = $our_form['field_college_state_list'][LANGUAGE_NONE]['#default_value'][0];
  }

  // Get the default value for college - comes from values on an ajax rebuild, otherwise it's in the form defaults.
  if (isset($values['field_college_sf_account'][LANGUAGE_NONE][0]['target_id'])) {
    $form_el_def_val = $values['field_college_sf_account'][LANGUAGE_NONE][0]['target_id'];
  }
  else {
    // If we have a state selected and a value entered for college name, sf_account is technically empty, but we
    // want it to show as other. We only read college name from the form defaults though, because there may be post
    // values left in the field if the user entered something and then decided to pick a college from the list instead.
    if(!empty($state) && $state != '_none' && !empty($our_form['field_college_name'][LANGUAGE_NONE][0]['value']['#default_value'])) {
      $form_el_def_val = 'other';
    }
    // Fallback on the form default
    else {
      $form_el_def_val = $our_form['field_college_sf_account'][LANGUAGE_NONE]['#default_value'][0];
    }
  }

  $form_el_opts = array('' => '- Select a state -');
  $data_tokens = array();
  // If we have a state selected, populate a list of available colleges for that state.
  if (!empty($state) && $state != '_none') {
    $college_options = sf_account_get_college_names_by_state($state);
    $form_el_opts = $college_options['options'];
    $data_tokens = $college_options['data_tokens'];
  }

  $our_form['field_college_sf_account']['#prefix'] = '<div id="' . $college_wrapper_id . '">';
  $our_form['field_college_sf_account']['#suffix'] = '</div>';
  $our_form['field_college_sf_account'][LANGUAGE_NONE]['#options'] = $form_el_opts;
  // For some reason #default_value comes through as an array, but it should be a scalar.
  $our_form['field_college_sf_account'][LANGUAGE_NONE]['#default_value'] = $form_el_def_val;
  $our_form['field_college_sf_account'][LANGUAGE_NONE]['#data_tokens'] = $data_tokens;
  $our_form['field_college_sf_account'][LANGUAGE_NONE]['#required'] = TRUE;

  // We no longer use autocomplete to suggest colleges from taxonomy, but who knows what the future may bring.
  //$our_form['field_college_name'][LANGUAGE_NONE][0]['value']['#autocomplete_path'] = 'get_state_college_selection/autocomplete';
  //$our_form['field_college_name'][LANGUAGE_NONE][0]['value']['#required'] = true;

  // Monthpicker js
  $form['#attached']['js'][] = '/sites/all/libraries/jquery.monthpicker/jquery.mtz.monthpicker.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'rcpar_mods') . '/js/month-picker.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'sales_funnel_widget') . '/sales_funnel_widget.js';

  // We need to run a special logic for the submit and the validation of our widget
  $form['#submit'][] = 'sales_funnel_user_fields_widget_submit';
  $form['#validate'][] = 'sales_funnel_user_fields_widget_validate';
  return $form;
}

/**
 * Element validation function for the funnel widget container element
 * Note that we are not really using it to validate anything, we are just
 * using it to move the error messages to another position in the commerce form (DIN-1259)
 * @param $form
 * @param $form_state
 * @return bool
 */
function sales_funnel_widget_container_validate($form, &$form_state) {
  // Note that the place where the data is going to be placed might be different if we placed our widget
  // inside a container element (see sales_funnel_widget_append_sales_funnel_widget doc block for an explanation
  // of the $parent_element var)
  if (isset($form_state['storage']['sales_funnel_widget']['parent_element']) && !isset($form_state['values']['sales_funnel_widget'])) {
    $parent_element = $form_state['storage']['sales_funnel_widget']['parent_element'];
    $values = &$form_state['values'][$parent_element]['sales_funnel_widget'];
    // Also, the path to fin the elements when we invoke form_set_error is different
    $path_to_fields = $parent_element . '][sales_funnel_widget]';
  }
  else {
    $values = &$form_state['values']['sales_funnel_widget'];
    $path_to_fields = 'sales_funnel_widget]';
  }

  // User selected a college of "other"
  if ($values['field_college_sf_account'][LANGUAGE_NONE][0]['target_id'] == 'other') {
    // "College name" field must not be empty in this case
    if (empty($values['field_college_name'][LANGUAGE_NONE][0]['value'])) {
      // Prevent duplicate error reporting on this field.
      sales_funnel_widget_form_unset_error($form['field_college_name'][LANGUAGE_NONE][0]['#title'] . " field is required.");
      $error_msg = 'If "OTHER" is selected, <em>' . $form['field_college_name'][LANGUAGE_NONE][0]['value']['#title'] . '</em> cannot be empty!';
      form_set_error($path_to_fields . '[field_college_name', $error_msg);
    }
    // "Other" is selected and the user has typed in a college:
    else {
      // When "other" is selected for field_college_sf_account, we have to replace it with a valid value
      // for the entity reference.
      if ($values['field_college_sf_account'][LANGUAGE_NONE][0]['target_id'] == "other") {
        form_set_value($form['field_college_sf_account'], array(LANGUAGE_NONE => array(0 => ['target_id' => 0])), $form_state);
      }
    }
  }
  // If the user did not select "other", we want to be sure to clear out any garbage leftover in the college name field.
  else {
    form_set_value($form['field_college_name'], array(LANGUAGE_NONE => array(0 => ['value' => ''])), $form_state);
  }

  // We just want to do something when the widget is on the commerce checkout pane
  if ($form_state['build_info']['form_id'] == 'commerce_checkout_form_student_account_info') {
    $form_errors = form_get_errors();
    foreach ($form_errors as $element => $error) {
      if (strpos($element, 'commerce_user_profile_pane][sales_funnel_widget]') === 0) {
        // Sales funnel widget element error
        // we are going to put a message on the standard messages
        // and we are going to hide the original messages
        $new_el = str_replace('commerce_user_profile_pane][sales_funnel_widget]',
          'commerce_fieldgroup_pane__group_additional_info][sales_funnel_widget]',
          $element
        );
        form_set_error($new_el, $error);
      }
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function sales_funnel_widget_form_alter(&$form, &$form_state, $form_id) {
  global $user;

  switch ($form_id) {
    case 'user_register_form':
    case 'user_profile_form':
      $account = $form_state['build_info']['args'][0];
      sales_funnel_widget_append_sales_funnel_widget($form, $form_state, $account);
      // Administrators need to bypass form validation on some fields when updating user profiles
      if (user_access('administer users')) {
        $form["sales_funnel_widget"]["field_college_state_list"]["#required"] = false;
        $form["sales_funnel_widget"]["field_college_state_list"]["und"]["#required"] = false;
        $form["sales_funnel_widget"]["field_graduation_month_and_year"]["und"][0]["#required"] = false;
        $form["sales_funnel_widget"]["field_when_do_you_plan_to_start"]["und"]["#required"] = false;
        $form["sales_funnel_widget"]["field_when_do_you_plan_to_start"]["und"][0]["#required"] = false;
      }
      $form['sales_funnel_widget']['#weight'] = 10;
      $form['actions']['#weight'] = 100;
      break;
    case 'commerce_checkout_form_student_account_info':
      $account = $form['commerce_user_profile_pane']['#user'];
      if ($account->uid == 0) {
        // This is the same strategy as rcpar_checkout.module rcpar_checkout_page
        // Construct a fake user entity to display this data
        $account = entity_create('user', array('uid' => 0));
      }
      $wrapper = entity_metadata_wrapper('user', $account);
      $user_profile_pane_defaults = rcpar_checkout_get_user_profile_pane_defaults($form_state['order']);

      // We'll always want to set defaults to any that were saved w/ the order.
      // This is especially important w/ anon users.
      foreach ($user_profile_pane_defaults AS $field_name => $value) {
        if (!is_null($value) && (!empty($value) || $value === '0')) {
          if ($field_name == 'field_when_do_you_plan_to_start' || $field_name == 'field_graduation_month_and_year') {
            // Sometimes these values will show as datetime formats which will
            // cause an exception, if so we'll change them to a timestamp.
            if (gettype($value) == 'string') {
              $value = strtotime($value);
            }
          }
          $wrapper->$field_name->set($value);
        }
      }

      sales_funnel_widget_append_sales_funnel_widget($form, $form_state, $account, 'commerce_user_profile_pane', $form_state['order']);

      // We need to place it just before the continue button
      $form['sales_funnel_widget']['#weight'] = 20;
      $form['buttons']['#weight'] = 21;
      // sales_funnel_widget_append_sales_funnel_widget adds the submit function to
      // $form['#submit'] var (which doesn't even exists for this form)
      // the correct place to place it is on $form['buttons']['continue']['#submit']
      // (same thing applies to validation function)
      unset($form['#submit']);
      unset($form['#validate']);
      $form['buttons']['continue']['#submit'][] = 'sales_funnel_user_fields_widget_submit';
      break;
    case 'rcpar_asl_create_account_form':
      // This includes FSL and Freetrial forms too (they are all the same form)
      // As this is a user creation form, we won't have an account
      // we can just send null to sales_funnel_widget_append_sales_funnel_widget
      $account = NULL;

      sales_funnel_widget_append_sales_funnel_widget($form, $form_state, $account);
      $form['sales_funnel_widget']['#weight'] = 10;
      $form['submit_button']['#weight'] = 100; // 100, to place it below the captcha fields
      break;

    case 'exam_scores_university_form':
      $account = $form['exam_scores_university_form']['#user'];
      sales_funnel_widget_append_sales_funnel_widget($form, $form_state, $account);
      $form['submit_button']['#weight'] = 50;
      break;

    case 'rcpar_mods_cpa_materials_form':
      $account = null;
      $form['sales_funnel_widget']['#weight'] = -7;
      sales_funnel_widget_append_sales_funnel_widget($form, $form_state, $account);
      $form['submit']['#weight'] = 50;
      break;
    case 'student_of_the_month_node_form':
      $account = null;
      sales_funnel_widget_append_sales_funnel_widget($form, $form_state, $account);
      $form['sales_funnel_widget']['#attributes']['class'][] = 'edit-sales-funnel-widget-container';
      $form['sales_funnel_widget']['#weight'] = 6;
      break;

  }
}

/**
 * This function tries to determine the account id of the user that's the protagonist of the current
 * form (can be a form where the user is created, edited or a commerce form where the user is the client)
 * @param $form
 * @param $form_state
 * @return mixed
 *  UID of the user
 */
function sales_funnel_widget_get_uid($form, $form_state) {
  $uid = NULL;
  switch ($form['form_id']['#value']) {
    case 'user_profile_form':
    case 'user_register_form':
      $uid = $form_state['values']['uid'];
      break;
    case 'rcpar_asl_create_account_form':
      $uid = $form_state['uid'];
      break;
    case 'commerce_checkout_form_student_account_info':
    case 'commerce_checkout_form_complete':
      $uid = $form_state['account']->uid;
      break;
    case 'exam_scores_university_form':
      $uid = $form['user']['#default_value'];
      break;
  }
  return $uid;
}

/**
 * Submit function for sales funnel widget
 *  This is where we actually save the widget values
 * @param $form
 * @param $form_state
 */
function sales_funnel_user_fields_widget_submit($form, &$form_state) {
  $account_id = $form_state['storage']['sales_funnel_widget']['account_id'];
  if (!$account_id) {
    // We are working on a form that might be creating a user
    // let's see if we can find it's id
    $account_id = sales_funnel_widget_get_uid($form, $form_state);
  }
  // Note that the place where the data is going to be placed might be different if we placed our widget
  // inside a container element (see sales_funnel_widget_append_sales_funnel_widget doc block for an explanation
  // of the $parent_element var)
  if(isset($form_state['storage']['sales_funnel_widget']['parent_element'])) {
    $parent_element = $form_state['storage']['sales_funnel_widget']['parent_element'];

    // The form is rendered twice (because rebuild is set to true.) The second time
    // it's rendered the $value will be meaningless unless we retrieve them from
    // the  $form_state['storage']['sales_funnel_widget'][$parent_element]['saved_values'];
    if (empty($form_state['rebuild_info'])) {
      // This is the second time the form is being rendered.
      $values = $form_state['storage']['sales_funnel_widget'][$parent_element]['saved_values'];
    }
    else {
      // This is the first time the form is being rendered.
      $values = $form_state['values'][$parent_element]['sales_funnel_widget'];
    }
  }
  else {
    // There is no parent element.
    // Note above about form being rendered twice does NOT apply here as it is not
    // a multipane form.
    $values = $form_state['values']['sales_funnel_widget'];
  }

  if ($account_id) {
    // TODO: THIS MAY BE WHERE WE HAVE TO LOAD DATA FROM THE ORDER INSTEAD OF THE USER ACCOUNT FOR DEV-318
    $account = user_load($account_id);

    // Sometimes, the commerce checkout process sends something weird on the
    // $form_state['values']['sales_funnel_widget'] variable
    // fortunately, we can pick one of the date values to check if the values comes in the correct
    // format, and do nothing in case their did not
    // eg, for field_graduation_month_and_year we need ['und'][0]['value']
    // to have the following value:
    //  Array
    //  (
    //    [value] => 2012-05-03 00:00:00
    //    [show_todate] =>
    //    [timezone] => America/Argentina/Buenos_Aires
    //    [offset] => -10800
    //    [offset2] => -10800
    //    [value2] => 2012-05-03 00:00:00
    //  )

    if(isset($values['field_graduation_month_and_year']['und'][0]) && count($values['field_graduation_month_and_year']['und'][0])<6){
      // Invalid format, trying to continue will give us problems
      // we just return here
      return;
    }

    foreach (SALES_FUNNEL_WIDGET_FIELDS as $field_name) {
      if (isset($values[$field_name])) {
        $v = reset($values[$field_name]);
        $account->{$field_name}['und'] = $v;
      }
    }
    user_save($account);
    // Update the $form_state version of the user account with the one with the latest values
    $form_state['storage']['sales_funnel_widget']['account_id'] = $account->uid;
  }
  else {
    // Store field values for retrieval later
    if (isset($parent_element)) {
      $form_state['storage']['sales_funnel_widget'][$parent_element]['saved_values'] = $values;
    }
    else {
      $form_state['storage']['sales_funnel_widget']['saved_values'] = $values;
    }
  } 
  // If is a node we need to put the sales funnel in the values.
  if(isset($form['nid'])){
    $form_state['values'] = (array_merge( $form_state['values'], $form_state['values']['sales_funnel_widget']));
  }
}

/**
 * Validation function for sales funnel widget
 * This is validation handler is added to the form as a whole. Because of the different contexts in which the
 * widget may be submitted, it is more flexible to handle its validation in the container's element validator,
 * but there could still be a future use for this.
 */
function sales_funnel_user_fields_widget_validate($form, &$form_state) {
  // This is invoked on each ajax call, but we just want to run it on the actual submit
  if ($form_state['triggering_element']['#type'] == 'submit') {
  }
}

/**
 * Clears an error against one form element.
 *
 * @param $message
 *   The name of the form element.
 */
function sales_funnel_widget_form_unset_error($message) {
  // Get errors in the page
  $errors = &drupal_static('form_set_error', array());
  $removed_messages = array();
  foreach ($errors as $key => $error) {
    // If error exists in error list will be removed.
    if($error == $message){ 
      $removed_messages[] = $errors[$key];
      unset($errors[$key]);
    }
  }
  $_SESSION['messages']['error'] = array_diff($_SESSION['messages']['error'], $removed_messages);
  // prevent an empty error message div if there are no errors
  if (empty($_SESSION['messages']['error'])) {
    unset ($_SESSION['messages']['error']);
  }
}

/**
 * Ajax callback for the selector of the sales funnel widget
 * @param $form
 * @param $form_state
 * @return array a renderable field array for the college state list
 */
function sales_funnel_widget_college_ajax_callback(&$form, &$form_state) {
  // See sales_funnel_widget_append_sales_funnel_widget doc block for an explanation
  // of the $parent_element var
  if (isset($form_state['storage']['sales_funnel_widget']['parent_element'])) {
    $parent_element = $form_state['storage']['sales_funnel_widget']['parent_element'];
    return $form[$parent_element]['sales_funnel_widget']['field_college_sf_account'];
  }
  else {
    return $form['sales_funnel_widget']['field_college_sf_account'];
  }
}

/**
 * Implementation of hook_rcpar_checkout_profile_pane_defaults_alter, defined in rcpar_checkout_get_user_profile_pane_defaults
 * function.
 * We are using this function to add the sales funnel widget to the student info panel on the checkout process
 * @param $profile_pane_defaults
 * @param $order
 */
function sales_funnel_widget_rcpar_checkout_profile_pane_defaults_alter(&$profile_pane_defaults, $order){
  // When there's a uid on the order, we'll load the defaults from the user account
  if ($order->uid) {
    $user = user_load($order->uid);
    foreach(SALES_FUNNEL_WIDGET_FIELDS as $field){
      $data[$field] = $user->{$field};
    }
  }
  // If the user hasn't been created yet, we take the data we need from the order itself
  else {
    foreach(SALES_FUNNEL_WIDGET_FIELDS as $field){
      if ($field == 'field_when_do_you_plan_to_start' || $field == 'field_graduation_month_and_year'){
        $data[$field] = $order->data['commerce_user_profile_pane']['sales_funnel_widget'][$field];
        // Complete: If we just send as it is, rcpar_checkout_page function will fail
        // and throw a metadata exception when it does:
        // $wrapper->$field_name->set($value);
        $data[$field][LANGUAGE_NONE][0]['value'] = strtotime($data[$field][LANGUAGE_NONE][0]['value']);
      }
      else {
        $data[$field] = $order->data['commerce_user_profile_pane']['sales_funnel_widget'][$field];
      }
    }
  }

  // Current business logic dictates that this will always be true. If the user answers "yes" to the "did you
  // go to college" question, we'll try to populate values in all available widget fields
  if ($data['field_did_you_graduate_college'][LANGUAGE_NONE][0]['value']){
    $fields = SALES_FUNNEL_WIDGET_FIELDS;
  }
  // If the answer is "no", we omit fields related to a specific college.
  else {
    // TODO: This is still a buggy, it shows w/ anon users by default at first.
    $fields = array('field_did_you_graduate_college',
      'field_when_do_you_plan_to_start', 'field_graduation_month_and_year',
    );
  }

  // Based on the fields we've defined, inject values from our data source.
  foreach($fields as $field_name){
    if(empty($data[$field_name])) {
      // We've got no data here, move on.
      continue;
    }

    // Here, we try to grab the value based on any number of field types - target_id is an entity reference field,
    // tid is a term reference field, and most everything else (textfield, integer, select list, etc) is a value.
    if(isset($data[$field_name][LANGUAGE_NONE][0]['target_id'])) {
      $profile_pane_defaults[$field_name] = $data[$field_name][LANGUAGE_NONE][0]['target_id'];
    }
    elseif (isset($data[$field_name][LANGUAGE_NONE][0]['tid'])) {
      $profile_pane_defaults[$field_name] = $data[$field_name][LANGUAGE_NONE][0]['tid'];
    }
    else {
      $profile_pane_defaults[$field_name] = $data[$field_name][LANGUAGE_NONE][0]['value'];
    }
  }
}

/**
 * Implementation of hook_init().
 *
 * The user.pages.inc file from the user module is required for the reset password/profile edit form,
 * which is being output in a theme template file (user-profile-edit.tpl.php)
 *
 * Without including this file, the ajax callback for the college state list field will not work.
 *
 * There is a preprocess function in the theme's template.php file that builds the profile edit form:
 * function bootstrap_rcpar_preprocess_user_profile_form()
 *
 * I tried including this user module file in that preproces function, but the collete state list ajax wouldn't work.
 * It would only work if I added it with hook_init().
 *
 */
function sales_funnel_widget_init() {
  module_load_include('inc', 'user', 'user.pages');
}


/**
 * Helper function that process the commerce user profile pane INCLUDING
 * sales funnel widget input for the fields that are enabled to get captured
 * updating the user field values.
 *
 * NOTE: this is similar to _commerce_user_profile_pane_update_user_profile()
 *
 * @param object $order
 *   the order object that will be used to create/update the user (order owner)
 */
function _sales_funnel_widget_update_user_profile($order) {
  // Since we need more info from the order than _commerce_user_profile_pane_update_user_profile,
  // we get the the entire order as an arg.

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Set up the variables the original function used.
  $user = $order_wrapper->owner->value();
  $form_input = $order->data['commerce_user_profile_pane'];

  $ship_phone = $order_wrapper->commerce_customer_shipping->field_phone->value(array('sanitize' => TRUE));

  $my_user_input_fields = array();

  // Check the order for sales_form_widget fields
  if (array_key_exists('sales_funnel_widget', $form_input)) {
    foreach (SALES_FUNNEL_WIDGET_FIELDS AS $field_name) {
      if (
        array_key_exists($field_name, $form_input['sales_funnel_widget']) &&
        !empty($form_input['sales_funnel_widget'][$field_name])) {
        $my_user_input_fields[$field_name] = $form_input['sales_funnel_widget'][$field_name];
      }
    }
  }

  // update the account profile
  if (!empty($my_user_input_fields)) {
    $account = user_save($user, $my_user_input_fields);
  }

  // Add the shipping phone to the user account.
  if (!empty($ship_phone)) {
    $user_wrapper = entity_metadata_wrapper('user', $account);
    $user_wrapper->field_phone_number = $ship_phone;
  }

  // Add the firm name.
  if (!empty($order->field_employer['und'][0]['safe_value'])) {
    if (!$user_wrapper) {
      $user_wrapper = entity_metadata_wrapper('user', $account);
    }
    $user_wrapper->field_firm = $order->field_employer['und'][0]['safe_value'];
  }
  if ($user_wrapper) {
    $user_wrapper->save();
  }
}

/**
 * Helper function to check if college info is being edited in checkout after moving to shipping or payment.
 * We need to keep the newly selected college state or our swoopy ajax won't re-render the college name list
 * @param $form_state
 * @param $field_name
 * @param $value
 * @param string $parent_element
 * @return string
 */
function _sales_funnel_widget_check_selected_state($form_state, $field_name, $value, $parent_element = '') {
  // All default values remain the same but we want to have a looksie at to see if field_college_state_list has changed
  if ($field_name == 'field_college_state_list') {
    if (!empty($parent_element) && $value != $form_state['values'][$parent_element]['sales_funnel_widget'][$field_name][LANGUAGE_NONE][0]['value']){
      return $form_state['values'][$parent_element]['sales_funnel_widget'][$field_name][LANGUAGE_NONE][0]['value'];
    }
    elseif (empty($parent_element) && $value != $form_state['values']['sales_funnel_widget'][$field_name][LANGUAGE_NONE][0]['value']) {
      return $form_state['values']['sales_funnel_widget'][$field_name][LANGUAGE_NONE][0]['value'];
    }
  }
  return $value;
}

/**
 * Class SalesFunnelWidgetCollege
 * A little helper class to abstract the logic of which college is selected via the sales funnel widget
 */
class SalesFunnelWidgetCollege {
  private $collegeName = '';
  private $collegeStateList = '';
  private $collegeSfAccountId = 0;
  private $collegeStateFull = '';

  /**
   * SalesFunnelWidgetCollege constructor.
   * @param int $collegeSfAccountId
   * @param string $collegeStateList
   * - College state abbreviation.
   * @param string $collegeName
   * @param string $collegeStateFull
   * - College state full name.
   */
  function __construct($collegeSfAccountId, $collegeStateList, $collegeName, $collegeStateFull = null  ) {
    $this->collegeSfAccountId = $collegeSfAccountId;
    $this->collegeStateList = $collegeStateList;
    $this->collegeName = $collegeName;
    $this->collegeStateFull = $collegeStateFull;
  }

  /**
   * Construct an instance of the class from a form state array.
   *
   * @param array $form_state
   * - A Drupal form API form state array.
   * @return SalesFunnelWidgetCollege
   */
  public static function createFromFormVals($form_state) {
    $collegeStateList = $form_state['values']['sales_funnel_widget']['field_college_state_list'][LANGUAGE_NONE][0]['value'];
    $collegeName = $form_state['values']['sales_funnel_widget']['field_college_name'][LANGUAGE_NONE][0]['value'];
    $collegeSfAccountId = $form_state['values']['sales_funnel_widget']['field_college_sf_account'][LANGUAGE_NONE][0]['target_id'];

    if ($collegeStateList) {
      $collegeStateFull = $form_state['field']['#parents']['sales_funnel_widget']['#fields']['field_college_state_list'][LANGUAGE_NONE]['field']['settings']['allowed_values'][$collegeStateList];
    }
    else {
      $collegeStateFull = '';
    }
    return new self($collegeSfAccountId, $collegeStateList, $collegeName, $collegeStateFull);
  }

  /**
   * Returns the name of the user's college. This is derived from an sf_account assocatied with the user if possible,
   * otherwise falls back on field_college_name, which is manually typed by the user.
   * @return string
   */
  public function getName() {
    // If there is a loadable sf account entity, we'll get it and return its name
    if (!empty($this->collegeSfAccountId) && is_numeric($this->collegeSfAccountId)) {
      $sf_account = entity_load_single('sf_account', $this->collegeSfAccountId);
      if (is_object($sf_account)) {
        return $sf_account->title;
      }
    }

    // Otherwise we'll send the manually entered name of the college
    return $this->collegeName;
  }

  /**
   * Gets the entity ID of the sf_account, if it exists. Otherwise, 0 is returned.
   * @return int
   */
  public function getId() {
    return $this->collegeSfAccountId;
  }

  /**
   * Gets the college state
   * @param bool $fullName
   * - When TRUE, returns the full name of the college state. When FALSE, returns the abbreviation.
   * @return int
   */
  public function getState($fullName = FALSE) {
    if ($fullName) {
      return $this->collegeStateFull;
    }
    else {
      return $this->collegeStateList;
    }
  }

}