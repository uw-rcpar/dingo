<?php

/**
 * @file
 * Rules integration for sales_funnel_widget.rules module to allow to update
 * user profile just after the commerce_checkout_new_account Rule create the
 * user account.
 */

/**
 * Implements hook_rules_action_info().
 *
 * NOTE: this is very similar to commerce_user_profile_pane_rules_action_info()
 */
function sales_funnel_widget_rules_action_info() {
  $actions = array();

  $actions['sales_funnel_widget_user_update'] = array(
    'label'     => t('Update the User with input from User Profile Pane inc. Sales Funnel Widget fields.'),
    'parameter' => array(
      'commerce_order' => array(
        'type'  => 'commerce_order',
        'label' => t('Order in checkout'),
      ),
    ),
    'group'     => t('Sales Funnel Widget'),
    'base'      => 'sales_funnel_widget_user_update',
  );

  return $actions;
}

/**
 * Update User Profile Rule callback that handle the update of the user profile
 * data just after the commerce_checkout_new_account Rule had created the user
 * account.
 *
 * NOTE: Similar to commerce_user_profile_pane_user_update()
 */
function sales_funnel_widget_user_update($order) {
  if (isset($order->data['commerce_user_profile_pane'])) {
    _sales_funnel_widget_update_user_profile($order);
  }
}
