(function ($, Drupal, window, document, undefined) { // BEGIN - Closure
  Drupal.behaviors.sales_funnel_widget = {
    /**
     * In theory, all of this could be done via Drupal's #states API, but it seems to be a bit buggy
     * in general and I couldn't get it to work with this particular field for some reason anyway. -jd
     */
    attach: function (context, settings) {
      // Store a reference to self
      var sfw = this;

      // Get the sf account select field
      var $sfaccount = $('.field-name-field-college-sf-account select', context);

      if ($sfaccount.length) {
        $sfaccount.change(function (e) {
          sfw.sfAccountValueUpdate($(this));
        });

        sfw.sfAccountValueUpdate($sfaccount);
      }
    },

    sfAccountValueUpdate: function ($el) {
      // "Enter your college" should be visible when "other" is selected
      if ($el.val() == 'other') {
        $('.field-name-field-college-name').show();
        $('.field-name-field-college-name label').find('.form-required').remove();
        $('.field-name-field-college-name label').append('<span class="form-required" title="This field is required.">*</span>');
      }
      // Otherwise, it should be invisible
      else {
        $('.field-name-field-college-name').hide();
        $('.field-name-field-college-name label').find('.form-required').remove();
      }
    }

  };
})(jQuery, Drupal, this, this.document); // END - Closure

