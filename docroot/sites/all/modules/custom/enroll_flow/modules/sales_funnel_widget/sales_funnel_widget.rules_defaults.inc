<?php

/**
 * @file
 * Default rule configurations for Sales Funnel Widget module.
 * (To replaces those from Commerce User Profile Pane, which do not account for
 * the fields added in the Sales Funnel Widget.)
 */

/**
 * Implements hook_default_rules_configuration().
 *
 * NOTE: Very similar to commerce_user_profile_pane_default_rules_configuration
 */
function sales_funnel_widget_default_rules_configuration() {
  $rule = rules_reaction_rule();
  $rule->label = t('Update the User with input from User Profile Pane including Sales Funnel Widget.');
  $rule->active = TRUE;

  $rule->event('commerce_checkout_complete')
    ->action('sales_funnel_widget_user_update', array(
      'commerce_order:select' => 'commerce-order',
    ));

  // load the commerce_checkout_new_account to determine the weight due this
  // rule should execute after commerce_checkout_new_account had created the
  // user account.

  // Since commerce_user_profile_pane_user update adds 1 to the weight, we'll add 2.
  $new_account_rule = rules_config_load('commerce_checkout_new_account');
  $rule->weight = $new_account_rule->weight + 2;

  $rules['sales_funnel_widget_user_update'] = $rule;

  return $rules;
}
