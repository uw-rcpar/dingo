//behaviour that will rewrite the add to cart form to work with option buttons
(function ($) {
  Drupal.behaviors.enroll_flow_options = {
    attach: function (context, settings) {
      //we hide the dropdown, if we are going to use option buttons
      $('.option-display-form-product',context).closest('form').find('.bootstrap-select').hide();
      //trigger the ajax option for dropdown
      $('.option-display-form-product',context).live("click", function (data) {
        var product_id = $(this).val();
        if(product_id !== ""){
          console.log(product_id);
          $('.option-display-form-product').closest('form').find('.selectpicker').val(product_id).trigger('change');
          $(this).next().prepend('<i class="fa fa-refresh fa-spin margin-right"></i>');
        }
      });
    }

  };
})(jQuery);
