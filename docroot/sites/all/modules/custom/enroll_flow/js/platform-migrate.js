(function($) {
  $(document).ready(function() {

    // display the correct content on the page
    if ($.QueryString['step']=='confirmation') {
      $('#confirmation-content').show();
      $('.pm-progress-bar-wrapper div').addClass('active');
    }
    else {
      $('#overview-content').show();
    }
    // Show different progress bar for data migration workflow
    if ($.QueryString['data']=='1') {
      $('#pm-progress-bar').hide();
      $('#pm-progress-bar-data').show();
    }
    if ($.QueryString['data']=='1' && $.QueryString['step']=='in-progress') {
      $('#pm-progress-bar-data .pm-progress-overview').addClass('active');
      $('#pm-progress-bar-data .pm-progress-agreement').addClass('active');
      $('#pm-progress-bar-data .pm-progress-progress').addClass('active');
      $('#overview-content').hide();
      $('#in-progress-content').show();

      // Check user's migration status every 30 seconds
      function checkMigrationStatus() {
        $.get('/api/migrate/status?jwt=' + RCPAR.jwt.getJwt(), function(data,status) {
          if (data.status) {
            var urlParts = window.location.href.split('/');
            var userId = urlParts[4];
            if (data.error) {
              $(location).attr('href','/user/' + userId + '/platform-migrate/error?data=1');
            }
            else {
              $(location).attr('href','/user/' + userId + '/platform-migrate?data=1&step=confirmation');
            }
          }
        });
      }
      window.setInterval(checkMigrationStatus, 30000);
    }

    $('.yes-button').click(function () {
      // redirect to dashboard if this is the "cancel-sure" button
      if ($(this).hasClass('cancel-sure')) {
        $(location).attr('href','/dashboard');
      }
      // otherwise show the next pane
      else {
        $('.toggle-wrapper').hide();
        $('#' + $(this).attr('id') + '-content').show();
        $('.pm-progress-' + $(this).attr('id')).addClass('active');
        $('#navbar-pm .logo').removeClass('step-one');
        // scroll user to top of content
        $('.platform-migration').animate({scrollTop: $('#' + $(this).attr('id') + '-content').offset().top}, 'fast');
      }
    });

    // Agreement pane checkbox
    $('#agreement-checkbox').prop('checked', false);

    $('#agreement-checkbox').click(function () {
      $('#confirmation').toggleClass('disabled');
    });

    // Multiple "I agree" checkboxes on phase 2
    $(".agree-box").change(function(){
        if ($('.agree-box:checked').length == $('.agree-box').length) {
            $('#confirmation').toggleClass('disabled', false);
        }
        else {
            $('#confirmation').toggleClass('disabled', true);
        }
    });

    // Clicking on logo
    $('#navbar-pm .logo').click(function () {
      // if in step one, just go back a page
      if ($(this).hasClass('step-one')) {
        history.back();
      }
      // otherwise show cancellation modal
      else {
        $('#cancellation-warning').modal();
      }
    });

    // Update the content height
    $(window).on('ready resize load', function () {
      $('.platform-migration').css('height', $(window).height() + 'px');
    });



    // Show the upgrade modal
    if ($('#upgrade-modal').length) {
      // Show the modal if the modal hasn't been recorded as seen
      var modals_to_check = ['upgrade_modal_block'];
      if (!RCPAR.modals.userHasSeenModals(modals_to_check)) {
        // only show it on the first page of the ipq quiz setup
        if ($('body').hasClass('page-study') || $('#select-all-none').length) {
          $('#upgrade-modal').modal();
        }
      }
      else {
        // show modal again after a week
        var modalDateSeen = RCPAR.modals.getDateModalWasSeen('upgrade_modal_block');

        //Get today's date using the JavaScript Date object.
        var ourDate = new Date();

        //Change it so that it is 7 days in the past.
        var pastDate = ourDate.getDate() - 7;
        ourDate.setDate(pastDate);
        // change to unix timestamp
        var ourDateUnixFormat = ourDate.getTime() / 1000;

        if (ourDateUnixFormat >  modalDateSeen) {
          // show modal
          $('#upgrade-modal').modal();
        }

      }

      // After the upgrade feature modal is hidden
      $('.remind-in-a-week').on('click', function (e) {
        var jwt = RCPAR.jwt.getJwt();
        RCPAR.modals.setModalData('upgrade_modal_block', jwt);

        // update modal timestamp
        // current date in unix timestamp
        var currentDate = new Date();
        var currentDateUnixFormat = currentDate.getTime() / 1000;
        RCPAR.modals.updateModalTimestamp('upgrade_modal_block', jwt, currentDateUnixFormat);
      });

      $('#modal-more').click(function () {
        // Send user to migration workflow page
        $(document).ajaxComplete(function( event, request, settings ) {
          $(location).attr('href','/user/' + Drupal.settings.enroll_flow.user_id + '/platform-migrate');
        });
      });
    }

  });

})(jQuery);