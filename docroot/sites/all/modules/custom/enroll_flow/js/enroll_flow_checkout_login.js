(function ($) {
  Drupal.behaviors.EnrollFlowCheckout = {
    /**
     * Code added via enroll_flow_reg_pane_validate to swich user to the
     * login/user registration block depending on which one rec'd the error
     *
     * Code to be loaded run on page and ajax load
     * $(function () is a shorthand for $(document).ready().
     * @see {@link http://api.jquery.com/ready} for further details.
     *
     * @returns {undefined}
     */
    attach: function (context, settings) {
      if (Drupal.settings.enroll_flow_checkout_login.do_login_form_init) {
        // This setting is set in enroll_flow_form_alter for form_id
        // commerce_checkout_form_login. It allows us to change the first
        // default first login form for an anon user to the register pane.

        // The first time the page is drawn, hide the form and the login link
        // block.
        $('#commerce-checkout-form-login').hide();

        // Hide block 66 with register link
        $('#block-block-66').hide();

        // See on window load below for how form & block are shown.
      }

      // Remove the top error code that says "Please correct the errors on this
      // form".
      $('.messages.error:not(".alert")', context).remove();

      // add a click function to the login/user reg "CONTINUE TO NEXT STEP" button
      $('#edit-continue', context).on('click', function () {
        // empty out hidden inputs so they won't attempt to be validated
        $('#user-login-form:hidden input, #edit-account:hidden input').val('');
        // save the id of the visible block
        var visibleBlockId = $('#user-login-form:visible, #edit-account:visible').attr('id');
        $('input[name="enroll-flow-visible-pane"]').val(visibleBlockId);
      });
    }
  }

  // this needs to run after the window has loaded
  $(window).load(function () {
    // check if there are validation errors in login/user registration blocks that are hidden
    if ($('.messages.error:hidden').length) {
      var visableCheckoutBlock = $('input[name="enroll-flow-visible-pane"]').val()
      // switch to the user login/user registration mode depending on which has the hidden error message
      // FYI these click listeners are set in /theme/custom.js
      if (visableCheckoutBlock == 'user-login-form') {
        $('#block-block-116 .register-link-wrapper a:visible').click();
      }
      else if (visableCheckoutBlock == 'edit-account') {
        $('#block-block-66 .register-link-wrapper a:visible').click();
      }
    }
    if (Drupal.settings.enroll_flow_checkout_login.do_login_form_init) {
      // This setting is set in enroll_flow_form_alter for form_id
      // commerce_checkout_form_login. It allows us to change the first default
      // first login form for an anon user to the register pane.
      $("a.checkout-register-link").click();
      // block 116 is the block with login link
      // block 66 is the block with register link
      $('#block-block-66').hide();
      $('#block-block-116').show();
      // Show the form.
      $('#commerce-checkout-form-login').show();

      Drupal.settings.enroll_flow_checkout_login.do_login_form_init = false;
    }
  });
}(jQuery))
