(function($) {
    if (Drupal.ajax !== undefined) {
        Drupal.ajax.prototype.error = function(response, uri) {
            if (response.status != 0) {
                alert(Drupal.ajaxError(response, uri));
            }
            // Remove the progress element.
            if (this.progress.element) {
                $(this.progress.element).remove();
            }
            if (this.progress.object) {
                this.progress.object.stopMonitoring();
            }
            // Undo hide.
            $(this.wrapper).show();
            // Re-enable the element.
            $(this.element).removeClass('progress-disabled').removeAttr('disabled');
            // Reattach behaviors, if they were detached in beforeSerialize().
            if (this.form) {
                var settings = response.settings || this.settings || Drupal.settings;
                Drupal.attachBehaviors(this.form, settings);
            }
        };
    }


    Drupal.behaviors.enroll_flow = {
        attach: function(context, settings) {
          /**
           * Change function for required shipping address elements recalculates if
           * state select doesn't have no shipping class and all required shipping
           * address elements are not empty
           *
           * @returns {undefined}
           */

          // when pseudo calculate shipping button is clicked
          $('.pseudo-shipping-button').click(function (e) {

            //add jquery throbber to button
            $(this).html('Calculate Shipping <div class="ajax-progress ajax-progress-throbber"><div class="glyphicon glyphicon-refresh glyphicon-spin"></div></div>');

            // calculate shipping
            $.fn.commerceCheckShippingRecalculation();

            // change button text and remove throbber
            var change_button_text = function () {
              $('.pseudo-shipping-button').html('Recalculate Shipping');
            };
            setTimeout(change_button_text, 2000);

          });

          // if button has been clicked and no shipping options are available,
          // we need to show the shipping message
          if ($('.pseudo-shipping-button').hasClass('no-shipping-options')) {
            $('.pseudo-shipping-button').html('Recalculate Shipping');

            $('.shipping-message').show();
          }

          function enrollFlowShippingAddressChangeFunc() {
            // assume that we will do the recalc
            var doRecalc = true;
            // don't do recalc if the state select has no-shipping class
            if ($('#edit-customer-profile-shipping-commerce-customer-address-und-0-administrative-area').hasClass("no-shipping")) {
              doRecalc = false;
            }
            else {
              // this selects all the required shipping fields needed for recalc
              $.each($('.required[id^="edit-customer-profile-shipping"]'), function () {
                if ($.trim($(this).val()) == '') {
                  doRecalc = false;
                  return false;
                }
              });
              if (doRecalc) {
                // Since shipping address is still collected even when there are no shippable products, it's possible
                // for this code to be triggered even when it is not possible to do a shipping calculation. To guard
                // against this, we catch and throw away any exception.
                try {
                  $.fn.commerceCheckShippingRecalculation();
                }
                catch (e) {
                  // Shipping calculation not available - probably an order without shippable products.
                }
              }
            }
          }


         // see if required shipping inputs are in our context
          if ($('.required[id^="edit-customer-profile-shipping"]', context).length) {
            // this selects all the required shipping fields needed for recalc
            // using .once so that add'l change listeners aren't applied each time the pane is redrawn (i.e. due to an error)
            $('.required[id^="edit-customer-profile-shipping"]').once('enrollFlowShippingAddressChangeFuncApply', function () {
              // add a change function to them
              $(this).change(function () {
                enrollFlowShippingAddressChangeFunc();
              });
            });
          }

          // add an additional change listener for the shipping recalc button if it exists
          if ($('.country', context).length) {
            $(".country").change(function () {
              $(this).next().html('loading...');
              // no need to add a recalc function as it was added above
            });
          }
            // Deal with user clicking
            $("#commerce-checkout-form-checkout input[type=checkbox]").not("#edit-terms").on('change', function(e) {
                if ($(this).attr('checked')) {
                    $(".billing_data_display").show();
                } else {
                    $(".billing_data_display").hide();
                }
            });

            // On the sales funnel widget, if the student's  college is not in our list we need to keep
            // the user's initial input and append it with an asterix "*" so the user's
            // unmatched college will pass validation.
            var college_current_value;
            $('[id*="sales-funnel-widget-field-college-name-und-0-value"]').on("change paste keyup", function() {
                college_current_value = $(this).val();
            });
            $('[id*="sales-funnel-widget-field-college-name"]', context).bind('autocompleteSelect', function(event, node) {
                // Get the autocomplete key and label values selected
                var key = $(node).data('autocompleteValue');
                var label = $(node).text();
                // If not listed, keep the current value and append with asterix
                if (key == 'MY SCHOOL IS NOT LISTED') {
                    // Set the value of this field.
                    $('[id*="sales-funnel-widget-field-college-name-und-0-value"]').val(college_current_value + '*');
                }
            });

            // We want to trigger Drupal's autocomplete function on focus, so we simulate a keyup event
            $('[id*="sales-funnel-widget-field-college-name-und-0-value"]').focus(function() {
                var e = $.Event("keyup");
                e.which = 25;
                $(this).trigger(e);
            });
        }

    };

    $(document).ready(function() {

        // Intercept the delete line item button click event in the shopping cart to provide a confirm dialog before
        // the product is removed.
        $('.delete-line-item').click(function (e, force) {
            // When this custom paramater is present, the user is confirming - we want to submit the form and not
            // intercept the event
            if(force == 'force') {
                return true;
            }

            // Grab the parent row
            var $row = $(this).closest('tr');

            // If we're already showing the confirm dialog, don't show it again
            if($row.find('.custom-remove-confirm').length > 0) {
                e.preventDefault();
                return;
            }

            // Grab the title cell, total cell, and title text
            var $titleAndTotalTDs = $row.find('.views-field-line-item-title, .views-field-commerce-total');
            var title = $row.find('.views-field-line-item-title').text().replace('View package contents','');

            // Hide the current title and price cells
            $titleAndTotalTDs.hide();

            // Build a new cell spanning 2 columns, and the yes/no links
            $row.append('<td colspan="2" class="views-field custom-remove-confirm"></td>');
            $row.find('.custom-remove-confirm').html('<div class="confirm-remove"><span class="remove-text">REMOVE &quot;' + title.trim() + '&quot; from your cart?</span>' +
              '<span class="yes-no"><a href="javascript:;" class="remove-yes">Yes</a> <a href="javascript:;" class="remove-no">No</a></span></div>');

            // Bind to the click event of the 'no' button - Restore the cells that we hid and remove our new markup
            $row.find('.remove-no').click(function (e) {
                $titleAndTotalTDs.show();
                $row.find('td.custom-remove-confirm').remove();
            });

            // Bind to the click event of the 'yes' button
            $row.find('.remove-yes').click(function (e) {
                // Trigger the click of the delete button with a custom parameter that will prevent us from re-intercepting
                // the event
                $row.find('.delete-line-item').trigger('click',['force']);
            });

            e.preventDefault();
        });
       
        // Add "open" class if we are on the last two pages of the checkout.
        if ($("body").hasClass("page-checkout-review") || $("body").hasClass("page-checkout-complete")) {
            $('#block-views-commerce-cart-summary-block-1 h2').addClass('open');
            $('#block-views-commerce-cart-summary-block-1 .view-content').show();
            $('#block-views-commerce-cart-summary-block-1 .view-header').hide();
        }

        // Need to add loop on fields that should be hidden from drupal settings.
        if (Drupal.settings.enroll_flow) {
            items = Drupal.settings.enroll_flow.items;
            // why is this wrapped in $( ... ) ?
            $($('td.views-field-edit-quantity').find("div").children('input').each(function(index, value) {
              // Default hide all quantity fields
              // only hide if it matches our list from settings should we show the qty field
              if (items.length >= 1 || items != '') {
                jQuery.each(items, function(index2, item) {
                  if (!$('#edit-edit-quantity-' + index).closest('.views-field-edit-quantity').hasClass(item)) {
                    $('#edit-edit-quantity-' + index).parent().parent().find('div').html('<div class="place-holder-qty">' + $('#edit-edit-quantity-' + index).val() + '</div>');
                  }
                });
              } else {
                $('#edit-edit-quantity-' + index).parent().parent().find('div').html('<div class="place-holder-qty">' + $('#edit-edit-quantity-' + index).val() + '</div>');
              }

            }));
        }

    });


    //
    // Run after Document is loaded.
    //
    $(window).load(function() {

        // Display billing data if checkbox is checked
        if ($('#edit-customer-profile-billing-commerce-customer-profile-copy').attr('checked')) {
            $(".billing_data_display").show();
        }
        
        // Direct Bill -- set Order total to $0
        try {
          if(Drupal.settings.enroll_flow.hide_prices_direct_bill == true){
              $('.view-commerce-cart-summary').find('.order-total').html("$0.00");
          }
        }
        catch(e) {
          // Swallow errors in case Drupal.settings.enroll_flow does not exist.
        }
        
        // Load this after all ajax has completed
        $(document).ajaxComplete(function(event, xhr, settings) {
          // Rebuild form UI since jquery may hav acted on it.
          try {
            $('.selectpicker').selectpicker('refresh');
          }
          catch(e) {
            // Catch errors - on admin pages, selectpicker may not be available (as is the case when editing a user
            // account as an admin. Catching this error keeps things tidy.
          }

          // quick fix for multiple throbbers showing in extra selects after one is added EN-359
          // remove these classes that would cause the throbber
          $('#block-enroll-flow-enroll-flow-upsell-extras  .input-group-btn.progress-disabled').removeClass('input-group-btn progress-disabled');
          // fix the spacing hangover left from the fix
          $('#block-enroll-flow-enroll-flow-upsell-extras  .input-group-addon').css('padding', '0');
          $('#block-enroll-flow-enroll-flow-upsell-extras  .btn-group.bootstrap-select.form-control.form-select.ajax-processed').css('width', '334px');

          // similar quick fix for weird formatting after college state select (found in DIN-718)
          $('.form-item-field-college-state-list-und  .input-group-addon').css({'padding': '0', "border": '0'});


        });

    });

})(jQuery);
