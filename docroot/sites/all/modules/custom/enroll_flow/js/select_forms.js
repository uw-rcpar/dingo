(function($) {
    $(document).ready(function() {
        listFilter($('.enroll-flow-fieldset-to-search'), $('.enroll-flow-search-box'));
        listFilter($('.enroll-flow-fieldset-to-search-mapped'), $('.enroll-flow-search-box-mapped'));
        listFilter($('.enroll-flow-fieldset-to-search-exam-ext'), $('.enroll-flow-search-box-exam-ext'));
        listFilter($('.enroll-flow-fieldset-to-search-cram-item'), $('.enroll-flow-search-box-cram-item'));
        listFilter($('.enroll-flow-fieldset-to-search-cram-dis'), $('.enroll-flow-search-box-cram-dis'));
        listFilter($('.enroll-flow-fieldset-to-search-offline'), $('.enroll-flow-search-box-offline'));
        listFilter($('.enroll-flow-fieldset-to-search-review-discount'), $('.enroll-flow-search-box-review-discount'));
        listFilter($('.enroll-flow-fieldset-to-search-offline-cram-item'), $('.enroll-flow-search-box-offline-cram-item'));
        listFilter($('.enroll-flow-fieldset-to-search-bundles'), $('.enroll-flow-search-box-bunldes'));
        listFilterOrderAdmin($('tbody tr'), $('.search-partner-payment'));
        
        $('.edit-order-manager-date').click(function(e) {
          e.preventDefault();
          $(this).parent().find('.close-order-manager-date').show();
          $(this).closest('td.views-field-php').find('.container-inline-date').show();
          $(this).closest('td.views-field-php').find('.edit-order-created-date-update-button').show();
          $(this).closest('td.views-field-php').find('.date-preview-order-update').hide();
          $(this).hide();
        });   
        
        $('.close-order-manager-date').click(function(e) {
          e.preventDefault();
          $(this).closest('td.views-field-php').find('.edit-order-manager-date').show();
          $(this).closest('td.views-field-php').find('.container-inline-date').hide();
          $(this).closest('td.views-field-php').find('.edit-order-created-date-update-button').hide();
          $(this).closest('td.views-field-php').find('.date-preview-order-update').show();
          $(this).hide();
        });        
        
        $('.fieldset-custom-order-manager').click(function(e) {
          e.preventDefault();
          if($(this).closest('fieldset').hasClass('collapsed')){
            $(this).closest('fieldset').removeClass('collapsed');
          }else{
            $(this).closest('fieldset').addClass('collapsed');
          }
          
        });
        
        //FIX FOR CONTACT US PAGE TO WORK WITH SELECTPICKER
        if ($(".section-contact-us").length) {
          $('select.form-select').removeAttr("disabled");
        }
        
        if ($(".page-node-edit.node-type-partners .field-add-more-submit").length) {

            $(document).ajaxComplete(function() {

              //  $('.selectpicker').selectpicker();

            });
        }
    });



    function listFilter(list, input) {
        var $lbs = list.find('.form-type-select label');

        function filter() {
            
            var regex = new RegExp('\\b' + this.value.toLowerCase());
            var $els = $lbs.filter(function() {                
                return regex.test($(this).text().toLowerCase());
            });

            $lbs.not($els).hide().parent().hide();
            $els.show().parent().show();
        };

        input.keyup(filter).change(filter)
    }

    function listFilterOrderAdmin(list, input) {
        var $lbs = list.find('.views-field-message').closest('fieldset');

        function filter() {
            
            var regex = new RegExp('\\b' + this.value.toLowerCase());
            var $els = $lbs.filter(function() {                
                return regex.test($(this).text().toLowerCase());
            });

            $lbs.not($els).hide().parent().hide();
            $els.show().parent().show();
        };

        input.keyup(filter).change(filter)
    }

})(jQuery);