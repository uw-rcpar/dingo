<?php
/**
 * @file
 * Rules integration for line items.
 *
 * @addtogroup rules
 * @{
 */

/**
 * implementation of hook_rules_condition_info()
 */
function enroll_flow_rules_condition_info() {
  $conditions = array();
  $conditions['enroll_flow_city_match'] = array(
    'label' => t('Match city for taxes.'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order containing the profile reference with the address in question.'),
      ),
      'address_field' => array(
        'type' => 'text',
        'label' => t('Address'),
        'options list' => 'enroll_flow_address_field_options_list',
        'description' => t('The address associated with this order whose component you want to compare.'),
        'restriction' => 'input',
      ),
      'address_component' => array(
        'type' => 'text',
        'label' => t('Address component'),
        'options list' => 'enroll_flow_address_component_options_list',
        'description' => t('The actual address component you want to compare. Common names of address components are given in parentheses.'),
        'restriction' => 'input',
      ),
      'value' => array(
        'type' => 'text',
        'label' => t('Value'),
        'description' => t('Rate match from database.'),
      ),
    ),
    'group' => t('Enroll Flow'),
    'callbacks' => array(
      'execute' => 'enroll_flow_condition_city_match',
    ),
  );
  
  return $conditions;
}

/**
 * Options list callback: address fields for the address comparison condition.
 * Copied from /modules/contrib/commerce/modules/commerce_order.rules.inc
 */
function enroll_flow_address_field_options_list() {
  $options = array();

  // Retrieve a list of all address fields on customer profile bundles.
  $address_fields = commerce_info_fields('addressfield', 'commerce_customer_profile');

  // Loop over every customer profile reference field on orders.
  foreach (commerce_info_fields('commerce_customer_profile_reference', 'commerce_order') as $field_name => $field) {
    // Retrieve the type of customer profile referenced by this field.
    $type = $field['settings']['profile_type'];

    // Loop over every address field looking for any attached to this bundle.
    foreach ($address_fields as $address_field_name => $address_field) {
      if (in_array($type, $address_field['bundles']['commerce_customer_profile'])) {
        // Add it to the options list.
        $instance = field_info_instance('commerce_customer_profile', 'commerce_customer_address', $type);
        $translated_instance = commerce_i18n_object('field_instance', $instance);

        $options[commerce_customer_profile_type_get_name($type)][$field_name . '|' . $address_field_name] = check_plain($translated_instance['label']);
      }
    }
  }

  if (empty($options)) {
    drupal_set_message(t('No order addresses could be found for comparison.'), 'error');
  }

  return $options;
}

/**
 * Options list callback: components for the address comparison condition.
 * Copied from /modules/contrib/commerce/modules/commerce_order.rules.inc
 */
function enroll_flow_address_component_options_list() {
  return array(
    'country' => t('Country'),
    'name_line' => t('Full name'),
    'first_name' => t('First name'),
    'last_name' => t('Last name'),
    'organisation_name' => t('Company name'),
    'thoroughfare' => t('Thoroughfare (Street address)'),
    'premise' => t('Premise (Building)'),
    'sub_premise' => t('Sub-premise (Suite)'),
    'locality' => t('Locality (City)'),
    'dependent_locality' => t('Dependent locality (Town)'),
    'administrative_area' => t('Administrative area (State / Province)'),
    'sub_administrative_area' => t('Sub-administrative area (District)'),
    'postal_code' => t('Postal code'),
  );
}


/**
 * Condition city_match
 */
function enroll_flow_condition_city_match($order, $address_field, $component, $rate) {
  list($field_name, $address_field_name) = explode('|', $address_field);

  if (!empty($order)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    // And if we can actually find the requested address data...
    if (!empty($wrapper->{$field_name}) && !empty($wrapper->{$field_name}->{$address_field_name})) {
      $address = $wrapper->{$field_name}->{$address_field_name}->value();

      //Only look at California cities.
      $state = $wrapper->{$field_name}->{$address_field_name}->administrative_area->value();
      if ($state != "CA") {
        return false;
      }

      if ($rate_match = enroll_flow_get_city_rate($address[$component], $state, $rate)) {
        //echo "$rate_match = $rate - " . $address[$component] . ", $state <br>";
        return true;
      }
    }
  }

  return false;
}