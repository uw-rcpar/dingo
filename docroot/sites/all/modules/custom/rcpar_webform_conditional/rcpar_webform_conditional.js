/**
 * JavaScript functions for front-end display of webform conditional components
 */
(function ($) {
	
Drupal.behaviors.rcpar_webform_conditional = Drupal.behaviors.rcpar_webform_conditional || {};
Drupal.behaviors.rcpar_webform_conditional.attach = function() {
	// create quasi static var to save perfomance
	Drupal.rcpar_webform_conditional.wrappers = new Object();
	Drupal.rcpar_webform_conditional.components = new Object();
  $.each(Drupal.settings, function(key, info) {
    if(key.substring(0, 20) == 'rcpar_webform_conditional_') {
      $.each(info.fields, function(triggerField_key, triggerField_info) {
        
        var formItemWrapper = Drupal.rcpar_webform_conditional.getWrapper(triggerField_info);
        if(formItemWrapper.length > 0){
            // Add onclick handler to Parent field
            Drupal.rcpar_webform_conditional.addOnChange (triggerField_key, triggerField_info, key);
        }
        });
      // after all added - trigger initial
  
      $.each(info.fields, function(triggerField_key, triggerField_info) {
        var formItemWrapper = Drupal.rcpar_webform_conditional.getWrapper(triggerField_info);
          if(formItemWrapper.length > 0){
            var field_name = Drupal.rcpar_webform_conditional.escapeId(triggerField_key);
            var components = Drupal.rcpar_webform_conditional.getComponentsByName(field_name, key);
            if(components.attr('type')=='radio' || components.attr('type')=='checkbox'){
              $(components[0]).triggerHandler('click');
            }else{
              components.triggerHandler('change');
            }
          }
        });
      }
    });
	return;
};
Drupal.rcpar_webform_conditional = Drupal.rcpar_webform_conditional || {};
// create quasi static var to save perfomance
Drupal.rcpar_webform_conditional.getWrapper = function(fieldInfo){
	if(Drupal.rcpar_webform_conditional.wrappers[fieldInfo['css_id']]){
		return Drupal.rcpar_webform_conditional.wrappers[fieldInfo['css_id']];
	}
	return Drupal.rcpar_webform_conditional.wrappers[fieldInfo['css_id']] = $("#" + fieldInfo['css_id']);
};
Drupal.rcpar_webform_conditional.addOnChange = function(triggerField_key, triggerField_info, key) {
	var monitor_field_name = Drupal.rcpar_webform_conditional.escapeId(triggerField_key);
	var changeFunction = function() {
		Drupal.rcpar_webform_conditional.setVisibility(triggerField_key,triggerField_info,key);
	};
	$.each(triggerField_info['dependent_fields'],function(dependent_field_key,dependent_field_info){
		var formItemWrapper = Drupal.rcpar_webform_conditional.getWrapper(dependent_field_info);
	    if(formItemWrapper.length > 0){
	            formItemWrapper.css("display", "none");
	    }

	});
	var components = Drupal.rcpar_webform_conditional.getComponentsByName(monitor_field_name, key);
	if(components.attr('type')=='radio' || components.attr('type')=='checkbox'){
		components.click(changeFunction)
	}else{
		components.change(changeFunction);
	}
	
};
Drupal.rcpar_webform_conditional.setVisibility = function(triggerField_key,triggerField_info,key,monitorField,monitorInfo){
	var monitor_field_name = Drupal.rcpar_webform_conditional.escapeId(triggerField_key);
	var currentValues = Drupal.rcpar_webform_conditional.getFieldValue(monitor_field_name); 
	var monitor_visible = true;
	if(monitorField !== undefined){
		monitor_visible = Drupal.rcpar_webform_conditional.getWrapper(monitorInfo).data('wfc_visible');
	}
	$.each(triggerField_info['dependent_fields'],function(dependentField,dependentInfo){
		if(((dependentInfo['operator'] == "=" && !Drupal.rcpar_webform_conditional.Matches(currentValues,dependentInfo['monitor_field_value']))
			|| (dependentInfo['operator'] == "!=" && Drupal.rcpar_webform_conditional.Matches(currentValues,dependentInfo['monitor_field_value']))) 
			|| !monitor_visible){
				// show the hidden div
				// have to set wfc_visible so that you check the visibility of this
				// immediately
			 Drupal.rcpar_webform_conditional.getWrapper(dependentInfo).hide().data('wfc_visible',false);
		}else {
				// otherwise, hide it
			Drupal.rcpar_webform_conditional.getWrapper(dependentInfo).show().data('wfc_visible',true);
				// and clear data (using different selector: want the
				// textarea to be selected, not the parent div)
		}
		Drupal.rcpar_webform_conditional.TriggerDependents(dependentField,dependentInfo,key);
	});
};
Drupal.rcpar_webform_conditional.getComponentsByName = function (field_name, key){
	// check to save jquery calls
	if(Drupal.rcpar_webform_conditional.components[field_name]){
		return Drupal.rcpar_webform_conditional.components[field_name];
	}
	// don't overwrite original name to store for caching
	var css_field_name = "[" + field_name + "]";
	settings = Drupal.settings[key];
	var nid = settings.nid;
	if(nid instanceof Array){
		nid = settings.nid[0];
	}
	return Drupal.rcpar_webform_conditional.components[field_name] = $("#webform-client-form-" + nid + " *[name*='"+css_field_name+"']");
};
Drupal.rcpar_webform_conditional.TriggerDependents = function(monitorField,monitorInfo, key){
  settings = Drupal.settings[key];
	$.each(settings.fields, function(triggerField_key, triggerField_info) {
		if(triggerField_key == monitorField){
			Drupal.rcpar_webform_conditional.setVisibility(triggerField_key, triggerField_info,key,monitorField,monitorInfo);
		};
	});
};
Drupal.rcpar_webform_conditional.getFieldValue = function(field_name){
	field_name = "[" + field_name + "]";
	var selected = [];
	var vals = [];
	if($('form input[name*="'+field_name+'"]:checked').length >= 1){
		selected =  $('form input[name*="'+field_name+'"]:checked');
	}else if($('form select[name*="'+field_name+'"] option:selected').length >= 1){
		selected = $('form select[name*="'+field_name+'"] option:selected');
	}
	if(selected.length == 0){
		return vals;
	}
	selected.each(function(i){
	     vals[i] = $(this).val();
	    });
	return vals;
};
Drupal.rcpar_webform_conditional.Matches = function(currentValues,triggerValues){
	var found = false;
	$.each(triggerValues, function(index, value) { 
		  if(jQuery.inArray(value,currentValues)> -1){
			  found = true;
			  return false;
		  }
		});
	return found;
};
// Drupal.rcpar_webform_conditional.escapeId
Drupal.rcpar_webform_conditional.escapeId = function(myid) {
	if (typeof myid == 'undefined') {
		return;
	}
	   return  myid.replace(/(:|\.)/g,'\\$1');
};
})(jQuery);