/**
 * @file
 * Handles rcpar_solr button & select functionality.
 *
 * NOTE: actionSelectChangeFunc() requires that
 *   sites/all/themes/bootstrap_rcpar/js/QueryString.js is added to the page.
 */
(function ($) {
  Drupal.behaviors.RcparSolrSelect = {
    attach: function (context, settings) {

      // Submit search functions with existing text on part button press on
      // homework-help-center page.
      $('ul.hhc-category-links li a', context).on('click', function () {
        var prefix = Drupal.settings.basePath + 'homework-help-center/search/';
        var suffixStart = '?f[0]=im_field_course_section%3A';
        /**
         * Drupal.settings.RcparSolrSelect.partTids is set in
         * rcpar_solr.module->rcpar_solr_form_alter to keep it current. it
         * looks like:
         *  {
         *    'AUD': 1452,
         *    'BEC': 1455,
         *    'FAR': 1453,
         *    'REG': 1454
         *  };
         */
        var which = $(this).html().substr(0, 3);
        var suffixEnd = Drupal.settings.RcparSolrSelect.partTids[which];
        top.location.href = prefix + encodeURIComponent($('input[name=search]').val()) + suffixStart + suffixEnd;
        return false;
      });


      // Submit search function with existing text on change of select.
      $('.facetapi-select', context).change(function () {
        var id = $(this).attr('id');
        var elem = document.getElementById(id);
        var lpos = Drupal.settings.basePath + 'homework-help-center/search/';
        var locBase = top.location.pathname.substr(0, lpos);
        var searchTerm = encodeURIComponent($('input[name=keys]').val());
        var partValue = $('#edit-part-select').val();
        // Build a URL string that varies based on moderator parameters
        // and will retain correct parameters if "Part" select is reset.
        var urlString = locBase + searchTerm;
        // Need to add the moderator parameter to the URL if it exists.
        // Sometimes the querystring is encoded and sometimes it isn't.
        if (typeof $.QueryString['f%5B100%5D'] !== 'undefined') {
          var modParam = '&f[100]=' + $.QueryString['f%5B100%5D'];
        }
        else {
          var modParam = '&f[100]=' + $.QueryString['f[100]'];
        }
        // If the select being changed is the Part select
        // and there is a search term, the modParam needs a prefix
        // and the urlString needs to be changed.
        if ($('#edit-part-select').val() == lpos + searchTerm) {
          var urlString = '?' + modParam;
        }
        // Otherwise just add the moderator parameter to the URL.
        else {
          var urlString = urlString + elem.options[elem.selectedIndex].value + modParam;
        }
        // Moderator redirect.
        if (typeof $.QueryString['f%5B100%5D'] !== 'undefined' || typeof $.QueryString['f[100]'] !== 'undefined') {
          top.location.href = urlString;
        }
        // Student redirect.
        else {
          top.location.href = urlString + elem.options[elem.selectedIndex].value;
        }
      });

      // Submit search function with existing text & facets on change of sort select.

      // NOTE: Requires that sites/all/themes/bootstrap_rcpar/js/QueryString.js is added to the page.

      $('#hcc-search-results-sort-select', context).change(function () {
        actionSelectChangeFunc('sort', $(this).val());
      });

      // Change function for items per page select.
      $('#hcc-search-results-items-per-pg-select', context).change(function () {
        actionSelectChangeFunc('rows', $(this).val());
      });

      /**
       * On change function for sort & items per page selects
       *
       * @param string changedParam
       *  - 'sort' or 'rows' indicating the type of sort or the number of items
       *   to be shown per page
       * @param string newVal
       *  - the new value of the select
       *
       * NOTE: actionSelectChangeFunc() requires that
       *   sites/all/themes/bootstrap_rcpar/js/QueryString.js is added to the page.
       */
      function actionSelectChangeFunc(changedParam, newVal) {

        // The original search params.
        var oldSearch = top.location.search;

        if (oldSearch.length == 0) {
          // Need to add the beginning ? if there are no search params yet.
          oldSearch = '?';
        }
        else {
          // Remove any page query if it exists. (We'll always go to the first
          // page.)
          var pgQ = $.QueryString.page;
          if (typeof pgQ != 'undefined') {
            var pgQstr = 'page=' + pgQ;
            if (oldSearch.indexOf(pgQstr + '&') != -1) {
              // if the page query isn't the last one, we need to remove the
              // '&' that follows it as well.
              pgQstr = pgQstr + '&';
            }
            oldSearch = oldSearch.replace(pgQstr, '');
          }
        }


        // Define the beginning of our new url.
        var lpos = top.location.pathname.lastIndexOf('/') + 1;
        var locBase = top.location.pathname.substr(0, lpos);
        if (top.location.pathname == '/homework-help-center/search') {
          // This happens when a page has been selected with no search query.
          // Add a / to the end of the pathname.
          locBase = '/homework-help-center/search/';
        }

        // This will hold the new query params we will relocate to.
        var newSearch;
        // Holds any query params after our changed param.
        var newSearchEnd = '';
        // See if changed query has already been set via the url.
        var myChangedQuery = $.QueryString[changedParam];
        // This is used to set the changed param.
        var myChangedParamStartString = '&' + changedParam + '=';

        if (typeof myChangedQuery != 'undefined') {
          // The param has been set, remove it so we can reset it.
          var startPos = oldSearch.indexOf(myChangedParamStartString);
          // Check if there are params to be added after the changed param.
          var endPos = oldSearch.indexOf('&', startPos + 1);
          if (endPos != -1) {
            newSearchEnd = oldSearch.substr(endPos);
          }
          newSearch = oldSearch.substr(0, startPos) + myChangedParamStartString + newVal + newSearchEnd;
        }
        else {
          // No param had been set yet. Just add the new one.
          newSearch = oldSearch + myChangedParamStartString + newVal;
        }
        // Redirect to the new page.
        // We need to reset the keys value because it may have been changed
        // without submitting before the changed param was changed.
        top.location.href = locBase + encodeURIComponent($('input[name=keys]').val()) + newSearch;
      }
    }
  };
})(jQuery);
