(function($) {
  Drupal.behaviors.rcpar_fixed_coupon = {
    attach: function(context, settings) {
      $('#commerce-coupon-form #edit-field-coupon-exempt-tax-und', context).click(function() {
        // If the user has checked the box, show them a dialog to confirm
        if($(this).prop('checked')) {
          ConfirmDialog('Are you sure that this promo code should allow for tax exempt orders', this);
        }
      });
    }
  };

  function ConfirmDialog(message, checkbox) {
    $(this).prop('yes', 'uncheck');
    $('<div></div>').appendTo('body')
      .html('<div><h6>' + message + '?</h6></div>')
      .dialog({
        modal: true, title: 'Tax Exempt', zIndex: 10000, autoOpen: true,
        width: 'auto', resizable: false,
        buttons: {
          Yes: function() {
            $(checkbox).prop('checked', true);
            $(this).dialog("close");
          },
          No: function() {
            $(checkbox).prop('checked', false);
            $(this).dialog("close");
          }
        },
        close: function(event, ui) {
          $(this).remove();
        }
      });
  }
}(jQuery));