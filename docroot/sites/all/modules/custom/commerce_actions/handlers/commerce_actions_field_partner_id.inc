<?php

/**
 * This field handler displays a partner id from an entitlement realted with an order.
 *
 */
class commerce_actions_field_partner_id extends views_handler_field_entity {

  function render($values) {
    $order_id = $values->commerce_payment_transaction_order_id;
    $user_id = $values->commerce_order_uid;    
    return commerce_actions_get_partner_id_from_view($order_id, $user_id);
    
  }
}
