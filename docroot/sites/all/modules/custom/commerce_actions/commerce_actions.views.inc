<?php

/**
 * Implements hook_views_data_alter().
 */
function commerce_actions_views_data_alter(&$data) {
   $data['commerce_order']['partner_id'] = array(
    'title' => 'Custom Partner Id',
    'help' => 'This works for views with data from ORDERs',
    'field' => array(
      'handler' => 'commerce_actions_field_partner_id',
      'click sortable' => TRUE,
    ),    
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),       
  );
}
