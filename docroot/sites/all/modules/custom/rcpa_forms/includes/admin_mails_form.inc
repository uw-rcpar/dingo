<?php
/**
 * Form to save the mail to sent the notifications.
 */
function rcpa_forms_admin_mails_form($form, $form_state){
  $form['mails_destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination Mail'),
    '#default_value' => variable_get('mails_destination', variable_get('site_mail', '')),
    '#description' => t("The destination mail after each submit of Student of the Month."),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}