<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */

if(arg(3) != 'partners' && arg(4) != 'partners-view'){
  include drupal_get_path('module', 'views') . '/theme/views-view-table.tpl.php';
  return;
}else{
  foreach (arg() as $value) {
    $partner_nid = is_numeric($value) ? $value : 0 ;
  }
}

?>
<ul class="nav nav-tabs" role="tablist">  
  <li><a href="/admin/settings/rcpar/partners/partners-view/order-by-partner/<?php print $partner_nid?>">All</a></li>
  <li><a href="/admin/settings/rcpar/partners/partners-view/order-by-partner-full/<?php print $partner_nid?>">Full Paid</a></li>
  <li><a href="/admin/settings/rcpar/partners/partners-view/order-by-partner-partial/<?php print $partner_nid?>">Partial</a></li>
  <li><a href="/admin/settings/rcpar/partners/partners-view/order-by-partner-not-fully-paid/<?php print $partner_nid?>">Not Fully Paid</a></li>
</ul>
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#order-details" id="execute-modal-order-details" style="display:none">
</button>
<div id="container-of-values"></div>
    <form action="/order-by-partner-action" method="post">
      <input type="hidden" name="direct_bill" value="direct_bill-<?php print $partner_nid?>">    
<table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
   <?php if (!empty($title) || !empty($caption)) : ?>
     <caption><?php print $caption . $title; ?></caption>
  <?php endif; ?>
  <?php if (!empty($header)) : ?>
    <thead>
      <tr>
        <?php foreach ($header as $field => $label): ?>
          <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
            <?php print $label; ?>
          </th>
        <?php endforeach; ?>
      </tr>
    </thead>
  <?php endif; ?>
  <tbody>
    <?php foreach ($rows as $row_count => $row): ?>
      <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
        <?php foreach ($row as $field => $content): ?>
          <td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
            <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
 <input type="submit" value="Submit"></form>
