
(function ($) {
  Drupal.behaviors.rcpa_forms = {
    attach: function (context, settings) {

      // Use this code only for HHC Course Section form item.
      if(Drupal.settings.rcpa_forms.include_select_behaviour){
        // This is needed for bootstrap to refresh the HHC section and topic selects properly when the course is changed
        $( '.selectpicker' ).selectpicker( 'refresh' );
        $("div[id^='edit-field-course-chapter-']").removeClass("form-control");

        // This will only be called when #helpcenter-question-node-form is in the context.
        // i.e. if ajax is returning just #chapters-topics-selects-container, it isn't called.
        // The 2nd restriction is used to NOT show the popup again when the form fails validation.
        if (
          $('#helpcenter-question-node-form', context).length &&
          !$('div.messages.error.alert.alert-block.alert-danger', context).length
        ) {
          if (!($.cookie('rogerphilipp-hhc_dont_show_guidelines'))) {
            $('#hhc-question-guidelines-w-checkbox').modal('toggle');
          }
          $('#hhc-question-guidelines-w-checkbox').on('hidden.bs.modal', function () {
            // Set a cookie when the don't show checkbox is checked.
            if ($('div.dont_show_checkbox input')[0].checked) {
              $.cookie('rogerphilipp-hhc_dont_show_guidelines', true);
            }
          });
        }
      }

      // TODO: Fix this, it doesn't belong here but I don't have time to remove & test it now.
      $(document).ready(function () {

        // Update partner Direct bill form
        $(".view-commerce-backoffice-orders table input[type='text']").each(function(index) {
          $(this).attr('disabled', 'disabled');
        });
        
        $('.view-commerce-backoffice-orders input[ value="Partial payment"]').on('change', function() {
          var input_var = $(this).closest('.direct-bill-form-row').find("input[type='text']");
          var full_check_var = $(this).closest('.direct-bill-form-row').find("input[ value='paid_in_full']");
          $(input_var).removeAttr('disabled'); 
          if($(this).is(':checked')){
            $(full_check_var).attr('disabled', 'disabled');
              // $(input_var).removeAttr('disabled');                
            }else{
              $(full_check_var).removeAttr('disabled');
              // $(input_var).attr('disabled', 'disabled');
            }

          });
        $('.view-commerce-backoffice-orders input[ value="paid_in_full"]').on('change', function() {
          var input_var = $(this).closest('.direct-bill-form-row').find("input[type='text']");
          var partial_check_var = $(this).closest('.direct-bill-form-row').find("input[ value='Partial payment']");
          $(input_var).removeAttr('disabled'); 
          if($(this).is(':checked')){                            
            // $(input_var).attr('disabled', 'disabled');
            var total_amount = $(this).parent('td').parent('tr').find('.views-field-commerce-order-total').html();
            $(input_var).val(total_amount);
            $(partial_check_var).attr('disabled', 'disabled'); 
          } else {
            $(input_var).val('');                
            $(partial_check_var).removeAttr('disabled');
          }
        });
        
        $('.intacct-export').live('click',function () {
          $(this).attr('href',$('.view-id-commerce_backoffice_orders_export .feed-icon a').attr('href'));
        });
        
      });
    }
  };
}(jQuery));

