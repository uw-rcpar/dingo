Reed EN-212 NOTE:
This module seems to have been based on a contributed module then modified to 
handle Drupal files as well as webform files. 

I spent a lot of time trying to get the form at /admin/settings/rcpar/services/hubspot_forms
to work, ended up getting it to load properly to show existing Drupal form field to 
Hubspot form field mappings. It is not capable of saving any settings.

I discovered that it seems ALL non-webforms have ended up being mapped programmatically. 
I ended up making my changes that was as well. 

Webforms file to Hubspot file mappings are made by editing the webform, webform tab, 
and clicking on the Hubspot link at the top right of the page.
