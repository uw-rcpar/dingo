<?php
/**
 * @file
 * Provides admin settings page to adjust form submission settings.
 */

function hubspot_form_settings($form, $form_state, $node) {
  $form = array();
  $contact_allowed_fields = array(
    'products_and_information','email_address','email_address_student','name',
    'phone_number_contact', 'email_address_discount_verification', 'name_student',
    'phone_number_student','phone_number_verifications', 'name_discount_verification');
  $hubspot_forms = _hubspot_get_forms();

  if (isset($hubspot_forms['error'])) {
    $form['webforms']['#description'] = $hubspot_forms['error'];
  }
  else {
    if (empty($hubspot_forms['value'])) {
      $form['webforms']['#description'] = t('No HubSpot forms found. You will need to create a form on HubSpot before you can configure it here.');
    }
    else {
      // Get hubspot forms data
      $vars = hubspot_get_forms_data($hubspot_forms);
      $hubspot_form_options = $vars['hubspot_form_options'];
      $hubspot_field_options = $vars['hubspot_field_options'];

      $nid = $node->nid;

      $form['nid'] = array(
        '#type' => 'hidden',
        '#value' => $nid,
        );

      $form['hubspot_form'] = array(
        '#title' => t('HubSpot form'),
        '#type' => 'select',
        '#options' => $hubspot_form_options,
        '#ajax' => array(
          'callback' => 'hubspot_get_webform_fields',
          'wrapper' => 'hubspot-dynamic-id',
          'method' => 'replace',
          'effect' => 'fade',
          ),
        );
      
      $key = 0 ;
      if(_hubspot_default_value($nid)){
        $key = _hubspot_default_value($nid); 
        $form['hubspot_form']['#default_value'] = $key;
      } elseif(isset($form_state['values']['hubspot_form'])){
        $key = $form_state['values']['hubspot_form'];
      }

      $options_select = array();
      $title = $key ? $hubspot_form_options[$key] : '';
      $form[$key] = array(
        '#title' => t('Field mappings for ' . $title),
        '#type' => 'fieldset',
        '#tree'=> TRUE,
        '#id' => 'hubspot-dynamic-id'
      );
      if($key){
        foreach ($node->webform['components'] as $component) {
          $proced = FALSE;
          if($component['nid'] == 114 && in_array($component['form_key'], $contact_allowed_fields)){
            $proced = TRUE;
          }elseif($component['nid'] != 114){
            $proced = TRUE;
          }
          if($proced){             
            $form[$key][$component['form_key']] = array(
              '#title' => $component['name'],
              '#type' => 'select',
              '#options' => $hubspot_field_options[$key]['fields'],
            ); 
            if(_hubspot_default_value($nid, $key, $component['form_key'])){
              $form[$key][$component['form_key']]['#default_value'] = _hubspot_default_value($nid, $key, $component['form_key']);
            }
          }
        }
      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => ('Save Configuration'),
  );
 // $form_state['rebuild'] = TRUE;
  return $form;
}

/**
 * Ajax callback for hubspot form selection
 */
function hubspot_get_webform_fields($form, &$form_state){
  $selected_key = $form_state['values']['hubspot_form'];
  $hub_forms_options = $form['hubspot_form']['#options'];
  $selected_title = $hub_forms_options[$selected_key];
  $vars = hubspot_get_forms_data($hubspot_forms);
  $hubspot_field_options = $vars['hubspot_field_options'];
  $node = $form_state['build_info']['args'][0];
  $nid = $node->nid;
  // Generate fieldset with settigns for webform - hubspot mapping
  $form[$selected_key] = array(
    '#title' => t('Field mappings for ' . $selected_title),
    '#type' => 'fieldset',
    '#tree'=> TRUE,
    '#id' => 'hubspot-dynamic-id'
  );
  // Get Webform fields 
  foreach ($node->webform['components'] as $component) {
    $proced = FALSE;
    if($component['nid'] == 114 && in_array($component['form_key'], $contact_allowed_fields)){
      $proced = TRUE;
    }elseif($component['nid'] != 114){
      $proced = TRUE;
    }
    if($proced){             
      $form[$selected_key][$component['form_key']] = array(
        '#title' => $component['name'],
        '#type' => 'select',
        '#options' => $hubspot_field_options[$selected_key]['fields'],
      ); 
    }
  }
  $form_state['rebuild'] = TRUE;
  return $form[$selected_key];
}

/**
 * Submit handler for hubspot_form_settings form.
 */
function hubspot_form_settings_submit($form, &$form_state) {
  $txn = db_transaction();

  db_delete('hubspot')->condition('nid', $form_state['values']['nid'])->execute();

  if ($form_state['values']['hubspot_form'] != '--donotmap--') {
    foreach($form_state['values'][$form_state['values']['hubspot_form']] as $webform_field => $hubspot_field) {
      $fields = array('nid' => $form_state['values']['nid'], 'hubspot_guid' => $form_state['values']['hubspot_form'], 'webform_field' => $webform_field, 'hubspot_field' => $hubspot_field);
      db_insert('hubspot')->fields($fields)->execute();
    }
  }

  drupal_set_message('The configuration options have been saved.');
}

/*
 * Get hubspot static info  
 */
function hubspot_get_forms_data ($hubspot_forms = array()){
  $vars = &drupal_static(__FUNCTION__);
  if (!isset($vars)) {
    foreach ($hubspot_forms['value'] as $hubspot_form) {
      $hubspot_form_options[$hubspot_form['guid']] = $hubspot_form['name'];
      $hubspot_field_options[$hubspot_form['guid']]['fields']['--donotmap--'] = "Do Not Map";
      foreach ($hubspot_form['fields'] as $hubspot_field) {
        $hubspot_field_options[$hubspot_form['guid']]['fields'][$hubspot_field['name']] = $hubspot_field['label'] ? $hubspot_field['label'] : $hubspot_field['name'];
      }
    }
    $vars = array(
      'hubspot_form_options' => $hubspot_form_options, 
      'hubspot_field_options' => $hubspot_field_options
    );
  }
  return $vars;
}