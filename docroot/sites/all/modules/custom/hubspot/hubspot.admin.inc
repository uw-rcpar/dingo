<?php


function hubspot_clean_name_inputs($inputs_array) {
     
    $var = array();
    
    foreach ($inputs_array as $key => $value) {
    
        $result = strpos($value['#id'], "-format");

        if($result === FALSE){
            
            $id_aux = explode('-und',$value['#id']);
            $value['#id']=$id_aux[0];
            
            $value['#id'] = str_replace('edit-','', $value['#id']);
            $value['#id'] = str_replace('-value','', $value['#id']);
            $value['#id'] = str_replace('-','_', $value['#id']);
            $value['#id'] = preg_replace('/__.*/', '', $value['#id']);
            
            $var[]=array('#title' => $value['#title'] , '#id' => $value['#id'] );    
            
        }
   
    }
   
    return $var;
    
        
}

/**
 * Provide admin settings for HubSpot API keey and connection + debug settings
 */
function hubspot_admin_settings_basic() {
  // start to create this settings page form
  $form = array();

  $form['additional_settings'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['settings'] = array(
    '#title' => t('Connectivity'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#group' => 'additional_settings',
  );

  $form['settings']['hubspot_portalid'] = array(
    '#title' => t('HubSpot Portal ID'),
    '#type' => 'textfield',
    '#required' => FALSE,
    '#default_value' => variable_get('hubspot_portalid', ''),
    '#description' => t('Enter the Hubspot Portal ID for this site.  It can be found by <a href="https://login.hubspot.com/login" target="_blank">logging into HubSpot</a> going to the Dashboard and examining the url. Example: "https://app.hubspot.com/dashboard-plus/12345/dash/".  The number after "dashboard-plus" is your Portal ID.'),
  );

  $form['settings']['hubspot_hapikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Hapikey'),
    '#default_value' => variable_get("hubspot_hapikey", ""),
    '#size' => 60,
    '#required' => FALSE,
    '#description' => t('The hapikey is a key need it to interact with the hubspot webservice, it can be generated clicking on  <a href="https://app.hubspot.com/keys/get" target="_blank">Getting hubspot api key</a>.'),
  );

  if (variable_get('hubspot_portalid', '')) {
    $form['settings']['hubspot_authentication'] = array(
      '#value' => t('Connect Hubspot Account'),
      '#type' => 'submit',
      '#validate' => array(),
      '#submit' => array('hubspot_oauth_submit'),
    );

    if (variable_get('hubspot_refresh_token', '')) {
      $form['settings']['hubspot_authentication']['#suffix'] = t('Your Hubspot account is connected.');
      $form['settings']['hubspot_authentication']['#value']  = t('Disconnect Hubspot Account');
      $form['settings']['hubspot_authentication']['#submit'] = array('hubspot_oauth_disconnect');
    }
  }

  $form['settings']['hubspot_log_code'] = array(
    '#title' => t('HubSpot Traffic Logging Code'),
    '#type' => 'textarea',
    '#default_value' => variable_get('hubspot_log_code', ''),
    '#description' => t('To enable HubSpot traffic logging on your site, paste the External Site Traffic Logging code here.'),
  );

  $form['debug'] = array(
    '#title' => t('Debugging'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#group' => 'additional_settings',
  );

  $form['debug']['hubspot_debug_on'] = array(
    '#title' => t('Debugging enabled'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('hubspot_debug_on', 0),
    '#description' => t('If debugging is enabled, HubSpot errors will be emailed to the address below. Otherwise, they will be logged to the regular Drupal error log.'),
  );

  $form['debug']['hubspot_debug_email'] = array(
    '#title' => t('Debugging email'),
    '#type' => 'textfield',
    '#default_value' => variable_get('hubspot_debug_email', variable_get('site_mail', '')),
    '#description' => t('Email error reports to this address if debugging is enabled.'),
  );

  return system_settings_form($form);
}

/**
 * Provides admin settings page to adjust HubSpot API key, debugging settings,
 * JavaScript embedding, and form submission settings.
 * NOTE: This form WAS NOT WORKING before this commit. See README.txt
 */
function hubspot_admin_settings() {

  // used to separate out drupal forms from webforms
  $display_form = arg(5);

  $form = array();

  $form['settings']['description_markup'] = array(
    '#type' => 'item',
    '#title' => "Note:",
    '#markup' => '<p>This page can be used to check the mapping of some Drupal forms fields to Hubspot form fields. They cannot be set here. Drupal forms shown are those whose form_id are in the hubspot_forms_list variable.</p>',
    '#weight' => -20,
  );

  // set up vertical tab to map fields to hubspot forms
  // Set labels of vertical tab for webforms or standard Drupal forms
  if ($display_form == "web") { // We're dealing w/ webforms, not Drupal forms
    $form['webforms'] = array(
      '#title' => t('Webforms'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#group' => 'additional_settings',
      '#description' => 'The following webforms have been detected and can be configured to submit to the HubSpot API.',
      '#tree' => TRUE,
    );
  }
  else { // dealing w/ Drupal forms
    $form['forms'] = array(
      '#title' => t('Drupal forms'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#description' => 'The following shows the Drupal Forms mappings. They can be read but not changed on this page',
    );
  }

  // variable doesn't exist on enrollemnt environment 2016-09-28
  $webform_nodes = variable_get('webform_node_types', array('webform'));
  $nodes         = array();

  // this variable ends up almost 32K lines of var_export
  // it holds approx 101 forms currently defined on our HubSpot account in an array like:
  /**
    array (
    'value' => // this is the only key at this level
    array (
    0 => // there are 101 keys here, indexes of each form stored on hubspot
    array (
    'guid' => '01827847-9dec-4d3d-9e81-f2a751067d06',
    'name' => 'Dear Roger questions',
    'action' => '',
    'method' => 'POST',
    'cssClass' => 'stacked',
    'redirect' => '',
    'fields' => // arrays for each fields for this form
    array (
    0 =>
    array (
    'name' => 'firstname',
    'label' => 'First name',
    'type' => 'string',
    'fieldType' => 'text',
    'description' => '',
    'groupName' => 'contactinformation',
    'displayOrder' => 10,
    'required' => true,
    'selectedOptions' =>
    array (
    ),
    'options' =>
    array (
    ),
    'validation' =>
    array (
    'name' => '',
    'message' => '',
    'data' => '',
    'useDefaultBlockList' => false,
    'blockedEmailAddresses' =>
    array (
    ),
    ),
    'enabled' => true,
    'hidden' => false,
    'defaultValue' => '',
    'isSmartField' => false,
    'unselectedLabel' => '',
    'placeholder' => '',
    'dependentFieldFilters' =>
    array (
    ),
    'labelHidden' => false,
    ),
    ...,
    ),
    'submitText' => 'Enter to Win!',
    'followUpId' => '',
    'notifyRecipients' => 'dmiles@rogercpareview.com',
    'leadNurturingCampaignId' => '',
    'formFieldGroups' =>
    array (
    0 =>
    array (
    'name' => 'group-0',
    'fieldNames' =>
    array (
    0 => 'firstname',
    ),
    'default' => true,
    ),
    1 =>
    array (
    'name' => 'group-1',
    'fieldNames' =>
    array (
    0 => 'lastname',
    ),
    'default' => true,
    ),
    2 =>
    array (
    'name' => 'group-2',
    'fieldNames' =>
    array (
    0 => 'email',
    ),
    'default' => true,
    ),
    ),
    'createdAt' => 1427139244616,
    'updatedAt' => 1427205138357,
    'performableHtml' => '',
    'migratedFrom' => '',
    'ignoreCurrentValues' => true,
    'metaData' =>
    array (
    ),
    'deletable' => true,
    'inlineMessage' => '',
    'tmsId' => '',
    'captchaEnabled' => false,
    'campaignGuid' => '',
    'cloneable' => true,
    'editable' => true,
    'formType' => 'HUBSPOT',
    'embeddedCode' => '<script charset="utf-8" src="http://js.hubspot.com/forms/current.js"></script>
    <script>
    hbspt.forms.create({
    portalId: \'11568\',
    formId: \'ff89fd3a-c0b7-4b9c-bd46-2d95b78bdccd\'
    });
    </script>',
    ),
   */
  $hubspot_forms = _hubspot_get_forms();

  // show communication error if necessary
  if (isset($hubspot_forms['error'])) {
    $form['forms']['#description'] = $hubspot_forms['error'];
  }
  else { // no communication error
    if (empty($hubspot_forms['value'])) { // no hubspot forms found
      $form['forms']['#description'] = t('No HubSpot forms found. You will need to create a form on HubSpot before you can configure it here.');
    }
    else { // hubspot forms found - build config tab
      // will hold hubspot form options for each existing Drupal form to be mapped to like:
      //array (
      //  '--donotmap--' => 'Do Not Map',
      //  '01827847-9dec-4d3d-9e81-f2a751067d06' => 'Dear Roger questions',
      //  ...
      //)
      $hubspot_form_options = array();

      // will hold hubspot field options for Drupal form fields to be mapped to keyed on hubspot form guid
      // each form's array looks like:
      /**
        array (
        'fields' =>
        array (
        '--donotmap--' => 'Do Not Map',
        'firstname' => 'First name',
        'lastname' => 'Last name',
        'email' => 'Email address',
        'phone' => 'Phone Number',
        'graduation_date_month' => 'Graduation Date Month',
        'graduationdate' => 'Graduation Date Year',
        'study_for_the_cpa_exam_month' => 'Study for the CPA Exam Month',
        'study_for_the_cpa_exam_year' => 'Study for the CPA Exam Year',
        'hs_rich_text_9e8e60b5800ad7fdb1d20f01649ce141e8d41dde' => 'Rich Text',
        'dear_roger_question' => 'Your Question for Roger:',
        ),
        )
       */
      $hubspot_field_options = array();

      // here $key is the index of the form, $hubspot_form is an array of hubspot form values (see above)
      foreach ($hubspot_forms['value'] as $key => $hubspot_form) {
        // the option value is the 
        $hubspot_form_options[$hubspot_form['guid']]                            = $hubspot_form['name'];
        $hubspot_field_options[$hubspot_form['guid']]['fields']['--donotmap--'] = "Do Not Map";
        foreach ($hubspot_form['fields'] as $hubspot_field) {
          $hubspot_field_options[$hubspot_form['guid']]['fields'][$hubspot_field['name']] = $hubspot_field['label'];
        }
        if (!count($hubspot_form['fields'])) {
          $hubspot_field_options[$hubspot_form['guid']]['fields'] = unserialize(HUBSPOT_DEFAULT_FIELDS);
        }
      }

      asort($hubspot_form_options); // make it easier to find the hubspot form
      // add the the default option not to map the Drupal form to the beginning of the list
      array_unshift($hubspot_form_options, array("--donotmap--" => "Do Not Map"));

      if ($display_form != "web") { // dealing w/ Drupal standard forms

        /*
         * REGULAR FORM LOGIC
         */
        // 2016-09-28 enrollment environment this shows
        //array (
        //  0 => 'user_register_form',
        //  1 => 'user_pass',
        //  2 => 'user_login_block',
        //)
        // used to define which Drupal form id's will be allowed to be mapped
        // need to limit them so as not to max out php memory
        $hubspot_form_list = variable_get("hubspot_forms_list", array());

        // start the forms array
        $forms = $hubspot_form_list;

        // add a special form to forms
        $forms[] = 'user_register_form_webcast';

        // used later to designate which inputs will be added to map for user_register_form_webcast
        $webcast_fields = array('name', 'mail', 'field_first_name', 'field_last_name');

        // create fieldsets for each of our selected forms
        foreach ($forms as $forms_form_id) {

          $add_fieldset_desc = '';
          switch ($forms_form_id) {
            case 'rcpar_asl_create_account_form':
              module_load_include("inc", "rcpar_asl", "includes/rcpar_asl.forms");
              $form_logic = drupal_get_form('rcpar_asl_create_account_form', $partner_node);

              // graduation date is set programmatically in hubspot_form_submission_insert
              unset($form_logic['graduation_date_new']);
              $add_fieldset_desc = t('Graduation Date Month & Year is set programmatically in hubspot_form_submission_insert.');
              break;

            case 'user_register_form_webcast':
              // I believe user_register_form_webcast is an adjustment made for webcast presentations
              $form_logic = @drupal_get_form('user_register_form');
              break;

            case 'verify_discount_create_account_form':
              module_load_include('inc', 'discount_verification', 'includes/discount_verification.forms');
              $form_logic = @drupal_get_form($forms_form_id);
              break;
            case 'student_of_the_month_node_form':
              // I'm not sure why this form ID is in the mapping table, there is no form that drupal_get_form
              // can retrieve when it's called. hubspot.install seems to indicate there is a matching node id
              // in the system for it and its presence seems intentional, but it invokes node form hooks which
              // ultimately throw errors and break the whole form when pathauto_form_node_form_alter is called
              // with a NULL entity.
              $form_logic = array();
              continue;
              break;

            default:
              $form_logic = @drupal_get_form($forms_form_id);
          }
          // will hold array of all Drupal textarea, textfield, & select fields in this form
          $array_inputs_form = array();

          hubspot_get_inputs_form($form_logic, $array_inputs_form);

          if (count($array_inputs_form) > 0) {
            // set up a fieldset for this Drupal form
            $form['forms']['frm-' . $forms_form_id] = array(
              '#title' => $forms_form_id,
              '#type' => 'fieldset',
              '#collapsible' => TRUE,
              '#collapsed' => TRUE,
            );

            if (!empty($add_fieldset_desc)) { // set in above switch statement
              $form['forms']['frm-' . $forms_form_id]['#description'] = $add_fieldset_desc;
            }
            // add a select of hubspot forms for this Drupal form to be mapped to
            $form['forms']['frm-' . $forms_form_id]['hubspot_form'] = array(
              '#title' => t('HubSpot form'),
              '#type' => 'select',
              '#name' => "forms[" . $forms_form_id . "][hubspot_form]",
              '#options' => $hubspot_form_options,
              '#default_value' => _hubspot_default_value($forms_form_id),
            );

            // cleans out the array so that each Drupal form element is represended by an array of
            // array(
            //   #id => <element's form id derived from the form build>
            //   #title => <elemment's form title>
            // )
            $array_inputs_form = hubspot_clean_name_inputs($array_inputs_form);


            // create a fieldset for each hubspot form (approx 101) inside this Drupal form fieldset
            // $key is the hubspot form guid, $value is the hubspot form name
            foreach ($hubspot_form_options as $key => $value) {
              if ($key != '--donotmap--') {
                $form['forms']['frm-' . $forms_form_id][$key] = array(
                  '#title' => t('Field mappings for ' . $value),
                  '#type' => 'fieldset',
                  '#collapsible' => FALSE,
                  '#states' => array(
                    // set javascript to set visiblity of the fieldset depending 
                    // on if the select of the fieldset matches this element's container id
                    'visible' => array(
                      ':input[name="forms[' . $forms_form_id . '][hubspot_form]"]' => array('value' => $key),
                    ),
                  ),
                );

                foreach ($array_inputs_form as $input) {
                  // old code was only adding Drupal fields to be input for user_register_form
                  // rewritten to add drupal elements for forms in hubspot_forms_list variable
                  if (
                    ($forms_form_id == "user_register_form_webcast" && in_array($input['#id'], $webcast_fields)) ||
                    in_array($forms_form_id, $hubspot_form_list)
                  ) {
                    $form['forms']['frm-' . $forms_form_id][$key][$input['#id']] = array(
                      '#title' => (!isset($input['#title'])) ? "NO_LABEL" : $input['#title'] . " (" . $input['#id'] . ")",
                      '#type' => 'select',
                      '#options' => $hubspot_field_options[$key]['fields'],
                      '#default_value' => _hubspot_default_value($forms_form_id, $key, $input['#id']),
                    );
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  // need to reset the page title as it was reset while loading forms
  drupal_set_title(t('Hubspot Drupal form mappings'));

  return $form;
}

/**
 * Submit handler for hubspot_admin_settings form.
 * Note: This handler never gets called
 */
function hubspot_admin_settings_submit($form, &$form_state) {
    $txn = db_transaction();


    // Check if webform values even exist before continuing.
    if (isset($form_state['values']['webforms'])) {
        foreach ($form_state['values']['webforms'] as $key => $settings) {
            db_delete('hubspot')->condition('nid', str_replace('nid-', '', $key))->execute();

            if ($settings['hubspot_form'] != '--donotmap--') {
                foreach ($settings[$settings['hubspot_form']] as $webform_field => $hubspot_field) {
                    $fields = array('nid' => str_replace('nid-', '', $key), 'hubspot_guid' => $settings['hubspot_form'], 'webform_field' => $webform_field, 'hubspot_field' => $hubspot_field);
                    db_insert('hubspot')->fields($fields)->execute();
                }
            }
        }
    }
       

    // Check if drupal forms values even exist before continuing.
    if (isset($form_state['values']['forms'])) {
        foreach ($form_state['values']['forms'] as $key => $settings) {

           
           $key=str_replace('frm-', '', $key);
           
           db_delete('hubspot')->condition('nid', $key )->execute();
     
           $settings['hubspot_form'] = $_POST['forms'][$key]['hubspot_form'];
           if ($settings['hubspot_form'] != '--donotmap--') {

                foreach ($settings[$settings['hubspot_form']] as $form_field => $hubspot_field) {
                   
                   $fields = array('nid' =>  $key ,
                                   'hubspot_guid' => $settings['hubspot_form'],
                                   'webform_field' => $form_field,
                                   'hubspot_field' => $hubspot_field);
                   
                  
                   db_insert('hubspot')->fields($fields)->execute();
                }
            }
        }
    }

    drupal_set_message('The configuration options have been saved.');
}

/**
 * Submit handler added from hubspot_admin_settings_basic(). Begins oAuth authentication process
 * with hubspot
 */
function hubspot_oauth_submit($form, &$form_state) {
  $data = array(
    'client_id' => HUBSPOT_CLIENT_ID,
    'redirect_uri' => hubspot_oauth_get_redirect_uri(),
    'scope' => HUBSPOT_SCOPE,
  );

  $form_state['redirect'][] = url('https://app.hubspot.com/oauth/authorize', array('query' => $data));
}

/**
 * Page callback for hubspot/oauth
 * HubSpot redirects the user here after we authenticate via oAuth per hubspot_oauth_submit()
 *
 * Saves OAuth tokens from HubSpot and redirects user.
 *
 * NOTE: For RCPAR purposes, it's not really necessary to do this.
 *
 * Per https://developers.hubspot.com/docs/methods/auth/oauth-overview
 * "HubSpot's APIs allow for two means of authentication, OAuth and API keys.  API keys are great for rapid prototyping,
 * but for security and commercial use, all integrations designed to be used by multiple HubSpot customers should use
 * OAuth - this is required for becoming a featured integration."
 *
 * We are not making an app to be used by other people, so API key is sufficient as well as easier.
 */
function hubspot_oauth_connect() {
  // If we got back an access code, we request our access tokens
  if(!empty($_GET['code'])) {
    // Request payload
    $data = array(
      'refresh_token' => variable_get('hubspot_refresh_token'),
      'client_id' => HUBSPOT_CLIENT_ID,
      'grant_type' => 'authorization_code',
      'client_secret' => HUBSPOT_CLIENT_SECRET,
      'redirect_uri' => hubspot_oauth_get_redirect_uri(),
      'code' => $_GET['code'],
    );
    $data = drupal_http_build_query($data);

    // HTTP request options
    $options = array(
      'headers' => array(
        'Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8'
      ),
      'method' => 'POST',
      'data' => $data,
    );

    // Request the token
    $return = drupal_http_request('https://api.hubapi.com/oauth/v1/token', $options);
    if(isset($return->data)) {
      $return = (object) drupal_json_decode($return->data);

      // SUCCESS! Got a token
      if($return->access_token) {
        drupal_set_message(t('Successfully authenticated with Hubspot.'), 'status', FALSE);

        variable_set('hubspot_access_token', $return->access_token);
        variable_set('hubspot_refresh_token', $return->refresh_token);
        variable_set('hubspot_expires_in', $return->expires_in);
      }
      // FAIL
      else {
        // Known error:
        if($return->error) {
          drupal_set_message(t('Refresh token failed with Error Code "%code: %status_message". Reconnect to your HubSpot account.', array(
            '%code' => $return->error,
            '%status_message' => $return->error_description
          )), 'error', FALSE);
        }
        // Unknown error
        else {
          drupal_set_message('Unknown error: <pre>' . print_r($return, TRUE) . '</pre>', 'error');
        }
      }

    }
  }
  // Something else weird went wrong
  else {
    drupal_set_message('An unexpected error occurred when trying to authenticate with Hubspot', 'error');
  }

  drupal_goto();
}

/**
 * Form submit handler.
 *
 * Deletes Hubspot OAuth tokens.
 */
function hubspot_oauth_disconnect($form, &$form_state) {
  variable_del('hubspot_access_token');
  variable_del('hubspot_refresh_token');
  variable_del('hubspot_expires_in');
  drupal_set_message('HubSpot account disconnected from Drupal.');
}
