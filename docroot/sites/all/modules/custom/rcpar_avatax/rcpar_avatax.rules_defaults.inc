<?php

/**
 * @return array
 * Commits the order on avatax after checkout process is complete
 * This rule should be imported after cache cleanup
 * see http://jamestombs.co.uk/2012-09-19/drupal-7-defining-rules-within-module-using-php for ref
 */
  function rcpar_avatax_default_rules_configuration() {
    $configs = array();
    $rule = '{ 
      "rules_rcpar_commit_order_sales_tax_after_checkout" : {
        "LABEL" : "RCPAR COMMIT order sales tax after checkout",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "commerce_order", "rcpar_avatax", "commerce_checkout" ],
        "ON" : { "commerce_checkout_complete" : [] },
        "IF" : [
          { "NOT commerce_order_contains_product" : {
            "commerce_order" : [ "commerce_order" ],
              "product_id" : "FREETRIAL-BUNDLE",
              "operator" : "\u003E=",
              "value" : "1"
            }
          }
        ],
        "DO" : [
          { "rcpar_avatax_commit_transaction" : { "order" : [ "commerce-order" ] } }
        ]
      }
    }';
    $configs['rules_rcpar_commit_order_sales_tax_after_checkout'] = rules_import($rule);

    $rule = '{ "rules_rcpar_cancel_order_sales_tax" : {
        "LABEL" : "RCPAR CANCEL order sales tax",
        "PLUGIN" : "reaction rule",
        "OWNER" : "rules",
        "REQUIRES" : [ "rules", "rcpar_avatax", "entity" ],
        "ON" : { "commerce_order_update" : [] },
        "IF" : [
          { "NOT data_is" : {
              "data" : [ "commerce-order:status" ],
              "value" : [ "commerce-order-unchanged:status" ]
            }
          },
          { "data_is" : { "data" : [ "commerce-order:status" ], "value" : "canceled" } }
        ],
        "DO" : [
          { "rcpar_avatax_cancel_transaction" : { "order" : [ "commerce-order" ] } }
        ]
      }
    }';
    $configs['rules_rcpar_cancel_order_sales_tax'] = rules_import($rule);

    return $configs;
  }


