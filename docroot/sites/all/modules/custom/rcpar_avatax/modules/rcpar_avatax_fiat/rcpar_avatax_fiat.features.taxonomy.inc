<?php
/**
 * @file
 * rcpar_avatax_fiat.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function rcpar_avatax_fiat_taxonomy_default_vocabularies() {
  return array(
    'avatax_tax_codes' => array(
      'name' => 'AvaTax Tax codes',
      'machine_name' => 'avatax_tax_codes',
      'description' => NULL,
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
