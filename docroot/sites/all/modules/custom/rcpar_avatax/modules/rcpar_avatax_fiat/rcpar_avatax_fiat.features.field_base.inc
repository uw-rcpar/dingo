<?php
/**
 * @file
 * rcpar_avatax_fiat.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function rcpar_avatax_fiat_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'avatax_code'.
  $field_bases['avatax_code'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'avatax_code',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'avatax_tax_codes',
          'parent' => NULL,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
