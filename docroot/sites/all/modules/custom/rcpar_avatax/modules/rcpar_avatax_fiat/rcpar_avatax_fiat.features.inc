<?php
/**
 * @file
 * rcpar_avatax_fiat.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function rcpar_avatax_fiat_commerce_product_default_types() {
  $items = array(
    'fiat' => array(
      'type' => 'fiat',
      'name' => 'FIAT',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}
