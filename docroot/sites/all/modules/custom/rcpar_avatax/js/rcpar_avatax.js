(function ($) {
    Drupal.behaviors.rcparAvataxModule = {
        attach: function (context, settings) {

            // Initially disable all the input texts that belongs to unchecked rows
            $("#edit-refund-items-wrapper table .form-type-checkbox input[type=checkbox]:not(:checked)").each(function(i,e){
                var $textfield = $(this).closest("tr").find("input[type=text]") ;
                $textfield.attr('disabled', 'disabled');
            });

            // When user checks/unchecks a row, needs to enable/disable the current input
            $("#edit-refund-items-wrapper table .form-type-checkbox input[type=checkbox]").change(function() {
                var $textfield = $(this).closest("tr").find("input[type=text]") ;
                if ($(this).is(":checked")) {
                    $textfield.removeAttr('disabled');
                    $textfield.attr('readonly', 'readonly');
                    $textfield.val($textfield.attr('summatory'));
                }
                else {
                    $textfield.attr('disabled', 'disabled');
                    $textfield.removeAttr('readonly');
                    $textfield.val('');
                }
            });
        }
    };
}(jQuery));