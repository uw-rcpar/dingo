<?php
function _rcpar_avatax_line_item_add_amount($line_item, $amount){
  $line_item = commerce_line_item_load($line_item->line_item_id); // load extra info
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  $line_item->commerce_total[LANGUAGE_NONE][0]['amount'] += $amount;
  $line_item->commerce_unit_price[LANGUAGE_NONE][0]['amount'] += $amount;
  $data = $line_item_wrapper->commerce_unit_price->data->value();
  $data['components'][0]['price']['amount'] += $amount;
  $line_item_wrapper->commerce_unit_price->data->set($data);
  commerce_line_item_save($line_item);
  entity_get_controller('commerce_line_item')->resetCache(array($line_item->line_item_id));
}

/**
 * @param $order
 * @return mixed
 * Returns the line item
 */
function _rcpar_avatax_get_taxes_line_item($order) {
  $items = commerce_line_item_load_multiple(array(), array('order_id'=>$order->order_id, 'type'=> 'avatax'));
  if ( count($items) == 1 ) {
    return current($items);
  }else{
    return null;
  }
}


/**
 * @param $order
 * @return mixed
 * Returns the line item
 */
function _rcpar_avatax_get_shipping_line_item($order) {
  $items = commerce_line_item_load_multiple(array(), array('order_id'=>$order->order_id, 'type'=> 'shipping'));
  if ( count($items) == 1 ) {
    return current($items);
  }else{
    return null;
  }
}


function _rcpar_avatax_adjustment($order, $adjustment_type = RCPAR_ADJUSTMENT_TYPE_REFUND ,$adjustment_args ,$amount,$doc_code_suffix = '' ) {
  switch ($adjustment_type) {
    case RCPAR_ADJUSTMENT_TYPE_REFUND :
      $refund_type = $adjustment_args['refund_type'];
      $refund_line_amounts = $adjustment_args['refund_line_amounts'];
      break;
  }

  $product_version =  variable_get('commerce_avatax_product_version', COMMERCE_AVATAX_BASIC_VERSION);
  $commit = true; //
  $use_mode = variable_get('commerce_avatax_use_mode');
  $doc_code_prefix = 'dc';
  $company_code = variable_get('commerce_avatax_' . $product_version . '_' . $use_mode . '_company', '');

  if (!$company_code) {
    drupal_set_message(t('AvaTax company code is not set.'), 'error');
    return FALSE;
  }

  // Sales Tax Shipping code.
  $shipcode = (variable_get('commerce_avatax_shipcode', ''));

  // Build order wrapper.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Exit if there are no line items in the order wrapper.
  if (count($order_wrapper->commerce_line_items) == 0) {
    drupal_set_message(t('There are no line items for this order.'), 'error');
    return FALSE;
  }

  // Get taxable address.
  $tax_address_profile = variable_get('commerce_avatax_tax_address', '');
  if ($tax_address_profile == 'Billing') {
    if (isset($order_wrapper->commerce_customer_billing->commerce_customer_address)) {
      $billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();
      $street1 = $billing_address['thoroughfare'];
      $street2 = $billing_address['premise'];
      $city = $billing_address['locality'];
      $state = $billing_address['administrative_area'];
      $country = $billing_address['country'];
      $zip = $billing_address['postal_code'];
    }
  }
  elseif ($tax_address_profile == 'Shipping') {
    if (isset($order_wrapper->commerce_customer_shipping->commerce_customer_address)) {
      $shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();
      $street1 = $shipping_address['thoroughfare'];
      $street2 = $shipping_address['premise'];
      $city = $shipping_address['locality'];
      $state = $shipping_address['administrative_area'];
      $country = $shipping_address['country'];
      $zip = $shipping_address['postal_code'];
    }
  }

  // Get primary business location.
  $primary_street1 = (variable_get('commerce_avatax_primary_street1', ''));
  $primary_street2 = (variable_get('commerce_avatax_primary_street2', ''));
  $primary_city = (variable_get('commerce_avatax_primary_city', ''));
  $primary_state = (variable_get('commerce_avatax_primary_state', ''));
  $primary_country = (variable_get('commerce_avatax_primary_country', ''));
  $primary_zip = (variable_get('commerce_avatax_primary_zip', ''));

  // Initialize sales tax exemption variable.
  $avatax_exemption_code = '';

  // Get User name or e-mail address.
  if ($order->uid == 0) {
    if ($order->order_id != 0 && $order->mail == '') {
      $user_id = 'administrator';
    }
    else {
      $user_email = $order->mail;
      $user_id = commerce_avatax_email_to_username($user_email);
    }
  }
  else {
    $user_data = user_load($order->uid);
    if (variable_get('commerce_avatax_exemptions_status', 0)) {
      if (isset($user_data->avatax_exemption_code[LANGUAGE_NONE][0]['value'])) {
        $avatax_exemption_code = $user_data->avatax_exemption_code[LANGUAGE_NONE][0]['value'];
      }
    }
    $user_id = $user_data->name;
  }

  $doc_date = REQUEST_TIME;


  // Get currency code from the order.
  $avatax_total = $order_wrapper->commerce_order_total->value();
  $currency_code = $avatax_total['currency_code'];

  // Construct arguments for AvaTax functions.
  $ava_args = compact('product_version', 'company_code', 'doc_code_prefix', 'doc_date', 'user_id', 'avatax_exemption_code',
    'commit', 'currency_code', 'shipcode', 'use_mode', 'street1', 'street2', 'city', 'state', 'country', 'zip',
    'primary_street1', 'primary_street2', 'primary_city', 'primary_state', 'primary_country', 'primary_zip');

  module_load_include('inc', 'commerce_avatax', 'includes/commerce_avatax_calc');
  // Call avatax calculate tax just to create a new order on avatax with negative values - see rcpar_avatax_get_tax($order, $order_wrapper, $ava_args);
  $request_body = array(
    'Client' => 'DrupalCommerce-CommerceGuys,4.3',
    'CompanyCode' => $ava_args['company_code'],
    'DetailLevel' => 'Tax',
    'Commit' => $ava_args['commit'],
    'CurrencyCode' => $ava_args['currency_code'],
    'DocType' => 'SalesInvoice',
    'DocCode' => $ava_args['doc_code_prefix'] . '-' . $order->order_id . '-'. $doc_code_suffix,
    'DocDate' => date("Y-m-d", $ava_args['doc_date']),
    'CustomerCode' => $ava_args['user_id'],
    'CustomerUsageType' => $ava_args['avatax_exemption_code'],
    'Addresses' => array(
      // Origin.
      array(
        'AddressCode' => 0,
        'Line1' => $ava_args['primary_street1'],
        'Line2' => $ava_args['primary_street2'],
        'City' => $ava_args['primary_city'],
        'Region' => $ava_args['primary_state'],
        'Country' => $ava_args['primary_country'],
        'PostalCode' => $ava_args['primary_zip'],
      ),
      // Destination.
      array(
        'AddressCode' => 1,
        'Line1' => $ava_args['street1'],
        'Line2' => $ava_args['street2'],
        'City' => $ava_args['city'],
        'Region' => $ava_args['state'],
        'Country' => $ava_args['country'],
        'PostalCode' => $ava_args['zip'],
      ),
    ),
  );

  $i = 1;
  if ($adjustment_type == RCPAR_ADJUSTMENT_TYPE_REFUND && $refund_type == RCPAR_REFUND_TYPE_ITEMS) {
    foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
      $line_item = $line_item_wrapper->value();
      $tax_code = '';

      $line_item_id = $line_item->line_item_id;

      if (isset($refund_line_amounts[$line_item_id])) {
        $refund_amount = $refund_line_amounts[$line_item_id] ;
        if (in_array($line_item->type, commerce_product_line_item_types()) || $line_item->type == 'shipping') {
          if ($ava_args['product_version'] == COMMERCE_AVATAX_PRO_VERSION) {
            $tax_field_name = 'avatax_code';
            if ($line_item->type == 'shipping') {
              // We use the sales Tax Shipping code as this like tax_code
              $tax_code = $shipcode;
              $line_title = $line_item->line_item_label;
            }
            else {
              // On this case, we need to load the product to get the avatax tax code
              $product_field = field_get_items('commerce_line_item', $line_item, 'commerce_product');
              $product_id = $product_field[0]['product_id'];
              $prod_data = commerce_product_load($product_id);
              $avatax_code_field_value = field_get_items('commerce_product', $prod_data, $tax_field_name);
              $line_title = $line_item_wrapper->commerce_product->title->value();
            }
            if ($avatax_code_field_value) {
              $tid = $avatax_code_field_value[0]['tid'];
              $taxonomy_term = taxonomy_term_load($tid);
              $tax_code = $taxonomy_term->name;
            }
          }

          $avatax_line_item_label = substr($line_item->line_item_label, 0, 49) . '-REFUND';
          // note that this block of code is one of the modifications made to the original function
          // we needed to control the way that discounts were spreaded over the lines,
          // so we added the field_apply_discount_avatax field to all products (see install file)
          $apply_discount = true;
          try {
            if (!$line_item_wrapper->commerce_product->field_apply_discount_avatax->value()) {
              $apply_discount = false;
            }
          } catch (EntityMetadataWrapperException $e) {
            // if the field is not defined, we do the default behaviour, that is to apply the discount
          }

          // Add negative amount
          if ($refund_amount > 0) {
            $lines[] = array(
              'LineNo' => $i,
              'ItemCode' => $avatax_line_item_label,
              'Description' => "REFUND FOR: " . $line_title,
              'TaxCode' => $tax_code,
              'Qty' => $line_item->quantity,
              'Amount' => -1 * $refund_amount,

              // determines if avatax will have this line into account when spreading the discount
              'Discounted' => $apply_discount ? 'true' : 'false',

              'Ref1' => '',
              'Ref2' => '',
              'CustomerUsageType' => '',
              'OriginCode' => 0,
              'DestinationCode' => 1,
            );
            $i++;
          }
        }
      }
    }
  }
  elseif ($adjustment_type == RCPAR_ADJUSTMENT_TYPE_REFUND && $amount ){
    $refund_line = array();
    // Build the line for the avatax request
    $refund_line[] = array(
      'LineNo' => 1,
      'ItemCode' => 'GENERAL-REFUND',
      'Description' => "GENERAL-REFUND",
      'TaxCode' => "NT",
      'Qty' => 1,
      'Amount' => $amount,
      // determines if avatax will have this line into account when spreading the discount
      'Discounted' =>  'false',
      'Ref1' => '',
      'Ref2' => '',
      'CustomerUsageType' => '',
      'OriginCode' => 0,
      'DestinationCode' => 1,

    );

    // Check if a cancellation product was selected
    if (isset($adjustment_args['refund_line_amounts']['fiat']) && is_object($adjustment_args['refund_line_amounts']['fiat'])) {

      // Get the cancellation product information
      $fiat_wrapper =  entity_metadata_wrapper('commerce_product', $adjustment_args['refund_line_amounts']['fiat']);

      // Build the line for the avatax request according with
      // the cancellation product
      $refund_line[] = array(
        'LineNo' => 2,
        'ItemCode' => 'CANCELLATION-FEE',
        'Description' => "CANCELLATION-FEE",
        'TaxCode' => $fiat_wrapper->avatax_code->name->value(),
        'Qty' => 1,
        'Amount' => $fiat_wrapper->commerce_price->amount->value() * 0.01,
        // determines if avatax will have this line into account when spreading the discount
        'Discounted' => 'false',
        'Ref1' => '',
        'Ref2' => '',
        'CustomerUsageType' => '',
        'OriginCode' => 0,
        'DestinationCode' => 1,
      );
    }

    // Ideally, we would want to return the corresponding taxes all the times we do a refund
    // however, if it's a partial refund and the refunded lines are not specified, we just can't
    // know how much taxes should be applied to the partial refund
    // the only case on which we can actually know how much taxes we should refund, it's when the
    // full order is refunded
    if ($order_wrapper->commerce_order_total->amount->value() == $amount * -100) {
      // When the full order is refunded, we override the taxes and set the correct amount
      // otherwise, avatax would calculate it as 0, since we are sending a NT code for this line

      // Let's find the order avatax assigned tax
      $price_components = $order_wrapper->commerce_order_total->data->value();
      $tax_amount = NULL;
      foreach ($price_components['components'] as $component) {
        if ($component['name'] == 'avatax') {
          $tax_amount = $component['price']['amount'];
        }
      }
      if ($tax_amount) {
        // Tax amount is a commerce amount value, we need to divide it by 100 to get the actual amount
        // also, as we want a discount line, we multiplicate it by -1
        $refund_line['TaxOverride'] = array(
          'TaxAmount'       => $tax_amount / -100,
          'Reason'          => t('Returned taxes'),
          'TaxOverrideType' => 'TaxAmount',
        );
      }
    }
    $lines = $refund_line;

  }elseif ($adjustment_type == RCPAR_ADJUSTMENT_TYPE_PAYMENT  ){
    $line = array(
      'LineNo' => 1,
      'ItemCode' => 'PAYMENT-ADDED',
      'Description' => "PAYMENT-ADDED",
      'TaxCode' => "NT",
      'Qty' => 1,
      'Amount' => $amount,
      'Discounted' =>  'false',
      'Ref1' => '',
      'Ref2' => '',
      'CustomerUsageType' => '',
      'OriginCode' => 0,
      'DestinationCode' => 1,
    );


    if  ( $adjustment_args['additional_shipping'] ) {
      $line['ItemCode'] =  'PAYMENT-ADDED(Shipping)';
      $line['Description'] = 'Shipping';
      $line['TaxCode'] = $ava_args['shipcode'];
    }

    $lines[] = $line;

  }

  $request_body['Lines'] = $lines;
  $response = commerce_avatax_post('/tax/get', $request_body);

  if (is_array($response) && $response['body']) {
    $ava_result = $response['body'];
    if ($ava_result['ResultCode'] == 'Success') {
      return $ava_result;
    }
    else {
      foreach ($ava_result['Messages'] as $msg) {
        $msg_text = t('AvaTax error: %msg - %source - %summary', array(
          '%msg' => $msg['Severity'],
          '%source' => $msg['Source'],
          '%summary' => $msg['Summary']
        ));
        drupal_set_message($msg_text, 'error');
        watchdog('rcpar_avatax', $msg);
      }
      return FALSE;
    }
  }
  else {
    $msg_text = t("AvaTax did not get a response.") ;
    drupal_set_message($msg_text, 'error');
    watchdog('rcpar_avatax', $msg_text) ;
    return FALSE;
  }
}


/**
 * @param $order
 * @param int $refund_type
 * @param array $refund_line_amounts
 * @param $general_refund_amount Positive number to refund
 * @param null $transaction
 */
function rcpar_avatax_refund_order($order, $refund_type = RCPAR_REFUND_TYPE_GENERAL, $refund_line_amounts = array(), $general_refund_amount, $transaction = null ) {
  $adjustment_args = array(
    'refund_type' => $refund_type,
    'refund_line_amounts' => $refund_line_amounts,
  );
  $doc_code_suffix = 'refund-'.$transaction->transaction_id ;
  if ( _rcpar_avatax_adjustment($order, RCPAR_ADJUSTMENT_TYPE_REFUND, $adjustment_args, -1 * $general_refund_amount, $doc_code_suffix ) ) {
    $log = _rcpar_avatax_format_adjustment_message(RCPAR_ADJUSTMENT_TYPE_REFUND, $adjustment_args) ;
    rcpar_avatax_add_transaction_rcpar_info($transaction, $log);
  }
}


function rcpar_avatax_add_payment($order, $amount, $transaction,  $additional_shipping) {
  $adjustment_args = array();
  if ($additional_shipping){
    $adjustment_args['additional_shipping'] = true ;
  }
  $suffix=$transaction->transaction_id;
  $doc_code_suffix = 'payment-'.$suffix;
  if ( $result =  _rcpar_avatax_adjustment($order, RCPAR_ADJUSTMENT_TYPE_PAYMENT, $adjustment_args,  $amount, $doc_code_suffix ) ){
    $log = _rcpar_avatax_format_adjustment_message(RCPAR_ADJUSTMENT_TYPE_PAYMENT, $adjustment_args) ;
    rcpar_avatax_add_transaction_rcpar_info($transaction, $log);
  }
  return $result ;
  
}
