<?php

/**
 * Creates a batch process based on results of the api
 */
function rcpar_avatax_process_cache() {
  $no_cancelled_filter = "status ne Cancelled";
  //get first transaction of the list from avatax
  $avatax = rcpar_avatax_object();
  $company_code = commerce_avatax_company_code();    
  $transaction = array_shift($avatax->listTransactionsByCompany($company_code, $no_cancelled_filter, 'id ASC'));

  if (is_array($transaction)) {
    //we clean the table
    db_truncate("avatax_cache")->execute();

    $unixdate = strtotime($transaction['date']);
    while ($unixdate <= REQUEST_TIME) {
      $date = date('Y-m-d', $unixdate);

      $operations[] = array(
        'rcpar_avatax_process_cache_operation_api',
        array(
          'filter' => "date eq '{$date}T00:00:00' and {$no_cancelled_filter}",
        )
      );

      $unixdate = strtotime("+1 day {$date}");
    }

    $batch = array(
      'operations' => $operations,
      'finished' => 'rcpar_avatax_process_cache_finish',
      'title' => t('Processing Transactions'),
      'init_message' => t('Starting Cache of avatax.'),
      'progress_message' => t('Processed @current out of @total Transactions.'),
      'error_message' => t('No Transaction was imported'),
      'file' => drupal_get_path('module', 'rcpar_avatax') . '/includes/rcpar_avatax_batch.inc',
    );
    batch_set($batch);
    batch_process('/admin/commerce/reports/avatax-discrepancies');
  }
}

function rcpar_avatax_process_cache_operation_api($filter, &$context) {
  //list of transactions comming from avatax based on filter
  $avatax = rcpar_avatax_object();
  $company_code = commerce_avatax_company_code();    
  $transactions = $avatax->listTransactionsByCompany($company_code, $filter, '');  
  if (count($transactions)) {
    $list = isset($context['results']['list']) ? $context['results']['list'] : array();
    $list = rcpar_avatax_batch_insert_transactions($transactions, $list);
    $context['results']['list'] = $list;
  }
}

/**
 * Batch 'finished' callback
 */
function rcpar_avatax_process_cache_finish($success, $results, $operations) {
  if ($success) {
    variable_set('avatax_cache_timestamp', date('F d, Y', REQUEST_TIME));
    drupal_set_message("Cache Process Complete Succesfully");
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(
        t('An error occurred while processing @operation with arguments : @args', array(
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
            )
        ), 'error'
    );
  }
}

/**
 * Get line item tax amount
 * @param $order_id number of the order
 * @return difference amount between avatax and local order or NULL
 */
function rcpar_avatax_get_line_item_amount($order_id) {
  $amount = NULL;
  try {
    $query = db_select('commerce_line_item', 'li');
    $query->fields('li', array('line_item_id'));
    $query->join('field_data_commerce_line_items', 'fli', 'fli.commerce_line_items_line_item_id = li.line_item_id');
    $query->condition('type', 'avatax');
    $query->condition('order_id', $order_id);
    $result = $query->execute();

    if ($li_id = $result->fetchObject()) {
      $lineitem = commerce_line_item_load($li_id->line_item_id);
      $line_wrapper = entity_metadata_wrapper('commerce_line_item', $lineitem);
      $price = $line_wrapper->commerce_unit_price->value();
      $amount = (float) str_replace('$', '', commerce_currency_format($price['amount'], $price['currency_code']));
    }
  }
  catch (Exception $ex) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . "ERROR: " . $ex->getMessage());
  }

  return $amount;
}

/**
 * Insert transactions into avatax_cache table
 * @param $transactions list of transactions retrieve from the api
 * @param $processed_list list of already processed items, to avoid duplicates
 * @return updated processed list to avoid duplicates
 */
function rcpar_avatax_batch_insert_transactions($transactions, $processed_list) {
  $insert = db_insert('avatax_cache')->fields(array('order_id', 'avatax_date', 'avatax_amount'));
  //list of transactions to update
  $update_list = array();
  foreach ($transactions as $transaction) {
    if (strstr($transaction['code'], 'dc-')) {
      $order_id = str_replace('dc-', '', $transaction['code']);
      if (is_numeric($order_id)) {
        $amount = rcpar_avatax_get_line_item_amount($order_id);        
        if (!is_null($amount) && $amount != $transaction['totalTax']) {
          if (in_array($order_id, $processed_list)) {
            db_update('avatax_cache')
                ->fields(array(
                  'avatax_date' => strtotime($transaction['date']),
                  'avatax_amount' => $transaction['totalTax']                  
                ))
                ->condition('order_id',$order_id)
                ->execute();
          } else {
            $processed_list[$order_id] = $order_id;
            $insert->values(array(
              'order_id' => $order_id,
              'avatax_date' => strtotime($transaction['date']),
              'avatax_amount' => $transaction['totalTax']              
            ));
          }
        }
      }
      elseif (strstr($transaction['code'], 'refund')){
       //we flagged the cache table as order refunded 
       $update_list[] = $transaction['code'];       
      }
    }
  }
  $insert->execute();

  //this update happened after the insert cause there is a chance that on 
  //last processed transactions are included also some refunds.
  rcpar_avatax_batch_refund($update_list);
  return $processed_list;
}

/**
 * Set an order as refunded
 * @param $update_list list of avatax code refund to process the order
 * TODO: we have to use at some point api v2 to update transactions instead
 * of creating a new one for refunding.
 */
function rcpar_avatax_batch_refund($update_list) {
  $order_ids = array();
  foreach ($update_list as $avatax_code) {
    $data = explode('-', $avatax_code);
    $order_id = trim($data[1]);
    if (is_numeric($order_id)) {
      $order_ids[$order_id] = $order_id;
    }
  }
  
  if (count($order_ids)) {
    db_update('avatax_cache')
        ->fields(array(
          'refunded' => 1,
        ))
        ->condition('order_id', $order_ids)
        ->execute();
  }
}

/**
 * Adding versioning of the line item to the query of the view
 */
function rcpar_avatax_views_query_alter(&$view, &$query) {
  if ($view->name == 'avatax_drupal_tax_discrepancies') {
    $join = new views_join();
    $join->table = 'field_data_commerce_line_items';
    $join->field = 'commerce_line_items_line_item_id';
    $join->left_table = 'commerce_line_item';
    $join->left_field = 'line_item_id';
    $join->type = 'INNER';
    $query->add_relationship('field_data_commerce_line_items', $join, 'commerce_line_item');
  }
}
