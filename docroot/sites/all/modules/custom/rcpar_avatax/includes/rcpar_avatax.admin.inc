<?php

/**
 * Configuration form for log file on avatax
 */
function rcpar_avatax_rcpar_log_form($form, &$form_state) {
  $form['rcpar_avatax_log_file'] = array(
    '#type' => 'textfield',
    '#title' => t('Log file path'),
    '#description' => t('Path to the file that\'s going to be used to store Avatax transactions log'),
    '#default_value' => variable_get('rcpar_avatax_log_file'),
  );
  $form['rcpar_avatax_enable_logging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable logging'),
    '#default_value' => variable_get('rcpar_avatax_enable_logging'),
  );
  
  $form['#validate'][] = 'rcpar_avatax_config_form_validate';

  return system_settings_form($form);
}

/**
 * validates if the path provided is writable
 */
function rcpar_avatax_config_form_validate($form, &$form_state){
  // If user enabled logging, we need to check that we have access to the file he is trying to set
  if ($form_state['values']['rcpar_avatax_enable_logging']){
    $file_path = $form_state['values']['rcpar_avatax_log_file'];
    if(!is_writable($file_path)){
      form_set_error('rcpar_avatax_log_file', t('Provided file path is not writable by Drupal.'));
    }
  }
}