<?php

/**
 * Implements hook_rules_action_info().
 */
function rcpar_avatax_rules_action_info() {
  $actions = array(
    'rcpar_avatax_cancel_transaction' => array(
      'label' => t('Change status of sales tax to VOIDED in AvaTax or create negative transaction if locked'),
      'group' => t('RCPAR AvaTax'),
      'parameter' => _commerce_avatax_rules_order_parameter_info(true),
    ),
    'rcpar_avatax_commit_transaction'=> array(
      'label' => t('RCPAR Change status of sales tax to COMMITTED in AvaTax'),
      'group' => t('RCPAR AvaTax'),
      'parameter' => _commerce_avatax_rules_order_parameter_info(true),
    )
  );

  return $actions;
}


/**
 * @param $order
 * @return bool
 * Code based on rcpar_avatax_retrieve_sales_tax
 */
function rcpar_avatax_void_locked_transaction($order) {
    rcpar_avatax_log('sending (rcpar_avatax_void_locked_transaction): '. print_r($request_body, true) , 'notice', $order->order_id);
    $order->refund = TRUE;
    $order_wrapper =  entity_metadata_wrapper('commerce_order', $order);
    commerce_avatax_calculate_tax($order_wrapper);
    rcpar_avatax_log('response (rcpar_avatax_void_locked_transaction): '. print_r($response, true) , 'notice', $order->order_id);
}


/**
 * @param $order
 * Tries to cancel a transaction on avatax.
 * In case of transaction is locked creates a new record with negative value referencing the original product
 */
function rcpar_avatax_cancel_transaction($order) {
  try {
    if (rcpar_avatax_commerce_is_transaction_locked($order)) {
      rcpar_avatax_log("Failed to void order " . $order->order_id, 'error', $order->order_id);
      rcpar_avatax_void_locked_transaction($order);
    } else {
      commerce_avatax_void_transaction($order, 'DocVoided');
    }
  } catch (Exception $ex) {
    rcpar_avatax_log("Failed to void order ".$order->order_id." - AvaTax did not respond ", 'error', $order->order_id);
  }
}

/**
 * COMMIT AvaTax transaction for $order.
 */
/**
 * COMMIT AvaTax transaction for $order.
 */
function rcpar_avatax_commit_transaction($order) {
  watchdog('rcpar_avatax', 'ACTION commit transaction STARTS');
  rcpar_avatax_log("ACTION commit transaction STARTS");  
  if (isset($order->data['commerce_avatax']['transaction_code']) && $avatax = commerce_avatax_object()) {
    // 'rcpar_avatax_uncommited' will be used to avoid regular commit/uncommit behaviour
    if (!empty($order->data['commerce_avatax']['company_code']) && !isset($order->data['rcpar_avatax_uncommited_tag'])) {
      $result = $avatax->transactionsCommit($order->data['commerce_avatax']['company_code'], $order->data['commerce_avatax']['transaction_code']);
    }
  }
}
