<?php

/**
 * Implements hooks_views_default_views
 */
function rcpar_avatax_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'avatax_drupal_tax_discrepancies';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_line_item';
  $view->human_name = 'Avatax - Drupal Tax Discrepancies';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Avatax - Drupal Tax Discrepancies';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access commerce tax reports';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'created' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'commerce_customer_address_administrative_area' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'secondary_label' => NULL,
    'collapsible_label' => NULL,
    'combine_rewrite' => NULL,
    'reset_label' => NULL,
    'bef_filter_description' => NULL,
    'any_label' => NULL,
    'filter_rewrite_values' => NULL,
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 1;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<strong>Process Tax Cache</strong>
<a href="/admin/avatax/process-cache">Here</a>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No Discrepancies Found.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['empty']['area']['tokenize'] = TRUE;
  /* Relationship: Commerce Line Item: Order ID */
  $handler->display->display_options['relationships']['order_id']['id'] = 'order_id';
  $handler->display->display_options['relationships']['order_id']['table'] = 'commerce_line_item';
  $handler->display->display_options['relationships']['order_id']['field'] = 'order_id';
  $handler->display->display_options['relationships']['order_id']['required'] = TRUE;
  /* Relationship: Commerce Order: RCPAR Avatax */
  $handler->display->display_options['relationships']['avatax_amount']['id'] = 'avatax_amount';
  $handler->display->display_options['relationships']['avatax_amount']['table'] = 'commerce_order';
  $handler->display->display_options['relationships']['avatax_amount']['field'] = 'avatax_amount';
  $handler->display->display_options['relationships']['avatax_amount']['relationship'] = 'order_id';
  $handler->display->display_options['relationships']['avatax_amount']['required'] = TRUE;
  /* Relationship: Commerce Order: Referenced customer profile */
  $handler->display->display_options['relationships']['commerce_customer_shipping_profile_id']['id'] = 'commerce_customer_shipping_profile_id';
  $handler->display->display_options['relationships']['commerce_customer_shipping_profile_id']['table'] = 'field_data_commerce_customer_shipping';
  $handler->display->display_options['relationships']['commerce_customer_shipping_profile_id']['field'] = 'commerce_customer_shipping_profile_id';
  $handler->display->display_options['relationships']['commerce_customer_shipping_profile_id']['relationship'] = 'order_id';
  /* Field: Commerce Order: Order ID */
  $handler->display->display_options['fields']['order_id']['id'] = 'order_id';
  $handler->display->display_options['fields']['order_id']['table'] = 'commerce_order';
  $handler->display->display_options['fields']['order_id']['field'] = 'order_id';
  $handler->display->display_options['fields']['order_id']['relationship'] = 'order_id';
  /* Field: Commerce Order: Created date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'commerce_order';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['relationship'] = 'order_id';
  $handler->display->display_options['fields']['created']['label'] = 'Order date';
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'F d, Y';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['created']['format_date_sql'] = 0;
  /* Field: Commerce Line item: Total */
  $handler->display->display_options['fields']['commerce_total']['id'] = 'commerce_total';
  $handler->display->display_options['fields']['commerce_total']['table'] = 'field_data_commerce_total';
  $handler->display->display_options['fields']['commerce_total']['field'] = 'commerce_total';
  $handler->display->display_options['fields']['commerce_total']['label'] = 'Drupal Tax';
  $handler->display->display_options['fields']['commerce_total']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_total']['settings'] = array(
    'calculation' => FALSE,
  );
  /* Field: Rcpar Avatax: Avatax Cache Date */
  $handler->display->display_options['fields']['avatax_date']['id'] = 'avatax_date';
  $handler->display->display_options['fields']['avatax_date']['table'] = 'avatax_cache';
  $handler->display->display_options['fields']['avatax_date']['field'] = 'avatax_date';
  $handler->display->display_options['fields']['avatax_date']['relationship'] = 'avatax_amount';
  $handler->display->display_options['fields']['avatax_date']['label'] = 'Avatax Date';
  $handler->display->display_options['fields']['avatax_date']['date_format'] = 'custom';
  $handler->display->display_options['fields']['avatax_date']['custom_date_format'] = 'F d, Y';
  $handler->display->display_options['fields']['avatax_date']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['avatax_date']['format_date_sql'] = 0;
  /* Field: Rcpar Avatax: Avatax Cache Amount */
  $handler->display->display_options['fields']['avatax_amount_1']['id'] = 'avatax_amount_1';
  $handler->display->display_options['fields']['avatax_amount_1']['table'] = 'avatax_cache';
  $handler->display->display_options['fields']['avatax_amount_1']['field'] = 'avatax_amount';
  $handler->display->display_options['fields']['avatax_amount_1']['relationship'] = 'avatax_amount';
  $handler->display->display_options['fields']['avatax_amount_1']['label'] = 'Avatax Amount';
  $handler->display->display_options['fields']['avatax_amount_1']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['avatax_amount_1']['precision'] = '2';
  $handler->display->display_options['fields']['avatax_amount_1']['prefix'] = '$';
  /* Field: hidden only to calculate */
  $handler->display->display_options['fields']['avatax_amount']['id'] = 'avatax_amount';
  $handler->display->display_options['fields']['avatax_amount']['table'] = 'avatax_cache';
  $handler->display->display_options['fields']['avatax_amount']['field'] = 'avatax_amount';
  $handler->display->display_options['fields']['avatax_amount']['relationship'] = 'avatax_amount';
  $handler->display->display_options['fields']['avatax_amount']['ui_name'] = 'hidden only to calculate';
  $handler->display->display_options['fields']['avatax_amount']['exclude'] = TRUE;
  $handler->display->display_options['fields']['avatax_amount']['separator'] = '';
  /* Field: hidden only to calculate */
  $handler->display->display_options['fields']['commerce_total_1']['id'] = 'commerce_total_1';
  $handler->display->display_options['fields']['commerce_total_1']['table'] = 'field_data_commerce_total';
  $handler->display->display_options['fields']['commerce_total_1']['field'] = 'commerce_total';
  $handler->display->display_options['fields']['commerce_total_1']['ui_name'] = 'hidden only to calculate';
  $handler->display->display_options['fields']['commerce_total_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['commerce_total_1']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_total_1']['type'] = 'commerce_price_raw_amount';
  $handler->display->display_options['fields']['commerce_total_1']['settings'] = array(
    'calculation' => FALSE,
  );
  /* Field: Global: Math expression */
  $handler->display->display_options['fields']['expression']['id'] = 'expression';
  $handler->display->display_options['fields']['expression']['table'] = 'views';
  $handler->display->display_options['fields']['expression']['field'] = 'expression';
  $handler->display->display_options['fields']['expression']['label'] = 'Discrepancy';
  $handler->display->display_options['fields']['expression']['set_precision'] = TRUE;
  $handler->display->display_options['fields']['expression']['precision'] = '2';
  $handler->display->display_options['fields']['expression']['prefix'] = '$';
  $handler->display->display_options['fields']['expression']['expression'] = '([commerce_total_1] / 100) -  [avatax_amount]';
  /* Sort criterion: Commerce Order: Created date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'commerce_order';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['relationship'] = 'order_id';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Commerce Line Item: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'commerce_line_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'avatax' => 'avatax',
  );
  /* Filter criterion: Commerce Line Item: Order ID */
  $handler->display->display_options['filters']['order_id']['id'] = 'order_id';
  $handler->display->display_options['filters']['order_id']['table'] = 'commerce_line_item';
  $handler->display->display_options['filters']['order_id']['field'] = 'order_id';
  $handler->display->display_options['filters']['order_id']['operator'] = 'not empty';
  /* Filter criterion: Commerce Order: Created date */
  $handler->display->display_options['filters']['created']['id'] = 'created';
  $handler->display->display_options['filters']['created']['table'] = 'commerce_order';
  $handler->display->display_options['filters']['created']['field'] = 'created';
  $handler->display->display_options['filters']['created']['relationship'] = 'order_id';
  $handler->display->display_options['filters']['created']['operator'] = 'between';
  $handler->display->display_options['filters']['created']['value']['min'] = '05/01/2016';
  $handler->display->display_options['filters']['created']['exposed'] = TRUE;
  $handler->display->display_options['filters']['created']['expose']['operator_id'] = 'created_op';
  $handler->display->display_options['filters']['created']['expose']['label'] = 'Date Between';
  $handler->display->display_options['filters']['created']['expose']['operator'] = 'created_op';
  $handler->display->display_options['filters']['created']['expose']['identifier'] = 'created';
  $handler->display->display_options['filters']['created']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
  );
  /* Filter criterion: Rcpar Avatax: Avatax Cache Refunded */
  $handler->display->display_options['filters']['refunded']['id'] = 'refunded';
  $handler->display->display_options['filters']['refunded']['table'] = 'avatax_cache';
  $handler->display->display_options['filters']['refunded']['field'] = 'refunded';
  $handler->display->display_options['filters']['refunded']['relationship'] = 'avatax_amount';
  $handler->display->display_options['filters']['refunded']['value'] = '0';
  /* Filter criterion: Commerce Customer profile: Address - Administrative area (i.e. State / Province) */
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['id'] = 'commerce_customer_address_administrative_area';
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['table'] = 'field_data_commerce_customer_address';
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['field'] = 'commerce_customer_address_administrative_area';
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['relationship'] = 'commerce_customer_shipping_profile_id';
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['exposed'] = TRUE;
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['expose']['operator_id'] = 'commerce_customer_address_administrative_area_op';
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['expose']['label'] = 'State';
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['expose']['operator'] = 'commerce_customer_address_administrative_area_op';
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['expose']['identifier'] = 'commerce_customer_address_administrative_area';
  $handler->display->display_options['filters']['commerce_customer_address_administrative_area']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/commerce/reports/avatax-discrepancies';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Avatax - Drupal Tax Discrepancies';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['path'] = 'admin/commerce/reports/avatax-discrepancies.csv';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );



  $views [$view->name] = $view;

  return $views;
}
