<?php

/**
 * Implements hooks_views_data_alter
 */
function rcpar_avatax_views_data_alter(&$data) {


  $data['commerce_order']['avatax_amount']['relationship'] = array(
    'title' => t('RCPAR Avatax'),
    'help' => t("Relate the order with its information on avatax."),
    'handler' => 'views_handler_relationship',
    'base' => 'avatax_cache',
    'base field' => 'order_id',
    'field' => 'order_id',
  );
}

/**
 * Implements hooks_views_data
 */
function rcpar_avatax_views_data() {
  $data = array();


  // Base information.
  $data['avatax_cache']['table']['group'] = t('Rcpar Avatax');

  $data['avatax_cache']['table']['base'] = array(
    'field' => 'order_id',
    'title' => t('RCPAR Avatax cache order id'),
    'help' => t('The order id related to the avatax record'),
  );

  $data['avatax_cache']['avatax_amount'] = array(
    'title' => t('Avatax Cache Amount'),
    'help' => t('The amount that is on avatax related to the order'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['avatax_cache']['avatax_date'] = array(
    'title' => t('Avatax Cache Date'),
    'help' => t('The date the avatax document was created'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['avatax_cache']['refunded'] = array(
    'title' => t('Avatax Cache Refunded'),
    'help' => t('The refunded value for the avatax transactions'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',      
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator'
    )
  );


  return $data;
}


