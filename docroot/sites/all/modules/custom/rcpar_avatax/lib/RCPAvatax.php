<?php

class RCPAvatax extends Avatax {

  /**
   * Retrieve all transactions
   * 
   * @param string $companyCode
   *   The company code of the company that recorded these transactions.
   *
   * @param 
   *   String $filter A filter statement to identify specific records to retrieve
   * 
   * @param 
   *    String $orderBy A comma separated list of sort statements in the format '(fieldname) [ASC|DESC]', for example 'id ASC'.
   * 
   * @param string[] $parameters
   *   An associative array of POST body parameters that should contain the
   *   code (the reason for voiding or cancelling this transaction).
   * 
   * @return array
   *   An array with records returned by the api. FALSE if no success
   */
  public function listTransactionsByCompany($companyCode, $filter = '', $orderBy = 'date ASC', $parameters = array()) {
    $queries = array('$filter' => $filter, '$orderBy' => $orderBy);
    $querystring = http_build_query($queries);
    $querystring = str_replace("amp;", "", $querystring);
    $result =  $this->doRequest('GET', "companies/$companyCode/transactions?$querystring", $parameters);
    return ($result['success']) && @count($result['result']['value']) ? $result['result']['value'] : FALSE;
  }
  
  /**
   * @param string $companyCode
   *   The company code of the company that recorded these transactions.
   *
   * @param $docid
   *  the document transaccion id
   * 
   * 
   * @return of transaction values
   */
  public function transactionsGet($companyCode, $docid) {
    $result =  $this->doRequest('GET', "companies/$companyCode/transactions/$docid?".'$include=details');
    return ($result['success']) && @count($result['result']) ? $result['result'] : FALSE;
  }

}
