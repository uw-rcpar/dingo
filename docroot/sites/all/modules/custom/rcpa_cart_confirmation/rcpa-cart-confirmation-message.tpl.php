<?php extract($variables) ?>
<div class="message-inner">
  <div class="added-product-title clearfix"><?php echo t('Item successfully added to your cart') ?></div>
  <div class="button-wrapper">
    <div class="button checkout"><?php echo l(t('Go to checkout'), 'cart') ?></div>
    <div class="button continue"><span class="commerce-add-to-cart-confirmation-close"><?php echo t('Continue shopping') ?></span></div>
  </div>
  <?php echo views_embed_view('confirm_message_product_display', 'default', $line_item_ids); ?>
  <a class="commerce-add-to-cart-confirmation-close" href="#">
    <span class="element-invisible"><?php echo t('Close') ?></span>
  </a>
</div>
