<?php

function rcpa_cart_confirmation_rules_action_info_alter(&$actions){
  if(isset($actions['commerce_add_to_cart_confirmation_message'])){
    $actions['commerce_add_to_cart_confirmation_message']['callbacks']['execute'] = 'rcpa_cart_confirmation_add_to_cart_message';
  }
}
