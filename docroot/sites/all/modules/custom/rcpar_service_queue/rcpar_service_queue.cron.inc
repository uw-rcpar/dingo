<?php

/**
 * Run the lightweight cron.
 *
 * This function is called from the external crontab job via url /rcpar_service_queue/cron
 * or it can be run interactively from the Scheduler configuration page at
 * /admin/settings/rcpar/rcpar_service_queue.
 */
function rcpar_service_queue_run_cron() {
  // Log this transaction as a background job in New Relic
  if (extension_loaded('newrelic')) { // Ensure PHP agent is available
    if (function_exists('newrelic_background_job')) {
      try {
        newrelic_background_job();
      }
      catch (Exception $e) {}
    }

  }

  $log = variable_get('rcpar_service_queue_cron_log', 1);
  if ($log) {
    watchdog('rcpar_service_queue', 'Lightweight cron run activated', array(), WATCHDOG_NOTICE);
  }

  rcpar_service_queue_cron_light();

  if ($log) {
    watchdog('rcpar_service_queue', 'Lightweight cron run completed', array(), WATCHDOG_NOTICE, l(t('settings'), 'admin/config/content/scheduler/cron'));
  }

  $menu_item = menu_get_item();
  if ($menu_item['path'] == 'admin/settings/rcpar/rcpar_service_queue') {
    // This cron run has been initiated manually from the configuration form.
    // Give a message and return something so that an output page is created.
    // No output should be returned if running from a crontab job.
    if (module_exists('dblog')) {
      drupal_set_message(t('Lightweight cron run completed - see <a href="@url">log</a> for details.', array('@url' => url('admin/reports/dblog'))));
    }
    else {
      drupal_set_message(t('Lightweight cron run completed.'));
    }
    return ' ';
  }
  // drupal_exit() is the proper controlled way to terminate the request, as
  // this will invoke all implementations of hook_exit().
  drupal_exit();
}

/**
 * Here's where we process the queued items and update their statuses
 */
function rcpar_service_queue_cron_light() {
  $log = variable_get('rcpar_service_queue_cron_log', 1);
  if ($log) {
    watchdog('rcpar_service_queue', 'service queue batch started', array(), WATCHDOG_NOTICE);
  }

  // We want to control the overall execution time of this batches, since we don't want the successive calls pile up
  // to do that, we defined a max exec time on the config and here we prevent the script to run more often that that
  // (and we try to stop previous runs to take longer than that interval)
  $interval = variable_get('rcpar_service_queue_lightweight_cron_interval', 60);
  $t0 = time();

  // We want to prevent the script to run more often than specified on the settings
  $next_batch_after = variable_get('rcpar_service_queue_light_cron_last_run', 0);
  $next_batch_after = $next_batch_after + $interval;
  if ($next_batch_after > REQUEST_TIME) {
    if ($log) {
      watchdog('rcpar_service_queue', 'an rcpar service queue light batch cron was attempted to run before it was time', array(), WATCHDOG_NOTICE);
    }
    return;
  }
  variable_set('rcpar_service_queue_light_cron_last_run', REQUEST_TIME);

  // Let's pick a batch of RCPAR_SERVICE_QUEUE_MAX_ELEMENTS_PER_BATCH elements
  // (we might not have the time to run all of them on this batch, but we will run as much as possible)

  $qry = db_select('rcpar_service_queue_items', 'items');
  $qry->fields('items');
  $qry->condition('processed', 0);
  $qry->orderBy('created', 'ASC');
  $qry->range(0, variable_get('rcpar_service_queue_max_items_per_batch', 200));

  $items = $qry->execute();
  foreach ($items as $key => $item) {
    // es events case
    if($item->service_name == 'es_events'){
      $params = json_decode($item->data, TRUE);
      module_load_include('inc', 'es_events');
      $client = es_events_es_client();
      $ret = $client->index($params);
      $ret_code = $ret['_id'];
    }
    else {
    // http request case
      $data = json_decode($item->data, TRUE);
      $ret = drupal_http_request($item->url, $data);
      $ret_code = $ret->code;
      if ($item->callback) {
        $callback_func = $item->callback;
        $callback_func($ret);
      }
    }

    // Previously, we would update updates with 'processed = 1' and leave them in the log for a while, but
    // the number of items became overwhelming in even a short period with more elastic search events being
    // logged.  There is not much value to storing them in the table, and we can log them in watchdog with
    // the module settings if we want, so we will delete them directly now.
    db_query('DELETE FROM {rcpar_service_queue_items} WHERE item_id = :id', array(':id' => $item->item_id));
    /*db_update('rcpar_service_queue_items')->fields(array(
      'processed' => 1,
      'executed'  => format_date(time(), 'custom', 'Y-m-d H:i:s'),
      'response'  => json_encode($ret),
    ))
      ->condition('item_id', $item->item_id, '=')
      ->execute();*/

    if ($log) {
      $message = 'call to @url returned with code @ret_code - item ID: @item_id - service name: @module';
      $vars = array(
        '@url'      => $item->url,
        '@ret_code' => $ret_code,
        '@item_id'  => $item->item_id,
        '@module'   => $item->service_name,
      );
      watchdog('rcpar_service_queue', $message, $vars, WATCHDOG_NOTICE);
    }

    // This is taking too long, let's stop and wait until the next batch starts to continue processing
    if ($t0 + $interval < time()) {
      break;
    }
  }

  if ($log) {
    watchdog('rcpar_service_queue', 'service queue batch finished', array(), WATCHDOG_NOTICE);
  }
}
