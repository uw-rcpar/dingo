<?php

/**
 * Administration form that appears on admin/settings/rcpar/rcpar_service_queue page
 * @param $form
 * @param $form_state
 * @return mixed
 */
function rcpar_service_queue_admin_form($form, &$form_state) {
  $form = array();

  $form['link'] = array('#markup' => l(t('Go to queued items'), 'admin/settings/rcpar/rcpar_service_queue/list'));

  $form['rcpar_service_queue_cron'] = array(
    '#type'   => 'submit',
    '#prefix' => '<div class="container-inline">' . t("You can run rcpar service queue cron process manually by clicking on the button") . ': ',
    '#value'  => t("Execute a batch of queued items"),
    '#submit' => array('rcpar_service_queue_cron_submit'),
    '#suffix' => "</div>\n",
  );

  $form['rcpar_service_queue_cron_log'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable logging.'),
    '#default_value' => variable_get('rcpar_service_queue_cron_log', 1),
    '#description'   => t("When this option is checked, the module will write an entry to the log every time the lightweight cron process is started and completed and when a request status is updated."),
  );

  $form['rcpar_service_queue_cron_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Lightweight cron settings'),
  );

  $form['rcpar_service_queue_cron_settings']['rcpar_service_queue_lightweight_cron_interval'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Minimum interval between runs'),
    '#size'          => 100,
    '#default_value' => variable_get('rcpar_service_queue_lightweight_cron_interval', '60'),
    '#description'   => t("Sets the minimun interval between runs (it will prevent the batches from running more more often than this).
      <br> This is also the maximum amount of time a batch is allowed to run.
      <br> <strong>IMPORTANT:</strong> requires the cron job to be configured to run more often that this"),
  );

  $form['rcpar_service_queue_cron_settings']['rcpar_service_queue_max_items_per_batch'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Max items per batch'),
    '#size'          => 10,
    '#default_value' => variable_get('rcpar_service_queue_max_items_per_batch', 200),
    '#description'   => t("The maximum number of items that can be processed per batch. Set to the maximum that can be safely loaded into memory.
     The cron interval time will still limit the duration of time that an individual batch can run."),
  );

  $form['rcpar_service_queue_cron_settings']['rcpar_service_queue_lightweight_cron_access_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Lightweight cron access key'),
    '#size'          => 100,
    '#default_value' => variable_get('rcpar_service_queue_lightweight_cron_access_key', ''),
    '#description'   => t("Similar to Drupal's cron key this acts as a security token to prevent unauthorised calls to scheduler/cron. The key should be passed as rcpar_service_queue/cron/&lt;this key&gt;. To disable security for lightweight cron leave this field blank."),
  );
  if (isset($form_state['rcpar_service_queue_generate_new_key'])) {
    $new_access_key = substr(md5(rand()), 0, 20);
    $form_state['input']['rcpar_service_queue_lightweight_cron_access_key'] = $new_access_key;
    drupal_set_message(t('A new random key has been generated but not saved. If you wish to use this, first "Save Configuration" to store the value, then modify your crontab job.'), 'warning');
  }
  $form['rcpar_service_queue_cron_settings']['create_key'] = array(
    '#type'   => 'submit',
    '#value'  => t('Generate new random key'),
    '#submit' => array('rcpar_service_queue_generate_key'),
  );
  return system_settings_form($form);
}

/**
 * Form submission handler for rcpar_service_queue_cron().
 *
 * This only fires when the 'Generate new random key' button is clicked.
 */
function rcpar_service_queue_generate_key($form, &$form_state) {
  $form_state['rcpar_service_queue_generate_new_key'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Form submission handler for rcpar_service_queue_cron() (Execute batch of queued items).
 */
function rcpar_service_queue_cron_submit() {
  // Load the cron functions file.
  module_load_include('inc', 'rcpar_service_queue', 'rcpar_service_queue.cron');
  rcpar_service_queue_run_cron();
}

/**
 * Administrative form that appears on admin/settings/rcpar/rcpar_service_queue/list
 * @param $form
 * @param $form_state
 * @return array
 */
function rcpar_service_queue_list_admin_form($form, &$form_state) {
  $form = array();

  $form['link'] = array('#markup' => l(t('Go to module configuration'), 'admin/settings/rcpar/rcpar_service_queue'));

  // This form is basically a table that shows the queued element with pagination and filters

  $form['filters'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Filters'),
  );

  $processed = isset($form_state['filter_processed']) ? $form_state['filter_processed'] : 0;
  $form['filters']['processed'] = array(
    '#type'          => 'select',
    '#title'         => t('Processed'),
    '#options'       => array(0 => 'No', 1 => 'Yes'),
    '#default_value' => $processed,
    '#required'      => TRUE,
  );
  $form['filters']['apply_filters'] = array(
    '#type'   => 'submit',
    '#value'  => t("Apply"),
    '#submit' => array('rcpar_service_queue_list_admin_form_filters_submit'),
  );
  $qry = db_select('rcpar_service_queue_items', 'items');
  $qry->fields('items');
  $qry->orderBy('created', 'ASC');
  $qry->condition('processed', $processed, '=');

  // Adding pager to the query
  $qry = $qry->extend('TableSort')->extend('PagerDefault')->limit(20);

  $items = $qry->execute();

  // Now, we create the elements table
  $form['table'] = array(
    '#theme'  => 'table',
    '#header' => array(t('id'), t('Created'), t('Service Name'), t('Url'), t('Data'), t('Processed')),
    'rows'    => array(),
  );

  foreach ($items as $key => $result) {
    $form['table']["#rows"]["'r$key'"] = array(
      'c1' => array('data' => array('#type' => 'item', '#markup' => $result->item_id)),
      'c2' => array('data' => array('#type' => 'item', '#markup' => $result->created)),
      'c3' => array('data' => array('#type' => 'item', '#markup' => $result->service_name)),
      'c4' => array('data' => array('#type' => 'item', '#markup' => $result->url)),
      'c5' => array('data' => array('#type' => 'item', '#markup' => $result->data)),
      'c6' => array('data' => array('#type' => 'item', '#markup' => $result->executed)),
    );
  }

  $form['pager'] = array(
    '#markup' => theme('pager'),
  );

  return $form;
}

/**
 * Submit function for the filters button
 * @param $form
 * @param $form_state
 */
function rcpar_service_queue_list_admin_form_filters_submit($form, &$form_state) {
  $form_state['filter_processed'] = $form_state['values']['processed'];
  $form_state['rebuild'] = TRUE;
}
