<?php
/*
 * @file
 * Referral Program module provides core referral functionality.
 */


/**
 * Implements hook_menu().
 * @return array
 */
function rcpar_referral_program_menu() {
  $items = array();

  // Referral Link
  $items['referral/%'] = array(//i am removing parameter user since this will not match with uworld ids
    'title' => t('Referral Program Entry'),
    'page callback' => 'rcpar_referral_program_get_redirect',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
  );
  // Admin page to alter block copy
  $items['admin/settings/rcpar/referrals/settings'] = array(
    'title' => 'Referral Program Block Content Settings',
    'description' => 'Gives admins the ability to edit the referral block copy',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rcpar_referral_program_block_settings'),
    'access arguments' => array('administer blocks'),
    'file' => 'includes/rcpar_referral_program.admin.inc',
  );

  return $items;
}


/**
 * Redirect after getting referral program data by way of url.
 * @param $referrer comes as an integer, is the uWorld id on the user object
 */
function rcpar_referral_program_get_redirect($referrer) {
  
  // Add validation for approval to use referral
  if (!empty($referrer)) {
    // Set referrer data in session
    rcpar_referral_program_set_cookie($referrer);
    // Debug cookie:
    // drupal_set_message(t('Referral cookie has been set.'), 'warning', false);
  }
  else {
    rcpar_referral_program_remove_cookie();
  }

  // If they are logged in, pass them to referral landing page.
  drupal_goto('cpa-courses', array(
      'query' => array(
        'utm_source' => 'referral_block',
        'utm_medium' => 'referral',
        'utm_term' => $referrer,
      )
    )
  );

}


/**
 * Implements hook_block_info().
 * This hook declares what blocks are provided by the module.
 * @return array
 */
function rcpar_referral_program_block_info() {
  $blocks = array();
  $blocks['rcpar_referral_program_user_links'] = array(
    'info' => t('Referral Program'),
  );
  return $blocks;
}


/**
 * Implements hook_block_view().
 * This hook generates the contents of the blocks themselves.
 * @param string $delta
 * @return array
 */
function rcpar_referral_program_block_view($delta = '') {
  global $user;
  $uworld_id = FALSE;

  $block = array();
  switch ($delta) {
    case 'rcpar_referral_program_user_links':
      //validate only if switch is on
      if (variable_get('enable_uworld_configurations', FALSE)) {
        try {
          $account = user_load($user->uid);
          $uworld_id = entity_metadata_wrapper('user', $account)->field_uwuid->value();
        }
        catch (Exception $ex) {
          watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . 'Error: ' . $ex->getMessage());
        }


        if (!$uworld_id) {
          //if no field exists with uworld info we do not display the block at all
          return $block;
        }
      }

      if (variable_get('rcpar_referral_program_block_title', '')) {
        $block['subject'] = t(variable_get('rcpar_referral_program_block_title', ''));
      }
      else {
        $block['subject'] = t('Referral Program');
      }
      $block['content'] = array(
        '#markup' => rcpar_referral_program_user_links($uworld_id),
        '#attached' => array(
          'css' => array(
            drupal_get_path('module', 'rcpar_referral_program') . '/css/referral.css',
          ),
          'js' => array(
            drupal_get_path('module', 'rcpar_referral_program') . '/js/referral.js',
          ),
        ),
      );

      break;
  }
  return $block;
}

/**
 * @param $uworld_id
 *   - The uworld id info comming from the user object
 * 
 * Generate content for referral program block.
 * @return string
 */
function rcpar_referral_program_user_links($uworld_id = FALSE) {
  global $user;

  $output = '';
  $referral_id = $uworld_id ? $uworld_id : $user->uid;
  $referral_url = $uworld_id ? variable_get('uworld_tracking_pixel_url', 'http://uworld.com/productreferral.aspx') . "?uid={$referral_id}" : url('referral/' . $referral_id, array('absolute' => TRUE, 'alias' => FALSE));
  $link_form = '<textarea type="text" readonly="readonly" id="referral-link-box" value="' . $referral_url . '">' . $referral_url . '</textarea>';

  if (variable_get('rcpar_referral_program_block_copy_link_text', '')) {
    $copy_link = '<a id="get-referral-link" class="btn-primary copy-link mkgtd-referral-button">' . variable_get('rcpar_referral_program_block_copy_link_text', '') . '</a>';
  }
  else {
    $copy_link = '<a id="get-referral-link" class="btn-primary copy-link mkgtd-referral-button">Copy Link</a>';
  }
  if (variable_get('rcpar_referral_program_block_copy', '')) {
    $referral_copy = '<div class="referral-copy">' . variable_get('rcpar_referral_program_block_copy', '') . '</div>';
  }
  else {
    $referral_copy = '<div class="referral-copy">Do you know someone considering a CPA? Copy this link to put them on the SmartPath.</div>';
  }

  $output .= t('<div class="block-wrapper-outer">
    <div class="block-wrapper">
      <div class="block-wrapper-inner">'
     . $referral_copy . $link_form . $copy_link .
     '</div>
    </div>');

  return $output;
}


/**
 * Set referral cookie data.
 * @param null $referrer
 */
function rcpar_referral_program_set_cookie($referrer = NULL) {
  
  if (isset($referrer)) {
    setcookie('referral', $referrer, time() + 60 * 60 * 24 * 30, '/');

    // If we have a ?destination= set the cookie.
    $destination = drupal_get_destination();
    // If there is no destination querystring, the destination will be referral/<uid>, and $destination should be set to NULL.
    $destination = ($destination['destination'] == 'referral/' . $referrer) ?  $destination = NULL : $destination ;
    // If destination is not NULL, set the cookie.
    if($destination) {
      setcookie('referral_destination', $destination['destination'], time() + 60 * 60 * 24 * 30, '/');
    }
  }
}


/**
 * Remove referral cookie data.
 */
function rcpar_referral_program_remove_cookie() {
  setcookie('referral', '', time() - 3600, '/');
  unset ($_COOKIE['referral']);
}

/**
 * Remove referral destination cookie data.
 */
function rcpar_referral_program_remove_destination_cookie() {
  setcookie('referral_destination', '', time() - 3600, '/');
  unset ($_COOKIE['referral_destination']);
}

/**
 * Get referral cookie data.
 * @return bool
 */
function rcpar_referral_program_get_cookie() {
  if (isset($_COOKIE['referral'])) {
    return $_COOKIE['referral'];
  }
  else {
    return FALSE;
  }
}

/**
 * Get referral destination cookie data.
 * @return bool
 */
function rcpar_referral_program_get_destination_cookie() {
  if (isset($_COOKIE['referral_destination'])) {
    return $_COOKIE['referral_destination'];
  }
  else {
    return NULL;
  }
}

/**
 * Return the referrer object by uid.
 * @param null $uid
 * @return mixed|void
 */
function rcpar_referral_program_get_referrer_by_uid($uid = NULL) {
  // Do nothing if no value present
  if ($uid == NULL) {
    return;
  }
  // Otherwise return the referrer's user object
  $referrer = user_load($uid);
  return $referrer;
}

/**
 * Implements hook_commerce_checkout_complete().
*/
function rcpar_referral_program_commerce_checkout_complete($order) {
  // If cookie exists, we want to log the order in the database.
  // However the purchaser's user id isn't available in this hook,
  // so we're registering a drupal shutdown function, which will add
  // the order to the database.
  if (rcpar_referral_program_get_cookie()) {
    drupal_register_shutdown_function('rcpar_referral_program_log_referral_order', $order);
  }
}

/**
 * A function to log a referred order in database.
 * Called via a Drupal register shutdown function in
 * commerce_checkout_complete() hook.
 */
function rcpar_referral_program_log_referral_order($order) {

  try {
    $user = user_load($order->uid);
    $uworld_id = entity_metadata_wrapper('user', $user)->field_uwuid->value();
  }
  catch (Exception $ex) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . 'Error: ' . $ex->getMessage());
  }

  db_insert('rcpar_referral_program')
      ->fields(array(
        'uid' => $uworld_id,
        'referring_uid' => rcpar_referral_program_get_cookie(),
        'referred_destination' => rcpar_referral_program_get_destination_cookie(),
        'order_id' => $order->order_id,
        'create_date' => format_date(time(), 'custom', 'Y-m-d H:i:s')
      ))
      ->execute();
}
