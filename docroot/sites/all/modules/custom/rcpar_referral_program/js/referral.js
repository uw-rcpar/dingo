(function ($) {
  Drupal.behaviors.rcpar_referral_program = {
    attach: function (context, settings) {

      // Function to copy referral link to user's clipboard.
      function copyLink() {
        /* Get the text field */
        var copyText = document.getElementById("referral-link-box");

        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("Copy");
      }
      // Copy the referral link text when the "Copy Link" is clicked on.
      $("#get-referral-link", context).click(function () {
        copyLink();
      });

    }
  };

}(jQuery));