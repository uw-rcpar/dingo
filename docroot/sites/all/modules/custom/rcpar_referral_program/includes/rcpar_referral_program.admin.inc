<?php

/**
 * Admin form for controlling copy in referral block.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function rcpar_referral_program_block_settings($form, &$form_state) {
  // Block settings fieldset.
  $form['block_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // Field for block title.
  $form['block_settings']['rcpar_referral_program_block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Block Title'),
    '#size' => 21,
    '#maxlength' => 255,
    '#default_value' => variable_get('rcpar_referral_program_block_title',''),
    '#description' => t('Block title'),
  );
  // Field for copy that appears in the block.
  $form['block_settings']['rcpar_referral_program_block_copy'] = array(
    '#type' => 'textarea',
    '#title' => t('Block copy'),
    '#default_value' => variable_get('rcpar_referral_program_block_copy',''),
    '#description' => t('Text for the link to copy the referral URL'),
  );
  // Field for the text of the link user will click on to copy the referral link.
  $form['block_settings']['rcpar_referral_program_block_copy_link_text'] = array(
    '#type' => 'textfield',
    '#size' => 21,
    '#maxlength' => 255,
    '#title' => t('Link text for copy link'),
    '#default_value' => variable_get('rcpar_referral_program_block_copy_link_text',''),
    '#description' => t('Text for the link to copy the referral URL'),
  );
  return system_settings_form($form);
}
