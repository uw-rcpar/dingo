<?php

/**
 * @file
 * Gives customercare access to the roles 'administrator' and 'customer support'
 * on installing the module
 */


/**
 * Implements hook_install().
 */
function rcpar_customercare_install(){
  $permissions = array('access customercare');
  foreach(array('administrator', 'customer support') as $role_name) {
    $role = user_role_load_by_name($role_name);
    user_role_grant_permissions($role->rid, $permissions);
  }
}

/**
 * Implements hook_schema().
 */
function rcpar_customercare_schema() {
  $schema                             = array();
  $schema['rcpar_customercare_notes'] = array(
    'description' => 'The base table for the CustomercareNotes entity',
    'fields' => array(
      'id' => array(
        'description' => 'Primary key of the CustomercareNotes entity',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'customer_uid' => array(
        'description' => 'Drupal user id of the customer the note is about',
        'type' => 'int',
        'unsigned' => TRUE,
        'length' => 10,
        'default' => 0,
        'not null' => TRUE,
      ),
      'note' => array(
        'description' => 'Note about this customer',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'author_uid' => array(
        'description' => 'Drupal user id of the author of the note',
        'type' => 'int',
        'unsigned' => TRUE,
        'length' => 10,
        'default' => 0,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => '1:(default) still applicable; 2: deleted',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the note was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'customer_uid' => array('customer_uid'),
      'created' => array('created'),
    ),
  );

  return $schema;
}

/**
 * Creates rcpar_customercare_notes table to hold info for customer notes
 */
function rcpar_customercare_update_7100() {
  // this is a dupe of the current rcpar_customercare_notes schema. Best practice 
  // is to define it specifically here in case the schema changes at some point and 
  // needs to be updated again
  $notes_schema = array(
    'description' => 'The base table for the CustomercareNotes entity',
    'fields' => array(
      'id' => array(
        'description' => 'Primary key of the CustomercareNotes entity',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'customer_uid' => array(
        'description' => 'Drupal user id of the customer the note is about',
        'type' => 'int',
        'unsigned' => TRUE,
        'length' => 10,
        'default' => 0,
        'not null' => TRUE,
      ),
      'note' => array(
        'description' => 'Note about this customer',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'author_uid' => array(
        'description' => 'Drupal user id of the author of the note',
        'type' => 'int',
        'unsigned' => TRUE,
        'length' => 10,
        'default' => 0,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => '1:(default) still applicable; 2: deleted',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the note was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'customer_uid' => array('customer_uid'),
      'created' => array('created'),
    ),
  );
  db_create_table('rcpar_customercare_notes', $notes_schema);
}
