<?php

/**
 * @file
 * 
 * Definition of rcpar_customercare_handler_field_boolean_yesno_format
 */

/**
 * Allows Yes or No for a boolean value
 */
class rcpar_customercare_handler_field_boolean_yesno_format extends views_handler_field {

  /**
   * overrides ancestor class function, provides default for the the added option
   * @return array $options
   */
  function option_definition() {
    $options = parent::option_definition();

    // set raw_format as default (for backwards compatibility w/ old views)
    $options['fld_format'] = array('default' => 'raw_format');

    return $options;
  }

  /**
   * overrides ancestor class function, adds a select for views format options
   * adds option for "Yes or No"
   * @return array drupal form
   */
  function options_form(&$form, &$form_state) {
    $form['fld_format'] = array(
      '#type' => 'select',
      '#title' => t('Boolean format'),
      '#options' => array(
        'raw_formet' => t('Integer value (0 or 1)'),
        'yesno_format' => t('Yes or No'),
      ),
      '#default_value' => isset($this->options['fld_format']) ? $this->options['fld_format'] : 'raw_formet',
    );
    parent::options_form($form, $form_state);
  }

  /**
   * overrides ancestor class function, 
   * renders the boolean according to user selection
   * @return string ('0' or '1') or ('No' or 'Yes')
   */
  function render($values) {
    $value  = $this->get_value($values);
    $format = $this->options['fld_format'];

    if ($format == 'raw_formet') {
      return $value;
    }
    else { // it's yes no format
      $value = ((empty($value)) ? 'No' : 'Yes');
      return $value;
    }
  }

}
