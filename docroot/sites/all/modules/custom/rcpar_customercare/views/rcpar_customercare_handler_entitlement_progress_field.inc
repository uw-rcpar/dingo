<?php

/**
 * @file
 * Definition of rcpar_customercare_handler_entitlement_progress_field
 */

/**
 * Provides a custom views field.
 */
class rcpar_customercare_handler_entitlement_progress_field extends views_handler_field {

  /**
   * overrides ancestor class function, does nothing everything happens in render
   */
  function query() {
    // do nothing -- to override the parent query.
  }

  /**
   * overrides ancestor class function, 
   * renders err or form w/ order action links
   * @return string err message or rendered form
   */
  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.
    //dpm($data);
    // the following fields are needed for this handler to work.
    // stored here as <data_keyname> => <human readable name (for error)>
    $needed_fieldname_valnames = array(
      'nid' => 'Entitlement Product Nid',
      'field_field_product_sku' => 'Product Sku'
    );

    // track if fields aren't available
    $missing_fields = array();

    // check that each of the fields needed for this handler exist in data
    foreach ($needed_fieldname_valnames AS $needed_fieldname => $valname) {
      if (!(property_exists($data, $needed_fieldname) && isset($data->{$needed_fieldname}))) {
        $missing_fields[] = $valname;
      }
    }

    // report that the handler won't work if necessary
    if (!empty($missing_fields)) {
      $output = t("Err-missing field(s): ");
      foreach ($missing_fields AS $missing_field) {
        $output .= $missing_field . ', ';
      }
      $output = substr($output, 0, -2);
      return $output;
    }

    $ent_id = $data->nid;

    $product_sku = $data->field_field_product_sku[0]['raw']['value'];

    // only online couses have percentages
    $online_course_types = rcpar_dashboard_entitlements_options() + rcpar_dashboard_entitlements_crams_options();
    if (!in_array($product_sku, $online_course_types)) {
      $output = 'n/a';
      return $output;
    }

    // get video history connected to the entitlement
    // get values connected to this entitlement
    //    - total of last_positions of all video histories
    //    - course_type (same for each entitlement) needed for course_stats lookup 
    //    - section (same for each entitlement) needed for course_stats lookup
    $query = db_query("SELECT sum(last_position) as viewed, course_type, section
                      FROM {eck_video_history} 
                      WHERE entitlement_product = :ent_id", array(':ent_id' => $ent_id));

    $vh = $query->fetchObject();

    // see if no history yet
    if (empty($vh->viewed)) {
      return '0%';
    }

    // actually we could probably do this in one query, breaking it up for compatibilty with 
    // possible future database changes from mySql
    $section = strstr(strtolower($vh->course_type), "cram") ? "{$vh->section}-CRAM" : $vh->section;

    // get the total time for this entitlement
    $query = db_query("SELECT total_time
                      FROM {course_stats} 
                      WHERE type = 'section'
                      AND section = :ent_section", array(':ent_section' => $section));

    $course_total_time = $query->fetchField();

    // get a percentage
    $ent_percent_viewed = ($vh->viewed * 100) / $course_total_time;

    // apparently needed in some cases (just following rcpar_dashboard_get_percentages_totals())
    if ($ent_percent_viewed > 100) {
      return '100%';
    }
    else { // make it pretty
      return round($ent_percent_viewed) . '%';
    }
  }

}
