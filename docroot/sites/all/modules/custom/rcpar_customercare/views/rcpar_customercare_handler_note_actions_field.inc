<?php

/**
 * @file
 * Definition of rcpar_customercare_handler_note_actions_field
 */

/**
 * Provides a custom views for orders pane w/ action links.
 */
class rcpar_customercare_handler_note_actions_field extends views_handler_field {

  /**
   * overrides ancestor class function, does nothing, everything happens in render
   */
  function query() {
    // do nothing -- to override the parent query.
  }

  /**
   * overrides ancestor class function, 
   * renders err or form w/ order action links
   * @return string err message or rendered form
   */
  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.
    // dpm($data);
    // check order_id is set & give an error if necessary
    if (!(property_exists($data, 'id')) || empty($data->id)) {
      $output = t('Err-Field id missing.');
      return $output;
    }

    $note_action_form = drupal_get_form('rcpar_customercare_note_actions_form', $data->id);
    $output           = drupal_render($note_action_form);

    return $output;
  }

}
