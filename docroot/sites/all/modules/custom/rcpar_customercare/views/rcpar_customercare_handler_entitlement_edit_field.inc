<?php

/**
 * @file
 * Definition of rcpar_customercare_handler_entitlement_edit_field
 */

/**
 * Provides a custom views for orders pane w/ action links.
 */
class rcpar_customercare_handler_entitlement_edit_field extends views_handler_field {

  /**
   * overrides ancestor class function, does nothing, everything happens in render
   */
  function query() {
    // do nothing -- to override the parent query.
  }

  /**
   * overrides ancestor class function, 
   * renders err or form w/ order action links
   * @return string err message or rendered form
   */
  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.
    //dpm($data);
    // check nid exists is set & give an error if necessary
    if (!(property_exists($data, 'nid')) || empty($data->nid)) { // latter or shouldn't happen
      $output = t('Err-Field nid missing.');
      return $output;
    }

    $customer_uid = $data->_field_data['nid']['entity']->uid;

    // build the form
    $entitlement_action_form_vals = array(
      'ent_nid' => $data->nid,
      'customer_uid' => $customer_uid,
    );

    $entitlements_action_form = drupal_get_form('rcpar_customercare_entitlements_action_form', $entitlement_action_form_vals);

    $output = drupal_render($entitlements_action_form);

    return $output;
  }

}
