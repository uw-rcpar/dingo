<?php

/**
 * @file
 * Definition of rcpar_customercare_handler_order_status_select_field
 */
  
/**
 * Provides a custom views field.
 */
class rcpar_customercare_handler_order_status_select_field extends views_handler_field {

  /**
   * overrides ancestor class function, does nothing everything happens in render
   */
  function query() {
    // do nothing -- to override the parent query.
  }

  /**
   * overrides ancestor class function, 
   * renders err or form w/ order action links
   * @return string err message or rendered form
   */
  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.
    //dpm($data);
    // give an error if necessary
    if (!(property_exists($data, 'order_id')) || empty($data->order_id)) {
      $output = t('Err-Field order_id missing.');
      return $output;
    }

    // get the current order status
    $order = commerce_order_load($data->order_id);

    // give err if  order doesn't load (shouldn't happen)
    if (empty($order)) {
      $output = t('Err-Order not loaded');
      return $output;
    }

    // build the form
    $status_form_vals = array(
      'order_id' => $order->order_id,
      'current_status' => $order->status,
    );

    $status_change_form = drupal_get_form('rcpar_customercare_status_change_form', $status_form_vals);
    $output             = drupal_render($status_change_form);

    return $output;
  }

}
