<?php

/**
 * @file rcpar_customercare.views.inc
 * 
 * adds views functionality for rcpar_customercare
 */

/**
 * Implements hook_field_views_data_alter to add custom views field handlers to 
 * Entitlement content data fields Expiration Date & Content Restricted
 * 
 * @param mixed $result
 * @param mixed $field
 */
function rcpar_customercare_field_views_data_alter(&$result, $field) {
  if (array_key_exists('field_data_field_expiration_date', $result)) {
    $result['field_data_field_expiration_date']['field_expiration_date']['field']['handler'] = 'rcpar_customercare_handler_field_date_format';
  }
  if (array_key_exists('field_data_field_content_restricted', $result)) {
    $result['field_data_field_content_restricted']['field_content_restricted']['field']['handler'] = 'rcpar_customercare_handler_field_boolean_yesno_format';
  }
}

/**
 * Implementation of hook_views_handlers().
 * 
 * adds field handlers for views
 */
function rcpar_customercare_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'rcpar_customercare') . '/views',
    ),
    'handlers' => array(
      // gives an option to add a custom format to a date or integer field
      'rcpar_customercare_handler_field_date_format' => array(
        'parent' => 'views_handler_field',
      ),
      // gives an option to show boolean field as 'Yes' or 'No'
      'rcpar_customercare_handler_field_boolean_yesno_format' => array(
        'parent' => 'views_handler_field',
      ),
      // add an order status select to a view
      'rcpar_customercare_handler_order_status_select_field' => array(
        'parent' => 'views_handler_field',
      ),
      // adds order actions links to a view
      'rcpar_customercare_handler_order_actions_field' => array(
        'parent' => 'views_handler_field',
      ),
      // adds entitlement progress field to a view
      'rcpar_customercare_handler_entitlement_progress_field' => array(
        'parent' => 'views_handler_field',
      ),
      // adds entitlement edit submit form (to open the entitlement edit form in a modal) a view
      'rcpar_customercare_handler_entitlement_edit_field' => array(
        'parent' => 'views_handler_field',
      ),
      // adds notes actions to a view
      'rcpar_customercare_handler_note_actions_field' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 * 
 * adds custom views fields
 */
function rcpar_customercare_views_data() {
  $data                                                    = array();
  // Add rcpar_customercare group
  $data['rcpar_customercare']['table']['group']            = t('RCPAR Customer Care');
  $data['rcpar_customercare']['table']['join']             = array(
    // #global is a special flag which let's a table appear all the time.
    '#global' => array(),
  );
  // add a custom field for changing an order's status
  $data['rcpar_customercare']['order_status_select_field'] = array(
    'title' => t('Order Status Select Field'),
    'help' => t('Provides orders pane form for changing the order status. (Must be preceeded by order_id in view.)'),
    'field' => array(
      'handler' => 'rcpar_customercare_handler_order_status_select_field',
    ),
  );
  // add a custom field for order action links
  $data['rcpar_customercare']['order_actions_field']       = array(
    'title' => t('Order Action Field'),
    'help' => t('Provides orders pane action links. (Must be preceeded by order_id in view.)'),
    'field' => array(
      'handler' => 'rcpar_customercare_handler_order_actions_field',
    ),
  );
  // add a custom field for entitlements progress
  $data['rcpar_customercare']['entitlement_progress_field'] = array(
    'title' => t('Entitlement Progress Field'),
    'help' => t('Provides entitlements pane progress field. (Must be preceeded by Content: Nid & Content: Product Sku in view, nodes must all be User Entitlement Product content type.)'),
    'field' => array(
      'handler' => 'rcpar_customercare_handler_entitlement_progress_field',
    ),
  );
  // add a custom field for entitlements edit form
  $data['rcpar_customercare']['entitlement_edit_action_field'] = array(
    'title' => t('Entitlement Edit Form Action Field'),
    'help' => t('Provides entitlements edit submit button to open the entitlement edit form in a modal. (Must be preceeded by Content: Nid, nodes must all be User Entitlement Product content type.)'),
    'field' => array(
      'handler' => 'rcpar_customercare_handler_entitlement_edit_field',
    ),
  );
  // add a custom field for note action links/buttons
  $data['rcpar_customercare']['note_actions_field']            = array(
    'title' => t('Note Action Field'),
    'help' => t('Provides notes pane action links. (Must be preceeded by id in view.)'),
    'field' => array(
      'handler' => 'rcpar_customercare_handler_note_actions_field',
    ),
  );
  return $data;
}
