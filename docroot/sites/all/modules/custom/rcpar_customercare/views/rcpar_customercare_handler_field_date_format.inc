<?php

/**
 * @file
 * 
 * Definition of rcpar_customercare_handler_field_date_format
 * 
 */

/**
 * Allows a conversion to a datetime into a date from an integer
 */
class rcpar_customercare_handler_field_date_format extends views_handler_field {

  /**
   * overrides ancestor class function, provides default for the the added options
   * @return array $options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['date_format']        = array('default' => 'raw');
    $options['custom_date_format'] = array('default' => '');

    return $options;
  }

  /**
   * overrides ancestor class function, adds a select for views format options
   * @return array drupal form
   */
  function options_form(&$form, &$form_state) {
    $date_formats = array();
    $date_types   = system_get_date_types();
    foreach ($date_types as $key => $value) {
      $date_formats[$value['type']] = check_plain(t($value['title'] . ' format')) . ': ' . format_date(REQUEST_TIME, $value['type']);
    }
    $form['date_format']        = array(
      '#type' => 'select',
      '#title' => t('Date format'),
      '#options' => $date_formats + array(
      'custom' => t('Custom'),
      'raw' => t('Raw integer value'),
      ),
      '#default_value' => isset($this->options['date_format']) ? $this->options['date_format'] : 'raw',
    );
    $form['custom_date_format'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom date format'),
      '#description' => t('If "Custom", see <a href="http://us.php.net/manual/en/function.date.php" target="_blank">the PHP docs</a> for date formats. Otherwise, enter the number of different time units to display, which defaults to 2.'),
      '#default_value' => isset($this->options['custom_date_format']) ? $this->options['custom_date_format'] : '',
      '#dependency' => array(
        'edit-options-date-format' => array(
          'custom',
        )
      ),
    );
    parent::options_form($form, $form_state);
  }

  /**
   * overrides ancestor class function, 
   * renders the integer field as a date format or as the raw integer
   * @return string rendered value
   */
  function render($values) {
    // get the raw value
    $value  = $this->get_value($values);
    // get the date format selection
    $format = $this->options['date_format'];

    // if it was custom, get entered date format
    if ($format == 'custom') {
      $custom_format = $this->options['custom_date_format'];
    }

    if ($value) { // should always work
      if ($format == 'raw') { // return the raw integer 
        return $value;
      }
      else { // need to format the integer (timestamp)
        switch ($format) {
          case 'custom':
            if ($custom_format == 'r') {
              return format_date($value, $format, $custom_format, NULL, 'en');
            }
            return format_date($value, $format, $custom_format);
          default:
            return format_date($value, $format);
        }
      }
    }
    else { // unlikely
      return t('Err-no value in field');
    }
  }

}
