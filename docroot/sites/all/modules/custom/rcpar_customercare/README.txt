**************************************
* CHANGE LOG
**************************************
---------------------------------
16-11-07 changes
DIN-817 Customer Care menu link
  version = 7.x-1.2
  - adds Customer Care Admin menu item to People admin menu

---------------------------------
16-10-13 changes
DIN-732 Notes pane
  version = 7.x-1.1
  - adds CustomercareNoteEntity

---------------------------------
16-09-07 changes
CA-12 Summary section & CA-4 Search Page
  version = 7.x-1.0a
  - don't show entitlements pane on first search page
  - show log of changes made in user profile pane

---------------------------------
16-09-06_18-42 changes
CA-26 Entitlements section (Section-wide operations)
  version = 7.x-1.0
  - adds Include expired checkbox to bottom of Entitlements pane
  - adds User entitlement admin link to bottom of Entitlements pane

---------------------------------
16-09-06_09-03 changes
  CA-27 & CA-46
  CA-27 Entitlements section (Column 6)
  CA-46 Changes msg on too many search results found
  version = 7.x-1.0-alpha12
  - changed permissions for views Customer Care Search Results & 
Customer Care Orders to 'access customercare'
  - added Entitlements section (Column 6)
  - fix typo in views boolean field format handler default format
  - remove sort on Product Sku in Entitlements pane
  - disables the school field to editing in User Profile section
  - shows ALL (not just active) entitlements in entitlements pane
  - entitlements & orders panes wrapped in fieldset

---------------------------------
16-09-01_15-43 changes
  CA-25 Entitlements section (Columns 1-5)
  version = 7.x-1.0-alpha11
  - uses views custom field & format handler for lecture progress
  - changes order actions summary submit label from "Order Summary" to "Summary"

---------------------------------
16-08-30_22-56 changes
version = 7.x-1.0-alpha10
  CA-38 & CA-39 PR fixes
  (some start code on entitlements)
 - adds views date field format handler
 - adds views boolean field format handler
 - adds views custom field & format handler for order status change
 - adds views custom field & format handler for order actions


---------------------------------
16-08-29_09-35 changes
version = 7.x-1.0-alpha9
 deals w/ CA-38 (orders pane all except action column) & CA-39 (action column)
 - adds orders pane w/ select status & action links for each order row

---------------------------------
16-08-26_08-18 changes
version = 7.x-1.0-alpha8
CA-46a commit
 - limit customer serch results to 200 & notifies user if there are more records
 - view shows first & last name in two sortable fields
 - widens search model width

---------------------------------
16-08-25_21-05 changes
version = 7.x-1.0-alpha7
first of CA-46 CA-44 CA-47 CA-45 CA-43 commit
 - adds ajax paging to customer search results in modal window
 - limit customer serch results to 500
 - use standard ctools model (w/ css tweeks)
 - add header sort to search results
 - clicking on button when cursor is in one field you've changed no longer takes 
2 clicks

---------------------------------
16-08-22_06-59 changes
version = 7.x-1.0-alpha6
first of CA-12 (Customer Profile Pane)
 - styles customer search pane

---------------------------------
16-08-17_13-38 changes
version = 7.x-1.0-alpha5
 - Addresses PR comment requests
---------------------------------
16-08-17_12-21 changes
version = 7.x-1.0-alpha4
 - Addresses PR comment requests (incl. creates 'access customercare' permission)
 - hook_install gives 'access customercare' permission to 'administrator' & 
'customer support' roles
 - always shows customer search results in a modal


---------------------------------
16-08-16_13-37 changes
version = 7.x-1.0-alpha3
 - clicking on Customer profile link in results adds Customer identification to 
browser window
 - Searching from Customer Profile page redraws page with result info under a 
blank search form
 - access restricted to users with 'administrator' or 'customer support' roles

---------------------------------
16-08-12_14-04 changes
version = 7.x-1.0-alpha2
 - Addresses PR comment requests
 - Added user name to search & search results
 - Removed link on email in search results
 - Adds a note to search panel that anything in Order # causes other inputs to 
be ignored
 - Changes label of action link in search results to "User profile"
 - customercare base url for results User profile link is set in module


---------------------------------
16-08-11_23-51 changes:
version = 7.x-1.0-alpha1

********************************
* TODO 
********************************
 - consider putting js & css in .info for aggregation
 - Use same summary mapping in ajax cb for page cb
 - use new views custom date format matching the current short format "mm/dd/yyyy - hh:mm"
so that the view won't break if the site's short format is changed ( at 
/admin/config/regional/date-time)