(function ($) {
  Drupal.behaviors.RcparCustomerCare = {
    /**
     * Code to be loaded run on page and ajax load
     * $(function () is a shorthand for $(document).ready().
     * @see {@link http://api.jquery.com/ready} for further details.
     *
     * @returns {undefined}
     */
    attach: function (context, settings) {

      //deselect the first link in modals
      $('#modalContent a, #modalContent button').blur();

      /**
       * SUMMARY FUNCTIONS
       */
      if ($('#summary-submit', context).length) { // skipped on first customer search
        $('#summary-submit', context).once('custcareSumSubmitEnable', function () { // cust info present
          $('.care-summary-change').each(function () {
            // change functions for each summary inputs
            $(this).on('input', function () {
              doSummarySubmitEnable();
            });
          });
        });

        /**
         * enables or disables Summary panes save button
         * @returns {undefined}
         */
        function doSummarySubmitEnable() {
          // assume the submit button should be disabled
          var disableVal = true;
          if (!($('#summary-password-wrapper input[id^="edit-password-pass1"]').val()) ||
            (
              ($('#summary-password-wrapper input[id^="edit-password-pass1"]').val() || $('#summary-password-wrapper input[id^="edit-password-pass2"]').val()) &&
              ($('#summary-password-wrapper input[id^="edit-password-pass1"]').val() == $('#summary-password-wrapper input[id^="edit-password-pass2"]').val())
              )
            )
          {
            disableVal = false;
          }
          // enable or disable the submit button based on our testing
          $('#summary-submit').prop('disabled', disableVal);
          // track that a val has been changed for password changes
          careSummaryIsChanged = true;
          // empty out the submit-msg (filled on ajax submit
          $('#summary-msg').html('');
        }

        // customercare summary panel 1st render & it's ajax submit
        if ($('#summary-msg', context).length) { // cust info present
          // global var tracks text input changes, used by password change
          careSummaryIsChanged = false;

          // disable the submit button
          $('#summary-submit').prop('disabled', true);

          // clicking on button when cursor is in one field you've changed should not take 2 clicks
          $($('#summary-submit').parent()).on('mousedown', function () {
            $('#edit-summary input').blur();
            if (!$('#summary-submit').prop('disabled')) {
              $('#summary-submit').trigger('mousedown');
            }
          })

          // add err message field
          $('#summary-password-wrapper input[id^="edit-password-pass2"]', context).after('<span id="care-pw-confirm-msg"></span>');

          // apply to both summary pw fields
          $('#summary-password-wrapper input[id^="edit-password-pass"]', context).each(function () {
            $(this)
              // add js notification for password confirmation field
              .on('keyup', function () {
                if ($(this).val() == $('#summary-password-wrapper input[id^="edit-password-pass1"]').val()) { // pw fields match
                  if ($(this).val()) { // the pw fields are not empty
                    $('#care-pw-confirm-msg').html('matching').css('color', 'green');
                  }
                  else { // both pw fields are empty remove the msg
                    $('#care-pw-confirm-msg').html('');
                  }
                }
                else { // pw fields do not match
                  $('#care-pw-confirm-msg').html('not matching').css('color', 'red');
                }
              })

              // enable or disable the submit button
              .on('input', function () {
                // assume the button should be disabled
                var isDisabled = true;
                // enable if one has a value and they both match
                if (($('#summary-password-wrapper input[id^="edit-password-pass2"]').val() || $('#summary-password-wrapper input[id^="edit-password-pass1"]').val())) { // one of these has a value
                  if ($('#summary-password-wrapper input[id^="edit-password-pass2"]').val() == $('#summary-password-wrapper input[id^="edit-password-pass1"]').val()) { // and they both match
                    isDisabled = false;
                  }
                }
                else { // pw fields both empty
                  // empty confirm msg if both pw fields are empty
                  $('#care-pw-confirm-msg').html('');
                  if (careSummaryIsChanged) { // pw fields both empty and a textfield has been changed
                    isDisabled = false;
                  }
                }
                // enable or disable the button
                $('#summary-submit').prop('disabled', isDisabled);
                if (!isDisabled) {
                  // empty out the submit-msg (filled on ajax submit
                  $('#summary-msg').html('');
                }
              });
          });
        }
      }
      /**
       * ORDERS FUNCTIONS
       */
      // customercare order view 1st render & pagination, skipped on first customer search
      if ($('button.rcpar-customercare-status-change-form-order-submit', context).length) { // there's a status submit button

        // disable the status submit buttons
        $('button.rcpar-customercare-status-change-form-order-submit', context).prop('disabled', true);

        // add change functions for the order status selects
        $('select.rcpar-customercare-status-change-form-order-select', context).each(function () {
          $(this).on('change', function () {
            // set the hidden value of the status submit form to the new select val
            $('input[name="status_val"]', $(this).parentsUntil('form')).val($(this).val());
            // enable/disable the status submit button
            var isDisabled = false; // assume we'll disable
            if ($(this).val() == $('input[name="status_original"]', $(this).parentsUntil('form')).val()) {
              isDisabled = true;
            }
            $('button.rcpar-customercare-status-change-form-order-submit', $(this).parentsUntil('form')).prop('disabled', isDisabled);
            // clear the status msg
            $('.care-orders-status-msg', $(this).parentsUntil('form')).html('');
          });
        });

      }
      // order status change ajax reply
      if ($('p.care-orders-status-msg', context).length) {
        // disable it's submit button
        $('p.care-orders-status-msg', context).each(function () {
          $('button.rcpar-customercare-status-change-form-order-submit', $(this).parentsUntil('form')).prop('disabled', true);
        });
      }
      /**
       * ENTITLEMENT FUNCTIONS
       */
      if ($('.care-include-inactive', context).length) { // the entitlements view is being refreshed
        var includeInactive = $('input', '#customercare-entitlement-all-checkbox').is(':checked');
        var includeInactiveBool = ((includeInactive) ? 1 : 0);
        $('.care-include-inactive', context).val(includeInactiveBool);
      }
      
      //Course Tree behavior
      $('.course-tree a', context).click(function (e) {
        $(this).parent().parent().find("ul").toggle("slow");
        $(this).parent().parent().toggleClass("collapsed");
        e.preventDefault();
      });


      // show and hide tree tabs
      $('.tree-tab', context).click(function (e) {
        // remove active class from tab
        $('.tree-tab').removeClass('tab-active');
        // hide the currently shown course tree
        $('.course-tree').hide();
        // add active class to tab clicked on
        $('#' + $(this).attr('id')).addClass('tab-active');
        // show correct course tree
        $('.' + $(this).attr('id')).show();
      });

    }


  }
}(jQuery))
