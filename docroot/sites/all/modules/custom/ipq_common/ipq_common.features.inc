<?php
/**
 * @file
 * ipq_common.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function ipq_common_eck_bundle_info() {
  $items = array(
    'tab_content' => array(
      'machine_name' => 'tab_content',
      'entity_type' => 'tab',
      'name' => 'content',
      'label' => 'Content',
      'config' => array(),
    ),
    'tab_file' => array(
      'machine_name' => 'tab_file',
      'entity_type' => 'tab',
      'name' => 'file',
      'label' => 'File',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function ipq_common_eck_entity_type_info() {
  $items = array(
    'ipq_answer' => array(
      'name' => 'ipq_answer',
      'label' => 'IPQ Answer',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
    'tab' => array(
      'name' => 'tab',
      'label' => 'Tab',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
  );
  return $items;
}
