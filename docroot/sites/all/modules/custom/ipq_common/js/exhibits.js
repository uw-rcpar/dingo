(function($, Drupal, window, document, undefined) {
  Drupal.behaviors.ipqExhibits = {
    // Storage for our exhibit objects
    exhibits: {},
    
    // Exhibits last touched
    lastTouched: [],

    /**
     * Gets a stored exhibit or instantiates it if it does not exist yet
     * @param {string} id
     * - ID of an exhibit container
     * @return {ipqExhibit}
     */
    getExhibit: function(id) {
      // If we don't have this exhibit instantiated yet, do it now
      if (!this.exhibits[id]) {
        this.exhibits[id] = new ipqExhibit(id);
      }

      return this.exhibits[id];
    },

    /**
     * Close all exhibit windows
     */
    closeAll: function() {
      $.each(this.exhibits, function(){
        this.hide();
      });
      // Set the icon classes
      Drupal.behaviors.ipqExhibits.setIconClasses();
    },

    /**
     * Returns the highest zindex of all exhibits in existence.
     * @return {number}
     */
    getHighestZindex: function() {
      var indexes = [];
      $.each(this.exhibits, function(){
        indexes.push(this.getZindex());
      });

      // Return this highest value
      return Math.max.apply(Math, indexes);
    },

    /**
     * Attach event handlers
     */
    attach: function(context, settings) {
      var self = this;

      // jQuery resizable iframe fix:
      // https://stackoverflow.com/questions/13473396/jquery-ui-resizable-resize-a-div-placed-over-an-iframe
      // todo - these events don't seem to fire. Intention was to react a little quicker than the fix described above.
      // Not a huge deal.
      $('.ui-resizable-handle').mousedown(function(){
        $('iframe').css('pointer-events','none');
      });
      $('.ui-resizable-handle').mouseup(function(){
        $('iframe').css('pointer-events','auto');
      });

      // React to an exhibit link being clicked
      $('#exhibit-list a', context).click(function(e) {
        var id = $(this).attr('data-ident');
        var type = $(this).attr('data-extype');
        self.getExhibit(id).click();
      });

      // Click handler for an exhibit window's "x" button
      $('.exhibit-header .close').click(function(e){
        var id = $(this).closest('.exhibit-container').attr('id');
        self.getExhibit(id).hide();
      });

      // Clicking any part of an exhibit should bring it to the foreground
      $('.exhibit-container').mousedown(function(e){
        self.getExhibit($(this).attr('id')).bringToFront();
      });

      // "Close all exhibits" link click
      $('#exhibits-close-all').click(function(e) {
        self.closeAll();
      });

      // Cascade link click
      $('#exhibits-cascade').click(function(e) {
        self.cascade();
      });

      // Tile link click
      $('#exhibits-tile').click(function(e) {
        self.tile();
      });
    },

    /**
     * Returns an array of visible exhibits
     * @return {Array}
     */
    getVisibleExhibits: function() {
      var visibleExhibits = [];
      $.each(this.exhibits, function(){
        if(this.isVisible) {
          visibleExhibits.push(this);
        }
      });

      return visibleExhibits;
    },

    /**
     * Set's classes on the 'tile', 'cascade', and 'close all' button based on the
     * status of visible exhibits.
     */
    setIconClasses: function() {
      var exhibits = this.getVisibleExhibits();
      // If no exhibits are open, remove active class from all icon links
      if(exhibits.length == 0) {
        $('#icons-wrapper a').removeClass('active');
      }
      // If at least one exhibit is open, add active class to close all link
      else if(exhibits.length == 1) {
        $('#exhibits-close-all').addClass('active');
        $('#exhibits-tile').removeClass('active');
        $('#exhibits-cascade').removeClass('active');
      }
      // If more than one exhibit is open, add active classes to tile and cascade links
      else if (exhibits.length > 1) {
        $('#exhibits-close-all').addClass('active');
        $('#exhibits-tile').addClass('active');
        $('#exhibits-cascade').addClass('active');
      }
    },

    /**
     * Adjusts the height of the an exhibit's content wrapper when its outer container changes size.
     * For PDFs, scrollbars are within the iframe so we want the outer container to be overflow:hidden.
     * For content, we also want the inner container to have scrollbars and the outer to be overflow:hidden
     * so that the jquery resize handles can appear on the outside of the scrollbars. Because of the height
     * of the header, the bottom of the inner container extends below the overflow of the outer wrapper,
     * so we have to dynamically resize the inner container when the outer wrapper is resized. -jd
     *
     * @param {ipqExhibit} i
     * - An object of type ipqExhibit
     */
    exhibitRedraw: function(i) {
      // Calculate the height of the header by subtracting the top position of the container from the top of the
      // content. The distance in between those is space the header occupies
      // Note: this approach didn't work, outerHeight() and height() does't seem to account for padding of elements
      // inside the element it's calculating height for
      // var desiredHeight = i.$container.outerHeight(true) - i.$container.find('.exhibit-header').outerHeight(true);
      var headerHeight = Math.abs(i.$container.offset().top - i.$container.find('.exhibit-content').offset().top);
      var desiredHeight = i.$container.outerHeight(true) - headerHeight;

      // Set the content wrapper height to the desired, which should fill the spacein the modal.
      // The iframe is set to 100% height via CSS, so adjusting the wrapper will by proxy affect
      // the height of the pdf iframe.
      i.$container.find('.exhibit-content').css('height', desiredHeight);
    },

    /**
     * Get's the distance from the page top of the top of the workspace.
     * Used to position exhibits and do other distance calculations relative to the workspace.
     * todo - calculate this dynamically based on page header height
     * @return {number}
     */
    getWorkspaceTop: function() {
      return 150;
    },

    /**
     * Gets the starting position for an exhibit. Currently based on the static width of the content area
     * and static height of the toolbar, but this may need to be calculated dynamically for different devices
     * or other responsive factors.
     * @return {{top: number, left: number}}
     */
    getStartingPosition: function() {
        // If our workspace area is wide enough to fit a whole exhibit, we can place our exhibit in the top left corner
        // of that area
        if(this.getWorkspaceWidth() >= this.getExhibitDefWidth()) {
            return {
                top: this.getWorkspaceTop(),
                left: $('#ipq-container').outerWidth() + 10
            };
        }
        // Otherwise we'll place the exhibit as far right as possible
        else {
            return {
                top: this.getWorkspaceTop(),
                left: $(window).width() - this.getExhibitDefWidth()
            };
        }
    },

    /**
     * Workspace width can be calculated by the width of the browser minus the width of question area, minus
     * a little more for some padding.
     * This could potentially be zero or even negative, which means we don't have a workspace.
     * return {int} in pixels
     */
    getWorkspaceWidth: function() {
      return $(window).width() - $('#ipq-container').outerWidth() - 10;
    },

    /**
     * Get workspace height - the distance from the workspace top to the bottom of the page
     * return {number}
     */
    getWorkspaceHeight: function() {
      return $(window).height() - this.getWorkspaceTop();
    },

    /**
     * The minimum width that an exhibit is allowed to be
     * @return {number}
     */
    getExhibitDefWidth: function() {
      // If our workspace is small, we'll make our exhibits a little smaller too, to a point.
      return 650;
    },
    /**
     * Get default height for an exhibit
     * @return {number}
     */
    getExhibitDefHeight: function() {
      return 600;
    },

    /**
     * Displays visible exhibits in a 'tile' fashion.
     */
    tile: function() {
      // Only operate if there are visible exhibits
      var exhibits = this.getVisibleExhibits();
      if (exhibits.length <= 1) {
        return;
      }

      var startingPos = {};
      var exhibitWidth = this.getExhibitDefWidth();

      // Determine if we can fit 2 exhibits wide in our workspace
      if(this.getWorkspaceWidth() >= this.getExhibitDefWidth() * 2) {
        startingPos = this.getStartingPosition();
        exhibitWidth = this.getWorkspaceWidth() / 2;
      }
      // Narrower window - We'll have to encroach into the question area
      else {
        startingPos.top = this.getWorkspaceTop();
        // Start showing exhibits at 2 widths of an exhibit in from the right of the window
        startingPos.left = $(window).width() - this.getExhibitDefWidth() * 2;
      }

      // We'll split the exhibits two columns - if the number of exhibits is not evenly divisible, this code
      // will add the extra exhibit always to the left column.
      // If it is divisble by 2, remainder will be 0 and both sides end up with the same number of exhibits.
      // Pretty clever if I do say so myself. -jd
      var remainder = exhibits.length % 2;
      var exhibitsPerColumnLeft = ((exhibits.length - remainder) / 2) + remainder;
      var exhibitsPerColumnRight = ((exhibits.length - remainder) / 2);

      // Calculate exhibit height by dividing available workspace height by number of exhibits to show
      var exhibitHeightLeft = this.getWorkspaceHeight() / exhibitsPerColumnLeft;
      var exhibitHeightRight = this.getWorkspaceHeight() / exhibitsPerColumnRight;

      var x;

      // Place left column exhibits
      for(x = 0; x < exhibitsPerColumnLeft; x++) {
        exhibits[x].$container
          .css('height', exhibitHeightLeft + 'px')
          .css('width', exhibitWidth + 'px')
          .css('top', (startingPos.top + (x * exhibitHeightLeft))  + 'px')
          .css('left', startingPos.left + 'px');

        // For pdf files, we need to do some resizing and such.
        if(exhibits[x].type == 'file') {
          this.exhibitRedraw(exhibits[x]);
        }
      }

      // Place right column exhibits
      for(x = 0; x < exhibitsPerColumnRight; x++) {
        exhibits[x + exhibitsPerColumnLeft].$container
          .css('height', exhibitHeightRight + 'px')
          .css('width', exhibitWidth + 'px')
          .css('top', (startingPos.top + (x * exhibitHeightRight))  + 'px')
          .css('left', startingPos.left + exhibitWidth + 'px');

        // For pdf files, we need to do some resizing and such.
        if(exhibits[x + exhibitsPerColumnLeft].type == 'file') {
          this.exhibitRedraw(exhibits[x + exhibitsPerColumnLeft]);
        }
      }
    },

    /**
     * Displays visible exhibits in a 'cascade' fashion.
     */
    cascade: function() {
      // Only operate if there are visible exhibits
      var exhibits = this.getVisibleExhibits();
      if (exhibits.length <= 1) {
        return;
      }
      // Get the starting position for the workspace
      var position = this.getStartingPosition();
      // Place each exhibit
      $.each(exhibits, function() {
        // Each exhibit will go in front of the next
        this.bringToFront();

        // Set position and resize to default
        this.$container
          .css('top', position.top + 'px')
          .css('left', position.left + 'px')
          .css('width', DBI.getExhibitDefWidth() + 'px')
          .css('height', DBI.getExhibitDefHeight() + 'px');

        // Adjust the position for the next one
        var offset = this.$container.find('.exhibit-header').height();
        position.top += offset;
        position.left += offset;

        // For pdf files, we need to do some resizing and such.
        if(this.type == 'file') {
          DBI.exhibitRedraw(this);
        }
      });
    },

    /**
     * Set, get, and un-touching exhibits is used to remember the last touched
     * exhibit so we can place the next exhibit relative to its position.
     * @param i
     */
    setExhibitLastTouched: function(i) {
      this.lastTouched.push(i);
    },
    getExhibitLastTouched: function(i) {
      return this.lastTouched[this.lastTouched.length - 1];
    },
    untouchExhibit: function(iToUntouch) {
      var self = this;
      // Remove the exhibit to untouch from our list.
      // Thar be dragons: I encountered a strange issue when using $.each and .slice() in conjunction with closeAll()
      // that seemed to break the $.each running in the closeAll method so that only one exhibit would close.
      // Not sure what it was related to exactly, but $.grep is cleaner anyway.
      this.lastTouched = $.grep(self.lastTouched, function(i) {
        return i.id == iToUntouch.id;
      }, true);
    }
  };

  // Make an easy reference to our parent class
  var DBI = Drupal.behaviors.ipqExhibits;

  /**
   * The main exhibit class
   * @param {string} id - the HTML id of the exhibit container.
   */
  function ipqExhibit(id) {
    // Initialize vars
    this.type = '';
    this.id = id;
    this.$link = $();
    this.$container = $();
    this.isVisible = false;

    // Know thyself
    var self = this;

    /* Instantiate the exhibit. Doesn't display yet. */
    this.create = function() {
      // Get the corresponding exhibit content and make it draggable & resizable
      self.$container = $('#' + self.id)
        .draggable({
          handle: '.exhibit-header',
          start: self.bringToFront,
          iframeFix: true // Prevents iframes (our pdfs) from capturing mousemove events during a drag
        })
        .resizable({
          handles: 'n, e, s, w, ne, se, sw, nw',
          start: function(event, ui) {
            // Iframe fix: https://stackoverflow.com/questions/13473396/jquery-ui-resizable-resize-a-div-placed-over-an-iframe
            // todo - this fix helps, but the 'start' event isn't called until you start dragging the resize handle.
            // You can drag the handle down, but you can't drag it up into the space of the iframe until the fix is
            // applied. We need the fix to happen sooner, like on mousedown of the drag handle.
            $('iframe').css('pointer-events','none');

            self.bringToFront();
          },
          stop: function(event, ui) {
            // Iframe fix
            $('iframe').css('pointer-events','auto');

            // Redraw pdf when a resize is complete
            DBI.exhibitRedraw(self);
          }
        });

      // Get the corresponding exhibit link
      self.$link = $('#' + self.id + '-link');

      // Type - either 'content' or 'file'
      self.type = self.$link.attr('data-extype');

      // Take this content out of the question area and into the body
      $('body').append(self.$container);

      // Initialize with 1000 z-index
      self.setZindex(1000);
    };

    /* Show the exhibit */
    this.show = function() {
      // Get the position for this exhibit. If we have no exhibits, use the starting position
      var pos = {};
      if(!DBI.getVisibleExhibits().length) {
        pos = DBI.getStartingPosition();
      }
      // Some exhibits are visible, position should be relative to the last exhibit
      else {
        var lasti = DBI.getExhibitLastTouched();
        // Note, position() and offset() always seemed to be offset by something, maybe admin menu, so we're forced
        // to read the CSS properties directly and parse as numbers to remove the 'px'
        pos.top = parseFloat(lasti.$container.css('top'));
        pos.left = parseFloat(lasti.$container.css('left'));

        // We'll offset the top and left position of the new exhibit by the height of its header
        var offset = lasti.$container.find('.exhibit-header').height();
        pos.top += offset;
        pos.left += offset;
      }

      self.$container
        .css('top', pos.top + 'px')
        .css('left', pos.left + 'px')
        .css('width', DBI.getExhibitDefWidth() + 'px')
        .css('height', DBI.getExhibitDefHeight() + 'px');
        // .css('height', DBI.getWorkspaceHeight() - 100 + 'px'); // workspace height minus a little for padding

      // Show the exhibit and mark it as visible
      self.$container.show();
      self.isVisible = true;

      // Mark the link as active
      self.$link.addClass('active');

      // For pdf files, we need to do some resizing and such.
      if(self.type == 'file') {
        DBI.exhibitRedraw(self);
      }

      // Set the icon classes
      DBI.setIconClasses();

      // Add this exhibit to the 'last touched' queue
      DBI.setExhibitLastTouched(self);
    };

    /* Hide the exhibit */
    this.hide = function() {
      // Hide the exhibit and mark as not visible
      self.$container.hide();
      self.isVisible = false;

      // Set the icon classes
      DBI.setIconClasses();

      // Mark the link as active
      self.$link.removeClass('active');

      // Remove this exhibit from the 'last touched' queue
      DBI.untouchExhibit(self);
    };

    /* Action taken when an exhibit _link_ is clicked */
    this.click = function() {
      // Whenever an exhibit link is clicked we should bring it to the front
      self.bringToFront();

      // If the exhibit wasn't visible, make it so. Otherwise we're done.
      if (!this.isVisible) {
        self.bringToFront();
        this.show();
      }
    };

    /* Gets the z-index of this exhibit, or 0 if not set. */
    this.getZindex = function() {
      var zindex = self.$container.css('z-index');
      if(zindex == '') {
        return 0;
      }

      return zindex;
    };

    /* Sets z-index */
    this.setZindex = function(i) {
      return self.$container.css('z-index', i);
    };

    /* Bring this exhibit visually to the forefront, relative to the other exhibits. */
    this.bringToFront = function() {
      // Set us in the foreground
      self.setZindex(DBI.getHighestZindex() + 1);
    };

    /* Obliterate from existence. Not used currently. */
    this.destroy = function() {
      self.$container.remove();
      self.isVisible = false;
    };

    // Constructor code:
    this.create();
  }

})(jQuery, Drupal, this, this.document); //END - Closure

