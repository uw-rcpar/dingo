(function ($) {

  // Function to format input numbers.
  function formatValue(input_value) {
    // Limit the input to 9 characters.
    charLimit = 9;
    // Unless the input is wrapped in parenthesis, then leave space for them.
    if (input_value.toString().substring(0, 1) == '-') {
      charLimit = 10;
    }
    // Unless the input is wrapped in parenthesis, then leave space for them.
    if (input_value.toString().substring(0, 1) == '(') {
      charLimit = 11;
    }
    // Truncate the string if above the character limit.
    if (input_value.length > charLimit) {
      input_value = input_value.toString().substring(0, charLimit);
    }
    formatted_value = numeral(input_value).format('(0,0)');
    return formatted_value;
  }

  // This is a hacky solution/band-aid for a bug in handsontable.
  // When a cell is selected (using tables.selectCell(row, col)),
  // this causes the scrollbar in the scrollable div to jump to the top.
  // To prevent this, we will temporarily disable and later enable scrolling
  // in the scrollable div for tbs and tbs journal question types.
  window.disableScrollingDiv = function() {
    var x=document.querySelector('.scrollable-area').scrollLeft;
    var y=document.querySelector('.scrollable-area').scrollTop;
    document.querySelector('.scrollable-area').onscroll=function(){
      document.querySelector('.scrollable-area').scrollTop = y;
    };
  };
  window.enableScrollingDiv = function() {
    document.querySelector('.scrollable-area').onscroll=function(){};
  };
  /**
   * Create a modal for TBS Journal numeric entries.
   * Includes functionality for buttons in footer of modal.
   * HTML for Modal is created in form--question-display.tpl.php.
   * @param row
   * @param col
   * @param hot1
   */
  window.answerEntryPopup = function (row, col, hot1) {
    // Store the current instance for later use (when the modal is hidden).
    var that = hot1;
    // Don't allow answer popups when the grid is read-only
    if(hot1.getSettings().readOnly) {
      return;
    }
    // Don't allow popups from two separate grids to be open at the same time.
    if ($('#tbs-entry-popup').is(":visible") || $('#tbs-answer-popup').is(":visible")) {
      return;
    }
    // Get the editor.
    var editor = hot1.getActiveEditor();

    // Get our TD cell.
    var td = editor.TD;

    // Add role to popup for accessibility.
    $('#tbs-entry-popup').attr("role", "dialog");

    // Show the modal.
    $('#tbs-entry-popup').modal();

    // Move focus to input.
    $('.tbs-entry-input').focus();

    // Make all grids read only while the modal is open.
    window.disableAllGrids();

    // add class to body for styling the hot table
    $('body').addClass('popup-open');

    // calculate positions and assign arrow direction
    var arrow = 'arrow-left';
    // Get half the height of the TD
    var tdHeightHalf = editor.TD.clientHeight / 2;
    // Get half the height of the modal
    var modalHeightHalf = $('#tbs-entry-popup .modal-content').height() / 2;
    // If the no-entry-required div exists, we need to add it to the top position value.
    var noEntryHeight = 0;
    if ($(hot1.rootElement).prev() != null && $(hot1.rootElement).prev().hasClass('no-entry-required-wrapper')) {
      noEntryHeight = $(hot1.rootElement).prev().height();
    }

    // Top position of modal = top position of table, plus top position of td, but half of the td height minus half of the modal height
    var posTop = (hot1.rootElement.offsetParent.offsetTop + noEntryHeight + editor.TD.offsetTop + tdHeightHalf - modalHeightHalf);
    // By default we want the modal to display to the right of the cell selected.
    // Left position of popup = left position of table, plus td left position, plus the width of the td, plus 15 px space for the arrow.
    var posLeft = hot1.rootElement.offsetParent.offsetLeft + editor.TD.offsetLeft + editor.TD.offsetWidth + 15;
    // Determine if the modal should be to the left of the td instead (because there is not enough room to the right of the cell).
    // Get the amount of space to the right of the TD. Add the width of the td to the left position of the td (to get it's right position),
    // and subtract that from the parent element's width.
    var rightSpace = hot1.rootElement.offsetWidth - (editor.TD.offsetLeft + editor.TD.offsetWidth);
    // If there isn't space for the modal to display to the right of the cell, change the left position and arrow class.
    if ($('#tbs-entry-popup').width() + 15 > rightSpace) {
      // add arrow class
      arrow = 'arrow-right';
      // change position
      posLeft = (hot1.rootElement.offsetParent.offsetLeft + editor.TD.offsetLeft) - $('#tbs-entry-popup').width() - 15;

      // In the case of a full width input, there may not be enough space on the left side either. In this case, the
      // best we can do is offset the position enough so that the modal is within the boundary of the page.
      if(posLeft < 0) {
        posLeft = 0;
      }
    }
    // Set position of popup and add class.
    $('#tbs-entry-popup').css({position: 'absolute', top: posTop + 'px', right: 'auto', left: posLeft + 'px', bottom: 'auto' }).addClass(arrow);

    // After modal is hidden
    $('#tbs-entry-popup').on('hidden.bs.modal', function (e) {
      // remove class from body
      $('body').removeClass('popup-open');
      // remove arrow class from popup
      $('#tbs-entry-popup').removeClass('arrow-left').removeClass('arrow-right');
      setTimeout(function() {

        // Make all of the grids writeable again.
        window.enableAllGrids();

        // Enable the scrolling div.
        // Adding this here as a safeguard in case it doesn't fire
        // in afterSelectionEnd (which is the case for ipads).
        window.enableScrollingDiv();
      }, 500);
      // For some reason this function fires multiple times after the modal has been opened more than once.
      // This is causing issues with no entry required grids becoming editable.
      // Adding this line will prevent it.
      $(this).off('hidden.bs.modal');
    });

    // if an answer was already input:
    if (editor.originalValue) {
      // enable reset button
      $('.tbs-entry-reset-btn').removeClass('disabled');
      // enable Accept/OK button
      $('.tbs-entry-ok-btn').removeClass('disabled');
      // put original answer in the input field
      $('.tbs-entry-input').val(formatValue(editor.originalValue));
      // put the original answer in the formatted entry display
      $('.entry-formatted-entry').text(formatValue(editor.originalValue));
    }
    // text input should be empty if nothing was previously entered
    else {
      $('.tbs-entry-input').val('');
      $('.entry-formatted-entry').text('');
    }

    // Formatted response display
    $('.tbs-entry-input').keyup(function(evt) {
      // enable OK/Accept button
      $('.tbs-entry-ok-btn').removeClass('disabled');
      // format as number
      inputText = evt.target.value;
      // stick the input value in the formatted response div
      $('.entry-formatted-entry').text(formatValue(inputText, editor.cellProperties.format_type));

    });

    // Reset button
    $('.tbs-entry-reset-btn').unbind().bind('click touchend', function () {
      // hide the reset button
      $(this).hide();
      // show the confirm reset button
      $('.tbs-entry-confirm-btn').removeClass('disabled').show();
    });

    // Confirm reset button
    $('.tbs-entry-confirm-btn').unbind().bind('click touchend', function () {
      // Hide the confirm reset button.
      $(this).hide();
      // Show the reset button and add disabled class
      $('.tbs-entry-reset-btn').addClass('disabled').show();
      // Add disabled class to OK/Accept button
      $('.tbs-entry-ok-btn').addClass('disabled');
      // Empty the answer input
      $('.tbs-entry-input').val('');
      $('.entry-formatted-entry').text('');
      // Set the answer to empty.
      hot1.setDataAtCell(row, col, '');
      // Hide the modal.
      $('#tbs-entry-popup').modal('hide');
    });

    // Cancel button - and close modal button
    $('.tbs-entry-cancel-btn').unbind().bind('click touchend', function () {
      // If reset button was clicked first, confirm-reset button needs to be hidden
      // and reset button needs to be shown. Add disabled classes to both.
      if ($('.tbs-entry-confirm-btn').not('.disabled')) {
        $('.tbs-entry-confirm-btn').addClass('disabled').hide();
        $('.tbs-entry-reset-btn').addClass('disabled').show();
      }
      // Set the answer to the original value or leave blank.
      if (editor.originalValue) {
        hot1.setDataAtCell(row, col, editor.originalValue);
      }
      else {
        hot1.setDataAtCell(row, col, '');
      }
      // Hide the modal.
      $('#tbs-entry-popup').modal('hide');
    });

    // OK/Accept button
    $('.tbs-entry-ok-btn').unbind().bind('click touchend', function () {
      // If reset button was clicked first, confirm-reset button needs to be hidden
      // and reset button needs to be shown. Add disabled classes to both.
      if ($('.tbs-entry-confirm-btn').not('.disabled')) {
        $('.tbs-entry-confirm-btn').hide().addClass('disabled');
        $('.tbs-entry-reset-btn').show().addClass('disabled');
      }
      // Update the formatted text to the correct format.
      var input_value = numeral($('.entry-formatted-entry').text()).format('0,0');
      // Set the answer to the formatted text.
      hot1.setDataAtCell(row, col, input_value);
      // disable OK/Accept button
      $(this).addClass('disabled');
      // Hide the modal.
      $('#tbs-entry-popup').modal('hide');
    });

  };


  /**
   * Create a modal with answer options for TBS questions and TBS Journal dropdown questions.
   * Includes functionality for buttons in footer of modal.
   * HTML for Modal is created in form--question-display.tpl.php.
   * @param row
   * @param col
   * @param hot1
   */
  window.answerSelectionPopup = function (row, col, hot1) {

    // Store the current instance for later use (when the modal is hidden).
    // If we are on review page we need to avoid opening the answer popup - it doesn't exist on review page but exists on view solution modal.
    var gid =  $(hot1.rootElement).data('gid') + ""; // convert possible number to str
    var isReviewPage =  gid.indexOf('-review') != -1  ;
    var that = hot1;
    // Don't allow answer popups when the grid is read-only
    if(hot1.getSettings().readOnly || isReviewPage ) {
      return;
    }
    // Don't allow popups from two separate grids to be open at the same time.
    if ($('#tbs-entry-popup').is(":visible") || $('#tbs-answer-popup').is(":visible")) {
      return;
    }
    $('#tbs-answer-popup').attr("role", "dialog");

    // Empty the modal body.
    $('#tbs-answer-popup .modal-body').html('');


    $('#tbs-answer-popup').modal();

    var originalValue = hot1.getDataAtCell(row, col);

    // make all of the grids read only while the modal is open
    window.disableAllGrids();

    // add class to body for styling the hot table
    $('body').addClass('popup-open');

    var options = "<ul class='ipad-tbs-answer-list'>";
    // Get the TBS answer list for the selected cell and put those options into a popup
    // $.each(hot1.getActiveEditor().cellProperties.source, function(i, val) {
    $.each(hot1.getCellMeta(row, col).source, function (i, val) {
      if (i > 0) {
        // Options begin from index #1, not 0

        // If the cell clicked on already has an answer in it,
        // that answer should be selected
        var option_selected = (originalValue == val ? ' selected' : '');
        options += '<li class="ipad-tbs-answer-option' + option_selected + '" ><a tabindex="0">' + val + '</a></li>';
      }
    });
    options += "</ul>";
    $('#tbs-answer-popup .modal-body').html(options);
    $('#tbs-answer-popup .modal-title').text('Select an option below');
    $('#tbs-answer-popup').addClass('tbs-popup');


    //hot1.getActiveEditor().beginEditing();

    $('.hot-ipq-cancel-btn').attr("tabindex", "0");
    $('.hot-ipq-ok-btn').addClass("disabled");

    // reset button should be enabled if answer was previously chosen
    if (originalValue) {
      $('.hot-ipq-reset-btn').removeClass('disabled');
      $('.hot-ipq-ok-btn').removeClass("disabled");
    }
    // Answer option
    $('.ipad-tbs-answer-option').bind('click touchend', function () {
      $('.ipad-tbs-answer-option.selected').removeClass('selected');
      $(this).addClass('selected');
      $('.hot-ipq-ok-btn').removeClass('disabled');
      // Set the answer to the selected text.
      // ** I think this was causing errors in Safari.
      // Commenting out, the data is still set when OK button is clicked. **
      //hot1.setDataAtCell(row, col, $(this).text());
    });

    // Reset button
    $('.hot-ipq-reset-btn').unbind().bind('click touchend', function () {
      // Add reset-selected class to selected answer opton.
      // This class is used later if the reset is canceled rather than confirmed.
      $('.ipad-tbs-answer-option.selected').addClass('reset-selected');
      // hide the reset button
      $(this).hide();
      // show the confirm reset button
      $('.hot-ipq-confirm-btn').removeClass('disabled').show();
    });

    // Confirm reset button
    $('.hot-ipq-confirm-btn').unbind().bind('click touchend', function () {
      // Remove classes from answer option.
      $('.ipad-tbs-answer-option').removeClass('selected').removeClass('reset-selected');
      // Hide the confirm reset button.
      $(this).hide();
      // Show the reset button and add disabled class
      $('.hot-ipq-reset-btn').addClass('disabled').show();
      // Add disabled class to OK/Accept button
      $('.hot-ipq-ok-btn').addClass('disabled');
      // Set the answer to empty.
      hot1.setDataAtCell(row, col, '');
      // Hide the modal.
      $('#tbs-answer-popup').modal('hide');
    });

    // Cancel button - and close modal button
    $('.hot-ipq-cancel-btn, #tbs-answer-popup .close').unbind().bind('click touchend', function () {
      // If reset button was clicked first, confirm-reset button needs to be hidden
      // and reset button needs to be shown. Add disabled classes to both.
      if ($('.hot-ipq-confirm-btn').not('.disabled')) {
        $('.hot-ipq-confirm-btn').addClass('disabled').hide();
        $('.hot-ipq-reset-btn').addClass('disabled').show();
        $('.ipad-tbs-answer-option.reset-selected').addClass('selected').removeClass('reset-selected');
      }
      // Set the answer to the original value or leave blank.
      if (originalValue) {
        hot1.setDataAtCell(row, col, originalValue);
      }
      // Hide the modal.
      $('#tbs-answer-popup').modal('hide');
    });


    // OK/Accept button
    $('.hot-ipq-ok-btn').unbind().bind('click touchend', function () {
      // If reset button was clicked first, confirm-reset button needs to be hidden
      // and reset button needs to be shown. Add disabled classes to both.
      if ($('.hot-ipq-confirm-btn').not('.disabled')) {
        $('.hot-ipq-confirm-btn').hide().addClass('disabled');
        $('.hot-ipq-reset-btn').show().addClass('disabled');
        $('.ipad-tbs-answer-option.reset-selected').addClass('selected').removeClass('reset-selected');
      }
      // Set the answer to the selected text.
      hot1.setDataAtCell(row, col, $('.ipad-tbs-answer-option.selected').text());
      // Hide the modal.
      $('#tbs-answer-popup').modal('hide');
    });
    // After modal is hidden
    $('#tbs-answer-popup').on('hidden.bs.modal', function (e) {
      // remove class from body
      $('body').removeClass('popup-open');
      setTimeout(function() {

        // Make all of the grids writeable again.
        window.enableAllGrids();

        // Enable the scrolling div.
        // Adding this here as a safeguard in case it doesn't fire
        // in afterSelectionEnd (which is the case for ipads).
        window.enableScrollingDiv();
      }, 500);
      // For some reason function fires multiple times after the modal has been opened more than once.
      // This is causing issues with no entry required grids becoming editable.
      // Adding this line will prevent it.
      $(this).off('hidden.bs.modal');
    });

    $('#tbs-answer-popup .modal-content').draggable({
      handle: '.modal-header',
      containment: 'window',
      revert: false,
      clone: 'helper',
      appendTo: 'body',
      scroll: false,
    });
    // position the modal
    $('#tbs-answer-popup .modal-content').css('left', (($(window).width() - $('#tbs-answer-popup .modal-content').width()) / 2));
  };

  /**
   * Disable a handsontable and make it read only
   * @param hot
   * - Instance of hot
   */
  window.disableHot = function(hot) {
    hot.updateSettings({
      readOnly: true, // make table cells read-only
      contextMenu: false, // disable context menu to change things
      disableVisualSelection: true, // prevent user from visually selecting
      manualColumnResize: false, // prevent dragging to resize columns
      manualRowResize: false, // prevent dragging to resize rows
      comments: false // prevent editing of comments
    });
  };

  /**
   * Enable a handsontable to make it writable again
   * @param hot
   * - Instance of hot
   */
  window.enableHot = function(hot) {
    hot.updateSettings({
      readOnly: false, // make table cells read-only
      contextMenu: true, // disable context menu to change things
      disableVisualSelection: false, // prevent user from visually selecting
      manualColumnResize: true, // prevent dragging to resize columns
      manualRowResize: true, // prevent dragging to resize rows
      comments: true // prevent editing of comments
    });
  };
  /**
   * Disable all handsontables to make them read only
   */
  window.disableAllGrids = function() {
    $.each(hotVariables, function(index, hot) {
      var instance = hot.spreadsheet.getInstance();
      // If the table has 'no entry required' checked, do nothing.
      var $noEntryCheckbox = $(instance.rootElement).closest('.question-table').find('.no-entry-required');
      if ($noEntryCheckbox.length && $noEntryCheckbox.prop('checked')) {
        return;
      }
      // Only enable grids with numeric ids (we want to ignore show results grids--
      // they will have "show_results" as part of their grid id.).
      if (hot.hotId.match(/^[0-9]+$/) != null) {
        window.disableHot(instance);
      }
    });
  };
  /**
   * Enable all handsontables to make them writable again
   */
  window.enableAllGrids = function() {
    $.each(hotVariables, function(index, hot) {
      var instance = hot.spreadsheet.getInstance();
      // If the table has 'no entry required' checked, do nothing.
      var $noEntryCheckbox = $(instance.rootElement).closest('.question-table').find('.no-entry-required');
      if ($noEntryCheckbox.length && $noEntryCheckbox.prop('checked')) {
        return;
      }
      // Only enable grids with numeric ids (we want to ignore show results grids--
      // they will have "show_results" as part of their grid id.).
      if (hot.hotId.match(/^[0-9]+$/) != null) {
        window.enableHot(instance);
      }
    });
  };

  Drupal.behaviors.rcpar_ipq_hot_general_settings = {


    applyCellStyles: function (instance, row, col, td) {
      // lets get the ipq settings for this cell
      // (we are setting this in the metadata when creating the HOT instance)
      var ipq_cell_settings = instance.getCellMeta(row, col, 'ipq').ipq;
      if (ipq_cell_settings && ipq_cell_settings.styles) {
        $.each(ipq_cell_settings.styles, function (index, value) {
          switch (value) {
            case 'bold':
              td.style.fontWeight = 'bold';
              break;
            case 'italic':
              td.style.fontStyle = 'italic';
              break;
            case 'underline':
              td.style.textDecoration = 'underline';
              break;
            default:
              /*
               // Commenting this out for now, it's not needed for the current changes,
               // but leaving in case this is useful later.
              if (typeof value == 'object') {
                if (value.background) {
                  if (value.background.color) {
                    td.style.background = value.background.color;
                  }
                }
                else if (value.foreground) {
                  if (value.foreground.color) {
                    td.style.color = value.foreground.color;
                  }
                }
              }*/
          }
        });
      }
    },

    hasRun: false,
    attach: function (context, settings) {
      // Preventing the attached code from being attached multiple times (which happens when the "Try Again"
      // button in the view solution and score as you go modals is clicked) because otherwise the hot is
      // re-instantiated and answers are lost. We're not using a once() because that could cause issues with scope.
      if (this.hasRun) {
        return;
      }


      var behaviours = this;

      function NumericCellRenderer(instance, td, row, col, prop, value, cellProperties) {
        // If no-entry-required checkbox has made the table read-only, we need to empty the cell.
        // Get the input checkbox within the no-entry-required-wrapper div.
        var $noEntryCheckbox = $(instance.rootElement).closest('.question-table').find('.no-entry-required');
        // If the checkbox exists and is checked, empty the cells.
        if ($noEntryCheckbox.length && $noEntryCheckbox.prop('checked')) {
          td.innerHTML = '';
          return;
        }
        Handsontable.renderers.NumericRenderer.apply(this, arguments);
        behaviours.applyCellStyles(instance, row, col, td);
        $(td).addClass('tbs-answer');
        $(td).addClass('htNumeric');
        if (value) {
          $(td).addClass('non-empty');
          td.innerHTML = value;
        }
        $(td).data('row', row);
        $(td).attr("aria-label", "Press enter to place your answer.");
        // this is needed for ipads
        $(td).attr("contenteditable", "true");

        // changes on hover on quiz take page
        if (window.location.href.indexOf('take') > -1) {
          Handsontable.dom.addEvent(td, 'mouseover', function (event) {
            // Don't allow hover stuff for readonly cells.
            var readOnly = $.inArray('htDimmed', td.classList) != -1;
            if (readOnly) {
              $(td).attr("contenteditable", "false");
              return;
            }
            // Do hover stuff.
            if ($('body').hasClass('popup-open') == false) {
              if (value) {
                td.innerHTML = value;
              }
              else {
                td.innerHTML = 'Click to answer';
              }
              $(td).addClass('dropdown-hover');
            }
          });
          // remove changes on mouseleave
          Handsontable.dom.addEvent(td, 'mouseleave', function (event) {
            // Don't allow hover stuff for readonly cells.
            var readOnly = $.inArray('htDimmed', td.classList) != -1;
            if (readOnly) {
              return;
            }
            // Do hover stuff.
            if ($('body').hasClass('popup-open') == false) {
              if (value) {
                td.innerHTML = value;
              }
              else {
                td.innerHTML = '';
              }
              $(td).removeClass('dropdown-hover');
            }
          });
        }
        return td;
      }

      function StaticCellRenderer(instance, td, row, col, prop, value, cellProperties) {
        Handsontable.renderers.TextRenderer.apply(this, arguments);
        behaviours.applyCellStyles(instance, row, col, td);
      }

      function DropdownCellRenderer(instance, td, row, col, prop, value, cellProperties) {
        var td_class = '';
        if (value) {
          td_class = ' non-empty';
          $(td).addClass(td_class);
        }
        // dropdown cells need some changes on hover on quiz take page
        if (window.location.href.indexOf('take') > -1) {
          // Don't do anything if we're in read only mode
          if (instance.getSettings().readOnly) {
            td.innerHTML = '';
          }
          Handsontable.dom.addEvent(td, 'mouseover', function (event) {
            if (!td.classList.contains('htDimmed')) {
              if (value) {
                td.innerHTML = value;
              }
              else {
                td.innerHTML = 'Click to select';
              }
              td.className = '';
              td.className = 'hot-input hot-input-dropdown dropdown-hover' + td_class;
            }
          });
          // remove changes on mouseleave
          Handsontable.dom.addEvent(td, 'mouseleave', function (event) {
            if (!td.classList.contains('htDimmed')) {
              if (value) {
                td.innerHTML = value;
              }
              else {
                td.innerHTML = '';
              }
              td.className = 'hot-input hot-input-dropdown' + td_class;
            }
          });
        }
        Handsontable.renderers.TextRenderer.apply(this, arguments);
        behaviours.applyCellStyles(instance, row, col, td);
      }

      // we are handling the percentages in a special way
      // we are changing the % per a $ sign, let HOT format it and then we change it back to a %
      // on rendering (that allow us to use the HOT formats for the percentages in the same way than
      // for the currencies
      function PercentCellRenderer(instance, td, row, col, prop, value, cellProperties) {
        // If no-entry-required checkbox has made the table read-only, we need to empty the cell.
        // Get the input checkbox within the no-entry-required-wrapper div.
        var $noEntryCheckbox = $(instance.rootElement).closest('.question-table').find('.no-entry-required');
        // If the checkbox exists and is checked, empty the cells.
        if ($noEntryCheckbox.length && $noEntryCheckbox.prop('checked')) {
          td.innerHTML = '';
          return;
        }

        var td_class = '';
        var cellHasAnswer = false;
        if (value) {
            td_class = ' non-empty';
            $(td).addClass(td_class);
            cellHasAnswer = true;
        }
        $(td).addClass('htNumeric');

        value = numeral(value).format(cellProperties.format || '0'); //docs: http://numeraljs.com/
        // Remove the '$' symbol and attach '%' after the value
        value = value.replace('$', '');
        value = value + "%";

        // Don't do anything if we're in read only mode
        if(instance.getSettings().readOnly) {
          if (value) {
              td.innerHTML = value;
          }
          // To make it look as the other columns
          $(td).addClass('htDimmed');
          $(td).addClass('htNumeric');
          return;
        }
        // this is needed for ipads
        $(td).attr("contenteditable", "true");
        if (typeof cellProperties.language !== 'undefined') {
          numeral.language(cellProperties.language);
        }
        // Check if user is trying to use math operations
        var stringValue = value.toString();
        if (stringValue.startsWith("=")) {
          value = PercentCellOperationProcess(value);
        }

        while (child = td.lastChild) {
          td.removeChild(child);
        }
        $(td).addClass('htNumeric');

        // percent cells need some changes on hover on quiz take page
        if (window.location.href.indexOf('take') > -1) {
          Handsontable.dom.addEvent(td, 'mouseover', function (event) {
            // Don't allow hover stuff for readonly cells.
            var readOnly = $.inArray('htDimmed', td.classList) != -1;
            if (readOnly) {
              if (cellHasAnswer) {
                td.innerHTML = value;
              }
              else {
                td.innerHTML = '';
              }
              $(td).attr("contenteditable", "false");
              return;
            }
            // Do hover stuff.
            if ($('body').hasClass('popup-open') == false) {
              if (cellHasAnswer) {
                td.innerHTML = value;
              }
              else {
                td.innerHTML = 'Click to answer';
              }
              $(td).addClass('dropdown-hover');
            }
          });
          // remove changes on mouseleave
          Handsontable.dom.addEvent(td, 'mouseleave', function (event) {
            // Don't allow hover stuff for readonly cells.
            var readOnly = $.inArray('htDimmed', td.classList) != -1;
            if (readOnly) {
              return;
            }
            // Do hover stuff.
            if ($('body').hasClass('popup-open') == false) {
              if (cellHasAnswer) {
                td.innerHTML = value;
              }
              else {
                td.innerHTML = '';
              }
              $(td).removeClass('dropdown-hover');
            }
          });
        }
        td.appendChild(document.createTextNode(value));
        behaviours.applyCellStyles(instance, row, col, td);
      }

      // Functionality for Math operations inside Percent Cells
      function PercentCellOperationProcess(value) {
        var operation = value.replace('=', '');
        var operationPercent = eval(operation) * 100;
        // Return result rounded up
        return Math.round(operationPercent * 100) / 100;
      }

      // registering the renderers
      if (!Handsontable.renderers.NumericCellRenderer) {
        Handsontable.renderers.registerRenderer('NumericCellRenderer', NumericCellRenderer);
        Handsontable.renderers.registerRenderer('StaticCellRenderer', StaticCellRenderer);
        Handsontable.renderers.registerRenderer('PercentCellRenderer', PercentCellRenderer);
        Handsontable.renderers.registerRenderer('DropdownCellRenderer', DropdownCellRenderer);
        Handsontable.renderers.registerRenderer('HtmlRenderer', Handsontable.renderers.HtmlRenderer);
      }

      // we are creating a custom editor for the TBS answers cells
      // mostly to add some UI to the default dropdown display
      // but we also are changing it's behaviour a little bit
      var TBSEditor = Handsontable.editors.DropdownEditor.prototype.extend();

      // overriding the DropdownEditor.open function
      TBSEditor.prototype.open = function () {
        // Not sure why this is needed, but it is. I assume to use the hooks below.
      };

      // overriding the DropdownEditor.updateDropdownHeight function
      TBSEditor.prototype.updateDropdownHeight = function () {
        Handsontable.editors.DropdownEditor.prototype.updateDropdownHeight.apply(this, arguments);
        // we remove the height restriction to show our custom header and footer
        $('.handsontableEditor').css('height', 'auto');
        $('.wtHolder').css('height', 'auto');
        $('.wtHolder').css('width', 'auto');

        $('.wtHider').css('height', 'auto');
        $('.wtHider').css('width', 'auto');


        $('.handsontableInput').css('visibility', '');

        // we want the editor to be centered on the screen
        $('.handsontableEditor').css("position", "fixed");
        $('.handsontableEditor').css("top", 170 + "px");
        $('.handsontableEditor').css("left", Math.max(0, (($(window).width() - $('.handsontableEditor').outerWidth()) / 2) +
            $(window).scrollLeft()) + "px");

        // the empty value option always come first in the option list (we are sending it in that way from the php)
        // it's necesary for the Dropdown editor to work correctly, but we don't want to expose that option to the users
        $('.handsontableEditor .wtSpreader tbody tr:first').hide();

        // Handsontable adds a strong tag on the previously selected answer
        // we need to remove it for correct styling
        $('.handsontableEditor .wtSpreader tbody tr strong').each(function (index) {
          $(this).replaceWith($(this).html());
        });

      };

      // overriding the DropdownEditor.prepare function
      TBSEditor.prototype.prepare = function (row, col, prop, td, value, cellProperties) {
        Handsontable.editors.DropdownEditor.prototype.prepare.apply(this, arguments);
        // Commenting this out because it's causing issues in Chrome with answer modals not
        // triggering in the afterSelectionEnd hook. It doesn't appear to be used for anything now.
        //$(td).wrapInner("<div class='cell-content-container'></div>");
        var parent = this;
        this.htOptions.afterOnCellMouseDown = function () {
          var value = this.getValue();
          // if the value is undefined then it means we don't want to set the value
          if (value !== void 0) {
            parent.preselected = value;
          }
        };
      };

      TBSEditor.prototype.close = function () {
        this.setValue(this.preselected);

        // The next line seems to do not be necessary anymore
        // Handsontable.editors.DropdownEditor.prototype.close.apply(this, arguments);
      };

      // registering the editor
      Handsontable.editors.registerEditor('TBSEditor', TBSEditor);

      this.hasRun = true;
    }
  }

}(jQuery));
