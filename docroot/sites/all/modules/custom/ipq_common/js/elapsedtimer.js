window.DigitalTimer = function (settings) {
  this.timerSettings = settings;
  this.pausedDate = new Date();

  this.getTimeElapsed = function () {
    this.timerMinutesElapsed = Math.floor((((new Date().getTime() - new Date(this.pastTime.getTime())) % 86400000) % 3600000) / 60000);
    this.timerHoursElapsed = Math.floor(((new Date().getTime() - new Date(this.pastTime.getTime())) % 86400000) / 3600000);
    this.timerSecondsElapsed = Math.floor(((((new Date().getTime() - new Date(this.pastTime.getTime())) % 86400000) % 3600000) % 60000) / 1000);
    this.elapsedInterval = setTimeout(this.getTimeElapsed.bind(this), 1000);
    this.handleElapsedTimerUI();
  };

  this.handleElapsedTimerUI = function () {
    jQuery(this.timerSettings.selector + ' .hours-elapsed').html(this.timerHoursElapsed);
    jQuery(this.timerSettings.selector + ' .minutes-elapsed').html(this.timerMinutesElapsed);
    var seconds = this.timerSecondsElapsed;
    seconds = ("00" + seconds).slice(-2);
    jQuery(this.timerSettings.selector + ' .seconds-elapsed').html(seconds);
    if (this.timerSettings.tickHandler) {
      this.timerSettings.tickHandler();
    }
  }

  this.setTimer = function () {
    this.pastTime = new Date();
    this.getTimeElapsed();
  };

  this.setCountdown = function (obj) {
    this.curTime = new Date().getTime();
    this.futureTime = new Date();

    if (this.timerSettings.hours) {
      this.timerSettings.hours = parseInt(this.timerSettings.hours);
      this.curHour = new Date().getHours();
      this.futureTime.setHours(this.curHour + this.timerSettings.hours);
      if (this.curTime >= this.futureTime.getTime()) {
        console.log('cannot set countdown to time in the past');
        return false;
      }
      this.futureHour = this.futureTime.getHours();
    }
    if (this.timerSettings.minutes) {
      this.timerSettings.minutes = parseInt(this.timerSettings.minutes);
      this.curMin = new Date().getMinutes();
      this.futureTime.setMinutes(this.curMin + this.timerSettings.minutes);
      if (this.curTime >= this.futureTime.getTime()) {
        console.log('cannot set countdown to time in the past');
        return false;
      }
      this.futureMin = this.futureTime.getMinutes();
    }
    if (this.timerSettings.seconds) {
      this.timerSettings.seconds = parseInt(this.timerSettings.seconds);
      this.curSec = new Date().getSeconds();
      this.futureTime.setSeconds(this.curSec + this.timerSettings.seconds);
      if (this.curTime >= this.futureTime.getTime()) {
        console.log('cannot set countdown to time in the past');
        return false;
      }
      this.futureSec = this.futureTime.getSeconds();
    }

    this.getTimeRemaining();
    this.hideUnusedElements();
  };

  this.resetTimer = function () {
    this.pastTime = new Date();
    this.timerHoursElapsed = 0;
    this.timerMinutesElapsed = 0;
    this.timerSecondsElapsed = 0;
    clearTimeout(this.elapsedInterval);
    this.handleElapsedTimerUI();
  };

  this.pauseTimer = function () {
    clearTimeout(this.elapsedInterval);
    this.pausedDate = new Date();
    return this.pausedDate;
  };

  this.unpauseTimer = function () {
    var timeDiff = new Date() - this.pausedDate;
    this.pastTime.setTime(this.pastTime.getTime() + timeDiff);
    this.getTimeElapsed();
    return timeDiff;
  };

  this.resetCountdown = function () {
    clearTimeout(this.timeRemainingInterval);
  };

  this.getTimerMinutesElapsed = function () {
    return this.timerMinutesElapsed;
  };

  this.getTimerHoursElapsed = function () {
    return this.timerHoursElapsed;
  };

  this.getTimerSecondsElapsed = function () {
    return this.timerSecondsElapsed;
  };

  this.getTimerMinutesRemaining = function () {
    return this.timerMinutesRemaining;
  }

  this.getTimerHoursRemaining = function () {
    return this.timerHoursRemaining;
  }

  this.handleTimerDone = function () {
    this.resetCountdown();
    this.resetTimer();
    if (this.timerSettings.callback) {
      try {
        this.timerSettings.callback();
      } catch (err) {
        console.log("Timer error: " + err);
      }
    }
    ;
  }

  this.checkIfTimerDone = function () {
    if (new Date().getTime() >= this.futureTime.getTime()) {
      this.handleTimerDone();
      return true;
    }
    return false;
  }

  this.examSimulatorTimerStart = function () {
    this.resetCountdown();
    this.setCountdown(this.timerSettings);
  };

  this.quizTimerStart = function () {
    this.resetTimer();
    this.setTimer();
  };

  this.hideUnusedElements = function () {
    if (!this.timerSettings.hours && !this.timerSettings.showHours) {
      jQuery(this.timerSettings.selector + ' .hours-remaining').hide();

      if (!this.timerSettings.minutes && !this.timerSettings.showMinutes) {
        jQuery(this.timerSettings.selector + ' .minutes-remaining').hide();

      }
    }

    if (!this.timerSettings.seconds && !this.timerSettings.showSeconds) {
      jQuery(this.timerSettings.selector + ' .seconds-remaining').hide();
    }
  };
};


(function ($) {
  var elapsed_timer ;
  Drupal.behaviors.rcpar_elapsedtimer = {
    set_elapsed: function(seconds){
      var miliseconds = seconds * 1000;
      elapsed_timer.pastTime = new Date(miliseconds);
      elapsed_timer.timerMinutesElapsed = elapsed_timer.pastTime.getMinutes();
      elapsed_timer.timerHoursElapsed = Math.floor(( miliseconds % 86400000) / 3600000);
      elapsed_timer.timerSecondsElapsed = elapsed_timer.pastTime.getSeconds();
      elapsed_timer.handleElapsedTimerUI();
      elapsed_timer.pauseTimer();
    },
    get_elapsed: function () {
      return elapsed_timer;
    },
    stop_elapsed: function(){
      elapsed_timer.pauseTimer();
    },
    attach: function (context, settings) {
      if(typeof elapsed_timer === 'undefined'){
        elapsed_timer = new DigitalTimer({
          selector: '#digital-timer',
          tickHandler: function () {
            // this was used to hide hours and seconds if they didn't exist,
            // but we want to display both always now. Leaving in case this needs to revert.

            //if (elapsed_timer.getTimerHoursElapsed() == 0) {
            //  jQuery('#digital-timer').addClass('no-hours');
            //}
            //else {
            //  jQuery('#digital-timer').removeClass('no-hours');
            //  jQuery('#digital-timer').addClass('no-seconds');
            //}
            /*
             if (elapsed_timer.getTimerMinutesElapsed() == 0) {
             jQuery('#digital-timer').addClass('no-minutes');
             }
             else {
             jQuery('#digital-timer').removeClass('no-minutes');
             jQuery('#digital-timer').addClass('no-seconds');
             }
             */
          }
        });

//  elapsed_timer.resetTimer();
        elapsed_timer.quizTimerStart();

        var questionStartTime = new Date();
        var timeDiff = 0;
        //var prevQuestionDurations = [];
        //if (Cookies.get('sessiontimer') !== undefined) {
        if (Drupal.settings.ipq_sessiontimer) {
          //var timeobj = Cookies.getJSON('sessiontimer');
          var timeobj = Drupal.settings.ipq_sessiontimer;
          if (timeobj.durations) {
            //var prevQuestionDurations = timeobj.durations;
          }

          if (timeobj.session_id !== Drupal.settings.ipqSessionId) {
            var value = {'session_id': Drupal.settings.ipqSessionId};
            jQuery('#timer_info').val(JSON.stringify(value));
            //Cookies.set('sessiontimer', value);
            //var prevQuestionDurations = [];
          }
        } else {
          var value = {'session_id': Drupal.settings.ipqSessionId};
          //Cookies.set('sessiontimer', value);
          jQuery('#timer_info').val(JSON.stringify(value));
          //var prevQuestionDurations = [];
        }

        var update_timer_info = function () {
          var timer = Drupal.behaviors.rcpar_elapsedtimer.get_elapsed();
          // Get the Spent time in the timer
          var currentQuestionDurationSeconds = (timer.timerHoursElapsed * 3600) + (timer.timerMinutesElapsed * 60) + timer.timerSecondsElapsed;
          //prevQuestionDurations.push(currentQuestionDurationSeconds);
          var value = {
            'session_id': Drupal.settings.ipqSessionId,
            'duration': currentQuestionDurationSeconds
          };
          //Cookies.set('sessiontimer', value);
          jQuery('#timer_info').val(JSON.stringify(value));
          return false;
        };

        // Register a custom event that allows other code to invoke a timer update
        jQuery(document).on('IpqUpdateTimer', function (event) {
          update_timer_info();
        });

        jQuery('#edit-score-as-you-go-info-submit').mousedown(function (event) {
          update_timer_info();
          $(this).click();
        });

        // when the form is submited, we need to update the timer info in the form hidden input
        jQuery('#ipq-common-session-navigation-form').submit(function (event) {
          update_timer_info();
        });

        jQuery('.pause-timer').on('click', function () {
          elapsed_timer.pauseTimer();
          jQuery('.paused-screen').css('display', 'block');
          jQuery('html').addClass('paused');
          jQuery('.page-wrapper').css('display', 'none');
        });

        jQuery('.unpause-timer').on('click', function () {
          var newTimeDiff = elapsed_timer.unpauseTimer();
          timeDiff = timeDiff + newTimeDiff;
          jQuery('.paused-screen').css('display', 'none');
          jQuery('html').removeClass('paused');
          jQuery('.page-wrapper').css('display', 'block');
        });

        // Set the value of the elapsed timer to be the previous amount of time spent on the question
        if (Drupal.settings.ipqCurrentQuestionTime) {
          elapsed_timer.pastTime.setSeconds(elapsed_timer.pastTime.getSeconds() - Drupal.settings.ipqCurrentQuestionTime);
        }
        
      }
    }, // attach
  }; // Drupal.behaviors.rcpar_elapsedtimer
}(jQuery));
