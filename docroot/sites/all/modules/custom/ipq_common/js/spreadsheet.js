jQuery(document).ready(function() {
    window.Spreadsheet = function() {
      this.container = document.getElementById('spreadsheet-table');

      this.init = function() {
        var that = this;
        hot_config = {
          minCols: 20,
          formulas: true,
          minRows: 10,
          contextMenu: true,
          startCols: 26,
          startRows: 50,
          maxCols: 26,
        };
        hot_config = addSpreadsheetSettings(hot_config);

        that.spreadsheet = new Handsontable(that.container, hot_config);

        initHotSpreadsheetVariables('spreadsheet-tool', that.spreadsheet);
        setSpreadsheetBtnHandlers();
      };

      this.processFormulaChange = function() {
        this.currentFormula = jQuery('.formula').val();
        if (this.currentCell[0] > -1 && this.currentCell[1] > -1) {
          this.spreadsheet.setDataAtCell(this.currentCell[0], this.currentCell[1], this.currentFormula);
        }
      };
    };

  });
