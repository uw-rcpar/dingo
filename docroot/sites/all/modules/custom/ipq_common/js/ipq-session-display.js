(function ($) {
  $(document).ready(function() {

    // After the show answer feature modal is hidden
    $('#show-correct-answer').on('hidden.bs.modal', function (e) {
      // If the "Don't show this again" checkbox is checked
      // record modal as seen in database
      if ($('#show-answer-popup-control').is(':checked')) {
        // Get the user's token
        var jwt = RCPAR.jwt.getJwt();
        RCPAR.modals.setModalData('show_correct_answer_feature', jwt);
      }
    });

    // Display modal for smaller screen resolutions if cookie has not
    // been set to never show it again.
    var showResolutionPopup = false;
    var cookieType = '';
    if ($(window).width() < 1366) {
      showResolutionPopup = true;
      cookieType = 'Window';
      $('#ipq-resolution-popup .modal-title').html('Your browser\'s window size is below the recommended minimum');
      $('#ipq-resolution-popup .modal-body p:first-child').html('For best results, maximize your browser window. The layout is usable at smaller window sizes, but may not be optimal.');
    }
    if (screen.width < 1366) {
      showResolutionPopup = true;
      cookieType = 'Resolution';
      $('#ipq-resolution-popup .modal-title').html('Your screen resolution is below the recommended minimum');
      $('#ipq-resolution-popup .modal-body p:first-child').html('For best results, use a screen resolution (the number of pixels displayed on your screen) of at least 1366 x 768. The layout is usable at smaller resolutions, but may not be optimal.');
    }
    // Determine if the user has asked to never see this popup again
    var modals_to_check = ['ipq_screensize_warning_Window', 'ipq_screensize_warning_Resolution'];
    if (RCPAR.modals.userHasSeenModals(modals_to_check)) {
      showResolutionPopup = false;
    }

    if (showResolutionPopup && !$.cookie('close' + cookieType + 'Popup')) {
      // If a modal is already open, don't show the screen resolution modal until the open modal is closed.
      // Needs a timeout because if a modal is already open, it takes some time to show up as visible.
      setTimeout(function() {
        if ($('.modal').is(':visible')) {
          // Get the ID of the open modal.
          var openModalID = $('.modal.in').attr('id');
          // When the modal is closed, show the screen resolution modal.
          $('#' + openModalID).on('hidden.bs.modal', function (e) {
            $('#ipq-resolution-popup').modal();
          });
        }
        // Otherwise just show the screen resolution modal.
        else {
          $('#ipq-resolution-popup').modal();
        }
        // After the screen resolution modal is hidden
        $('#ipq-resolution-popup').on('hidden.bs.modal', function (e) {
          // Set a cookie for the quiz session. This will be removed in ipq_session.inc when a new session is started.
          $.cookie('close' + cookieType + 'Popup', 1, { path: '/' });
          // If the "Don't show this again" checkbox is checked
          // record modal as seen in database
          if ($('#resolution-popup-control').is(':checked')) {
            // Get the user's token
            var jwt = RCPAR.jwt.getJwt();
            RCPAR.modals.setModalData('ipq_screensize_warning_' + cookieType, jwt);
          }
        });
      }, 500);
    }

    // by default browsers will focus on a form element on a page
    // but we want browsers to focus on the navigation for accessibilty
    // research questions have a select that gets focused on by default
    if ($('.research-select').length) {
      $('.research-select').blur();
      // focus on the logo in the nav instead
      $('#cp-logo').focus();
    }
    // function to calculate the height of fixed header elements
    // and position content accordingly
    var CalculateFixedHeaders = function () {
      var nav_height = 0;
      // add together the height of all divs with header-element class
      $('.header-element').each(function () {
        if ($(this).is(':visible')) {
          nav_height = $(this).outerHeight() + parseInt(nav_height);
        }
      });
      // for act quizzes, include the top position of the cp-nav
      if ($('.cp-nav').length) {
        nav_height = $('.cp-nav').offset().top + parseInt(nav_height);
      }
      // include the admin menu height if it exists on the page
      if ($('#admin-menu').length) {
        nav_height = $('#admin-menu').height() + parseInt(nav_height);
      }
      else if ($('body').hasClass('admin-menu')) {
        nav_height = parseInt(nav_height) + 30;
      }
      // position the question-nav div
      $('.question-nav').css({"top": nav_height});

      // add the height of the question-nav div for ipq-wrapper
      nav_height = $('.question-nav').height() + parseInt(nav_height);
      // if the page has drupal messages, add the height of the messages
      if ($('div.messages').length) {
        $('.messages').css({"margin-top": nav_height});
      }
      else {
        $('#ipq-wrapper').css({"padding-top": nav_height});
      }
    };

    // run the function
    CalculateFixedHeaders();

    // also run it if exam navbar is expanded
    $('.exam-pulldown').click(function(){
      // first add header-element class to revealed navbar to include it in calculations
      $('#ipq-navbar').addClass('header-element');
      CalculateFixedHeaders();
    });

    // also run if message has been closed
    $('.messages').on('closed.bs.alert', function() {
      setTimeout(function() {
        CalculateFixedHeaders();
      }, 10);
    });

    // adjust height of TBS question scrollable area
    var CalculateScroll = function () {
      if ($('.scrollable-area').length) {
        // will need the top position of scrollable area div
        var scrollable_area_pos = $('.scrollable-area').offset();
        // height of window/viewport minus top position of scrollable area minus previous next nav area
        var height_adjusted = $(window).height() - scrollable_area_pos.top - $('.previous-next-wrapper').outerHeight();
        // set the height
        $('.scrollable-area').css({"height": height_adjusted});
      }
    }

    // run the function
    CalculateScroll();

    // if the window has been resized, calculate headers and scroll
    $(window).bind('resize', function(e){
      window.resizeEvt;
      $(window).resize(function(){
        clearTimeout(window.resizeEvt);
        window.resizeEvt = setTimeout(function(){
          CalculateFixedHeaders();
          CalculateScroll();
        }, 250);
      });
    });

    // If the user has already seen the correct answer for this question, trigger the display of the
    // "show correct answer" feature
    if($('#mcq-correct-answer-shown').val()) {
      Drupal.behaviors.rcpar_ipq_session_display.show_correct_answer(false);
    }
  });

  if (Drupal.admin != undefined) {
    Drupal.admin.behaviors.fixedHeaders = function (context, settings, $adminMenu) {
      var nav_height = 0;
      // add together the height of all divs with header-element class
      $('.header-element').each(function () {
        if ($(this).is(':visible')) {
          nav_height = $(this).outerHeight() + parseInt(nav_height);
        }
      });
      // include the admin menu height if it exists on the page
      if ($('#admin-menu').length) {
        nav_height = $('#admin-menu').outerHeight() + parseInt(nav_height);
      }
      // position the question-nav div
      $('.question-nav').css({"top": nav_height});

      // add the height of the question-nav div for ipq-wrapper
      nav_height = $('.question-nav').outerHeight() + parseInt(nav_height);
      $('#ipq-wrapper').css({"padding-top": nav_height});
    };
  }
  Drupal.behaviors.rcpar_ipq_session_display = {

    modal_triggering_btn: null,
    skip_solution_modal: false,
    skip_skipped_questions_modal: false,
    skip_score_as_you_go_modal: false,
    midPointBreakHandler: null,

    // Each of the questions submodules is in charge of updating this variable when needed
    needsSaving: false,

    show_score_as_you_go_modal_if_needed: function(triggering_btn){
      if($('#score_as_you_go').val() && $('#is-disabled').val() != true && $('#is-deleted').val() != true){
        // if score as you go  feature is active

        this.modal_triggering_btn = triggering_btn;

        // the edit-next button still controls the submit of the form
        // we need to detect the case when the continue button was clicked on the modal
        // to know if we must continue with the submit or show the modal instead

        if(!this.skip_score_as_you_go_modal){
          var selected = $('input[name=possible_answers]:checked');
          if (selected.length > 0) {
            selectedVal = selected.val();
            var correct = $('#mcq-correct-answer').val() == selectedVal;
            if(correct){
              $('.try-again-btn').hide();
            }
          }

          $('#ipq-score-information').on('show.bs.modal', function () {
            // For some reason this is no longer required, I think related to changes in the view solution modal.
            // Commenting it out in case it is needed again for whatever reason. - SM
            // $('#ipq-score-information .modal-content').css('height',$( window ).height()*0.8);

            // Limit the draggable
            $('#ipq-score-information .modal-content').draggable({
              handle: '.modal-header',
              containment: 'window',
              revert: false,
              clone: 'helper',
              appendTo: 'body',
              scroll: false,
            });
            // position the ipq-score-information modal
            $("#ipq-score-information .modal-content").css("left", (($(window).width() - $("#ipq-score-information .modal-content").width()) / 2));
          });

          $('#score-as-you-go-info-container').html('loading...');
          // NOTE: we use mousedown instead of click, to trigger some special logic on
          // elapsedtimer.js before the info is submitted
          // elapsedtimer will trigger the click event after its done
          $('#edit-score-as-you-go-info-submit').trigger('mousedown');

          $('#ipq-score-information').modal('show');
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    },

    show_emulated_score_modal: function(){

      // show the emulated scoring info, similar look/feel to score_as_you_go_modal,
      // but less functions since it's just 'show/hide' on events

      $('#ipq-emulate-score-information').on('show.bs.modal', function () {
        $('.modal-content').css('height',$( window ).height()*0.8);
      });

      $('#emulate-score-info-container').html('loading...');
      $('#ipq-emulate-score-information').modal('show');
      return false;
    },
    
    show_solutions_modal_preview_display: function (triggering_btn) {
      // Verify if we are in the correct page.
      if ($('#is-disabled').val() != true && $('#is-deleted').val() != true) {
        // if view solution feature is active
        this.modal_triggering_btn = triggering_btn;

        // the edit-next button still controls the submit of the form
        // we need to detect the case when the continue button was clicked on the modal
        // to know if we must continue with the submit or show the modal instead
        if (!this.skip_solution_modal) {
          $('#ipq-view-solution').on('show.bs.modal', function () {
            $('#ipq-view-solution .modal-body').css('height', $(window).height() * 0.75);
            $('#ipq-view-solution .modal-header h3#termsLabel').text("View Solution");
          });
          // If the View solution modal has the 'view-solution-info-container' as id, then we need to perform
          // an ajax call to load result from server - otherwise means that static content is loaded there
          var $ajaxContentCt = $('#ipq-view-solution .modal-body #view-solution-info-container');
          if ($ajaxContentCt.size()) {
            // Show the modal.
            $('#ipq-view-solution .modal-body #view-solution-info-container').html('Loading...');
            $('#edit-view-solution-info-submit').click();
          }

          $('#ipq-view-solution').modal('show');
          $('#ipq-view-solution').draggable({
            handle: '.modal-header'
          });
          return false;
        } 
        else {
          $('#ipq-view-solution').draggable({
            handle: '.modal-header'
          });
          return true;
        }
      } 
      else {
        $('#ipq-view-solution').draggable({
          handle: '.modal-header'
        });
        return true;
      }
    },

    show_solutions_modal_if_needed: function(triggering_btn){
      if($('#view_solutions').val() && $('#is-disabled').val() != true && $('#is-deleted').val() != true){
        // if view solution feature is active

        this.modal_triggering_btn = triggering_btn;

        // the edit-next button still controls the submit of the form
        // we need to detect the case when the continue button was clicked on the modal
        // to know if we must continue with the submit or show the modal instead
        if(!this.skip_solution_modal){

          $('#ipq-view-solution').on('show.bs.modal', function () {
            $('#ipq-view-solution .modal-body').css('height',$( window ).height()*0.75);
            $('#ipq-view-solution .modal-header h3#termsLabel').text("View Solution");
          });
          // If the View solution modal has the 'view-solution-info-container' as id, then we need to perform
          // an ajax call to load result from server - otherwise means that static content is loaded there
          var $ajaxContentCt = $('#ipq-view-solution .modal-body #view-solution-info-container' );
          if ($ajaxContentCt.size()) {
            $('#ipq-view-solution .modal-body #view-solution-info-container').html('Loading...');
            $('#edit-view-solution-info-submit').click();
          }

          $('#ipq-view-solution').modal('show');
          $('#ipq-view-solution').draggable({
            handle: '.modal-header'
          });
          return false;
        } else {
          $('#ipq-view-solution').draggable({
            handle: '.modal-header'
          });
          return true;
        }
      } else {
        $('#ipq-view-solution').draggable({
          handle: '.modal-header'
        });
        return true;
      }
    },

    show_solutions_modal_cancel: function() {
      // Set the hidden input field #question_try_again's value to 1 to indicate that the user clicked Try Again
      $('#question_try_again').val(1);
      // Trigger a click so that this will be counted as an answer attempt
      $('#edit-score-as-you-go-info-submit').trigger('click');
      // Set the hidden input field back to 0, so that an extra attempt is not counted when the user clicks on the Continue button (as opposed to the Try Again button)
      $('#question_try_again').val(0);
      $('#ipq-view-solution').modal('hide');
      $('#force_question').val('');
      this.skip_solution_modal = false;
      this.skip_score_as_you_go_modal = false;
      this.skip_skipped_questions_modal = false;
      return true;
    },

    show_solutions_modal_continue: function() {
      this.skip_solution_modal = true;
      $('#ipq-view-solution').modal('hide');
      if(this.modal_triggering_btn == 'back'){
        $('#edit-back').click();
      } else {
        $('#edit-next').click();
      }
      return true;
    },

    score_as_you_go_modal_continue: function(){
      this.skip_score_as_you_go_modal = true;
      $('#ipq-score-information').modal('hide');
      if(this.modal_triggering_btn == 'back'){
        $('#edit-back').click();
      } else {
        $('#edit-next').click();
      }
      return true;
    },

    score_as_you_go_modal_cancel: function() {
      // Set the hidden input field #question_try_again's value to 1 to indicate that the user clicked Try Again
      $('#question_try_again').val(1);
      // Trigger a click so that this will be counted as an answer attempt
      $('#edit-score-as-you-go-info-submit').trigger('click');
      // Set the hidden input field back to 0, so that an extra attempt is not counted when the user clicks on the Continue button (as opposed to the Try Again button)
      $('#question_try_again').val(0);
      $('#ipq-score-information').modal('hide');

      return true;
    },

    score_as_you_go_modal_closed: function(){
      // this means that the modal was closed without using the button
      // when use the 'next question' button, the skip_score_as_you_go_modal var is set to true
      if (!this.skip_score_as_you_go_modal){
        // on this case, we want to reset the status of the solution modals
        this.skip_solution_modal = false;
        this.skip_skipped_questions_modal = false;
      }
      return true;
    },

    /**
     * Determines whether the user should see the correct answer and accompanying information,
     * or if an informational modal should show first
     */
    show_correct_answer_if_needed: function () {
      // Check if we should show the "Using the 'Show correct answer' feature" modal first
      if (!RCPAR.modals.userHasSeenModals(['show_correct_answer_feature'])) {
        // Show the modal and bind to its 'yes' button to show the correct answer
        $('#show-correct-answer').modal();
        $('#show-correct-answer .btn-confirm-show-answer').click(this.show_correct_answer);
      }
      // We don't need to show the modal first, show the correct answer
      else {
        this.show_correct_answer();
      }
    },

    show_correct_answer: function (createAnswerAttempt = true) {
      /*
      * NOTE: There is code in ipq-mcq-question.js that binds events to the "Show Correct Answer"
      * button and the "Yes" button ('.btn-confirm-show-answer') in the modal window.
      * It should probably be moved here, but leaving it as is for now.
      */

      // Update the value of the hidden form element that holds timer info.
      $(document).trigger('IpqUpdateTimer');

      // Once 'show correct answer' has been triggered, the timer should no longer be pausable
      $('.timer-pause').hide();

      // Make an ajax request by tapping into a hidden Drupal AJAX submit handler we've created
      // This will save the user's answer and make some session data.
      if(createAnswerAttempt) {
        $('#edit-show-correct-answer').trigger('click');
      }

      // Reveal answer stats
      $('.answer-rate').show();

      // Get the user's answer and determine if it's correct
      var selected = $('input[name=possible_answers]:checked');
      var correctAnswer = $('#mcq-correct-answer').val();
      selectedVal = selected.val();
      var isCorrect = correctAnswer == selectedVal;

      // Check if the user has answered at all...
      if (selected.length > 0) {
        // User has the correct answer:
        if (isCorrect) {
          selected.closest('.form-type-radio').prepend('<span class="correct-mark answer-indicator"><i class="fa fa-check"></i></span>');
        }
        // Incorrect answer:
        else {
          // Mark the user's answer as incorrect
          selected.closest('.form-type-radio').prepend('<span class="incorrect-mark answer-indicator"><i class="fa fa-close"></i></span>');

          // Mark the correct answer
          $('.answers input[value=' + correctAnswer + ']').closest('.form-type-radio').prepend('<span class="correct-mark answer-indicator"><i class="fa fa-check"></i></span>');
        }
      }
      // Unanswered question:
      else {
        // Show which answer was correct:
        $('.answers input[value=' + correctAnswer + ']').closest('.form-type-radio').prepend('<span class="correct-mark answer-indicator"><i class="fa fa-check"></i></span>');
      }

      // If question is using the new explanation field, display it
      if ($('#singular-explanation').length) {
        $('#singular-explanation').show();
      }
      // Otherwise use the old explanations fields.
      else {
        // Hide all of the individual explanations
        $('#answer-explanations').show();
      }

      // Remove the user's ability to change answers by disabling all other unselected options
      $('.answers :radio:not(:checked)').attr('disabled', true);

      // Remove the 'Show correct answer' button itself
      $('.show-correct-answer').remove();


      // Reveal the container that holds all explanations
      $('.answer-explanation-area').removeClass('hidden');
    },

    // The idea is to only have one of this objects instantiated at the same time
    getMidPointBreakHandler: function(modalId) {
      if (!this.midPointBreakHandler){
        this.midPointBreakHandler = new MidpointBreakHandler(this, modalId);
      }
      return this.midPointBreakHandler;
    },

    saveUserInput: function(){
      if (Drupal.behaviors.rcpar_ipq_session_display.needsSaving){
        Drupal.behaviors.rcpar_ipq_session_display.needsSaving = false;
        // We just need to trigger a click on the auto save button, Drupal will handle the rest
        $('#edit-autosave-input').click();
      }
    },

    shouldShowBreakOption: function(){
      return $('#should_show_mid_point_break').val();
    },

    shouldShowNonPausedBreak: function () {
      return $('#should_show_non_paused_break').val();
    },


    attach: function (context, settings) {
      var behaviours = this;

      // Close Excel modal if spreadsheet modal is opened.
      // This code is unnecessary on review pages and breaks if viewing a deleted question.
      if ($.cookie !== undefined) {
        $('#open-spreadsheet-link', context).click(function () {
          $('.modal-backdrop').hide();
          $('#ipq-open-excel').modal('toggle');
          // Set a cookie to remember if the spreadsheet tool has been used.
          $.cookie('useSpreadsheet', 1, { path: '/' });
          // Hide the link in the IPQ nav that opens the Excel text modal.
          $('.spreadsheet.tool-menu-item #spreadsheet-link').hide();
          // Show the link in the IPQ nav that opens the spreadsheet tool modal.
          $('.spreadsheet.tool-menu-item #nav-spreadsheet-link').show();
        });
        // If user has chosen to use the spreadsheet tool
        if ($.cookie('useSpreadsheet')) {
          // Hide the link in the IPQ nav that opens the Excel text modal.
          $('.spreadsheet.tool-menu-item #spreadsheet-link').hide();
          // Show the link in the IPQ nav that opens the spreadsheet tool modal.
          $('.spreadsheet.tool-menu-item #nav-spreadsheet-link').show();
        }
      }

      // Autosave handler
      $('body').once('ipq-session-processed', function(){
        if (Drupal.settings.ipq_autosave_interval > 0) {
          Drupal.behaviors.rcpar_ipq_session_display.needsSaving = false;
          window.setInterval(behaviours.saveUserInput, Drupal.settings.ipq_autosave_interval * 1000);
        }
      });

      // Submits a Quizlet or Testlet for final Save.
      $('#submit-test-link', context).once('confirm-processed', function(){
        // note that we are adding a namespace to this event handler
        // in case other modules need to identify this handler later
        $(this).on('click.submit-session',function(){
          // Sometimes we need to display the modal that indicates the user that he skipped some questions
          // (skip_skipped_questions_modal indicates if we already displayed it and its used to control
          // that we don't display it more than once per click on submit-test-link
          // as the continue button on that modal calls this method). Exams only.
          if($('#ipq-session-type').val() == 'exam') {
            if ($('input[name=testlet_have_skipped_questions]').val() == 1 && !behaviours.skip_skipped_questions_modal){
              $('#ipq-skipped-modal').modal();
              return false;
            }
          }

          var confirmed;
          if ($('input[name=last_question_in_test]').val() == true){
            if($('#ipq-session-type').val() == 'exam'){
              confirmed = confirm("Exiting this question will end the exam.\nAre you sure you want to continue?");
            } else {
              confirmed = confirm("Exiting this question will end the test.\nAre you sure you want to continue?");
            }
          } else if ($('#is-last-quizlet').val() == true){
            if($('#ipq-session-type').val() == 'exam'){
              confirmed = confirm("Exiting this section will take you out of the exam.\nAre you sure you want to continue?");
            } else {
              confirmed = confirm("Exiting this section will take you out of the test.\nAre you sure you want to continue?");
            }
          } else {
            if($('#ipq-session-type').val() == 'exam') {
              confirmed = confirm("Are you sure you want to exit this part of the test?");
            } else {
              confirmed = confirm("Are you sure you want to exit this part of the quiz?");
            }
          }

          if (confirmed){
            $('#force_question').val('skip-set');

            // In case we can offer the student the option to take a break, we do it now
            // otherwise, we just continue.

            var breakType = '';
            var modalId = '';
            // The non-paused timer break modal.
            if (behaviours.shouldShowNonPausedBreak()) {
              breakType = 'non-paused-break';
              modalId = '#ipq-non-paused-break-timer-popup';
            }
            // The paused timer break modal.
            if (behaviours.shouldShowBreakOption()) {
              breakType = 'paused-break';
              modalId = '#ipq-break-timer-popup';
            }
            var breakHandler = behaviours.getMidPointBreakHandler(modalId);
            // If we need a break, start the modal magic.
            if (breakType){
              breakHandler.startBreak(breakType);
              // Add a body class so the page knows the modal is open.
              $('body').addClass('break-modal-open');
              // Hide the ipq tool icons while the modal is open.
              $('#ipq-header .tool-menu').hide();
              // Hide the submit testlet button while the modal is open.
              $('.submit-testlet.tool-menu-item').hide();
              // We wait for the break to finish before we can continue
              $('body').on('ipq-break-finished', function(){
                $("#edit-next").click();
                $('body').removeClass('break-modal-open');
              });
            }
            else {
              $("#edit-next").click();
            }
          }
          else {
            // We might need to re show the skipped questions modal
            behaviours.skip_skipped_questions_modal = false;
          }
          return false;
        });
      });

      // Set Reminder for a given Question
      // Without using 'once' here, this will fire twice.
      $('.bookmark-ipq-flag', context).once('processed', function(){
        $(this).click(function(){
          // Note that we are changing two buttons here
          // the one on the footer, and the one on the testlet overview modal
          var button = $('.bookmark-ipq-flag[qid='+$(this).attr('qid')+']');
          var question_id = button.attr('qid');
          var session_id = button.attr('session');
          var button_text = button.html();
          console.log('here');
          button.html("" + button_text + '<div class="ajax-progress"><div class="glyphicon glyphicon-refresh glyphicon-spin"></div></div>');
          $.ajax({
            url: "/ipq/flag/toggle/" + session_id + "/" + question_id,
          }).done(function (msg) {
            if (msg == 'success') {
              button.toggleClass("flagged");
              button.html(button_text);
            }
          });
          return false;
        });
      });

      // Hrmm.. shouldn't it be enough to use the 'context' in the selector?
      // for some reason I had to use the 'once' function to make this work
      $('.pager-link', context).once('processed', function(){
        $(this).click(function(){
          $('#force_question').val($(this).attr('data-qid'));
          // if the skipped question modal is open, it needs to be closed
          // in case solution modal or other modal is shown before redirecting page
          if (($("#ipq-skipped-modal").data('bs.modal') || {}).isShown) {
            $('#ipq-skipped-modal').modal('hide');
          }
          $("#edit-next").click();
          return false;
        });
      });

      // Toggle active class on options link
      $('.options-wrapper a', context).click(function () {
        $(this).toggleClass('active');
      });

      // Check on page load if the Hide Timer was set already. If so Hide the damn timer.
      if($('#hide_timer').val() == 1){
        Drupal.behaviors.rcpar_global.toggle_timer('hide');
      }
      // Handle user turning timer on/off
      $('#ipq-header', context).on('click', '#hide-timer-link', function(){
        if($(this).hasClass('active')){
          $('#hide_timer').val(true);
        } else {
          $('#hide_timer').val('');
        }
        Drupal.behaviors.rcpar_global.toggle_timer();
      });

      $('#ipq-header', context).on('click', '#view-solution-link', function(){
        if($(this).hasClass('active')){
          $('#view_solutions').val(true);
          //toggle the checkbox on the modal window
          $('#ipq-view-solution-cb').attr('checked', true);
        } else {
          $('#view_solutions').val('');
          //toggle the checkbox on the modal window
          $('#ipq-view-solution-cb').attr('checked', false);
        }
      });

      $('#ipq-header', context).on('click', '#explain-answer-link', function(){
        if($(this).hasClass('active')){
          $('#explain_answers').val(true);
          //toggle the checkbox on the modal window
          $('#ipq-view-solution-cb').attr('checked', true);
        } else {
          $('#explain_answers').val('');
          //toggle the checkbox on the modal window
          $('#ipq-view-solution-cb').attr('checked', false);
        }
      });

      $('#ipq-header', context).on('click', '#score-as-you-go', function(){
        if($(this).hasClass('active')){
          $('#score_as_you_go').val(true);
          $('#score-as-you-go-cb').prop('checked', true);
        } else {
          $('#score_as_you_go').val('');
          $('#score-as-you-go-cb').prop('checked', false);
        }
      });

      // score as you go shut off
      $('#score-as-you-go-cb', context).click(function(){
        if($('#score-as-you-go-cb').prop('checked')){
          $('#score_as_you_go').val(true);
          $('#score-as-you-go').addClass('active');
        } else {
          $('#score_as_you_go').val('');
          $('#score-as-you-go').removeClass('active');
        }
      });

      //explain answer shut off for mcq
      $('#ipq-view-solution-cb', context).click(function(){
        if($('#ipq-view-solution-cb').prop('checked')){
          $('#explain_answers').val(true);
          $('#view_solutions').val(true);
          $('#explain-answer-link').addClass('active');
          $('#view-solution-link').addClass('active');
        } else {
          $('#explain_answers').val('');
          $('#view_solutions').val('');
          $('#explain-answer-link').removeClass('active');
          $('#view-solution-link').removeClass('active');
        }
      });

      $('#options-tooltip', context).click(
        function() {
          setTimeout(function(){
            if($('#explain_answers').val()){
              $('#explain-answer-link').addClass('active');
            } else {
              $('#explain-answer-link').removeClass('active');
            }

            if($('#view_solutions').val()){
              $('#view-solution-link').addClass('active');
            } else {
              $('#view-solution-link').removeClass('active');
            }

            if($('#score_as_you_go').val()){
              $('#score-as-you-go').addClass('active');
            } else {
              $('#score-as-you-go').removeClass('active');
            }
          }, 10);

        }
      );
      $(".page-act-quiz .rcpar-cp-view-solution").on('click', function(){
        behaviours.show_solutions_modal_preview_display('next');
      });
      // pseudo previous and next navigation under question text
      $('.pseudo-question-previous', context).click(
        function() {
          $('.question-number-heading #edit-back').click();
        }
      );
      $('.pseudo-question-next', context).click(
        function() {
          $('.question-number-heading #edit-next').click();
        }
      );

      // now we set the events related to the view solution and score as you go
      // features
      $('.try-again-btn', context).once('rcpar_ipq_session_display').click(function() {
        behaviours.show_solutions_modal_cancel();
      });

      $('#ipq-view-solution .continue-btn', context).click(function() {
        behaviours.show_solutions_modal_continue();
      });


      $('#ipq-score-information .try-again-btn', context).once('confirm-processed-sess-disp', function() {
        $(this).click(function() {
          behaviours.score_as_you_go_modal_cancel();
        });
      });

      $('#ipq-score-information .continue-btn', context).once('confirm-processed-sess-disp', function() {
        $(this).click(function(){
          behaviours.score_as_you_go_modal_continue();
        });
      });

      $('#ipq-score-information').on('hidden.bs.modal', function (e) {
        setTimeout(function(){
          behaviours.score_as_you_go_modal_closed();
        }, 10);
      });

      $(".page-act-question-preview .rcpar-cp-view-solution").on('click', function(){
        behaviours.show_solutions_modal_preview_display('next');
      });

      $('#edit-next', context).once('confirm-processed-sess-disp', function() {
        $(this).click(function() {
          var r = behaviours.show_solutions_modal_if_needed('next');
          if (r){
            r = behaviours.show_score_as_you_go_modal_if_needed('next');
          }
          return r;
        });
      });

      // Show correct answer - event binding
      $('.show-correct-answer', context).once('show-correct-answer', function() {
        $(this).click(function() {
          behaviours.show_correct_answer_if_needed();
        });
      });

      $('#edit-emulate-submit', context).once('rcpar_ipq_session_display').click(function() {
        r = behaviours.show_emulated_score_modal();
        return r;
      });

      $('#edit-back', context).once('confirm-processed-sess-disp', function() {
        $(this).click(function() {
          var r = behaviours.show_solutions_modal_if_needed('back');
          if (r){
            r = behaviours.show_score_as_you_go_modal_if_needed('back');
          }
          return r;
        });
      });

      // this button appears when the question is disabled
      $('.disabled-question-next', context).click(function () {
        if ($('input[name=last_question_in_test]').val() == true) {
          $('#submit-test-link').click();
        }
        else {
          $('#edit-next').click();
        }
        return false;
      });

      // ipq-skipped-ok-btn = continue button on skipped questions modal
      $('.ipq-skipped-ok-btn', context).once('rcpar_ipq_session_display').click(function(){
        setTimeout(function(){
          behaviours.skip_skipped_questions_modal = true;
          // We are going to use a timeout to allow us to continue with the click handler logic
          // and close the skipped question modal before show the modals that might appear on
          // the #submit-test-link click handler

          $('#submit-test-link').click();
        }, 10);

      });
    }
  }

  var MidpointBreakHandler = function (parentObj, modalSelector) {
    // Hide all open modals and exhibits
    $('.modal').modal('hide');
    $('.exhibit-container').hide();

    this.current_step = 1;
    this.parent = parentObj;
    this.timer = null;
    this.modalSelector = modalSelector;

    /// Class method declarations

    /**
     * This method is going to be executed at the end of the constructor code
     */
    this.init = function () {
      var that = this;
      // There are two types of break modals - one for the midpoint of the exam, and one for
      // between all the other testlets. The midpoint break pauses the exam timer and includes
      // a 15 minute timer. The other break modal that shows between testlets doesn't pause
      // the exam timer and doesn't have it's own timer.
      // Only create the timer for the modal if it's the midpoint break modal.
      if (Drupal.behaviors.rcpar_ipq_session_display.shouldShowBreakOption()) {
        // Note that for the timer we are reusing the same timer class that we use for the exam countdown
        this.timer = new DigitalTimer({
          selector: '#ipq-break-timer-popup .remaining-time',
          hours: 0,
          minutes: 15,
          seconds: 0,
          showHours: true,
          showSeconds: true,
          twoDigitsForSeconds: true,
          twoDigitsForMinutes: true,
          callback: function () {
            that.nextStep();
          },
          tickHandler: function () {
            // We want to change some styles when the time is running out
            // so we add a class when some threshold is reached
            if (that.timer.getTimerMinutesRemaining() < 5) {
              $('.remaining-time', modalSelector).addClass('almost-done');
            }
          }
        });
      }
      this.showStep(1);

      // Let's setup the buttons handlers
      $('.next-step', modalSelector).click(function () {
        that.nextStep();
        return false;
      });
      $('.finish-break', modalSelector).click(function () {
        // After the break, show the ipq header tools and the submit testlet button.
        $('#ipq-header .tool-menu').show();
        $('.submit-testlet.tool-menu-item').show();
        that.endBreak();
        return false;
      });

    };

    /**
     * Show a particular step of the Mid Point Break Dialog
     * @param stepNumber
     * - Step number (integer on the 1-3 range)
     */
    this.showStep = function (stepNumber) {
      this.current_step = stepNumber;

      switch (this.current_step) {
        case 1:
          break;
        case 2:
          // Only pause the exam timer and start the modal timer if it's the midpoint break.
          if (Drupal.behaviors.rcpar_ipq_session_display.shouldShowBreakOption()) {
            window.IPQCountDownTimerObj.pauseTimer();
            this.timer.examSimulatorTimerStart();
          }
          break;
        case 3:
          window.IPQCountDownTimerObj.unpauseTimer();
          break;
      }
      $('.break-step', this.modalSelector).hide();
      $('#break-step-' + stepNumber, this.modalSelector).show();
    };

    /**
     * Advance the Mid Point Dialog to the next step
     */
    this.nextStep = function () {
      if (this.current_step == 3) {
        return;
      }
      else {
        this.showStep(this.current_step + 1);
      }
    };

    /**
     * Move the Mid Point Dialog to the previous step
     */
    this.prevStep = function () {
      if (this.current_step == 1) {
        return;
      }
      else {
        this.showStep(this.current_step - 1);
      }
    };

    /**
     * Handles all the logic required to start the exam mid point break (show dialog, pause timer, etc)
     * @param string breakType
     * - Used to detect if the modal is the midpoint break modal or the non-paused break modal.
     */
    this.startBreak = function (breakType) {
      $(modalSelector).modal('show');
      if (breakType == 'paused-break') {
        // We add a hidden input to let the server side know that we have shown the break
        $('<input>').attr({
          type: 'hidden',
          id: 'mid_point_break_shown',
          name: 'mid_point_break_shown',
          value: true
        }).appendTo('form');
      }
    }

    /**
     * Handles all the logic required to end the exam mid point break (hide dialog, resume timer, etc)
     */
    this.endBreak = function () {
      // Hide the modal and trigger an event
      $(modalSelector).modal('hide');
      $('body').trigger('ipq-break-finished');

      // We must ensure that the exam time is running after the break is finished
      window.IPQCountDownTimerObj.unpauseTimer();
    };

    /////////// - End of Class methods declaration

    // Let's initialize the object
    this.init();
  };
}(jQuery));
