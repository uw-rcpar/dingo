var Calculator = {

  results_id:    'calculator-result',
  results_value: '0',
  memory_id:     'calculator-screen',
  memory_value:  '',
  history_id:    'calc-history-list',
  history_value: [],
  memory_store_value: '0',
  new_calculation: false,

  SUM:  '+',
  MIN:  '-',
  DIV:  '/',
  MULT: '*',
  PROC: '%',
  SIN:  'sin(',
  COS:  'cos(',
  MOD:  ' mod ',
  BRO:  '(',
  BRC:  ')',
  SQR: '&radic;',

  calculate: function() {
    this.history_value.push(this.memory_value);
    this.results_value = this.engine.exec(this.memory_value);
    this.add_to_history();
    this.refresh();
    this.new_calculation = true;
    console.log(this.new_calculation);
  },
  
  memeory_store: function() {
  	this.memory_store_value = document.getElementById(this.results_id).innerHTML;
  },
  
  memeory_recall: function() {
   	this.memory_value = this.memory_value + ' ' +this.memory_store_value;
   	this.update_memory();
  },
  
  memeory_clear: function() {
   	this.memory_store_value = '0';
  },
  
  backspace: function() {
   	this.memory_value = this.memory_value.substring(0, this.memory_value.length - 1);
   	this.update_memory();
  },
  
  clipboard: function() {
  	var answer = document.getElementById(this.results_id).innerHTML;
   	window.prompt ("Copy to clipboard: Ctrl+C, Enter", answer);
  },
  
  one_over_x: function() {
   	this.memory_value = '1/'+this.memory_value;
   	this.update_memory();
  },

  put: function(value) {
  	//clear display if starting a new calculation
  	this.check_new_calculation();
  	var check =  this.memory_value + value;
  	//no hitting any function key twice
    var count = this.checkDoubleFunction(check);
    
    if (count == 0) {
    	this.memory_value += value;
  		this.update_memory();
  	}
   
  },

  reset: function() {
    this.memory_value = '';
    this.results_value = '0';
    this.clear_history();
    this.refresh();
  },
  
  reset_display: function() {
    this.memory_value = '';
    this.results_value = '0';
    this.refresh();
  },

  refresh: function() {
    this.update_result();
    //this.update_memory();
  },

  update_result: function() {
  	
    document.getElementById(this.results_id).innerHTML = this.results_value;
    //document.getElementById(this.memory_id).css = 'display:none;';
    //$('#'+this.memory_id).css("display", "none");
    $('#'+this.memory_id).hide();
    $('#'+this.results_id).show();
    
    
  },

  update_memory: function() {
  	$('#'+this.memory_id).show();
    document.getElementById(this.memory_id).innerHTML = this.memory_value;
     $('#'+this.results_id).hide();
  },
  
 add_to_history: function() {
    if (isNaN(this.results_value) == false) {
      var div = document.createElement('li');
      div.innerHTML = this.memory_value + ' = ' + this.results_value;

      var tag = document.getElementById(this.history_id);
      //tag.insertBefore(div, tag.firstChild);
      tag.insertBefore(div, div.nextSibling);
    }
  },

  clear_history: function(){
    $('#'+this.history_id+ '> li').remove();
  },
  // can only use + - * once
  checkDoubleFunction: function(txt) {
  		var count=0;
  		var functionArray = ["--","++","-+","//","**","%%","&radic;&radic;"];
    	var arrayLength = functionArray.length;
		for (var i = 0; i < arrayLength; i++) {
			if (txt.indexOf(functionArray[i])>=0){
				count++;
				
			};   
		}
		return count;
    },
    
  //clear display if it's a new calculation
  check_new_calculation: function() {
  	if (this.new_calculation == true) {
  		this.reset_display();
  		this.new_calculation=false;
  	}
  	
  },

  engine: {
    exec: function(value) {
      try {return eval(this.parse(value))}
      catch (e) {return e}
    },

    parse: function(value) {
      if (value != null && value != '') {
        value = this.replaceFun(value, Calculator.PROC, '/100');
        value = this.replaceFun(value, Calculator.MOD, '%');
        value = this.addSequence(value, Calculator.PROC);

        value = this.replaceFun(value, 'sin', 'Math.sin');
        value = this.replaceFun(value, 'cos', 'Math.cos');
        value = this.replaceFun(value, '&radic;', 'Math.sqrt(');
        value = this.closeBracket(value);
         
        
        return value;
      }
      else return '0';
    },

    replaceFun: function(txt, reg, fun) {
      
      return txt.replace(new RegExp(reg, 'g'), fun);
     
    },
    
    closeBracket: function(txt) {
    	if ((txt.indexOf("Math.sqrt") !== -1))
    	{return txt + ')';} 
    	else 
    	{return txt;}
    },   

    addSequence: function(txt, fun) {
      var list = txt.split(fun);
      var line = '';
			
      for(var nr in list) {
        if (line != '') {
          line = '(' + line + ')' + fun + '(' + list[nr] + ')';
        } else {
          line = list[nr];
        }
      }
      return line;
    }
  }
};


$(document).ready(function() {
  $(".btn").click(function(e) {
    e.preventDefault();
	
    if ($(this).data('constant') != undefined){
      return Calculator.put(Calculator[$(this).data('constant')]);
    }

    if ($(this).data('method') != undefined){
      return Calculator[$(this).data('method')]();
    }

    return Calculator.put($(this).html());
  });
  
    
  //keyboard input
  $(document).keydown(function(e) {
  	e.preventDefault();
  	val = e.keyCode;
  	
  	//val = String.val;
  	
  	//capture the enter key
  	if(e.which == 13) {
    // enter pressed
     $("#equals").click();
  	}
    
    //check for keypad numbers
    if (val >= 96 && val <= 105) {
        //setup keyCode value object
        var code_numbers = {"96":"0", "97":"1", "98":"2", "99":"3", "100":"4","101":"5","102":"6","103":"7","104":"8","105":"9"}; 
        var val_string = val.toString();
        var value = code_numbers[val_string];
        $("#"+value).click();
	        //do something with value;
	    

       //$("#"+val).click();
    } else {
    	//plus sign
    	
    	switch(val)
		{
		case 107:
		  $("#plus").click();
		  break;
		//backspace button
		case 8:
		   e.preventDefault();
		  $("#backspace").click();
		  break;
		case 109:
		  $("#minus").click();
		  break;
		case 111:
		  $("#divide").click();
		  break;
		case 106:
		  $("#prod").click();
		  break;
		case 61:
		  $("#equals").click();
		  break;
		case 110:
		  $("#point").click();
		  break;
		
		  
		}
    }
  });
});
