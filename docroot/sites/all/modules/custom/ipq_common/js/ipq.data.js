(function ($) {
  Drupal.behaviors.rcpar_ipq_data = {
    attach: function (context, settings) {
      // Show or hide the overall performance graph or table:
      // If the sp_overall cookie exists, user has chosen to see the graph and not the table.
      var sp_overall_cookie = Cookies.get('sp_overall');
      if (sp_overall_cookie) {
        // The dropdown should have the graph selected.
        $('#toggle-select', context).prop('selectedIndex', 1);
        // Show the graph
        $('#overall-graph', context).show();
      }
      else {
        // The dropdown should have the table selected.
        $('#toggle-select', context).prop('selectedIndex', 0);
        // Show the table.
        $('#overall-table', context).show();
      }

      // Change whichever display is chosen when dropdown is used.
      $('#toggle-select', context).change(function () {
        $('.overall-display', context).hide();
        $('#' + $('#toggle-select option:selected', context).val()).show();
        // Set a cookie to remember which chart is displayed (table or graph).
        // If the cookie already exists, user has chosen to see the table.
        // Remove the cookie.
        if (sp_overall_cookie) {
          Cookies.remove('sp_overall');
        }
        // Otherwise the user wants to see the graph.
        // Set the cookie.
        else {
          Cookies.set('sp_overall', '1', { expires: 7 });
        }
      });

      // Show and hide "what is smart path" explanation:
      // If user has already hidden the smart path explantion,
      // Don't display it again.
      // First check to see if the sp_intro cookie has been set.
      var sp_intro_cookie = Cookies.get('sp_intro');
      var sp_slide_cookie = Cookies.get('sp_slide');
      // If the cookie exists, user has hidden the smart path intro explanation.
      // Show the link and hide the button and copy.
      if (sp_intro_cookie) {
        $('a.what-is-smartpath', context).show();
        $('button.what-is-smartpath', context).hide();
        $('#what-is-smartpath', context).hide();
      }
      // Otherwise, show the explanation and hide the link.
      else {
        $('a.what-is-smartpath', context).hide();
        // Show the smartpath intro without an animation if they have seen the animation already.
        if (sp_slide_cookie) {
          $('#what-is-smartpath', context).show();
        }
        // Otherwise show it with an animation.
        else {
          setTimeout(function (){
            $('#what-is-smartpath', context).slideDown( "slow", function() {
              // Animation complete.
            });
          }, 800);
        }
      }
      $('.what-is-smartpath', context).click(function () {
        // Set a cookie to remember hidden or shown state.
        // If the cookie already exists, user has hidden the explanation
        // And wants to see it again. Remove the cookie.
        if (sp_intro_cookie) {
          Cookies.remove('sp_intro');
        }
        // Otherwise the user is hiding the explanation.
        // Set the cookie.
        // Also set a cookie to remember that they have seen the slide animation.
        else {
          Cookies.set('sp_intro', '1', { expires: 7 });
          Cookies.set('sp_slide', '1', { expires: 365 });
        }
        // Hide the button or the link.
        $('.' + $(this).attr('class'), context).toggle();
        // Slide the content area.
        $('#' + $(this).attr('class'), context).slideToggle();
        return false;
      });


      // Scroll to section when graph title links are clicked.
      $('.graph-scroll-to', context).click(function () {
        $('html, body', context).animate({scrollTop: $('#' + this.id + '-content', context).offset().top}, 'fast');
      });
      // Scroll to section when table title links are clicked.
      $('.table-scroll-to', context).click(function () {
        $('html, body', context).animate({scrollTop: $('#' + this.id + '-content', context).offset().top}, 'fast');
      });

      // function to remove classes added to progress filter selectpicker
      // doing this because for some reason the method to replace a class is not working
      // instead it just adds more and more classes each time something is selected
      function removeSelectpickerClasses() {
        $('.progress-filter .selectpicker', context).selectpicker('setStyle', 'awaiting-activity', 'remove');
        $('.progress-filter .selectpicker', context).selectpicker('setStyle', 'making-progress', 'remove');
        $('.progress-filter .selectpicker', context).selectpicker('setStyle', 'getting-started', 'remove');
        $('.progress-filter .selectpicker', context).selectpicker('setStyle', 'getting-close', 'remove');
        $('.progress-filter .selectpicker', context).selectpicker('setStyle', 'targets-met', 'remove');
        $('.progress-filter .selectpicker', context).selectpicker('setStyle', 'needs-improvement', 'remove');
        $('.progress-filter .selectpicker', context).selectpicker('setStyle', 'needs-improvement-low', 'remove');
      }
      // progress filter for chapters
      $('.progress-filter #filter-select', context).change(function () {
        // if 'none' is selected
        if ($(this).val() == 'none') {
          // show all chapters
          $('.smart-chapter', context).show();
          // remove all classes from selectpicker button we may have added
          removeSelectpickerClasses();
        }
        // if anything other than 'none' is selected
        else {
          // hide all chapters
          $('.smart-chapter', context).hide();
          // show all chapters of the selected status
          // if any exist on the page
          $('.chapter-' + $(this).val(), context).show();
          // remove all classes from selectpicker button we may have added
          removeSelectpickerClasses();
          // add class to selectpicker button
          $('.progress-filter .selectpicker', context).selectpicker('setStyle', $(this).val());
          // hide or show the no chapters messaging
          if ($('.chapter-' + $(this).val()).length > 0) {
            $('.no-chapters', context).hide();
          }
          else {
            $('.no-chapters', context).show();
          }
        }
      });
    }
  }

  $(document).ready(function () {
    // On page load, reset filters to none
    $('.progress-filter #filter-select').prop('selectedIndex', 0);
  });
}(jQuery));