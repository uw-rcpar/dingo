(function($, Drupal, window, document, undefined) {
  $(document).ready(function() {
    // Set equal heights on load so that the help menu resize handle goes the whole vertical height of the modal
    $('#ipq-help-modal').on('shown.bs.modal', function () {
      ipqHelpEqualHeights();
    });

    // Make IPQ help menu resizable
    $('#ipq-help-menu').resizable({
      handles: 'e',
      maxWidth: 300,
      minWidth: 110,
      resize: function() {
        // Resize the right side in tandem. Subtract a little more to account for padding
        $('#ipq-help-content').width($('#ipq-help-modal').width() - $('#ipq-help-menu').width() - 25)
      },
      start: function(event, ui) {},
      stop: function(event, ui) {}
    });

    // Because our order is on the right side (content), when the left side (menu) grows beyond the modal
    // height, an area appears without a border. Equal heights fixes this, but we have to clear height each time.
    function ipqHelpEqualHeights() {
      $('.ipq-help-wrapper .left-side, .ipq-help-wrapper .right-side').css('height', 'auto').equalHeights();
    }

    // Help edge case: it's possible to have folders with no articles in them - in that case, we don't want
    // them to open a new page. In practice, this shouldn't happen though.
    $('#ipq-help-menu').find('li[data-type="taxonomy-term"] a').click(function(e) {
      e.preventDefault();
    });

    // Attach to help topic links so that clicking loads an article by ajax in the modal
    $('#ipq-help-menu .node a').click(function(e){
      e.preventDefault();

      // Store the content div for quick access/performance
      var $helpContent = $('#ipq-help-content .content-wrapper');

      // Get the node id of the clicked article
      var nid = $(this).closest('li').attr('data-nid');

      $.ajax({
        url : Drupal.settings.basePath + 'ipq-tools/help/ajax/' + nid,
        type : 'GET',
        dataType : 'html',
        // Replace the help content area with the returned HTML
        success : function(data, status, xmlhttprequest) {
          $helpContent.html(data);
          ipqHelpEqualHeights();
        },
        // Show a loading indicator
        beforeSend : function() {
          $helpContent.html('<div class="ajax-throbber"><div class="glyphicon glyphicon-refresh glyphicon-spin"></div></div>');
          ipqHelpEqualHeights();
        },
        error : function(data) {
          $helpContent.html('<p>An unexpected error occurred, please try again shortly.</p>');
          ipqHelpEqualHeights();;
        }
      });
    });

    // IPQ help folder links - click binding
    $('#ipq-help-menu .has-nodes > a').click(function(e){
      e.preventDefault();

      // Show/hide the articles in this folder. This selector is a bit lazy, but we know there is only
      // one level to the help tree hierarchy.
      var $li = $(this).closest('li');
      $li.toggleClass('open');
      $li.find('.item-list').toggle();

      ipqHelpEqualHeights();
    });

    // Initialize display of IPQ help folder links (hide children of folders so that they appear closed
    $('#ipq-help-menu .has-nodes .item-list').hide();
  }); //END - document.ready
})(jQuery, Drupal, this, this.document); //END - Closure