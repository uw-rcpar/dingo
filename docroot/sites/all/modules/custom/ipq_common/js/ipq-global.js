function modal_styles_fix() {
  var body = $('#ipq-container');
  if (body.hasClass("ipq-drs-question") ||
    body.hasClass("ipq-tbs-form-question") ||
    body.hasClass("ipq-tbs-journal-question") ||
    body.hasClass("ipq-tbs-question")) {
    $("#ipq-view-solution .modal-content").css("height", "calc(100% - 45px)");
  }
  width = $(window).width() * 0.80;
  if ($(window).width() >= 980 && width < 920) {
    width = 920;
  }
  if (width < 320) {
    width = 320;
  }
  // position view solution modals for various question types
  if (body.hasClass("ipq-drs-question") ||
    body.hasClass("ipq-tbs-form-question") ||
    body.hasClass("ipq-tbs-journal-question") ||
    body.hasClass("ipq-tbs-question")) {
    $("#ipq-view-solution .modal-content").css("left", (($(window).width() - width) / 2));
  }
  if (body.hasClass("ipq-mcq-question") || body.hasClass("ipq-research-question")) {
    $("#ipq-view-solution .modal-content").css("left", (($(window).width() - $("#ipq-view-solution .modal-content").outerWidth()) / 2));
  }
}

(function ($) {
  $(document).ready(function () {
    var containing_div = $('#ipq-container');
    if (containing_div.length > 0) {
      if (containing_div.hasClass("ipq-drs-question") ||
        containing_div.hasClass("ipq-tbs-form-question") ||
        containing_div.hasClass("ipq-tbs-journal-question") ||
        containing_div.hasClass("ipq-tbs-question")) {
        $('#ipq-view-solution .modal-content').resizable({handles: 'n, e, s, w'});
        // The resize event on a modal will bubble up to the window,
        // which triggers some functions we don't want triggered when a modal is resized.
        // This will prevent it.
        $('#ipq-view-solution .modal-content').resizable().on('resize', function (e) {
          e.stopPropagation();
        });
      }
    }

    if ($('#ipq-view-solution .modal-content').length > 0) {
      // Limit the draggable for any view solution modal, including MCQ
      $('#ipq-view-solution .modal-content').draggable({
        handle: '.modal-header',
        containment: 'window',
        revert: false,
        clone: 'helper',
        appendTo: 'body',
        scroll: false,
      });
    }
    function disableScrollable() {
      $('.scrollable-area').css('overflowY', 'hidden');
    }
    function enableScrollable() {
      $('.scrollable-area').css('overflowY', 'auto');
    }
    $(".question-number-heading [type='submit']").click(function () {
      if ($("#ipq-view-solution").is(':visible')) {
        modal_styles_fix();
      }
    });
    $(".previous-next-wrapper a").click(function () {
      if ($("#ipq-view-solution").is(':visible')) {
        modal_styles_fix();
      }
    });
    $(".submit-testlet .submit-test-link").click(function () {
      if ($("#ipq-view-solution").is(':visible')) {
        modal_styles_fix();
      }
    });
  });
  // todo - this function needs to be re-assessed
  /**
   * Set the height of the directions modal window based on the viewport
   */
  window.redrawDirectionsAndDisabledQuestionModals = function () {
    // Only do this if .ipq-question-wrapper div is on the page (deleted questions will not have it).
    if ($('.ipq-question-wrapper').length > 0) {
      var viewport_height = $(window).height();
      var container_pos = $('.ipq-question-wrapper').offset();
      var modal_height = viewport_height - container_pos.top;
      var direction_height = modal_height - 264;

      // only present when the question is disabled
      $('#ipq-disabled-question').css({
        height: modal_height, //probably not needed
        'overflow-y': 'auto',
        'overflow-x': 'auto'
      });

      // only present when the question was deleted
      $('#ipq-container.deleted-question').css({
        height: modal_height, //probably not needed
      });

      // we only do this if the question exists and is active (otherwise, this modal will interfere with the others)
      if ($('#is-disabled').val() != true && $('#is-deleted').val() != true) {
        $('#ipq-help-modal').css({
          height: modal_height, //probably not needed
          'overflow-y': 'auto',
          'overflow-x': 'auto'
        });
        $('#instructions-wrapper').css({height: direction_height});
      }
    }
  };
  /**
   * Add "Unanswered" text to any cell in a journal grid that should have it
   */
  window.processJournalToggleCells = function () {
    // Add a tooltip (actually a title attribute)
    $('.page-ipq-review .ipq-journal-toggl-collapse, #view-solution-info-container .ipq-journal-toggl-collapse').prop('title', 'Expand');
    // Each unanswered answer-div needs to say "Unanswered"
    $.each($('.ipq-journal-toggl-collapse.skipped .answer-div'), function () {
      // Check if div is empty and if it's been processed
      if (!$(this).text().trim().length && !$(this).hasClass('skipped-processed')) {
        // If the div contains an answer-div-copy div, append to that
        if ($(this).find('.answer-div-copy').length !== 0) {
          $(this).find('.answer-div-copy').append('<em>Unanswered</em>');
        }
        // Otherwise append to answer-div
        else {
          $(this).append('<em>Unanswered</em>');
        }
      }
      if (!$(this).hasClass('skipped-processed')) {
        $(this).addClass('skipped-processed');
      }
    });
  }



  $(document).ready(function () {

    // show and hide timer
    $('.timer-tab').click(Drupal.behaviors.rcpar_global.toggle_timer);


    // nav IPQ section slide down / show/hide
    $('#ipq-section li.non-active').hide();
    $('#ipq-section li.active').show();

    $('#ipq-section li.active a').mouseenter(function () {
      $('#ipq-section li').stop().slideDown(300, '');
    })

    $('#ipq-section').mouseleave(function () {
      $('#ipq-section li.non-active').stop().slideUp(300);
      $('#ipq-section li.active').show();
    });

    // exam collapsible nav
    $('.exam-pulldown').click(function () {
      $('#exam-navbar').hide();
      $('#ipq-navbar').stop().slideDown("slow", function () {
        redrawDirectionsAndDisabledQuestionModals();
      });
      $('.session-type-exam').addClass('exam-nav-expanded');
    });

    // Quiz footer options active toggle
    // Will only work after tooltip is shown
    $('#options-tooltip').on('shown.bs.tooltip', function () {
      $('#ipq-header .options-link').click(function () {
        $(this).toggleClass('active');
      })

      $('#ipq-view-solution .close').click(function () {
        $('#view-solution-link').toggleClass('active');
      })
    });


    // show and hide the answer explanation on TBS review page
    $(document).on('click', '.user-answer .answer-row .answer-col2 .answer-div', function () {
      // find the parent .answer-col2 div
      var parent_div = $(this).closest('.answer-col2').attr('id');
      if (!$(this).hasClass('opened')) {
        // Change the tooltip text (actually the element's title)
        $(this).prop('title', 'Collapse');
        // Show the answers explanation
        $('#' + parent_div + ' .answer-explanation').stop().slideDown("fast", function () {
          // Animation complete.
        });
        $(this).addClass('opened');
      }
      else {
        // Change the tooltip text (actually the element's title)
        $(this).prop('title', 'Expand');
        // Show the answers explanation
        $('#' + parent_div + ' .answer-explanation').hide();
        $(this).removeClass('opened');
      }
    });

    // expand and collapse all answer explanations on TBS review page
    $(document).on('click', '.expand-collapse-all', function () {
      $(this).toggleClass('expanded');
      if ($(this).hasClass('expanded')) {
        // Change link text.
        $(this).html('<i class="fa fa-chevron-up"></i> Collapse all rows');
        // Expand all rows.
        $('.user-answer .answer-row .answer-explanation').stop().slideDown("fast", function () {
          // Animation complete.
        });
        $('.user-answer .answer-row .answer-div').addClass('opened').prop('title', 'Collapse');
      }
      else {
        // Change link text.
        $(this).html('<i class="fa fa-chevron-down"></i> Expand all rows');
        // Collapse or hide all rows.
        $('.user-answer .answer-row .answer-explanation').hide();
        $('.user-answer .answer-row .answer-div').removeClass('opened').prop('title', 'Expand');
      }
    });
  }); // end document.ready


  Drupal.behaviors.rcpar_global = {


    toggle_timer: function (force_display) {
      if (force_display == undefined) {
        force_display = 'show';
      }
      if (!$('.digital-timer').hasClass('closed') || force_display == 'hide') {
        $('.collapsible-area').hide();
        $('.digital-timer').addClass('closed');
        $('.timer-tab').toggle();
        //check the hide-timer checkbox in the footer tools for persistance between pages
        $('#hide-timer-link').addClass('active');
        $('#hide_timer').val(true);
        // Check if the show correct answer button hasnt been clicked.
        if($('.show-timer-show').length > 0 && typeof $('a.show-correct-answer') !== "undefined"){
          $('.show-timer-show').addClass("hidden show-timer-");
          $('.show-timer-show').removeClass('show-timer-show');
        }
      }
      else {
        $('.collapsible-area').show();
        $('.digital-timer').removeClass('closed');
        $('.timer-tab').toggle();
        //uncheck the hide-timer checkbox in the footer tools for persistance between pages
        $('#hide-timer-link').removeClass('active');
        $('#hide_timer').val('');
        // Check if the show correct answer button has been clicked.
        if($('.show-timer-').length > 0 && $('a.show-correct-answer').length == 0) {
          $('.show-timer-').removeClass("hidden");
          $('.show-timer-').addClass('show-timer-show');
        }
      }
      return false;
    },

    attach: function (context, settings) {

      // Display a popover message when a user attempts
      // to add some disallowed chapters into an IPQ quiz or
      // tries to click a "smart quiz" link (ie, free trial users).
      $(".ipq_chapter_blocked").bind(
        "mouseenter mouseleave",
        function (event) {
          var a_element = $(this);
          // Messaging for smart quiz links.
          if (a_element.find('a').hasClass('smart-quiz')) {
            var message = 'Upgrade your course for full access.';
          }
          // Messaging for IPQ quiz setup.
          else {
            var message = 'Upgrade your course for full lecture access.';
          }
          a_element.addClass("restricted-item")
            .attr('data-toggle', 'popover')
            .attr('title', 'Currently Unavailable')
            .attr('data-content', message)
            .attr('data-placement', 'bottom')
            .attr('data-trigger', 'hover');
          $('[data-toggle="popover"]').popover().bind("mouseenter mouseleave", function () {
            setTimeout(function () {
              $('.restricted-item').popover('hide');
            }, 6000);
          });
        }
      );

      // auth lit modal needs visible backdrop but calculator modal needs invisible backdrop
      // we'll add a class to the body when auth lit modal is viewed
      $('#ipq-auth-lit').on('show.bs.modal', function () {
        $('body').addClass('has-auth-lit-popup');
      });
      $('#ipq-auth-lit').on('hidden.bs.modal', function () {
        $('body').removeClass('has-auth-lit-popup');
      });

      // Make Excel modal draggable
      $('#ipq-open-excel').on('show.bs.modal', function () {
        // Make it draggable
        $('#ipq-open-excel .modal-content').draggable({
          handle: '.modal-header',
          containment: 'window',
          revert: false,
          clone: 'helper',
          appendTo: 'body',
          scroll: false,
        });
        // position the modal
        $('#ipq-open-excel .modal-content').css('left', (($(window).width() - $('#ipq-open-excel .modal-content').width()) / 2));
      });


      // Show the motal to remove the free trial mode
      $('#activation-container-of-values').bind('DOMNodeInserted', function (event) {
        $('#course-activation').modal('show');
      });

      // IPQ Tools Modal Controller
      // Match to Bootstraps data-toggle for the modal
      // and attach an onclick event handler

      $('.ipq-tools-modal').once('tool-menu-item').on('click', context, function (e) {

        // From the clicked element, get the data-target arrtibute
        // which BS3 uses to determine the target modal
        var target_modal = $(e.currentTarget).data('target');
        //set the title
        var ipqtitle = $(this).attr('ipqtitle');
        //Adding this puppy to insure spreadsheet only loads once per click
        var executed = false;

        // Find the target modal in the DOM
        var modal = $(target_modal);
        // Find the modal's <div class="modal-body"> so we can populate it
        var modalBody = $(target_modal + ' .modal-body');
        //clear any html already rendered
        modalBody.empty();

        if (e.currentTarget.href.length > 0) {
          if (modalBody.find('.handsontable').length == 0) {
            //hit the loader first
            modalBody.addClass("calc-iframe");
            //load it up
            modalBody.load(e.currentTarget.href, function () {

              if (e.currentTarget.href.indexOf('spreadsheet') > -1) {
                //there is some weirdness in the spreadsheet that causes this code block to loop. Using brute force hack to only allow one itineration
                if (!executed) {
                  var ss = new Spreadsheet();
                  ss.init();
                  ss.spreadsheet.selectCell(0, 0);
                  modal.find('.wtHolder').css('overflow', 'scroll');
                  executed = true;
                }
              }
            });
          }
          // Else if this is not the first time opening the spreadsheet popup...
          else {

            // This is a hack to make the grid re-render itself after closing and re-opening the spreadsheet popup. Not sure how else to fix this right now.
            modal.find('.wtHolder').animate({scrollTop: 1}, 500);
          }
        }

        //render away
        modal.modal();

        // Make it draggable
        modal.draggable({
          handle: '.modal-header',
          containment: 'window',
          revert: false,
          clone: 'helper',
          appendTo: 'body',
          scroll: false,
        });
        modal.css({
          width: '35%', //probably not needed
          height: '35%', //probably not needed
          display: 'table', //important
          'overflow-y': 'auto',
          'overflow-x': 'auto'
        });

        $('.modal-title', target_modal).html(ipqtitle);

        // position the ipq-tools modal (calculator and spreadsheet)
        $("#ipq-tools").css("left", (($(window).width() - $("#ipq-tools").outerWidth()) / 2));

        // Now return a false (negating the link action) to prevent Bootstrap's JS 3.1.1
        // from throwing a 'preventDefault' error due to us overriding the anchor usage.
        return false;
      });

      // On window resize, perform a modal resize as long as the directions content is visible
      // todo - reassess
      $(window).resize(function () {
        if ($('#ipq-help').is(':visible')) {
          redrawDirectionsAndDisabledQuestionModals();
        }
        // On review pages and view solution popups, add some stuff to td collapseable cells:
        if ($('.page-ipq-review .ipq-journal-toggl-collapse, #view-solution-info-container .ipq-journal-toggl-collapse').length) {
          setTimeout(function () {
            processJournalToggleCells();
          }, 300);
        }
      });

      if ($('#ipq-disabled-question').length > 0) {
        $('#ipq-disabled-question').modal('toggle');
        redrawDirectionsAndDisabledQuestionModals();
      }
      if ($('#ipq-container.deleted-question').length > 0) {
        redrawDirectionsAndDisabledQuestionModals();
      }

      // Add link to expand and collapse all rows on TBS journal review pages
      $('.journal-question-text.review  .question-table', context).once('expand-collapse').each(function () {
        // Create expand toggle link on each table
        var $expandToggleLink = $('<a class="expand-collapse-all-journal"><i class="fa fa-chevron-down"></i> Expand all rows</a>');
        $expandToggleLink.insertBefore($(this));

        // Expand and collapse all rows on TBS journal review pages
        $expandToggleLink.click(function () {
          $(this).toggleClass('expanded');
          if ($(this).hasClass('expanded')) {
            $(this).html('<i class="fa fa-chevron-up"></i> Collapse all rows');
            $('#' + $(this).next('.question-table').find('.handsontable-tbs-journal-container').attr('id') + ' .ipq-journal-toggl-collapse').prop('title', 'Collapse');
            $('#' + $(this).next('.question-table').find('.handsontable-tbs-journal-container').attr('id') + ' td.ipq-journal-toggl-collapse').removeClass('row-collapsed');
            $('#' + $(this).next('.question-table').find('.handsontable-tbs-journal-container').attr('id') + ' td.tbs-journal-collapsible').removeClass('hidden');
          }
          else {
            $(this).html('<i class="fa fa-chevron-down"></i> Expand all rows');
            $('#' + $(this).next('.question-table').find('.handsontable-tbs-journal-container').attr('id') + ' .ipq-journal-toggl-collapse').prop('title', 'Expand');
            $('#' + $(this).next('.question-table').find('.handsontable-tbs-journal-container').attr('id') + ' td.ipq-journal-toggl-collapse').addClass('row-collapsed');
            $('#' + $(this).next('.question-table').find('.handsontable-tbs-journal-container').attr('id') + ' td.tbs-journal-collapsible').addClass('hidden');
          }
        });
      });
    }
  }
}(jQuery));
