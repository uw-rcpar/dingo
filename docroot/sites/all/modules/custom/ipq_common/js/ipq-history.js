( function($) {
  Drupal.behaviors.rcpar_ipq_history = {
    attach : function(context, settings) {

      $('.rename-session').click(function(){
        var session_id = $(this).attr('id').replace('rename_current_session_','');
        $('#current-name-container-'+session_id).hide();
        $('#new-name-container-'+session_id).show();
        return false;
      });

      $('.modal').on('show.bs.modal', function() {
        $('.modal-dialog').draggable({
          handle: '.modal-header'
        });
      });

      // History Resume
      $('.review-sess-btn').click(function(e) {
        var sid = $(this).attr('sid');
        var sess = $(this).attr('sess');
        var action = $(this).attr('action');
        var self = this;
        if (sid) {
          $.ajax({
            'url' : '/ipq/rebuild/' + sid + '/' + action,
            'type' : 'GET',
            'dataType' : 'json',
            'success' : function(data) {
              if (data.status == "success") {                
                $('.sess-action-message-' + sid).html(data.message);
                setTimeout(function() {
                  $('.sess-action-message-' + sid).hide();
                }, 3000);
                $(location).attr('href', $(self).attr('href'));
              } else {
                $('.sess-action-message-' + sid).html("We encountered an unexpected error - unknown reason.");
              }
            },
            beforeSend : function() {
              $(document).ready(function() {
//                $('.sess-action-message-' + sid).html('<img src="./css/images/circular_throbber.gif"> Building your session...');
                $('.sess-action-message-' + sid).html('Building your session...');
                $('.sess-action-message-' + sid).addClass('alert alert-success');
              });
            },
            'error' : function(data) {
              $(document).ready(function() {
                $('.sess-action-message-' + sid).html("Error Occured");
              });
            }
          });
        }
        return false;
      });

      // History Resume
      $('.rebuild-sess-btn').click(function(e) {
       var sid = $(this).attr('sid');
       var sess = $(this).attr('sess');
       var action = $(this).attr('action');
       if (sid) {
        $.ajax({
         'url' : '/ipq/rebuild/' + sid + '/' + action,
         'type' : 'GET',
         'dataType' : 'json',
         'success' : function(data) {
          if (data.status == "success") {
           $('.sess-action-message-' + sid).html(data.message);
           setTimeout(function() {
            $('.sess-action-message-' + sid).hide();
           }, 3000);
             $(location).attr('href', '/ipq/'+sess+'/take');
          } else {
           $('.sess-action-message-' + sid).html("We encountered an unexpected error - unknown reason.");
          }
         },
         beforeSend : function() {
          $(document).ready(function() {
//           $('.sess-action-message-' + sid).html('<img src="./css/images/circular_throbber.gif"> Building your session...');
           $('.sess-action-message-' + sid).html('Building your session...');
           $('.sess-action-message-' + sid).addClass('alert alert-success');
          });
         },
         'error' : function(data) {
          $(document).ready(function() {
           $('.sess-action-message-' + sid).html("Error Occured");
          });
         }
        });
       }
       return false;
      });
  
      // History Delete
      $('.del-sess-btn').click(function(e) {
       var sid = $(this).attr('sid');
       if (sid) {
        deleteSession(sid);
       }
       return false;
      });
      function deleteSession(sid) {
       $.ajax({
        'url' : '/ipq/history-del/' + sid + '/json',
        'type' : 'GET',
        'dataType' : 'json',
        'success' : function(data) {
         if (data.status == "success") {
          // We need to close the delete modal here or the backdrop doesn't disappear
          // because we replace the modal's containing div with new content below
          $('.modal').modal('hide');
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();

          // Empty the content div
          $(".session-entry-" + sid).html('').hide();
          $('.sess-action-message-' + sid).html(data.message);
          setTimeout(function() {
           $('.sess-action-message-' + sid).hide();
          }, 3000);
         } else {
          $('.sess-action-message-' + sid).html("Deletion Failed - unknown reason.");
         }
        },
        beforeSend : function() {
         $(document).ready(function() {
          $('.sess-action-message-' + sid).html('<img src="./css/images/circular_throbber.gif"> Sending Request...');
          $('.sess-action-message-' + sid).addClass('alert alert-success');
         });
        },
        'error' : function(data) {
         $(document).ready(function() {
          $('.sess-action-message-' + sid).html("Error Occured");
         });
        }
       });
       return false;
      }

    }
  }
}(jQuery));
