( function($) {

  // Jquery used in the IPQ Question Inventory views exposed filter form

  $(document).ready(function() {

    // add classes based on Section and exam version to Chapter drop down select
    // to manipulate the select options later in the code
    $('#edit-field-ipq-chapter-target-id > option').each(function(){
      // All option gets it's own class
      if ($(this).val() == 'All') {
        var class_to_add = 'all';
        $(this).addClass(class_to_add);
      }
      else {
        // we want the first four characters of the option text to get the exam version

        // IE, AUD, BEC, FAR or REG
        var class_to_add = 'ev_' + $(this).text().substring(0,4);
        $(this).addClass(class_to_add);

        // we want the three characters corresponding to the option text
        // IE, AUD, BEC, FAR or REG
        var class_to_add = $(this).text().substring(7,10);
        $(this).addClass(class_to_add);
      }
    });
    
    // add class from optgroup label to Topic drop down select
    $('#edit-field-ipq-topic-target-id > optgroup').each(function(){
      var str = $(this).prop('label');
      str = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '').toLowerCase();
      // then we add the class to the option
      $(this).addClass(str);
    });

    // get the Section that is selected
    var section_value = $('#edit-field-ipq-course-section-tid option:selected').val();
    var exam_version_value = $('#edit-field-exam-version-single-val-tid option:selected').val();

    // disable the Chapter select if section or exam version chosen is All
    if (section_value == 'All' || exam_version_value == 'All') {
     $('#edit-field-ipq-chapter-target-id').prop('disabled', true);
    }
    // but if page has been reloaded and items are already selected, we have to do some stuff
    else {
      // get the selected option's text, IE, AUD, BEC, FAR, REG or All
      var section = $('#edit-field-ipq-course-section-tid option:selected').text();
      var exam_version = $('#edit-field-exam-version-single-val-tid option:selected').text();
      // hide all the options
      $('#edit-field-ipq-chapter-target-id > option').hide();
      // but show the Any/All option
      $('#edit-field-ipq-chapter-target-id > option.all').show();
      // then show just the ones with the chosen section and exam version
      $('#edit-field-ipq-chapter-target-id > option.ev_'+exam_version+'.' + section).show();
      // enable to Chapter select
      $('#edit-field-ipq-chapter-target-id').prop('disabled', false);
    }

    // get the Chapter that is selected
    var chapter_value = $('#edit-field-ipq-chapter-target-id option:selected').val();

    // if the Chapter selected is All,
    // disable the Topic select
    if (chapter_value == 'All') {
      $('#edit-field-ipq-topic-target-id').prop('disabled', true);
    }
    // but if page has been reloaded and items are already selected, we have to do some stuff
    else {
      // get the Chapter that is selected
      var chapter_name = $('#edit-field-ipq-chapter-target-id option:selected').text();
      chapter_name = chapter_name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '').toLowerCase();
      // hide all the options
      $('#edit-field-ipq-topic-target-id > optgroup').hide();
      // then show just the ones with the chosen section
      $('#edit-field-ipq-topic-target-id > optgroup.' + chapter_name).show();
      // enable the Topic select
      $('#edit-field-ipq-topic-target-id').prop('disabled', false);
    }

    // hiding and showing Chapter options when Section and exam version drop down are changed
    $('#edit-field-ipq-course-section-tid, #edit-field-exam-version-single-val-tid').on('change', function() {
      // get the section that is selected
      var section_value = $('#edit-field-ipq-course-section-tid option:selected').val();
      var exam_version_value = $('#edit-field-exam-version-single-val-tid option:selected').val();

      if (section_value == 'All' || exam_version_value == 'All') {
        // show all the chapter options but disable chapter dropdown
        // and select the first visible option
        $('#edit-field-ipq-chapter-target-id > option').show();
        $('#edit-field-ipq-chapter-target-id').prop('disabled', true);
        $('#edit-field-ipq-chapter-target-id > option[selected="selected"]').removeAttr('selected');
        $('#edit-field-ipq-chapter-target-id').find('option:visible:first').attr('selected','selected');
        // we should also disable the topic drop down if necessary
        $('#edit-field-ipq-topic-target-id').prop('disabled', true);
        $('#edit-field-ipq-topic-target-id').find('option:visible:first').attr('selected','selected');

      }
      else {
        // get the selected option's text, IE, AUD, BEC, FAR, REG or All
        var section = $('#edit-field-ipq-course-section-tid option:selected').text();
        var exam_version = $('#edit-field-exam-version-single-val-tid option:selected').text();
        // hide all the options
        $('#edit-field-ipq-chapter-target-id > option').hide();
        // but show the Any/All option
        $('#edit-field-ipq-chapter-target-id > option.all').show();
        // then show just the ones with the chosen section
        $('#edit-field-ipq-chapter-target-id > option.ev_' + exam_version + '.' + section).show();
        $('#edit-field-ipq-chapter-target-id').prop('disabled', false);
        // and select the first option
        $('#edit-field-ipq-chapter-target-id > option[selected="selected"]').removeAttr('selected');
        $('#edit-field-ipq-chapter-target-id').find('option:visible:first').attr('selected','selected');
      }
    });

    // hiding and showing Topic dropdown when Chapter drop down is changed
    $('#edit-field-ipq-chapter-target-id').on('change', function() {
      // get the Chapter that is selected
      var chapter_name = $('#edit-field-ipq-chapter-target-id option:selected').text();
      chapter_name = chapter_name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '').toLowerCase();
      // if any is selected we need to disable topic dropdown
      // and make sure "All" is selected
      if (chapter_name == 'any') {
        $('#edit-field-ipq-topic-target-id').prop('disabled', true);
        $('#edit-field-ipq-topic-target-id').find('option:visible:first').attr('selected','selected');
      }
      else {
        // hide all the options
        $('#edit-field-ipq-topic-target-id > optgroup').hide();
        // then show just the ones with the chosen section
        $('#edit-field-ipq-topic-target-id > optgroup.' + chapter_name).show();
        $('#edit-field-ipq-topic-target-id').prop('disabled', false);
      }

    });

    // As we are maintaining two set of options (one per exam version term)
    // and as those options might have repeated values for a particular filter
    // we keep the options corresponding to the current exam version term clean
    // and append the exam version to the value of the other options
    $('#edit-field-exam-version-single-val-tid option').each(function( index ) {
      console.log( index + ": " + $( this ).text() );
      var ev_name = $( this ).html();
      var ev_val = $( this ).val();
      $('#edit-field-ipq-chapter-target-id option.ev_' + ev_name + ' , #edit-field-ipq-topic-target-id option.ev_' + ev_name).each(function( i ) {
        var current_val = $(this).val();

        // Note that the exam version year might have been already appended to the option
        // that's why we check before adding it
        if (val.indexOf('-'+ev_name) == -1){
          // We need to append the ev name to the option value
          $(this).val(current_val + '-' + ev_name);
        }
      });
    });

    var sel_exam_version = $('#edit-field-exam-version-single-val-tid option:selected').text();
    $('#edit-field-ipq-chapter-target-id option.ev_' + sel_exam_version + ' , #edit-field-ipq-topic-target-id option.ev_' + sel_exam_version).each(function( i ) {
      var current_val = $(this).val();

      // Note that the exam version year might have been already striped of the option
      // that's why we check before removing it
      if (val.indexOf('-'+sel_exam_version) != -1){
        $(this).val(current_val + '-' + ev_name);
      }
    });


  });


 }(jQuery));
