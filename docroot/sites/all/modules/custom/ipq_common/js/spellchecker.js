(function($) {

    // This code is used to get the current cursor position from contenteditable="true" elements
    window.getCaretCharacterOffsetWithin = function(element) {
        var caretOffset = 0;
        var doc = element.ownerDocument || element.document;
        var win = doc.defaultView || doc.parentWindow;
        var sel;
        if (typeof win.getSelection != "undefined") {
            sel = win.getSelection();
            if (sel.rangeCount > 0) {
                var range = win.getSelection().getRangeAt(0);
                var preCaretRange = range.cloneRange();
                preCaretRange.selectNodeContents(element);
                preCaretRange.setEnd(range.endContainer, range.endOffset);
                caretOffset = preCaretRange.toString().length;
            }
        } else if ( (sel = doc.selection) && sel.type != "Control") {
            var textRange = sel.createRange();
            var preCaretTextRange = doc.body.createTextRange();
            preCaretTextRange.moveToElementText(element);
            preCaretTextRange.setEndPoint("EndToEnd", textRange);
            caretOffset = preCaretTextRange.text.length;
        }
        return caretOffset;
    };

    // This code is to get the current cursor position from textarea elements
    $.fn.getCursorPosition = function() {
        var el = $(this).get(0);
        var pos = 0;
        if('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    };


  Drupal.behaviors.ipqSpellChecker = {
    attach: function (context, settings) {
      var textareas = [];

      $('.ipq-spellcheck', context).each(function (i, elem) {
        var id = $(elem).attr('id');
        textareas[i] = new SpellChecker({
          textarea: true,
          editable: '#' + id
        });
        textareas[i].init();
      });

    }
  }

})(jQuery, Drupal);

    

// This code is used to spellcheck contenteditable elements and textareas
  SpellChecker = function(settings) {
      if (settings.textarea) {
        this.contenteditable = false;
      }
      else {
        this.contenteditable =  true;
      }
      if (settings.iframe) {
        this.iframe = jQuery(settings.iframe);
        this.container = this.iframe.contents();
        this.editable = this.container.find(settings.editable);
      }
      else {
        this.editable = jQuery(settings.editable);
      }
      this.mouseX;
      this.mouseY;
      this.disabled = false;
      this.wordList = {};
      this.timeout = false;
      this.incorrectList = {};
      this.key = false;
      if (this.contenteditable) {
        this.html = this.editable.text();
      }
      else {
        this.html = this.editable.val();
      }

      var that = this;

      this.init = function() {
        jQuery('body').click(function() {
          jQuery('#contextmenu').remove();
        });

        if (this.iframe) {
          jQuery(this.container).mousemove(function(event) {
              that.mouseX = event.pageX;
              that.mouseY = event.pageY;
          });
        }
        else {
          jQuery(document).mousemove(function(event) {
              that.mouseX = event.pageX;
              that.mouseY = event.pageY;
          });
        }

      if (this.container && this.iframe) {
        this.container.mousedown(function(e) {
            if (e.button == 2) {
              that.editable.attr('spellcheck','false');
              e.preventDefault ? e.preventDefault : e.returnValue = false;
              e.stopPropagation();
              if (!that.disabled) {
                that.scanTypedText();
              }
              return false;
            }
            jQuery('#contextmenu').remove();
            return true;
          });

          if (this.editable) {
            this.editable.oncontextmenu = function(e) { 
                e.preventDefault ? e.preventDefault : e.returnValue = false;
                return false; 
            };
          }

          this.container.on("contextmenu", function(e) {
            e.preventDefault ? e.preventDefault : e.returnValue = false;
            e.stopPropagation();
            if (!that.disabled) {
              that.checkWords();
            }
            return false;
          });
        }
        else if (!this.iframe) {
          this.editable.mousedown(function(e) {
            if (e.button == 2) {
              that.editable.attr('spellcheck','false');
              e.preventDefault ? e.preventDefault : e.returnValue = false;
              e.stopPropagation();
              if (!that.disabled) {
                that.scanTypedText();
              }
              return false;
            }
            jQuery('#contextmenu').remove();
            return true;
          });

          if (this.editable.get(0)) {
            this.editable.get(0).oncontextmenu = function(e) { 
                e.preventDefault ? e.preventDefault : e.returnValue = false;
                return false; 
            };
          }

          this.editable.on("contextmenu", function(e) {
            e.preventDefault ? e.preventDefault : e.returnValue = false;
            e.stopPropagation();
            if (!that.disabled) {
              that.checkWords();
            }
            return false;
          });
        }

        this.fetchCorrections();

        this.editable.keyup(function(evt) {
          clearTimeout(that.timeout);  
          that.timeout = setTimeout(function() {
            that.editable.attr('spellcheck','true');
            that.fetchCorrections();
          }, 1000);
        });

      };

      this.fetchCorrections = function() {
        if (this.contenteditable) {
          this.html = this.editable.text();
        }
        else {
          this.html = this.editable.val();
        }
        this.wordList = {};

        jQuery.post("/sites/all/modules/custom/ipq_common/includes/spellcheck.php", {'text': that.html})
          .done(function(data) {
            try {
              data = JSON.parse(data);
              for(var word in data.words) {
                if (data.words[word].length < 10) {
                  that.wordList[word] = data.words[word];
                }
                else {
                  that.wordList[word] = data.words[word].slice(0, 10);
                }
              }
            }
            catch(e) {
              // Servers without the spellcheck plugin on the server side can fail to parse and otherwise
              // cause a JS error here
            }
            that.scanTypedText();
          });
      };

      this.scanTypedText = function() {
        if (this.contenteditable) {
          this.html = this.editable.text();
        }
        else {
          this.html = this.editable.val();
        }
        this.incorrectList = {};
        for(var word in this.wordList) {
            this.incorrectList[word] = [];
            var lastOccurrence = 0;
            var length = word.length;
            var index;
            while (this.html.indexOf(word, lastOccurrence) > -1) { 
              index = this.html.indexOf(word, lastOccurrence);
              this.incorrectList[word].push([index, index + length]);
              lastOccurrence = (index + length);
            }
          }  
      };

      this.checkWords = function() {
        this.scanTypedText();
        if (this.contenteditable) {
          var cursorPos = getCaretCharacterOffsetWithin(this.editable.get(0));
        }
        else if (!this.contenteditable) {
          var cursorPos = jQuery(this.editable).getCursorPosition();
        }
        var found = false;

        this.key = false;

        for(var word in this.incorrectList) {
          if (!found) {
            for (var i = 0; i < this.incorrectList[word].length; i++ ) {
              if (cursorPos <= this.incorrectList[word][i][1] && cursorPos >= this.incorrectList[word][i][0]) {
                found = true;
                this.key = word;
              }
            }
          }
        }
          
        if (found) {
          if (jQuery('#contextmenu').length == 0) {
            jQuery('<div>').attr('id', 'contextmenu').appendTo('body');
            jQuery('#contextmenu').css('z-index', 1000);
            jQuery('#contextmenu').css('position', 'absolute');
            jQuery('<ul>').appendTo('#contextmenu');
          }

          if (this.iframe) {
            jQuery('#contextmenu').css('top', this.mouseY + this.iframe.offset()['top']);
            jQuery('#contextmenu').css('left', this.mouseX + this.iframe.offset()['left']);
          }
          else {
            jQuery('#contextmenu').css('top', this.mouseY);
            jQuery('#contextmenu').css('left', this.mouseX);
          }

          jQuery('#contextmenu ul').empty();

          var str = "";

          for (var i = 0; i < this.wordList[this.key].length; i++) {
            str += "<li class='suggestion'>" + this.wordList[this.key][i] + "</li>";
          }
          str += "<li class='close'>Ignore All</li>";

          jQuery('#contextmenu ul').html(str);  

          jQuery('#contextmenu li.close').click(function() {
              jQuery('#contextmenu').remove();
              that.editable.attr('spellcheck','false');
              that.editable.unbind('keyup');
              that.disabled = true;
              if (that.contenteditable) {
                var value = that.editable.html();
                that.editable.html('');
                that.editable.html(value);
              }
              else {
                var value = that.editable.val();
                that.editable.val('');
                that.editable.val(value);
              }
          });

          jQuery('#contextmenu li.suggestion').click(function() {
              var pattern = "\\b" + that.key + "\\b";
              var re = new RegExp(pattern, "g");
              if (that.contenteditable) {
                var htmlBlock = that.editable.html();
              }
              else {
                var htmlBlock = that.editable.val();
              }
              htmlBlock = htmlBlock.replace(re, jQuery(this).text());
              if (that.contenteditable) {
                that.editable.html(htmlBlock);
              }
              else {
                that.editable.val(htmlBlock);
              }
              jQuery('#contextmenu').remove();
              that.editable.attr('spellcheck','true');
              that.scanTypedText();
          });
        } 
      };

    }; // end SpellChecker

