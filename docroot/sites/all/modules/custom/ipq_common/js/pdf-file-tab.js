/**
 * Created by jdodge on 2/25/2016.
 */

(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.custom = {
    attach: function (domcontext, settings) {
      // Iterate through each file tab in order to register an event that occurs inside its iframe ('pagerendered'
      // event triggered by pdfjs)
      $('#exhibits-wrapper .exhibit-type-file iframe', domcontext).each(function (e) {
        // var iframe = $(exhibitId + ' iframe', domcontext).get(0);
        var iframe = this;

        // Allow the parent frame to resize the iframe to fit the content
        var iDoc = iframe.contentWindow || iframe.contentDocument;
        if (iDoc.document) {
          // Responsd to PDF.js 'pagerendered' event in order to resize the iframe
          iDoc.window.addEventListener('pagerendered', function (e) {
            try {
              // Trigger a custom event (not used right now)
              // $(document).trigger("pdfrendered", [this]);

              // Old way:
              //parent.ipqTabRedraw();

              // Resize iframe containers
              $('.exhibit-content .file-content').each(function (e) {
                $(this).css('height', '500px');
              });

              console.log(this);

            }
            catch (e) {
              console.log(e);
            }
          });
        }
      });
    },
    pdfExhibitRedraw: function() {

    }
  };

})(jQuery, Drupal, this, this.document); //END - Closure
