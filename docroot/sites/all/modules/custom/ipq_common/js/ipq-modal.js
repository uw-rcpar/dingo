(function ($) {

  Drupal.behaviors.rcpar_ipq_modal = {
    attach: function (context, settings) {
      var that = this;
      $('.modal-load', context).once('modalLoad', function () {
        $(this).on('click', function () {
          $('#myModal .modal-body').html(
            '<div class="progress progress-striped active" style="margin-bottom:100px; margin-top: 100px;"><div class="progress-bar" style="width: 100%"></div></div>'
          );
          $('#myModal').modal();
          $('#myModal').on('show.bs.modal', function () {
            $('.modal .modal-body').css('overflow-y', 'auto');
            $('.modal .modal-body').css('max-height', $(window).height() * 0.9);
          });
          var url = $(this).attr('href');
          that.ajaxLoadModal(url);
          return false;
        });
      });

      $(".region-content", context).on('click', ".ajax-nav, .ajax-nav-pager a", function () {
        var destination = $(this).attr('href');
        that.ajaxLoadModal(destination);
        return false;
      });
    },

    ajaxLoadModal: function (url) {
      // we need to ensure that the url starts with "/ajax"
      if (url.indexOf("/ajax") != 0) {
        url = "/ajax" + url;
      }
      url = url + '?no-js=1';
      // TODO: in case we use this technique, we would probably want to change this loader
      $("#myModal .modal-body").html(
        '<div class="progress progress-striped active" style="margin-bottom:100px; margin-top: 100px;"><div class="progress-bar" style="width: 100%"></div></div>'
      );
      $("#myModal .modal-body").load(url, '{ "choices[]": [ "Jon", "Susan" ] }', function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "error") {
          if (xhr.status == 403) {
            $("#myModal .modal-body").html('<div class="error">You are not authorized to access this page.</div>');
          }
          else {
            $("#myModal .modal-body").html('<div class="error">Internal Error</div>');
          }
        }
        Drupal.attachBehaviors("#myModal .modal-body");
        //this code does not work on ipq but on details for monitoring center
        var chapter_collection = [];
        var version = 0;
        $.each($('.view-id-course_elements h3'), function () {
          var item = $(this).find(".viewed-wrapper");
          chapter_collection.push(item.attr('id'));
          version = item.attr('data-version');
        });

        $('.monitoring-center-courses > .view-course-elements > .view-content').find('a').each(function () {
          classes = $(this).attr('class');
          html = $(this).html();
          $(this).replaceWith('<span class="' + classes + '">' + html + "</span>");
        });

        var uid = 0;
        var pid = "";
        if ($('.view-course-elements').closest('.modal-body').length) {
          uid = document.getElementById("details-uid").value;
          pid = "/" + document.getElementById("details-pid").value;
        }
        $.ajax({
          url: Drupal.settings.basePath + "viewed-info/" + chapter_collection + "/" + uid + "/" + version + pid,
          success: function (data) {
            $.each(data, function (id) {
              $("#" + id).append(data[id]);
              $("#" + id).find('.chap-loading-tag').remove();
            });
          }
        });
      });
    }

  };

}(jQuery));