(function ($) {
  Drupal.behaviors.rcpar_ipq_session_review = {

    attach: function (context, settings) {
      $('.pager-link', context).click(function(){
        $('#force_question').val($(this).attr('data-qid'));
        $("#edit-next-question").click();
        return false;
      });
      // this button appears when the question is disabled
      $('.disabled-question-next', context).click(function () {
        if ($('input[name=last_question_in_test]').val() == true) {
          $('#submit-test-link').click();
        }
        else {
          $('#edit-next-question').click();
        }
        return false;
      });

    }
  }
  
}(jQuery));
