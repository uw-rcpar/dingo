(function ($) {
  Drupal.behaviors.rcpar_ipq_notes_display = {
    attach: function (context, settings) {

      // todo - We use once() here to prevent the event handler attaching twice,
      // but aren't sure why it's attaching twice to begin with. Should look into this... -Pedro
      // look at https://jira.rogercpareview.com/browse/IB-275 for more info

      // Delete note on Quiz Question pages.
      $('.delete-note', context).once('edit-processed', function() {
        $(this).click(function(){
            var confirmed = confirm("Are you sure that you want to delete the note?\nThis operation can be undone");
            if(confirmed){
              $(this).trigger('ipq_delete_confirmed');
            }
            return false;
        });
      });
      // Edit note on Quiz Question pages.
      $('.edit-note', context).once('edit-processed', function() {
        $(this).click(function () {
          $(".node-edition-container", $(this).parent()).show();
          return false;
        });
      });
    }
  }
}(jQuery));