if (!hotVariables){

  var hotVariables = {};

  function addSpreadsheetSettings(hot_config, cells_info){
    hot_config.colHeaders = true;
    hot_config.rowHeaders = true;
    hot_config.manualColumnResize = true;
    hot_config.manualRowResize = true;
    hot_config.enterMoves = {row: 1, col: 0};
    hot_config.formulas = true;
    hot_config.columnSorting = true;
    hot_config.afterValidate = function(isValid, value) {
        if (!isValid && value.indexOf('=') > -1 && value.indexOf('(') > -1 && value.indexOf(')') > -1) {
          return true;
        }
    };

    hot_config.beforeKeyDown = function(evt) {
      if ( jQuery(evt.target).hasClass('handsontableInput') ) {
        var hotId = jQuery(this.rootElement).attr('data-hot-id');
        var that = getHotVariables(hotId);
        if (that.allowedCharacters.indexOf(evt.key) !== -1 || evt.key == "Backspace") {
          if (evt.key == "Backspace") {
            that.currentFormula = that.currentFormula.slice(0, -1);
            jQuery('.formula[data-hot-id='+hotId+']').val( that.currentFormula );
          }
          else {
            that.currentFormula += evt.key;
            jQuery('.formula[data-hot-id='+hotId+']').val( that.currentFormula );
          }
        }
      }
    };

    hot_config.afterOnCellMouseDown = function(e, coords, td) {
      var hotId = jQuery(this.rootElement).attr('data-hot-id');
      if (bowser.name == "iPad" || bowser.ipad == true) {
        var popup = jQuery('#ipad-text-input-popup');
        popup.modal();
        popup.find('.modal-title').text('Edit Cell Text');
        popup.find('input').css('width','100%');
        popup.unbind('keypress');
        popup.keypress(function(e) {
          if (e.which == 13) {
            var newvalue = popup.find('input').val();
            getHotVariables(hotId).spreadsheet.setDataAtCell(coords.row, coords.col, newvalue);
            popup.find('input').val('');
            popup.find('input').blur();
            popup.modal('hide');
            $('body').scrollTop(0);
            $('.wtHolder').scrollTop(0);
          }
        });
      }
    };

    hot_config.afterChange = function(changes) {
      var hotId = jQuery(this.rootElement).attr('data-hot-id');
      var that = getHotVariables(hotId);

      if (changes) {
        if (changes[0]) {
          if (changes[0][3] && changes[0][3].indexOf) { // we only want to do this with sting inputs and indexOf is only defined for strings
            if (changes[0][3].indexOf('=') !== -1 && changes[0][3].indexOf("(") !== -1 && changes[0][3].indexOf(")") == -1 && that.currentFormula !== changes[0][3]) {
              that.currentFormula = changes[0][3];
            }
          }
        }
      }
    };

    hot_config.afterSelectionEnd = function (r, c, r2, c2) {
      var hotId = jQuery(this.rootElement).attr('data-hot-id');
      var that = getHotVariables(hotId);
      that.oldCellValue = that.spreadsheet.getValue();
      if (that.oldCellValue == null) {
        that.oldCellValue = '';
      }
      if (that.currentFormula) {
        if (that.currentFormula.indexOf("(") !== -1 && that.currentFormula.indexOf(")") == -1) {
          if (that.spreadsheet.getDataAtCell(r, c) == that.currentFormula) {
            that.currentCell[0] = r;
            that.currentCell[1] = c;
            that.currentCellStr = that.colHeaders[c] + (r + 1);

            jQuery('.current-cell[data-hot-id=' + hotId + ']').text(that.currentCellStr);
          }

          var formulaStart = that.currentFormula.split("(")[0];

          // This is really annoying, but it's necessary to properly label rows. For example, if r equals 0, we need to change it to 1 so that the cell label can be, for example, A1. The cell label will be used for Excel formulas and it is also displayed in the upper left corner above the spreadsheet.
          r++;
          r2++;

          var col, row;

          for (col = c; col <= c2; col++) {
            if (that.selection.indexOf(that.colHeaders[col] + r) == -1) {
              if ((that.colHeaders[col] + r) !== that.currentCellStr) {
                that.selection.push(that.colHeaders[col] + r);
              }
            }
          }
          for (row = (r + 1); row <= r2; row++) {
            if (that.selection.indexOf(that.colHeaders[c] + row) == -1) {
              if ((that.colHeaders[c] + row) !== that.currentCellStr) {
                that.selection.push(that.colHeaders[c] + row);
              }
            }
          }
          var formulaStr = that.selection.join();
          formulaStr = formulaStart + "(" + formulaStr + ")";

          jQuery('.formula[data-hot-id=' + hotId + ']').val(formulaStr);
          that.currentFormula = jQuery('.formula[data-hot-id=' + hotId + ']').val();
          that.spreadsheet.setDataAtCell(that.currentCell[0], that.currentCell[1], that.currentFormula);
        }
        else {
          that.selection = [];
          jQuery('.formula[data-hot-id=' + hotId + ']').val(that.spreadsheet.getDataAtCell(r, c));
          that.currentFormula = jQuery('.formula[data-hot-id=' + hotId + ']').val();
          that.currentCell[0] = r;
          that.currentCell[1] = c;
          that.currentCellStr = that.colHeaders[c] + (r + 1);
          jQuery('.current-cell[data-hot-id=' + hotId + ']').text(that.currentCellStr);
        }
      }
      else {
        that.currentCell[0] = r;
        that.currentCell[1] = c;
        that.currentCellStr = that.colHeaders[c] + (r + 1);
        jQuery('.current-cell[data-hot-id=' + hotId + ']').text(that.currentCellStr);
      }
      // If cells_info exists, the spreadsheet is a hot grid and
      // dropdown and numeric cells need popups activated.
      // (If cells_info doesn't exist, then the spreadsheet is the modal spreadsheet.)
      if (cells_info) {
        // If cell type is dropdown, activate answer popup.
        if (cells_info[r][c] && cells_info[r][c]['type'] == "dropdown") {
          window.answerSelectionPopup(r, c, this.getInstance());
        }
        // If cell type is numeric, activate entry popup.
        if (cells_info[r][c] && cells_info[r][c]['type'] == "numeric") {
          window.answerEntryPopup(r, c, this.getInstance());
          this.getInstance().deselectCell();
        }
      }

    }

    return hot_config;
  }


  function getHotVariables(hotId){
    var hv = hotVariables[hotId];
    return hv;
  }

  function initHotSpreadsheetVariables(hotid, instance){
    if (!hotVariables[hotid]){
      hotVariables[hotid] = {};
    }
    hotVariables[hotid].hotId = hotid;
    hotVariables[hotid].colHeaders = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    hotVariables[hotid].selection =  [];
    hotVariables[hotid].currentFormula = '';
    hotVariables[hotid].currentCell =  [];
    hotVariables[hotid].currentCellStr = '';
    hotVariables[hotid].copyBuffer = '';
    hotVariables[hotid].spreadsheet = null;
    hotVariables[hotid].oldCellValue = '';
    hotVariables[hotid].allowedCharacters = 'abcdefghijklmnopqrstuvwxyz=(),:+*-0123456789';
    hotVariables[hotid].spreadsheet = instance;

    hotVariables[hotid].processFormulaChange = function() {
      this.currentFormula = jQuery('.formula[data-hot-id='+this.hotId+']').val();
      if (this.currentCell[0] > -1 && this.currentCell[1] > -1) {
        this.spreadsheet.setDataAtCell(this.currentCell[0], this.currentCell[1], this.currentFormula);
      }
    };
  }

  function setSpreadsheetBtnHandlers(){
    jQuery('.show-functions').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      jQuery('.functionslist[data-hot-id='+hotId+']').css('display','block');
    });

    jQuery('.functionslist .close').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      jQuery('.functionslist[data-hot-id='+hotId+']').css('display', 'none');
    });

    jQuery('.function-names').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      jQuery('.function-name-input[data-hot-id='+hotId+']').val( jQuery('.function-names[data-hot-id='+hotId+']').val() );
    });

    jQuery('.function-name-input').keyup(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      var currentVal = jQuery('.function-name-input[data-hot-id='+hotId+']').val();
      var found = false;
      jQuery('.function-names option').each(function() {
        if (found == false) {
          var regex = new RegExp("^" + currentVal);
          if ( jQuery(this).val().search(regex) > -1 ) {
            jQuery('.function-names[data-hot-id='+hotId+']').val(jQuery(this).val());
            found = true;
          }
        }
      });
    });

    jQuery('.function-apply').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      var vars = getHotVariables(hotId);
      jQuery('.formula[data-hot-id='+hotId+']').val('=' + jQuery('.function-names[data-hot-id='+hotId+']').val() + '()');
      vars.processFormulaChange();
    });

    jQuery('.function-submit').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      var vars = getHotVariables(hotId);
      jQuery('.formula').val('=' + jQuery('.function-names[data-hot-id='+hotId+']').val() + '()');
      vars.processFormulaChange();
      jQuery('.functionslist[data-hot-id='+hotId+']').css('display', 'none');
    });

    jQuery('.function-cancel').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      jQuery('.functionslist[data-hot-id='+hotId+']').css('display', 'none');
    });

    jQuery('.cancel').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      var that = getHotVariables(hotId);

      that.spreadsheet.setDataAtCell(that.currentCell[0], that.currentCell[1], that.oldCellValue);
      that.spreadsheet.unlisten();
      that.currentFormula = '';
      jQuery('.formula[data-hot-id='+hotId+']').val( that.currentFormula );
      that.currentCell[0] = -1;
      that.currentCell[1] = -1;
    });

    jQuery('.submit').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      var that = getHotVariables(hotId);

      that.spreadsheet.unlisten();
      that.currentFormula = '';
      jQuery('.formula[data-hot-id='+hotId+']').val( that.currentFormula );
      that.currentCell[0] = -1;
      that.currentCell[1] = -1;
    });

    jQuery('.cut').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      var that = getHotVariables(hotId);

      that.copyBuffer = that.spreadsheet.getDataAtCell(that.currentCell[0], that.currentCell[1]);
      jQuery('.formula[data-hot-id='+hotId+']').val("");
      that.spreadsheet.setDataAtCell(that.currentCell[0], that.currentCell[1], "");
    });

    jQuery('.copy').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      var that = getHotVariables(hotId);

      that.copyBuffer = that.spreadsheet.getDataAtCell(that.currentCell[0], that.currentCell[1]);
    });

    jQuery('.paste').click(function() {
      var hotId = jQuery(this).attr('data-hot-id');
      var that = getHotVariables(hotId);
      that.spreadsheet.setDataAtCell(that.currentCell[0], that.currentCell[1], that.copyBuffer);
    });

    jQuery('.formula').keyup(function(evt) {
      var hotId = jQuery(this).attr('data-hot-id');
      var vars = getHotVariables(hotId);
      vars.processFormulaChange();
    });
  }

}
