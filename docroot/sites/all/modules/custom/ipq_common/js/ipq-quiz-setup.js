(function ($) {
  Drupal.behaviors.rcpar_ipq_quiz_setup = {
    update_checkboxes_availability: function () {
      var select_all_cb_value = $('#select-all-none').val();
      $('.question_count:visible').each(function () {
        var cb = $("input[type=checkbox]", $(this).parent().parent());
        if ($('.question-count-value', $(this)).html() == '0') {
          $(cb).prop("disabled", true);
          $(cb).prop('checked', false);
        }
        else {
          $(cb).prop("disabled", false);

          // depending of the value on the select-all-none element
          // we might have to set or unset the checkboxes
          if (select_all_cb_value == 'selectAll') {
            $(cb).prop('checked', true);
          }
          else if (select_all_cb_value == 'selectNone') {
            $(cb).prop('checked', false);
          }
        }
      });
      this.update_next_button_state();
    },
    update_next_button_state: function () {
      var at_least_one_checked = false;
      $('.options-container input[type=checkbox]').each(function () {
        if ($(this).is(":checked")) {
          at_least_one_checked = true;
        }
      });
      $('#edit-next').prop('disabled', !at_least_one_checked);
    },
    update_next_button_state_step2_quiz: function () {
      var count = 0;
      $('select.question-type-count').each(function (index) {
        var v = $(this).val();
        count = count + parseInt(v, 10);
      });
      // we only enable the next button if the user selected at least one question
      $('#edit-next').prop('disabled', count == 0);
    },
    update_next_button_state_step2_exam: function () {
      var disable = true;
      if ($('#edit-understand-information').is(":checked")) {
        disable = false;
      }
      $('#edit-next').prop('disabled', disable);
    },
    attach: function (context, settings) {
      var that = this;


      // function to know if research questions should be shown
      var show_research = function() {
        if (
          // if both hasnasb and "no research" are checked, don't show research
          $('#edit-hasnasba').is(":checked") && $('#edit-no-research').is(":checked") ||
          // if hasnasb is unchecked, don't show research
          $('#edit-hasnasba').is(":not(:checked)") ||
          // if "no research" is checked, don't show research
          $('#edit-no-research').is(":checked")) {
          return false;
        }
        else {
          return true;
        }
      }


      $('.mutualy-exclusive-cb input[type=checkbox]').click(function () {
        $('.mutualy-exclusive-cb input[type=checkbox]').not(this).prop('checked', false);
        return true;
      });

      $('#edit-only-no-seen').click(function () {
        $('.option-container .question_count').hide();
        // note that we have two containers for the counts, one for
        // the never seen questions including the research questions and other
        // without them
        if ($(this).is(":checked")) {
          if (show_research()) {
            $('.option-container .never-seen.with_research').show();
          }
          else {
            $('.option-container .never-seen.no_research').show();
          }
        }
        else {
          if (show_research()) {
            $('.option-container .total.with_research').show();
          }
          else {
            $('.option-container .total.no_research').show();
          }
        }
        that.update_checkboxes_availability();
      });

      $('#edit-only-incorrect').click(function () {
        $('.option-container .question_count').hide();
        // note that we have two containers for the counts, one for
        // the incorrect questions including the research questions and other
        // without them
        if ($(this).is(":checked")) {
          if (show_research()) {
            $('.option-container .incorrect.with_research').show();
          }
          else {
            $('.option-container .incorrect.no_research').show();
          }
        }
        else {
          if (show_research()) {
            $('.option-container .total.with_research').show();
          }
          else {
            $('.option-container .total.no_research').show();
          }
        }
        that.update_checkboxes_availability();
      });

      $('#edit-only-skipped').click(function () {
        $('.option-container .question_count').hide();
        // note that we have two containers for the counts, one for
        // the skipped questions including the research questions and other
        // without them
        if ($(this).is(":checked")) {
          if (show_research()) {
            $('.option-container .skipped.with_research').show();
          }
          else {
            $('.option-container .skipped.no_research').show();
          }
        }
        else {
          if (show_research()) {
            $('.option-container .total.with_research').show();
          }
          else {
            $('.option-container .total.no_research').show();
          }
        }
        that.update_checkboxes_availability();
      });

      $('#edit-hasnasba').click(function () {
        // when the user changes the value of this checkbox
        // we need to recalculate which count container we should show

        // we are going to get the count container selector name
        // to use it later to show it
        var enabled_containe_selector = '.option-container';
        if ($('#edit-only-skipped').is(":checked")) {
          enabled_containe_selector += ' .skipped';
        }
        else if ($('#edit-only-incorrect').is(":checked")) {
          enabled_containe_selector += ' .incorrect';
        }
        else if ($('#edit-only-no-seen').is(":checked")) {
          enabled_containe_selector += ' .never-seen';
        }
        else {
          enabled_containe_selector += ' .total';
        }
        if (show_research()) {
          enabled_containe_selector += '.with_research';
        }
        else {
          enabled_containe_selector += '.no_research';
        }

        // we hide all question count containers
        $('.option-container .question_count').hide();
        // now we show the selected one
        $(enabled_containe_selector).show()

        // we also need to update which chapters checkbox should be enabled
        that.update_checkboxes_availability();
      });
      $('#edit-no-research').click(function () {
        // when the user changes the value of this checkbox
        // we need to recalculate which count container we should show

        // we are going to get the count container selector name
        // to use it later to show it
        var enabled_containe_selector = '.option-container';
        if ($('#edit-only-skipped').is(":checked")) {
          enabled_containe_selector += ' .skipped';
        }
        else if ($('#edit-only-incorrect').is(":checked")) {
          enabled_containe_selector += ' .incorrect';
        }
        else if ($('#edit-only-no-seen').is(":checked")) {
          enabled_containe_selector += ' .never-seen';
        }
        else {
          enabled_containe_selector += ' .total';
        }
        if (show_research()) {
          enabled_containe_selector += '.with_research';
        }
        else {
          enabled_containe_selector += '.no_research';
        }

        // we hide all question count containers
        $('.option-container .question_count').hide();
        // now we show the selected one
        $(enabled_containe_selector).show()

        // we also need to update which chapters checkbox should be enabled
        that.update_checkboxes_availability();
      });

      $('#select-all-none').change(function () {
        if ($(this).val() == 'selectAll') {
          $('.options-container input[type=checkbox]').prop('checked', true);
        }
        else if ($(this).val() == 'selectNone') {
          $('.options-container input[type=checkbox]').prop('checked', false);
        }
        that.update_checkboxes_availability();
      });

      $('.options-container input[type=checkbox]').click(function () {
        that.update_next_button_state();
      });


      $('#rename_current_session').click(function () {
        $('#current-name-container').hide();
        $('#new-name-container').show();
        return false;
      });

      if ($('.options-container input[type=checkbox]').length > 0) {
        this.update_checkboxes_availability();
        this.update_next_button_state();
      }

      // if we have question-type-count selects, then we are on step two
      if ($('.question-type-count').length > 0) {
        this.update_next_button_state_step2_quiz();
        $('.question-type-count', context).change(function () {
          that.update_next_button_state_step2_quiz();
        });
      }

      if ($('#edit-understand-information').length > 0) {
        this.update_next_button_state_step2_exam();
        $('#edit-understand-information', context).change(function () {
          that.update_next_button_state_step2_exam();
        });
      }

    }
  }
}(jQuery));