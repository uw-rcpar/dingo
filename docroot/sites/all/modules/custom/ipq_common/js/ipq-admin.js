(function ($) {
    Drupal.behaviors.rcpar_ipq_admin = {
        attach: function (context, settings) {
            // The click on correct answer will be save like a correct answer
            $('.ipq-answers-radio').click(function () {
              $('.field-name-field-answers .ief-entity-submit').trigger("mousedown");
            });
            $(".question-types-list-item").click(function(){
                var id = $(this).attr("id");
                id = id.replace('show-','');
                $(".question-type-info").hide();
                $("#"+id).css('display', 'inline-block');
            });
        }
    }
}(jQuery));