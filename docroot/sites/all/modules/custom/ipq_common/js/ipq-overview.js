( function($) {
  Drupal.behaviors.rcpar_ipq_overview = {
   attach : function(context, settings) {
    $('#ipq-how-to').on('show.bs.modal', function() {
      $('#ipq-how-to').draggable({
        handle: '.modal-header'
      });
    });

    $(document).ready(function() {
     $('#edit-submit').hide();
    });

    //submit the overview dropdown menu on change
    $(document).on("click", '.dropdown-menu li a', function(e) {
     e.preventDefault();
     $(this).closest('.form-type-select .form-item-sessions').find('input').val($(this).data('value'));
     $(this).closest('form').submit();
    });

   }
  }
 }(jQuery));
