jQuery(document).ready(function() {
  // Global variable, to allow us to manipulate the timer on other sections
  window.IPQCountDownTimerObj = null;
  window.DigitalTimer = function(settings) {
    this.timerSettings = settings;
    this.firstSecond = true;

    this.getTimeElapsed = function() {
        this.timerMinutesElapsed = Math.floor((((new Date().getTime() - new Date(this.pastTime.getTime())) % 86400000) % 3600000) / 60000);
        this.timerHoursElapsed = Math.floor(((new Date().getTime() - new Date(this.pastTime.getTime())) % 86400000) / 3600000);
        this.timerSecondsElapsed = Math.floor(((((new Date().getTime() - new Date(this.pastTime.getTime())) % 86400000) % 3600000) % 60000) / 1000);
        this.elapsedInterval = setTimeout(this.getTimeElapsed.bind(this), 1000);
    };
    
    this.getTimeRemaining = function() {
      if (!this.checkIfTimerDone() && !this.pausedDate) {
        this.timerSecondsRemaining = Math.floor(((((new Date(this.futureTime.getTime()) - new Date().getTime()) % 86400000) % 3600000) % 60000) / 1000);
        this.timerMinutesRemaining = Math.floor((((new Date(this.futureTime.getTime()) - new Date().getTime()) % 86400000) % 3600000) / 60000);
        this.timerHoursRemaining = Math.floor(((new Date(this.futureTime.getTime()) - new Date().getTime()) % 86400000) / 3600000);

        this.timeRemainingInterval = setTimeout(this.getTimeRemaining.bind(this), 1000);
      }

      this.handleTimerUI();
    };

    this.handleTimerUI = function() {
      if (this.timerSettings.tickHandler && !this.firstSecond) {
        settings.tickHandler();
      }

      // We want to always show the remaining hours, even if they are 0
      var hours = this.timerHoursRemaining;

      hours = ("0" + hours).slice(-1);

      jQuery(this.timerSettings.selector + ' .hours-remaining').html(hours);
      jQuery(this.timerSettings.selector + ' .hours-remaining-container .label').html(":");

      // We might have to format the minutes a little bit (always shows minutes using two digits)
      var minutes = this.timerMinutesRemaining;
      if (this.timerSettings.twoDigitsForMinutes){
        minutes = ("00" + minutes).slice(-2);
      }
      jQuery(this.timerSettings.selector + ' .minutes-remaining').html(minutes);
      jQuery(this.timerSettings.selector + ' .minutes-remaining-container .label').html(":");

      // Same thing that for minutes (always shows seconds using two digits)
      if (this.showSeconds()){
        var seconds = this.timerSecondsRemaining;
        // we want to show the seconds on decrements of 5 seconds
        seconds = seconds - (seconds % 5);
        if (this.timerSettings.twoDigitsForSeconds){
          seconds = ("00" + seconds).slice(-2);
        }
        jQuery(this.timerSettings.selector + ' .seconds-remaining').html(seconds);
        jQuery(this.timerSettings.selector + ' .seconds-remaining-container .label').html("");
      }

      // Solution for weird bug where seconds remaining is 0 at the very beginning of a countdown
      if (this.timerSecondsRemaining !== 0) {
        this.firstSecond = false;
      }
    };

    this.setTimer = function() {
      this.pastTime = new Date();  
      this.getTimeElapsed();
    };

    this.setCountdown = function(obj) {
      this.curTime = new Date().getTime();
      this.futureTime = new Date();

      // Temporary values
      this.timerMinutesRemaining = 59;
      this.timerHoursRemaining = 99;
      this.timerSecondsRemaining = 59;

      if (this.timerSettings.hours) {
        this.timerSettings.hours = parseInt(this.timerSettings.hours);
        this.curHour = new Date().getHours();
        this.futureTime.setHours(this.curHour + this.timerSettings.hours);
        if (this.curTime >= this.futureTime.getTime()) {
          console.log('cannot set countdown to time in the past');
          return false;
        }
        this.futureHour = this.futureTime.getHours();
      }
      if (this.timerSettings.minutes) {
        this.timerSettings.minutes = parseInt(this.timerSettings.minutes);
        this.curMin = new Date().getMinutes();
        this.futureTime.setMinutes(this.curMin + this.timerSettings.minutes);
        if (this.curTime >= this.futureTime.getTime()) {
          console.log('cannot set countdown to time in the past');
          return false;
        }
        this.futureMin = this.futureTime.getMinutes();
      }
      if (this.timerSettings.seconds) {
        this.timerSettings.seconds = parseInt(this.timerSettings.seconds);
        this.curSec = new Date().getSeconds();
        this.futureTime.setSeconds(this.curSec + this.timerSettings.seconds);
        if (this.curTime >= this.futureTime.getTime()) {
          console.log('cannot set countdown to time in the past');
          return false;
        }
        this.futureSec = this.futureTime.getSeconds();
      }

      this.getTimeRemaining();
      this.refreshUI();
    };

    this.resetTimer = function() {
      clearTimeout(this.elapsedInterval);
    };

    this.resetCountdown = function() {
      clearTimeout(this.timeRemainingInterval);
    };

    this.getTimerMinutesElapsed = function() {
      return this.timerMinutesElapsed;
    };

    this.getTimerHoursElapsed = function() {
      return this.timerHoursElapsed;
    };

    this.getTimerSecondsElapsed = function() {
      return this.timerSecondsElapsed;
    };

    this.getTimerMinutesRemaining = function() {
      return this.timerMinutesRemaining;
    }

    this.getTimerHoursRemaining = function() {
      return this.timerHoursRemaining;
    }

    this.getTimerSecondsRemaining = function() {
      return this.timerSecondsRemaining;
    }

    this.handleTimerDone = function() {
      this.resetCountdown();
      this.resetTimer();
      if (this.timerSettings.callback) {
        try {
          this.timerSettings.callback();
        }
        catch (err) {
          console.log("Timer error: " + err);
        }
      };
    }

    this.checkIfTimerDone = function() {
      if (new Date().getTime() >= this.futureTime.getTime()) {
        this.handleTimerDone();
        return true;
      }
      return false;
    }

    this.examSimulatorTimerStart = function() {
      this.resetCountdown();
      this.setCountdown(this.timerSettings);
    };

    this.quizTimerStart = function() {
      this.resetTimer();
      this.setTimer();  
    };

    this.pauseTimer = function() {
      clearTimeout(this.elapsedInterval);
      // Let's see if the timer it's not already paused
      if (!this.pausedDate){
        this.pausedDate = new Date();
      }
    };

    this.unpauseTimer = function() {
      // To unpause the timer, it needs to be on pause status
      // if it's not, we just don't do nothing
      if(this.pausedDate) {
        var timeDiff = new Date() - this.pausedDate;
        this.futureTime.setTime(this.futureTime.getTime() + timeDiff);
        this.pausedDate = null;
        this.getTimeRemaining();
      }
    };

    this.showSeconds = function(){
      if (!this.timerSettings.showSeconds) {
        return false;
      }
      else {
        return true;
      }
    };

    this.refreshUI = function() {
      if (!this.timerSettings.seconds || !this.timerSettings.showSeconds) {
        jQuery(this.timerSettings.selector).addClass('no-seconds');
      }

      if (this.timerSettings.showSeconds) {
        jQuery(this.timerSettings.selector).removeClass('no-seconds');
      }
      if (!this.timerSettings.hours && !this.timerSettings.showHours) {
        jQuery(this.timerSettings.selector + ' .hours-remaining').hide();

        if (!this.timerSettings.minutes && !this.timerSettings.showMinutes) {
          jQuery(this.timerSettings.selector + ' .minutes-remaining').hide();

        }
      }
    };
  };

  var questionStartTime = new Date();
  //var prevQuestionDurations = [];

  //if (Cookies.get('sessiontimer') !== undefined) {
  if (Drupal.settings.ipq_sessiontimer) {
    //var timeobj = Cookies.getJSON('sessiontimer')['sessiontimer'];
    var timeobj = Drupal.settings.ipq_sessiontimer;
//    if (!timeobj['sessiontimer'].hours && !timeobj['sessiontimer'].minutes && !timeobj['sessiontimer'].seconds) {
    if (!timeobj['sessiontimer']) {
      var timeobj = {};
      //Cookies.set('sessiontimer', {'sessiontimer': timeobj, 'session_id': Drupal.settings.ipqSessionId});
      timeobj['sessiontimer'] = Drupal.settings.examCountdownTime;
      timeobj['session_id'] = Drupal.settings.ipqSessionId;
      jQuery('#timer_info').val(JSON.stringify(timeobj));
      //var prevQuestionDurations = [];
    }
  }
  else {
    var timeobj = Drupal.settings.ipq_sessiontimer;
    //Cookies.set('sessiontimer', {'sessiontimer': timeobj, 'session_id': Drupal.settings.ipqSessionId});
    timeobj['sessiontimer'] = timeobj['sessiontimer'];
    timeobj['session_id'] = Drupal.settings.ipqSessionId;
    jQuery('#timer_info').val(JSON.stringify(timeobj));
    //var prevQuestionDurations = [];
  }

  var update_timer_info = function(){
    var currentQuestionDuration = new Date().getTime() - questionStartTime.getTime();
    var currentQuestionDurationSeconds = (((currentQuestionDuration % 86400000) % 3600000)) / 1000;
    //prevQuestionDurations.push(currentQuestionDurationSeconds);
    var value = {'sessiontimer': {
      'hours': countdown.getTimerHoursRemaining(),
      'minutes': countdown.getTimerMinutesRemaining(),
      'seconds': countdown.getTimerSecondsRemaining()
    },
      'session_id': Drupal.settings.ipqSessionId,
      'duration': currentQuestionDurationSeconds
    }
    //Cookies.set('sessiontimer', value);
    jQuery('#timer_info').val(JSON.stringify(value));
  };

  jQuery('#edit-score-as-you-go-info-submit').mousedown(function(event){
    update_timer_info();
    $(this).click();
  });
  
  // when the form is submited, we need to update the timer info in the form hidden input
  jQuery('#ipq-common-session-navigation-form').submit(function(event){
    update_timer_info();
  });

// Leaving this here for testing. You can change the number values to whatever you want.
//
// uncomment if you want the timer to end in 3 seconds (seconds only)
// timeobj['sessiontimer'].hours = 0;
// timeobj['sessiontimer'].minutes = 0;
// timeobj['sessiontimer'].seconds = 3;
//
// uncomment if you want the timer to count down from 3 minutes (minutes only)
// timeobj['sessiontimer'].hours = 0;
// timeobj['sessiontimer'].minutes = 3;

  var countdown = new DigitalTimer({
    twoDigitsForMinutes: true,
    twoDigitsForSeconds: true,
    showHours: true,
    selector: '#digital-timer',
    hours: timeobj['sessiontimer'].hours,
    minutes: timeobj['sessiontimer'].minutes,
    seconds: timeobj['sessiontimer'].seconds,
    showSeconds: false,
    callback: function() {
     var modal = jQuery("#time-limit-reached");
     // Find the modal's <div class="modal-body"> so we can populate it
     var modalBody = jQuery('#time-limit-reached .modal-body');

      modal.find('.dismiss').click(function() {
        window.location.pathname = "/ipq/history";
      });
      
     //render away
     modal.modal();

     modal.draggable({
       handle: '.modal-header'
     });

     jQuery('#time-limit-reached').css({
      width : '35%', //probably not needed
      height : '35%', //probably not needed
      display : 'table', //important
      'overflow-y' : 'auto',
      'overflow-x' : 'auto',
      'top': '25%',
      'bottom': '25%',
      'margin': 'auto'
     });

     jQuery('.modal-title').html('Time Limit Reached');
    },
    tickHandler: function() {
      if (countdown.getTimerMinutesRemaining() <= 1 && countdown.getTimerHoursRemaining() == 0) {
        jQuery('.digital-timer').addClass('almost-done');
        countdown.timerSettings.showSeconds = true;
        countdown.refreshUI();
      }
    }
  });
  window.IPQCountDownTimerObj = countdown;
  countdown.examSimulatorTimerStart();
});
