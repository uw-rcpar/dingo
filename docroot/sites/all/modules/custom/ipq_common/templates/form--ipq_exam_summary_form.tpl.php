<?php
// Template for exam setup wizard
?>
<div class="ipq-container <?php rcpar_dashboard_user_is_freetrial($form['section']['#value']) ? print 'ipq-free-trial-gray-out' : ''; ?>">
	
	<h1 class="ipq-section-header"><?php print $form['section']['#value']; ?> Exam Simulation<div class="pull-right"><?php print drupal_render($form['actions']); ?></div></h1>

	<?php
		if (rcpar_dashboard_user_is_freetrial($form['section']['#value'])) {
	?>
			<!-- Free Trial mode message -->
			<div id='ipq-exam-setup-free-trial-mode-message'>
				<div id='ipq-exam-setup-content'>
					<div id='ipq-exam-setup-title'>
						YOU'RE IN FREE TRIAL MODE
					</div>
					<div id='ipq-exam-setup-message'>
						<p>
							For unlimited access to practice exams, please confirm you want to end your trial below and then enroll in a full course package on the <a href='/cpa-courses'>Courses & Products</a> page.
						</p>
						<span class='ipq-end-free-trial-form'>
							<?php
								print drupal_render($form['view_details']);
							?>
						</span>
					</div>
				</div>
			</div> <!-- end of Free Trial mode message -->
			<div id="activation-container-of-values"></div>
	<?php
		}
	?>

	<h2 class="ipq-section-subheading">Your practice exam will include the following:</h2>
	
	<ul class="exam-list">
		<?php
			$i = 1;
			foreach($form['quizlets']['#value'] as $quizlet_info){
				print '<li><strong>Testlet '.$i.':</strong> '.$quizlet_info['max_questions'].' '.$quizlet_info['question_type_human_name'].'</li>';
				$i++;
			}
		?>
	</ul>
	
	<div class="simulator-summary-note">Note: Questions in each testlet are selected randomly for each simulation.</div>

	
	<?php print drupal_render($form['scheduled_nasba']); ?>
	<div class="well">
	  <p>This simulated examination allows you to practice in an exam-like environment.  Upon completion, you will receive a summary to help you assess your strengths and weaknesses. Please note that your final score will not be an exact indicator of how you would perform on the actual CPA Exam.</p>
	  <?php print drupal_render_children($form); ?>
	</div>


</div><!-- /ipq-container -->


<!-- Tools Modal -->
<div id="ipq-more" class="modal">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="termsLabel" class="modal-title">
        <i class="fa fa fa-info-circle"></i> Research Task Authoritative Literature</h3>
    </div>
    <div class="modal-body ng-scope">
      <p>While you will have access to certain professional literature within this program (e.g. GAAS, IRC, and PCAOB standards),
        to obtain a free six-month subscription to FASB Current Text, FASB Original Pronouncements, and AICPA Professional Standards
        for research task questions we recommend all eligible students submit a free Request for Professional Literature from NASBA.
        To subscribe you must have a valid Notice To Schedule (NTS) and complete the registration at
        <a target="_blank" href="https://proflit.nasba.org/">https://proflit.nasba.org/</a>.</p>

      <p>Subscription approval can take up to three weeks to process, so make sure to submit your request in time to practice before your exams!</p>
    </div>
    <div class="modal-footer ng-scope">
      <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">OK great!</button>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal -->
