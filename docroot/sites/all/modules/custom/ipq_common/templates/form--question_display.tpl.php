<?php
$navigation_info = $form['navigation_info']['#value'];
$action = $navigation_info['action'];
$session_id = $navigation_info['session_id'];
$session_type = $navigation_info['session_type'];


$disabled_question = false;
// Don't show disabled questions unless an admin is viewing
if($navigation_info['question_status'] == 0 && !user_access('ipq_view_unpublished_questions')){
  $disabled_question = true;
}
//set the button label on the Score as you go modal window
//  $btn_label = ($navigation_info['last_in_question_set']) ? 'Submit Quizlet' : 'Next Question';
$btn_label = 'Continue';

$last_testlet = ($navigation_info['testlet_index'] + 1 == $navigation_info['testlet_count']) ? true : false;
?>
    <div class="ipq-question-wrapper ipq-<?php print $navigation_info['session_type']; ?>-wrapper">
        <input type="hidden" id="ipq-session-type" value="<?php print $session_type; ?>">
        <input type="hidden" id="is-last-quizlet" value="<?php print $last_testlet; ?>">
        <input type="hidden" id="is-disabled" value="<?php print $disabled_question; ?>">
        <input type="hidden" id="is-deleted" value="<?php print $navigation_info['deleted_question']; ?>">
        <div id="ipq-wrapper">
            <div id="ipq-progress-indicator" class="header-element">
                <div class="section-name">
                  <?php
                  // this comes from the taxonomy term description
                  print $navigation_info['exam_part'];
                  ?>
                </div>
                <div class="quiz-progress">
                  <?php
                  // loop through the testlet count and create testlet progress indicator divs
                  // and bootstrap tooltips for testlet question counts
                  $i = 1;
                  $n = 0;
                  while ($i <= $navigation_info['testlet_count']) {
                    // assign active class to current testlet
                    $testlet_class = '';
                    if ($n == $navigation_info['testlet_index']) {
                      $testlet_class = ' active';
                    }
                    // assign completed class to testlets already submitted
                    if ($n <  $navigation_info['testlet_index']) {
                      $testlet_class = ' completed';
                    }
                    ?>
                      <div class="testlet-<?php print $i; ?> quiz-testlet <?php print $testlet_class; ?>"><div data-toggle="tooltip" data-placement="bottom" title="<?php
                        print $navigation_info['testlet_question_count'][$n] . ' ' . $navigation_info['testlet_question_type'][$n]; ?>"><?php print ($navigation_info['session_type'] == 'exam' ? 'Testlet' : 'Quizlet')." $i"; ?></div></div>
                    <?php
                    $i++;
                    $n++;
                  } ?>
                </div>

              <?php print drupal_render($form['question_id']); ?>
              <?php if ($navigation_info['session_type'] == 'quiz') { ?>
                  <div class="chapter-number-name"><?php
                    // this comes from the taxonomy term description
                    print $navigation_info['section_title'];
                    ?>
                  </div>
              <?php } ?>
            </div>
            <div id="ipq-header" class="quiz-exam-header header-element">
                <div class="row">
                    <div class="ipq-timer">
                        <!-- Recommended Time -->
                      <?php if (!$disabled_question) { ?>
                          <div class="clock-icon"></div>
                          <!-- Exam Countdown Timer -->
                        <?php if ($navigation_info['session_type'] == 'exam') { ?>
                              <div class="digital-timer digi-timer clearfix exam-timer">
                                  <div id="digital-timer">
                                      <div class="timer-wrapper">
                                          <div class="hours-remaining-container time"><span class="amount hours-remaining"></span><span class="label">:</span></div>
                                          <div class="minutes-remaining-container time"><span class="amount minutes-remaining"></span><span class="label">:</span></div>
                                          <div class="seconds-remaining-container time"><span class="amount seconds-remaining"></span><span class="label"></span></div>
                                      </div>
                                      <div class="time-remaining">Exam Time<br />Remaining</div>
                                  </div>
                              </div>
                        <?php } ?>

                          <!-- Quiz Elapsed Time Counter -->
                        <?php if ($navigation_info['session_type'] == 'quiz') { ?>
                              <div class="digital-timer digi-timer clearfix">
                                  <div class="time-wrap"></div>
                                  <div class="collapsible-area">
                                      <div id="digital-timer">
                                          <div class="hours-elapsed-container time"><span class="amount hours-elapsed"></span><span class="label">:</span></div>
                                          <div class="minutes-elapsed-container time"><span class="amount minutes-elapsed"></span><span class="label">:</span></div>
                                          <div class="seconds-elapsed-container time"><span class="amount seconds-elapsed"></span><span class="label"></span></div>
                                      </div>
                                      <div class="time-elapsed">
                                          <div class="timer-pause"><button type="button" class="pause-timer" value="Pause" data-toggle="popover" title="" data-html="true" data-trigger="hover" data-content="Pause quiz" data-placement="left"></button></div>
                                          <div class="timer-recommended"><button type="button" class="btn-recommended" data-toggle="popover" title="" data-html="true" data-trigger="hover" data-content="Recommended Time<br /><?php print $navigation_info['reco_time']; ?>" data-placement="right"></button></div>
                                          <div class="time-label">Time<br />Elapsed</div>
                                      </div>
                                  </div>
                                  <button class="timer-tab"  data-toggle="popover" title="" data-html="true" data-trigger="hover" data-content="Hide timer" data-placement="right"></button>
                                  <button class="timer-tab closed"  data-toggle="popover" title="" data-html="true" data-trigger="hover" data-content="Show timer" data-placement="right"></button>
                              </div>
                        <?php } ?>
                      <?php } ?>
                    </div><!-- /ipq-timer-->

                    <!-- Tool menu-->
                    <div class="tool-menu tool-menu-no-tabs<?php if ($navigation_info['authoritative_literature'] != '') { ?> tool-menu-auth-lit<?php } ?>">
                      <?php if (!$disabled_question) {
                        // Don't show calculator, spreadsheet, or auth lit links for written communication questions
                        if ($navigation_info['current_question_type'] != 'ipq_wc_question') { ?>
                            <div class="calculator tool-menu-item"><a href="/ipq-tools/calculator" data-toggle="#ipq-tools" class="ipq-tools-modal" id="calculator-link"  data-target="#ipq-tools" ipqtitle="Calculator" aria-label="Calculator"><div class="icon-calculator"></div>Calc.</a></div>
                            <div class="spreadsheet tool-menu-item"><?php if ($navigation_info['current_question_type'] == 'ipq_mcq_question') { ?><div id="spreadsheet-empty"></div><span>Excel</span><?php } else { ?><a href="#" data-toggle="modal" data-target="#ipq-open-excel" class="ipq-open-excel" id="spreadsheet-link"><div class="icon-spreadsheet"></div>Excel</a>
                                    <a href="/ipq-tools/spreadsheet" data-toggle="#ipq-tools" data-target="#ipq-tools" class="ipq-tools-modal" id="nav-spreadsheet-link" ipqtitle="Spreadsheet Tool"><div class="icon-spreadsheet"></div>Excel</a><?php } ?>
                            </div>
                          <?php if ($navigation_info['authoritative_literature'] != '') { ?>
                                <div class="auth-lit tool-menu-item"><a href="#" id="auth-lit-link" data-toggle="modal" data-target="#ipq-auth-lit" aria-label="Authoritative Literature"><div class="icon-auth-lit"></div>Auth. Lit.</a></div>
                          <?php } ?>
                        <?php } ?>
                          <!-- Quiz overview -->
                          <div class="overview tool-menu-item">
                              <a data-toggle="modal" id="overview-link" href="#" data-target="#ipq-overview-modal" aria-label="Quiz Overview"><div class="icon-overview"></div>Overview</a>
                          </div>

                          <!-- Help -->
                          <div class="help tool-menu-item">
                              <a data-toggle="modal" id="help-link" data-target="#ipq-help-modal" aria-label="Help"><div class="icon-help"></div>Help</a>
                          </div>
                      <?php } ?>
                    </div><!-- /tool-menu-->
                  <?php
                  // options only appear on quizzes, not exams
                  if ($navigation_info['session_type'] == 'quiz') {

                    $tooltip_options = "";
                    // View Solution session option
                    if($navigation_info['show_view_solutions_opt']){
                      $active = '';
                      if($navigation_info['view_solutions']){
                        $active = 'active';
                      }
                      $tooltip_options .= "<a class='ipq-tools-modal link options-link ".$active."' id='view-solution-link' data-target='#ipq-view-solution' ipqtitle='View Solution'>View solution</a>";
                    }

                    $active = '';
                    if($navigation_info['score_as_you_go']){
                      $active = 'active';
                    }
                    $tooltip_options .= "<a class='options-link ".$active."' id='score-as-you-go' >Score as you go</a>";

                    // Show Timer session option
                    $active = '';
                    if($navigation_info['hide_timer']){
                      $active = 'active';
                    }
                    $tooltip_options .= "<a class='link options-link ".$active."' id='hide-timer-link'>Hide timer</a>";

                  }

                  ?>
                  <?php
                  // options only appear on quizzes, not exams
                  if ($navigation_info['session_type'] == 'quiz') {
                    ?>
                      <div class="options-wrapper <?php if (arg(1)=='exam' && (!empty($navigation_info['previous_question_id']))) { ?> exam-has-previous<?php } ?>">
                          <a class="options-icon" href="#"
                             data-toggle="tooltip"
                             title="<?php print $tooltip_options; ?>"
                             data-html="true"
                             data-container="#ipq-header"
                             data-placement="bottom"
                             data-trigger="click"
                             id="options-tooltip"> <i class="fa fa-caret-down"></i></a>
                      </div>
                  <?php } ?>
                </div><!-- /row-->
            </div><!-- /ipq-header-->
          <?php
          $question_count_class = '';
          // get the total number of questions in the current testlet
          $question_count = $navigation_info['testlet_question_count'][$navigation_info['testlet_index']];
          if ($question_count > 19) {
            $question_count_class = "stretch";
          }
          ?>
            <div class="question-nav <?php print $question_count_class; ?> clearfix question-type-<?php print drupal_html_class($navigation_info['current_question_type']); ?>" role="navigation">
              <?php

              $i = 1;

              foreach($navigation_info['current_type_question_list'] as $q):

                $extra_class = '';
                $flagged = (is_object($navigation_info['current_type_questions_information'][$q]) && $navigation_info['current_type_questions_information'][$q] -> is_flagged) ? "flagged" : "";

                if ($q == $navigation_info['question_id']) {
                  $extra_class .= ' current';
                }
                // If there's a 2 on $navigation_info['testlet_questions_status'][$q]
                // then the user didn't try to answer this question yet
                if ($navigation_info['testlet_questions_status'][$q] != IPQ_Q_STATUS_UNANSWERED) {
                  // otherwise the user did try
                  $extra_class .= ' tried';
                }

                ?>
                  <div class="number-flag-links number-<?php print $i; ?> <?php print $extra_class; ?>">
                    <?php if ($navigation_info['session_type'] == 'quiz') { ?>
                        <a data-qid="<?php print $q; ?>" href="" class="pager-link <?php print $extra_class; ?>"
                           data-toggle="tooltip"
                           title="<p><?php $ni=$i-1; print str_ireplace('"', '', $navigation_info['tooltips'][$ni]); ?></p>"
                           data-html="true"
                           data-container="#ipq-footer"
                           data-placement="top"
                           data-trigger="hover"
                           aria-label="Question <?php print $i; ?> of <?php print count($navigation_info['current_type_question_list']); ?>"><?php print $i; ?></a>
                    <?php } else { ?>
                        <a data-qid="<?php print $q; ?>" href="" class="pager-link <?php print $extra_class; ?>"><?php print $i; ?></a>
                    <?php } ?>

                      <a href="#" class="bookmark-ipq-flag <?php echo "btn-{$i} {$extra_class} $flagged"?>" qid="<?php echo $navigation_info['current_type_question_list'][$i-1]?>" session="<?php echo $navigation_info['session_id']?>">Bookmark this question</a>

                  </div>
                <?php $i++; ?>
              <?php endforeach; ?>
                <div class="clear-empty"></div>
            </div>
            <div id="ipq-container" class="ipq-container question-wrapper clearfix <?php print drupal_html_class($navigation_info['current_question_type']); ?> <?php if($navigation_info['deleted_question']) print 'deleted-question'; ?> <?php if ($navigation_info['current_question_type'] == 'ipq_mcq_question') { ?>mcq-question-type<?php } else { ?>tbs-question-type<?php }  ?>">
                <div class="ipq-content-wrapper <?php if($disabled_question) print 'disabled'; ?>">

                    <div id="ipq-content">
                      <?php if($navigation_info['deleted_question']): ?>
                          <div class="deleted-question-info">
                              <img src="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/img/icon_ATTENTION.svg" height="33" width="39" />
                              <div class="ipq-disabled-question-message">
                                <?php print check_markup(variable_get('deleted_question_message', 'This question has been removed from this test'), 'full_html'); ?>
                              </div>
                              <button name="next_question" value="Next" class="btn disabled-question-next">Next Question</button>
                          </div>
                      <?php endif; ?>
                        <div class="scrollable-area">
                            <h1 class="question-number-heading <?php if (!isset($form['actions']['back'])) { ?> first-question-wrapper<?php } ?> <?php if ($navigation_info['last_in_question_set']) { ?> last-question-wrapper<?php } ?>">
                                <div class="question-previous"><?php if (isset($form['actions']['back'])) { print drupal_render($form['actions']['back']); } else { ?><div class="disabled-previous"></div><?php } ?></div>
                                <div class="question-number" aria-label="Quiz question number <?php print $navigation_info['current_question_index'] + 1; ?>"><?php print $navigation_info['current_question_index'] + 1; ?></div>
                              <?php
                              $current_qid = $navigation_info['question_id'];
                              $flagged = (is_object($navigation_info['current_type_questions_information'][$current_qid]) && $navigation_info['current_type_questions_information'][$current_qid] -> is_flagged) ? "flagged" : "";
                              ?>
                                <div class="question-bookmark"><button tabindex="0" href="#" role="button" class="bookmark-ipq-flag btn-<?php print $navigation_info['current_question_index']+1 . ' ' . $flagged; ?>" qid="<?php echo $current_qid ?>" session="<?php echo $navigation_info['session_id']?>">Bookmark this question</button></div>
                                <div class="question-next"><?php if ($navigation_info['last_in_question_set']) { ?><div class="disabled-next"></div><?php } else { print drupal_render($form['actions']['next']); } ?></div>
                            </h1>


                          <?php
                          // Question display, courseware links, and notes
                          print drupal_render($form['question_display']);
                          print drupal_render($form['courseware_links']);
                          print drupal_render($form['notes']);
                          ?>


                            <!-- TBS Numeric Entry Modal -->
                          <?php
                          $m = new RCPARModal('tbs-entry-popup','tbs-popup',array('data-backdrop' => 'false',));

                          $entry_body = '<div class="entry-container">';
                          $entry_body .= '<div class="entry-img"><img src="/' . drupal_get_path('module', 'ipq_common') . '/css/img/clipboard.png" height="28" width="27" /></div>';
                          $entry_body .= '<div class="entry-input"><input class="tbs-entry-input" type="text" maxlength="15"></div>';
                          $entry_body .= '<div class="entry-formatted">';
                          $entry_body .= '<div class="entry-formatted-heading">Formatted response</div>';
                          $entry_body .= '<div class="entry-formatted-entry"></div>';
                          $entry_body .= '</div>';
                          $entry_body .= '</div>';
                          $m->setBodyContent($entry_body) // jquery requires an empty body div
                          ->setFooterContent('<div class="btn-wrapper"><div class="btn-inner">
                        <button class="tbs-entry-reset-btn btn disabled" type="button">Reset</button>
                        <button class="tbs-entry-confirm-btn btn" type="button">Confirm</button>
                        <button class="tbs-entry-ok-btn btn disabled" data-dismiss="modal" aria-label="Close" type="button">Accept</button>
                        <button class="tbs-entry-cancel-btn btn" type="button">Cancel</button>
                      </div></div>');
                          print $m->render();
                          ?>
                            <!-- / End TBS Numeric Entry Modal -->

                            <div class="clearfix copyright"> Copyright © 2013-<?php print date('Y'); ?> Roger CPA Review. All rights reserved.</div>
                        </div>
                        <!-- end scrollable area -->
                        <div class="previous-next-wrapper">
                            <div class="question-previous"><?php if (isset($form['actions']['back']) && count($navigation_info['current_type_question_list']) != 1) {  ?><a class="pseudo-question-previous" href="#" aria-label="Previous Question"></a> <?php } else { ?><div class="disabled-previous"></div><?php } ?></div>
                            <div class="question-next"><?php if ($navigation_info['last_in_question_set']) { ?><div class="disabled-next"></div><?php } else { ?><a class="pseudo-question-next" href="#" aria-label="Next Question"></a><?php } ?></div>
                        </div>


                    </div><!-- #ipq-content -->
                </div><!-- ipq-content-wrapper -->
            </div><!-- /ipq-container question-wrapper-->
        </div><!-- /ipq-wrapper-->

        <div class="submit-testlet tool-menu-item">
            <a href="#" class="submit-test-link" id="submit-test-link" tabindex="0"><?php print ($session_type=='quiz' ? 'Submit Quizlet' : 'Submit Testlet' ); ?></a>
        </div>
        <!-- Question Content -->

    </div><!-- /.ipq-wrapper-->
    </div><!-- /.ipq-question-wrapper-->

    <!-- Tools Modal -->
<?php
$m = new RCPARModal('ipq-tools','ipq-popup',array('data-backdrop' => 'false','role'=>'dialog','aria-label' => 'Quiz Tools popup', 'tabindex' => '-1',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-label="Close Popup">x</button><h3 id="termsLabel" class="modal-title"></h3>')
  ->setBodyContent('&nbsp;'); // jquery needs an empty div
print $m->render();
?>
    <!-- / End Tools Modal -->

    <!-- Screen Resolution Modal -->
<?php

$resolution_footer = '<div class="resolution-popup-control pull-left"><input id="resolution-popup-control" type="checkbox" name="resolution-popup-control">';
$resolution_footer .= '<label for="resolution-popup-control">Don\'t show this again</label></div>';
$resolution_footer .= '<button type="button" class="ipq-resolution-ok-btn btn btn btn-success pull-right" data-dismiss="modal" aria-label="Close Popup">OK, Got It</button>';

$m = new RCPARModal('ipq-resolution-popup','ipq-popup',array('data-backdrop' => 'false','role'=>'dialog','aria-label' => 'Screen Resolution popup', 'tabindex' => '-1',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-label="Close Popup">x</button><h3 id="termsLabel" class="modal-title">Your screen resolution is below the recommended minimum</h3>')
  ->setBodyContent('<p>For best results, use a screen resolution (the number of pixels displayed on your screen) of at least 1366 x 768. The layout is usable at smaller resolutions, but may not be optimal.</p><p>To better emulate the CPA Exam testing experience at Prometric Testing Centers, use a screen resolution of 1920 x 1080.</p>')
  ->setFooterContent($resolution_footer);
print $m->render();
?>
    <!-- / End Screen Resolution Modal -->

    <!-- Skipped Questions Modal -->
<?php
$m = new RCPARModal('ipq-skipped-modal','ipq-popup',array('data-backdrop' => 'false',));
$skipped_body = '<div class="skipped-questions">Navigate to: ';
$skipped_links = '';
$i = 1;
$question_set_name = 'Quizlet';
if ($navigation_info['session_type'] == 'exam') {
  $question_set_name = 'Testlet';
}
foreach($navigation_info['current_type_question_list'] as $q):
  // Don't show the current question in the skipped list. If they're on that question, they know what they've done.
  // This matches the logic in ipq_common_quiz_display() that determines the skipped question count. It also
  // conveniently avoids the issue of DEV-455.
  if($q == $current_qid) {
    $i++; // Still need to increment counter so the right question number is shown
    continue;
  }

  $extra_class = '';
  // If there's a 2 on $navigation_info['testlet_questions_status'][$q]
  // then the user didn't tried to answer this question yet
  if ($navigation_info['testlet_questions_status'][$q] == IPQ_Q_STATUS_UNANSWERED) {
    $ni=$i-1;
    $skipped_links .= '<a 
      data-qid="' . $q .'" href="" 
      class="pager-link ' . $extra_class .' " 
      title="<p>' . str_ireplace('"', '', $navigation_info['tooltips'][$ni]) . '</p>" 
      data-html="true" data-placement="top" data-trigger="hover" data-dismiss="modal">' . $i . '</a>' . PHP_EOL;
  }
  $i++;
endforeach;
$skipped_body .= $skipped_links .'</div>';
$skipped_body .= '<div class="skipped-instructions">Click Submit '.$question_set_name.' to confirm your submission, or click on a question number above to complete your work. You will not be able to return to this testlet after you click Submit '.$question_set_name.'.</div>';

$skipped_footer = '
  <div class="btn-wrapper">
    <div class="btn-inner">
       <button class="ipq-skipped-ok-btn btn " data-dismiss="modal" aria-label="Close" type="button">Submit '.$question_set_name.'</button>
    </div>
  </div>';

$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h3 id="termsLabel" class="modal-title">You have skipped one or more questions on this '.$question_set_name.'</h3>')
  ->setBodyContent($skipped_body)
  ->setFooterContent($skipped_footer);
print $m->render();
?>
    <!-- / End Skipped Questions Modal -->


    <!-- Overview Modal -->
<?php
$m = new RCPARModal('ipq-overview-modal','ipq-popup',array('data-backdrop' => 'true','role'=>'dialog','aria-label' => 'Quiz overview popup', 'tabindex' => '-1',));
$overview_header = '<h3 id="termsLabel" class="modal-title" >'.$question_set_name.' ';
$overview_header .= $navigation_info['testlet_index'] + 1;
$overview_header .= ' Overview</h3><button type="button" class="close" data-dismiss="modal" aria-label="Close Overview Modal">x</button>';
$overview_body = '<div class="to-header">
        <div class="column-left">Question</div>
        <div class="column-divider"></div>
        <div class="column-right">**Select a question number to visit that item</div>
      </div>';
$i = 1;
foreach($navigation_info['current_type_question_list'] as $q):

  $flagged = (is_object($navigation_info['current_type_questions_information'][$q]) && $navigation_info['current_type_questions_information'][$q] -> is_flagged) ? " flagged" : "";
  $extra_class = '';
  if ($q == $navigation_info['question_id']) {
    $extra_class .= ' current';
  }
  // If there's a 2 on $navigation_info['testlet_questions_status'][$q]
  // then the user didn't tried to answer this question yet
  if ($navigation_info['testlet_questions_status'][$q] != IPQ_Q_STATUS_UNANSWERED) {
    // otherwise the user did tried
    $extra_class .= ' tried';
  }
  $ni=$i-1;
  $overview_body .= '<div class="to-item ' . $extra_class .'">';
  $overview_body .= '<div class="column-left" aria-label="Question number and bookmark icon"><a data-qid="'  . $q . '" href="" class="pager-link '  . $extra_class . '">' . $i . '</a>' . PHP_EOL;
  $overview_body .= '<a href="#" class="bookmark-ipq-flag btn-' . $i . $extra_class . $flagged . '" qid="' . $navigation_info['current_type_question_list'][$i-1] . '" session="' .  $navigation_info['session_id'] . '">Bookmark this question</a>' . PHP_EOL;
  $overview_body .= '</div>';
  $overview_body .= '<div class="column-divider"></div>';
  $overview_body .= '<div class="column-right ellipsis-overflow" aria-label="Truncated question text">';
  $ni=$i-1;
  $overview_body .= str_ireplace('"', '', $navigation_info['question_titles'][$ni]);
  $overview_body .= '</div></div>';
  $i++;
endforeach;

$m->setHeaderContent($overview_header)
  ->setBodyContent($overview_body);
print $m->render();
?>
    <!-- / End Overview Modal -->

    <!-- Help Modal -->
<?php
module_load_include('inc', 'ipq_common', 'includes/ipq.help');
$m = new RCPARModal('ipq-help-modal', 'ipq-popup', array('data-backdrop' => 'true', 'role' => 'dialog', 'aria-label' => 'Help', 'tabindex' => '-1'));
$m->setHeaderContent('<h3 class="modal-title">Help</h3><button type="button" class="close" data-dismiss="modal" aria-label="Close Overview Modal">x</button>');
$m->setBodyContent(ipq_common_help($navigation_info['session_type']));
print $m->render();

?>
    <!--/ End Help Modal -->

    <!-- Authoritative Literature Modal -->
<?php
$m = new RCPARModal('ipq-auth-lit','ipq-popup auth-lit-popup',array('data-backdrop' => 'true','role'=>'dialog','aria-label' => 'Authoritative Literature popup', 'tabindex' => '-1',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-label="Close Authoratative Literature Popup">x</button><h3 id="authLabel" class="modal-title">Authoritative Literature</h3>')
  ->setBodyContent($navigation_info['authoritative_literature']);
print $m->render();
?>
    <!-- / End Authoritative Literature Modal -->

    <!-- TBS Answer Selection Modal -->
<?php
$m = new RCPARModal('tbs-answer-popup','ipq-popup',array('data-backdrop' => 'false',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h3 id="termsLabel" class="modal-title"></h3>')
  ->setBodyContent('&nbsp;') // jquery requires an empty body div
  ->setFooterContent('<div class="btn-wrapper"><div class="btn-inner">
                        <button class="hot-ipq-reset-btn btn disabled" type="button">Reset</button>
                        <button class="hot-ipq-confirm-btn btn" type="button">Confirm Reset</button>
                        <button class="hot-ipq-cancel-btn btn" type="button">Cancel</button>
                        <button class="hot-ipq-ok-btn btn disabled" data-dismiss="modal" aria-label="Close" type="button">Accept</button>
                      </div></div>');
print $m->render();
?>
    <!-- / End TBS Answer Selection Modal -->

    <!-- Exam Time Limit Modal -->

<?php
$m = new RCPARModal('time-limit-reached','',array('data-backdrop' => 'true',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h3 id="termsLabel" class="modal-title"></h3>')
  ->setBodyContent('You have run out of time to complete the exam!')
  ->setFooterContent('<button type="button" class="btn dismiss" data-dismiss="modal" aria-hidden="true">OK</button>');
print $m->render();
?>
    <!-- / End Exam Time Limit Modal -->

    <!-- iPad Spreadsheet Text Modal -->
<?php
$m = new RCPARModal('ipad-text-input-popup','',[]);
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h3 id="termsLabel" class="modal-title">Edit Cell Text</h3>')
  ->setBodyContent('<input type="text" class="handsontable-ipad-input" placeholder="Type here, then tap Go or Enter" />');
print $m->render();
?>
    <!-- / End iPad Spreadsheet Text Modal -->

    <!-- Disabled Question Modal -->
<?php if($disabled_question):
  $m = new RCPARModal('ipq-disabled-question','fade',array('data-backdrop' => 'static','role' => 'dialog'));
  // Admins should be able to close this modal.
  if ($is_admin) {
    $m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>');
  }
  $disabled_body = '<div class="ipq-disabled-question-message">';
  $disabled_body .= '<img src="/' . drupal_get_path('module', 'ipq_common') . '/css/img/icon_ATTENTION.svg" height="33" width="39" class="disabled-icon" />';
  $disabled_body .= check_markup(variable_get('disabled_question_message', 'This question has been disabled'), 'full_html');
  $disabled_body .= '</div>';
  $m->setBodyContent($disabled_body)
    ->setFooterContent('<button type="button" class="btn disabled-question-next">Next Question</button>');
  print $m->render();
endif;
?>

<?php if($navigation_info['question_status'] == 0 && user_access('ipq_view_unpublished_questions')):
  $m = new RCPARModal('ipq-disabled-question','fade',array('data-backdrop' => 'static','role' => 'dialog'));
  $disabled_body = '<div class="ipq-disabled-question-message">';
  $disabled_body .= '<img src="/' . drupal_get_path('module', 'ipq_common') . '/css/img/icon_ATTENTION.svg" height="33" width="39" class="disabled-icon" />';
  $disabled_body .= check_markup(variable_get('disabled_question_message', 'This question has been disabled'), 'full_html');
  $disabled_body .= '</div>';
  $m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>');
  $m->setBodyContent($disabled_body)
    ->setFooterContent('<button type="button" class="btn disabled-question-next">Next Question</button>');
  print $m->render();
endif;
?>
    <!-- / END Disabled Question Modal -->


    <!-- IPQ Score Info Modal -->
<?php
$m = new RCPARModal('ipq-score-information','',array('data-backdrop' => 'false',));

$score_info_body = drupal_render($form['score_as_you_go_info']);
$score_info_body .= drupal_render($form['score_as_you_go_info_submit']);

$score_info_footer = '<div class="score-control pull-left"><input id="score-as-you-go-cb" type="checkbox" name="vehicle" value="1" ';
$score_info_footer .= $navigation_info['score_as_you_go'] ? 'checked' : '';
$score_info_footer .= '><label for="score-as-you-go-cb">Score as I go</label></div>';
$score_info_footer .= '<button type="button" class="try-again-btn btn btn-default">Try Again</button>';
$score_info_footer .= '<button type="button" class="continue-btn btn btn-success pull-right">' . $btn_label . '</button>';

$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h3 id="termsLabel" class="modal-title">Your Score</h3>')
  ->setBodyContent($score_info_body)
  ->setFooterContent($score_info_footer);

print $m->render();

?>
    <!-- / END IPQ Score Info Modal -->

    <!-- IPQ Emulate Score Info Modal -->
<?php
$m = new RCPARModal('ipq-emulate-score-information','',array('data-backdrop' => 'false',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h3 id="termsLabel" class="modal-title">(Emulated) Your Score </h3>')
  ->setBodyContent(drupal_render($form['emulate_results']))
  ->setFooterContent('<button type="button" data-dismiss="modal" class="btn btn-success pull-right">Close</button>');
print $m->render();
?>
    <!-- / END IPQ Emulate Score Info Modal -->

    <!-- Exam break timer modal
    This is a special case-->
    <div id="ipq-break-timer-popup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">

        <!-- STEP 1 -->
      <?php
      $m = new RCPARModal('break-step-1','break-step',[]);
      $m->setHeaderContent('<h3 class="modal-title">OPTIONAL BREAK</h3>')
        ->setBodyContent('
        <div class="exam-info clearfix"><div class="exam-info-icon"><img src="/' . drupal_get_path('module', 'ipq_common') . '/css/img/icon-exam-info.svg" width="45" height="45" /></div><div class="exam-info-copy">The text below will be displayed when you\'re offered a break during the actual CPA Examination. A launch code will not be required during this simulation.</div></div>
        <div class="text">
          <p><strong>Click Take a Break to pause the exam timer.</strong></p>
          <p><strong>For this break only, the exam timer will pause for up to 15 minutes.</strong></p>
          <ul>
            <li>If you choose to take a break, you must leave the testing room.</li>
            <li>Your 7-digit exam launch code will be required to continue the exam.</li>
          </ul>
          <p>OR</p>
          <p>Click Continue Exam to proceed to the next testlet.</p>
        </div>')
        ->setFooterContent('<button type="button" data-dismiss="modal" class="btn pull-left next-step">Take a Break</button>
        <button type="button" data-dismiss="modal" class="btn pull-right finish-break">Continue Exam</button>');
      print $m->render();
      ?>

        <!-- STEP 2 -->
      <?php
      $m = new RCPARModal('break-step-2','break-step',[]);
      $m->setHeaderContent('<h3 class="modal-title">YOU ARE ON A BREAK</h3>')
        ->setBodyContent('
        <div class="exam-info clearfix"><div class="exam-info-icon"><img src="/' . drupal_get_path('module', 'ipq_common') . '/css/img/icon-exam-info.svg" width="45" height="45" /></div><div class="exam-info-copy">The text below will be displayed when you\'re offered a break during the actual CPA Examination. A launch code will not be required during this simulation.</div></div>
        <div class="text">
          <p>The exam timer is paused.</p>
          <ul>
          <li>You must leave the testing room.</li>
          <li>The Test Center Administrator will require you to sign out at the desk.</li>
          <li>You must present your ID upon return to the testing room.</li>
          <li>Your 7-digit exam launch code is required to continue the exam.</li>
</ul>
        </div>')
        ->setFooterContent('
          <div class="timer-text">Timer resumes in:</div>
          <div class="timer-time"><div class="remaining-time"><span class="minutes-remaining">15</span>:<span class="seconds-remaining">00</span></div></div><button type="button" data-dismiss="modal" class="btn btn-success pull-right finish-break">Continue Exam</button>');
      print $m->render();
      ?>

        <!-- STEP 3 -->
      <?php
      $m = new RCPARModal('break-step-3','break-step',[]);
      $m->setHeaderContent('<h3 class="modal-title">Your break time has expired.</h3>')
        ->setBodyContent('<div class="text">The exam timer has resumed.</div>')
        ->setFooterContent('<button type="button" data-dismiss="modal" class="btn btn-success pull-right finish-break">Continue Exam</button>');
      print $m->render();
      ?>

    </div><!-- /.modal -->

    <!-- / END Exam break timer modal -->



    <!-- Non-paused Exam break timer modal
    This is a special case-->
    <div id="ipq-non-paused-break-timer-popup" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog">

        <!-- STEP 1 -->
      <?php
      $m = new RCPARModal('break-step-1','break-step',[]);
      $m->setHeaderContent('<h3 class="modal-title"> OPTIONAL BREAK</h3>')
        ->setBodyContent('
        <div class="exam-info clearfix"><div class="exam-info-icon"><img src="/' . drupal_get_path('module', 'ipq_common') . '/css/img/icon-exam-info.svg" width="45" height="45" /></div><div class="exam-info-copy">The text below will be displayed when you\'re offered a break during the actual CPA Examination. A launch code will not be required during this simulation.</div></div>
        <div class="text">
          <p><strong>To leave the room, click Take a Break.</strong></p>
          <p><strong>If you choose to take a break, the exam timer will continue to run.</strong></p>
          <p>Your 7-digit exam launch code will be required to continue the exam.</p>
          <p>OR</p>
          <p>Click Continue Exam to proceed to the next testlet.</p>
        </div>')
        ->setFooterContent('<button type="button" data-dismiss="modal" class="btn pull-left next-step">Take a Break</button>
        <button type="button" data-dismiss="modal" class="btn pull-right finish-break">Continue Exam</button>');
      print $m->render();
      ?>

        <!-- STEP 2 -->
      <?php
      $m = new RCPARModal('break-step-2','break-step',[]);
      $m->setHeaderContent('<h3 class="modal-title">YOU ARE ON A BREAK</h3>')
        ->setBodyContent('
        <div class="exam-info clearfix"><div class="exam-info-icon"><img src="/' . drupal_get_path('module', 'ipq_common') . '/css/img/icon-exam-info.svg" width="45" height="45" /></div><div class="exam-info-copy">The text below will be displayed when you\'re offered a break during the actual CPA Examination. A launch code will not be required during this simulation.</div></div>
        <div class="text">
          <p><strong>The exam timer continues to run.</strong></p>
          <ul>
          <li>You must leave the testing room.</li>
          <li>The Test Center Administrator will require you to sign out at the desk.</li>
          <li>You must present your ID upon return to the testing room.</li>
          <li>Your 7-digit exam launch code is required to continue the exam.</li>
</ul>
        </div>')
        ->setFooterContent('<button type="button" data-dismiss="modal" class="btn btn-success pull-right finish-break">Continue Exam</button>');
      print $m->render();
      ?>

    </div><!-- /.modal -->

    <!-- / END Exam break timer modal -->


    <!-- Exam Excel Modal -->
<?php
$blogUrl = url('blog/how-microsoft-excel-will-function-cpa-exam');
$excelFooter = '<div class="excel-footer pull-left align-left">';
if ($navigation_info['session_type'] == 'quiz') {
  $excelFooter .= '<div><a href="/ipq-tools/spreadsheet" data-toggle="#ipq-tools" data-target="#ipq-tools" class="ipq-tools-modal" id="open-spreadsheet-link" ipqtitle="Spreadsheet Tool">Use our web-based spreadsheet tool instead</a></div>';
  $excelFooter .= '<div class="footer-footnote">This will launch our simple spreadsheet tool for use if you\'re unable to access Excel.</div>';
}
$excelFooter .= '</div><button type="button" data-dismiss="modal" class="btn btn-success pull-right">OK, got it</button>';
$m = new RCPARModal('ipq-open-excel','',array('data-backdrop' => 'true','role'=>'dialog','aria-label' => 'Excel popup', 'tabindex' => '-1',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal"  aria-label="Close Excel Popup">x</button><h3 id="termsLabel" class="modal-title">Please use Excel on your computer</h3>')
  ->setBodyContent('<p>When completing the CPA Exam, if you click on the Excel icon, Microsoft Excel® will open.</p>
<p>If you would like to use Excel while completing the sample test you are able to use your own desktop version.</p>
<p>During the Exam, you will be able to use Excel’s full formula, sort, and filter capabilities, but for security purposes, certain Excel functions are unavailable. If Excel is open, it will always appear above any other open resources, exhibits or tools.</p>
<p>Any time you close Excel, or navigate to a new question, the work you have entered in the Excel file will automatically be saved. At any point you will also be able to save your work by clicking the save button in Excel. Work in Excel will only be saved in a testlet - once you navigate to the next testlet, you will start with a new Excel workbook.</p>
<p>If you’d like to review how Excel functions in the Exam, <a href="' . $blogUrl .'">see this article</a> for more detailed information.</p>
')
  ->setFooterContent($excelFooter);
print $m->render();
?>
    <!-- / END Exam Excel Modal -->

<!-- Confirm No Entry Required Modal -->
<?php
$m = new RCPARModal('no-entry-popup','ipq-popup',array('data-backdrop' => 'false','role'=>'dialog','aria-label' => 'Confirm No Entry Required popup', 'tabindex' => '-1',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-label="Close Popup">x</button><h3 id="termsLabel" class="modal-title">No Entry Required</h3>')
  ->setBodyContent('<p>The Journal Entry contains responses.</p> <p>Select Confirm to clear all responses. Otherwise, select cancel.</p> ')
  ->setFooterContent('<button type="button" data-dismiss="modal" class="btn btn-success btn-confirm-no-entry pull-right">Confirm</button><button type="button" data-dismiss="modal" class="btn btn-success btn-cancel-no-entry pull-left">Cancel</button>');
print $m->render(); // jquery needs an empty div
print $m->render();
?>
<!-- Confirm No Entry Required Modal -->

<!-- Show Correct Answer Modal -->
<?php

$showcorrectanswer_footer = '<div class="show-answer-popup-control pull-left"><input id="show-answer-popup-control" type="checkbox" name="show-answer-popup-control">';
$showcorrectanswer_footer .= '<label for="show-answer-popup-control">Don\'t show this again</label></div>';
$showcorrectanswer_footer .= '<button type="button" data-dismiss="modal" class="btn btn-success btn-confirm-show-answer pull-right">Yes</button>';
$showcorrectanswer_footer .= '<button type="button" data-dismiss="modal" class="btn btn-success btn-cancel-show-answer pull-right">Cancel</button>';

$m = new RCPARModal('show-correct-answer','ipq-popup',array('data-backdrop' => 'static','role'=>'dialog','aria-label' => 'Confirm No Entry Required popup', 'tabindex' => '-1',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-label="Close Popup">x</button><h3 id="termsLabel" class="modal-title">Using the \'Show correct answer\' feature</h3>')
  ->setBodyContent('<p>Activating the \'Show correct answer\' feature will cause your answer to become locked for this question. Upon selecting the operation, the following information will be displayed for this question only:</p>
<ul>
<li>The percentage of users who chose each answer option.</li>
<li>The status of your selected answer, and the correct answer if you chose an incorrect option.</li>
<li>The question explanation.</li>
<li>The amount of time you spent answering the question.</li>
</ul>
<p>Are you sure you want to activate the feature for this question?</p>')
  ->setFooterContent($showcorrectanswer_footer);
print $m->render();
?>
<!-- Show Correct Answer Modal -->

<?php print drupal_render_children($form); ?>

