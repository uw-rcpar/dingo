<?php
	$session = $data['session'];
?>
<div class="quiz-results-wrapper">
<div class="ipq-container quiz-results">
	<?php
  $chapters_count = count($data['session']['session_config']['chapters']);
	?>
	<h2 class="session-name">
	<!-- edit session name -->
	<div class="session-name">
	<div class="col-md-12 row editing-session-name">
		<span>
			<?php print drupal_render($data['form']); ?> 
		</span>
	</div><!-- /editing-session-name -->
	</div><!-- /session-name -->
		
	<div class="pull-right"><a href="<?php print url('ipq/review'); ?>" action="review" sess="<?php print $data['session']['session_type']; ?>" sid="<?php print $data['session']['id']; ?>"  class="btn btn-primary btn-sm review-sess-btn" role="button"> Review <span class="reader-instructions">this quiz</span></a></div></h2>
    <?php if (strpos($data['session']['name'], 'Smart') !== FALSE) { ?>
      <div class="quiz-sections">This SmartQuiz covers <?php print format_plural($chapters_count, '1 section', '@count sections'); ?> containing:</div>
    <?php }
    else {
    ?>
      <div class="quiz-sections">This <?php if ($data['session']['session_type'] == 'quiz') { ?>Quiz<?php } else {?>Exam<?php } ?> covers <?php print format_plural($chapters_count, '1 section', '@count sections'); ?> containing:</div>
    <?php } ?>
 <?php print $data['summary'] ?>
	
	<div class="quiz-section-includes">Includes: <strong><?php print $data['section']; ?>:</strong> <?php print $data['section_numbers']; ?></div>
	<div class="result-percent-correct">
		<?php print $data['session']['percent_correct']; ?>% Correct
	</div>
	<?php $first = true; ?>
	<?php foreach($data['question_info'] as $q_type => $q_type_data): ?>
		<div class="result-wrapper <?php if($first) {print 'first'; $first = false; } ?>">
			<h3 class="results-heading"><?php print $q_type_data['name']; ?></h3>
			<table class="table table-quiz-results">
			 <tbody>
				<tr>
				<td <?php if ($q_type_data['skipped'] >0): ?>class="has-items"<?php endif; ?> >
					<div class="icon-question"><img src="<?php print base_path() . drupal_get_path('module', 'ipq_common'); ?>/css/img/icon-unanswered.png" alt="Unanswered icon" /></div>
					<div class="results"><?php print $q_type_data['skipped']; ?> Unanswered</div>
				</td>
				<td <?php if ($q_type_data['correct'] >0): ?>class="has-items"<?php endif; ?>>
					<div class="icon-checkmark"><img src="<?php print base_path() . drupal_get_path('module', 'ipq_common'); ?>/css/img/icon-correct.png" alt="Correct icon" /></div>
					<div class="results"><?php print $q_type_data['correct']; ?> Correct</div>
				</td>
				<td <?php if ($q_type_data['incorrect'] >0): ?>class="has-items"<?php endif; ?>>
					<div class="icon-x"><img src="<?php print base_path() . drupal_get_path('module', 'ipq_common'); ?>/css/img/icon-incorrect.png" alt="Incorrect icon" /></div>
					<div class="results"><?php print $q_type_data['incorrect']; ?> Incorrect</div>
				</td>
				<td <?php if ($q_type_data['flagged'] >0): ?>class="has-items"<?php endif; ?>>
					<div class="icon-flag"><img src="<?php print base_path() . drupal_get_path('module', 'ipq_common'); ?>/css/img/icon-bookmark.png" alt="Bookmarked icon" /></div>
					<div class="results"><?php print $q_type_data['flagged']; ?> Bookmarked</div>
				</td>
				<td <?php if ($q_type_data['notes'] >0): ?>class="has-items"<?php endif; ?>>
					<div class="icon-note"><img src="<?php print base_path() . drupal_get_path('module', 'ipq_common'); ?>/css/img/icon-noted.png" alt="Noted icon" /></div>
					<div class="results"><?php print $q_type_data['notes']; ?> Noted</div>
				</td>
				</tr>
				</tbody>
			</table>
		</div>
	<?php endforeach; ?>
</div><!-- /ipq-container -->

<div class="ipq-container section-breakdown">

<h2 class="section-breakdown-header">Section Breakdown</h2>

<?php foreach($data['overview'] as $ov): ?>
<div class="row">
<div class="quiz-chapter col-md-6 col-sm-12">
<div class="chapter-inner-wrapper">
	<div class="chapter-info">
	<div class="chapter-info-text">
		<div class="chapter-number"><?php print $data['section']; ?> <?php print $ov['chapter_number']; ?></div>
		<div class="chapter-name"><?php print $ov['chapter_title']; ?></div>
	</div><!-- /chapter-info-text -->
	</div><!-- /chapter-info -->
	<?php

	$attempted = $ov['total_correct'] + $ov['total_incorrect'];
	$available = 0;
	foreach($ov['question_counts'] as $q_type => $q_type_count){
		$available += $q_type_count['total'];
	}
?>
<div class="question-scores">
<div class="question-score-bar">	
	<div data-toggle="tooltip"  data-placement="top" title="Incorrect Questions" class="double-rounded-corners incorrect-questions" style="width: 100%; <?php if ($attempted == 0) { print 'background-color: #ccc;'; } ?>">
		<span class="numbers pull-right"><?php if ($ov['total_incorrect'] > 0 && $attempted != 0) { print $attempted - $ov['total_correct']; }  ?><?php if ($attempted == 0) { print '0'; }  ?></span>
	</div><!-- /double-rounded-corners -->
	<div data-toggle="tooltip"  data-placement="top" title="Correct Questions" class="correct-questions double-rounded-corners" style="width: <?php if ($attempted != 0) { print 100 * ($ov['total_correct'] / $attempted); } else { print '0'; } ?>%;">
		<span class="numbers pull-right"><?php if ($ov['total_correct'] > 0) { print $ov['total_correct']; } ?></span>
	</div><!-- /double-rounded-corners -->
</div><!-- /question-score-bar -->

	<div class="questions-attempted"><?php print $attempted; ?> of <?php print $available; ?> Questions Attempted</div>
			
<table class="uppercase table table-bordered table-striped question-categories text-center">
	<thead>
	<tr>
		<th class="lbl">Type</th>
		<th><span data-toggle="tooltip"  data-placement="top" title="Unanswered Questions" class="sprite-question"></span></th>
		<th><span data-toggle="tooltip"  data-placement="top" title="Correct Questions" class="sprite-checkmark"></span></th>
		<th><span data-toggle="tooltip"  data-placement="top" title="Incorrect Questions" class="sprite-x"></span></th>
		<th><span data-toggle="tooltip"  data-placement="top" title="Bookmarked Questions" class="sprite-flag"></span></th>
		<th><span data-toggle="tooltip"  data-placement="top" title="Questions with Notes" class="sprite-note"></span></th>
	</tr>
	</thead>
	<tbody>

	<?php $pair = false; ?>
	<?php foreach($ov['question_counts'] as $q_type => $q_type_count): ?>
		<tr>
			<td class="<?php if ($pair) print 'td-white'; $pair = !$pair; ?> lbl">
				<?php print(str_ireplace('Questions', '', $data['question_info'][$q_type]['name'])); ?> <div class="total-number-questions"><?php print $q_type_count['total']; ?> Questions</div>
			</td>
			<td class=""><?php print $q_type_count['unanswered']; ?></td>
			<td><a ><?php print $q_type_count['correct']; ?></a></td>
			<td><a ><?php print $q_type_count['incorrect']; ?></a></td>
			<td><a ><?php print $q_type_count['flagged']; ?></a></td>
			<td><a ><?php print $q_type_count['noted']; ?></a></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
</div><!-- /question-scores -->
</div><!-- /chapter-inner-wrapper -->
</div><!-- /quiz-chapter -->
<?php endforeach; ?>		

</div><!-- /row -->
</div><!-- /ipq-container -->
</div>