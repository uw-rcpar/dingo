<!doctype html>
<html>
	<head>
		<title>Calculator</title>

		<link rel="stylesheet" href="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/bootstrap.min.css">
		<link rel="stylesheet" href="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/bootstrap-responsive.min.css">
		<link rel="stylesheet" href="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/application.css">
		<script src="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/js/jquery-1.9.1.js"></script>
		<script src="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/js/calculator.js"></script>
		<style type="text/css">
			body {
				padding: 0;
				width: 655px;
				height: 100%;
				overflow: hidden; 
			}
		</style>
	</head>
	<body>

		<div class="container" style="margin:0">

			<div class="jumbotron" id="calculator-wrapper">
				<div class="row">
					<div class="col-sm-11 col-md-11">
						<div id="calculator-screen" class="uneditable-input" style="display: none"></div>
						<div id="calculator-result" class="uneditable-input">
							0
						</div>
					</div>

				</div>

			</div>

			<div class="row">

				<div class="well col-sm-12 col-md-12">
					<div id="calc-board">

						<div class="row">
							<a href="#" class="btn btn-default" id="backspace" style="width: 21%" data-method="backspace">Backspace</a>
							<a href="#" class="btn btn-default" data-method="reset_display">CM</a>
							<a href="#" class="btn btn-danger" data-method="reset">C</a>
						</div>

						<br>
						<br>
						<div class="row">
							<a href="#" class="btn btn-default" data-method="memeory_clear">MC</a>
							<a href="#" id="7" class="btn btn-default">7</a>
							<a href="#" id="8" class="btn btn-default">8</a>
							<a href="#" id="9" class="btn btn-default">9</a>
							<a href="#" id="divide" class="btn btn-default" data-constant="DIV">/</a>
							<a href="#" class="btn btn-default" data-constant="SQR"><b>√</b></a>
						</div>
						<div class="row">
							<a href="#" class="btn btn-default" data-method="memeory_recall">MR</a>
							<a href="#" id="4" class="btn btn-default">4</a>
							<a href="#" id="5" class="btn btn-default">5</a>
							<a href="#" id="6" class="btn btn-default">6</a>
							<a href="#" id="prod" class="btn btn-default" data-constant="MULT">*</a>
							<a href="#" class="btn btn-default" data-constant="PROC">%</a>

						</div>
						<div class="row">
							<a href="#" class="btn btn-default" data-method="memeory_store">MS</a>
							<a href="#" id="1" class="btn btn-default">1</a>
							<a href="#" id="2" class="btn btn-default">2</a>
							<a href="#" id="3" class="btn btn-default">3</a>
							<a href="#" id="minus" class="btn btn-default" data-constant="MIN">-</a>
							<a href="#" class="btn btn-default" data-method="one_over_x">1/X</a>

						</div>
						<div class="row">
							<a href="#" class="btn btn-default" data-method="clipboard"><span class="fa fa-file"></span></a>

							<a href="#" id="0" class="btn btn-default" style="width: 21%">0</a>
							<a href="#" id="point" class="btn btn-default">.</a>

							<a href="#" id="plus" class="btn btn-default" data-constant="SUM">+</a>
							<a href="#" id="equals" class="btn btn-primary" data-method="calculate">=</a>
						</div>
					</div>
				</div>

			</div>

		</div>

		<div class="container tape">
			<div class="well">
				<legend>
					Tape
				</legend>
				<div id="calc-panel">
					<div id="calc-history">
						<ol id="calc-history-list"></ol>
					</div>
				</div>
			</div>
		</div>

	</body>
</html>
