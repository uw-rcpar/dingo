<?php
  $session = $form['session']['#value'];
  $question = $form['question']['#value'];
  $navigation_info = $form['navigation_info']['#value'];

  //$tid = 
  $term = taxonomy_term_load($question->field_ipq_course_section['und']['0']['tid']);
  $section_name = $term->name;
  if ($question->type == 'ipq_tbs_question' ||  $question->type == 'ipq_tbs_form_question' || $question->type == 'ipq_tbs_journal_question') {
    $course_section = 'Task Based Simulation';
  }
  if ($question->type == 'ipq_drs_question') {
    $course_section = 'Document Review Simulation';
  }
  if ($question->type == 'ipq_mcq_question') {
    $course_section = 'Multiple Choice';
  }
  if ($question->type == 'ipq_wc_question') {
    $course_section = 'Written Communication Self Assessment';
  }
  if ($question->type == 'ipq_research_question') {
    $course_section = 'Research';
  }
  // Determine if question has been disabled (unpublished)
  $disabled_question = false;
  if($navigation_info['question_status'] == 0 ){
    $disabled_question = true;
  }
  ?>
<div id="ipq-container" class="ipq-container <?php if($navigation_info['deleted_question']) print 'deleted-question'; ?>">

  <?php if($navigation_info['deleted_question']): ?>
    <div class="deleted-question-info">
      <img src="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/img/icon_ATTENTION.svg" height="33" width="39" alt="Attention" />
      <div class="ipq-disabled-question-message">
        <?php print check_markup(variable_get('deleted_question_message', 'This question has been removed from this test'), 'full_html'); ?>
      </div>
      <button name="next_question" value="Next" class="btn disabled-question-next">Next Question</button>
    </div>

  <?php else: ?>

    <h1 class="ipq-section-header review-header">Review<div class="pull-right">

        <?php
        // The logic is: if exam versioning is on, we can show the retake button on any quiz review.
        // But if versioning is not on, we can only show the retake button for quizzes
        // that were taken using the default exam version
        if (exam_version_is_versioning_on()) {
          print drupal_render($form['actions']['retake']);

        }
        else {
          if ($session['session_config']['exam_version'] == exam_version_get_default_version()) {
            print drupal_render($form['actions']['retake']);
          }
        }
        ?>

    </div></h1>

    <div class="ipq-question-wrapper ipq-<?php print $session['session_type']; ?>-wrapper">
      <div id="ipq-review-body">
        <div class="session-entry-wrapper">
          <div class="row">
            <div class="col-md-9">
              <div class="session-name">
                <div class="col-md-12 row editing-session-name section-<?php print $section_name; ?>">
                  <span>
                    <h2><?php print $session['name']; ?></h2>
                    </span>
                </div><!-- /editing-session-name -->
                <div class="created-on">
                  <span class="session-type"><?php  print ucfirst($session['session_type']); ?></span>
                  <span class="created-on-timestamp">| Created <?php print format_date($session['created_on'] , 'custom', 'm-j-Y'); ?> | <?php print format_date($session['created_on'] , 'custom', 'G:i:s'); ?></span>
                </div><!-- /created-on -->
              </div><!-- /session-name -->
            </div>
            <div class="col-md-3">
              <?php if ($question->type == 'ipq_mcq_question') { ?>
                <div class="time-spent-wrapper">
                  <div class="time-wrapper pull-right">
                    <div class="time-spent">
                      <?php
                      print format_plural($form['time_spent']['#value'], '1 Second', '@count Seconds');
                      ?>
                    </div>
                    <div class="time-spent-label">Time Spent</div>
                  </div>
                  <div class="time-icon pull-right"><img src="/<?php print drupal_get_path('module','ipq_common'); ?>/css/img/icon-time-spent.svg" width="24" /></div>
                </div>
              <?php } ?>
            </div>
          </div>
        </div><!-- /session-entry-wrapper -->
        <h2 class="review-subheading"><span>Question <?php print $form['question_index']['#value']; ?></span> | <?php print $course_section; ?></h2>
        <div>
          <?php print $form['question_review_display']['#value'];
    
            print render($form['courseware_links']);

            print drupal_render($form['notes']);
         ?>
        </div>

        <div class="question-number-id">Question ID #<?php print $question->nid; ?></div>
      </div>
    </div>

  <?php endif; ?>

</div><!-- /.ipq-container-->

<?php 
  // adding footer classes for first, last and middle questions
    $footer_class = '';
  if ($form['question_index']['#value'] == '1') { 
    $footer_class = ' first-question'; 
  }
  if (!empty($form['previous_question_id']['#value']) && !empty($form['next_question_id']['#value'])) { 
    $footer_class = ' middle-question'; 
  } 
  if (!isset($form['next_question_id']['#value'])) { 
    $footer_class = ' last-question'; 
  }
  if (count($navigation_info['current_type_question_list']) == 1) {
    $footer_class = 'first-question last-question';
  }
  ?>
<div id="ipq-footer" class="<?php print $footer_class;
// add classes to footer for number of questions count
if (count($navigation_info['current_type_question_list']) > 31) {
  print ' full';
}
if (count($navigation_info['current_type_question_list']) == 30) {
  print ' exact';
} 
if (count($navigation_info['current_type_question_list']) > 18) {
  print ' full-mobile';
}
if (count($navigation_info['current_type_question_list']) > 36) {
  print ' full-mobile-extra';
}
if (count($navigation_info['current_type_question_list']) > 60) {
  print ' over-60';
}
if (count($navigation_info['current_type_question_list']) > 90) {
  print ' over-90';
}
if (!empty($form['previous_question_id']['#value'])) {
  print ' with-previous';
} ?> review-footer" role="navigation">
  <div class="reminder footer-item"><img src="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/img/flag-icon-active.png" alt="Reminder icon" height="12" width="13" /> = Reminder</div>
  <div class="number-link-wrapper">
    <div class="number-link">               
      <div class="pager-wrapper footer-item">
        <?php $i = 1; ?>
        <?php foreach($navigation_info['current_type_question_list'] as $q): ?>
          <?php

          $flagged = (is_object($navigation_info['current_type_questions_information'][$q]) && $navigation_info['current_type_questions_information'][$q] -> is_flagged) ? "flagged" : "";

          if ($i == 1) {
            $extra_class = 'first';
          } else if ($i == count($navigation_info['current_type_question_list'])) {
            $extra_class = 'last';
          } else {
            $extra_class = '';
          }
          if ($q == $question->nid) {
            $extra_class .= ' current';
          }
          ?>
          <div class="number-flag-links number-<?php print $i; ?><?php if ($i == 31 || $i == 61 || $i == 91 || $i == 116) { print ' new-row'; } ?><?php if ($i == '19' || $i == '37') { print ' new-row-mobile'; } ?>">

            <a data-qid="<?php print $q; ?>" href="" class="pager-link <?php print $extra_class; ?>"
               data-toggle="tooltip"
               data-html="true"
               data-container="#ipq-footer"
               data-placement="top"
               data-trigger="hover"><span class="reader-instructions">Question </span><?php print $i; ?><span class="reader-instructions"> of <?php print count($navigation_info['current_type_question_list']); ?></span></a>

            <a href="#" class="bookmark-ipq-flag <?php echo "btn-{$i} {$extra_class} $flagged"?>" qid="<?php echo $navigation_info['current_type_question_list'][$i-1]?>" session="<?php echo $navigation_info['session_id']?>">Bookmark question <?php echo $i; ?></a>
          </div>
          <?php
          $i++; ?>
        <?php endforeach; ?>
      </div><!-- /pager-wrapper-->
    </div><!-- /number-link-wrapper-->
  </div><!-- /number-link-wrapper-->

  <div class="next-button footer-item <?php print $footer_class; ?>">
    <?php print drupal_render($form['actions']); ?>
  </div>
</div><!-- /.ipq-footer-->

<?php
// Disabled question modal.
if($disabled_question):
  $m = new RCPARModal('ipq-disabled-question','fade',array('data-backdrop' => 'static','role' => 'dialog'));
  // Admins should be able to close this modal.
  if ($is_admin) {
    $m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>');
  }
  $disabled_body = '<div class="ipq-disabled-question-message">';
  $disabled_body .= '<img src="/' . drupal_get_path('module', 'ipq_common') . '/css/img/icon_ATTENTION.svg" height="33" width="39" class="disabled-icon" />';
  $disabled_body .= check_markup(variable_get('disabled_question_message', 'This question has been disabled'), 'full_html');
  $disabled_body .= '</div>';
  $m->setBodyContent($disabled_body)
    ->setFooterContent('<button type="button" class="btn disabled-question-next">Next Question</button>');
  print $m->render();

  endif; ?>

<?php print drupal_render_children($form); ?>
