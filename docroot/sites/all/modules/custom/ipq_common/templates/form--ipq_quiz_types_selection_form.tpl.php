
<div class="ipq-container">
	
	<h1 class="ipq-section-header">Question Type<div class="pull-right"><?php print drupal_render($form['actions']); ?></div></h1>
	
	
	<h2 class="ipq-section-subheading question-type">What type of questions do you want to practice?</h2>
	<div class="row question-choices">
	<?php print drupal_render_children($form); ?>
	</div>
</div><!-- /ipq-container -->
<div class="ipq-container quiz-step-two">
	<h2 class="ipq-section-subheading">Selected <?php print $form['section']['#value']; ?> Sections:</h2>
	<div class="row">
	<?php foreach($form['selected_chapters']['#value'] as $key => $chapter): ?>
	        <div class="option-container quiz-chapter quiz-chapter-mini pin col-sm-6">
	            <h4><?php print $chapter['#ipq_chapter_header']; ?></h4>
	            <div class="quiz-chapter-info"><?php print $chapter['#ipq_chapter_name']; ?></div>
	            <div class="quiz-chapter-info">
	                <span class="question_count incorrect with_research" style="<?php if ($form['show_count_type']['#value'] != 'incorrect' || !$form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><?php print $chapter['#ipq_incorrect_questions']; ?> INCORRECT</span>
									<span class="question_count incorrect no_research" style="<?php if ($form['show_count_type']['#value'] != 'incorrect' || $form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><?php print ($chapter['#ipq_incorrect_questions'] - $chapter['#ipq_incorrect_question_count_ipq_research_question']); ?> INCORRECT</span>

	                <span class="question_count never-seen with_research" style="<?php if ($form['show_count_type']['#value'] != 'never_seen' || !$form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><?php print $chapter['#ipq_never_seen_question_count']; ?> UNANSWERED</span>
									<span class="question_count never-seen no_research" style="<?php if ($form['show_count_type']['#value'] != 'never_seen' || $form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><?php print ($chapter['#ipq_never_seen_question_count'] - $chapter['#ipq_never_seen_question_count_ipq_research_question'] ); ?> UNANSWERED</span>

									<span class="question_count skipped with_research" style="<?php if ($form['show_count_type']['#value'] != 'skipped' || !$form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><?php print $chapter['#ipq_skipped_answered_question_count']; ?> UNANSWERED</span>
									<span class="question_count skipped no_research" style="<?php if ($form['show_count_type']['#value'] != 'skipped' || $form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><?php print ($chapter['#ipq_skipped_answered_question_count'] - $chapter['#ipq_skipped_answered_question_count_ipq_research_question']); ?> UNANSWERED</span>

									<span class="question_count total with_research" style="<?php if ($form['show_count_type']['#value'] != 'all' || !$form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><?php print $chapter['#ipq_total_questions']; ?> TOTAL QUESTIONS</span>
									<span class="question_count total no_research" style="<?php if ($form['show_count_type']['#value'] != 'all' || $form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><?php print ($chapter['#ipq_total_questions'] - $chapter['#ipq_question_count_ipq_research_question']); ?> TOTAL QUESTIONS</span>

	            </div>
	        </div>
	<?php endforeach; ?>
	</div>
</div><!-- /ipq-container -->