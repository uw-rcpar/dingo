<?php
/*
 IPQ Link of actual page: http://testcenter.rogercpareviewdev.com/testmodule/setup.php#/dashboard?section=aud 
 */
$args = arg();
$has_ajax = in_array('ajax', $args);
extract($overview);

// verify in case we are accessing with partner user, so we can have access to 
// the monitoring center.
global $user;
$pid = NULL;
if ($user->uid != $account_uid && isset($args[5])) {
  $group_id = $args[5];
  $pid = rcpar_partners_get_partner_by_group($group_id);
}
?>

<?php
    $secs_by_part = rcpar_partner_get_access_from_list($account_uid, $pid);
    $avail_sections = array();
    foreach($secs_by_part as $secs_by_p){
        foreach ($secs_by_p as $s){
            if (!in_array($s, $avail_sections)){
                $avail_sections[] = $s;
            }
        }
    }
    $a_classes = array('ajax-nav ipq-nav ipq-nav-aud');
    $b_classes = array('ajax-nav ipq-nav ipq-nav-bec');
    $r_classes = array('ajax-nav ipq-nav ipq-nav-reg');
    $f_classes = array('ajax-nav ipq-nav ipq-nav-far');

    switch(strtoupper($section)){
        case 'AUD':
            $a_classes[] = 'active';
            break;
        case 'BEC':
            $b_classes[] = 'active';
            break;
        case 'REG':
            $r_classes[] = 'active';
            break;
        case 'FAR':
            $f_classes[] = 'active';
            break;
    }

    if ($group_id){
        $group_part = '/'.$group_id;
    } else {
        $group_part = '';
    }

?>

<?php if($has_ajax) : ?>
<div class="" id="setupcontainer">
	<div class="ipq-header score-history-header">
		<div class="container">
			<h3 class="top-heading pull-left col-sm-5"><?php print t('@section Section Overview', array('@section' => strtoupper($section))); ?></h3>
			<div class="ipq-header-nav pull-left col-sm-7">
				<?php
                    if (ipq_common_perms($account_uid, 'AUD') && in_array('AUD', $avail_sections)){
                        print l('AUD', 'dashboard/testcenter/overview/'.$account_uid.$group_part.'/AUD',  array('attributes' => array('class' => $a_classes, 'ipq-sec'=>'AUD')) );
                    } else {
                        print '<span class="inactive ipq-nav ipq-nav-aud">' . t('AUD') . '</span>';
                    }
                ?>
			    <?php
                    if (ipq_common_perms($account_uid, 'BEC') && in_array('BEC', $avail_sections)){
                        print l('BEC', 'dashboard/testcenter/overview/'.$account_uid.$group_part.'/BEC',  array('attributes' => array('class' => $b_classes, 'ipq-sec'=>'BEC')) );
                    } else {
                        print '<span class="inactive ipq-nav ipq-nav-bec">' . t('BEC') . '</span>';
                    }
                ?>
			    <?php
                    if (ipq_common_perms($account_uid, 'FAR') && in_array('FAR', $avail_sections)){
                        print l('FAR', 'dashboard/testcenter/overview/'.$account_uid.$group_part.'/FAR',  array('attributes' => array('class' => $f_classes, 'ipq-sec'=>'FAR')) );
                    } else {
                        print '<span class="inactive ipq-nav ipq-nav-far">' . t('FAR') . '</span>';
                    }
                ?>
			    <?php
                    if (ipq_common_perms($account_uid, 'REG') && in_array('REG', $avail_sections)){
                        print l('REG', 'dashboard/testcenter/overview/'.$account_uid.$group_part.'/REG',  array('attributes' => array('class' => $r_classes, 'ipq-sec'=>'REG')) );
                    } else {
                        print '<span class="inactive ipq-nav ipq-nav-reg">' . t('REG') . '</span>';
                    }
                ?>
			</div>
		</div>
		<div class="user-information">
			<div class="container">
			    <div class="generated-on"> <span>Generated On:</span> <?php print format_date(time(), 'custom', 'n/j/Y g:ia'); ?></div>
			    <?php
			        $account = user_load($account_uid);
			        $email = '';
			        $fullname = '';
			        try {
			            $wapper = entity_metadata_wrapper('user', $account);
			            $email = $account->mail;
			            $fullname = $wapper->field_first_name->value() . ' ' . $wapper->field_last_name->value();
			        } catch (EntityMetadataWrapperException $exc) {
			            watchdog(__FILE__, "function ".__FUNCTION__." entity error");
			        }
			    ?>
			    <div class="user-fullname pull-left"><span class="user-fullname-label">Student:</span> <span class="user-fullname-wrapper"><?php print $fullname; ?></span></div>
			    <div class="user-email pull-left"><a href="mailto:<?php print $email ?>"><?php print $email; ?></a></div>
			</div>
		</div>
	</div>
</div>
<?php endif?>
<div class="ipq-container">
 <div class="ipq-header overview-header">
  <div class="row">
	<h1 class="ipq-overview-header col-sm-4"><?php print t('@section Overview', array('@section' => strtoupper($section))); ?></h1>
        <?php if(!$has_ajax) : ?>
	<div class="how-to-link col-sm-3"><a href="#" data-toggle="modal" data-target="#ipq-how-to">How to read this page</a></div>
	<div class="col-sm-5"><a href="/ipq/quiz/setup" class="btn btn-primary">Start a New Quiz</a></div> 
        <?php endif?>
	</div><!-- /.container -->
</div><!-- /.ipq-header -->
	
<div class="ipq-content ipq-content-<?php print $section; ?>"></div>
	
<div class="row section-overview overall">
	<div class="row">
		<div class="col-sm-4"><h2 class="progress-header"><?php print t('Total @section Progress', array('@section' => strtoupper($section))); ?></h2></div>
		<div class="col-sm-8"><?php print ($has_ajax) ? '' : drupal_render($form)  ?></div>
	</div>

		
<div class="text-center question-scores">
<div class="questions-attempted"><?php print $section_overview['general']['totals']['answered'] ?> of <?php print $section_overview['general']['totals']['total_qs']; ?> Questions Attempted</div>
<div class="question-score-bar long">
<?php
	$answered = $section_overview['general']['totals']['answered'];
	$correct_percent = $section_overview['general']['totals']['correct_percent'];
	$incorrect_percent = $section_overview['general']['totals']['incorrect_percent'];
?>
	<div class="incorrect-questions<?php if ($incorrect_percent == 100) { ?> double-rounded-corners<?php } ?>" style="width: <?php print $incorrect_percent; ?>%; left: <?php print $correct_percent; ?>%"
		 data-toggle="tooltip"
		 title="Incorrect Questions"
		 data-html="true"
		 data-placement="top">
		<span class="numbers pull-right"<?php if ($incorrect_percent == 0) { ?> style="display: none;"<?php } ?>><?php print $section_overview['general']['totals']['incorrect']; ?></span>
	</div>
	<div class="correct-questions" style="width: <?php print $correct_percent; ?>%"
		 data-toggle="tooltip"
		 title="Correct Questions"
		 data-html="true"
		 data-placement="top">
		<span class="numbers pull-right"<?php if ($correct_percent == 0) { ?> style="display: none;"<?php } ?>><?php print $section_overview['general']['totals']['correct']; ?></span>
	</div>
	</div><!-- /.question-score-bar -->
	<div class="question-scores-copy">
		<span class="right">Green: Questions answered Correctly</span>
		<span class="wrong pull-right">Orange: Questions answered Incorrectly</span>
	</div><!-- /.question-scores-copy -->
<table class="uppercase table table-bordered table-striped question-categories one-colum-table">
	<thead>
	<tr>
		<th class="lbl">Type</th>
		<th><span class="sprite-question" 
	   data-toggle="tooltip" 
     title="Unanswered Questions" 
		 data-html="true"
		 data-placement="top"></span></th>
		<th><span class="sprite-checkmark" 
	   data-toggle="tooltip" 
     title="Correct Questions" 
		 data-html="true"
		 data-placement="top"></span></th>
		<th><span class="sprite-x" 
	   data-toggle="tooltip" 
     title="Incorrect Questions" 
		 data-html="true"
		 data-placement="top"></span></th>
		<th><span class="sprite-flag" 
	   data-toggle="tooltip" 
     title="Bookmarked Questions" 
		 data-html="true"
		 data-placement="top"></span></th>
		<th><span class="sprite-note" 
	   data-toggle="tooltip" 
     title="Questions With Notes" 
		 data-html="true"
		 data-placement="top"></span></th>
	</tr>
	</thead>
	<tbody>


	<?php $pair = false; foreach($section_overview['general']['per_type'] as $type => $info): ?>
		<tr>
			<td class="<?php if($pair) print 'td-white'; $pair = !$pair; ?> lbl">
				<?php print(str_ireplace('Questions', '', $info['name'])); ?> <div class="total-number-questions"><?php print $info['total_qs'] ?> Questions</div>
			</td>
			<td><?php print $info['skipped']; ?></td>
			<td><?php print $info['correct']; ?></td>
			<td><?php print $info['incorrect']; ?></td>
			<td><?php print $info['flagged']; ?></td>
			<td><?php print $info['noted']; ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
</div><!--.question-scores-->
	</div><!--.overall-->
</div><!--.ipq-container-->
<div class="ipq-container">	
	<div class="row section-overview chapter-tables">
	<div class="browser-check-alert"></div>
	<h2 class="progress-header"><?php strtoupper($section); ?> Section Progress</h2> 
	
	<?php foreach($section_overview['chapters'] as $c) : ?>
	<div class="col-md-6 col-sm-12 quiz-pick-topics-container">
	<div class="quiz-chapter pin">
	<div class="chapter-info">
		<div class="chapter-info-text">
			<div class="chapter-number"><?php print $c['section']; ?> <?php print $c['order']; ?></div>
			<div class="chapter-name" style="display:inline-block;"><?php
			$name = $c['chapter_name'];
			$name = str_replace($c['section'] . ' ' . $c['order'] . ':', '', $name);
			print $name;
			?></div>
		</div><!--.chapter-info-text-->
	</div><!--.chapter-info-->
<div class="text-center question-scores"> 
	<div class="question-score-bar">
		<?php
		$answered = $c['answered'];
		$correct_percent = $c['correct_percent'];
		$incorrect_percent = $c['incorrect_percent'];
		?>
		<div class="incorrect-questions<?php if ($incorrect_percent == 100) { ?> double-rounded-corners<?php } ?>" style="width: <?php print $incorrect_percent; ?>%; left: <?php print $correct_percent; ?>%"
			 data-toggle="tooltip"
			 title="Incorrect Questions"
			 data-html="true"
			 data-placement="top">
			 <span class="numbers pull-right" <?php if ($incorrect_percent == 0) { ?> style="display: none;"<?php } ?>><?php print $c['incorrect']; ?></span>
		</div>
		<div class="correct-questions" style="width: <?php print $correct_percent; ?>%"
			 data-toggle="tooltip"
			 title="Correct Questions"
			 data-html="true"
			 data-placement="top">
			<span class="numbers pull-right" <?php if ($correct_percent == 0) { ?> style="display: none;"<?php } ?>><?php print $c['correct']; ?></span>
		</div>
	</div><!--.question-score-bar-->

<div class="questions-attempted"><?php print $answered; ?> of <?php print $c['total_qs']; ?> Questions Attempted</div>

<table class="uppercase table table-bordered table-striped question-categories">
  <thead>
  <tr>
  <th class="lbl">Type</th>
  <th><span class="sprite-question" 
	   data-toggle="tooltip" 
     title="Unanswered Questions" 
		 data-html="true"
		 data-placement="top"></span></th>
  <th><span class="sprite-checkmark" 
	   data-toggle="tooltip" 
     title="Correct Questions" 
		 data-html="true"
		 data-placement="top"></span></th>
  <th><span class="sprite-x" 
	   data-toggle="tooltip" 
     title="Incorrect Questions" 
		 data-html="true"
		 data-placement="top"></span></th>
  <th><span class="sprite-flag" 
	   data-toggle="tooltip" 
     title="Bookmarked Questions" 
		 data-html="true"
		 data-placement="top"></span></th>
  <th><span class="sprite-note" 
	   data-toggle="tooltip" 
     title="Questions With Notes" 
		 data-html="true"
		 data-placement="top"></span></th>
  </tr>
  </thead>
  <tbody>
	<?php $pair = false; ?>
	<?php	foreach($c['question_counts'] as $type => $qt_info): ?>
		<?php
			// This is the base url for the links to create sessions from the stats
			// eg /ipq/overview/AUD/4335/mcq/not_answered but note that the 'not_answered'
			// part is added later in the template
			$links_base_url = url("/ipq/overview/$section/{$c['chapter_nid']}/$type" );
		?>
		<tr>
			<td class="<?php if($pair) {print 'td-white';} $pair = !$pair; ?> lbl">
					<?php
						$type_name = $section_overview['general']['per_type'][$type]['name'];
						print(str_ireplace('Questions', '', $type_name));
					?>
					<div class="total-number-questions"><?php print $qt_info['total_qs'] ?>
					Questions
				</div>
			</td>
			<td>
				<?php
					if($qt_info['skipped']) {
						print '<a href="' . $links_base_url . '/not_answered">';
						print $qt_info['skipped'];
						print '</a>';
					}
					else {
						print $qt_info['skipped'];
					}
				?>
			</td>
			<td>
				<?php
				if($qt_info['correct']) {
					print '<a href="' . $links_base_url . '/correct">';
					print $qt_info['correct'];
					print '</a>';
				}
				else {
					print $qt_info['correct'];
				}
				?>
			</td>
			<td>
				<?php
				if($qt_info['incorrect']) {
					print '<a href="' . $links_base_url . '/incorrect">';
					print $qt_info['incorrect'];
					print '</a>';
				}
				else {
					print $qt_info['incorrect'];
				}
				?>
			</td>
			<td>
				<?php
				if($qt_info['flagged']) {
					print '<a href="' . $links_base_url . '/flagged">';
					print $qt_info['flagged'];
					print '</a>';
				}
				else {
					print $qt_info['flagged'];
				}
				?>
			</td>
			<td>
				<?php
				if($qt_info['noted']) {
					print '<a href="' . $links_base_url . '/noted">';
					print $qt_info['noted'];
					print '</a>';
				}
				else {
					print $qt_info['noted'];
				}
				?>
			</td>
		</tr>
	<?php endforeach; ?>
  </tbody>
</table>
</div><!--.question-scores-->	                    
</div><!--.quiz-chapter-->
</div><!--.quiz-pick-topics-container-->
	<?php endforeach; ?>
	
	</div> <!--.chapter-tables-->
</div><!--.ipq-container-->


<!-- How to Modal -->
<div id="ipq-how-to" class="modal" data-backdrop="false">
    <div class="modal-content"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="termsLabel" class="modal-title">Section Overview Help </h3>
      </div><!-- /.modal-header -->
      <div class="modal-body"><img src="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/img/help-overview.jpg" /></div>
    </div><!-- /.modal-content -->

</div><!-- /.modal --> 