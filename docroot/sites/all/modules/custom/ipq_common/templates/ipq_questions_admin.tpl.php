<?php
    $block = module_invoke('views', 'block_view', '-exp-ipq_question_search-page');
    print render($block['content']);    
?>

<div class="question-types-list">
    <ul>
        <?php foreach($available_question_types as $type => $qt): ?>
            <li class="question-types-list-item" id="show-type-<?php print $type; ?>">
                <span><?php echo $qt->numeric_type; ?></span>
                <?php echo $qt->name; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<?php foreach($available_question_types as $type => $qt): ?>
<?php
    $type_url_str = str_replace('_', '-', $qt->type);
    $question_node_type = node_type_load($qt->type);
?>
<div class="question-type-info <?php print ($active_question_type && $qt->id == $active_question_type->id ? 'active' : '') ?>" id="type-<?php print $type; ?>">
    <div class="question-type-buttons">
        <div>
            <?php print l('Create New '.$qt->name, 'node/add/'.$type_url_str); ?>
        </div>
        <div>
            <?php print l('List Overview', 'admin/settings/rcpar/ipq/ipq-question-search', array('query' => array('type' => $type_url_str))); ?>
        </div>
    </div><div class="question-type-description">
        <?php if($question_node_type->description){
            print $question_node_type->description;
        }else{
            print '...question type description...';
        } ?>
    </div>
</div>
<?php endforeach; ?>