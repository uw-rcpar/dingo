<?php  $module_path = drupal_get_path('module', 'ipq_common'); ?>
<!-- /////////// OVERVIEW ////////// -->
<div class="ipq-container smartpath-container <?php if(!empty($_GET['noscroll'])) { print 'noscroll'; } ?>">
  <div class="ipq-content ipq-content-<?php print $section; ?>"></div>

  <div class="row section-overview overall">
    <div class="row">
      <h2 class="progress-header"><?php print t('@section Overall Performance', array('@section' => strtoupper($section))); ?>
        <a href="#" class="what-is-smartpath">What is SmartPath&trade;?</a></h2>
      <div id="what-is-smartpath">
        <button type="button" class="what-is-smartpath" aria-hidden="true">&times;</button>
        <div class="what-is-smartpath-img"><img src="<?php print base_path() . $module_path; ?>/css/img/smartpath-img.png" alt="" /></div>
        <h2>Compare your scores to students who have passed the CPA Exam</h2>
        <p>SmartPath Predictive Technology<span style='font-family: "Open Sans","Arial","Helvetica",sans-serif;'>&trade;</span> provides data-driven recommendations on how to focus your efforts by comparing your progress and proficiency with previous successful students. 91% of students who meet the targets pass!</p>
      </div>
      <div class="toggle-select-wrapper">
        <form class="clearfix">
          <label for="toggle-select">View:</label> <select id="toggle-select" class="selectpicker">
            <option value="overall-table" selected="selected">Table</option>
            <option value="overall-graph">Graph</option>
          </select>
        </form>

      </div>
    </div>

    <!--
    BEGIN CHART VERSION OF OVERALL PERFORMANCE
    -->

    <div class="overall-chart clearfix overall-display" id="overall-graph">
      <div class="row">
        <div class="column-left section-column text-right trending-avg">Score</div>
        <div class="column-right data-column">
          <div class="trending-legend">
            <span class="trending-legend-segment-first">0%</span>
            <div class="legend clearfix">
              <div class="trending-legend-segment">
                <div class="percent-label">10%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">20%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">30%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">40%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">50%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">60%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">70%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">80%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">90%</div>
                <div class="divider"></div>
              </div>
              <div class="trending-legend-segment">
                <div class="percent-label">100%</div>
                <div class="divider"></div>
                <div class="divider-last"></div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- /.row-->
      <div class="scrollable-area">
        <?php
        // Get the total number of questions available by finding the highest number of available questions in all chapters.
        $available_questions = [];
        foreach ($data as $datum) {
          // create an array containing available questions for each chapter
          $available_questions[] = $datum['stats']['available_questions'];
        } // end foreach
        $total_available = max($available_questions);

        foreach ($data as $datum) {
          ?>
          <div class="row">
            <div class="column-left section-column <?php print $datum['status_class']; ?>" title="<?php print $datum['status_title']; ?>">
              <?php
              // truncate $datum['chapter_name'] string and add ellipse if over 20 characters
              $chapter_name = (strlen($datum['chapter_name']) > 30) ? substr($datum['chapter_name'], 0, 30) . '...' : $datum['chapter_name'];
              ?>
              <a class="graph-scroll-to" id="graph-scroll-to-<?php print substr(drupal_html_class($chapter_name),0,10); ?>"
                 data-toggle="tooltip" data-placement="top" title="<?php print $datum['status_title']; ?> - <?php print $datum['status_message']; ?>">
                <img src="<?php print base_path() . $module_path; ?>/css/img/indicator-<?php print $datum['status_class']; ?>.svg" alt="Icon indicating <?php print $datum['status_title']; ?>" width="14" />
                <?php print $chapter_name; ?>
              </a>
            </div>
            <div class="column-right data-column data">
              <div class="your-questions-bar"
                   style="width: <?php print round($datum['distinct_questions'] * 100 / $total_available) . '%'; ?>"
                   data-toggle="tooltip" data-placement="top"
                   title="Your questions attempted: <?php print round($datum['distinct_questions']); ?>">
              </div>
              <div class="trending-avg-bar"
                   style="width: <?php print round($datum['trending_average']) . '%'; ?>"
                   data-toggle="tooltip" data-placement="top"
                   title="Your score: <?php print round($datum['trending_average']) . '%'; ?>">
              </div>
              <div class="target-trending-avg-tick"
                   style="left: <?php print round($datum['stats']['trending_average']) . '%'; ?>"
                   data-toggle="tooltip" data-placement="top"
                   title="Target score: <?php print round($datum['stats']['trending_average']) . '%'; ?>">
              </div>
              <div class="target-questions-answered-tick"
                   style="left: <?php print round($datum['stats']['distinct_questions'] * 100 / $total_available) . '%'; ?>"
                   data-toggle="tooltip" data-placement="top"
                   title="Target questions: <?php print round($datum['stats']['distinct_questions']); ?>">
              </div>
              <?php
              // Bottom of target range = trending average minus standard deviation, rounded
              $range_bottom = round($datum['stats']['trending_average']-$datum['stats']['sd']);
              // Top of target range = trending average plus standard deviation, rounded
              $range_top = round($datum['stats']['trending_average'] + $datum['stats']['sd']);
              // If the top range is over or equal to 100, it needs to be adjusted to fit in the graph.
              if ($range_top >= 100) {
                $range_top = 99.7;
              }
              // Width of target range = the difference between the two, rounded
              $range_width = $range_top - $range_bottom;
              ?>
              <div class="target-range-indicator-bottom"
                   style="left: <?php print $range_bottom . '%'; ?>;">
              </div>
              <div class="target-range-indicator-span"
                   style="left: <?php print $range_bottom . '%'; ?>;
                     width: <?php print $range_width . '%'; ?>;">
              </div>
              <div class="target-range-indicator-top"
                   style="left: <?php print $range_top . '%'; ?>;">
              </div>
            </div>
          </div><!-- /.row-->
        <?php } // end foreach ?>

        </div><!-- /.scrollable-area -->
        <div class="row">
          <div class="column-left section-column text-right questions-answered">Questions attempted</div>
          <div class="column-right data-column">

            <!-- Axis labels -->
            <div class="questions-legend">
              <div class="questions-legend-segment-first">0</div>
              <div class="legend clearfix">
                <?php
                // Create 10 even segments from the total question count, starting with 0 & ending on the total available
                $axis_interval = ceil($total_available / 10);
                $segments = range($axis_interval, $total_available, $axis_interval);
                // If the total question count is evenly divisible by 10, then it is added to the
                // $segments array via the range() function above.
                // But if the total question count divided by 10 contains a decimal,
                // we need to add the available questions as the final segment.
                if ($total_available % 10 != 0) {
                  $segments[] = $total_available;
                }
                foreach ($segments as $axis_val) {
                  print '<div class="questions-legend-segment">
                          <div class="divider"></div>
                          <div class="divider-last"></div>
                          <div class="number">' . $axis_val . '</div>
                         </div>';
                }
                ?>
              </div><!-- /.legend -->
            </div><!-- /.questions-legend -->
          </div><!-- /.data-column -->
        </div><!-- /.row-->
        <div class="row">
          <div class="column-left section-column"></div>
          <div class="column-right data-column key">
            <div class="row">
              <div class="col-sm-3"><span class="your-trending-avg-key"></span>
                <span class="key-copy">Your score</span></div>
              <div class="col-sm-3"><span class="target-trending-avg-key"></span>
                <span class="key-copy">Target score</span></div>
            </div>
            <div class="row">
              <div class="col-sm-3"><span class="your-questions-answered-key"></span>
                <span class="key-copy">Your questions attempted</span></div>
              <div class="col-sm-3"><span class="target-questions-answered-key"></span>
                <span class="key-copy">Target questions</span></div>
            </div>
          </div>
        </div>
    </div>

    <!--
    END CHART VERSION OF OVERALL PERFORMANCE
    -->
    <!--
    BEGIN TABLE VERSION OF OVERALL PERFORMANCE
    -->
    <div class="text-center question-scores overall-display" id="overall-table">
      <div class="table-heading clearfix">
        <div class="section"><div>Section</div></div>
        <div class="status"><div>Status</div></div>
        <div class="trending"><div>Your Score</div></div>
        <div class="count"><div>Your Questions Attempted</div></div>
      </div>
      <div class="table-wrapper">
        <table class="table ipq-smartpath-table">
          <tbody>

          <?php
          foreach ($data as $datum):
            $status_indicator = '<img src="' . base_path() . $module_path . '/css/img/indicator-' . $datum['status_class'] . '.svg" alt="Icon indicating ' . $datum['status_title'] . '" width="14" /> ' . $datum['status_title'];

            ?>
            <tr class="spacer-row">
              <td colspan="4"></td>
            </tr>
            <tr>
              <td class="section">
                <div class="wrapping">
                  <?php // truncate $datum['chapter_name'] string and add ellipse if over 70 characters
              $chapter_name = (strlen($datum['chapter_name']) > 48) ? substr($datum['chapter_name'], 0, 48) . '...' : $datum['chapter_name'];
              print '<a class="table-scroll-to" id="table-scroll-to-' . substr(drupal_html_class($chapter_name),0,10) . '">' . $chapter_name . '</a>'; ?>
                </div>
              </td>
              <td class="status <?php print $datum['status_class']; ?>"><div class="wrapping"><?php print $status_indicator; ?></div></td>
              <td class="trending"><div class="wrapping">
                <span class="<?php print $datum['status_class']; ?>"><?php print round($datum['trending_average']) . '%</span> | Target: <strong>' . round($datum['stats']['trending_average']) . '%</strong>'; ?>
                </div></td>
              <td class="count"><div class="wrapping">
                <span class="<?php print $datum['status_class']; ?>"><?php print round($datum['distinct_questions']) . '</span> | Target: <strong>' . round($datum['stats']['distinct_questions']) . '</strong>'; ?>
                </div></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>

    <!--
    END TABLE VERSION OF OVERALL PERFORMANCE
    -->

  </div><!--.overall-->
</div><!--.ipq-container-->
<!-- /////////// END OVERVIEW ////////// -->


<!-- /////////// PER SECTION DATA ////////// -->
<div class="ipq-container">
  <div class="row section-overview chapter-tables">
    <div class="browser-check-alert"></div>
    <h2 class="progress-header">Analysis by Section</h2>

    <div class="progress-filter">
      <form>
        <label for="filter-select">Filter by status:</label>
        <select id="filter-select" class="selectpicker">
          <option value="none" selected="selected" class="none">None</option>
          <option value="awaiting-activity" class="awaiting-activity">Awaiting activity</option>
          <option value="getting-started" class="getting-started">Getting started</option>
          <option value="making-progress" class="making-progress">Making progress</option>
          <option value="getting-close" class="getting-close">Getting close</option>
          <option value="targets-met" class="targets-met">Targets met</option>
          <option value="needs-improvement" class="needs-improvement">Needs improvement</option>
          <option value="needs-improvement-low" class="needs-improvement-low">Needs improvement</option>
        </select>
      </form>
    </div>
    <div class="no-chapters">There are no sections matching the selected filter criteria.</div>

    <?php
    foreach ($data as $chapter_nid => $datum) {

      // Create progress indicator icons
      if ($datum['status_class'] == 'targets-met') {
        $alt_text = 'A person celebrating successfully climbing a mountain';
        $img_width = '75';
      }
      if ($datum['status_class'] == 'making-progress') {
        $alt_text = 'A person climbing a mountain';
        $img_width = '68';
      }
      if ($datum['status_class'] == 'getting-close') {
        $alt_text = 'A person nearing the top of a mountain';
        $img_width = '90';
      }
      if ($datum['status_class'] == 'getting-started') {
        $alt_text = 'A person starting out on a hike';
        $img_width = '97';
      }
      if ($datum['status_class'] == 'needs-improvement' || $datum['status_class'] == 'needs-improvement-low') {
        $alt_text = 'A person climbing a mountain';
        $img_width = '68';
      }
      if ($datum['status_class'] == 'awaiting-activity') {
        $alt_text = 'A clock icon';
        $img_width = '105';
      }
      $progress_indicator = '<img src="' . base_path() . $module_path . '/css/img/progress-' . $datum['status_class'] . '.svg" alt="' . $alt_text . '" width="' . $img_width . '" />';

      // Do some rounding for prettier display:
      $datum['stats']['trending_average'] = round($datum['stats']['trending_average'], 1);
      $datum['trending_average'] = round($datum['trending_average'], 1);

      // Truncate the chapter name for html id
      $chapter_name = (strlen($datum['chapter_name']) > 30) ? substr($datum['chapter_name'], 0, 30) . '...' : $datum['chapter_name'];
      ?>
      <!-- div for graph chapter name scroll to-->
      <div id="graph-scroll-to-<?php print substr(drupal_html_class($chapter_name),0,10); ?>-content"></div>
      <!-- div for table chapter name scroll to-->
      <div id="table-scroll-to-<?php print substr(drupal_html_class($chapter_name),0,10); ?>-content"></div>
      <div class="smart-chapter clearfix chapter-<?php print $datum['status_class']; ?>">
        <h3 class="chapter-name"><?php print $datum['chapter_name']; ?>
          <span class="chapter-status <?php print $datum['status_class']; ?>"><img src="<?php print base_path() . $module_path; ?>/css/img/indicator-<?php print $datum['status_class']; ?>.svg" alt="Icon indicating <?php print $datum['status_title']; ?>" width="15" /><?php print $datum['status_title']; ?></span>
        </h3>
        <div class="col-sm-7">
          <div class="trending-avg-wrapper stat-wrapper clearfix">
            <div class="col-sm-7">
              <!-- Graph - trending average -->
              <div class="graph-wrapper"><?php
                // The left position of the trending average tick as a percentage
                // minus the width (as a percentage) of the tick itself.
                $tick_pos_trending_avg = round($datum['stats']['trending_average']) - 3;

                // The success flag and copy needs to align on the right with the tick.
                // The copy is text-align right, so we just need to apply a padding-right to position it correctly.
                // Subtract the tick position value from 100 to get the padding needed, minus the width of the
                // Flag icon (3%).
                $success_pos_trending_avg = 100 - $tick_pos_trending_avg - 3;
                ?>
                <div class="success" style="padding-right: <?php print $success_pos_trending_avg . '%'; ?>">
                  Target Score: <?php print round($datum['stats']['trending_average']) . '%'; ?>
                  <img src="<?php print base_path() . $module_path; ?>/css/img/icon-flag.svg" alt="Flag Icon" width="10"/>
                </div>
                <div class="graph">
                  <div class="progress-tick" style="left: <?php print $tick_pos_trending_avg . '%'; ?>"></div>
                  <div class="bar" style="width:<?php print round($datum['trending_average']); ?>%" data-toggle="tooltip" data-placement="top" title="Your score: <?php print round($datum['trending_average']) . '%'; ?>"></div>
                </div>
                <!-- Axis labels (Whitespace between the legend divs breaks rendering) -->
                <div class="legend-wrapper">
                  <span class="legend-segment-first">0%</span>
                  <div class="legend">
                    <div class="legend-segment">20%</div>
                    <div class="legend-segment">40%</div>
                    <div class="legend-segment">60%</div>
                    <div class="legend-segment">80%</div>
                    <div class="legend-segment">100%</div>
                  </div>
                </div>
              </div><!-- / .graph-wrapper-->
            </div><!-- / nested .col-sm-7-->
            <div class="col-sm-5">
              <div class="user-data clearfix">
                <!-- User's data -->
                <div class="value trending-avg"><?php print round($datum['trending_average']) . '%'; ?></div>
                <div class="user-data-label">Your <br />score <a data-toggle="tooltip" data-placement="top" title="Your score is calculated from the most recent attempt of each question you’ve answered in this section. The target score reflects the average performance of students who used our course and passed the CPA Exam."><img src="<?php print base_path() . $module_path; ?>/css/img/icon-info.svg" alt="Information Icon" width="12" /></a></div>
              </div>
            </div><!-- / nested .col-sm-5-->
          </div><!-- / .trending-avg-wrapper-->

          <div class="unique-questions-wrapper stat-wrapper clearfix">
            <div class="col-sm-7">
              <!-- Graph - question count -->
              <div class="graph-wrapper">
                <?php
                // Let's do some math to determine placements of success flag and target ticks for unique question stats.
                // Find what percentage of the available questions the distinct questions are and use that to set the left position of the tick.
                // Take the number of distinct questions in the chapter, multiple by 100, and divide by number
                // of available questions, then subtract the width of the tick itself (1%).
                $tick_pos_unique_questions = round($datum['stats']['distinct_questions'] * 100 / $datum['stats']['available_questions'] - 1);

                // The success flag and copy needs to align on the right with the tick.
                // The copy is text-align right, so we just need to apply a padding-right to position it correctly.
                // Subtract the tick position value from 100 to get the padding needed, minus the width of the
                // Flag icon (3%).
                $success_pos_unique_questions = 100 - $tick_pos_unique_questions - 3;
                ?>
                <div class="success" style="padding-right: <?php print $success_pos_unique_questions . '%'; ?>">
                  Target Questions: <?php print $datum['stats']['distinct_questions']; ?>
                  <img src="<?php print base_path() . $module_path; ?>/css/img/icon-flag.svg" alt="Flag Icon" width="10"/>
                </div>
                <div class="graph">
                  <div class="progress-tick" style="left: <?php print $tick_pos_unique_questions . '%'; ?>"></div>
                  <div class="bar" style="width:<?php print round(($datum['distinct_questions'] / $datum['stats']['available_questions']) * 100, 1); ?>%" data-toggle="tooltip" data-placement="top" title="Your questions attempted: <?php print $datum['distinct_questions']; ?>"></div>
                </div>
                <!-- Axis labels (Whitespace between the legend divs breaks rendering) -->
                <div class="legend-wrapper">
                  <span class="legend-segment-first">0</span>
                  <div class="legend">
                    <?php
                    // Create 5 even segments from the total question count, starting with 0 & ending on the total available
                    $axis_interval = ceil($datum['stats']['available_questions'] / 5);
                    $segments = range($axis_interval, $datum['stats']['available_questions'], $axis_interval);
                    // If the total question count is evenly divisible by 5, then it is added to the
                    // $segments array via the range() function above.
                    // But if the total question count divided by 5 contains a decimal,
                    // we need to add the available questions as the final segment.
                    if ($datum['stats']['available_questions'] % 5 != 0) {
                      $segments[] = $datum['stats']['available_questions'];
                    }
                    foreach ($segments as $axis_val) {
                      print '<div class="legend-segment">' . $axis_val . '</div>';
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div><!-- / nested .col-sm-7-->
            <div class="col-sm-5">
              <div class="user-data clearfix">
                <!-- User's data -->
                <div class="value trending-avg"><?php print $datum['distinct_questions']; ?></div>
                <div class="user-data-label">Your questions<br/>attempted <a data-toggle="tooltip" data-placement="top" title="The number of different questions you’ve attempted in this section. The target number of questions is the average number attempted by students who used our course and passed the CPA Exam."><img src="<?php print base_path() . $module_path; ?>/css/img/icon-info.svg" alt="Information Icon" width="12" /></a></div>
              </div>
            </div><!-- / nested .col-sm-5-->
          </div> <!-- / .unique-questions-wrapper -->
        </div><!-- / .col-sm-7 -->
        <div class="col-sm-5">
          <div class="status-icon"><?php print $progress_indicator; ?></div>
          <div class="suggestion-text">
            <?php print '<p>' . $datum['status_message'] . '</p>'; ?>
          </div>
          <div class="action-links <?php print $datum['status_class']; ?>">
            <a href="/dashboard/my-courses?section=<?php print $datum['chapter_section_number']; ?>#<?php print $datum['chapter_section'] . '-tab'; ?>" class="btn btn-default view-lectures">View Lectures</a>
            <?php
            // Display disabled Smart Quiz link if chapter is not allowed for user (ie, for free trial users)
            if (!ipq_common_chapter_is_allowed($chapter_nid, $datum['chapter_section'])) {
              print '<div class="ipq_chapter_blocked"><a class="btn btn-default smart-quiz disabled">SmartQuiz</a></div>';
            }
            // Or display Smart Quiz link
            else {
              print '<a href="' . url("ipq/smartpath/quiz/$chapter_nid") . '" class="btn btn-default smart-quiz">SmartQuiz</a>';
            } ?>
          </div>
        </div>
      </div><!-- / .col-sm-5 -->
    <?php } ?>


  </div> <!--.chapter-tables-->
</div><!--.ipq-container-->
