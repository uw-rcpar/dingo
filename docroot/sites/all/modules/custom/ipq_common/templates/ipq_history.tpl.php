<?php 
/*
  IPQ Link of actual page: http://testcenter.rogercpareviewdev.com/testmodule/setup.php#/my-scores?section=aud
*/
$args = arg();
$has_ajax = in_array('ajax', $args);
extract($history);

// verify in case we are accessing with partner user, so we can have access to 
// the monitoring center.
global $user;
$pid = NULL;
if ($user->uid != $account_uid && isset($args[5])) {
  $group_id = $args[5];
  $pid = rcpar_partners_get_partner_by_group($group_id);
}

?>

<?php
    $secs_by_part = rcpar_partner_get_access_from_list($account_uid, $pid, TRUE);
    $avail_sections = array();
    foreach($secs_by_part as $secs_by_p){
        foreach ($secs_by_p as $s){
            if (!in_array($s, $avail_sections)){
                $avail_sections[] = $s;
            }
        }
    }
    $a_classes = array('ajax-nav ipq-nav ipq-nav-aud');
    $b_classes = array('ajax-nav ipq-nav ipq-nav-bec');
    $r_classes = array('ajax-nav ipq-nav ipq-nav-reg');
    $f_classes = array('ajax-nav ipq-nav ipq-nav-far');

    switch(strtoupper($section)){
        case 'AUD':
            $a_classes[] = 'active';
            break;
        case 'BEC':
            $b_classes[] = 'active';
            break;
        case 'REG':
            $r_classes[] = 'active';
            break;
        case 'FAR':
            $f_classes[] = 'active';
            break;
    }

    if ($group_id){
        $group_part = '/'.$group_id;
    } else {
        $group_part = '';
    }

?>

<?php if($has_ajax) : ?>
<div class="" id="setupcontainer">
	<div class="ipq-header score-history-header">
		<div class="container">
			<h3 class="top-heading pull-left col-sm-5"><?php print t('@section Section History', array('@section' => strtoupper($section))); ?></h3>
			<div class="ipq-header-nav pull-left col-sm-7">
				<?php
                    if (ipq_common_perms($account_uid, 'AUD') && in_array('AUD', $avail_sections)){
                        print l('AUD', 'dashboard/testcenter/history/'.$account_uid.$group_part.'/AUD',  array('attributes' => array('class' => $a_classes, 'ipq-sec'=>'AUD')) );
                    } else {
                        print '<span class="inactive ipq-nav ipq-nav-aud">' . t('AUD') . '</span>';
                    }
                ?>
			    <?php
                    if (ipq_common_perms($account_uid, 'BEC') && in_array('BEC', $avail_sections)){
                        print l('BEC', 'dashboard/testcenter/history/'.$account_uid.$group_part.'/BEC',  array('attributes' => array('class' => $b_classes, 'ipq-sec'=>'BEC')) );
                    } else {
                        print '<span class="inactive ipq-nav ipq-nav-bec">' . t('BEC') . '</span>';
                    }
                ?>
			    <?php
                    if (ipq_common_perms($account_uid, 'FAR') && in_array('FAR', $avail_sections)){
                        print l('FAR', 'dashboard/testcenter/history/'.$account_uid.$group_part.'/FAR',  array('attributes' => array('class' => $f_classes, 'ipq-sec'=>'FAR')) );
                    } else {
                        print '<span class="inactive ipq-nav ipq-nav-far">' . t('FAR') . '</span>';
                    }
                ?>
			    <?php
                    if (ipq_common_perms($account_uid, 'REG') && in_array('REG', $avail_sections)){
                        print l('REG', 'dashboard/testcenter/history/'.$account_uid.$group_part.'/REG',  array('attributes' => array('class' => $r_classes, 'ipq-sec'=>'REG')) );
                    } else {
                        print '<span class="inactive ipq-nav ipq-nav-reg">' . t('REG') . '</span>';
                    }
                ?>
			</div>
		</div>
		<div class="user-information">
			<div class="container">
			    <div class="generated-on"> <span>Generated On:</span> <?php print format_date(time(), 'custom', 'n/j/Y g:ia'); ?></div>
			    <?php
			        $account = user_load($account_uid);
			        $email = '';
			        $fullname = '';
			        try {
			            $wapper = entity_metadata_wrapper('user', $account);
			            $email = $account->mail;
			            $fullname = $wapper->field_first_name->value() . ' ' . $wapper->field_last_name->value();
			        } catch (EntityMetadataWrapperException $exc) {
			            watchdog(__FILE__, "function ".__FUNCTION__." entity error");
			        }
			    ?>
			    <div class="user-fullname pull-left"><span class="user-fullname-label">Student:</span> <span class="user-fullname-wrapper"><?php print $fullname; ?></span></div>
			    <div class="user-email pull-left"><a href="mailto:<?php print $email ?>"><?php print $email; ?></a></div>
			</div>
		</div>
	</div>
</div>
<?php endif?>


<div class="ipq-container" id="setupcontainer">
	<h1 class="ipq-section-header"><?php print t('@section Score History', array('@section' => strtoupper($section))); ?></h1>
		
	<div class="ipq-content ipq-content-<?php print $section; ?>"></div>
	<div class="row">
	<?php if(count($sessions) == 0):  ?>
	    <p class="no-saved-quizzes">You do not have any saved quiz scores. <br>
	    On this page you will find your latest scores broken down by topic.</p>
	<?php endif; ?>
	
	<?php
	    foreach($sessions AS $sess) :
	?>  
        <div class="sess-action-message-<?php print $sess['session_id']; ?> sess-action-message"></div>
	    <div class="session-entry-<?php print $sess['session_id']; ?> row session-entry-wrapper">
	    	<div class="col-sm-8">

	        	  <div class="session-name">
	            	<div class="col-md-12 row editing-session-name">
									<span>
										<?php print drupal_render($forms[$sess['session_id']]); ?>
									</span>
								</div><!-- /editing-session-name -->
							  <div class="created-on">
									<span class="session-type">
										<?php print ucfirst((strpos($sess['session_name'], 'Smart') !== FALSE) ? 'SmartQuiz' : $sess['session_type']); ?>
									</span>
									<span class="created-on-timestamp">| Created on  <?php print format_date($sess['timestamp'] , 'custom', 'n/j/Y, g:i:s A'); ?></span>
									<div class="exam-version"> <?php print ucfirst($sess['session_type']); ?> version: <?php print $sess['exam_version_description']; ?></div>
	              </div><!-- /created-on -->
	            </div><!-- /session-name -->

	            <?php if(count($sess['q_types']) > 0) : ?>
	            <div class="session-metadata">
	            	<div class="subtitle sub-heading">This session covers <?php print format_plural($sess['chapters_covered_count'], '1 Section', '@count Sections')?> containing:</div>
	                <div class="session-metadata-body">
	                	<ul class="session-metadata-list">
	                    <?php foreach($sess['q_types'] AS $quizlet_name => $count) : ?>
	                    	<li class="quiz-summary-row">
	                        <?php if(is_numeric($count)) { echo $count; } ?>
		                        <span>
	                            <?php 
  	                              // TODO: PA move this into a single value set from the module.
																	
																	$text = ipq_comon_get_qt_group_human_name($quizlet_name);
																	if ($text == 'Written Communications'){
																		if ($sess['section'] != 'BEC') {
																			$text = 'Task-Based Simulation';
																		}
																		else {
																			$text = 'Written Communications';
																		}
																	}
																	print $text;
	                            ?>
		                        </span>
							</li>
							<?php endforeach; ?>
						</ul>
					</div><!-- /session-metadata-body -->
					<div class="chapters-covered-info">Includes:
	                	<span class="chapters-covered-label"><?php print strtoupper($section); ?>:</span>
                        <?php
                              $chapters = array();
                              foreach($sess['chapters_covered'] as $chapter){
                                $chapter_wrapper = entity_metadata_wrapper('node', $chapter);
                                $delta = explode("-",$chapter_wrapper->field_prefix->value());
                                $chapters[] = isset($delta[1]) ? $delta[1] : 0;
                              }
															sort($chapters);
                            print implode(", ", $chapters);                                        
                        ?>
	               </div><!-- /chapters-covered-info -->
			</div><!-- /session-metadata -->
	        <?php endif; ?>
		</div><!-- /col-sm-8 -->
	    <div class="col-sm-4">
	    	<div class="score-history">
	        	<div class="clearfix text-right score-text">
	            	<div>
                    <?php
                        $percent =  round($sess['total_percent']);
                        print $percent;
                    ?>% Correct
	                </div>
	                <div class="question-score-bar">
	                	<div style="width:<?php print $percent; ?>%" class="correct"></div>
	                </div>
	            </div>
	        </div><!-- /score-history -->
                <?php if(!$has_ajax) : ?>
	        <div class="action-buttons score-history-buttons">
	          <!-- Review Button -->
            <?php
            $reviewURL = url('ipq/' . $sess['session_type'] . '/results');
            ?>
 	          <button type="button" class="btn btn-primary btn-sm review-sess-btn" action="review" sess="<?php print $sess['session_type']; ?>" sid="<?php print $sess['session_id']; ?>" href="<?php print $reviewURL; ?>">Review</button>
 	          <?php if (!$sess['finished']) { ?>
  	        <!-- Resume -->
            <?php if($section_version == $sess['exam_version_description']){ ?>
              <button type="button" class="btn btn-primary btn-sm rebuild-sess-btn" action="resume" sess="<?php print $sess['session_type']; ?>" sid="<?php print $sess['session_id']; ?>">Resume</button>
            <?php } else {
            	$def_version = exam_version_get_default_version();
            	if($prev_version = exam_versions_previous_version($def_version)){
            		$previous_version = $prev_version->name;?>
              		<button class="btn rebuild-sess-btn-diss use-only-bootstrap" data-toggle="popover" data-trigger="click" data-html="true" title="" data-content="<p>This action is not available because it pertains to the <?php print $previous_version;?> version of course materials, and you've upgraded to <?php print $def_version;?> materials..</p>" data-placement="bottom" data-template="<div class='popover materials-selection-popover' role='tooltip'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div></div>" data-original-title="Feature Disabled" > Resume </button>
            <?php } }?>
          <?php } else { ?>
	          <!-- Retake Button -->
            <?php if($section_version == $sess['exam_version_description']){ ?>
              <button type="button" class="btn btn-primary btn-sm rebuild-sess-btn" action="retake" sess="<?php print $sess['session_type']; ?>" sid="<?php print $sess['session_id']; ?>">Retake</button>
            <?php } else {
              	$def_version = exam_version_get_default_version();
            	if($prev_version = exam_versions_previous_version($def_version)){
            		$previous_version = $prev_version->name;?>
              		<button class="btn rebuild-sess-btn-diss use-only-bootstrap" data-toggle="popover" data-trigger="click" data-html="true" title="" data-content="<p>This action is not available because it pertains to the <?php print $previous_version;?> version of course materials, and you've upgraded to <?php print $def_version;?> materials..</p>" data-placement="bottom" data-template="<div class='popover materials-selection-popover' role='tooltip'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div></div>" data-original-title="Feature Disabled" > Retake </button>
            <?php } } ?>

          <?php } ?>
	          <!-- Delete Btn -->
	          <button type="button" class="btn btn-primary btn-sm delete-btn" data-toggle="modal" data-target=".conf-<?php print $sess['session_id']; ?>">Delete</button>
				<!-- Delete Modal -->
				<div class="modal fade bs-example-modal-sm conf-<?php print $sess['session_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="Delete Sess">
					<div class="modal-dialog modal-sm">
						<div class="modal-content">
	                        <div class="modal-header">
	                        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                             <h3 class="modal-title" id="myModalLabel">Delete Session</h3>
	                        </div><!-- /modal-header -->
	                        <div class="modal-body">
	                        	Please confirm the deletion of: <?php print $sess['session_name']; ?>
	                        </div><!-- /modal-body -->
	                        <div class="modal-footer">
	                        	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                            <button type="button" class="btn btn-primary del-sess-btn" data-dismiss="modal" sid="<?php print $sess['session_id']; ?>">Delete</button>
	                         </div><!-- /modal-footer -->
	                     </div><!-- /modal-content -->
					</div><!-- /modal-dialog -->
				</div><!-- /modal -->
			</div><!-- /action-buttons -->
                        <?php endif; ?>
	     </div><!-- /col-sm-4 -->
		<div class="clear-empty"></div>
	</div><!-- /container -->
	
	<?php
	    endforeach;            
	?>
    <?php echo theme('ajax_pager', array('parameters' => array('selector' => 'container')));?>
</div><!-- /ipq-container -->
