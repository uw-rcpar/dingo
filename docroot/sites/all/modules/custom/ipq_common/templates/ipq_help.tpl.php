<?php

$tree = $data['tree'];
$article = $data['article'];

?>

<div class="ipq-help-wrapper">
  <!-- Menu tree -->
  <div id="ipq-help-menu" class="left-side">
    <div class="tree-wrapper">
      <?php print(render($tree)); ?>
    </div>
  </div>

  <!-- Article content -->
  <div id="ipq-help-content" class="right-side">
    <div class="content-wrapper">
      <?php print(render($article)); ?>
    </div>
  </div>
</div>