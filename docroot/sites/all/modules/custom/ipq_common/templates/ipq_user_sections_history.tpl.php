<?php
sort($sections);
?>

<div class="ipq-section-container">
    <div class="section-selection-container">
        <h1 class="page-title ipq-title" >Please Choose a Course</h1>
        <div class="course-section-links">

            <?php foreach ($sections as $section) { ?>
              <div class="col-sm-3">
                  <a class="ipq-section-history-link" href="/ipq/history/<?php print $uid?>/<?php print $section ?>/nojs"> <?php print $section ?> </a>
              </div>
            <?php } ?>

        </div>
    </div>
</div>