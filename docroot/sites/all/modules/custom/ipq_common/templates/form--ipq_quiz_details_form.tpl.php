<div class="ipq-container">
<?php
    // Template for the 'quiz_details' step of quiz setup wizard
?>
	
	<?php
	    $chapters_count = count($form['ipq_session_info']['#value']['session_config']['chapters']);
	?>
	<h1 class="ipq-section-header">Quiz Details<div class="pull-right"><?php print drupal_render($form['actions']); ?></div></h1>
	
	
	
	<h2 class="session-name"><?php print drupal_render($form['name_container']); ?></h2>
	<div class="created-on">Quiz | Created on <?php print format_date($form['ipq_session_info']['#value']['created_on'], 'custom', 'n/j/Y'); ?></div>
	
	<div class="quiz-sections">This Quiz covers <?php print $chapters_count; ?> section<?php print $chapters_count > 0 ? 's' : ''; ?> containing:</div>
	
	
	<ul class="quiz-sections-list">
	    <?php foreach($form['ipq_session_info']['#value']['session_config']['question_types'] as $type => $count): ?>
	        <?php if($count > 0): ?>
						<?php
							// user might have selected X TBS questions, but then unchecked the 'include DRS questions checkbox'
							// and that, in some particular cases might result in a session that have a little less (see issue IB-175
					    // https://jira.rogercpareview.com/browse/IB-175).
							// hence, we must print the actual question number intead of the selected one here

							$count = isset($form['ipq_session_info']['#value']['session_config']['question_list'][$type]) ? count($form['ipq_session_info']['#value']['session_config']['question_list'][$type]) : $count;
						?>
	            <li><?php print $count ?> <?php print $form['question_types_info']['#value'][$type]['name']; ?></li>
	        <?php endif; ?>
	    <?php endforeach; ?>
	</ul>
</div>
<div class="ipq-container quiz-step-three">
	<h2 class="ipq-section-subheading border-line">Additional Options:</h2>
	
	<?php print drupal_render($form['hide_timer']); ?>
	<?php print drupal_render($form['score_as_you_go']); ?>
	
	<?php print drupal_render_children($form); ?>
	<div class="clear-empty"></div>
</div>