<div class="spreadsheet-dialog" id="spreadsheet" title="Spreadsheet">
				<span data-hot-id="spreadsheet-tool" class="current-cell" title="Current cell"></span>
				<input data-hot-id="spreadsheet-tool" type="button" class="cancel" value="Cancel" title="Cancel"></input>
				<input data-hot-id="spreadsheet-tool" type="button" class="submit" value="Submit" title="Submit"></input>
				<input data-hot-id="spreadsheet-tool" type="button" class="show-functions" value="List All Functions" title="List all functions"></input>
				<input data-hot-id="spreadsheet-tool" class="formula" type="text" title="Enter formula here"/>
				<input data-hot-id="spreadsheet-tool" type	="button" class="cut" value="Cut" title="Cut"></input>
				<input data-hot-id="spreadsheet-tool" type="button" class="copy" value="Copy" title="Copy"></input>
				<input data-hot-id="spreadsheet-tool" type="button" class="paste" value="Paste" title="Paste"></input>
				<div class="spreadsheet-container">
					<div data-hot-id="spreadsheet-tool" id="spreadsheet-table" class="hot handsontable"></div>
				</div>
</div>

	<div data-hot-id="spreadsheet-tool" class="modal-content functions-dialog functionslist" id="functionslist">
		<div class="modal-header">
			<button data-hot-id="spreadsheet-tool" class="close" type="button">x</button>
			<h3 class="modal-title">Functions List</h3>
		</div>
		<div class="modal-body">
				<select data-hot-id="spreadsheet-tool" class="function-select function-types" size="11">
					<option value="all">All</option>
				</select>
				<select data-hot-id="spreadsheet-tool" class="function-select function-names" size="11">
					<option value="abs">ABS</option>
					<option value="accrint">ACCRINT</option>
					<option value="acos">ACOS</option>
					<option value="acosh">ACOSH</option>
					<option value="acoth">ACOTH</option>
					<option value="and">AND</option>
					<option value="arabic">ARABIC</option>
					<option value="asin">ASIN</option>
					<option value="asinh">ASINH</option>
					<option value="atan">ATAN</option>
					<option value="atan2">ATAN2</option>
					<option value="atanh">ATANH</option>
					<option value="avedev">AVEDEV</option>
					<option value="average">AVERAGE</option>
					<option value="averagea">AVERAGEA</option>
					<option value="averageif">AVERAGEIF</option>
					<option value="base">BASE</option>
					<option value="bessli">BESSELI</option>
					<option value="besselj">BESSELJ</option>
					<option value="besselk">BESSELK</option>
					<option value="bessely">BESSELY</option>
					<option value="betadist">BETADIST</option>
					<option value="betainv">BETAINV</option>
					<option value="bin2dec">BIN2DEC</option>
					<option value="bin2hex">BIN2HEX</option>
					<option value="bin2oct">BIN2OCT</option>
					<option value="binomdist">BINOMDIST</option>
					<option value="binomdistrange">BINOMDISTRANGE</option>
					<option value="binominv">BINOMINV</option>
					<option value="bitand">BITAND</option>
					<option value="bitlshift">BITLSHIFT</option>
					<option value="bitor">BITOR</option>
					<option value="bitrshift">BITRSHIFT</option>
					<option value="bitxor">BITXOR</option>
					<option value="ceiling">CEILING</option>
					<option value="ceilingmath">CEILINGMATH</option>
					<option value="ceilingprecise">CEILINGPRECISE</option>
					<option value="char">CHAR</option>
					<option value="chisqdist">CHISQDIST</option>
					<option value="chisqinv">CHISQINV</option>
					<option value="code">CODE</option>
					<option value="combin">COMBIN</option>
					<option value="combina">COMBINA</option>
					<option value="complex">COMPLEX</option>
					<option value="concatenate">CONCATENATE</option>
					<option value="confidencenorm">CONFIDENCENORM</option>
					<option value="confidencet">CONFIDENCET</option>
					<option value="convert">CONVERT</option>
					<option value="correl">CORREL</option>
					<option value="cos">COS</option>
					<option value="cosh">COSH</option>
					<option value="cot">COT</option>
					<option value="coth">COTH</option>
					<option value="count">COUNT</option>
					<option value="counta">COUNTA</option>
					<option value="countblank">COUNTBLANK</option>
					<option value="countif">COUNTIF</option>
					<option value="countifs">COUNTIFS</option>
					<option value="countin">COUNTIN</option>
					<option value="countunique">COUNTUNIQUE</option>
					<option value="covariancep">COVARIANCEP</option>
					<option value="covariances">COVARIANCES</option>
					<option value="csc">CSC</option>
					<option value="csch">CSCH</option>
					<option value="cumipmt">CUMIPMT</option>
					<option value="cumprinc">CUMPRINC</option>
					<option value="date">DATE</option>
					<option value="datevalue">DATEVALUE</option>
					<option value="day">DAY</option>
					<option value="days">DAYS</option>
					<option value="days360">DAYS360</option>
					<option value="db">DB</option>
					<option value="ddb">DDB</option>
					<option value="dec2bin">DEC2BIN</option>
					<option value="dec2hex">DEC2HEX</option>
					<option value="dec2oct">DEC2OCT</option>
					<option value="decimal">DECIMAL</option>
					<option value="degrees">DEGREES</option>
					<option value="delta">DELTA</option>
					<option value="devsq">DEVSQ</option>
					<option value="dollar">DOLLAR</option>
					<option value="dollarde">DOLLARDE</option>
					<option value="dollarfr">DOLLARFR</option>
					<option value="e">E</option>
					<option value="edate">EDATE</option>
					<option value="effect">EFFECT</option>
					<option value="eomonth">EOMONTH</option>
					<option value="erf">ERF</option>
					<option value="erfc">ERC</option>
					<option value="even">EVEN</option>
					<option value="exact">EXACT</option>
					<option value="expondist">EXPONDIST</option>
					<option value="false">FALSE</option>
					<option value="fdist">FDIST</option>
					<option value="finv">FINV</option>
					<option value="fisher">FISHER</option>
					<option value="fisherinv">FISHERINV</option>
					<option value="if">IF</option>
					<option value="int">INT</option>
					<option value="iseven">ISEVEN</option>
					<option value="isodd">ISODD</option>
					<option value="ln">LN</option>
					<option value="log">LOG</option>
					<option value="log10">LOG10</option>
					<option value="max">MAX</option>
					<option value="maxa">MAXA</option>
					<option value="median">MEDIAN</option>
					<option value="min">MIN</option>
					<option value="mina">MINA</option>
					<option value="mod">MOD</option>
					<option value="not">NOT</option>
					<option value="odd">ODD</option>
					<option value="or">OR</option>
					<option value="pi">PI</option>
					<option value="power">POWER</option>
					<option value="round">ROUND</option>
					<option value="rounddown">ROUNDDOWN</option>
					<option value="roundup">ROUNDUP</option>
					<option value="sin">SIN</option>
					<option value="sinh">SINH</option>
					<option value="split">SPLIT</option>
					<option value="sqrt">SQRT</option>
					<option value="sqrtpi">SQRTPI</option>
					<option value="sum">SUM</option>
					<option value="sumif">SUMIF</option>
					<option value="sumifs">SUMIFS</option>
					<option value="sumproduct">SUMPRODUCT</option>
					<option value="sumsq">SUMSQ</option>
					<option value="sumx2my2">SUMX2MY2</option>
					<option value="sumx2py2">SUMX2PY2</option>
					<option value="tan">TAN</option>
					<option value="tanh">TANH</option>
					<option value="true">TRUE</option>
					<option value="trunc">TRUNC</option>
					<option value="xor">XOR</option>
				</select>

				<input data-hot-id="spreadsheet-tool" class="function-name-input" type="text" />

				<div class="funtion-actions">
					<input data-hot-id="spreadsheet-tool" type="button" class="function-submit btn btn-default" value="OK"></input>
					<input data-hot-id="spreadsheet-tool" type="button" class="function-cancel btn btn-default" value="Cancel"></input>
					<input data-hot-id="spreadsheet-tool" type="button" class="function-apply btn btn-primary" value="Apply"></input>
				</div>
		</div>
	</div>
