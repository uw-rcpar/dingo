<?php 
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 * - $section: the section for AUD BEC REG FAR
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['header']: Items for the header region.
 * - $page['lower_navigation']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<!-- THIS IS THE SCREEN USERS WILL SEE WHEN THEY PAUSE A QUIZ -->
<div class="paused-screen">
  <input type="button" class="unpause-timer" value="Unpause"></input>
</div>
<div class="page-wrapper">
<!-- THIS IS THE SIDE MENU -->
<nav id="cbp-spmenu-s2" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
    <div id="dashboard-close-x" class="dashboard-close-button"></div>
     <?php if (!empty($page['lower_navigation'])): ?>
         <div class="navigation-nav">
             <?php print render($page['lower_navigation']); ?>
         </div><!-- /navigation-nav -->
    <?php endif; ?>
    <?php if (!empty($page['header'])): ?>
        <?php print render($page['header']); ?>
    <?php endif; ?>
    
</nav><!-- /nav -->

  <!-- **************************** -->
  <!-- NOTIFCATIONS -->
  <!-- **************************** -->
  <nav id="cbp-spmenu-notif" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
  </nav>
  <div class="overlay-container"></div>
  <!-- **************************** -->
  <!-- END NOTIFCATIONS -->
  <!-- **************************** -->


  <div class="overlay-container"></div>
<!-- END OF SIDE MENU -->

<div class="session-type-<?php print arg(1); ?>">
	<header id="ipq-navbar" role="banner" class="<?php print $navbar_classes; ?> no-gutter<?php if (arg(2)=='take') { ?> take-quiz-navbar<?php }  ?> header-element">
	        <a class="logo rcpar-logo navbar-btn pull-left" href="/" title="<?php print t('Home'); ?>"><img src="/<?php print drupal_get_path('module', 'ipq_common'); ?>/css/img/ipq-r-logo.png" alt="<?php print t('RCPAR'); ?>" /></a><a class="logo navbar-btn pull-left" href="/ipq/overview" title="<?php print t('IPQ'); ?>"><img src="/<?php print drupal_get_path('module', 'ipq_common'); ?>/css/img/ipq-logo.png" alt="<?php print t('IPQ'); ?>" /></a>
	        
	        <ul class="menu nav navbar-nav" id="ipq-menu">
		        <li class="ipq-overview-link"><a href="/ipq/overview">Section Overview</a></li>
		        <?php if(ipq_common_smartpath_access()) { ?><li class="ipq-smartpath-link"><a href="<?php print url('ipq/smartpath'); ?>">SmartPath</a></li><?php } ?>
		        <li class="ipq-quiz-link"><a href="/ipq/quiz/setup">New Quiz</a></li>
		        <li class="ipq-exam-link"><a href="/ipq/exam/setup">Exam Simulator</a></li>
            <li class="ipq-history-link"><a href="/ipq/history">Score History</a></li>
					</ul>
	        
	      <button id="showRightPush" class="navbar-toggle-2" type="button">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <div id="close-x" class="close-button hidden-xs hidden-sm hidden-md hidden-lg"></div>
	      </button>
    <button type="button" class="navbar-toggle-notif hidden" id="showNotifications">
    </button>
	        <ul id="ipq-section">
		        <li class="ipq-section-link ipq-section-AUD<?php if ($sectionlink=='AUD') { ?> active<?php } else { ?> non-active<?php } ?> <?php echo in_array('AUD',$sections) ? "" : "section-disabled"?>" id="section-AUD"><a <?php echo in_array('AUD',$sections) ? 'href="?section=AUD"' : ''?>>AUD</a></li>
		        <li class="ipq-section-link ipq-section-BEC<?php if ($sectionlink=='BEC') { ?> active<?php } else { ?> non-active<?php } ?> <?php echo in_array('BEC',$sections) ? "" : "section-disabled" ?>" id="section-BEC"><a <?php echo in_array('BEC',$sections) ? 'href="?section=BEC"' : ''?>>BEC</a></li>
		        <li class="ipq-section-link ipq-section-FAR<?php if ($sectionlink=='FAR') { ?> active<?php } else { ?> non-active<?php } ?> <?php echo in_array('FAR',$sections) ? "" : "section-disabled"?>" id="section-FAR"><a <?php echo in_array('FAR',$sections) ? 'href="?section=FAR"' : ''?>>FAR</a></li>
		        <li class="ipq-section-link ipq-section-REG<?php if ($sectionlink=='REG') { ?> active<?php } else { ?> non-active<?php } ?> <?php echo in_array('REG',$sections) ? "" : "section-disabled"?>" id="section-REG"><a <?php echo in_array('REG',$sections) ? 'href="?section=REG"' : ''?>>REG</a></li>
	        </ul>
	</header>
	<!-- this is the collapsed version of the menu for exams-->
	<div id="exam-navbar" class="exam-collapsed navbar container header-element">
  	<a class="exam-rcpar-logo pull-left" href="/" title="<?php print t('Home'); ?>"><img src="/<?php print drupal_get_path('module', 'ipq_common'); ?>/css/img/exam-logo.png" alt="<?php print t('RCPAR'); ?>" /></a><a class="exam-ipq-logo pull-left" href="/ipq/overview" title="<?php print t('IPQ'); ?>"><img src="/<?php print drupal_get_path('module', 'ipq_common'); ?>/css/img/exam-ipq-logo.png" alt="<?php print t('IPQ'); ?>" /></a><a class="exam-pulldown" title="<?php print t('IPQ'); ?>"><img src="/<?php print drupal_get_path('module', 'ipq_common'); ?>/css/img/exam-pulldown.png" alt="<?php print t('IPQ'); ?>" /></a>
	</div><!-- /#exam-navbar -->
	
	<div class="main-container container">
	
	  <?php print $messages; ?>
	
	  <div class="row">
	     
	      <a id="main-content"></a>
        <?php if (!empty($page['highlighted'])): ?>
          <div class="highlighted"><?php print render($page['highlighted']); ?></div>
        <?php endif; ?>
	      <?php if (!empty($page['help'])): ?>
	        <?php print render($page['help']); ?>
	      <?php endif; ?>
	      
	      <?php print render($page['content']); ?>
	
	  </div><!-- /row -->
	  	<?php if (arg(2)!='take') { ?><div class="clearfix copyright"> Copyright © 2013-<?php print date('Y'); ?> Roger CPA Review. All rights reserved.</div><?php } ?>
	</div><!-- /main-container -->
</div>
<script type="text/javascript">
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        menuRight = document.getElementById( 'cbp-spmenu-s2' ),
        menuTop = document.getElementById( 'cbp-spmenu-s3' ),
        menuBottom = document.getElementById( 'cbp-spmenu-s4' ),
        showRightPush = document.getElementById( 'showRightPush' ),
        body = document.body;


    showRightPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toleft' );
        classie.toggle( menuRight, 'cbp-spmenu-open' );
        disableOther( 'showRightPush' );
    };

    function disableOther( button ) {
        if( button !== 'showRightPush' ) {
            classie.toggle( showRightPush, 'disabled' );
        }
    }
</script>
 <script type="text/javascript" src="https://jira.rogercpareview.com/s/add0e988e840f60d236cb501a7b7e5c2-T/en_US69eqt9/64022/7/1.4.26/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=8cc41220"></script>
