
<div class="ipq-container">

<?php
    // Template for the 'section_selection' step of quiz setup wizard
?>


<h1 class="ipq-section-header"><?php print $form['section']['#value']; ?><div class="pull-right"><?php print drupal_render($form['actions']); ?></div></h1>


<h2 class="ipq-section-subheading">Select <?php print $form['section']['#value']; ?> Sections</h2>

<p class="blurb">Create practice quizzes that focus on a specific section or sections. This is best to complete after reviewing the
    corresponding textbook sections and lectures, or if there are particular areas you need to focus on.</p>

<h3 class="ipq-section-content-heading">Question Filters</h3>
<p class="blurb filters-blurb">Without filters, questions are selected randomly and can include answered, unanswered, correct and incorrect questions.</p>

<?php print drupal_render($form['question_filters']); ?>

<select id="select-all-none" class="chaptersSelectBox selectpicker">
    <option value="select">Select</option>
    <option value="selectAll">All</option>
    <option value="selectNone">None</option>
</select>

<div class="options-container">
    <?php $i = 0; foreach($form['chapters']['chapts'] as $key => &$chapter): ?>
        <?php if(is_numeric($key)): ?>
            <div class="option-container quiz-chapter quiz-chapter-mini pin col-sm-6">
                <h4><?php print $form['section']['#value']; ?> <?php print $chapter['#ipq_delta']; ?></h4>
                <div><?php print drupal_render($chapter); ?></div>
                <div class="quiz-chapter-info">
                    <span class="question_count incorrect with_research" style="<?php if ($form['show_count_type']['#value'] != 'incorrect' || !$form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><span class="question-count-value"><?php print $chapter['#ipq_incorrect_question_count']; ?></span> INCORRECT</span>
                    <span class="question_count incorrect no_research" style="<?php if ($form['show_count_type']['#value'] != 'incorrect' || $form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><span class="question-count-value"><?php print ($chapter['#ipq_incorrect_question_count'] - $chapter['#ipq_incorrect_question_count_ipq_research_question']); ?></span> INCORRECT</span>

                    <span class="question_count never-seen with_research" style="<?php if ($form['show_count_type']['#value'] != 'never_seen' || !$form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><span class="question-count-value"><?php print $chapter['#ipq_never_seen_question_count']; ?></span> UNANSWERED</span>
                    <span class="question_count never-seen no_research" style="<?php if ($form['show_count_type']['#value'] != 'never_seen' || $form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><span class="question-count-value"><?php print ($chapter['#ipq_never_seen_question_count'] - $chapter['#ipq_never_seen_question_count_ipq_research_question'] ); ?></span> UNANSWERED</span>

                    <span class="question_count total with_research" style="<?php if ($form['show_count_type']['#value'] != 'all' || !$form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><span class="question-count-value"><?php print $chapter['#ipq_question_count']; ?></span> TOTAL QUESTIONS</span>
                    <span class="question_count total no_research" style="<?php if ($form['show_count_type']['#value'] != 'all' || $form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><span class="question-count-value"><?php print ($chapter['#ipq_question_count'] - $chapter['#ipq_question_count_ipq_research_question']); ?></span> TOTAL QUESTIONS</span>

                    <span class="question_count skipped with_research" style="<?php if ($form['show_count_type']['#value'] != 'skipped' || !$form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><span class="question-count-value"><?php print $chapter['#ipq_skipped_answered_question_count']; ?></span> SKIPPED QUESTIONS</span>
                    <span class="question_count skipped no_research" style="<?php if ($form['show_count_type']['#value'] != 'skipped' || $form['hasNASBA']['#value']) { print 'display: none;'; } ?>"><span class="question-count-value"><?php print ($chapter['#ipq_skipped_answered_question_count'] - $chapter['#ipq_skipped_answered_question_count_ipq_research_question']); ?></span> SKIPPED QUESTIONS</span>
                </div>
            </div>
        <?php endif; $i++; ?>
    <?php endforeach;
        // if there are an odd number of divs output we need an extra empty div to fill the space
        if ($i % 2 != 0) { ?>
        <div class="empty-option-container col-sm-6"><div></div></div>
     <?php    } ?>
</div>
<div class="row">
<?php print drupal_render_children($form); ?>
</div>
</div>