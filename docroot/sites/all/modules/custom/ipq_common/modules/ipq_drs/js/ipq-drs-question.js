(function ($) {
  Drupal.behaviors.rcpar_ipq_drs_display = {

    displayWidget: function(widget, show, context){
      var qid = this.getQuestionIdFromWidget(widget);
      if (show){
        $('#subquestion-widget-popup-'+qid, context).show();
        $(widget).addClass('active');
      } else {
        $('#subquestion-widget-popup-'+qid, context).hide();
        $(widget).removeClass('active');
      }
    },

    // Please note that the "temporary" parameter is optional (false will be assumed if not present)
    updateSubQuestionAnswer: function(widget, value, temporary){
      temporary  = typeof temporary  !== 'undefined' ? temporary  : false;

      // if the user didn't selected anything, we just return
      if (typeof value == 'undefined'){
        return;
      }

      if (value == ''){
        value = 'reset';
      }
      // we get the selected subquestion id for later usage
      var qid = this.getQuestionIdFromWidget(widget);

      // first, we clear the widget status
      $(widget).removeClass('not-answered');
      $(widget).removeClass('answered');
      $(widget).removeClass('deleted');
      if (value == 'reset'){
        $(widget).addClass('not-answered');
      } else if (value == 'delete'){
        $(widget).addClass('deleted');
      } else {
        $(widget).addClass('answered');
      }

      // when deleting the question, we just print a line-through the current answer
      if (value != 'delete'){
        var answer_text = $('#subquestion-widget-'+qid+'-'+value+'-value').html();
        $('#selected-answer-'+qid).html(answer_text);
      }
      if(!temporary){
        // on some cases (like when clicking the reset button)
        // we also need to update the selected radio button
        $('input:checked', $('#subquestion-widget-popup-'+qid)).prop('checked', false);
        $('input[value="' + value + '"]', $('#subquestion-widget-popup-'+qid)).prop('checked', true);
        if(value == 'reset'){

          var first_option = $('.original-'+qid).first();
          var value = $(first_option).attr('id').replace('answer-', '');
          var answer_text = $('#subquestion-widget-'+qid+'-'+value+'-value').html();
          $('#selected-answer-'+qid).html(answer_text);

          $('#subquestion-widget-'+ qid +'-current-value').val('');
        } else {
          $('#subquestion-widget-'+ qid +'-current-value').val(value);
        }

        // the reset button, will only be enabled when the answer was reverted to its original value
        if(value != 'reset'){
          $('#subquestion-widget-reset-btn-'+qid).removeAttr("disabled");
        } else {
          $('#subquestion-widget-reset-btn-'+qid).attr("disabled", true);
        }
      }
      // Wrapping this in a try/catch because this variable may not be availabe when reviewing or previewing questions and might throw an error.
      try {
        // We mark the question as modified for the auto saving feature
        Drupal.behaviors.rcpar_ipq_session_display.needsSaving = true;
      }
      catch (e) {}
    },

    getQuestionIdFromWidget: function(widget){
      return $(widget).attr('data-widget-id');
    },

    attach: function (context, settings) {
      var behaviours = this;

      $('.subquestion-widget.subquestion-widget-form', context).once('subquestion-widget', function() {

        var widget = this;
        var qid = behaviours.getQuestionIdFromWidget(widget);
        // now we load the body of the popup widget and place it inside the html
        var widget_body = $("#subquestion-widget-script-container-" + qid, context).html().trim();
        $('#subquestion-widget-' + qid, context).after(widget_body);
        $(this).click(function () {
          // open the current one widget popup
          // For some reason context is not limited to the question and is affecting the view solution modal
          // causing conflicts on clicked element.
          var isInsideSolutionModal = $(this).closest(".modal").length;
          if (!isInsideSolutionModal) {
            behaviours.displayWidget(widget, true, context);
          }
        });

        $('input:radio', $('#subquestion-widget-popup-' + qid)).change(function () {
          var value = $(this).val();
          // we set the values, but we use the "temporary" option of the updateSubQuestionAnswer function
          behaviours.updateSubQuestionAnswer(widget, value, true);
        });

        $('#subquestion-widget-reset-btn-' + qid).click(function () {
          $('#subquestion-widget-reset-btn-' + qid).hide();
          $('#confirm-reset-container-' + qid).show();
          return false;
        });

        $('#subquestion-widget-confirm-reset-btn-' + qid).click(function () {
          behaviours.displayWidget(widget, false);
          behaviours.updateSubQuestionAnswer(widget, 'reset');

          $('#subquestion-widget-reset-btn-' + qid).show();
          $('#subquestion-widget-reset-btn-' + qid).attr("disabled", "disabled");
          $('#confirm-reset-container-' + qid).hide();
          return false;
        });

        $('#subquestion-widget-cancel-btn-' + qid).click(function () {
          behaviours.displayWidget(widget, false);

          var qid = behaviours.getQuestionIdFromWidget(widget);
          // we need to get the current value (we want to discard the selected radio button)
          var current_value = $('#subquestion-widget-' + qid + '-current-value').val();
          behaviours.updateSubQuestionAnswer(widget, current_value);

          $('#subquestion-widget-reset-btn-' + qid).show();
          $('#confirm-reset-container-' + qid).hide();
          return false;
        });

        $('#subquestion-widget-accept-btn-' + qid).click(function () {
          var new_value = $('input:checked', $('#subquestion-widget-popup-' + qid)).val();
          behaviours.updateSubQuestionAnswer(widget, new_value);
          behaviours.displayWidget(widget, false);

          $('#subquestion-widget-reset-btn-' + qid).show();
          $('.confirm-reset-container-' + qid).hide();
          return false;
        });


      });

    },
  };
}(jQuery));
