<?php
  $subquestion_id = $subquestion->id->value();
  $widget_id = 'subquestion-widget-'.$subquestion_id;
  $default_value = '';
  $default_value_id = '';
  // by default, the first answer is loaded
  if(isset($subquestion->field_drs_answers[0])){
    $original_value = $subquestion->field_drs_answers[0]->title->value();
    $original_value_id = $subquestion->id->value();
  }

  $user_answer = null;
  $default_text = $original_value;

  if (isset($saved_values[$subquestion_id])){
    $user_answer = $saved_values[$subquestion_id];
    // user already answered this question, we need to find the answer and load it as default value
    foreach($subquestion->field_drs_answers as $delta => $answer){
      if ($answer->id->value() == $saved_values[$subquestion_id]){
        $default_text = $answer->title->value();
        $default_value = $default_text;
        $default_value_id = $answer->id->value();
        break;
      }
    }
  }

  if (isset($saved_values[$subquestion_id]) && $saved_values[$subquestion_id] != '' ) {
    if ($saved_values[$subquestion_id] == 'delete'){
      $default_value_id = 'delete';
      $answer_class = 'deleted';
    } else {
      $answer_class = 'answered';
    }
  } else {
    $answer_class = 'not-answered';
  }
?>
<span id="<?php print $widget_id; ?>" data-widget-id="<?php print $subquestion_id; ?>" class="subquestion-widget <?php print $answer_class; ?> subquestion-widget-form">
  <span id="selected-answer-<?php print $subquestion_id; ?>" class="selected-answer"><?php print $default_text; ?></span>
</span>
<?php
  // now we are defining the body of the widget but as we need to use some divs
  // (and that wouldn't work well if we put them inside the inline subquestion-widget element)
  // we define the structure here and let the js to place it in the html
?>
<script type="text/html" id="subquestion-widget-script-container-<?php print $subquestion_id; ?>">
  <div id="subquestion-widget-popup-<?php print $subquestion_id; ?>" class="subquestion-widget-popup" style="display: none;">
    <input type="hidden"
           id="subquestion-widget-<?php print $subquestion_id; ?>-current-value"
           name="subquestion-widget-<?php print $subquestion_id; ?>-current-value"
           value="<?php print $default_value_id; ?>">
    <div class="subquestion-widget-header">
      Choose an option below:
    </div>
    <ul class="widget-options">
      <?php  foreach($subquestion->field_drs_answers as $delta => $answer): ?>
        <?php $aid = $answer->id->value(); ?>
        <li id="answer-<?php print $aid; ?>" class="<?php print ($delta==0) ? ('original-'.$subquestion_id) : ''; ?>" >
          <input type="radio" id="<?php print $widget_id ?>-radio-<?php print $aid;?>"
                 name="<?php print $widget_id ?>"
            <?php if ($user_answer && $user_answer == $aid): ?> checked="checked" <?php endif; ?>
                 value="<?php print $aid; ?>">
          <label for="<?php print $widget_id ?>-radio-<?php print $aid;?>">
            <?php if($delta==0): ?>
              <span class="special-label">[Original text]</span>
            <?php endif; ?>
            <span id="<?php print $widget_id ?>-<?php print $aid;?>-value"><?php print $answer->title->value(); ?></span>
          </label>
        </li>

        <?php if($delta==0): ?>
          <li>
            <input type="radio" id="<?php print $widget_id ?>-radio-delete"
                   name="<?php print $widget_id ?>"
              <?php if ($user_answer && $user_answer == 'delete'): ?> checked="checked" <?php endif; ?>
                   value="delete">
            <label for="<?php print $widget_id ?>-radio-delete"><span class="special-label">[Delete text]</span> </label>
          </li>
        <?php endif; ?>

      <?php endforeach; ?>
    </ul>
    <div class="subquestion-widget-footer">
      <div class="reset-container">
        <button id="subquestion-widget-reset-btn-<?php print $subquestion_id; ?>" class="subquestion-widget-reset-btn pull-left" <?php if(!$user_answer): ?>disabled="disabled"<?php endif; ?> >Reset</button>
          <span style="display: none;" id="confirm-reset-container-<?php print $subquestion_id; ?>" class="confirm-reset-container">
            <button id="subquestion-widget-confirm-reset-btn-<?php print $subquestion_id; ?>" class="subquestion-widget-confirm-reset-btn" >Confirm Reset</button>
            <span class="reset-wrapper"><span class="confirm-reset-text" >Click to confirm reset.</span></span>
          </span>
      </div>
      <button id="subquestion-widget-accept-btn-<?php print $subquestion_id; ?>" class="subquestion-widget-accept-btn pull-right">Accept</button>
      <button id="subquestion-widget-cancel-btn-<?php print $subquestion_id; ?>" class="subquestion-widget-cancel-btn pull-right">Cancel</button>
    </div>
  </div>
</script>
