<?php
  $subquestion_id = $subquestion->id->value();
  $widget_id = 'subquestion-widget-'.$subquestion_id;

  // $answer_type could be 'not-answered', 'correct', 'incorrect'
  switch($answer_type){
    case 'not answered':
      $answer_class = 'skipped';
      $icon_alt_text = 'Skipped icon';
      break;
    case 'correct':
      $answer_class = 'correct';
      $icon_alt_text = 'Icon indicating you answered this question correctly';
      break;
    case 'incorrect':
      $answer_class = 'incorrect';
      $icon_alt_text = 'Icon indicating you answered this question incorrectly';
      break;
  }

  if ($deleted){
    $deleted_class .= ' deleted';
  }
?>
<span id="<?php print $widget_id; ?>" data-widget-id="<?php print $subquestion_id; ?>" class="subquestion-widget subquestion-review <?php print $answer_class . $deleted_class; ?>">
    <span id="selected-answer-<?php print $subquestion_id; ?>" class="selected-answer">
      <span class=" glyphicon icon-open" style='background-image: url("/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/img/icon_chevron.png");'></span>
      <img src="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/img/icon_flat_<?php print ($answer_class == 'skipped')? 'incorrect' : $answer_class; ?>.png" height="11" alt="<?php print $icon_alt_text; ?>" />
      <span class="user-answer <?php print $answer_class . $deleted_class; ?>"><?php print $user_answer; ?></span>
    </span>
</span>
<?php
// now we are defining the body of the widget but as we need to use some divs
// (and that wouldn't work well if we put them inside the inline subquestion-widget element)
// we define the structure here and let the js to place it in the html
?>
<script type="text/html" id="subquestion-widget-script-container-<?php print $subquestion_id; ?>">
  <div id="subquestion-widget-popup-<?php print $subquestion_id; ?>" class="subquestion-widget-popup" style="display: none;">
    <?php if($answer_class != 'correct'){ ?>
      <div class="subquestion-explanation-title"><?php echo t('Correct answer');?></div>
      <div class="subquestion-explanation"><?php print $correct_answer; ?></div>
    <?php } ?>
    <div class="subquestion-explanation-title">Explanation</div>
    <div class="subquestion-explanation <?php echo $answer_class;?>"><?php print $explanation; ?></div>
    <div class="subquestion-widget-footer">
      <button id="subquestion-widget-cancel-btn-<?php print $subquestion_id; ?>" class="subquestion-widget-cancel-btn">Close</button>
    </div>
  </div>
</script>