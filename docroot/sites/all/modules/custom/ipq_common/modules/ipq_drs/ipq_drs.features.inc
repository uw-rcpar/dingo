<?php
/**
 * @file
 * ipq_drs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ipq_drs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function ipq_drs_eck_bundle_info() {
  $items = array(
    'drs_answer_drs_answer' => array(
      'machine_name' => 'drs_answer_drs_answer',
      'entity_type' => 'drs_answer',
      'name' => 'drs_answer',
      'label' => 'DRS Answer',
      'config' => array(),
    ),
    'drs_subquestion_drs_subquestion' => array(
      'machine_name' => 'drs_subquestion_drs_subquestion',
      'entity_type' => 'drs_subquestion',
      'name' => 'drs_subquestion',
      'label' => 'DRS Subquestion',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function ipq_drs_eck_entity_type_info() {
  $items = array(
    'drs_answer' => array(
      'name' => 'drs_answer',
      'label' => 'DRS Answer',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
    'drs_subquestion' => array(
      'name' => 'drs_subquestion',
      'label' => 'DRS Subquestion',
      'properties' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function ipq_drs_node_info() {
  $items = array(
    'ipq_drs_question' => array(
      'name' => t('IPQ DRS 3.0 Question'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
