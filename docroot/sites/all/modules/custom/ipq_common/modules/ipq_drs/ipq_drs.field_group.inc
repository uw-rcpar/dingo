<?php
/**
 * @file
 * ipq_drs.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ipq_drs_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_delete_text|drs_subquestion|drs_subquestion|form';
  $field_group->group_name = 'group_delete_text';
  $field_group->entity_type = 'drs_subquestion';
  $field_group->bundle = 'drs_subquestion';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Delete Text Answer',
    'weight' => '3',
    'children' => array(
      0 => 'field_ipq_answer_explanation',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Delete Text Answer',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-delete-text field-group-fieldset',
        'description' => 'The answer option "[Delete Text]" is always available.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_delete_text|drs_subquestion|drs_subquestion|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Delete Text Answer');

  return $field_groups;
}
