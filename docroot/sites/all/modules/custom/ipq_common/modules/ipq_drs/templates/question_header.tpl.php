<?php if($state){ ?>
  <div class="question-header row <?php print $state->status?>">
    <div class="answer-section answer-status">
      <h3 class="review-tilte-header">Answer status:</h3>
      <span class="<?php print $state->status?>">
        <span  class="question-<?php print $state->status?>"></span>
        <?php print $state->status?>
      </span>
    </div>
    <div class="answer-section answer-score ">
      <h3 class="review-tilte-header">Subquestion score:</h3> 
      <div class="answer-content">
        <div id="exams-review-content" class="exam-ipq-links<?php if(exam_version_is_versioning_on()) { ?> exam-versions-exist<?php } ?>">
          <div class="drs-wrapper course-detail-wrapper col-md-3 col-sm-6 col-xs-6 section-1">
            <div class="radial-element" id="drs">
              <p class="element-value"> <?php print $state->percentaje ?> </p>
            </div>
          </div>
        </div>
        <div class="labels-content">
          <span class="question-percentaje"><?php print $state->percentaje?>%</span>
          <span class="question-count"><?php print $state->correct?> / <?php print $state->count?></span>
        </div>
      </div>
    </div>
  </div>
<?php } ?>