(function ($) {
  Drupal.behaviors.rcpar_ipq_drs_review = {

    displayWidget: function (widget, show, context) {
      var qid = this.getQuestionIdFromWidget(widget);
      if (show) {
        $('#subquestion-widget-popup-' + qid, context).show();
        $(widget).addClass('active');
      } else {
        $('#subquestion-widget-popup-' + qid, context).hide();
        $(widget).removeClass('active');
      }
    },

    getQuestionIdFromWidget: function (widget) {
      return $(widget).attr('data-widget-id');
    },

    attach: function (context, settings) {
      var behaviours = this;

      $('.subquestion-widget.subquestion-review', context).once('subquestion-widget', function () {
        var widget = this;
        var qid = behaviours.getQuestionIdFromWidget(widget);
        // now we load the body of the popup widget and place it inside the html
        var widget_body = $("#subquestion-widget-script-container-" + qid, context).html().trim();
        $('#subquestion-widget-' + qid, context).after(widget_body);
        $(this).click(function () {
          var visibility = $('#subquestion-widget-popup-' + qid, context).is(":hidden");
          // open the current one widget popup
          behaviours.displayWidget(widget, visibility, context);
          icon = $(this).children(".selected-answer").children('.icon-open');

          /*  Invert the icon class */
          if (icon.hasClass("icon-invert")) {
            icon.removeClass("icon-invert");
          } else {
            icon.addClass("icon-invert");
          }
        });

        $('#subquestion-widget-cancel-btn-' + qid, context).click(function () {
          behaviours.displayWidget(widget, false);
          return false;
        });
      });

    },
  };
}(jQuery));
