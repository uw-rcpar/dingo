(function ($, Drupal, window, document, undefined) {
    Drupal.behaviors.custom = {
      attach: function (context, settings) {
        // When a regular answer option is select as correct, make sure the 'delete text' option is unselected as correct
        $('.ipq-answers-radio', context).click(function(e) {
          $(this).closest('.ief-form').find('input.delete-is-correct').removeAttr('checked');
        });

        // When the 'delete text' option is selected as correct, make all of the regular answer options incorrect
        $('.delete-is-correct', context).click(function(e) {
          $(this).closest('.ief-form').find('input.ipq-answers-radio').removeAttr('checked');
        });

        // if there's no selected radio button, the correct answer if "delete question" so we manually set
        // the status of the corresponding radio button to selected
        if($( ".ipq-answers-radio:checked", context).length == 0){
          $('.delete-is-correct', context).prop('checked', true);
        }

      }
    };
})(jQuery, Drupal, this, this.document); //END - Closure