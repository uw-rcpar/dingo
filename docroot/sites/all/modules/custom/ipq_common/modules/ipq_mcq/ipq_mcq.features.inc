<?php
/**
 * @file
 * ipq_mcq.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ipq_mcq_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function ipq_mcq_eck_bundle_info() {
  $items = array(
    'ipq_answer_mcq' => array(
      'machine_name' => 'ipq_answer_mcq',
      'entity_type' => 'ipq_answer',
      'name' => 'mcq',
      'label' => 'MCQ',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function ipq_mcq_node_info() {
  $items = array(
    'ipq_mcq_question' => array(
      'name' => t('IPQ MCQ Question'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
