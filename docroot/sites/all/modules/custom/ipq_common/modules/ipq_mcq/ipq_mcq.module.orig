<?php
/**
 * @file
 * Code for the IPQ MCQ feature.
 */

include_once 'ipq_mcq.features.inc';

/**
 * Implements hook_eck_entity_label
 *
 * Returns the value that should be used for the label of an entities when viewed on ECK administrative screens such
 * as admin/structure/entity-type/ipq_answer/mcq
 */
function ipq_mcq_eck_entity_label($entity, $entity_id) {
  // Only attempt to affect the ipq_answer entity
  if ($entity->entityType()  == 'ipq_answer') {
    // Wrap for consistent access to entity properties and values
    $wrapper = entity_metadata_wrapper('ipq_answer', $entity);

    // For MCQ entities, use the 'answer text' field as the entity label
    if($wrapper->getBundle() == 'mcq') {      
      try{        
        return $wrapper->field_ipq_answer_text->value->value();
      } catch (Exception $exc) {
        watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
      }            
    }
  }
}

/**
 * Implements hook_entity_view_alter()
 *
 * Override output of answer entities completely for viewing on quizzes
 *
 * @param array $build
 * - Drupal renderable array to modify (or replace) that represents the output
 * @param string $type
 * - The entity type
 */
function ipq_mcq_entity_view_alter(&$build, $type) {
  // IPQ answer
  if($type == 'ipq_answer') {
    // Grab the entity and wrap it with metadata helper class
    $entity = $build['#entity'];
    $wrapper = entity_metadata_wrapper('ipq_answer', $entity);

    // Multiple Choice Question:
    if($wrapper->getBundle() == 'mcq') {
      $items = array();

      // Get the answer text for each answer
      foreach ($wrapper->field_ipq_answer_text as $value) {
        // @todo - Grab the real field value, was in a hurry. -jd
        $items[] = '<div> <input type="radio" /> ' . 'Answer Text' . '</div>';
      }

      // Override the output completely
      $build = array(
        '#markup' => implode("\n", $items),
      );
    }
  }
}

/**
 * Implements hook_inline_entity_form_table_fields_alter
 *
 * Used to override the fields displayed ief table displayed on the node edit form for this entity reference field
 */
function ipq_mcq_inline_entity_form_table_fields_alter(&$fields, $context) {
  // Redefine the table fields from scratch, the default just shows the id field.

  $fields = array(
    // Answer text field
    'field_ipq_answer_text' => array(
      'label'  => 'Question',
      'weight' => 1,
      'type'            => 'callback',
      'render_callback' => 'ipq_mcq_ief_answer_text_render'
    ),

    // 'Is Correct' field
    'field_ipq_answer_is_correct'  => array(
      'label'           => 'Is Correct',
      'weight'          => 5,

      // Use a dedicated render function to display this field on the admin screen.
      'type'            => 'callback',
      'render_callback' => 'ipq_mcq_ief_is_correct_render'
    ),
  );
}

/**
 * Implements hook_form_alter().
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function ipq_mcq_form_alter(&$form, &$form_state, $form_id) {
  if($form_id == 'ipq_mcq_question_node_form') {
    // we added the UI to allow users to select the correct answer
    // but that's not saved by ief, we need to do it manually
    $form['#submit'][] = 'ipq_mcq_submit';

    // custom fields to store correct answer data
    $form['possible_answers'] = array(
      '#prefix' => '<div id="possible-answers-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#tree' => true,
    );
    $form['possible_answers']['correct'] = array(
      '#type' => 'hidden'
    );
  }
}

/**
 * Custom submit function
 *  we are using it to save the is_correct field of the answers
 * @param $form
 * @param $form_state
 *
 */
function ipq_mcq_submit($form, &$form_state){
  $ef = reset($form_state['inline_entity_form']);
  foreach($ef['entities'] as &$e){
    if (isset($e['entity']->is_new) && $e['entity']->is_new){
      $entity_id = $e['entity']->temporary_id;
      $is_new = true;
    } else {
      $entity_id = $e['entity']->id;
      $is_new = false;
    }
    if ($form_state['values']['possible_answers']['correct'] == $entity_id){
      $new_is_correct_val = 1;

    } else {
      $new_is_correct_val = 0;
    }
    if ($e['entity']->field_ipq_answer_is_correct['und'][0]['value'] != $new_is_correct_val){
      $e['entity']->field_ipq_answer_is_correct['und'][0]['value'] = $new_is_correct_val;
      if ($is_new){
        $e['needs_save'] = true;
      } else {
        $e['entity']->save();
      }
    }
  }
}

/**
 * Implements hook_inline_entity_form_entity_form_alter()
 *
 * Perform alterations to the entity form before it's is included in the IEF widget.
 * This hook needs to be used for the entity form in the context of inline entity form - hook_form_alter won't work.
 *
 * @param $entity_form
 *   Nested array of form elements that comprise the entity form.
 * @param $form_state
 *   The form state of the parent form.
 */
function ipq_mcq_inline_entity_form_entity_form_alter(&$entity_form, &$form_state) {
  if ($entity_form['#entity_type'] == 'ipq_answer') {

  }
}


/**
 * Custom render callback for field_ipq_answer_is_correct when being displayed in the ief table during editing.
 * note that we are allowing users to select the correct answer using a custom UI
 *
 * @param $entity_type
 * @param $entity
 * @return array
 * - Drupal renderable array
 */
<<<<<<< HEAD
function ipq_mcq_ief_is_correct_render($entity_type, $entity) {
  // Check for the value of our boolean 'is correct' field and format it nicely.
  if(!empty($entity->field_ipq_answer_is_correct[LANGUAGE_NONE])) {
    $output = $entity->field_ipq_answer_is_correct[LANGUAGE_NONE][0]['value'] ? true : false;
  }
  else {
    // If there is no value at all, assume 'false'.
    $output = false;
  }

  // we need a way to identify the answer choose as correct
  // see IPQAnswerEntityInlineEntityFormController::entityForm to understand temporary_id parameter
  if ($entity->is_new){
    $radio_entity_id = $entity->temporary_id;
  } else {
    $radio_entity_id = $entity->id;
  }

  $render = array(
    '#type' => 'radio',
    '#return_value' => $radio_entity_id,
    '#value' => $output,
    '#name' => 'possible_answers[correct]',
    '#parents' => array('answers'),
    '#attributes' => array('class' => array('ipq-answers-radio')),
  );
  return $render;
}

/**
 * Custom render callback for ipq_mcq_ief_answer_text_render when being displayed in the ief table during editing.
 *
 * @param $entity_type
 * @param $entity
 * @return array
 * - Drupal renderable array
 */
function ipq_mcq_ief_answer_text_render($entity_type, $entity){

  if(!empty($entity->field_ipq_answer_text[LANGUAGE_NONE])) {
    $output = $entity->field_ipq_answer_text[LANGUAGE_NONE][0]['value'];
  }
  else {
    $output = '';
  }

  $render = array('#markup' => $output);
  return $render;
=======
function ipq_mcq_ief_is_correct_render($entity_type, $entity) {  
  try{
    $wrapper = entity_metadata_wrapper('ipq_answer', $entity);
    //default value
    $output = 'No';
    // Check for the value of our boolean 'is correct' field and format it nicely.
    if(isset($wrapper->field_ipq_answer_is_correct)) {
      $output = $wrapper->field_ipq_answer_is_correct->value() ? 'Yes' : 'No';
    }
  } catch (Exception $exc) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
  }           
  
  return array('#markup' => $output);   
>>>>>>> 50fc7c0a692700bb92a926bd89a44a619bce6aae
}



/**
 * Implements hook_entity_info_alter
 */
function ipq_mcq_entity_info_alter(&$entity_info) {
  // Declare the class that can override default methods of inline entity form, like form generation, validation, etc.
  // This should only be needed when it's default behaviors need a very drastic change - hopefully implementations of
  // hook_inline_entity_form_entity_form_alter() can handle most of this.

  $entity_info['ipq_answer']['inline entity form'] = array(
    'controller' => 'IPQAnswerEntityInlineEntityFormController',
  );
}


class IPQAnswerEntityInlineEntityFormController extends EckInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    $entity_form = parent::entityForm($entity_form, $form_state);

    // we don't want users to be able to select the correct answer using this form
    // (we are adding the UI for that inside this same module)
    $entity_form['field_ipq_answer_is_correct']['#access'] = false;

    // since we don't have an id for the question yet
    // and that we need to have a way to identify the rows in the table to set the current correct answer
    // we add a temporary id (note that it won't be stored in the db)
    if (isset($entity_form['#entity']->is_new) && $entity_form['#entity']->is_new){
      $entity_form['#entity']->temporary_id = uniqid();
    }
    
    return $entity_form;
  }

}

