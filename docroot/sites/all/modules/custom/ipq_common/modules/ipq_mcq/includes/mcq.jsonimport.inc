<?php

/**
 * MCQ importer form for JSON files
 */
function ipq_mcq_import_form($form, &$form_state) {
  /////////////// File upload /////////////
  $form['upload_fieldset'] = array(
    '#title' => 'Upload an import file manually',
    '#type' => 'fieldset',
  );

  $form['upload_fieldset']['file'] = array(
    '#title' => 'JSON file',
    '#type' => 'file',
  );

  // Submit button
  $form['upload_fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#submit' => array('ipq_mcq_import_form_submit'),
  );

  ///////////////// API testing utility /////////////
  $form['api_test'] = array(
    '#title' => 'API Testing',
    '#type' => 'fieldset',
  );
  $form['api_test']['info'] = array(
    '#markup' => '<p>Current day: ' . date('mdY', mktime(0,0,0)) . '</p>' .
      '<p>Last successful automated import day: ' . date('mdY', variable_get('uworld_last_import', mktime(0,0,0,1,29,2019))) . '</p>',
  );
  $form['api_test']['from'] = array(
    '#title' => 'From date',
    '#type' => 'textfield',
    '#description' => 'Specifies the beginning of the date range for which to request updated MCQ data. <br>
      Format: <em>mmddyyyy</em>. Example: 01172019',
  );
  $form['api_test']['to'] = array(
    '#title' => 'To date',
    '#type' => 'textfield',
    '#description' => 'Specifies the end of the date range for which to request updated MCQ data. <br>
      Format: <em>mmddyyyy</em>. Example: 01182019',
  );
  $form['api_test']['save'] = array(
    '#title' => 'Save data from API',
    '#type' => 'checkbox',
    '#description' => 'When checked, data retrieved from the API will actually be saved to the database. Leave 
    unchecked to simply view the response from the API without further action.',
  );
  $form['api_test']['api_submit'] = array(
    '#type' => 'submit',
    '#value' => 'API Test',
    '#submit' => array('ipq_mcq_api_test_form_submit'),
  );

  // Build a log table
  $header = array('Question ID', 'Title', 'Import Status', 'Updated on');

  // Build a pager query
  $query = db_select('ipq_mcq_import_log', 'i')->extend('PagerDefault');
  $query->join('node', 'n', 'i.ipq_question_id = n.nid');
  $query->fields('i', array('ipq_question_id','updated','status'));
  $query->fields('n', array('title'));

  // Change the number of rows with the limit() call.
  $limit = !empty($_GET['limit']) ? $_GET['limit'] : 20;
  $result = $query
    ->limit($limit)
    ->orderBy('i.id', 'DESC')
    ->execute();

  // Go through the query results and build a table
  $rows = array();
  foreach ($result as $row) {
    // BUild the table row and add it
    $table_row = array(
      l($row->ipq_question_id, "node/$row->ipq_question_id/edit", array('attributes' => ['target' => '_blank'])),
      $row->title,
      $row->status ? 'Success' : 'Failure',
      format_date($row->updated),
    );

    $rows[] = $table_row;
  }

  // Create a render array ($build) which will be themed as a table with a pager.
  $build['pager_table'] = array(
    '#caption' => 'Import log',
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There were no log records found.'),
  );

  // Attach the pager theme.
  $build['pager_pager'] = array('#theme' => 'pager');

  // Add the log table back into the render array
  $form['log'] = $build;

  return $form;
}

/**
 * Form submit function for ipq_mcq_import_form
 * Imports a JSON file of MCQ data
 */
function ipq_mcq_import_form_submit(&$form, &$form_state) {
  // Get file data
  $file_contents = file_get_contents($form_state['values']['file']['tmp_name']);
  $json_data = json_decode($file_contents);

  // Process data
  $t = new RCPARIpqMcqJsonImport($json_data);
  $t->process();

  // Set feedback message
  drupal_set_message($t->getProcessingMessage());
}

/**
 * Form submit function for API testing from ipq_mcq_import_form
 * Gets data from the UWorld API and optionally saves it to the database as well.
 */
function ipq_mcq_api_test_form_submit(&$form, &$form_state) {
  $from = $form_state['values']['from'];
  $to = $form_state['values']['to'];
  $save = $form_state['values']['save'];

  // Get data from the API
  $return = ipq_mcq_api_make_request($from, $to);
  $json_data = json_decode($return);
  drupal_set_message('<strong>API Response:</strong><pre>' . print_r($json_data, TRUE) . '</pre><hr>');

  // Save question nodes using this data, if requested
  if($save) {
    // Process data
    $t = new RCPARIpqMcqJsonImport($json_data);
    $t->process();

    // Set feedback message
    drupal_set_message($t->getProcessingMessage());
  }
}

/**
 * Make a request to the UWorld API for MCQ question content updates.
 *
 * @param int $from
 * - Beginning of the data range to request data for. In the format of 'mmddyyyy'
 * @param int $to
 * - Beginning of the data range to request data for. In the format of 'mmddyyyy'
 * @return string
 * - If successful, returns an unencoded string of JSON data from the API. If not, returns the output from
 * curl_error().
 */
function ipq_mcq_api_make_request($from, $to) {
  // The API endpoint URL
  $url = variable_get('uworld_api_url', 'https://api.uworld.com') . '/service/GetQuestions';

  // Data parameters
  $data = json_encode(
    array(
      'from_mmddyyyy' => $from,
      'to_mmddyyyy' => $to
    )
  );

  // Set HTTP headers and method
  $method = 'POST';
  $headers = array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data),
    'x-api-key: ' . variable_get('uworld_api_key', '46f6a85d-80c4-4587-bf81-c48cf7a52a80'),
  );

  // create curl resource
  $ch = curl_init($url);

  // Set url, http method, and headers
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

  // Be permissive with SSL and return the transfer as a string
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  // $output contains the output string
  $output = curl_exec($ch);

  // Catch errors
  if ($error = curl_error($ch)) {
    return $error;
  }

  // Close curl resource to free up system resources
  curl_close($ch);

  return $output;
}

/**
 * Class RCPARIpqMcqJsonImport
 * - Helper class for JSON imports
 */
class RCPARIpqMcqJsonImport {
  // Import data
  protected $jsonData;

  // Logging data
  protected $successes = array();
  protected $failures = array();
  protected $messages = array();

  /**
   * RCPARIpqMcqJsonImport constructor.
   * @param array $json_data
   * - Provide an array of data to import. May also be provided after constructor
   * via setJsonData()
   */
  function __construct($json_data = NULL) {
    if (!empty($json_data)) {
      $this->setJsonData($json_data);
    }
  }

  /**
   * Processes all import data
   */
  public function process() {
    // Get import data
    $json_data = $this->getJsonData();

    if (!empty($json_data)) {
      foreach ($json_data as $question) {
        $nid = $question->roger_question_id;
        try {
          $node_wrapper = $this->processQuestion($question);
          $node_wrapper->save();

          // Log that an update has occurred successfully
          $this->log($nid, 1);

          $this->successes[] = $nid;
        }
        catch (Exception $e) {
          // Log a message (if this is a manual import)
          $this->messages[] = 'Failed to import question ID: ' . $question->roger_question_id . '. Message: ' . $e->getMessage();

          // Log that an update failed to process successfully
          $this->log($nid, 0);

          $this->failures[] = $nid;
        }
      }
    }
  }

  /**
   * Get messages and information about processing status of each item.
   * @return string
   */
  public function getProcessingMessage() {
    $msg = 'Total records attempted: ' . (count($this->successes) + count($this->failures));
    $msg .= '<br><br>Sucesses:<br>' . implode('<br>', $this->successes);
    $msg .= '<br><br>Failures:<br>' . implode('<br>', $this->failures);
    $msg .= '<br><br>Messages:<br>' . implode('<br>', $this->messages);

    return $msg;
  }

  /**
   * Process an individual question
   * @param array $question
   * - Array of JSON data to process from import file
   * @return EntityMetadataWrapper
   * @throws Exception
   */
  protected function processQuestion($question) {
    // ChangeTypeID = 1 - Import all _except_ the status
    // ChangeTypeID = 2 - Save status only
    // ChangeTypeID = 3 - Save all
    // "In other situations where only question text changed slightly or answer text changed slightly without modifying
    // explanation to new standard then we will  not send those questions instead ask Jay to modify in your system as
    // well (assuming this is very rare instance)."

    // Ensure we have a valid change type ID of 1, 2, or 3.
    $change_type_id = $this->getAndValidateChangeTypeId($question);

    // Load the node and reflect the "changed" date. If we're modifying the question at all, we always reflect changed date.
    $node = node_load($question->roger_question_id);
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $node->changed = $question->date_modified; // entity wrapper doesn't like changing this

    // Change type 2 - we'll be changing status only
    if($change_type_id == 2) {
      $node->status = $question->published;
      return $node_wrapper;
    }

    // Change type 1 and 3 - we'll save all available data, except for 1 we'll ignore status
    // Set title, question text, answer explanation text, and published status
    $node_wrapper->title->set($question->question_title);
    $node_wrapper->field_ipq_question_text->set(array('value' => $question->question_text, 'format' => 'full_html'));
    $node_wrapper->field_answer_explanation->set(array('value' => $question->explanation_text, 'format' => 'full_html'));
    if($change_type_id == 3) {
      $node->status = $question->published;
    }

    // We may need to re-order the answers, so we'll load them all up into an array keyed by ID first
    $mcq_answers = $this->getAnswersById($node_wrapper);

    // Compare the size of the answers provided vs the ones we have. They need to match
    if (count($mcq_answers) != count($question->answer_choice_list)) {
      throw new Exception('Number of answers provided by import data does not match question in database');
    }

    // Now we'll iterate through the supplied answer data and apply values to the answers in our array
    foreach ($question->answer_choice_list as $answer) {
      $answer_entity = entity_load_single('ipq_answer', $answer->roger_answer_id);
      $answer_wrapper = entity_metadata_wrapper('ipq_answer', $answer_entity);

      // Set answer text
      $answer_wrapper->field_ipq_answer_text->set(array('value' => $answer->answer_text, 'format' => 'full_html'));

      // If this is the correct answer...
      if ($question->correct_answer == $answer->roger_answer_id) {
        $answer_wrapper->field_ipq_answer_is_correct->set(1);
      }
      // This is NOT the correct answer
      else {
        $answer_wrapper->field_ipq_answer_is_correct->set(0);
      }

      // Save the answer
      $answer_wrapper->save();

      // Now place this answer into the correct sequence
      $node_wrapper->field_ipq_mcq_answer[$answer->sequence_no]->set($answer->roger_answer_id);
    }

    return $node_wrapper;
  }

  /**
   * Ensure we have a change type ID that we can handle
   * @param object $question
   * - JSON data for a single question
   * @throws Exception
   * @return int
   * - Return the change type ID
   */
  protected function getAndValidateChangeTypeId($question) {
    $change_type_id = $question->change_type_id;

    if (!in_array($change_type_id, array(1, 2, 3))) {
      throw new Exception('Encountered invalid change type ID.');
    }

    return $change_type_id;
  }

  /**
   * Write a record to the import log that this node was updated
   * @param int $nid
   * - Node ID of the question to log
   * @param int $success
   * - 1 for a successful update, 0 for failure.
   */
  protected function log($nid, $success) {
    $record = array(
      'ipq_question_id' => $nid,
      'updated' => REQUEST_TIME,
      'status' => $success,
    );
    drupal_write_record('ipq_mcq_import_log', $record);
  }

  /**
   * Get an array of MCQ answers keyed by entity ID.
   * @param EntityMetadataWrapper $node_wrapper
   * @return EntityDrupalWrapper[]
   */
  protected function getAnswersById($node_wrapper) {
    $mcq_answers = array();
    foreach ($node_wrapper->field_ipq_mcq_answer as $mcq_answer) {
      $mcq_answers[$mcq_answer->id->value()] = $mcq_answer;
    }
    return $mcq_answers;
  }

  /**
   * Gets JSON data to be used for the import.
   * @return array
   */
  public function getJsonData() {
    return $this->jsonData;
  }

  /**
   * @param array $jsonData
   */
  public function setJsonData($jsonData): void {
    $this->jsonData = $jsonData;
  }

  /**
   * Gets successfully imported node ids.
   * @return int[]
   * - An array of node IDs that were successfully processed.
   */
  public function getSuccesses() {
    return $this->successes;
  }

  /**
   * Gets node ids that failed to import.
   * @return int[]
   * - An array of node IDs that failed to process.
   */
  public function getFailures() {
    return $this->failures;
  }
}
