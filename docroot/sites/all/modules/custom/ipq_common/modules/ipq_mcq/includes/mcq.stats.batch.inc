<?php

/**
 * Menu callback for admin/settings/rcpar/ipq/mcq-stats and form builder for MCQ answer stats.
 */
function ipq_mcq_stats_batch_form($form, &$form_state) {
  $form['help'] = array(
    '#markup' => '<p>Executing this form will calculate MCQ answer statistics for all MCQ questions in the system.<br>
    These statistics indicate the number of users that chose each answer, in each question.</p>',
  );

  $form['resume'] = array(
    '#type' => 'checkbox',
    '#title' => 'Attempt resume',
    '#description' => 'By checking this option, only the questions who have not been updated within the configured 
    time range will be updated. The current time range setting is: ' . variable_get('ipq_mcq_stats_update_time', '-1 month')
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Build MCQ answer stats',
  );

  return $form;
}

function ipq_mcq_stats_batch_form_submit($form, &$form_state) {
  $resume = $form_state['values']['resume'];
  ipq_mcq_stats_batch($resume);
}

/**
 * The batch callback.
 */
function ipq_mcq_stats_batch($resume = FALSE) {
  $batch = array(
    'operations' => array(),
    'finished' => 'ipq_mcq_stats_batch_finished',
    'title' => t('MCQ Answer Stats Batch'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'ipq_mcq') . '/includes/mcq.stats.batch.inc'
  );

  // Query for every MCQ question in the system.
  $sql = "
    SELECT n.nid FROM node n
    LEFT JOIN ipq_mcq_answer_stats s ON n.nid = s.ipq_question_id
    WHERE n.type IN (SELECT type FROM ipq_question_types)";

  // With the resume option, we only get stats on questions that have been updated prior to the acceptable range,
  // or have never been updated.
  if($resume) {
    $timestring = variable_get('ipq_mcq_stats_update_time', '-1 month');
    $sql .= " AND (s.updated < :time OR s.updated IS NULL)";
    $question_ids_to_update = db_query($sql,[':time' => strtotime($timestring)])->fetchCol();
  }
  // Without resume, we'll be updated every question
  else {
    $question_ids_to_update = db_query($sql)->fetchCol();
  }

  // Split the operations into X number of nodes per operation.
  $job_chunks = array_chunk($question_ids_to_update, $limit_per_run = variable_get('ipq_mcq_stats_limit_per_run', 3));
  foreach ($job_chunks as $nids) {
    $batch['operations'][] = array('ipq_mcq_stats_batch_process', array($nids));
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/rcpar/ipq/mcq-stats'); // The path to redirect to when done.
  }
}

/**
 * Process a set of MCQ questions to generate answer stats
 * @param int[] $question_ids_to_update
 * - An array of question IDs
 * @param array $context
 * - Batch API data.
 */
function ipq_mcq_stats_batch_process($question_ids_to_update, &$context) {
  foreach ($question_ids_to_update as $nid) {
    if (RCPARIpqMcqQuestionStats::buildAnswerStats($nid)) {
      // Store some result data just to show messaging at the end
      $context['results']['success_nids'][$nid] = $nid;
    }
    else {
      // Store some result data just to show messaging at the end
      $context['results']['fail_nids'][$nid] = $nid;
    }
  }
}


/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Some stored data about success/failure.
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function ipq_mcq_stats_batch_finished($success, $results, $operations){
  if ($success) {
    drupal_set_message('Updated stats sucessfully for ' . sizeof($results['success_nids']) . ' questions. Failed on: ' . sizeof($results['fail_nids']) . ' questions.');

    // Show all failed nids, if any
    if (sizeof($results['fail_nids']) > 0) {
      drupal_set_message("Failed NIDS: <pre>" . print_r($results['fail_nids'], TRUE) . ' </pre>');
    }
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}
