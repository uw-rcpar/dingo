<?php

/**
 * Menu callback for admin/settings/rcpar/ipq/question-map and form builder for MCQ uworld questions.
 */
function ipq_mcq_uworld_batch_form($form, &$form_state) {

  $form['help'] = array(
    '#markup' => '<p>Upload the information followng the standard format file:  '.l('template','/admin/settings/rcpar/ipq/template.csv').'</p>',
  );

  $form['upload'] = array(
    '#name' => 'files[' . implode('_', $element['#parents']) . ']',
    '#type' => 'file',
    '#title' => t('Choose a file'),
    '#title_display' => 'invisible',
    '#size' => 22,
    '#theme_wrappers' => array(),
    '#weight' => -10,
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Build MCQ Questions',
  );

  return $form;
}

function ipq_mcq_uworld_batch_form_submit($form, &$form_state) {
  $file = fopen($form_state['values']['upload'][0]['tmp_name'], "r");
  $header = TRUE;
  while (($data = fgetcsv($file)) !== FALSE) {
    if ($header) {
      $header = FALSE;
      continue;
    }
    $operations[] = array(
      'ipq_mcq_uworld_batch_operation',
      array(
        'uworld_id' => $data[0],
        'questions_count' => $data[1]
      )
    );
  }

  fclose($file);

  $batch = array(
    'operations' => $operations,
    'finished' => 'ipq_mcq_uworld_batch_finished',
    'title' => t('Creating MCQ Questions'),
    'init_message' => t('Inicializing CSV import questionss.'),
    'progress_message' => t('Processed @current out of @total questions created.'),
    'error_message' => t('No questions were updated'),
    'file' => drupal_get_path('module', 'ipq_mcq') . '/includes/mcq.uworld.batch.inc',
  );

  batch_set($batch);
}

/**
 * 
 * @param int $uworld_id
 *    Value provided on the csv
 * @param type $questions_count
 * - number of answers the question will generate
 * @param array $context
 * - Batch API data.
 */
function ipq_mcq_uworld_batch_operation($uworld_id, $questions_count, &$context) {
  $node = new stdClass();
  $node->title = "UWorld Question - $uworld_id";
  $node->type = "ipq_mcq_question";
  node_object_prepare($node);
  $node->language = LANGUAGE_NONE;
  $node->uid = 1;
  $node->status = 0;
  $node->promote = 0;
  $node->comment = 0;
  try {
    for ($index = 1; $index <= $questions_count; $index++) {
      $answer = "Answer {$index}";
      $correct = ($index == 1);
      $entity_type = 'ipq_answer';
      $entity = entity_create($entity_type, ['type' => 'mcq']);
      $wrapper = entity_metadata_wrapper($entity_type, $entity);
      $wrapper->field_ipq_answer_text->set(['format' => 'full_html', 'value' => $answer]);
      $wrapper->field_ipq_answer_explanation->set(['format' => 'full_html', 'value' => $answer]);
      $wrapper->field_ipq_answer_is_correct->set($correct);
      $wrapper->save();
      $node->field_ipq_mcq_answer[LANGUAGE_NONE][] = ['target_id' => $wrapper->getIdentifier()];
      $answer_ids[$wrapper->getIdentifier()] = $wrapper->getIdentifier();
    }
  }
  catch (Exception $ex) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . 'ERROR: ' . $ex->getMessage());
  }

  $node = node_submit($node); // Prepare node for saving
  node_save($node);
  $context['results']['csv'][] = ['roger_id' => $node->nid, 'uworld_id' => $uworld_id, 'answer_ids' => $answer_ids];
}

/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Some stored data about success/failure.
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function ipq_mcq_uworld_batch_finished($success, $results, $operations) {
  if ($success) {
    $data[] = implode(',', ['Rcpar Question ID', 'uWorld Question ID', 'Answer IDs']);
    foreach ($results['csv'] as $line) {
      $data[] = implode(',', [$line['roger_id'], $line['uworld_id'], implode('|', $line['answer_ids'])]);
    }
    $content = implode("\n\t", $data);
    $file = file_save_data($content, 'public://questionsMap.csv', FILE_EXISTS_RENAME);

    $url = file_create_url($file->uri);

    drupal_set_message('Created sucessfully for ' . sizeof($results['csv']) . ' questions. To download click here ' . l($file->filename, $url));
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}


//call back for the downloaded csv file
function ipq_mcq_uworld_template_csv() {
  drupal_add_http_header('Content-Type', 'text/csv; utf-8');
  drupal_add_http_header('Content-Disposition', 'attachment; filename = template.csv');
  $fh = fopen('php://output', 'w');
  fputcsv($fh, array('UWorld Question ID', 'Number of answer options'));
  fputcsv($fh, array('', ''));
  //close the stream
  fclose($fh);
  drupal_exit();  
}
