<?php
$img_path = drupal_get_path('module', 'ipq_common');
$letter_ordering = range("a", "z");
$question_wrapper = entity_metadata_wrapper('node', $question);

?>

<div class="question-text">
  <?php
  $field = field_view_field('node', $question, 'field_ipq_question_text', array('label' => 'hidden'));
  print drupal_render($field);
  ?>
</div>

<div class="user-answer mcq-user-answer">
  <div class="answer-header" id="answer-heading">Your answer was <?php if ($session_data['is_correct']) { ?>
      <span class="right">correct</span><?php } else { ?><span class="wrong">incorrect</span><?php } ?>:
  </div>
  <ul class="answer-options" aria-labelledby="answer-heading">
    <?php

    // build array containing answer delta, answer text, answer status, answer stats count (precentage rate) and explanations
    $answer_info = array();
    $i = 0;
    foreach ($question_wrapper->field_ipq_mcq_answer as $index => $answer) {
      $t = $answer->field_ipq_answer_text->value();

      $answer_info[$i]['answer_delta'] = $answer->getIdentifier();
      $answer_info[$i]['answer_text'] = $t['value'];

      // Get the answer stats count (precentage rate).
      if (is_object($answer_stats)) {
        $answer_info[$i]['answer_stats'] = $answer_stats->getAnswerRate($answer->id->value());
      }

      // determine the answer status and answer explanations
      $answer_status = '';
      // $session_data['user_input'] = user's selection
      if ($session_data['user_input'] == $answer->id->value()) {
        // Get incorrect answer explanation
        $incorrect_explanation = $answer->field_ipq_answer_explanation->value();
        if (isset($incorrect_explanation['safe_value'])) {
          $incorrect_explanation = $incorrect_explanation['safe_value'];
        }
        // Get answer status
        if ($session_data['is_correct']) {
          $answer_status = 'correct';
        }
        else {
          $answer_status = 'incorrect';
        }
      }
      if ($answer->field_ipq_answer_is_correct->value()) {
        $answer_status = 'correct';
        $correct_explanation = $answer->field_ipq_answer_explanation->value();
        if (isset($correct_explanation['safe_value'])) {
          $correct_explanation = $correct_explanation['safe_value'];
        }
      }

      $answer_info[$i]['answer_status'] = $answer_status;
      $answer_info[$i]['correct_explanation'] = $correct_explanation;
      $answer_info[$i]['incorrect_explanation'] = $incorrect_explanation;
      $i++;
    }

    // Alphabetize the display order if answer explanation field is empty
    if (empty($question_wrapper->field_answer_explanation->value())) {
      // Loop through $answer_info array and add a key/value containing answer text stripped of html and leading spaces.
      foreach ($answer_info as $key => $row) {
        $no_html_tags = strip_tags($row['answer_text']);
        $no_leading_spaces = preg_replace('~\x{00a0}~siu', '', $no_html_tags);
        $answer_info[$key]['answer_text_plain'] = $no_leading_spaces;
      }
      // Sort the $answer_info array alphabetically by answer_text_plain.
      usort($answer_info, function ($x, $y) {
        if ($x['answer_text_plain'] == $y['answer_text_plain']) {
          return 0;
        }
        return ($x['answer_text_plain'] < $y['answer_text_plain']) ? -1 : 1;
      });
    }

    $letter = 'A';
    foreach ($answer_info as $index => $answer):
      // Get the answer rate percentage
      $answer_rate = '';
      $answer_text = trim($answer['answer_text']);
      if (isset($answer_info[$index]['answer_stats']) && !empty($answer_info[$index]['answer_stats'])) {
        $answer_text = ipq_mcq_answer_percentage_insertion($answer['answer_text'],$answer_info[$index]['answer_stats']);
      }
      ?>
      <li class="answer-option <?php print $answer['answer_status']; ?>">
        <?php
        if ($answer['answer_status'] == 'incorrect') {
          ?>
          <span class="indicator"><i class="fa fa-close"></i><span class="radio-bg checked"></span></span>
        <?php } elseif ($answer['answer_status'] == 'correct') { ?>
          <span class="indicator"><i class="fa fa-check"></i><span class="radio-bg<?php if ($session_data['is_correct']) { ?> checked <?php } ?>"></span></span>
        <?php } else { ?>
          <span class="indicator"><span class="radio-bg"></span></span>
        <?php } ?>
        <?php print '<strong class="letter">' . $letter . '.</strong> ' . $answer_text; ?>
      </li>
      <?php
      $letter++;
    endforeach; ?>
  </ul>

  <!-- Answer explanation area -->
  <div class="mcq-solution answer-explanation-area">
    <h3>Explanation</h3>
    <div class="explanation-container clearfix">
      <?php
      // if we have anything in the answer_explanation field, display it.
      if (!empty($question_wrapper->field_answer_explanation->value())) {
        print $question_wrapper->field_answer_explanation->value()['safe_value'];
      }
      else {
        // Otherwise, use our old answer explanations.

        // Build an array of information about the explanations
        $explanation_info = array();
        $i = 0;
        $letter = 'A';
        foreach ($answer_info as $index => $info) {
          // Answer delta
          $explanation_info[$i]['delta'] = $info['answer_delta'];
          // Answer display order
          $explanation_info[$i]['display_order'] = $letter;
          // Is the answer the correct one
          $explanation_info[$i]['is_correct_option'] = FALSE;
          if ($info['answer_status'] == 'correct') {
            $explanation_info[$i]['is_correct_option'] = TRUE;
          }
          // Get the text of the answer explanation
          $explanation_info[$i]['explanation'] = ipq_mcq_get_answer_explanation($question, $info['answer_delta']);
          // Increment our counters
          $i++;
          $letter++;
        }
        // Get the status of the explanations (are they each unique, partially unique, or identical)
        $explanation_status = ipq_mcq_answer_explanation_status($explanation_info);

        // Display answer explanation.
        echo ipq_mcq_answer_explanations($explanation_info, $explanation_status);
      }
      ?>

    </div>
  </div>
</div>
<div class="divider"></div>
