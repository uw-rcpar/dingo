(function ($) {
  Drupal.behaviors.rcpar_ipq_drs_display = {
    get_timer: function(){
      // Stop the timer
      Drupal.behaviors.rcpar_elapsedtimer.stop_elapsed();
      // Get the current timer 
      timer = Drupal.behaviors.rcpar_elapsedtimer.get_elapsed();
      // Get the total of seconds
      var seconds = (timer.timerHoursElapsed*3600)+(timer.timerMinutesElapsed*60)+timer.timerSecondsElapsed;
      $('.digital-timer-spent .seconds-elapsed').html(seconds);
      if (seconds < 2) {
        $('.digital-timer-spent .seconds-label').html('Second');
      }
      else {
        $('.digital-timer-spent .seconds-label').html('Seconds');
      }
    },
    attach: function (context, settings) {
      var hide_timer = $('#hide_timer');
      var behaviours = this;
      if($('.show-timer-show').length > 0 && hide_timer.val() != "true"){
        seconds = $('.show-timer-show .seconds-elapsed').html();
        $('.digital-timer-spent .seconds-elapsed').html(seconds);
        Drupal.behaviors.rcpar_elapsedtimer.set_elapsed(seconds);
      }
      // Only when the show correct answer has been clicked and the timer is hidden.
      if($('.show-timer-show').length == 0 && hide_timer.val() == "true"){
        seconds = $(".time-elapsed").attr("spent-time");
        // If Seconds has been saved in the temporary element.
        if(typeof seconds !== "undefined"){
          Drupal.behaviors.rcpar_elapsedtimer.set_elapsed(seconds);
        }
        
      }
      // Pause the timer the when the "yes" button is clicked in the modal
      $('.btn-confirm-show-answer', context).click(function() {
        behaviours.get_timer();
      });

      $('.show-correct-answer', context).click(function() {
        // If the user has chosen to never see the "show correct answer feature" modal,
        // Stop the timer.
        if (RCPAR.modals.userHasSeenModals(['show_correct_answer_feature'])) {
          behaviours.get_timer();
        }
        // If hide timer is disable the spent time will be visible.
        if( hide_timer.val() != "true") {
          $('.show-timer-').removeClass("hidden");
          $('.show-timer-').addClass('show-timer-show');
        }
        else{
          // Stop the timer
          Drupal.behaviors.rcpar_elapsedtimer.stop_elapsed();
          // Get the current timer 
          timer = Drupal.behaviors.rcpar_elapsedtimer.get_elapsed();
          // Get the total of seconds
          var seconds = (timer.timerHoursElapsed*3600)+(timer.timerMinutesElapsed*60)+timer.timerSecondsElapsed;
          // Temporary save
          $(".time-elapsed").attr("spent-time", seconds); 
        }
      });
      $('input:radio', context).click(function() {
        // We mark the question as modified for the auto saving feature
        Drupal.behaviors.rcpar_ipq_session_display.needsSaving = true;
      });
    },
  };
}(jQuery));
