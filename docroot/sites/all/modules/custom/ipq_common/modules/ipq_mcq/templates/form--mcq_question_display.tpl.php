<?php
$session = $form['session']['#value'];
$question = $form['question']['#value'];

$question_text = field_view_field('node', $question, 'field_ipq_question_text', array('label' => 'hidden'));
$answers = field_view_field('node', $question, 'field_ipq_mcq_answer');
$explain_answers = $_SESSION['ipq_session']['session_config']['explain_answers'];
$hide_timer = $_SESSION['ipq_session']['session_config']['hide_timer'];
// Determine if the user has already seen the correct answer
$answer_shown = FALSE;
if (is_array($session['session_config']['answers_shown']) && in_array($question->nid, $session['session_config']['answers_shown'])) {
  $answer_shown = TRUE;
}
$seconds = $_SESSION['ipq_session']['score_as_you_go_info'][$question->nid]['time'];
?>
  <!-- Timer area -->
<div class="time-spent-block show-timer-<?php echo ($answer_shown && !$hide_timer) ? "show" : " hidden"; ?>">
  <div class="digital-timer-spent">
    <div class="seconds-container time">
      <span class="amount seconds-elapsed"><?php echo $seconds; ?></span>
      <span class="seconds-label"><?php echo format_plural($seconds, 'Second', 'Seconds'); ?></span>
    </div>
    <div class="time-spent-label">Time Spent</div>
  </div>
  <div class="time-icon pull-right"><img src="/<?php print drupal_get_path('module','ipq_common'); ?>/css/img/icon-time-spent.svg" width="24" /></div>
</div>
<!-- Question area -->
<div class="question question-mcq">
  <!-- Question text -->
  <div class="quesion-text">
    <?php print drupal_render($question_text); ?>
  </div>

  <!-- Answer options -->
  <div class="answers">
    <?php print drupal_render_children($form); ?>
  </div>

  <!--  Show correct answer button -->
  <?php if ($explain_answers) { ?> <div class="show-answer-btn-wrapper"><a class="show-correct-answer" href="javascript:;">Show Correct Answer</a></div> <?php } ?>

  <!-- Answer explanation area -->
  <div class="mcq-solution hidden answer-explanation-area">

    <h3>Explanation</h3>

    <?php
    $question_wrapper = entity_metadata_wrapper('node', $question);
    $correct_answer = NULL;
    // If the new answer explanation field is empty, use our old explanation fields
    if (empty(trim($question_wrapper->field_answer_explanation->value()['safe_value']))) {
      // Find the correct answer's id
      foreach ($question_wrapper->field_ipq_mcq_answer as $index => $answer) {
        if ($answer->field_ipq_answer_is_correct->value() == "1") {
          $correct_answer = $answer->id->value();
        }
      }
        ?>

        <!-- Answer explanation text -->
        <div class="explanation-container clearfix" id="answer-explanations">
          <?php

          // Build an array of information about the explanations
          $explanation_info = array();
          $i = 0;
          $letter = 'A';
          foreach ($form['possible_answers']['#options'] as $delta => $text) {
            // Answer delta
            $explanation_info[$i]['delta'] = $delta;
            // Answer display order
            $explanation_info[$i]['display_order'] = $letter;
            // Is the answer the correct one
            $explanation_info[$i]['is_correct_option'] = FALSE;
            if ($delta == $correct_answer) {
              $explanation_info[$i]['is_correct_option'] = TRUE;
            }
            // Get the text of the answer explanation
            $explanation_info[$i]['explanation'] = ipq_mcq_get_answer_explanation($question, $delta);
            // Increment our counters
            $i++;
            $letter++;
          }

          // Get the status of the explanations (are they each unique, partially unique, or identical)
          $explanation_status = ipq_mcq_answer_explanation_status($explanation_info);

          // Display answer explanation.
          echo ipq_mcq_answer_explanations($explanation_info, $explanation_status);

          ?>
        </div>

      <?php
    // Otherwise use the new explanation field
    }
    else {
      foreach ($question_wrapper->field_ipq_mcq_answer as $index => $answer) {
        // Store the answer id of the correct answer when we come across it
        if ($answer->field_ipq_answer_is_correct->value() == "1") {
          $correct_answer = $answer->id->value();
          break;
        }
      }
      $explanation = $question_wrapper->field_answer_explanation->value()['safe_value'];
       ?>
        <!-- Answer explanation text -->
        <div class="explanation-container" id="singular-explanation">
          <?php echo $explanation; ?>
        </div>
      <?php
    }

    ?>

    <!-- Data for JS to read the correct answer ID -->
    <input type="hidden" id="mcq-correct-answer" value="<?php print $correct_answer; ?>">

    <!-- Data for JS to know if answer has already been shown -->
    <input type="hidden" id="mcq-correct-answer-shown" value="<?php print $answer_shown; ?>">
  </div>


</div>

