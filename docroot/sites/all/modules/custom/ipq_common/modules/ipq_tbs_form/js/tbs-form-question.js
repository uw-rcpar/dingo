(function ($) {
  Drupal.behaviors.rcpar_ipq_tbs_form_display = {

    updateValues: function(hot){
      // Get the JSON data from handsontable and put this, as a string, into #handsontable_raw_input so that it can be sent to the back-end.
      var data = hot.getData();
      $('#hansontable_raw_input').val(JSON.stringify(data));

      // Wrapping this in a try/catch because this variable may not be availabe when reviewing or previewing questions and might throw an error.
      try {
        // We mark the question as modified for the auto saving feature
        Drupal.behaviors.rcpar_ipq_session_display.needsSaving = true;
      }
      catch (e) {}
    },

    attach: function (context, settings) {
      var behaviours = this;
      var lastRowEdited = null;
      var blurHasHappened = false;
      var firstRun = true;
      var parseFields = false;
      var data = [];
      // we just want to do all this once
      $('#data .hot', context).once('processed', function(){
        var rowListing = Drupal.settings.ipq_tbs_form.inputs;
        // rowListing is a map of the fields with custom labels which are displayed over the form image. This is what the rowListing object looks like, as an example:
        //  {
        //    'agi':
        //      [
        //        {'id': "23", value: "some value in a field"},
        //        {'id': "24", value: "some text"}
        //      ],
        //    'income':
        //      [
        //        {'id': "7", value: "438.28"},
        //        {'id': "8", value: "foobar"}
        //      ]
        //  }
        //
        //  'agi' and 'income' are custom labels for different sections within a form. You can have as many different sections as you'd like.
        //
        //  Section labels are used to avoid namespace collision. For example if both 'income' and 'agi' need to have a row labeled 'total'.
        //
        //  The "id" refers to a row label. The id could be anything, such as the word 'total'.
        //
        //  The "value" is the text that should be displayed in the HTML field.
        //
        var rowMapping = {};
        var tables = false;
        var currentRow, section, dataRow, rowNum, resultsObj;
        var i = 1;
        var cells_info = [];


        disableScrolling = function() {
          var x=window.scrollX;
          var y=window.scrollY;
          window.onscroll=function(){window.scrollTo(x, y);};
        };


        enableScrolling = function() {
          window.onscroll=function(){};
        };

        // Loop through sections such as "agi" or "income"
        for (section in rowListing) {
          // Loop through rows with custom labels in sections. Each row will have an 'id', which could be a word like "total" or "8a"
          for (rowNum in rowListing[section]) {
            var input_definition = rowListing[section][rowNum];
            // cells_info with an array with individual row information, such as "id" and "value"
            cells_info.push(rowListing[section][rowNum]);
            var id = input_definition.id;
            var value = null;
            if(input_definition.value){
              value = input_definition.value;
            }
            // rowMapping is an object containing the index of rows. For example rowMapping["total"] might store the index 4, which means it is the 4th row. It is greater than 0.
            rowMapping[id] = i;
            dataRow = {};

            dataRow["label"] = id;
            dataRow["answer"] = value;
            dataRow["section"] = section;
            dataRow["format_type"] = "";
            dataRow["format"] = "";
            try {
              dataRow["format_type"] = input_definition.cell_cfg.format_type;
              dataRow["format"] = input_definition.cell_cfg.format;
            }  catch(e) {
            }

            data.push(dataRow);
            // Auto-generate HTML fields with IDs such as "row-[customlabel]". For example, "row-totals"
            // (in case those fields doesn't exists)
            if ($('#row-' + id).length == 0){
              $('.user-input .' + section, context).append( $('<input>').attr('value', value).attr('type', 'text').attr('id', 'row-' + id).addClass('row-form').addClass('row-' + id) );
            }

            // There are some strange bugs associated with the focus on this particular question type
            // to work around them, we are handling the focus ourselves here
            $(':focusable', context).once('focusable').on('keydown', function(evt){
              var list = $(':focusable').filter(":visible");
              var index = list.index(this);
              if (evt.key == 'Tab' ){
                if (evt.shiftKey){
                  $(list.get(index-1)).focus();
                }
                else {
                  $(list.get(index + 1)).focus();

                }
                return false;
              }
            });

            if (value !== null) {
              if (typeof value == 'string' && value.indexOf("sum") > -1) {
                // If this field contains auto-summed values, make it read-only
                $('.user-input .' + section, context).find('.row-' + id).attr('readonly','readonly');
              }
            }
            i++;
          }
        }

        $('.taxform-container', context).blur(function(evt) {
          // This is a hacky solution/band-aid for a bug in handsontable.
          // When a cell is selected (using tables.selectCell(row, col)),
          // this causes the scrollbar to jump to the top of the page.
          // To prevent this, I am temporarily disabling scrolling.
          disableScrolling();
        });

        // This timeoutVar will be used for the setTimeout below
        var timeoutVar;

        // Every time a user types something into an HTML field, we need to transfer that text over to handsontable so that it can be formatted correctly (for example, adding commas/decimal points or summing values).
        $('.user-input .row-form', context).blur(function(evt) {

          // This cleartimeout is needed so that the text that users type in is not processsed too often.  The text is sent for processing every 900 milliseconds. If a user types in something new before 900 milliseconds have passed, then the timeout resets and start counting down from 900 again.  This way, text is not processed until there is a pause in typing.
          if (lastRowEdited == $(this).attr('id')) {
            clearTimeout(timeoutVar);
          }

          // Make sure the setTimeout fires for this cell even if the user starts typing in a different cell within the next 900 milliseconds
          lastRowEdited = $(this).attr('id');

          var elem = $(this);
        
          // Check to see if any formulas are being used, such as "=sum()"
          if (elem.val().indexOf('=') > -1 && elem.val().indexOf('(') > -1 && elem.val().indexOf(')') > -1) {
            parseFields(elem);
          }
          else {
            timeoutVar = setTimeout(function() {
              // Find the id of the HTML field. For example, the ID could be "row-totals". In this case, "totals" is the name of the custom row label.
              var row = elem.attr('id').split('-')[1];

              // Pass the text from the HTML field to the corresponding handsontable cell.
              //
              // rowMapping[row]-1 will be the index of the row. For example, index 5 would mean it is the 5th row.
              // The reason why we are subtracting 1 is because the handsontable indexes start at 0, whereas handsontable row labels start at 1 (such as A1, B1, etc).

              // We might be losing focus on one of our formula cells
              // on that case, we don't want to update the handsontable with the displayed value (eg: $10.00)
              // because that would break the formula (the cell instead of say something like =sum(a1,a2) now would say
              // $10.00) with is a static value
              var is_formula_cell = false;
              var current_val = tables.getDataAtRowProp(rowMapping[row]-1, 'answer');
              var is_string = typeof current_val  === 'string' || current_val instanceof String;
              if (is_string && current_val.indexOf('=') > -1 && current_val.indexOf('(') > -1 && current_val.indexOf(')') > -1) {
                is_formula_cell = true;
              }
              if (!is_formula_cell){
                // Not a formula cell, we can update the value on handsontable table
                tables.setDataAtRowProp((rowMapping[row]-1), 'answer', clearValue(elem.val()));
              }
            }, 50);
          }
          // end setTimeout
        });

        // Create a new handsontable
        tables = new Handsontable($('#data .hot', context).get(0), {
          data: data,
          // Allow for formulas such as "=sum()"
          formulas: true,
          // Stretch table to 100% width
          stretchH: "all",
          // In this use case, we just need 1 handsontable column.
          maxCols: 1,
          contextMenu: false,
          renderAllRows: true,

          cells: function (row, col, prop) {
            if (col == 1) {
              if (!isNaN(row)) {
                return cells_info[row].cell_cfg;
              }
              else {
                return null;
              }
            }
            else {
              return null;
            }
          },
          // This function runs after a change is made to a handsontable cell.
          afterChange: function (changes, source) {
            if (!firstRun) {
              try {
                var row = changes[0][0]; // row

                // Selecting a cell seems to fix rendering problems.
                // Sometimes the table won't render all the HTML rows (<tr>'s)
                // it should have, making it impossible to get the value of
                // some cells. This little hack seems to fix that problem.

                // Commenting this out because it stopped working. Instead we are using
                // 'renderAllRows' in the handsontable options.
                //tables.selectCell(row, 1);


                // In case you're wondering why we're looping through TR tags,
                // it's because handsontable does not provide a function to
                // easily get the rendered value of an auto-summed cell.
                //
                // For example, an auto-summed cell's actual value is =sum(8,9),
                // but the cell's rendered value is the result of that sum.
                // The rendered result would look something like "$25.00".
                //
                // When you use the getDataAtRowProp() or getCell() functions
                // that handsontable provides, they only give you the
                // raw formula (=sum(a,b)), and not the rendered result ($25).
                //
                // We need to transfer the rendered result back to the HTML
                // input fields in the TBS Form.  In order to do that, we
                // need to get the innerHTML of the cell TD, which will have
                // the format that we need.
                //
                // The 1st TD in each row contains the cell's label/ID
                // The 2nd TD in each row should contain the cell's value
                // The 3rd TD in each row contains the cell's section/group
                // The 4th TD in each row contains the format (such as $0,0.00)

                // Loop through handsontable td elements and copy their values over to the form HTML fields.
                $('#data .htCore tbody tr').each(function (i) {
                  // Get the section name
                  var id = $(this).find('td:nth-child(1)').text();
                  var section = $(this).find('td:nth-child(3)').text();
                  var cellType = $(this).find('td:nth-child(4)').text();
                  var format = $(this).find('td:nth-child(5)').text();

                  // Get the handsontable cell value.
                  var cellText = $(this).find('td:nth-child(2)').text();

                  if (cellText.indexOf("VALUE") > -1 && source != 'change_not') {
                    var elem = $('#row-' + id);
                    // Find the id of the HTML field. For example, the ID could be "row-totals". In this case, "totals" is the name of the custom row label.
                    var row = id;

                    // Pass the text from the HTML field to the corresponding handsontable cell.
                    //
                    // rowMapping[row]-1 will be the index of the row. For example, index 5 would mean it is the 5th row.
                    // The reason why we are subtracting 1 is because the handsontable indexes start at 0, whereas handsontable row labels start at 1 (such as A1, B1, etc).

                    // We might be losing focus on one of our formula cells
                    // on that case, we don't want to update the handsontable with the displayed value (eg: $10.00)
                    // because that would break the formula (the cell instead of say something like =sum(a1,a2) now would say
                    // $10.00) with is a static value
                    var is_formula_cell = false;
                    var current_val = tables.getDataAtRowProp(rowMapping[row] - 1, 'answer');
                    var is_string = typeof current_val === 'string' || current_val instanceof String;
                    if (is_string && current_val.indexOf('=') > -1 && current_val.indexOf('(') > -1 && current_val.indexOf(')') > -1) {
                      is_formula_cell = true;
                    }
                    if (!is_formula_cell) {
                      // Not a formula cell, we can update the value on handsontable table
                      tables.setDataAtRowProp((rowMapping[row] - 1), 'answer', clearValue(elem.val()));
                    }
                  }
                  else {
                    // if cellType is true, that means I have to format the cell text according to its format.
                    // this usually only applies to auto-summed cells
                    if (cellType.length > 0) {

                      // Replace ! with =
                      // The reason for substituting = with ! before the first load is so that the cell does not display "ERROR" because it could not calculate the value of the other cells used in its =sum() function
                      if (cellText.indexOf('!') > -1 && cellText.indexOf('(') > -1 && cellText.indexOf(')') > -1) {
                        if (parseFields !== false) {
                          // The firstRun variable is important because it prevents the code in handsontable's afterChange callback from running too soon
                          firstRun = false;
                          parseFields($(this));
                          return;
                        }
                      }
                      // Check to see if format contains a percent sign, and temporarily convert it to a $ sign so that numeralJS can parse it
                      var tempFormat = false;
                      if (format) {
                        if (format.indexOf('%') > -1) {
                          var tempFormat = format.replace("%", "$");
                        }
                      }
                      // Check to see if the cell has a number value
                      if (cellType == "currency" || cellType == "numeric") {
                        // This will format the value of the cell to something like "$1,234.56" or "1234.56" depending on what the variable "format" contains
                        if (tempFormat) {
                          // If the format previously had a % sign in it, change the $ sign back to a % sign for display purposes
                          cellText = numeral(cellText).format(tempFormat).replace("$", "%");
                        }
                        else {
                          cellText = numeral(cellText).format(format);
                        }
                      }

                    }
                    // Transfer the handsontable cell value back to the corresponding HTML field. Handsontable row 0 will correspond to $('.row-form').eq(0).
                    $('.taxform-container.user-input .row-form.row-' + id).val(cellText);
                  }

                });
              }
              catch (e) {
              }
            }

            behaviours.updateValues(this);
            if (firstRun) {
              // Here we save the HOT input default value to be able to know if the user changed it later
              // (used to see if the user skipped the question or tried to answer it)

              // Note. It seems firstRun variable is always true.
              // LoadData condition is added to make  hansontable_raw_input_default and hansontable_raw_input
              // equal only at the begining. This will allow to compare differences between them on ipq_tbs_form_ipq_question_input_check
              if (source == 'loadData') {
                $('#hansontable_raw_input_default').val($('#hansontable_raw_input').val());
              }

            }
            setTimeout(function () {
              enableScrolling();
            }, 600);
          } // end afterChange callback
        }); // end Handsontable initialization


        parseFields = function (elem) {
          if (elem.val().indexOf('!') > -1 && elem.val().indexOf('(') > -1 && elem.val().indexOf(')') > -1) {
            elem.val(elem.val().replace('!', '='));
          }

          var row = elem.attr('id').split('-')[1];

          // Parse the formula. We just want the value inside the parenthesis. For example, if the value is "=sum(total, subtotal)" we want to map the labels "total" and "subtotal" to the handsontable row labels, which will look like "A1" or "B1" and so on.
          var currentValue = elem.val().split('(');

          // The formulaName could be "=sum"
          var formulaName = currentValue[0];

          // cells will be the values inside the parenthesis. For example "total,subtotal".
          var cells = currentValue[1].split(')')[0];
          // Check to see if the value contains a comma or a colon. If it's a comma, we're just comparing two cells. If it's a colon, we are processing a range of cells.
          if (cells.indexOf(',') > -1) {
            var separator = ',';
            var separated = cells.split(',');
          }
          else if (cells.indexOf(':') > -1) {
            var separator = ':';
            var separated = cells.split(':');
          }
          else if (cells.indexOf('-') > -1) {
            var separator = '-';
            var separated = cells.split('-');
          }

          // separated will be an array that looks something like ["total", "subtotal"]
          if (separated) {
            i = 0;
            // Loop through the cell labels
            for (i; i < separated.length; i++) {
              var calculationCell = separated[i];

              // As an example, rowMapping["total"] will be an index greater than 0.
              if (rowMapping[calculationCell] !== undefined) {
                // separated[i] will be equal to a handsontable row label, such as "B1". In this case, we are only using column B.
                separated[i] = 'B' + rowMapping[calculationCell];
              }
            }

            // Put the formula back together again, but this time, substitute the custom row labels with handsontable row labels. So, for example, "=sum(totals, subtotal)" becomes something like "=sum(A1,C5)" so that handsontable can process it.
            var formula = formulaName + '(' + separated.join(separator) + ')';
            // Put the parsed formula into the corresponding handsontable cell.
            tables.setDataAtRowProp((rowMapping[row] - 1), 'answer', formula);
          }
        };

        clearValue = function (value) {
          // Remove symbols and letters from the value in order to avoid different issues
          var clearVal = value.replace(/[^\d.-]/g, '');
          clearVal = clearVal == "" ? 0 : clearVal;
          return clearVal;
        };

        // On page load, loop through the HTML fields and check to see if any fields are auto-summed (i.e. contain "=sum(7a,8a)", for example)
        $('.user-input .row-form').each(function () {
          var elem = $(this);
          // Check to see if any formulas are being used, such as "=sum()"
          // Note that at this point, the = sign might have not been replaced by the ! yet
          // (like on the question review page)
          if ((elem.val().indexOf('=') > -1 || elem.val().indexOf('!') > -1) && elem.val().indexOf('(') > -1 && elem.val().indexOf(')') > -1) {
            // Make sure parseFields function is defined
            if (parseFields !== false) {
              // The firstRun variable is important because it prevents the code in handsontable's afterChange callback from running too soon
              firstRun = false;  
              parseFields($(this));

              // Here we save the HOT input default value to be able to know if the user changed it later
              // (used to see if the user skipped the question or tried to answer it)
              $('#hansontable_raw_input_default').val($('#hansontable_raw_input').val());
              
            }
          }
        });
      });

      $('.handsontable', context).click(function() {
        firstRun = false;
      });

    },
  };
}(jQuery));

