<div class="taxform-container user-input form-type-<?php print $form_type; ?>">
  <div
    class="taxform"
    style="
      background: url(<?php print $img_info['url']; ?>);
      width: <?php print $img_info['width']; ?>px;
      height: <?php print $img_info['height']; ?>px;
    "
  ></div>

  <div class="hot-container hidden">
    <div id="data">
      <div class="hot handsontable"></div>
    </div>
  </div>

  <div class="fields">
    <?php foreach($input_containers as $container): ?>
      <div class="<?php print $container; ?>">
      </div>
    <?php endforeach; ?>
  </div>

</div>


<div id="ipq-view-solution" class="modal form-completion-solution form-type-<?php print $form_type; ?>" data-backdrop="false">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="termsLabel" class="modal-title">
        View Solution
      </h3>
    </div><!-- /.modal-header -->
    <div class="modal-body">


      <div class="taxform-container">
        <div class="taxform" style="
          background: url(<?php print $img_info['url']; ?>);
          width: <?php print $img_info['width']; ?>px;
          height: <?php print $img_info['height']; ?>px;"></div>
        <div class="fields">
          <?php foreach($rows_config_correct as $container => $cells): ?>
            <div class="<?php print $container; ?>">
              <?php foreach($cells as $cell): ?>
                <input readonly type="text" class="row-form row-<?php print $cell['id']; ?>" value="<?php print $cell['value']; ?>">
              <?php endforeach; ?>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
      <?php 
      $question_wrapper = entity_metadata_wrapper('node', $question);
      $explanation = $question_wrapper->field_answer_explanation->value();
      if(isset($explanation['safe_value'])){
        $explanation = $explanation['safe_value'];
      }
      else {
        $explanation = $explanation['value'];
      }
      
      if ($explanation) {
      
      ?>
        <div class="form-answer-explanation-heading">Answer Explanation:</div>
        <div class="form-answer-explanation"><?php print $explanation; ?></div>
      <?php } ?>
      

    </div>
    <div class="modal-footer">
      <div class="score-control pull-left"><input id="ipq-view-solution-cb" type="checkbox" name="view-solution-mcq" value="1" <?php print $_SESSION['ipq_session']['session_config']['view_solutions'] ? 'checked' : ''; ?> > <label for="ipq-view-solution-cb">View Solutions</label></div>
      <button type="button" class="try-again-btn btn btn-default">Try Again</button>
      <button type="button" class="continue-btn btn btn-success">Continue</button>
    </div>
  </div><!-- /.modal-content -->

</div><!-- /.modal -->
