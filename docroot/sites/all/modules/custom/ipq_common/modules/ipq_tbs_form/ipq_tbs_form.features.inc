<?php
/**
 * @file
 * ipq_tbs_form.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ipq_tbs_form_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ipq_tbs_form_node_info() {
  $items = array(
    'ipq_tbs_form_question' => array(
      'name' => t('IPQ TBS 2.5 Form Completion'),
      'base' => 'node_content',
      'description' => t('TBS 2.5 Form Completion Question Type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
