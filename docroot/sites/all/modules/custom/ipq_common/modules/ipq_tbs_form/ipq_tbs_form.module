<?php
/**
 * @file
 * Code for the IPQ TBS 2.5 - Form Completion feature.
 */

include_once 'ipq_tbs_form.features.inc';


/**
 * Implements hook_theme().
 */
function ipq_tbs_form_theme() {
  return array(
    'ipq_tbs_form_question_display' => array(
      'template' => 'templates/ipq_tbs_form_question_display',
      'variables' => array(),
    ),
    'ipq_tbs_form_question_review'=> array(
      'template' => 'templates/ipq_tbs_form_question_review',
    ),
  );
}

/**
 * Implements hook_field_widget_form_alter().
 */
function ipq_tbs_form_field_widget_form_alter(&$element, &$form_state, $context) {
    $field = $context['field'];
    if (isset($field['field_name']) && ('field_ipq_form_display_def' == $field['field_name'])) {
      $element['form']['field_ipq_form_display_def']['#suffix'] = '<div>'.ipq_tbs_form_get_input_help().'<div>';
    }
}

/**
 * Implements hook_form_FORM_ID_alter(). 
 * Specifically we are only looking for ipq_common_settings_form so we can add some basic settings
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function ipq_tbs_form_ipq_common_settings_form_alter(&$form, &$form_state, $form_id) {
	
    $form['tbs_fc_settings'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Quiz - TBS Form Completion settings'),
    );

    $form['tbs_fc_settings']['ipq_tbs_form_question_max_questions_per_session'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of TBS Form Completion questions per session'),
      '#default_value' => variable_get('ipq_tbs_form_question_max_questions_per_session', 15),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );

  $form['tbs_fc_settings']['ipq_tbs_form_question_recommended_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum recommended time per TBS Form Completion question - current exam version'),
    '#default_value' => variable_get('ipq_tbs_form_question_recommended_time', '15 minutes'),
    '#size' => 55,
    '#maxlength' => 55,
    '#required' => TRUE,
  );

  $form['tbs_fc_settings']['ipq_tbs_form_question_recommended_time_upcoming'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum recommended time per TBS Form Completion question - upcoming exam version'),
    '#default_value' => variable_get('ipq_tbs_form_question_recommended_time_upcoming', '15 minutes'),
    '#size' => 55,
    '#maxlength' => 55,
    '#required' => TRUE,
  );

    $form['tbs_fc_exam_settings'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Exam - TBS Form Completion settings'),
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_exam_weight'] = array(
      '#type' => 'textfield',
      '#title' => t('Weight (order) of the TBS Form Completion questions inside the exams.'),
      '#default_value' => variable_get('ipq_tbs_form_exam_weight', 1),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_question_max_questions_per_aud_exam_session'] = array(
      '#type' => 'textfield',
      '#title' => t('AUD - Maximum number of TBS Form Completion questions per Exam session'),
      '#default_value' => variable_get('ipq_tbs_form_question_max_questions_per_aud_exam_session', 30),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_question_sets_per_aud_exam_session'] = array(
      '#type' => 'textfield',
      '#title' => t('AUD - Total testlets'),
      '#default_value' => variable_get('ipq_tbs_form_question_sets_per_aud_exam_session', 3),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_question_max_questions_per_bec_exam_session'] = array(
      '#type' => 'textfield',
      '#title' => t('BEC - Maximum number of TBS Form Completion questions per Exam session'),
      '#default_value' => variable_get('ipq_tbs_form_question_max_questions_per_bec_exam_session', 24),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_question_sets_per_bec_exam_session'] = array(
      '#type' => 'textfield',
      '#title' => t('BEC - Total testlets'),
      '#default_value' => variable_get('ipq_tbs_form_question_sets_per_bec_exam_session', 3),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_question_max_questions_per_far_exam_session'] = array(
      '#type' => 'textfield',
      '#title' => t('FAR - Maximum number of TBS Form Completion questions per Exam session'),
      '#default_value' => variable_get('ipq_tbs_form_question_max_questions_per_far_exam_session', 30),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_question_sets_per_far_exam_session'] = array(
      '#type' => 'textfield',
      '#title' => t('FAR - Total testlets'),
      '#default_value' => variable_get('ipq_tbs_form_question_sets_per_far_exam_session', 3),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_question_max_questions_per_reg_exam_session'] = array(
      '#type' => 'textfield',
      '#title' => t('REG - Maximum number of TBS Form Completion questions per Exam session'),
      '#default_value' => variable_get('ipq_tbs_form_question_max_questions_per_reg_exam_session', 24),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
    $form['tbs_fc_exam_settings']['ipq_tbs_form_question_sets_per_reg_exam_session'] = array(
      '#type' => 'textfield',
      '#title' => t('REG - Total testlets'),
      '#default_value' => variable_get('ipq_tbs_form_question_sets_per_reg_exam_session', 3),
      '#size' => 2,
      '#maxlength' => 2,
      '#required' => TRUE,
    );
	
}

/**
 * Implements ipq_question_form_display_alter hook (custom hook)
 * here we specify the fields to be shown on the session quiz
 * @param $form
 * - This variable is just a portion of the larger form array built in ipq_common ipq_common_session_navigation_form
 *   function
 * @param $form_state
 */
function ipq_tbs_form_ipq_question_form_display_alter(&$form, $form_state){
  // we can get the question and the session info from the form_state variable
  $session = $form_state['build_info']['args'][0];
  $question = $form_state['build_info']['args'][1];

  // Make sure we only alter the display for TBS 2.5 questions.
  if($question->type != 'ipq_tbs_form_question') {
    return;
  }

  $wrapper = entity_metadata_wrapper('node', $question);

  // exists if the user already tried to answer this questions
  $previous_values = null;
  if(isset($form_state['storage']['previous_value'])){
    $previous_values_tmp = json_decode($form_state['storage']['previous_value'], true);
    // ok, we have the previous values, but it will be handy to have them in a hash
    // where the id is the key
    $previous_values = array();
    foreach($previous_values_tmp as $key_val){
      $previous_values[$key_val[0]] = $key_val[1];
    }
  }

  $img_info = array();
  try{
    $img = $wrapper->field_ipq_form_template->value();

    $img_info['width'] = $img['width'];
    $img_info['height'] = $img['height'];
    $img_info['url'] = file_create_url($img['uri']);

    // determine the type of form being used
    $form_type = $wrapper->field_form_type->value();


    $formula_str = $wrapper->field_ipq_form_display_def->value();
    // we trim the formula, to remove empty lines at the beginning and the end
    // (those will cause new rows to be added)


    $rows_config = ipq_tbs_form_parse_display_definition($formula_str, $previous_values);

    $answers_str = $wrapper->field_ipq_form_answer_def->value();
    $answers = ipq_tbs_form_parse_answers($answers_str);
    $answer_values = array();

    // answers array, includes the actual answer and a explanation for the answer
    foreach($answers as $answer_id => $answer_value){
      $answer_values[$answer_id] = $answer_value['answer'];
    }

    // and a version with the correct answers
    $rows_config_correct_values = ipq_tbs_form_parse_display_definition($formula_str, $answer_values);

  } catch (Exception $exc) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
  }

  // now, lets parse the display definition formula

  $form['question_display'] = array(
    '#markup' => theme('ipq_tbs_form_question_display',
      array(
        'question' => $question,
        'img_info' => $img_info,
        'form_type' => $form_type,
        'input_containers' => array_keys($rows_config),
        'rows_config_correct' => $rows_config_correct_values,
      )
    ),
  );

  $form['hansontable_raw_input'] = array(
    '#type' => 'hidden',
    '#default_value' => '',
    '#attributes' => array(
      'id' => 'hansontable_raw_input'
    )
  );

  // Here we are going to store the default value, because we need to compare the user value with it to detect
  // if the question was answered or skipped
  $form['hansontable_raw_input_default'] = array(
    '#type' => 'hidden',
    '#default_value' => '',
    '#attributes' => array(
      'id' => 'hansontable_raw_input_default'
    )
  );

  drupal_add_js(array('ipq_tbs_form' => array('inputs' => $rows_config ) ), 'setting');

  drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/handsontable.full.js', array('group' => JS_DEFAULT, 'type' => 'file'));
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/handsontable.full.min.css', array('group' => CSS_DEFAULT, 'type' => 'file'));

  // we need to include the handsontable speadsheet generic code
  drupal_add_js(drupal_get_path('module','ipq_common') . '/js/spreadsheet.inc.js');
  // and the spreadsheet css
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/spreadsheet.css', array('group' => CSS_DEFAULT, 'type' => 'file'));

  drupal_add_js(drupal_get_path('module','ipq_common') . '/js/ipq-hot-general-settings.js');
  drupal_add_js(drupal_get_path('module','ipq_tbs_form') . '/js/tbs-form-question.js');

  drupal_add_css(drupal_get_path('module','ipq_tbs_form') . '/css/ipq-tbs-form-question.css');

  return $form;
}

function ipq_tbs_form_get_input_help(){
  $help_text = '';

  // TBS Form cell label help
  $help_text .= '<div>Custom Cell Label Example:</div>';
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label Here[Form Group Name Here] | :string</b>';
  $help_text .= '</div>';

  // auto-sum help
  $help_text .= '<div>Getting the sum of two cells:</div>';
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label Name[Form Group Name] | :numeric[=sum(Other Cell Label Here, Another Cell Label Here)],format=\'0.00\' (format is optional)</b>';
  $help_text .= '</div>';
  $help_text .= 'OR';
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label Name[Form Group Name] | :currency[=sum(Other Cell Label Here, Another Cell Label Here)],format=\'$0,0.00\' (format is optional)</b>';
  $help_text .= '</div><br />';

  // auto-sum help (range summing)
  $help_text .= '<div>Getting the sum of a range of cells:</div>';
  $help_text .= '<b>Cell Label Name[Form Group Name] | :numeric[=sum(Other Cell Label Here:Another Cell Label Here)],format=\'0.00\' (format is optional)</b>';
  $help_text .= '</div>';
  $help_text .= 'OR';
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label Name[Form Group Name] | :currency[=sum(Other Cell Label Here:Another Cell Label Here)],format=\'$0,0.00\' (format is optional)</b>';
  $help_text .= '</div><br />';

  // auto-sum help (subtraction)
  $help_text .= '<div>Subtracting one cell from another:</div>';
  $help_text .= '<div>';
  $help_text .= '<b>:numeric[=sum(CellNameHere-CellNameHere)],format=\'0.00\' (format is optional)</b>';
  $help_text .= '</div>';
  $help_text .= 'OR';
  $help_text .= '<div>';
  $help_text .= '<b>:currency[=sum(CellNameHere-CellNameHere)],format=\'$0,0.00\' (format is optional)</b>';
  $help_text .= '</div><br />';

  // auto-sum help (subtraction and addition)
  $help_text .= '<div>Subtracting AND adding:</div>';
  $help_text .= '<div>';
  $help_text .= '<b>Subtract cell 2 from cell 1 and add cell 3</b><br/>';
  $help_text .= '<b>:numeric[=sum(CellNameHere-CellNameHere,CellNameHere)],format=\'0.00\' (format is optional)</b>';
  $help_text .= '</div>';
  $help_text .= 'OR';
  $help_text .= '<div>';
  $help_text .= '<b>:currency[=sum(CellNameHere-CellNameHere,CellNameHere)],format=\'$0,0.00\' (format is optional)</b>';
  $help_text .= '</div><br />';

  // currency help
  $help_text .= '<div>Static Cells:</div>';
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label[Form Group Name] | :static[Cell Text],bold,italic,underline,bg_col=#ff0000,fg_col=#00ff00,width=\'20\'</b>
    <br>(all styles are optional, also static values might be entered directly if no styling is required, also notice that width is in pixels and it will only work on the top columns)
  ';
  $help_text .= '</div>';

  $help_text .= '<div>Available inputs:</div>';

  // string help
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label Name[Form Group Name] | :string</b>';
  $help_text .= '</div>';

  // numeric help
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label Name[Form Group Name] | :numeric[value],format=\'0.00\'</b> (value and format are optionals you can just use :numeric)';
  $help_text .= '</div>';

  // date help
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label Name[Form Group Name] | :date[value],format=\'MM/DD/YYYY\'</b>';
  $help_text .= '</div>';

  // currency help
  $help_text .= '<div>';
  $help_text .= '<b>Cell Label Name[Form Group Name] | :currency[value],format=\'$0,0.00\',language=\'en\'</b> (value and other settings are optionals you can just use :numeric and will be formated as $0,0.00)';
  $help_text .= '</div>';

  $help_text .= '<br>';

  return $help_text;
}

/**
 * Implements hook_ipq_input_check (custom hook)
 * This function should check the user input and return an array with the correct percentage achieved by the user
 * for a particular question
 *
 * @param $form_state
 */
function ipq_tbs_form_ipq_question_input_check($form_state){
  $session = $form_state['build_info']['args'][0];
  $question = $form_state['build_info']['args'][1];

  // we want to react only when it's a question of TBS 2.5 type
  if ($question->type == 'ipq_tbs_form_question'){
    $answer_is_correct = 0;
    $percent_correct = 0;
    $processed_user_input = '';
    $skipped = true;
    $score = 0;

    // The hansontable_raw_input_default var contains the initial status of the question when it's ready
    // to be answered (we are going to use that var to check if the user skipped the question)
    // but that might include a previous answer (the user might be returning to this question)
    // if that's the case, we shouldn't mark it as skipped
    // to workaround that, we can look at ipq_saved_session_data to see if the user already
    // answered this question
    $old_sd = db_select('ipq_saved_session_data', 'sd')
      ->fields('sd')
      ->condition('ipq_saved_sessions_id', $session['id'], '=')
      ->condition('ipq_question_id', $question->nid, '=')
      ->execute()
      ->fetchAssoc();
    if ($old_sd['status'] != 2){
      $skipped = false;
    }

    // we need to get the user input and have it in a format that we can understand later
    // Note that if the question was skipped previously, then we must enter to this part of the code
    // to avoid incorrecly marking it as skipped
    if (!$skipped || $form_state['input']['hansontable_raw_input'] != $form_state['input']['hansontable_raw_input_default']) {
      $grid_data = json_decode($form_state['input']['hansontable_raw_input']);
      $processed_user_input = $form_state['input']['hansontable_raw_input'];

      $max_question_score = ipq_tbs_form_ipq_question_max_score($question);

      $wrapper = entity_metadata_wrapper('node', $question);
      $answers_str = '';
      try{
        $answers_str = $wrapper->field_ipq_form_answer_def->value();
      } catch (Exception $exc) {
        watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
      }
      // Answer definition. This contains the correct answer values which are
      // expected.
      $answers = ipq_tbs_form_parse_answers($answers_str);

      foreach($grid_data as $entry){
        // Field ID (such as "8a" or "21")
        $id = $entry[0];
        // Value such as "=sum(2,4)" or "$57,000"
        $value = $entry[1];

        if($value){
          if(isset($answers[$id])) {
            if (isset($answers[$id]['answer'])) {
              // Remove currency symbols and commas
              $correct_answer = preg_replace("/[^\d\.]/", "", $answers[$id]['answer']);
              if (strpos($value, "(") !== FALSE && strpos($value, ")") !== FALSE) {
                // If this is an auto-summed field, skip grading for this cell
                // and just mark it as correct, because the user cannot edit the
                // field on the front-end anyway.
                $score++;
              }
              else {
                // The user must have supplied some input here - mark as not skipped
                $skipped = false;

                // Remove currency symbols and commas
                $value = preg_replace("/[^\d\.]/", "", $value);
                // Handle all other cell types
                if ($correct_answer  == $value) {
                  $score++;
                }
              }
            }
          }
        }
        else {
          // in this case, the cell was left blank or $0.00.
          // Note: there are probably situations where an answer should be a 0, but we can't discern a 0 from a skipped
          // cell because skipped cells become 0. Nevertheless, we can expect that not ALL answers will be 0, so if
          // the user truly supplied any input, it should be detected above and marked as not skipped.

          // Increment the user's score if the correct answer is a blank or $0
          if ($value == $answers[$id]['answer']) {
            $score++;
          }
        }
      }
      $percent_correct = round($score / $max_question_score * 100);

      if ($percent_correct == 100){
        $answer_is_correct = 1;
      } else {
        $answer_is_correct = 0;

        if(!isset($form_state['values']['op']) || $form_state['values']['op'] != 'Emulate Submit'){
          // Note that for 'Emulate submit' functionality we still want to display the correct percent
          // to help admins to create the questions

          // This line should be changed if, in the future, we decide to give
          // partial credit to students who partially answer questions (this was added on DIN-573 resolution)
          $percent_correct = 0;
        }
      }
    }

    // return the information to be processed by the hook caller
    return array(
      'skipped' => $skipped,
      'is_correct' => $answer_is_correct,
      'percent_correct' => $percent_correct,
      'score' => $score,
      'processed_user_input' => $processed_user_input,
    );
  }
}

/**
 * Implements ipq_question_max_score hook (custom hook)
 * should return the maximum achievable score for the question if it belongs to the module question type
 * For form completion questions, this simply returns one point per line in the answer definition. This
 * is slightly inflated, because some of the lines will be auto-calculated by spreadsheet functions like
 * SUM().
 * @param $question
 *  - Question node
 * @return int
 *  - The maximum score achievable for this question
 */
function ipq_tbs_form_ipq_question_max_score($question){
  // we want to react only when it's a question of MCQ type
  if ($question->type == 'ipq_tbs_form_question'){
    $wrapper = entity_metadata_wrapper('node', $question);
    $answers = array();
    try{
      $answers_str = $wrapper->field_ipq_form_answer_def->value();
      $answers = ipq_tbs_form_parse_answers($answers_str);
    } catch (Exception $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
    }
    return count($answers);
  }
}

/**
 * Implements ipq_question_exam_quizlets (custom hook)
 * This hook should return the quizlets and the maximum questions for them for the IPQ exam sessions
 *
 * @param $section
 *  - AUD, BEC, etc...
 * @return array
 *  - array of quizlets and the maximum questions for them
 */
function ipq_tbs_form_ipq_question_exam_quizlets($section){
  // TODO - ipq_tbs_form_ipq_question_exam_quizlets
}

/**
 * Implements ipq_question_review_display_alter hook (custom hook)
 * allows module question types to populate the review display for that given question.
 * @param $data
 *  - Question and session data
 * @return null
 */
function ipq_tbs_form_ipq_question_review_display_alter(&$data) {
  if (isset($data['session'])) {
    $session = $data['session'];

  }
  if ($data['question']->type == 'ipq_tbs_form_question'){
    // Add display of question for review
    $sd = $data['question_session_data'];

    $img_info = array();
    $skipped = $sd['status'] == 2;
    $anwers = array();
    try{

      $wrapper = entity_metadata_wrapper('node', $data['question']);
      $img = $wrapper->field_ipq_form_template->value();

      $img_info['width'] = $img['width'];
      $img_info['height'] = $img['height'];
      $img_info['url'] = file_create_url($img['uri']);
      // determine the type of form being used
      $form_type = $wrapper->field_form_type->value();

      $formula_str = $wrapper->field_ipq_form_display_def->value();

      // we need to load the user values to show them if they exists
      if (!$skipped){
        $user_values = array();
        $user_values_tmp = json_decode($sd['user_input'], true);
        foreach($user_values_tmp as $v){
          $user_values[$v[0]] = $v[1];
        }
      } else {
        $user_values = null;
      }

      $answers_str = $wrapper->field_ipq_form_answer_def->value();
      $answers = ipq_tbs_form_parse_answers($answers_str);
      // version with the user input loaded
      $rows_config = ipq_tbs_form_parse_display_definition($formula_str, $user_values);

      // we are going to need the default values too, to display the original question
      $rows_config_default_values = ipq_tbs_form_parse_display_definition($formula_str, null);

      // and a version with the correct answers
      $answer_values = array();

      // answers array, includes the actual answer and a explanation for the answer
      foreach($answers as $answer_id => $answer_value){
        $answer_values[$answer_id] = $answer_value['answer'];
      }

      $rows_config_correct_values = ipq_tbs_form_parse_display_definition($formula_str, $answer_values);

    } catch (Exception $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
    }

    // We need to include all the js and css to be able to use the calculated formulas (eg. '=sum()') on this page
    drupal_add_js(array('ipq_tbs_form' => array('inputs' => $rows_config ) ), 'setting');

    drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/handsontable.full.js', array('group' => JS_DEFAULT, 'type' => 'file'));
    drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/handsontable.full.min.css', array('group' => CSS_DEFAULT, 'type' => 'file'));

    drupal_add_js(drupal_get_path('module','ipq_common') . '/js/ipq-hot-general-settings.js');
    drupal_add_js(drupal_get_path('module','ipq_tbs_form') . '/js/tbs-form-question.js');

    drupal_add_css(drupal_get_path('module','ipq_tbs_form') . '/css/ipq-tbs-form-question.css');

    $data['question_review_display'] = theme('ipq_tbs_form_question_review',
      array(
        'question' => $data['question'],
        'skipped' => $skipped,
        'rows_config' => $rows_config,
        'rows_config_default' => $rows_config_default_values,
        'rows_config_correct' => $rows_config_correct_values,
        'img_info' =>  $img_info,
        'form_type' =>  $form_type,
        'answers' => $answers
      )
    );
  }
}

/**
 * Implements ipq_question_information hook (custom hook)
 * here we specify the information to be shown on question type information page when the user is
 * taking a quiz or an exam
 * @param $question_type
 *  - node question type machine name
 * @return array|void
 *  - array with the title to be shown in the menu and the information to be displayed for the current
 *    question type
 */
function ipq_tbs_form_ipq_question_information($question_type){
  // Make sure we only alter the display for TBS 2.5 questions.
  if($question_type != 'ipq_tbs_form_question') {
    return;
  }
  return array(
    'title' => 'TBS Form Completion Questions',
    'information' => 'test information',
  );
}

/**
 * Get the correct answers keyed by it's input id
 * @param $answers_def_str
 *  - content of the field_ipq_form_answer_def node field
 * @return array
 *  - array of correct answers, keyed by input id
 */
function ipq_tbs_form_parse_answers($answers_def_str){
  $answers_def_str = trim($answers_def_str);
  $answers_def_str = str_replace("null", "", $answers_def_str);
  $rows = explode("\n", $answers_def_str);
  $answers = array();
  foreach($rows as $key=>$value) {
    $parts = explode(' | ', $value);
    $answers[trim($parts[0])]['answer'] = trim($parts[1]);
    if (isset($parts[2])){
      $answers[trim($parts[0])]['explanation'] = trim($parts[2]);
    } else {
      $answers[trim($parts[0])]['explanation'] = '';
    }

  }
  return $answers;
}

/**
 * @param $formula_str
 * @param $previous_values
 * @return array
 */
function ipq_tbs_form_parse_display_definition($formula_str, $previous_values){
  // we trim the formula, to remove empty lines at the beginning and the end
  // (those will cause new rows to be added)
  $formula_str = trim($formula_str);
  $formula_str = str_replace("null", "", $formula_str);
  $rows = explode("\n", $formula_str);
  foreach($rows as $key=>$value) {
    $rows[$key] = explode(' | ', $value);
  }
  $rows_config = array();
  foreach($rows as $key_row => $cells) {
    // on this question type, the display definition follows the following format:
    // 5[container-id] | :string
    // where '5' is a alphanumeric id, container id is an (optional) div container for the entries
    // and the :string (after the mandatory '|' separator) is the input definition,
    // similar to the input definition of the TBS Journal type

    $id_formula = trim($cells[0]);
    $container_id = 'default';
    if(preg_match('/^([a-zA-Z0-9]*)(\\[(.*)])?$/',$id_formula, $parts)){
      $id = $parts[1];
      if (isset($parts[3])){
        $container_id = $parts[3];
      }
    }
    if(!isset($rows_config[$container_id])){
      $rows_config[$container_id] = array();
    }
    $input_def = ipq_common_parse_cell_formula($cells[1], 'ipq_tbs_form');
    $input_cfg_hot = ipq_common_input_def_to_hot_config($input_def);

    if($previous_values && isset($previous_values[$id]) && $previous_values[$id] !== null){
      $default = $previous_values[$id];
    } else {
      $default = $input_cfg_hot['value'];
    }

    // We shouldn't consider the cells with aggregated formulas as inputs
    // (and for this particular question type, that's the only type of cell that's not an input)
    if (stripos($default, '=sum(') !== FALSE || stripos($default, '!sum(') !== FALSE) {
      $is_input = FALSE;
    }
    else {
      $is_input = TRUE;
    }

    // On some cases, we want to normalize the value to be able to compare it directly
    // (eg: we might want to compare a user answer of "100" with a stored correct answer of "$100.00")
    // for that, we can use the normalized value of both options
    $normalized_value = ipq_tbs_form_normalize_value($input_cfg_hot['settings'], $default);

    $rows_config[$container_id][] = array(
      'id' => $id,
      'cell_cfg' => $input_cfg_hot['settings'],
      'value' => $default,
      'normalized_value' => $normalized_value,
      'is_input' => $is_input
    );
  }
  return $rows_config;
}

/**
 * Some field types (like the currency or the numeric cell type) allow users to input the same value using different
 * formats (eg: '7000', '7,000', '$7,000.00', etc.) this function returns the value in a standarized format
 * that we can use to compare the values
 *
 * @param $field_config
 * - Field definition information
 * @param $value
 * - Value to normalize
 */
function ipq_tbs_form_normalize_value($field_config, $value){
  // if the answer or the user input is null some formatters need to handle it in a special way
  // so we check that first
  // (eg: number_format will throw a warning)
  if($value != '') {
    if ($field_config['format_type'] == 'numeric' || $field_config['format_type'] == 'currency') {
      // the expected correct answer for the numeric, currency and percentage values is just the number
      // (without the % and the $ signs)
      // but for convenience, we are allowing admins to add the signs in the answers and then we remove them
      // here before checking
      if (isset($field_config['format'])){
        if (strpos($field_config['format'], '%') !== false){
          $value = str_replace('%','',$value);
        } else if (strpos($field_config['format'], '$') !== false) {
          $value = str_replace('$', '', $value);
        }
      }
      // we need the answer and the user value expressed in the same format
      // 12,000.00 (coma for thousands separator, point for decimals and tho decimal numbers
      $number = floatval(str_replace(',', '', $value));
      if(is_numeric($number)){
        $value = number_format($number, 2, '.', ',');
      }
    }
  }
  return $value;
}
