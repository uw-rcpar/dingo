<?php  $img_path = drupal_get_path('module', 'ipq_common'); ?>
<div class="user-answer tbs-form-container form-type-<?php print $form_type; ?>">
  <?php if(!$skipped): ?>
    <div class="taxform-container user-input">
      <div
        class="taxform"
        style="
          background: url(<?php print $img_info['url']; ?>);
          width: <?php print $img_info['width']; ?>px;
          height: <?php print $img_info['height']; ?>px;
          "
      ></div>

      <div class="hot-container hidden">
        <div id="data">
          <div class="hot handsontable"></div>
        </div>
      </div>

      <div class="fields">
        <?php foreach($rows_config as $container => $cells): ?>
          <div class="<?php print $container; ?>">
            <?php foreach($cells as $cell): ?>
              <input disabled="disabled" type="text" id="row-<?php print $cell['id']; ?>" class="row-form row-<?php print $cell['id']; ?>" value="<?php print $cell['value']; ?>">
            <?php endforeach; ?>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="answer-status">
        <?php

        foreach($rows_config_correct as $container => $values):
          $i = 0;
          ?>
          <div class="<?php print $container; ?>">
            <?php foreach($values as $value):?>
              <?php if($rows_config[$container][$i]['is_input']): ?>
                <?php
                // user answer is empty
                if ($rows_config[$container][$i]['value']=='') {
                  $answer_status = 'empty';
                  ?>
                  <div class="answer-status-skipped answer-status-row-form row-<?php print $value['id']; ?>"><a data-toggle="modal" data-target="#ipq-explanation-<?php print $value['id']; ?>" class="ipq-explanation-modal" id="explanation-link" data-backdrop="true"><img src="/<?php  print $img_path; ?>/css/img/icon_skipped.svg" width="14" height="14" alt="Icon indicating this entry was left blank." /></a></div>
                  <?php
                  // user answer is correct
                } elseif ($rows_config[$container][$i]['normalized_value']==$value['normalized_value']) {
                  $answer_status = 'correct'; ?>
                  <div class="answer-status-correct answer-status-row-form row-<?php print $value['id']; ?>"><a data-toggle="modal" data-target="#ipq-explanation-<?php print $value['id']; ?>" class="ipq-explanation-modal" id="explanation-link" data-backdrop="true"><img src="/<?php  print $img_path; ?>/css/img/icon_correct.svg" width="14" height="14" alt="Icon indicating this entry was correct." /></a></div>
                  <?php
                  // user answer is incorrect
                } else {
                  $answer_status = 'incorrect'; ?>
                  <div class="answer-status-incorrect answer-status-row-form row-<?php print $value['id']; ?>"><a data-toggle="modal" data-target="#ipq-explanation-<?php print $value['id']; ?>" class="ipq-explanation-modal" id="explanation-link" data-backdrop="true"><img src="/<?php  print $img_path; ?>/css/img/icon_incorrect.svg" width="14" height="14" alt="Icon indicating this entry was incorrect" /></a></div>

                <?php  } ?>


                <!-- Explanation Modal -->
                <div id="ipq-explanation-<?php print $value['id']; ?>" class="modal" data-backdrop="false">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h3 id="termsLabel" class="modal-title">
                        <span class="line-number">LINE <?php print $value['id']; ?></span>
                        <?php if ($answer_status == 'empty') { ?>
                          <span class="students-answer answer-<?php print $answer_status; ?>"><img src="/<?php  print $img_path; ?>/css/img/icon_skipped.svg" width="14" height="14" alt="Skipped icon" /> You did not answer this line.</span>
                        <?php } else { ?>
                          <span class="students-answer answer-<?php print $answer_status; ?>"><img src="/<?php  print $img_path; ?>/css/img/icon_<?php print $answer_status; ?>.svg" width="14" height="14" alt="<?php print $answer_status; ?> icon" /> Your answer of <strong><?php print $rows_config[$container][$i]['normalized_value']; ?></strong> is <?php print $answer_status; ?>.</span>
                        <?php } ?>

                      </h3>
                    </div><!-- /.modal-header -->
                    <div class="modal-body">
                      <?php if ($answer_status != 'correct') { ?>
                        <div class="answer-correct"><img src="/<?php  print $img_path; ?>/css/img/icon_correct.svg" width="14" height="14" alt="Correct icon" /> The correct answer is <strong><?php print $value['value']; ?></strong>.</div>
                      <?php } ?>
                      <?php
                      // note that answers have an array for each answer
                      // that includes the actual answer and an explanation
                      $explanation = isset($answers[$value['id']]['explanation']) ? $answers[$value['id']]['explanation'] : '';

                      ?>
                      <?php if ($cell['value']==$value['value']) { ?>
                        <span class="correct-form-answer">
                          <?php print $explanation; ?>
                        </span>
                      <?php } else { ?>
                        <span class="incorrect-form-answer">
                          <?php print $explanation; ?>
                        </span>
                      <?php }  ?>
                    </div>
                  </div><!-- /.modal-content -->

                </div><!-- /.modal -->
              <?php endif; ?>
              <?php
              $i++;
            endforeach; ?>
          </div>
        <?php endforeach; ?>
      </div>
      <?php
      $question_wrapper = entity_metadata_wrapper('node', $question);
      $explanation = $question_wrapper->field_answer_explanation->value();
      if(isset($explanation['safe_value'])){
        $explanation = $explanation['safe_value'];
      }
      else {
        $explanation = $explanation['value'];
      }

      if ($explanation) {

        ?>
        <div class="form-answer-explanation-heading">Answer Explanation:</div>
        <div class="form-answer-explanation"><?php print $explanation; ?></div>
      <?php } ?>

    </div>

  <?php else: ?>

    <ul class="answer-options"><li class="answer-option skipped"><img src="/<?php  print $img_path; ?>/css/img/icon_skipped.svg" width="14" height="14" alt="Skipped icon" /> You did not answer this question.</li></ul>

  <?php endif; ?>
</div>
<div class="divider"></div>