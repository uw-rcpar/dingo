<div class="question question-tbs-journal">
  <div class="question-text">
    <?php print $question_text; ?>
  </div>
</div>

<div id="ipq-view-solution" class="modal" data-backdrop="false">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="termsLabel" class="modal-title">
        View Solution
      </h3>
    </div><!-- /.modal-header -->
    <div class="modal-body" >
      <div id="ipq-review-body">
        <div id="view-solution-info-container">
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <div class="score-control pull-left"><input id="ipq-view-solution-cb" type="checkbox" name="view-solution-mcq" value="1" <?php print $_SESSION['ipq_session']['session_config']['view_solutions'] ? 'checked' : ''; ?> > <label for="ipq-view-solution-cb">View Solutions</label></div>
      <button type="button" class="try-again-btn btn btn-default">Try Again</button>
      <button type="button" class="continue-btn btn btn-success">Continue</button>
    </div>
  </div><!-- /.modal-content -->

</div><!-- /.modal -->

