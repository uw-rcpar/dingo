<?php
/**
 * @file
 * ipq_tbs_journal.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ipq_tbs_journal_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function ipq_tbs_journal_eck_bundle_info() {
  $items = array(
    'tbs_journal_table_tbs_journal_table' => array(
      'machine_name' => 'tbs_journal_table_tbs_journal_table',
      'entity_type' => 'tbs_journal_table',
      'name' => 'tbs_journal_table',
      'label' => 'TBS Journal Table',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function ipq_tbs_journal_eck_entity_type_info() {
  $items = array(
    'tbs_journal_table' => array(
      'name' => 'tbs_journal_table',
      'label' => 'TBS Journal Table',
      'properties' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function ipq_tbs_journal_node_info() {
  $items = array(
    'ipq_tbs_journal_question' => array(
      'name' => t('IPQ TBS 2.3 Journal'),
      'base' => 'node_content',
      'description' => t('TBS with Journal Entry Grids'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
