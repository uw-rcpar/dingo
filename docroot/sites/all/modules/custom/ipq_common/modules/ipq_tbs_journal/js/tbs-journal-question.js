(function ($) {
  Drupal.behaviors.rcpar_ipq_tbs_journal_display = {

    updateValues: function(hot, gid){
      var data = hot.getData();

      // Numbers that have decimal points need to be rounded or they will be scored incorrect.
      var input_value = JSON.stringify(data, function(key, val) {
        val = val != null ? val : 0;
        return val.toFixed ? Number(val.toFixed(0)) : val;
      });

      $('#hansontable_raw_input_' + gid).val(input_value);

      // No entry required grids on review pages
      if (gid.indexOf('review') != -1 && Drupal.settings.ipq_tbs_journal.no_entry_required) {
        var no_entry = Drupal.settings.ipq_tbs_journal.no_entry_required;
        // Don't show answer rows if table was correctly selected as "no entry required"
        if(jQuery.type(no_entry[gid]) !== "undefined"){
          if (no_entry[gid].answer_rows == false) {
            // Remove collapseable classes
            $('#handsontable-tbs-journal-container-' + gid + ' .ipq-journal-toggl-collapse').removeClass('ipq-journal-toggl-collapse');
            // Every cell will equal 'No entry required'
            $.each(data, function (row_index, row) {
              $.each(row, function (col_index, col) {
                // Make cells display as empty too
                if (data[row_index][col_index] != '') {
                  var meta = hot.getCellMeta(row_index, col_index);
                  if (meta.renderer !== 'StaticCellRenderer') {
                    hot.setDataAtCell(row_index, col_index, '');
                  }
                }
                data[row_index][col_index] = 'No entry required';
              });
            });
          }
        }
      }
      // No entry required grids on quiz pages
      else {
        // We may want to override the handonstable input if the "no entry required" checkbox is selected
        var $noEntryCheckbox = $('input#no-entry-required-' + gid);
        if ($noEntryCheckbox.length && $noEntryCheckbox.prop('checked')) {

          // Every cell will equal 'No entry required'
          $.each(data, function (row_index, row) {
            $.each(row, function (col_index, col) {
              // Make cells display as empty too
              if (data[row_index][col_index] != '') {
                var meta = hot.getCellMeta(row_index, col_index);
                if (meta.renderer !== 'StaticCellRenderer') {
                  hot.setDataAtCell(row_index, col_index, '');
                }
              }
              data[row_index][col_index] = 'No entry required';
            });
          });
          input_value = JSON.stringify(data);
          $('#hansontable_raw_input_' + gid).val(input_value);
        }
      }
      // We mark the question as modified for the auto saving feature.
      // This var may not exist if we are just previewing a question so check to avoid throwing JS errors.
      if(Drupal.behaviors.rcpar_ipq_session_display) {
        Drupal.behaviors.rcpar_ipq_session_display.needsSaving = true;
      }
    },

    updateDefaultValue: function(hot, gid){
      var data = hot.getData();
      $('#hansontable_raw_input_default_' + gid).val(JSON.stringify(data));
    },
    /**
     * Determine if the user has entered anything in a grid.
     * @param cells_info
     * - Array containing info for each cell in the current grid, including editor, renderer,
     * styles, type, default value, etc.
     * @param data
     * - Array containing the content of each cell in each grid.
     * @param gid
     * - Grid id
     * @return boolean
     */
    gridHasEntry: function (cells_info, data, gid) {
      var G = [];
      var hasEntry = false;
      // Create an array G containing row number and column info for each cell in the table.
      // If the cell is an editable type, set the col_info value to editable.
      if (cells_info != '') {
        $.each(cells_info, function(row, rows) {
          $.each(rows, function(col, value) {
            var column = '';
            if (value.type == 'dropdown' || value.type =='numeric' || value.type =='currency') {
              column = 'editable';
            }
            G.push({row_number: row, col_info: column, default_value: value.default_value});
          });
        });
      }
      // The data array starts at zero, but our gid starts at one,
      // so create a variable to get the correct gid from data.
      data_id = numeral(gid) - 1;
      // Create an array F from the data array with the same structure
      // as the G array for comparing values.
      var F = [];
      $.each(data[data_id], function (data_index, data_value) {
        $.each(data_value, function (col_index, col_value) {
          F.push({row_number: data_index, col_value: col_value});
        });
      });
      // Loop through F array and match row numbers to G, checking if col_info is editable
      // and if the column in F has any content besides the default value (this means user
      // has entered something into the cell).
      $.each(F, function (index, value) {
        // Get the default value of the cell if there is one.
        var default_value = '';
        if (G[index].default_value !== undefined) {
          default_value = G[index].default_value;
        }
        if (value.row_number == G[index].row_number) {
          if (G[index].col_info == 'editable') {
            if (value.col_value != '' && value.col_value != default_value) {
              hasEntry = true;
            }
          }
        }
      });
      return hasEntry;
    },
    hasRun: false,
    processedGids:[],
    attach: function (context, settings) {
      var self = this ;
      var behaviours = this;

      // React to the "No entry required" checkbox changing
      $('input.no-entry-required', context).change(function(e){

        // Get the grid id from the input
        var gid = $(this).attr('data-gid');
        // Some grids are for showing results--we don't want to run this on those grids.
        // They will have non-numeric characters in their gids, so check for only numeric gids.
        if (gid.match(/^[0-9]+$/) == null) {
          return;
        }

        // Determine if the table has any entries in it.
        var hasEntry = false;
        if($(this).prop('checked')) {
          var cells_info = '';
          jQuery.each(settings, function(index, el) {
            if (gid == settings[index].gid) {
              cells_info = settings[index].cells;
            }
          });

          // If the user has entered data, we need to show the modal,
          // and the modal buttons control what happens from there.
          if (behaviours.gridHasEntry(cells_info, data, gid)) {

           $('#no-entry-popup', context).attr("role", "dialog");

            $('#no-entry-popup', context).modal('show');

            $('#no-entry-popup', context).on('shown.bs.modal', function () {
              // For some reason after opening and and confirming the modal a few times
              // the modal fires twice, but this will stop it from doing so.
              $(this).off('shown.bs.modal');
            });

            $('.btn-confirm-no-entry', context).unbind().bind('click touchend', function () {
              $('#handsontable-tbs-journal-container-' + gid).addClass('no-entry-required');
              behaviours.updateValues(hot_grids[gid],gid);
              // Disable the grid
              window.disableHot(hot_grids[gid]);
            });

            $('.btn-cancel-no-entry', context).unbind().bind('click touchend', function () {
              $('#no-entry-required-' + gid).prop('checked', false);
              $('#handsontable-tbs-journal-container-' + gid).removeClass('no-entry-required');
              // Allow the grid to be writable again
              window.enableHot(hot_grids[gid]);
            });

          }
          // If the user has not entered data, then we can just disable the hot grid.
          else {
            behaviours.updateValues(hot_grids[gid],gid);
            $('#handsontable-tbs-journal-container-' + gid).addClass('no-entry-required');
            // Make the grid read-only
            window.disableHot(hot_grids[gid]);
          }

        }
        // If the user has not checked the "no entry required" checkbox, that means they're
        // unchecking it and we need to enable the hot grid.
        else {
          behaviours.updateValues(hot_grids[gid],gid);
          $('#handsontable-tbs-journal-container-' + gid).removeClass('no-entry-required');
          // Allow the grid to be writable again
          window.enableHot(hot_grids[gid]);
        }

      });

      // settings loading and variables definition
      var settings = Drupal.settings.ipq_tbs_journal.hansontable_settings;
      // It's important to clone the formula data because Drupal.settings.ipq_tbs_journal can be modified when Drupal's
      // AJAX api is used, which in turn can affect the data in the handsontable which references it directly.
      var data =  $.parseJSON(JSON.stringify(Drupal.settings.ipq_tbs_journal.formula));
      //data = Drupal.settings.ipq_tbs_journal.formula;
      var hot_grids = [];

      jQuery.each(settings, function(index, el){
        var gid = settings[index].gid;
        var isReview = ( gid.indexOf("review") != -1 ) ;

        if (!isReview && self.processedGids.indexOf(gid) != -1) {
          // Preventing the attached code from being attached multiple times (which happens when the "Try Again"
          // button in the view solution and score as you go modals is clicked) because otherwise the hot is
          // re-instantiated and answers are lost. We're not using a once() because that could cause issues with scope.
          // On View solution loaded content, if user tries again, then we need to re process the gids.
          // So we need to skip the gids that belongs to the review page
          return ;
        }
        self.processedGids.push(gid);

        var use_spreadsheet = settings[index].use_spreadsheet;
        var cells_info = settings[index].cells;
        container = document.getElementById('handsontable-tbs-journal-container-'+gid);
        $(container).addClass(settings[index].headers.length +'-columns-table');
        // creating the Handsontable instance

        // we don't want to run all this more than once
        if (!container || $(container).hasClass('processed')){
          return;
        }
        $(container).addClass('processed');

        // we need to construct an array with the information of the
        // special colspan and rowspan cells
        // see http://docs.handsontable.com/0.17.0/demo-merge-cells.html
        var mergeCells = [];

        $.each( cells_info, function( row_index, row ){
          $.each( row, function( col_index, col){
            if (col.styles){
              $.each(col.styles, function (i, style){
                if(style.colspan || style.rowspan){
                  var rowspan = 1;
                  var colspan = 1;
                  if (style.colspan){
                    colspan = style.colspan;
                  }
                  if (style.rowspan){
                    rowspan = style.rowspan;
                  }
                  mergeCells.push({row: row_index, col: col_index, rowspan: rowspan, colspan: colspan});
                }
              });
            }
          });
        });
        // If the grid is a view solution grid, it should be read-only.
        var makeReadOnly = false;
        if (gid.indexOf('show_results') >= 0) {
          var makeReadOnly = true;
        }
        var hot_config = {
          readOnly: makeReadOnly,
          data: data[index],
          columnSorting: false,
          contextMenu: false,
          formulas: true,
          stretchH: "all",
          mergeCells: mergeCells,
          maxRows: settings[index].cells.length,
          renderAllRows: true,

          cells: function (row, col, prop) {
            var cellProperties = cells_info[row][col];
            // we set the ipq cell properties to the cell metadata to be able to retrieve it anywhere

              if ((typeof cellProperties['className'] !== "undefined") ) {
                  this.instance.setCellMeta(row, col, 'className', cellProperties['className']);
                  // delete(cellProperties['className']);
              }

            this.instance.setCellMeta(row, col, 'ipq', cellProperties);
            // some formating properties seems to be interfering with the correct working of the sum feature
            // and also, that seems to be handled somewhere else
            // so we are not sending those parameters when using it on a cell
            var val = this.instance.getDataAtCell(row, col);
            if(typeof val == 'string' && val.indexOf("=sum(") == 0 ){
              cellProperties.readOnly = 'true';
            }

            return cellProperties;
          },
          afterInit: function (){

            // Needs to be a dynamic binding as not all cells are rendered at all times - Pedro
            // Opens modal when enter key is pressed.

            // Commenting this out for now because it isn't working right with the new answer entry popup--
            // instead I'm using code in the afterSelectionEnd hook below. I think this code is for ipads,
            // so it may need to be reworked when we test for ipad.
            /*
            $('.handsontable-tbs-journal-container').on('keypress', 'td.tbs-answer', function (evt) {
              if (evt.which == 13) {
                var row = $(this).data('row');
                hot_config.selectCell(row, 1, row, 1);
                window.answerEntryPopup(row, 1, hot_config);
                hot_config.deselectCell();
              }
            });*/
            // We have to trigger the updateValues function here for review pages with no entry required grids.
            // The afterChange hook doesn't work for grids that have been answered with "no entry required" checked.
            if (gid.indexOf('review') != -1 && $('.no-entry-required').length) {
              behaviours.updateValues(this, gid);
            }
            // FOR IPADS
            $('.htCore tbody tr', this.rootElement).each(function (trIndex) {
              $(this).find('td').each(function (tdIndex, elem) {
                if ($(elem).hasClass('htNumeric')) {
                  $(elem).bind('touchend', function (evt) {
                    evt.stopPropagation();
                    window.answerEntryPopup(trIndex, tdIndex, hot_grids[gid]);
                  });
                }
                if ($(elem).hasClass('htAutocomplete') || $(elem).hasClass('hot-input-dropdown')) {
                  $(elem).bind('touchend', function (evt) {
                    evt.stopPropagation();
                    window.answerSelectionPopup(trIndex, tdIndex, hot_grids[gid]);
                  });
                }
              });
            });
            // On init, loop through all the cells and add a space at the end of their content. This is a hack which fixes the "#VALUE!" error you will see after init if you are using the =sum() function to sum the value of another cell which uses also uses =sum().
            //
            // For example:
            // Cell A1 has the formula =sum(A2,A3).
            // Cell A2 has the formulas =sum(1,1).
            //
            // On first load, cell A1 will have the error "#VALUE!" because it cannot calculate the sum of A2 on first load.
            // To get around this, you simply need to change the value of A1 on first load, such as adding a space.
            // Adding a space will not interfere with the =sum() function.
            $.each(this.getSourceData(), function(rowNum, columns) {
              $.each(columns, function(colNum, text) {
                setTimeout(function() {
                  if (settings[index].cells[rowNum][colNum].renderer && settings[index].cells[rowNum][colNum].renderer == 'HtmlRenderer') {
                    // This cell has special html content, we cannot change it.
                    return;
                  }

                  // Add a class to auto-sum cells which need currency or numeric formatting.
                  // Why am I not using a custom renderer for this? Because then the =sum() function stops working.

                  // Get the format_type (such as "currency" or "numeric") and the format itself (such as "$0,0.00" or "0.00")
                  // These settings are defined in ipq_common.module
                  try {
                    var cellType = settings[index].cells[rowNum][colNum].format_type;
                    var format = settings[index].cells[rowNum][colNum].format;
                  }
                  catch(e) {
                    var cellType = false;
                  }

                  // See comments above for an explanation.
                  if (cells_info[rowNum][colNum] && cells_info[rowNum][colNum]['type'] == "text") {
                    // we only do this for string cells
                    // if we add a space to numeric cells, they are marked as invalid
                    hot_grids[gid].setDataAtCell(rowNum, colNum, text + " ");
                  }
                }, 500);
              });
            });

            // On review pages, add some stuff to td collapseable cells:
            if ($('.page-ipq-review .ipq-journal-toggl-collapse, #view-solution-info-container .ipq-journal-toggl-collapse').length) {
              window.processJournalToggleCells();
            }


            // Toggle and collapse rows on journal review pages
            $('.ipq-journal-toggl-collapse', this.rootElement).click(function () {
              // Change the tooltip on the td cell (actually a title element)
              if ($('body').hasClass('page-ipq-review')) {
                if ($(this).hasClass('row-collapsed')) {
                  $(this).prop('title', 'Collapse');
                }
                else {
                  $(this).prop('title', 'Expand');
                }
              }
              // Get classes of current element.
              var myClass = $(this).attr("class");
              // Find the sibling rows that are collapsed/hidden.
              var className = myClass.match(/ipq-journal-toggl-collapse-[0-9]+/);
              if (className.length > 0) {
                // Get the first class name.
                var row_id = className[0];
                // Get just the row id from the class name.
                row_id = row_id.replace('ipq-journal-toggl-collapse-', '');
                // Hide or show all collapsible rows - but limit to only current grid.
                $('#' + $(this).closest('.handsontable-tbs-journal-container').prop('id') + ' .tbs-journal-collapsible-' + row_id).each(function () {
                  if ($(this).hasClass('hidden')) {
                    $('#' + $(this).closest('.handsontable-tbs-journal-container').prop('id') + ' .ipq-journal-toggl-collapse-' + row_id).removeClass('row-collapsed');
                    $(this).removeClass('hidden');
                  }
                  else {
                    $('#' + $(this).closest('.handsontable-tbs-journal-container').prop('id') + ' .ipq-journal-toggl-collapse-' + row_id).addClass('row-collapsed');
                    $(this).addClass('hidden');
                  }
                });
              }
            });

            // TBS Journal no entry required tooltips
            if (Drupal.settings.ipq_tbs_journal.no_entry_required) {
              var no_entry = Drupal.settings.ipq_tbs_journal.no_entry_required;
              // Don't allow clicking on checkboxes
              $('#ipq-review-body .no-entry-required').attr('disabled', true);
              $.each(no_entry, function (gid, data) {
                // Add a correct or incorrect class to each wrapper div.
                $('#no-entry-required-wrapper-' + gid).addClass('tooltip-status-' + data['tooltip_status']);
                // Change the tooltip text.
                $('#no-entry-required-wrapper-' + gid + ' [data-toggle="tooltip"]').attr('data-original-title', data['tooltip_text']);
              });
              // Show all the tooltips.
              $('.no-entry-required-wrapper [data-toggle="tooltip"]').tooltip('show');
            }

            // After we modified all the values, and everything got settled up ready for the user to
            // answer the question, we update the original question default value to be able to compare with it
            // on server side
            setTimeout(function () {
              behaviours.updateDefaultValue(hot_grids[gid], gid);

              // Trigger the change event for on load so that grids will be disabled if they need to be
              $('input#no-entry-required-' + gid).trigger('change');
            }, 550);
          },
          afterSelectionEnd: function (r, c, r2, c2) {
            // If cell type is dropdown, activate answer popup.
            if (cells_info[r][c] && cells_info[r][c]['type'] == "dropdown") {
              window.answerSelectionPopup(r, c, hot_grids[gid]);
            }
            // If cell type is numeric, activate entry popup.
            if (cells_info[r][c] && cells_info[r][c]['type'] == "numeric") {
              window.answerEntryPopup(r, c, hot_grids[gid]);
              //hot_grids[gid].deselectCell();
            }

            // Enable the scrolling div.
            setTimeout(function() {
              window.enableScrollingDiv();
            }, 600);
          },
          beforeOnCellMouseDown: function (event, coords, TD) {
            // We only want to disable scrolling when the cell is an input cell.
            // This is check is because otherwise iPads will disable scrolling for any cell you click on.
            if ($.inArray('hot-input-dropdown', TD.classList) != -1 || $.inArray('hot-input-numeric', TD.classList) != -1 || $.inArray('htAutocomplete', TD.classList) != -1) {
              // This is a hacky solution/band-aid for a bug in handsontable.
              // When a cell is selected (using tables.selectCell(row, col)),
              // this causes the scrollable div scrollbar to jump to the top.
              // To prevent this, I am temporarily disabling scrolling.
              window.disableScrollingDiv();
            }
          },
        };
        // Replace hotconfig settings for spreadsheet grids.
        if(use_spreadsheet){
          hot_config = addSpreadsheetSettings(hot_config, cells_info);
        }

        hot_config.beforeRender = function(){
          // We need to adjust table widths when they are wider than the available space.
          // To do that, we are going to alter the table's column widths calculated by
          // the HOT autoColumnSize plugin.
          // We will determine which columns contain editor cells, to adjust the size
          // to allow for a background image.
          // We will determine how many columns are the default minimum width (50),
          // add their width to the width of column(s) that contain editors, and then
          // divide the remaining width equally among remaining columns.
          var rootElement = this.getInstance().rootElement;
          var plugin = this.getInstance().getPlugin('autoColumnSize');

          // We depend on the widths calculated by the plugin, so it only makes sense if enabled.
          if (plugin.isEnabled()) {
            // Get the width of the table on the page.
            var tableWidth = $(rootElement).closest('.handsontable-tbs-journal-container').width();

            // Get the number of columns in the table.
            var columnCount = plugin.widths.length;
            
            // Get columns that contain editor cells because we need
            // to add some width to them for the background image.
            var columns_with_editors = [];
            $.each(cells_info, function (row, rows) {
              $.each(rows, function (col, value) {
                // Create an array of columns that have editors.
                if (value.editor != false) {
                  columns_with_editors.push(col);
                }
              });
            });

            // Determine how many columns contain editors (n) that need additional width and
            // how many columns (t) have a width of less than the minimum width (50, or 60 for review grids).
            var n = 0;
            var t = 0;
            var minWidth = 50;
            if ($(this.container).closest("#ipq-view-solution").length || $(this.container).closest("#ipq-review-body").length) {
              var minWidth = 60;
            }
            $.each(plugin.widths, function (i, value) {
              // Sometimes columns with editors in them are wider than 76, because
              // a heading cell has content that makes them so. We only want to force
              // editor columns to be at least 76 in width if they're smaller than 76.
              if (columns_with_editors.indexOf(i) >= 0 && value.width < 76) {
                n++;
              }
              if (value.width < (minWidth + 1)) {
                t++;
              }
            });
            // Get the total widths of columns that are defaulting to the minimum width (t).
            miniumWidthColumns = t * minWidth;

            // Get the total amount of additional space needed by editor columns.
            widthAdjustmentForEditorCells = n * 25;

            // Add both together.
            minWidthPlusEditor = miniumWidthColumns + widthAdjustmentForEditorCells;

            // We need to make some adjustments for review grids.
            // There should not be any editor cells in review grids,
            // instead we need to make the first column 175px wide
            // to provide room for the expand/collapse and correctness indicator.
            if ($(this.container).closest("#ipq-view-solution").length || $(this.container).closest("#ipq-review-body").length) {
              widthAdjustmentForFirstColumn = 175;
              minWidthPlusEditor = miniumWidthColumns + widthAdjustmentForFirstColumn;
            }

            // How many columns are left?
            remainingColumns = columnCount - t;

            // Get the remaining width available.
            remainingWidth = tableWidth - minWidthPlusEditor;

            // Divide that amount into equal widths.
            equalWidth = remainingWidth / remainingColumns;


            // Finally, adjust widths of the columns.
            // 1. Give columns less than 60 the minimum width of 60.
            $.each(plugin.widths, function (i) {
              if (plugin.widths[i] < minWidth) {
                plugin.widths[i] = minWidth;
              }
            });

            // 2. Give columns with editors the width of 75, if they are not bigger already.
            $.each(plugin.widths, function (i) {
              // Change widths of editor cells to 75 to allow for background image.
              if (columns_with_editors.indexOf(i) >= 0 && plugin.widths[i] < 76) {
                plugin.widths[i] = 75;
              }
            });

            // 3. For review grids, we want the first column to have a width of 175,
            // to provide room for the expand/collapse and correctness indicator.
            if ($(this.container).closest("#ipq-view-solution").length || $(this.container).closest("#ipq-review-body").length) {
              plugin.widths[0] = 175;
            }

            // 4. Adjust widths of remaining columns equally.
            $.each(plugin.widths, function (i) {
              if (!(plugin.widths[i] == minWidth || plugin.widths[i] == 75 || plugin.widths[i] == 175)) {
                plugin.widths[i] = equalWidth;
              }
            });

          }
        };

        hot_config.afterRender = function(){

          // Add some css classes.
          var that = this;
          // Add a class to cells if they're readonly.
          var td_class = '';
          if (that.getSettings().readOnly) {
            td_class = 'htDimmed';
          }
          $.each(cells_info, function(row, rows) {
            $.each(rows, function(cell, value) {
              // If the cell has an editor, it needs some classes.
              if (value.editor != false){
                var td = that.getCell(row, cell);
                $(td).addClass("hot-input");
                $(td).addClass("hot-input-" + value.type );
                $(td).addClass(td_class);
                // If the first row contains a cell with an editor, then the row is not a heading row,
                // but rather an input row--the table has no heading row. Add a no-header class to the table.
                if (row == 0) {
                  that.table.className = that.table.className + " no-header";
                }
              }
              // Add a no-header class for view solution tables -- they won't have editors, but
              // rather the first row will have at least one cell with a non-empty class.
              if (row == 0) {
                var td = that.getCell(row, cell);
                if ($(td).hasClass('non-empty')) {
                  that.table.className = that.table.className + " no-header";
                }
              }
            });
          });
        };

        $(container).handsontable(hot_config);
        hot_grids[gid] = $(container).handsontable('getInstance');

        var locked = false;
        var firstRun = true;
        Handsontable.hooks.add('afterChange', function(changes) {
          behaviours.updateValues(hot_grids[gid], gid);
          if (!locked) {
            // Check to see if locked is false. If it is, that means that this code is not ready to run.
            // Otherwise, it might crash the browser from too much recursion.
            if (changes !== null) {
              // The "changes" variable looks like this:
              // [[0, 0, "oldval", "newval"]]
              //
              // changes[0][0] // row
              // changes[0][1] // col
              // changes[0][2] // old text value
              // changes[0][3] // new text value
              if (changes[0][3]) {
                var row = changes[0][0];
                var col = changes[0][1];

                setTimeout(function() {
                  // This try-catch is positioned here for a specific reason. Do not move it, or else all the cells in the handsontable will be formatted according to the last cell's format
                  try {
                    var cellType = settings[index].cells[row][col].format_type;
                    var format = settings[index].cells[row][col].format;
                  }
                  catch(e) {
                    var cellType = false;
                  }
                  if (cellType == 'dropdown' || cellType == 'numeric') {
                    return;
                  }
                  // This "locked" variable is necessary to prevent the "afterChange" hook from running again and crashing the browser from too much recursion.
                  locked = true;

                  if (firstRun) {
                    var cell = $(hot_grids[gid].rootElement).find('tbody tr:nth(' + row + ')').find('td:nth(' + col + ')');
                    if (typeof cell.text() == "string") {
                      if (cell.text().indexOf('#VALUE!') > -1) {
                        hot_grids[gid].setDataAtCell(row, col, changes[0][3] + ' ');
                      }
                    }

                    // Check to see if format contains a percent sign, and temporarily convert it to a $ sign so that numeralJS can parse it
                    var tempFormat = false;
                    if (format) {
                      if (format.indexOf('%') > -1) {
                        var tempFormat = format.replace("%", "$");
                      }
                    }

                    // Check to see if the cell has a number value
                    if (cellType == "currency") {
                      // This will format the value of the cell to something like "$1,234.56" or "1234.56" depending on what the variable "format" contains
                      if (tempFormat) {
                        cell.text(numeral(cell.text()).format(tempFormat).replace("$","%"));
                      }
                      else {
                        cell.text(numeral(cell.text()).format(format));
                      }
                    }

                    // If the format previously had a % sign in it, change the $ sign back to a % sign for display purposes


                    // Locked is now false, allowing the code in the "afterChange" hook to run again
                    locked = false;
                  }

                  if (!firstRun) {
                    // Format the cells in a grid, but only if the grid is not a review grid.
                    if (gid.indexOf('review') == -1) {
                      formatCells();
                    }
                  }
                }, 1000);
              }
            }
          }
        });
        Handsontable.hooks.add('afterOnCellMouseOver', function(changes) {
          // Format the cells in a grid, but only if the grid is not a review grid.
          if ($(this.rootElement.closest("#ipq-review-body")).length == 0) {
            formatCells();
          }
        });
        // A function to format cells in a grid. Used when a cell has been changed or is moused over.
        function formatCells() {
          $(hot_grids[gid].rootElement).find('tbody tr').each(function(row) {
            $(this).find('td').each(function(col) {
              // This try-catch is positioned here for a specific reason. Do not move it,
              // or else all the cells in the handsontable will be formatted according to the last cell's format
              try {
                var cellType = settings[index].cells[row][col].format_type;
                var format = settings[index].cells[row][col].format;
              }
              catch(e) {
                var cellType = false;
              }

              // Get the data from the cell
              var formula = hot_grids[gid].getDataAtCell(row, col);
              // If you add a space after a =sum() formula it gets rid of the "#VALUE!" error message displayed in the cell
              if (typeof $(this).text() == "string") {
                if ($(this).text().indexOf('#VALUE!') > -1) {
                  hot_grids[gid].setDataAtCell(row, col, formula + ' ');
                }
              }

              var that = this;
              setTimeout(function() {
                // Check to see if format contains a percent sign, and temporarily convert it to a $ sign so that numeralJS can parse it
                var tempFormat = false;
                if (format) {
                  if (format.indexOf('%') > -1) {
                    tempFormat = format.replace("%", "$");
                  }
                }

                // Check to see if the cell has a number value
                if (cellType == "currency" || cellType == "numeric") {
                  // This will format the value of the cell to something like "$1,234.56" or "1234.56" depending on what the variable "format" contains
                  if (tempFormat) {
                    $(that).text(numeral($(that).text()).format(tempFormat).replace("$","%"));
                  }
                  else {
                    $(that).text(numeral($(that).text()).format(format));
                  }
                }

                // Locked is now false, allowing the code in the "afterChange" hook to run again
                locked = false;
              }, 500);
            });
          });
        }
        $('.handsontable', context).click(function() {
          firstRun = false;
        });
        initHotSpreadsheetVariables(gid, hot_grids[gid]);

      });

      setSpreadsheetBtnHandlers();

     $('#ipq-explanation-journal').on('show.bs.modal', function () {
        $('#ipq-explanation-journal').draggable({
          handle: '.modal-header'
        });
      });

      $('#ipq-view-solution, #ipq-explanation-journal').on('show', function(){
        // after the modal popup is shown, we need to re render the HOT instance
        // to make it auto fit to the container

        $('.handsontable', this).each(function(index){
          var hot = $(this).handsontable('getInstance');
          if(hot){
            setTimeout(function(){
              hot.render();
            }, 100);
          }
        });

      });

      this.hasRun = true;
    }
  };
}(jQuery));
