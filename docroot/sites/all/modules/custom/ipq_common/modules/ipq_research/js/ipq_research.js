(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.ipqResearch = {
    attach: function (context, settings) {
      // Attach to all research widgets
      $('.research-widget-wrapper', context).each(function(e) {
        IPQResearchWidget(this);
      });
    }
  };
})(jQuery, Drupal, this, this.document); //END - Closure


function IPQResearchWidget(container) {
  // Import jQuery
  var $ = jQuery;

  // Public methods/properties to return
  var pubMethods = {};

  // The outermost wrapper of the widget
  var $widget = $(container);

  // The hidden form input field that holds our true, plain-text value
  var $input = $widget.find('.research-value input');


  /**
   * Position the arrow indicator to be above the input that has been clicked on.
   * @param element
   * - the input that has been clicked on
   */
  function positionIndicator(element) {
    // Get the bottom of the element (#edit-tips) above the text inputs.
    // This minus two pixels will be the top position of the indicator div.
    var $wrapper = $('#edit-tips');
    var topPos = $wrapper.position().top + $wrapper.outerHeight(true) - 2;
    // Get the off of the input that was clicked on.
    var thisOffset = $(element).offset();
    // Get the center of the input that was clicked on by dividing its width by 2.
    var inputCenter = $(element).width() / 2;
    // Get the offset of the wrapping div (#edit-wrapper).
    var wrapperOffset = $('#edit-wrapper').offset();
    // The left position of the indicator is the left offset of the input clicked on
    // minus the left offset of the wrapping div, plus the measurement to the center
    // of the input minus 10.
    var leftPos = (thisOffset.left - wrapperOffset.left) + inputCenter - 10;
    // Set the position of the indicator.
    $('.research-widget-wrapper .indicator').css({'left': leftPos, 'top': topPos});
  }

  function init() {
    //// Attach event handlers ////

    // Select box change
    $widget.find('select').change(function (e) {

      // Remove classes from inputs.
      $('input.form-text').removeClass('in-progress').removeClass('incorrect').removeClass('correct');


      // the select might have (or not, depending on the section)
      // an empty option, when that option is selected, we must hide all the boxes
      if($(this).val() == ''){
        $widget.find('.box-wrapper').addClass('hidden');
        setTipArea('<h3>Choose a title from the list.</h3>');
        setMessageArea('');
      } else {
        $widget.find('.box-wrapper').removeClass('hidden');
      }

      // Update the true, plain text, value of the user's answers based on this change.
      updateTrueValue();

      // Show input boxes and example
      revealBoxesForCurrentTitle();
      showExample(); // Example is the same for all titles, but when the select box changes the example should be shown.

      // Trigger the focus event as well, in case the element was changed without being selected (helps resolve issues
      // form replacement plugin as well)
      $(this).trigger('focus');
    });

    // Select box focus
    $widget.find('select').focus(function (e) {
      setTipArea('<h3>Choose a title from the list.</h3><div class="tip-copy"></div>');

      // Position the arrow indicator.
      positionIndicator($(this));

      // Add classes to text inputs.
      $('input.form-text').each(function(i, obj) {
        // Inputs only need classes if there is a value in the input.
        if ($(obj).val() != '') {
          // If the value is valid, add correct class.
          if (isValid($(obj))) {
            $(obj).removeClass('in-progress').removeClass('incorrect').addClass('correct');
          }
          // Otherwise add incorrect class.
          else {
            $(obj).removeClass('in-progress').removeClass('correct').addClass('incorrect');
          }
        }
      });
    });

    // Form text change
    $widget.find('.box input.form-text').keyup(function (e) {

      // Update the true, plain text, value of the user's answers based on this change.
      updateTrueValue();

      // If there is any input in the box, run a validation. Otherwise, show the example.
      if ($(this).val() != '') {
        // The subtype is only used to describe this box in the correct/incorrect message per box.
        var subtype = $(this).attr('data-subtype');


        // Position the arrow indicator.
        positionIndicator($(this));

        if (isValid(this)) {
          // Show success message for this component in the message area
          var subtypeCapitalized = subtype.charAt(0).toUpperCase() + subtype.slice(1);
          setMessageArea(subtypeCapitalized + ' is correctly formatted.');
          // Add correct class to input.
          $(this).removeClass('in-progress').removeClass('incorrect').addClass('correct');
        }
        else {
          // Show failed validation message
          setMessageArea('Incorrectly formatted ' + subtype + '.');
          // Add incorrect class to input.
          $(this).removeClass('in-progress').removeClass('correct').addClass('incorrect');
        }
      }
      // No input on focus, show default message (the example)
      else {
        showExample();
        return;
      }

      // If the whole row is correct, indicate this to the user
      if(rowIsValid()) {
        setMessageArea('Correctly formatted response.');
        // Add correct class to input.
        $('input.form-text').removeClass('in-progress').removeClass('incorrect').addClass('correct');
      }
    });

    // There are some instances where the value can change without a keyup, so make sure to trigger our keyup event
    // so that all the necessary validation is performed.
    $widget.find('.box input.form-text').change(function (e) {
      $(this).trigger('keyup');
    });

    // Form focus
    $widget.find('.box input.form-text').focus(function (e) {

      // Set the tip text for this input box
      //var tipHTML = $(this).closest('.box').find('.help-block').html();
      var tipHTML = $(this).attr('data-tip');
      setTipArea(tipHTML);

      // Position the arrow indicator.
      positionIndicator($(this));

      // Add in-progress class to input.
      $(this).addClass('in-progress');

      // Trigger the keyup event so that the current value is validated
      $(this).trigger('keyup');
    });

    // Form blur (loses focus)
    $widget.find('.box input.form-text').blur(function (e) {
      // If the input is empty, remove all classes.
      if ($(this).val() =='') {
        $(this).removeClass('in-progress').removeClass('correct').removeClass('incorrect');
      }
    });

    // Triggering the change event helps us initialize the widget display
    $widget.find('select').trigger('change');

    // If the select list has only one option, we'll focus the user on the next box since they can't change it
    if($widget.find('select option').length == 1) {
      $widget.find('input:first').trigger('focus');
    }

    // todo - Split $input.val and determine if there is already a default value. If so, we need to update the select box and text fields.
  }

  /**
   * Validate an individual textfield component
   * param inputElement
   * - DOM element of a textfield within the research widget
   * returns {boolean}
   */
  function isValid(inputElement) {
    var regexp = new RegExp($(inputElement).attr('data-validate'));
    var val = $(inputElement).val();

    return val.match(regexp);
  }

  /**
   * Returns true if all user input fields for the current title are correctly formatted
   * returns {boolean}
   */
  function rowIsValid() {
    var title = getCurrentTitle();
    if(title != ''){
      var $rowinputs = $widget.find('.box-wrapper.' + title).find('input');
      var numCorrect = 0;

      $rowinputs.each(function(index){
        if(isValid(this)) {
          numCorrect++;
        }
      });

      return $rowinputs.length == numCorrect;
    } else {
      return true;
    }
  }

  /**
   * Show the entry boxes for the currently selected title
   */
  function revealBoxesForCurrentTitle() {
    var title = getCurrentTitle();
    $widget.find('.box-wrapper').addClass('hidden');
    if(title != ''){
      $widget.find('.box-wrapper.' + title).removeClass('hidden');
    }
  }

  /**
   * Populate the message area with the question's example text
   */
  function showExample() {
    var title = getCurrentTitle();
    var exampleHTML = '';
    if(title != ''){
      exampleHTML = $widget.find('.box-wrapper.' + title + ' .example').html();
    }
    $widget.find('.message-area').html(exampleHTML);
    setMessageArea(exampleHTML);
  }

  /**
   * Populate the message area with the supplied HTML
   * param html
   * - String of an HTML or text
   */
  function setMessageArea(html) {
    $widget.find('.message-area').html(html);
  }

  function setTipArea(html) {
    $widget.find('.tips').html(html);
  }

  /**
   * Get the currently selected value of the widget's select box
   */
  function getCurrentTitle() {
    return $widget.find('select').val();
  }

  /**
   * Get the value of each box, concatenate it, and supply it as the value to our hidden textfield which represents the
   * actual answer that the user has supplied for this question.
   */
  function updateTrueValue() {
    var trueAnswer = [];

    var title = getCurrentTitle();
    if(title != '') {
      // Iterate through each textfield and add its value to our array
      var $boxes = $widget.find('.box-wrapper.' + title).find('input.form-text');
      $boxes.each(function(index){
        trueAnswer.push($(this).val());
      });

      // Join all the pieces with a pipe character
      trueAnswer = title + '|' + trueAnswer.join('|');
    }
    // Copy the user's plain text answer to the value of the hidden textfield

    if ($input.val() != trueAnswer){
      // Wrapping this in a try/catch because this variable may not be availabe when reviewing or previewing questions and might throw an error.
      try {
        // We mark the question as modified for the auto saving feature
        Drupal.behaviors.rcpar_ipq_session_display.needsSaving = true;
      }
      catch (e) {}
    }
    $input.val(trueAnswer);
  }

  // Attach event behaviors and run start up tasks
  init();

  return pubMethods;
}