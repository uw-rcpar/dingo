<?php
// Get the correct answer
$question_wrapper = entity_metadata_wrapper('node', $question);
$answer_default = ipq_research_plaintext_answer($question_wrapper->field_ipq_research_answer->value());

// Get the answer explanation
$explanation = $question_wrapper->field_ipq_answer_explanation->value();
if (isset($explanation['safe_value'])) {
  $explanation = $explanation['safe_value'];
}

// Determine if question was skipped or not
$skipped = $session_data['user_input'] == NULL ? TRUE : FALSE;

// Grab the user's answer if they didn't skip the question
if (!$skipped) {
  $user_answer = ipq_research_plaintext_answer($session_data['user_input']);
  // Add a class based on correctness
  $extra_class = $session_data['is_correct'] ? 'correct' : 'incorrect';
  $icon_alt_text = $session_data['is_correct'] ? 'Icon indicating you answered this question correctly' : 'Icon indicating you answered this question incorrectly';
}

?>

<div class="question-text">
  <?php
  // Render the question text content field
  $field = field_view_field('node', $question, 'field_ipq_question_text', array('label' => 'hidden'));
  print drupal_render($field);
  ?>
</div>

<div class="user-answer research-user-answer">
  <?php 
  
  $img_path = drupal_get_path('module', 'ipq_common');
  if (!$skipped):
    ?>
    <div class="answer-header" id="answer-heading">Your Answer:</div>
    <ul class="answer-options" aria-labelledby="answer-heading"><li class="answer-option <?php print $extra_class; ?>"><img src="/<?php  print $img_path; ?>/css/img/icon_<?php print $extra_class; ?>.svg" width="14" height="14" alt="<?php print $icon_alt_text; ?>" /> <?php print $user_answer; ?></li></ul>
  <?php else: ?>
   <ul class="answer-options" aria-labelledby="answer-heading"><li class="answer-option skipped"><img src="/<?php  print $img_path; ?>/css/img/icon_skipped.svg" width="14" height="14" alt="Skipped icon" /> You did not answer this question.</li></ul>
  <?php endif; ?>
</div>

<?php if (!$skipped): ?>
  <div class="correct-answer">
    <?php if (!$session_data['is_correct']): ?>
      <div class="answer-header" id="correct-answer-heading">Correct Answer:</div>
      <ul class="answer-options" aria-labelledby="correct-answer-heading"><li class="answer-option correct"><img src="/<?php  print $img_path; ?>/css/img/icon_correct.svg" width="14" height="14" alt="Correct Answer icon" />  <?php print $answer_default; ?></li></ul>
    <?php endif; ?>
    <?php if (!empty($explanation)) { ?>
      <div class="explanation-header">Answer Explanation:</div>
      <div class="explanation-text"><?php print $explanation; ?></div>
    <?php } ?>

  </div>
<?php endif; ?>
