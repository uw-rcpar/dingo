<?php
/**
 * @file
 * ipq_research.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ipq_research_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ipq_research_node_info() {
  $items = array(
    'ipq_research_question' => array(
      'name' => t('IPQ Research 5.0 Question'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
