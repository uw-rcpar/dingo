<div class="question question-research">
  <div class="question-text">
    <?php print drupal_render(field_view_field('node', $question, 'field_ipq_question_text',array('label'=>'hidden') )); ?>
  </div>
</div>
<?php
$checked = $_SESSION['ipq_session']['session_config']['view_solutions'] ? 'checked' : '';
// View solution modal
$m = new RCPARModal('ipq-view-solution','research-solution',array('data-backdrop' => 'false','role'=>'dialog','aria-label' => 'Written Communication Self Assessment', 'tabindex' => '-1',));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="termsLabel" class="modal-title">View Solution</h3>')
  ->setBodyContent($solution)
  ->setFooterContent('<div class="score-control pull-left"><input id="ipq-view-solution-cb" type="checkbox" name="view-solution-mcq" value="1" ' . $checked . ' <label for="ipq-view-solution-cb"> View Solutions</label></div>
      <button type="button" class="try-again-btn btn btn-default">Try Again</button>
      <button type="button" class="continue-btn btn btn-success">Continue</button>');
print $m->render();

?>