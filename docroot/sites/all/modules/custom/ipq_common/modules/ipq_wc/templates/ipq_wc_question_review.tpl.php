<?php
  $question_wrapper = entity_metadata_wrapper('node', $question);
  //print_r($question);
  if ($user_input == null){
    $skipped = true;
  } else {
    $skipped = false;
  }
?>
<div class="question-text">
  <?php
  $field = field_view_field('node', $question, 'field_ipq_question_text',array('label'=>'hidden') );
  print drupal_render($field);
  ?>
  
  <?php if(!$skipped) { ?>

        <div id="selfscore-wrapper2" class="sc-review-scoring">
          <div class="self-score-heading">Your self-graded score:</div>
          <div class="row">
            <div class="col-sm-3 sc-stars">
              <div class="row">
                <div class="rating-label col-sm-6">Organization <span class="reader-instructions">Your self-graded score is <?php print $user_self_scoring['org']; ?> out of 5 stars.</span></div>
                <div class="sc-star-rating sc-organization col-sm-6 org-<?php print $user_self_scoring['org']; ?>"> 
                  <span class="fa fa-star star-1" aria-hidden="true"></span><span class="fa fa-star star-2" aria-hidden="true"></span><span class="fa fa-star star-3" aria-hidden="true"></span><span class="fa fa-star star-4" aria-hidden="true"></span><span class="fa fa-star star-5" aria-hidden="true"></span>
                </div>
              </div>
              
              <div class="row">
                <div class="rating-label col-sm-6">Development <span class="reader-instructions">Your self-graded score is <?php print $user_self_scoring['dev']; ?> out of 5 stars.</span></div>
                <div class="sc-star-rating sc-development col-sm-6 dev-<?php print $user_self_scoring['dev']; ?>"> 
                  <span class="fa fa-star star-1" aria-hidden="true"></span><span class="fa fa-star star-2" aria-hidden="true"></span><span class="fa fa-star star-3" aria-hidden="true"></span><span class="fa fa-star star-4" aria-hidden="true"></span><span class="fa fa-star star-5" aria-hidden="true"></span>
                </div>
              </div>
              
              <div class="row">
                <div class="rating-label col-sm-6">Expression <span class="reader-instructions">Your self-graded score is <?php print $user_self_scoring['exp']; ?> out of 5 stars.</span></div>
                <div class="sc-star-rating sc-expression col-sm-6 exp-<?php print $user_self_scoring['exp']; ?>"> 
                  <span class="fa fa-star star-1" aria-hidden="true"></span><span class="fa fa-star star-2" aria-hidden="true"></span><span class="fa fa-star star-3" aria-hidden="true"></span><span class="fa fa-star star-4" aria-hidden="true"></span><span class="fa fa-star star-5" aria-hidden="true"></span>
                </div>
              </div>
            </div>
            
            <div class="col-sm-4 sc-score">
              <h3 id="sc-score-perc"><?php print $score; ?>%</h3>
              <div class="sc-explain">Your grade for this written communication simulation is an average of the 3 scores to the left.</div>
            </div>
          </div>
          <div class="wc-example">
            <?php
              print nl2br($sample_answer_text);
            ?>
          </div>
        </div>
        <?php } ?>
</div>

<div class="user-answer wc-user-answer">
  <?php if(!$skipped): ?>
    <h2>Your Answer:</h2>
    <div class="user-answer-to">
      <?php $question_to = field_view_field('node', $question, 'field_ipq_question_to',array('label'=>'hidden') );
            print drupal_render($question_to); ?>
    </div>
    <div class="user-answer-re">
      <?php $question_re = field_view_field('node', $question, 'field_ipq_question_re',array('label'=>'hidden') );
            print drupal_render($question_re); ?>
    </div>
    <div class="user-answer-text">
      <?php print $user_input; ?>
    </div>

  <?php else: ?>
    <ul class="answer-options"><li class="answer-option skipped"><img src="/<?php  print drupal_get_path('module', 'ipq_common'); ?>/css/img/icon_skipped.svg" width="14" height="14" alt="Skipped icon" /> You did not answer this question</li></ul>
  <?php endif; ?>
</div>

<?php if(!$skipped): ?>
  <div class="sample-answer">
    <h2>Sample Answer:</h2>
    <?php
      $v = $question_wrapper->field_wc_sample_answer->value();
      print nl2br(check_plain($v));
    ?>
  </div>
<?php endif; ?>