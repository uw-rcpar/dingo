<?php
/**
 * @file
 * ipq_wc.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ipq_wc_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ipq_wc_node_info() {
  $items = array(
    'ipq_wc_question' => array(
      'name' => t('IPQ WC 4.0 Question'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
