(function ($) {
  Drupal.behaviors.rcpar_ipq_wc_session_display = {

    // this question type, have a special logic for the 'next' and 'submit quizlet/testlet' buttons
    // we open several dialogs (scoring dialog, confirm exit from quiz/exam, etc)
    // and the dialogs must know which button has started the user interaction and
    // we need to know if we already shown the scoring dialog to the user to don't show it
    // repeated times
    triggering_btn: null,
    already_shown_scoring_modal: false,

    // we also need this variable to be able to detect when the scoring modal was hidden
    // by clicking outside of it
    controlled_hidden_modal: false,

    score_as_you_go_controlled_hidden_modal: false,

    // some buttons have standard behaviours defined for the common ipq questions
    // but on this question type, the presence of the self scoring popup, requires a special logic
    // to be executed before (and sometimes after) the original handlers is executed
    // for those buttons, we do a little hack: we store the original handler, remove it from the current button handlers
    // and then, inside the logic of our new handlers, we invoke them when is required
    submit_test_original_callback: null,
    edit_next_original_callback: null,
    edit_back_original_callback: null,
    score_as_you_go_continue_original_callback: null,

    attach: function (context, settings) {
      var behaviours = this;
      // This code adds a spell-checker to the TBS WC textarea
      try {
        CKEDITOR.on("instanceReady", function (event) {
          var sc = new SpellChecker({
            iframe: '.cke_wysiwyg_frame',
            textarea: false,
            editable: '.cke_editable'
          });

          sc.init();

          event.editor.on("change", function (event) {
            // We mark the question as modified for the auto saving feature
            Drupal.behaviors.rcpar_ipq_session_display.needsSaving = true;
          });

          // Make body of ckeditor iframe fill available area. See PROD-209
          event.editor.document.appendStyleText('body {min-height:450px !important;}');
        });
      }
      catch (e) {}

      // WC 4.0 - Star Self-Scoring System
      var SetRatingStar = function(rtype) {
      var $star_rating = $('.'+rtype+' .fa');
        return $star_rating.each(function() {
          // Enable submit if user selects any ratings
          $('#save-self-score').removeAttr('disabled');
          if (parseInt($('#'+rtype).val()) >= parseInt($(this).data('rating'))) {
            return $(this).removeClass('fa-star-o').addClass('fa-star');
          } else {
            return $(this).removeClass('fa-star').addClass('fa-star-o');
          }
        });
      };

      $('.sc-star-rating .fa', context).on('click', function() {
        var rtype = $(this).parent().attr('rtype');
        $('#'+rtype).val($(this).data('rating'));
        SetRatingPercentage();
        return SetRatingStar(rtype);
      });

      // Set score in display only
      var SetRatingPercentage = function() {
        total = 15;
        score = parseInt($('#sc-organization').val()) + parseInt($('#sc-development').val()) + parseInt($('#sc-expression').val());
        totalperc = (score / total * 100).toFixed(2);
        $('#sc-score-perc').html(totalperc+'%');
      };

      // Re=populates the scores if set
      SetRatingStar('sc-organization');
      SetRatingStar('sc-development');
      SetRatingStar('sc-expression');
      // Set percentage based on self score values
      SetRatingPercentage();

      // Decide when to allow scoring or not.
      $('#edit-next').addClass('self-score');


      // we need to change the moment when the original submit-test-link click handler is executed
      // for that, we remove it from the handlers list and store a reference to it to use it later
      if(!behaviours.submit_test_original_callback){
        behaviours.submit_test_original_callback = $._data($('#submit-test-link')[0]).events['click'][0].handler;
      }
      $('#submit-test-link', context).off('click',behaviours.submit_test_original_callback);
      $('#submit-test-link', context).on('click.f1',function(){
        behaviours.triggering_btn = 'submit-test-link';
        // Without this check, the scoring modal is being shown again
        // after the skipped question modal.
        if (!behaviours.already_shown_scoring_modal) {
          var showing_modal = showWCScore();
        }
        else {
          showing_modal = false;
        }
        behaviours.already_shown_scoring_modal = true;
        if(showing_modal){
          return false;
        } else {
          return behaviours.submit_test_original_callback();
        }
      });

      $('#edit-back', context).once('confirm-processed-wc', function() {
        // we need to change the moment when the original edit-back click handler is executed (we need to execute ours first)
        // for that, we remove it from the handlers list and store a reference for later usage
        if(!behaviours.edit_back_original_callback){
          behaviours.edit_back_original_callback = $._data($('#edit-back', context)[0]).events['click'][1].handler;
          $('#edit-back', context).off('click',behaviours.edit_back_original_callback);
        }
        $(this).click(function() {
          behaviours.triggering_btn = 'edit-back';
          if(!behaviours.already_shown_scoring_modal){
            var showing_modal = showWCScore();
            behaviours.already_shown_scoring_modal = true;
            if(showing_modal){
              return false;
            } else {
              return behaviours.edit_back_original_callback();
            }
          } else {
            behaviours.already_shown_scoring_modal = false;
            return behaviours.edit_back_original_callback();
          }
        });
      });


      $('#edit-next', context).once('confirm-processed-wc', function() {
        // we need to change the moment when the original edit-next click handler is executed (we need to execute ours first)
        // for that, we remove it from the handlers list and store a reference for later usage
        if(!behaviours.edit_next_original_callback){
          behaviours.edit_next_original_callback = $._data($('#edit-next', context)[0]).events['click'][1].handler;
          $('#edit-next', context).off('click',behaviours.edit_next_original_callback);
        }
        $(this).on('click', function () {
          behaviours.triggering_btn = 'edit-next';
          if (!behaviours.already_shown_scoring_modal) {
            var showing_modal = showWCScore();
            behaviours.already_shown_scoring_modal = true;
            if (showing_modal) {
              return false;
            }
            else {
              behaviours.score_as_you_go_controlled_hidden_modal = false;
              return behaviours.edit_next_original_callback();
            }
          }
          else {
            if (behaviours.score_as_you_go_controlled_hidden_modal){
              return true;
            } else {
              return behaviours.edit_next_original_callback();
            }
          }
        });
      });

      $('#ipq-score-information .continue-btn', context).once('confirm-processed-wc', function() {
        // we need to change the moment when the original continue-btn click handler is executed (we need to execute ours first)
        // for that, we remove it from the handlers list and store a reference for later usage
        if(!behaviours.score_as_you_go_continue_original_callback){
          behaviours.score_as_you_go_continue_original_callback = $._data($(this)[0]).events['click'][1].handler;
          $(this).off('click',behaviours.score_as_you_go_continue_original_callback);
        }
        $(this).click(function () {
          behaviours.score_as_you_go_controlled_hidden_modal = true;
          behaviours.score_as_you_go_continue_original_callback();
          return false;
        });
      });


      $('#ipq-score-information').once('confirm-processed-wc', function() {
        $(this).on('hidden.bs.modal', function (e) {
          // if the score modal has been hidden by...
          if (behaviours.score_as_you_go_controlled_hidden_modal){
            behaviours.already_shown_scoring_modal = true;
          } else {
            behaviours.already_shown_scoring_modal = false;
          }
        })
      });

      $('.sc-scoring', context).on('hidden.bs.modal', function () {
        if (!behaviours.controlled_hidden_modal){
          $('#sc-close').click();
        }
        // we reset the variable value
        behaviours.controlled_hidden_modal = false;
      });
      // Manage the modal-backdrop div since we are envoking manually
      $('#sc-close', context).on('click', function() {
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
          // user is cancelling so set flag back to 1
          $('#sc-populated').val(1);
          behaviours.already_shown_scoring_modal = false;
          behaviours.controlled_hidden_modal = true;
      });
      $('#sc-cancel', context).on('click', function() {
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
          // user is cancelling so set flag back to 1
          $('#sc-populated').val(1);
          behaviours.already_shown_scoring_modal = false;
          behaviours.controlled_hidden_modal = true;
      });

      $('#sc-skip', context).once('confirm-processed', function(){
        $(this).on('click', function() {
          // open a dialog to let user know that the answer will be lost if he continues
          var confirmed = confirm("If you skip this question, you answer will not be stored.\nAre you sure you want to continue?");
          if(confirmed){
            // we use this variable to tell the server that we should ignore the user answer and scoring if exists
            $('input[name=force_skip]').val(1);

            $('.sc-scoring').modal('hide');
            behaviours.controlled_hidden_modal = true;
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            if(behaviours.triggering_btn == 'submit-test-link'){
              return behaviours.submit_test_original_callback();
            } else if (behaviours.triggering_btn == 'edit-back') {
              $('#edit-back').click();
            } else {
              $('#edit-next').click();
            }
          } else {
            // if the user canceled the skipping, the logic is the same that in the cancel button
            $('#sc-cancel').click();
          }
        });
      });

      // Save self score
      $('#save-self-score', context).once('confirm-processed', function(){
        $(this).on('click', function() {
          behaviours.controlled_hidden_modal = true;
          $('.sc-scoring').modal('hide');
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
          if(behaviours.triggering_btn == 'submit-test-link'){
            return behaviours.submit_test_original_callback();
          } else if (behaviours.triggering_btn == 'edit-back') {
            $('#edit-back').click();
          } else {
            $('#edit-next').click();
          }
        });
      });
    }
  }
}(jQuery));

function getUserInput(){
  // Import jQuery
  var $ = jQuery;
  if (CKEDITOR.instances['edit-user-response-value'] != null){
    return CKEDITOR.instances['edit-user-response-value'].getData();
  } else {
    return $('#edit-user-response-value').val();
  }

}


function showWCScore() {
  // Import jQuery
  var $ = jQuery;
  var showing_modal = false;
  // Show Self Score Widget
  if ($.trim(getUserInput()).length >= 1) {
    // Make modal draggable
    $('.sc-scoring .modal-content').draggable({
      handle: '.modal-header',
      containment: 'window',
      revert: false,
      clone: 'helper',
      appendTo: 'body',
      scroll: false,
    });
    // position the modal
    $('#sc-scoring .modal-content').css('left', (($(window).width() - $('#sc-scoring .modal-content').width()) / 2));
    // Show modal for scoring
    $('.sc-scoring').modal('show');
    showing_modal = true;
    $('.sc-scoring').on('show.bs.modal', function () {
      $('.sc-scoring .modal-body').css('height', $(window).height() * 0.65);
    });
    // we reset the value of this variable, in case we have set it before
    $('input[name=force_skip]').val(0);
    // Don't allow scoring unless passes
    if ($('#edit-next').hasClass('self-score') &&
      ( $('#sc-organization').val() == 0 ||
      $('#sc-development').val() == 0 ||
      $('#sc-expression').val() == 0 ) ||
      ($('#sc-populated').val() == 1)
    ) {
      // Remove the first submit pause used to allow for user review
      if ($('#sc-populated').val() == 1) {
        $('#sc-populated').val(0);
        $('#save-self-score').removeAttr('disabled');
      }
    }
  }
  return showing_modal;
}
