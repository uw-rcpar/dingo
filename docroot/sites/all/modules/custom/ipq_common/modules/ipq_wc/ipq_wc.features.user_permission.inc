<?php
/**
 * @file
 * ipq_wc.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ipq_wc_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use text format simple_wysiwyg'.
  $permissions['use text format simple_wysiwyg'] = array(
    'name' => 'use text format simple_wysiwyg',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
