<?php
  $session = $form['session']['#value'];
  $question = $form['question']['#value'];

  $question_text = field_view_field('node', $question, 'field_ipq_question_text',array('label'=>'hidden') );
  //$answers = field_view_field('node', $question, 'field_ipq_wc_answer');
  $sample_answer_text = field_view_field('node', $question, 'field_wc_sample_answer',array('label'=>'hidden') );
  $sample_answer_text = drupal_render($sample_answer_text);
?>
<div class="question question-wc">
  <div class="quesion-text">
    <?php print drupal_render($question_text); ?>
  </div>
 
  <div class="answers wc-user-input">
    <?php print drupal_render_children($form);  ?>
  </div>        
</div>

<!-- Self Scoring Modal -->
<?php
$modal_body = '<div id="selfscore-wrapper2">

  <div class="row">
    <div class="col-sm-4 sc-stars">
      <div class="row">
        <div class="rating-label col-sm-6">Organization</div>
        <div class="sc-star-rating sc-organization col-sm-6" rtype="sc-organization">
          <span class="fa fa-star" data-rating="1"></span><span class="fa fa-star" data-rating="2"></span><span class="fa fa-star" data-rating="3"></span><span class="fa fa-star" data-rating="4"></span><span class="fa fa-star" data-rating="5"></span>
        </div>
      </div>

      <div class="row">
        <div class="rating-label col-sm-6">Development</div>
        <div class="sc-star-rating sc-development col-sm-6" rtype="sc-development">
          <span class="fa fa-star" data-rating="1"></span><span class="fa fa-star" data-rating="2"></span><span class="fa fa-star" data-rating="3"></span><span class="fa fa-star" data-rating="4"></span><span class="fa fa-star" data-rating="5"></span>
        </div>
      </div>

      <div class="row">
        <div class="rating-label col-sm-6">Expression</div>
        <div class="sc-star-rating sc-expression col-sm-6" rtype="sc-expression">
          <span class="fa fa-star" data-rating="1"></span><span class="fa fa-star" data-rating="2"></span><span class="fa fa-star" data-rating="3"></span><span class="fa fa-star" data-rating="4"></span><span class="fa fa-star" data-rating="5"></span>
        </div>
      </div>
    </div>

    <div class="col-sm-5 sc-score">
      <h3 id="sc-score-perc">0%</h3>
      <div class="sc-explain">Your grade for this written communication simulation is an average of the 3 scores to the left.</div>
    </div>
  </div>
  <div class="wc-example">
    <h3>Master Level Written Communication Example</h3>'
    . nl2br($sample_answer_text) .'</div>
</div>';

$m = new RCPARModal('sc-scoring','sc-scoring',array('data-backdrop' => 'true','role'=>'dialog','aria-label' => 'Written Communication Self Assessment', 'tabindex' => '-1',));
$m->setHeaderContent('<button type="button" class="close" id="sc-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Written Communication Self Assessment</h3>
        <p>Click on the stars to rate your written communication according to the criteria below. Use the master-level written communication below as a guide.</p>')
  ->setBodyContent($modal_body)
  ->setFooterContent('<button type="button" class="btn btn-default" id="sc-cancel" data-dismiss="modal">Back to Question</button>
        <button type="button" class="btn btn-default" id="sc-skip" data-dismiss="modal">Skip Question</button>
        <button type="button" class="btn btn-primary save-score-btn" disabled id="save-self-score" question="<?php print $questions; ?>">Save Question</button>');
print $m->render();
?>

<!-- View Solution Modal -->
<div id="ipq-view-solution" class="modal" data-backdrop="false">
    <div class="modal-content"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="termsLabel" class="modal-title">
          View Sample
        </h3>
      </div><!-- /.modal-header -->
      <div class="modal-body">
        <?php
          $question_wrapper = entity_metadata_wrapper('node', $question);
        ?>
        <?php
          $v = $question_wrapper->field_wc_sample_answer->value();
          print nl2br(check_plain($v));
        ?>
      </div>
      
      <div class="modal-footer">
       <div class="score-control pull-left"><input id="ipq-view-solution-cb" type="checkbox" name="view-solution-mcq" value="1" <?php print $_SESSION['ipq_session']['session_config']['view_solutions'] ? 'checked' : ''; ?> > <label for="ipq-view-solution-cb">View Solutions</label></div>
      </div><!-- /modal-footer -->
    </div><!-- /.modal-content -->

</div><!-- /.modal --> 
