<?php
/**
 * @file
 * ipq_wc.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function ipq_wc_filter_default_formats() {
  $formats = array();

  // Exported format: Simple WYSIWYG.
  $formats['simple_wysiwyg'] = array(
    'format' => 'simple_wysiwyg',
    'name' => 'Simple WYSIWYG',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(),
  );

  return $formats;
}
