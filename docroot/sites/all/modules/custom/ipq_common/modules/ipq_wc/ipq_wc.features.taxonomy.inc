<?php
/**
 * @file
 * ipq_wc.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ipq_wc_taxonomy_default_vocabularies() {
  return array(
    'ipq_tag' => array(
      'name' => 'IPQ Tag',
      'machine_name' => 'ipq_tag',
      'description' => 'Arbitary tags and labels for IPQ questions',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
