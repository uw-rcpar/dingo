<?php
/**
 * @file
 * ipq_tbs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ipq_tbs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function ipq_tbs_eck_bundle_info() {
  $items = array(
    'tbs_answer_tbs_answer' => array(
      'machine_name' => 'tbs_answer_tbs_answer',
      'entity_type' => 'tbs_answer',
      'name' => 'tbs_answer',
      'label' => 'TBS Answer',
      'config' => array(),
    ),
    'tbs_subquestion_tbs_subquestion' => array(
      'machine_name' => 'tbs_subquestion_tbs_subquestion',
      'entity_type' => 'tbs_subquestion',
      'name' => 'tbs_subquestion',
      'label' => 'TBS Subquestion',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function ipq_tbs_eck_entity_type_info() {
  $items = array(
    'tbs_answer' => array(
      'name' => 'tbs_answer',
      'label' => 'TBS Answer',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
    'tbs_subquestion' => array(
      'name' => 'tbs_subquestion',
      'label' => 'TBS Subquestion',
      'properties' => array(
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'uuid' => array(
          'label' => 'UUID',
          'type' => 'text',
          'behavior' => 'uuid',
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function ipq_tbs_node_info() {
  $items = array(
    'ipq_tbs_question' => array(
      'name' => t('IPQ TBS 2.1 Question'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
