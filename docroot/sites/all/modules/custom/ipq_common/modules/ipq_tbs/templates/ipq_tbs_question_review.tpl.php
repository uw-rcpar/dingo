<?php
$question_wrapper = entity_metadata_wrapper('node', $question);
$img_path = drupal_get_path('module', 'ipq_common');
?>

<div class="question-header row <?php print $state->status ?>">
  <div class="answer-section answer-status">
    <h3 class="review-tilte-header">Answer status:</h3>
    <span class="<?php print $state->status ?>">
      <span class="question-<?php print$state->status ?>"></span>
      <?php print $state->status ?>
    </span>
  </div>
  <div class="answer-section answer-score ">
    <h3 class="review-tilte-header">Subquestion score:</h3>
    <div class="answer-content">
      <div id="exams-review-content" class="exam-ipq-links<?php if (exam_version_is_versioning_on()) { ?> exam-versions-exist<?php } ?>">
        <div class="drs-wrapper course-detail-wrapper col-md-3 col-sm-6 col-xs-6 section-1">
          <div class="radial-element" id="TBS">
            <p class="element-value"> <?php print $state->percentaje ?> </p>
          </div>
        </div>
      </div>
      <div class="labels-content">
        <span class="question-percentaje"><?php print $state->percentaje ?>%</span>
        <span class="question-count"><?php print $state->correct ?> / <?php print $state->count ?></span>
      </div>
    </div>
  </div>
</div>
<div class="question-text">
  <?php
  $field = field_view_field('node', $question, 'field_ipq_question_text', array('label' => 'hidden'));
  print drupal_render($field);
  ?>
  <div class="answer-heading" id="answer-heading">Your Answers:</div>
</div>

<div class="user-answer tbs-user-answer">
  <a class="expand-collapse-all"><i class="fa fa-chevron-down"></i> Expand all rows</a>
  <table class="answers-table">
    <tr>
      <th><strong><?php print $question_wrapper->field_question_column_header->value(); ?></strong></th>
      <th><strong><?php print $question_wrapper->field_answers_column_header->value(); ?></strong></th>
    </tr>
    <?php
    $correct_count = 0;
    foreach ($question_wrapper->field_questions as $index => $subquestion):
      $subquestion_text = $subquestion->field_question_text->value();
      $subquestion_text = $subquestion_text['value'];
      $correct = FALSE;
      $answer_text = '';
      $correct_answer = '';
      $answer_explanation = $subquestion->field_ipq_answer_explanation->value();
      $answer_explanation = $answer_explanation['value'];

      foreach ($subquestion->field_answers as $aindex => $answer) {
        if ($answer->field_ipq_answer_is_correct->value()) {
          $correct_answer = $answer->title->value();
          break;
        }
      }
      foreach ($subquestion->field_answers as $aindex => $answer) {
        if (isset($user_input[$subquestion->id->value()]) && $user_input[$subquestion->id->value()] == $answer->id->value()) {
          $answer_text = $answer->title->value();
          if ($answer->field_ipq_answer_is_correct->value()) {
            $correct = TRUE;
          }
        }
      }
      if ($correct) {
        $correct_count++;
        $correctnes_class = 'correct';
        $icon_alt_text = 'Icon indicating you made the correct selection';
      }
      elseif ($answer_text) {
        $correctnes_class = 'incorrect';
        $icon_alt_text = 'Icon indicating you made an incorrect selection';
      }
      else {
        $correctnes_class = 'unanswered';
        $icon_alt_text = 'Icon indicating you did not answer the question';
        $answer_text = 'Unanswered';
      }
      ?>
      <tr class="<?php print $correctnes_class; ?> answer-row">
        <td class="answer-col1 <?php print $correctnes_class; ?>"><?php print $subquestion_text; ?></td>
        <td class="answer-col2" id="answer-col-wrapper<?php print $index; ?>">
          <div class="<?php print $correctnes_class; ?> answer-div" title="Expand">
            <i></i>
            <div class="chevron-icon">
              <img src="/<?php print $img_path; ?>/css/img/icon_chevron.png" alt="<?php print $icon_alt_text; ?>"/>
            </div>
            <div class="answer-status-icon">
              <img src="/<?php print $img_path; ?>/css/img/icon_flat_<?php print $correctnes_class; ?>.png" alt="<?php print $icon_alt_text; ?>"/>
            </div>
            <div class="tbs-option-list <?php print $correctnes_class; ?>"><?php print $answer_text; ?></div>
          </div>
          <div class="answer-explanation">
            <?php
            if ($correctnes_class == 'incorrect' || $correctnes_class == 'unanswered') {
              ?>
              <div class="answer-header" id="correct-answer-heading"><strong>Correct Answer:</strong></div>
              <div class="answer-explanation">
                <?php print $correct_answer; ?>
              </div>
            <?php } ?>
            <div class="answer-header"><strong>EXPLANATION</strong></div>
            <div class="answer-explanation">
              <?php print $answer_explanation; ?>
            </div>
          </div>
        </td>
      </tr>
    <?php endforeach; ?>
  </table>
</div>
<div class="divider"></div>
