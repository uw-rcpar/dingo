(function ($) {
  Drupal.behaviors.rcpar_ipq_tbs_display = {

    cleanHandsontableStyles: function () {
      // nothing yet...
    },

    updateValues: function (hot) {
      var data = hot.getData();
      // Wrapping this in a try/catch because this variable may not be available when reviewing or previewing questions and might throw an error.
      try {
        // We mark the question as modified for the auto saving feature
        Drupal.behaviors.rcpar_ipq_session_display.needsSaving = true;
      }
      catch (e) {}
      $('#hansontable_raw_input').val(JSON.stringify(data));
    },

    hasRun: false,
    attach: function (context, settings) {
      // Preventing the attached code from being atttached multiple times (which happens when the "Try Again"
      // button in the view solution and score as you go modals is clicked) because otherwise the hot is
      // re-instantiated and answers are lost. We're not using a once() because that could cause issues with scope.
      if (this.hasRun) {
        return;
      }
      // It's important to clone the formula data because Drupal.settings.ipq_tbs can be modified when Drupal's
      // AJAX api is used, which in turn can affect the data in the handsontable which references it directly.
      var data =  $.parseJSON(JSON.stringify(Drupal.settings.ipq_tbs.formula));

      var lastRowSelected = -1;
      var lastColSelected = -1;
      var behaviours = this;
      // settings loading and variables definition
      var
        //data = Drupal.settings.ipq_tbs.formula,
        columns_info = Drupal.settings.ipq_tbs.hansontable_settings.columns,
        cells_info = Drupal.settings.ipq_tbs.hansontable_settings.cells,
        container = document.getElementById('handsontable-tbs-container'),
        hot1 = null;

      // we don't want to run all this more than once
      if ($(container).hasClass('htColumnHeaders')) {
        return;
      }


      // Custom Answer renderer - we need to add a class on the questions that were already answered
      function TBSAnswerRenderer(instance, td, row, col, prop, value, cellProperties) {
        $(td).wrapInner("<div class='tbs-cell-container-answer'></div>");
        Handsontable.TextCell.renderer.apply(this, arguments);
        $(td).addClass('tbs-answer');
        $(td).addClass('htAutocomplete');
        if (value) {
          $(td).addClass('non-empty');
        }
        $(td).data('row', row);
        $(td).attr("aria-label", "Press enter to place your answer.");

        // changes on hover on quiz take page
        if (window.location.href.indexOf('take') > -1) {
          Handsontable.dom.addEvent(td, 'mouseover', function (event) {
            if (value) {
              td.innerHTML = value;
            }
            else {
              td.innerHTML = 'Click to select';
            }
            $(td).addClass('dropdown-hover');
          });
          // remove changes on mouseleave
          Handsontable.dom.addEvent(td, 'mouseleave', function (event) {
            if (value) {
              td.innerHTML = value;
            }
            else {
              td.innerHTML = '';
            }
            $(td).removeClass('dropdown-hover');
          });
        }

        return td;
      }

      // Custom Question renderer - we are using this just to wrap the cell content inside a div
      function TBSQuestionRenderer(instance, td, row, col, prop, value, cellProperties) {
        var v = Handsontable.helper.stringify(value);
        td.innerHTML = v;

        $(td).wrapInner("<div class='tbs-cell-container-question'></div>");
        $(td).addClass('tbs-question');
        return td;
      }

      $(container).addClass(Drupal.settings.ipq_tbs.hansontable_settings.headers.length + '-columns-table');

      // creating the Handsontable instance
      hot1 = new Handsontable(container, {
        enterMoves: {row: 0, col: 0},
        data: data,
        colHeaders: Drupal.settings.ipq_tbs.hansontable_settings.headers,
        columnSorting: false,
        contextMenu: false,
        stretchH: "all",
        formulas: true,
        maxRows: Drupal.settings.ipq_tbs.hansontable_settings.cells.length,
        afterInit: function () {
          // Needs to be a dynamic binding as not all cells are rendered at all times - Pedro
          // Opens modal when enter key is pressed.
          /*$('#handsontable-tbs-container').on('keypress', 'td.tbs-answer', function (evt) {
            if (evt.which == 13) {
              var row = $(this).data('row');
              hot1.selectCell(row, 1, row, 1);
              window.answerSelectionPopup(row, 1, hot1);
              hot1.deselectCell();
            }
          });*/


          $('.handsontable-tbs-container tbody tr').each(function (trIndex) {
            $(this).find('td').each(function (tdIndex, elem) {
              if ($(elem).hasClass('htAutocomplete') || $(elem).hasClass('hot-input-dropdown')) {
                $(elem).bind('touchend', function (evt) {
                  evt.stopPropagation();
                  window.answerSelectionPopup(trIndex, tdIndex, hot1);
                });
              }
            });
          });
        },
        cells: function (row, col, prop) {
          var cellProperties = cells_info[row][col];
          // answer and questions cells have a special renderer
          if (col == 1) {
            cellProperties.renderer = TBSAnswerRenderer;
          }
          else {
            cellProperties.renderer = TBSQuestionRenderer;
          }
          return cellProperties;
        },
        beforeRender: function () {

          // We want to adjust the column widths for tbs questions.
          // This is mainly an issue for Safari, IE and Edge.
          var rootElement = this.getInstance().rootElement;
          var plugin = this.getInstance().getPlugin('autoColumnSize');

          // We depend on the widths calculated by the plugin, so it only makes sense if enabled.
          if(plugin.isEnabled()){
            // Get the width of the table on the page.
            var tableWidth = $(rootElement).closest('.handsontable-tbs-container').width();
            // adjust the column widths
            plugin.widths[0] = tableWidth / 2;
            plugin.widths[1] = tableWidth / 2;
          }
        },
        /*afterRender: function () {
          behaviours.cleanHandsontableStyles();
          // Adjust some widths for Safari only.
          $('.safari .htCore col').each(function( index ) {
            $( this ).css('width', '50%');
          });
          $('.safari .wtHider').css('width', 'auto');
        },*/
        modifyColWidth: function () {
          behaviours.cleanHandsontableStyles();
        },
        afterSelectionEnd: function (r, c, r2, c2) {
          if (cells_info[r][c] && cells_info[r][c]['type'] == "dropdown") {
            window.answerSelectionPopup(r, c, hot1);
          }
          // Enable the scrolling for scrolling div.
          setTimeout(function() {
            window.enableScrollingDiv();
          }, 600);
        },
        beforeOnCellMouseDown: function (event, coords, TD) {

          // We only want to disable scrolling when the cell is an input cell.
          // This is check is because otherwise iPads will disable scrolling for any cell you click on.
          if ($.inArray('htAutocomplete', TD.classList) != -1) {
            // This is a hacky solution/band-aid for a bug in handsontable.
            // When a cell is selected (using tables.selectCell(row, col)),
            // this causes the scrollable div scrollbar to jump to the top.
            // To prevent this, I am temporarily disabling scrolling.
            window.disableScrollingDiv();
          }
      },
      });
      Handsontable.hooks.add('afterChange', function () {
        behaviours.updateValues(hot1);
      });
      this.hasRun = true;
    },
  };
}(jQuery));
