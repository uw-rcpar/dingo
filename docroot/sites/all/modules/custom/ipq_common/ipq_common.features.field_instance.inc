<?php
/**
 * @file
 * ipq_common.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ipq_common_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'tab-content-field_tab_content'.
  $field_instances['tab-content-field_tab_content'] = array(
    'bundle' => 'content',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'tab',
    'field_name' => 'field_tab_content',
    'label' => 'Tab Content',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'tab-file-field_tab_file'.
  $field_instances['tab-file-field_tab_file'] = array(
    'bundle' => 'file',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'tab',
    'field_name' => 'field_tab_file',
    'label' => 'File Upload',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'description_field' => 1,
      'file_directory' => 'ipq-tabs',
      'file_extensions' => 'pdf',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('File Upload');
  t('Tab Content');

  return $field_instances;
}
