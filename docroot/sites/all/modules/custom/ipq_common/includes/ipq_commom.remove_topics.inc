<?php

/**
 * Form to start executing the process of cleaning unncesarry topics
 */
function ipq_common_remove_topics_batch_form($form, $form_state) {
  $form['import'] = array(
    '#type' => 'managed_file',
    '#title' => t('Choose a csv file'),
    '#upload_location' => 'public://task_import',
    '#status' => FILE_STATUS_PERMANENT,
    '#name' => 'files[]',
    '#upload_validators' => array(
      'file_validate_extensions' => array('csv'),
    ),
  );
  $form['actions']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Start Update Sessions Process'),
    '#submit' => array('ipq_common_remove_topics_submit'),
  );

  return $form;
}

/**
 * Operations of the batch to start cleaning each question with unnecesary topics
 */
function ipq_common_remove_topics_submit($form, $form_state) {
  $operations = array();
  // Get the URI file
  $uri = db_query("SELECT uri FROM {file_managed} WHERE fid = :fid", array(':fid' => $form_state['input']['import']['fid'],
      ))->fetchField();

  if (!empty($uri)) {
    if (file_exists(drupal_realpath($uri))) {
      // Open the csv
      $handle = fopen(drupal_realpath($uri), "r");
      while (($data = fgetcsv($handle, 0, ',', '"')) !== FALSE) {
        //Ignore the information row
        if (is_numeric($data[0])) {
          $operations[] = array('ipq_common_clean_topics', array($data));
        }
      }
    }
  }
  // Batch Structure
  $batch = array(
    'operations' => $operations,
    'finished' => 'ipq_common_finished_clean_batch',
    'title' => t('Cleaning Question Pool'),
    'init_message' => t('Inicializing Cleaning Process.'),
    'progress_message' => t("Processed @current out of @total Chapters."),
    'error_message' => t('Error'),
    'file' => drupal_get_path('module', 'ipq_common') . '/includes/ipq_commom.remove_topics.inc',
  );
  batch_set($batch);
}

/**
 * Last message when the batch finish it's procesing.
 */
function ipq_common_finished_clean_batch(){
  drupal_set_message("All unnecesary topics have been removed");
}

/**
 * Remove unncesary topcis per question
 * Using the relationship between topic and question
 * Also validate to apply only in 2020 exam version
 * @param 
 *  - Data in CSV
 */
function ipq_common_clean_topics($data) {
  try {
    // Get the entity id
    $sql = "select t4.field_ipq_topic_target_id, t4.entity_id from field_data_field_section_per_exam_version as t1,"
        . " eck_node_ref_by_exam_version as t2, field_data_field_exam_version_single_val as t3, "
        . "field_data_field_ipq_topic as t4 where t1.entity_id = :question and "
        . "t1.field_section_per_exam_version_target_id = t2.id and t3.entity_id = id "
        . "and t3.field_exam_version_single_val_tid =  25441 and t4.entity_id = t2.id";
    $result = db_query($sql, array(
      ':question' => $data[0],
    ));
    if ($result) {
      $topic_id = end(explode("|", $data[1]));
      while ($row = $result->fetchAssoc()) {
        // Only save the last element in topic list
        if($topic_id != $row['field_ipq_topic_target_id']){
          db_delete('field_data_field_ipq_topic')
            ->condition('field_ipq_topic_target_id', $row['field_ipq_topic_target_id'])
            ->condition('entity_id', $row['entity_id'])
            ->execute();
        }
      }
    }
  }
  catch (EntityMetadataWrapperException $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}