<?php

/**
 * A helper function for question-type sub-modules to call in their implementations of hook_install()
 * This function will add a record in the ipq_question_types table.
 * @param string $node_type
 * - Machine name of a node type
 * @param $label
 * - Human readable name of the question type
 * @param float $numeric_type
 * - Numeric type of this question
 */
function ipq_common_question_type_install_helper($node_type, $label, $numeric_type, $question_group) {
  // Try to make an existing record active first
  $rows_changed = ipq_common_question_type_enable_helper($node_type);

  // If we found an existing row to make active, we're done
  if($rows_changed > 0) {
    return;
  }

  // Delete existing records with this machine name.  This should not be necessary, but could prevent an error in
  // situations where a module was not uninstalled properly or it's machine name changed without a thorough update.
  // The type, aka machine name needs to be unique, in the future it should probably be the primary key.
  db_delete('ipq_question_types')
    ->condition('type', $node_type)
    ->execute();

  // Clean install: Insert a new record
  db_insert('ipq_question_types')
    ->fields(array(
      'name'         => $label,
      'type'         => $node_type,
      'numeric_type' => $numeric_type,
      'question_group' => $question_group,
      'is_active'    => 1
    ))->execute();
}

/**
 * A helper function for question-type sub-modules to call in their implementations of hook_uninstall()
 * This function will update the corresponding row in 'ipq_question_types' to be inactive.
 * @param string $node_type
 * - Machine name of a node type
 */
function ipq_common_question_type_uninstall_helper($node_type) {
  // We don't ever want to remove records from this table, so we'll just disable for now.
  return ipq_common_question_type_disable_helper($node_type);

  db_delete('ipq_question_types')
    ->condition('type', $node_type)
    ->execute();

  // Get the question type ID from the ipq_question_types table
  $question_type_id = db_query('SELECT id FROM {ipq_question_types} WHERE type = :type', array(':type' => $node_type))->fetchField();

  // Remove all questions of this type from the question pool
  db_delete('ipq_question_pool')
    ->condition('ipq_question_types_id', $question_type_id)
    ->execute();
}

/**
 * A helper function for question-type sub-modules to call in their implementations of hook_enable()
 * Set the corresponding row in 'ipq_question_types' to active.
 * @param string $node_type
 * - Machine name of the question's node type
 * @return int
 * - Returns the number of rows affected
 */
function ipq_common_question_type_enable_helper($node_type) {
  return db_update('ipq_question_types')
    ->fields(array('is_active' => 1))
    ->condition('type', $node_type)
    ->execute();
}

/**
 * A helper function for question-type sub-modules to call in their implementations of hook_disable()
 * Set the corresponding row in 'ipq_question_types' to inactive.
 * @param string $node_type
 * - Machine name of the question's node type
 */
function ipq_common_question_type_disable_helper($node_type) {
  db_update('ipq_question_types')
    ->fields(array('is_active' => 0))
    ->condition('type', $node_type)
    ->execute();

  // Get the question type ID from the ipq_question_types table
  $question_type_id = db_query('SELECT id FROM {ipq_question_types} WHERE type = :type', array(':type' => $node_type))->fetchField();

  // Remove all questions of this type from the question pool
  db_delete('ipq_question_pool')
    ->condition('ipq_question_types_id', $question_type_id)
    ->execute();
}
