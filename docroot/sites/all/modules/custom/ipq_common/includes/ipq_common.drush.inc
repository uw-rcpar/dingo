<?php

/**
 * @file
 * Drush implementation for the ipq_common module.
 */

/**
 * Implements hook_drush_command().
 */
function ipq_common_drush_command() {
    $items = array();
    $items['ipq-session-delete'] = array(
        'description' => 'Delete specific session.',
        'aliases' => array('ipqsd'),
        'callback' => 'ipq_common_ipq_session_delete',
        'arguments'   => array(
            'session_id'     => "Session ID",
        ),
    );
    return $items;
}

/**
 * Implements hook_drush_help().
 */
function ipq_common_drush_help($section) {
    switch ($section) {
        case 'drush:ipq-session-delete':
            return dt("This command will delete the ipq session specified as parameter");
    }
}

/**
 * Delete ipq_session and data related with it
 * @param session_id the session_id of the ipq_session
 */
function ipq_common_ipq_session_delete($session_id) {

    if(!isset($session_id)) {
        return drush_user_abort('Session was not specified');
    }

    if (!drush_confirm(dt('Are you sure you want to delete the ' . $session_id . ' ipq_session?'))) {
        return drush_user_abort();
    }

    drush_log(dt('Deleting ipq sessions ' . $session_id . '.'), 'ok');


    $result = db_query('SELECT id, uid FROM ipq_saved_sessions WHERE session_id = :session_id', array(':session_id' => $session_id));
    foreach($result as $key => $session) {
        //Delete session and the data related with it
        db_delete('ipq_saved_sessions')
        ->condition('id', $session->id)
        ->condition('uid', $session->uid)
        ->execute();

        $c = new RCPARSmartPathCache($session->uid);
        $sessions_data = db_query('SELECT id, chapter_id FROM ipq_saved_session_data WHERE ipq_saved_sessions_id = :session_id', array(':session_id' => $session->id));
        foreach($sessions_data as $index => $session_data) {
            db_delete('ipq_saved_session_data')
            ->condition('id', $session_data->id)
            ->condition('uid', $session->uid)
            ->execute();

            db_delete('ipq_notes')
            ->condition('ipq_saved_session_data_id', $session_data->id)
            ->condition('uid', $session->uid)
            ->execute();

            // Clear SmartPath cache for this user, for this chapter
            $c->invalidateChapterCache($session_data->chapter_id); // Updated chapter, if it's changed
            $c->saveCache();
        }

        db_delete('rcpar_cp_ipq_map')
        ->condition('ipq_saved_sessions_id', $session->id)
        ->execute();

        db_delete('ipq_session_overview')
        ->condition('ipq_saved_sessions_id', $session->id)
        ->condition('uid', $session->uid)
        ->execute();
    }

    drush_log(dt('IPQ session deleted succesfully.'), 'ok');
}
