<?php


/**
 * Generated pass rate statistics based on SmartPath targets met and other metrics
 */
function ipq_common_statistics_batch_form($form, &$form_state) {
  $form['groupinfo'] = array(
    '#type' => 'fieldset',
    '#title' => 'Existing target groups',
    '#description' => 'These are the group names that exist in the database with SmartPath statistics that have been generated so far',
    'contents' => array(
      '#markup' => implode('<br>', db_query("SELECT DISTINCT groupname FROM ipq_stats_chapter_avgs")->fetchCol()),
    ),
  );

  // Group name
  $form['groupname'] = array(
    '#type' => 'textfield',
    '#title' => 'Group name',
    '#default_value' => IPQ_SMARTPATH_COMPARE_GROUP,
    '#description' => 'A name to identify this new data set. Current groups: <br>' .
      implode('<br>', db_query("SELECT DISTINCT groupname FROM ipq_stats_chapter_avgs")->fetchCol()) .
      '<br><br>The current group being used for SmartPath targets is: <strong>' . IPQ_SMARTPATH_COMPARE_GROUP . '</strong>',
    '#required' => TRUE,
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Build SmartPath targets',
  );

  return $form;
}

function ipq_common_statistics_batch_form_submit($form, &$form_state) {
  ipq_common_statistics_batch($form_state['values']['groupname']);
}

/**
 * The batch callback.
 */
function ipq_common_statistics_batch($groupname) {
  $batch = array(
    'operations' => array(),
    'finished' => 'ipq_common_statistics_batch_finished',
    'title' => t('IPQ Statistics Stats'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'ipq_common') . '/includes/smartpath.batch.stats.inc'
  );

  // If the user did not take at least this number of questions for the given exam section, we will
  // not use their data for statistical analysis - they are clearly not using IPQ in earnest for
  // their studies.
  $minimum_questions_per_section = IPQ_SMARTPATH_STATS_MIN_Q_PER_SEC;

  $ct = new RCPARCourseTree();

  $groups = array(
    IPQ_SMARTPATH_COMPARE_GROUP => 'score >= 75', // All Passing
    'smartpath2'                => 'score >= 75', // All Passing
    'passing2018only'           => 'score >= 75 AND exam_date > 1514764800', // All Passing
    'passingmultiswrong'        => 'score >= 75', // Passing, but multiple attempts answers are marked wrong
    '>=85'                      => 'score >= 85', // Passing strongly
    '>=85updated'               => 'score >= 85', // Passing strongly
    '>=75 AND <=84'             => 'score >=75 AND score <=84', // Passing, but not amazing
    '<=74'                      => 'score <= 74', // ALL Failing
    '>=65 AND <=74'             => 'score >=65 AND score <=74', // Failing, but not badly
    '<=64'                      => 'score <= 64', // Failing badly
  );

  foreach ($ct->getAllSectionOpts() as $section_nid => $section) {
    // Find all the users that passed this section
    foreach (db_query("SELECT * FROM eck_exam_scores WHERE " . $groups[$groupname] . " AND section = :section", array(':section' => $section)) as $user_passing) {

      $batch['operations'][] = array('ipq_common_statistics_batch_process', array($user_passing, $section, $groupname));

      // For testing only, limits the number of batch operations
      //if(sizeof($batch['operations']) > 3) {
      //  break 2;
      //}
    }
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/rcpar/ipq/config/smartpath/targetdata'); // The path to redirect to when done.
  }
}

/**
 * Process a user's IPQ data for statistics
 * @param object $user
 * - A row object from the eck_exam_scores that represents a user with a passing score
 * @param string $section\
 * - 'AUD', 'BEC', 'FAR', or 'REG'
 * @param $context
 */
function ipq_common_statistics_batch_process($user, $section, $groupname, &$context) {
  $ct = new RCPARCourseTree();
  $section_nid = $ct->getSectionIdFromName($section);

  // Determine if the user has enough IPQ data in this section to meet our criteria
  // If not, we will skip them and not use their data. We'll ignore skipped questions
  // NOTE: the batch process runs out of memory or times out if we check this for every user. We can do it during processing
  $minimum_questions_per_section = IPQ_SMARTPATH_STATS_MIN_Q_PER_SEC;
  $meets_threshold = db_query("SELECT count(*)
    FROM ipq_saved_session_data 
    WHERE uid = :uid AND section_id = :sectionid AND created_on < :examdate AND status <> 2
    LIMIT $minimum_questions_per_section", array(
      ':uid' => $user->uid,
      ':sectionid' => $section_nid,
      ':examdate' => $user->exam_date
    ))->fetchField()
    >= $minimum_questions_per_section;
  if (!$meets_threshold) {
    return;
  }

  // Get ALL chapters, irrespective of exam version or section. Note that this is pretty inefficient,
  // but there is not a clear cut way to know which exam version the user studied for, and the exam
  // version is needed to get a list of chapters for a given part. We will make sure to filter IPQ
  // data queries by section AND chapter despite the fact that there will be invalid combinations,
  // that produce no results, this will ensure that we don't miss ANY applicable IPQ data that the
  // user may have generated.
  $chapter_ids = db_query("SELECT nid FROM node WHERE type = 'rcpa_chapter'")->fetchCol();

  // Store the groupname that this data will be tagged with
  $context['results']['groupname'] = $groupname;

  foreach ($chapter_ids as $chapter_nid) {
    // This might be a chapter that's not in this section. To avoid unnecessary querying, we'll check what
    // section it is in which is an expensive operation, but probably less so than making querying for every
    // single chapter.
    $chapters_section_nid = RCPARCourseTree::getSectionForChapter($chapter_nid);
    if ($chapters_section_nid != $section_nid) {
      continue;
    }

    $where_clause = 'WHERE uid = :uid AND chapter_id = :chapterid AND created_on < :examdate AND status <> 2';
    $where_replacements = array(
      ':uid' => $user->uid,
      ':examdate' => $user->exam_date,
      ':chapterid' => $chapter_nid,
    );


    // Get the latest score for each distinct question the user answered in a chapter, as well as number of times
    // the question was attempted
    $latest_scores = ipq_common_user_get_chapter_trending_avg_and_questions($user->uid, $chapter_nid, $user->exam_date, TRUE);

    // Don't store this users statistics if they didn't do any quizzes on this chapter
    if (count($latest_scores) == 0) {
      continue;
    }

    // Total average for chapter:
    $context['results']['chapter_averages'][$chapter_nid][$user->uid] = db_query("
        SELECT FLOOR(score / max_score * 100) as avg
        FROM ipq_saved_session_data i
        $where_clause
        GROUP BY ipq_question_id", $where_replacements)->fetchField();

    // Distinct questions: Get the number of distinct questions answered byt his user for this chapter
    $context['results']['chapter_distinct'][$chapter_nid][$user->uid] = sizeof($latest_scores);

    // Total questions: Get the total number of questions answered by this user for this chapter
    $context['results']['chapter_total'][$chapter_nid][$user->uid] = db_query("
          SELECT count(ipq_question_id) FROM ipq_saved_session_data 
          $where_clause", $where_replacements)->fetchField();

    // Store our values
    $context['results']['chapter_trending_averages'][$chapter_nid][$user->uid] = 0;
    foreach ($latest_scores as $question_id => $score) {
      /* Note: this block of code works perfectly well, but I suspect that with so many records that passing this
       * data around to each batch job process is ultimately leading to memory exhaustion.
       * $context['results']['question_averages'][$question_id][] = $score->avg;
       * $context['results']['question_attempts'][$question_id][] = $score->attempts;
       */

      // Add up scores to get an average
      $context['results']['chapter_trending_averages'][$chapter_nid][$user->uid] += $score->avg;
    }
    // Divide out scores by number of questions answered to get the chapter average for this user
    $context['results']['chapter_trending_averages'][$chapter_nid][$user->uid] = ceil($context['results']['chapter_trending_averages'][$chapter_nid][$user->uid] / count($latest_scores));
  }
}


/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function ipq_common_statistics_batch_finished($success, $results, $operations){
  if ($success) {
    $groupname = $results['groupname'];

    drupal_set_message('Batch completed!');

    // Calculate standard deviation for each chapter average score
    foreach (array_keys($results['chapter_trending_averages']) as $chapter_nid) {
      drupal_set_message('Chapter ' . $chapter_nid . ' sample size:' . count($results['chapter_trending_averages'][$chapter_nid]));
      $results['chapter_std_deviation'][$chapter_nid] = stats_standard_deviation($results['chapter_trending_averages'][$chapter_nid]);
    }

    // Calculate standard deviation for each question average score
    foreach (array_keys($results['question_averages']) as $question_id) {
      $results['question_std_deviation'][$question_id] = stats_standard_deviation($results['question_averages'][$question_id]);
    }

    // Calculate standard deviation for each average question attempts
    foreach (array_keys($results['question_attempts']) as $question_id) {
      $results['question_attempts_std_deviation'][$question_id] = stats_standard_deviation($results['question_attempts'][$question_id]);
    }

    // Write chapter averages to database
    foreach ($results['chapter_trending_averages'] as $chapter_nid => $avgs) {
      $trending_average = array_sum($avgs) / count($avgs);
      $average = array_sum($results['chapter_averages'][$chapter_nid]) / count($results['chapter_averages'][$chapter_nid]);
      $total_questions = array_sum($results['chapter_total'][$chapter_nid]) / count($results['chapter_total'][$chapter_nid]);
      $distinct_questions = array_sum($results['chapter_distinct'][$chapter_nid]) / count($results['chapter_distinct'][$chapter_nid]);

      // Build a table record
      $record = array(
        'chapter_id' => $chapter_nid,
        'average' => $average,
        'total_questions' => $total_questions,
        'distinct_questions' => $distinct_questions,
        'trending_average' => $trending_average,
        'sd' => $results['chapter_std_deviation'][$chapter_nid],
        'groupname' => $groupname,
        'updated_on' => REQUEST_TIME,
      );

      // Remove the previous record for this chapter and group and write in a new one
      db_query("DELETE FROM ipq_stats_chapter_avgs WHERE chapter_id = :chapid AND groupname = :groupname",
        array(':chapid' => $chapter_nid, ':groupname' => $groupname));
      drupal_write_record('ipq_stats_chapter_avgs', $record);
    }

    // Write question averages to database
    foreach ($results['question_averages'] as $question_id => $avgs) {
      $trending_average = array_sum($avgs) / count($avgs);

      $record = array(
        'ipq_question_id' => $question_id,
        'trending_average' => $trending_average,
        'sd' => $results['question_std_deviation'][$question_id],
        'groupname' => $groupname,
        'updated_on' => REQUEST_TIME,
      );

      // Remove the previous record for this question and group and write in a new one
      db_query("DELETE FROM ipq_stats_quest_avgs WHERE ipq_question_id = :qid AND groupname = :groupname",
        array('::qid' => $question_id, ':groupname' => $groupname));
      drupal_write_record('ipq_stats_quest_avgs', $record);
    }

    // Write question attempts to database
    foreach ($results['question_attempts'] as $question_id => $avgs) {
      $trending_average = array_sum($avgs) / count($avgs);

      $record = array(
        'ipq_question_id' => $question_id,
        'trending_average' => $trending_average,
        'sd' => $results['question_attempts_std_deviation'][$question_id],
        'groupname' => $groupname,
        'updated_on' => REQUEST_TIME,
      );

      // Remove the previous record for this question and group and write in a new one
      db_query("DELETE FROM ipq_stats_quest_attempts WHERE ipq_question_id = :qid AND groupname = :groupname",
        array('::qid' => $question_id, ':groupname' => $groupname));
      drupal_write_record('ipq_stats_quest_attempts', $record);
    }
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}