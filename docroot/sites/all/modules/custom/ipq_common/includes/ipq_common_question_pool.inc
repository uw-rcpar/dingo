<?php

/**
* Remove all question with the current chapter from the Question Pool
* Also remove topic_ref_by_exam_version with relationship to the Chapter
* @param
*  - node of type Chapter to check
*/
function ipq_common_clean_chapter_questions($node) {
  try {
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $entitys = $node_wrapper->field_topic_per_exam_version->value();
    foreach ($entitys as $entity) {
      entity_delete('node_ref_by_exam_version', $entity->id);
    }
    // Delete the questions from the pool that have the current chapter
    $query = "
      DELETE FROM ipq_question_pool
      WHERE chapter_id = :chapter_id";
    $result = db_query($query, array(
      ":chapter_id" => $node->nid,
        )
    );
  }
  catch (EntityMetadataWrapperException $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}

/**
* Clean the content relationships by course
* @param
*  - node of type course to be clean
*/
function ipq_common_clean_questions_by_course($node) {
  try {
    $node_wrapper = entity_metadata_wrapper('node', $node);
    foreach ($node_wrapper->field_chapter_per_exam_version->value() as $chapter_per_exam) {
      $entity_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $chapter_per_exam);
      // Get all chapters in the entity
      $chapters = $entity_wrapper->field_topic_exam_chapter_ref->value();
      foreach ($chapters as $chapter) {
        $query = "DELETE FROM ipq_question_pool WHERE chapter_id = :chapter_id";
        $result = db_query($query, array(
          ":chapter_id" => $chapter->nid,
            )
        );
      }
      entity_delete('node_ref_by_exam_version', $chapter_per_exam->id);
    }
  }
  catch (EntityMetadataWrapperException $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}

/**
* Clean the relationships of the topics
* @param
* - node of type topic to be clean
*/
function ipq_commom_clean_topics_relationships($node) {
  try {
    // Get all topic_ref_by_exam_version by topic
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node_ref_by_exam_version')
        ->entityCondition('bundle', 'topic_ref_by_exam_version')
        ->fieldCondition('field_topic_reference', 'target_id', $node->nid, '=');
    $result = $query->execute();
    if (isset($result['node_ref_by_exam_version'])) {
      foreach ($result['node_ref_by_exam_version'] as $value) {
        $entity = array_shift(entity_load('node_ref_by_exam_version', array($value->id)));
        $entity_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $entity);
        // if the entity has more that one topic
        if (count($entity_wrapper->field_topic_reference->value()) > 1) {
          // Remove only the topic
          db_query("DELETE FROM field_data_field_topic_reference WHERE field_topic_reference_target_id = :nid and bundle = 'topic_ref_by_exam_version'", array(":nid" => $node->nid));
        }
        else {
          // Remove the entity that only has one topic
          entity_delete('node_ref_by_exam_version', $value->id);
        }
      }
    }
    // Clean cou_chap_top_ref_by_exam_version entity
    ipq_commom_clean_chap_top_ref($node->nid);
    db_query("DELETE FROM ipq_question_pool WHERE topic_id = :nid", array(':nid' => $node->nid));
  }
  catch (EntityMetadataWrapperException $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}

/**
* Clean cou_chap_top_ref_by_exam_version entity by topic
* @param
*  - int $topic_id the id of the topic to clean
*/
function ipq_commom_clean_chap_top_ref($topic_id) {
  try {
    // Get all cou_chap_top_ref_by_exam_version with specific topic.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node_ref_by_exam_version')
        ->entityCondition('bundle', 'cou_chap_top_ref_by_exam_version')
        ->fieldCondition('field_ipq_topic', 'target_id', $topic_id, '=');
    $result = $query->execute();
    if (isset($result['node_ref_by_exam_version'])) {
      foreach ($result['node_ref_by_exam_version'] as $value) {
        $entity = array_shift(entity_load('node_ref_by_exam_version', array($value->id)));
        $entity_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $entity);
        // if the entity has more that one topic
        if (count($entity_wrapper->field_ipq_topic->value()) > 1) {
          // Remove only the topic
          db_query("DELETE FROM field_data_field_ipq_topic WHERE field_ipq_topic_target_id = $topic_id and bundle = 'cou_chap_top_ref_by_exam_version'", array(":nid" => $topic_id));
        }
        else {
          // Remove the entity that only has one topic
          entity_delete('node_ref_by_exam_version', $value->id);
        }
      }
    }
  }
  catch (EntityMetadataWrapperException $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}

/**
* Validate if the topic has a validate chapter
* @param
*  - int $topic_id the id of the topic to validate.
* @param
*  - int $exam_version the id of the exam version to check.
* @param
*  - int $chapter_id the id of the current chapter.
* @return bool
*  true if the has a good chapter, false otherwhise
*/
function ipq_common_validate_topic($topic_id, $exam_version, $chapter_id){
  $query = db_query("
    select section.entity_id from 
      field_data_field_topic_per_exam_version section,  
      field_data_field_topic_reference topic, 
      field_data_field_exam_version_single_val exam 
    where 
      topic.field_topic_reference_target_id = :topic 
      and exam.field_exam_version_single_val_tid = :version 
      and exam.entity_id = topic.entity_id 
      and exam.entity_id = section.field_topic_per_exam_version_target_id 
      and section.entity_id = :chapter", 
      array(
        ':topic' => $topic_id,
        ':version' => $exam_version,
        ':chapter' => $chapter_id
      )
    );
  if ($chapter_record = $query->fetchObject()) {
    return true;
  }
  return false;
}

/**
* Generate the Question pool according with the changes on topic
* Delete from the question if node is unpublished.
* @param
*  - Node of type topic to regenarate the Question pool
*/
function ipq_commom_topic_update_question_pool($topic_node, $exam_version = false) {
  if ($topic_node->status == NODE_NOT_PUBLISHED) {
    // Delete the questions in the pool with the current topic
    db_query("DELETE FROM ipq_question_pool WHERE topic_id = :nid", array(':nid' => $topic_node->nid));
  }
  else {
    try {
      // Get all cou_chap_top_ref_by_exam_version by topic
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node_ref_by_exam_version')
          ->entityCondition('bundle', 'cou_chap_top_ref_by_exam_version')
          ->fieldCondition('field_ipq_topic', 'target_id', $topic_node->nid, '=');
      $result = $query->execute();
      if (isset($result['node_ref_by_exam_version'])) {
        foreach ($result['node_ref_by_exam_version'] as $value) {
          $entity = array_shift(entity_load('node_ref_by_exam_version', array($value->id)));
          $entity_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $entity);
          // The field_data_field_section_per_exam_version is used to save the relation between the question and the entity
          $question = db_query("
            SELECT entity_id FROM field_data_field_section_per_exam_version
            WHERE field_section_per_exam_version_target_id = :entity",
            array(":entity" => $value->id)
          );
          if ($question_record = $question->fetchObject()) {
            $node = node_load($question_record->entity_id);
            if($node->status == NODE_PUBLISHED){
              $question_type = db_query('SELECT * FROM ipq_question_types WHERE type = :question_type ', array(':question_type' => $node->type));
              if ($type_record = $question_type->fetchObject()) {  
                $chapter = $entity_wrapper->field_ipq_chapter->value();
                $section = $entity_wrapper->field_ipq_course_section->value();
                // If exam version is diferent of false we use the param.
                $exam = ($exam_version) ? $exam_version : $entity_wrapper->field_exam_version_single_val->value();
                if(ipq_common_validate_topic($topic_node->nid, $exam->tid, $chapter->nid)){
                  // Regenarate the question pool
                  db_merge('ipq_question_pool')
                    ->key( array(
                              'topic_id' => $topic_node->nid,
                              'chapter_id' => $chapter->nid,
                              'exam_version_id' => $exam->tid,
                              'ipq_questions_id' => $question_record->entity_id
                            )
                          )
                    ->fields(array(
                      'topic_id' => $topic_node->nid,
                      'chapter_id' => $chapter->nid,
                      'section_id' => $section->tid,
                      'exam_version_id' => $exam->tid,
                      'ipq_question_types_id' => $type_record->id,
                      'ipq_questions_id' => $question_record->entity_id
                    ))
                    ->execute();
                }
              }
            }
          }
        }
      }
    }
    catch (Exception $e) {
      watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
    }
  }
}

/**
* Generate the Question pool according with the changes on the Chapter
* Delete from the Question pool if node is unpublished.
* @param
*  - Node of type Chapter to regenarate the Question pool
* @param $exam_version
*  - Exam version term if we want to check specific version
*/
function ipq_commom_update_question_pool_by_chapter($chapter_node, $exam_version = false) {
  try {
    $node_wrapper = entity_metadata_wrapper("node", $chapter_node);
    if ($chapter_node->status == NODE_NOT_PUBLISHED) {
      // Delete the questions in the pool with the current topic
      db_query("DELETE FROM ipq_question_pool WHERE chapter_id = :nid", array(':nid' => $chapter_node->nid));
    }
    else {
      // Deteled all questions from the pool to be recreated those that correspond.
      $query = db_query("DELETE FROM ipq_question_pool WHERE chapter_id = :nid", array(
        ':nid' => $chapter_node->nid,
          )
      );
      // if node is PUBLISHED we will check if all questions in the pool have been addded
      foreach ($node_wrapper->field_topic_per_exam_version->value() as $topic_per_chapter) {
        $entity_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $topic_per_chapter);
        $topics = $entity_wrapper->field_topic_reference->value();
        foreach ($topics as $topic) {
          if ($topic->status == NODE_PUBLISHED) {
            // check all topics if have the correct questions list in the pool
            ipq_commom_topic_update_question_pool($topic, $exam_version);
          }
        }
      }
    }
  }
  catch (Exception $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}

/**
* Generate the Question pool according with the Course.
* Delete from the Question pool if node is unpublished.
* @param
*  - Node of type Couse to regenarate the Question pool
*/
function ipq_commom_course_update_question_pool($course_node) {
  try {
    $node_wrapper = entity_metadata_wrapper("node", $course_node);
    // if node change of status to published
    if ($course_node->status == NODE_PUBLISHED
      && $course_node->original->status != $course_node->status) {
      // if node is PUBLISHED we will check if all questions in the pool have been addded
      foreach ($node_wrapper->field_chapter_per_exam_version->value() as $chapter_per_course) {
        $entity_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $chapter_per_course);
        foreach ($entity_wrapper->field_topic_exam_chapter_ref->value() as $chapter) {
          if ($chapter->status == NODE_PUBLISHED) {
            // check all topics if have the correct questions list in the pool
            ipq_commom_update_question_pool_by_chapter($chapter);
          }
        }
      }
    }
    else if ($course_node->status == NODE_NOT_PUBLISHED) {
      foreach ($node_wrapper->field_chapter_per_exam_version->value() as $chapter_per_course) {
        $entity_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $chapter_per_course);
        foreach ($entity_wrapper->field_topic_exam_chapter_ref->value() as $chapter) {
          db_query("DELETE FROM ipq_question_pool WHERE chapter_id = :nid", array(':nid' => $chapter->nid));
        }
      }
    }
  }
  catch (Exception $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}

/**
* Verify if the list of chapter per couse change
* Delete from the Question pool if the chapter have been removed.
* @param
*  - entity of type chapter_ref_by_exam_version
*/
function ipq_common_delete_chapter_on_course($entity) {
  $node_wrapper = entity_metadata_wrapper("node_ref_by_exam_version", $entity);
  $exam_version = $node_wrapper->field_exam_version_single_val->value();
  $original_wrapper = entity_metadata_wrapper("node_ref_by_exam_version", $entity->original);
  $current_chapters = array();
  $original_chapters = array();
  // Get Current chapters
  foreach ($node_wrapper->field_topic_exam_chapter_ref->value() as $chapter) {
    $current_chapters[] = $chapter->nid;
  }
  foreach ($original_wrapper->field_topic_exam_chapter_ref->value() as $chapter) {
    $original_chapters [] = $chapter->nid;
    // If the Chapter id have been removed from the current course.
    if (!in_array($chapter->nid, $current_chapters)) {
      db_query("DELETE FROM ipq_question_pool WHERE chapter_id = :nid and exam_version_id = :exam", array(
        ':nid' => $chapter->nid,
        ':exam' => $exam_version->tid
          )
      );
    }
  }
  ipq_common_verify_add_chapter($node_wrapper->field_topic_exam_chapter_ref->value(), $original_chapters, $exam_version);
}

/**
* Generate the Question pool according with the new chapters in the course.
* @param $current_chapters
*  - List of Chapters in the Couse.
* @param $original_chapters
*  - List of nids from the original chapters in the Course.
* @param $exam_version
*  - Exam version term of the chapter_ref_by_exam_version entity.
*/
function ipq_common_verify_add_chapter($current_chapters, $original_chapters, $exam_version) {
  foreach ($current_chapters as $chapter) {
    if (!in_array($chapter->nid, $original_chapters)) {
      ipq_commom_update_question_pool_by_chapter($chapter, $exam_version);
    }
  }
}
