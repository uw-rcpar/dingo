<?php
/**
 * Page callback for ipq/review
 * Session review main page. Normally, this is a student reviewing their own IPQ session. In theory, an instructor could
 * also review another user's session, but the UI doesn't currently allow it.
 *
 * @return array
 * - A Drupal form array
 */
function ipq_user_session_review($question_id = NULL, &$ipq_session = null) {
  global $user;
  $current_section =  ipq_common_get_default_section();

  if(!$ipq_session && isset($_SESSION['ipq_preview_temp_session']) && isset($_SESSION['ipq_session'])){
    // the session can come by parameter, or in $_SESSION ipq_preview_temp_session and/or ipq_session
    // if we have both ipq_preview_temp_session and ipq_session defined
    // we use the updated_on colum to choose which one we should use
    if ($_SESSION['ipq_preview_temp_session']['updated_on'] < $_SESSION['ipq_session']['updated_on']){
      $ipq_session = &$_SESSION['ipq_session'];
    } else {
      $ipq_session = &$_SESSION['ipq_preview_temp_session'];
    }
  }
  if(!$ipq_session && isset($_SESSION['ipq_preview_temp_session'])){
    $ipq_session = &$_SESSION['ipq_preview_temp_session'];
  }
  else if(!$ipq_session && isset($_SESSION['ipq_session'])){
    $ipq_session = &$_SESSION['ipq_session'];
  }
  if ((!isset($ipq_session)) || (!ipq_common_perms($user->uid, $current_section))){
      return MENU_ACCESS_DENIED;
  }

  // Before we can continue, we need to check if the user has set the exam version for this
  // section
  ipq_common_exam_version_for_session_check($ipq_session);

  // if user requested a particular question, we update the session information to mark it as
  // the current question
  if ($question_id){
    update_session_current_question($ipq_session, $question_id);
  }

  $data = ipq_user_session_review_get_form_data($ipq_session);
  $form = drupal_get_form('ipq_user_session_review_form', $data);
  return $form;
}

/**
 * Build data needed for the ipq_user_session_review_form.
 * Also handles submits/question navigation within the form to determine which question to show, etc.
 *
 * @param $ipq_session
 * @return array
 */
function ipq_user_session_review_get_form_data($ipq_session, $question_p = NULL) {

  // Already confirmed it's set so use it.
  $session_config = $ipq_session['session_config'];
  $question = isset($question_p) ? $question_p : node_load($ipq_session['session_config']['current_question_id']);

  $current_type_list = $session_config['question_list'][$session_config['current_question_type']];
  $current_question_index = array_search($session_config['current_question_id'], $current_type_list);

  // now, we get the prev and next questions info
  // prev
  $previous_question = null;
  $questions_types_list = array_keys($session_config['question_list']);
  if ($current_question_index != 0){
    $previous_question = $current_type_list[$current_question_index-1];
  } else {

    // we look for the current question type index
    $current_qt_index = array_search($session_config['current_question_type'], $questions_types_list);
    // if we have a next element in the session question types, we choose that one to get the next question
    if ( $current_qt_index > 0 ){
      $prev_type_list = $session_config['question_list'][$questions_types_list[$current_qt_index - 1]];
      $previous_question = end($prev_type_list); // get prev quizlet last element
    } else {
      //  if we don't, that's the first question of the quiz
    }

  }
  // next
  $next_question = null;
  if ($current_question_index < count($current_type_list) - 1){
    $last_in_question_set = false;
    $next_question = $current_type_list[$current_question_index+1];
  } else {
    $last_in_question_set = true;
    // maybe is the last question in the current question set, but we have other questions sets
    // we look for the current question type index
    $next_element = array_search($session_config['current_question_type'], $questions_types_list);

    // if we have a next element in the session question types, we choose that one to get the next question
    if ( $next_element < count($session_config['question_list']) - 1 ){
      $next_type_list = $session_config['question_list'][$questions_types_list[$next_element + 1]];
      $next_question = $next_type_list[0];
    } else {
      //  if we don't, that's the last question of the quiz
    }
  }

  if(!isset($ipq_session['is_temporary']) ) {

    $ipq_session_id = $ipq_session['id'];
    // we also need some information from the ipq_saved_session_data table
    if(isset($ipq_session_id) && isset($current_type_list)
      && is_array($current_type_list) && count($current_type_list) > 0) {
      $results = db_select('ipq_saved_session_data', 'sd')
        ->fields('sd')
        ->condition('ipq_saved_sessions_id', $ipq_session_id, '=')
        ->condition('ipq_question_id', $current_type_list, 'IN')
        ->execute();
    }
    else {
      $results = array();
    }
    // we create this array, to sort the questions in the same order than we have on $current_type_list
    $questions_info = array();
    foreach ($current_type_list as $qid) {
      $questions_info[$qid] = array();
    }
    // now we load the question info inside the array
    foreach ($results as $r) {
      $questions_info[$r->ipq_question_id] = $r;
    }
    $current_question_session_data = ipq_session_get_question_data($ipq_session['id'], $ipq_session['session_config']['current_question_id']);
  } else {
    // for temporary sessions, this lists came preloaded
    $questions_info = $ipq_session['session_config']['questions_info'][$ipq_session['session_config']['current_question_type']];
    $current_question_session_data = $questions_info[$ipq_session['session_config']['current_question_id']];

    $chapter_id = $ipq_session['session_config']['chapters'][0];
    $question_filter = $ipq_session['session_config']['temp_session_question_filter'];
    $questions_type = $ipq_session['session_config']['temp_session_question_type'];
  }
  if($question) {
    $question_status = $question->status;
    $deleted_question = FALSE;
    // For now we treat questions that are not part of the current exam year as deleted
    if (!ipq_common_is_current_version($question)) {
      $deleted_question = true;
    }
  }
  else {
    // if question is deleted
    $question_status = 1;
    $deleted_question = true;
  }

  $data = array(
    'question' => $question,
    'session' => $ipq_session,
    'question_review_display' => '', // this part will be overridden by the corresponding module
    'question_session_data' => $current_question_session_data,
    'navigation_info' => array(
      'session_id' => isset($ipq_session['id']) ? $ipq_session['id'] : null, // when the session is temporary session id is null
      'session_type' => $ipq_session['session_type'],
      'current_type_question_list' => $current_type_list,
      'current_type_questions_information' => $questions_info,
      'previous_question_id' => $previous_question,
      'next_question_id' =>$next_question,
      'last_in_question_set' => $last_in_question_set,
      'question_status' => $question_status,
      'deleted_question' => $deleted_question,
      'current_question_id' => $ipq_session['session_config']['current_question_id']
    )
  );

  // we allow question type modules to insert data inside review display element
  if($question){
    drupal_alter('ipq_question_review_display', $data);
  }


  return $data;
}

function ipq_user_session_review_form($form, &$form_state){
  // we load a coupe of variables to be able to use them inside the template
  $session = $form_state['build_info']['args'][0]['session'];
  $question = $form_state['build_info']['args'][0]['question'];
  $navigation_info = $form_state['build_info']['args'][0]['navigation_info'];
  // we will also need the question index

  // And the exam version
  $exam_version = $session['session_config']['exam_version'];
  // Old sessions doesn't have the $session['session_config']['exam_version'] set
  // for those sessions we assume 2016
  if (!$exam_version){
    $exam_version = "2016";
    $session['session_config']['exam_version'] = $exam_version;
  }

  $next_qid = null;
  // we are going to create an ordered array with all the questions of the session (including all types)
  // we need it to get the current question index and the next question id
  $questions_array = array();
  foreach($session['session_config']['question_list'] as $ql){
    foreach($ql as $qid){
      $questions_array[] = $qid;
    }
  }

  $question_index = array_search($navigation_info['current_question_id'], $questions_array);

  // Deleted questions will fail here, so wrap in a try/catch.
  try {
    /* Get the chapter node using chapter ID */
    $chapter = node_load($form_state['build_info']['args'][0]['question_session_data']['chapter_id']);
    $chapter_wrapper = entity_metadata_wrapper('node', $chapter);
    /* Obtain the chapter title */
    $chapter_title = $chapter_wrapper->title->value();
  }
  catch(EntityMetadataWrapperException $e) {
    watchdog('ipq', t("There was a problem getting the chapter name ". $e->getMessage()), array(), WATCHDOG_ERROR);
  }

  /* Obtain the section and the chapter number (chapter weight) */
  $course_info = db_select('course_stats', 'cs')
    ->fields('cs')
    ->condition('type', 'CHAPTER')
    ->condition('nid', $form_state['build_info']['args'][0]['question_session_data']['chapter_id'])
    ->execute()->fetchObject();

  $section_title = "{$course_info->section} {$course_info->weight} : {$chapter_title}";

  //title to show on the "Review related course topics" section
  $related_title = "{$course_info->section} {$course_info->weight}!section : {$chapter->title}";

  /* Create courseware links as form elements */
  // We verify that the user has an entitlement to the relevant course section before attempting
  // to display any links to that course. Publisher Access users may not have access to the online
  // courses but are still leveraging IPQ functionality via quizzes. Also, although it is not true
  // at the time of this writing, IPQ entitlements will eventually exist that are separate from
  // course entitlements.
  if (isset($GLOBALS['user']->user_entitlements['products'][$course_info->section])) {
    $form['courseware_links'] = array(
      '#type'       => 'container',
      '#id'         => 'courseware-links'
    );

    $form['courseware_links']['header'] = array(
      '#markup' => '<p>Review related course topics</p>',
    );
    global $user;
    $user_current_exam_version = ipq_common_get_exam_version($user, $course_info->section);
    $default_version = exam_version_get_default_version();
    // We want to prevent the users from access to out of date content, so if they already upgraded the exam version
    // and the related links that we are about to show are out of date, we block them and let the user know that they are
    // out of date
    $block_related_topic_links = false;
    if($user_current_exam_version == $default_version && $user_current_exam_version <> $exam_version){
      $block_related_topic_links = true;
    }

    $form['courseware_links']['links'] = array(
      '#prefix' => '<ul>',
      '#suffix' => '</ul>',
      '#markup' => ipq_common_get_courseware_url($question, $section_title, $related_title, $exam_version, $block_related_topic_links),
    );
  }
  /* End courseware links */

  $form['session'] = array(
    '#type' => 'value',
    '#value' => $session,
  );

  $form['question'] = array(
    '#type' => 'value',
    '#value' => $question,
  );

  $form['navigation_info'] = array(
    '#type' => 'value',
    '#value' => $navigation_info,
  );

  $form['question_index'] = array(
    '#type' => 'value',
    '#value' => $question_index+1, // on the template, we use a 1 based index
  );

  $form['previous_question_id'] = array(
    '#type' => 'value',
    '#value' => $navigation_info['previous_question_id'],
  );

  $form['next_question_id'] = array(
    '#type' => 'value',
    '#value' => $navigation_info['next_question_id'],
  );
  $form['question_review_display'] = array(
    '#type' => 'value',
    '#value' => $form_state['build_info']['args'][0]['question_review_display'],
  );
  $form['time_spent'] = array(
    '#type' => 'value',
    '#value' => $form_state['build_info']['args'][0]['question_session_data']['time'],
  );
  // we are using this to implement the footer pagination
  $form['force_question'] = array(
    '#type' => 'hidden',
    '#attributes' => array('id' => 'force_question'),
  );

  // now, we add the buttons
  $form['actions']['retake'] = array(
    '#type' => 'submit',
    '#name' => 'retake',
    '#value' => t('Retake This Quiz'),
  );

  if($navigation_info['previous_question_id']){
    $form['actions']['prev_question'] = array(
      '#type' => 'submit',
      '#name' => 'prev_question',
      '#value' => t('Previous'),
    );
  }

  if($navigation_info['next_question_id']){
    $form['actions']['next_question'] = array(
      '#type' => 'submit',
      '#name' => 'next_question',
      '#value' => t('Next'),
    );
  }

  // End Review
  if($navigation_info['next_question_id'] == null && $navigation_info['last_in_question_set']){
    $form['actions']['next_question'] = array(
      '#type' => 'submit',
      '#name' => 'exit_review',
      '#value' => t('Exit'),
    );
  }
  if($question && $question->status ){
    //add Notes feature, only valid for non temporary sessions
    _add_notes_form($form, $form_state, $session, $question);
  }

  return $form;
}

function ipq_user_session_review_form_submit($form, &$form_state){
  $session = $form_state['build_info']['args'][0]['session'];

  if (isset($session['is_temporary'])){
    $ipq_session = &$_SESSION['ipq_preview_temp_session'];
  } else {
    $ipq_session = &$_SESSION['ipq_session'];
  }

  if($form_state['triggering_element']['#name'] == 'retake') {
    module_load_include('inc', 'ipq_common', 'includes/ipq_session');
    // retake session: create new session, set it to session var and redirect user to the session take page

    // when the session is temporary, we need to save instead of using the actual 'retake' feature
    // (temporary sessions are not based on existing sessions)
    if ($ipq_session['is_temporary']) {
      $s = create_session_from_temporary($ipq_session);
      $_SESSION['ipq_session'] = $s;
    }
    else {
      ipq_quiz_retake_session($ipq_session['id']);
    }

    if ($ipq_session['session_type'] == 'exam') {
      $form_state['redirect'] = 'ipq/exam/take';
    }
    else {
      $form_state['redirect'] = 'ipq/quiz/take';
    }

  } if(isset($form_state['values']['force_question']) && $form_state['values']['force_question'] != '') {
    // this is how we implement the pagination
    $next_question = $form_state['values']['force_question'];
    update_session_current_question($ipq_session, $next_question);

  } else if($form_state['triggering_element']['#name'] == 'next_question') {
    update_session_current_question($ipq_session, $form_state['values']['next_question_id']);
    $form_state['redirect'] = 'ipq/review';
  } else if($form_state['triggering_element']['#name'] == 'prev_question') {
    update_session_current_question($ipq_session, $form_state['values']['previous_question_id']);
    $form_state['redirect'] = 'ipq/review';
  } else if($form_state['triggering_element']['#name'] == 'exit_review') {
    $form_state['redirect'] = 'ipq/history';
  }
}

