<?php

/**
 * Admin form/settings for SmartPath
 * (/admin/settings/rcpar/ipq/config/smartpath)
 */
function ipq_common_smartpath_settings_form($form, $form_state){
  $form = array();

  // Beta fieldset
  $form['beta'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Beta Program')
  );

  // Beta on/off
  $form['beta']['smartpath_beta_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Access to Beta users only'),
    '#description' => t('When this box is checked, only admins and beta participants can use SmartPath. When unchecked, 
      access will be granted as usual to any users with IPQ access.'),
    '#default_value' => variable_get('smartpath_beta_active', TRUE),
  );

  // Beta emails
  $form['beta']['smartpath_beta_emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Beta participant emails'),
    '#description' => t('Enter email addresses for users who are participating in the SmartPath beta, one per line.'),
    '#default_value' => variable_get('smartpath_beta_emails', ''),
  );

  // Data collection
  $form['data_collection'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Data collection')
  );

  // Data collection
  $form['smartpath'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('SmartPath end user settings')
  );


  return system_settings_form($form);
}


/**
 * Page callback for /admin/settings/rcpar/ipq/config/smartpath/targetdata/report
 * Provides a visual summary of all the statistics for each chapter of the given statistics group. This shows
 * an easy breakdown of where the score ranges are for various states in SmartPath, etc.
 */
function ipq_common_statistics_report() {
  // Make a mini-form with the GET method to choose a group to display

  // Target groups
  $targetgroups  = db_query("SELECT DISTINCT groupname FROM ipq_stats_chapter_avgs")->fetchCol();

  $groupname = !empty($_GET['targetsgroup']) ? $_GET['targetsgroup'] : IPQ_SMARTPATH_COMPARE_GROUP;
  $render = array(
    '#prefix' => '<form method="GET">',
    'targetsgroup' => array(
      '#title' => 'Stat group',
      '#description' => 'Display information about this statistics group',
      '#type' => 'select',
      '#name' => 'targetsgroup',
      '#options' => array_combine(array_values($targetgroups), array_values($targetgroups)),
      '#required' => TRUE,
      '#value' => $groupname,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => 'Show report',
    ),
    '#suffix' => '</form>',
  );

  // Instantiate course tree object
  $ct = new RCPARCourseTree();

  // Foreach AUD, FAR, BEC, & REG
  foreach ($ct->getAllSectionOpts() as $section_tid => $section_name) {
    // Get the chapters for this section
    $chapters = $ct->getChapterOptions($section_tid);

    // Get target data for these chapters in the specified data set
    $data = db_query("SELECT * FROM ipq_stats_chapter_avgs WHERE groupname = :groupname",
      array(':groupname' => $groupname))->fetchAllAssoc('chapter_id');

    // Build a table
    $caption = "<h2>$section_name Targets</h2>";

    $ptm = base_path() . drupal_get_path('module', 'ipq_common') . '/css/img';
    $badimg =   "<img height='16px' width='16px' src='$ptm/indicator-needs-improvement-low.svg' />";
    $nbncimg =  "<img height='16px' width='16px' src='$ptm/indicator-needs-improvement.svg' />";
    $closeimg = "<img style='background-color:#fff;' height='16px' width='16px' src='$ptm/indicator-making-progress.svg' />";
    $goodimg =  "<img height='16px' width='16px' src='$ptm/indicator-targets-met.svg' />";

    $header = array(
      'Chapter',
      'Score target (trending avg) and std dev.',
      $goodimg,
      $closeimg,
      $nbncimg,
      $badimg,
      //'Score ranges',
      'Overall average',
      'Total question attempts',
      'Distinct Questions Attempted',
      'Last Updated'
    );
    $rows = array();



    foreach ($chapters as $chapter_nid => $chapter_name) {
      $chapter_data = $data[$chapter_nid];
      $bad = round($chapter_data->trending_average - ($chapter_data->sd * SMARTPATH_SD_FAR), 1);
      $close = round($chapter_data->trending_average - ($chapter_data->sd * SMARTPATH_SD_CLOSE), 1);
      $good = round($chapter_data->trending_average, 1);
      $sd = round($chapter_data->sd, 1);

      // Build table row
      $rows[] = array(
        $chapter_name, // Chapter name
        "$good% <br>SD: $sd%", // Score & SD

        // Score ranges
        "$good% - 100%",
        "$close% - $good%",
        "$bad% - $close% ",
        "below $bad% ",

        round($chapter_data->average, 1) . '%', // Overall average
        $chapter_data->total_questions, // Total question attempts
        $chapter_data->distinct_questions, // Distinct question attempts
        format_date($chapter_data->updated_on, 'short'), // Last updated
      );
    }

    $vars = array('caption' => $caption, 'header' => $header, 'rows' => $rows);
    $render["table-$section_name"] = array('#markup' => theme('table', $vars));
  }

  $output = render($render);
  return $output;
}

/**
 * Page callback for /admin/settings/rcpar/ipq/config/smartpath/targetdata/comparisonreport
 * Provides a visual summary of all the statistics for each chapter of the given statistics group. This shows
 * an easy breakdown of where the score ranges are for various states in SmartPath, etc.
 */
function ipq_common_smartpath_comparison_report() {
  // Make a mini-form with the GET method to choose groups to compare

  // Stat groups
  $statgroups = ipq_common_smartpath_get_statgroups(TRUE);

  $basegroup = !empty($_GET['basegroup']) ? $_GET['basegroup'] : IPQ_SMARTPATH_COMPARE_GROUP;
  $comparegroup = !empty($_GET['comparegroup']) ? $_GET['comparegroup'] : IPQ_SMARTPATH_COMPARE_GROUP;
  $render = array(
    '#prefix' => '<form method="GET">',
    'basegroup' => array(
      '#title' => 'Baseline group',
      '#description' => 'The baseline group for the comparison.',
      '#type' => 'select',
      '#name' => 'basegroup',
      '#options' => $statgroups,
      '#required' => TRUE,
      '#value' => $basegroup,
    ),
    'comparegroup' => array(
      '#title' => 'Comparison group',
      '#description' => 'The group to compare to.',
      '#type' => 'select',
      '#name' => 'comparegroup',
      '#options' => $statgroups,
      '#required' => TRUE,
      '#value' => $comparegroup,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => 'Show report',
    ),
    '#suffix' => '</form>',
  );

  // Instantiate course tree object
  $ct = new RCPARCourseTree();

  // Foreach AUD, FAR, BEC, & REG
  foreach ($ct->getAllSectionOpts() as $section_tid => $section_name) {
    // Get the chapters for this section
    $chapters = $ct->getChapterOptions($section_tid);

    // Get baseline and comparison data for these chapters in the specified data set
    $basedata = db_query("SELECT * FROM ipq_stats_chapter_avgs WHERE groupname = :groupname",
      array(':groupname' => $basegroup))->fetchAllAssoc('chapter_id');
    $comparedata = db_query("SELECT * FROM ipq_stats_chapter_avgs WHERE groupname = :groupname",
      array(':groupname' => $comparegroup))->fetchAllAssoc('chapter_id');

    // Build a table
    $caption = "<h2>$section_name Targets</h2>";

    $ptm = base_path() . drupal_get_path('module', 'ipq_common') . '/css/img';
    $badimg =   "<img height='16px' width='16px' src='$ptm/indicator-needs-improvement-low.svg' />";
    $nbncimg =  "<img height='16px' width='16px' src='$ptm/indicator-needs-improvement.svg' />";
    $closeimg = "<img style='background-color:#fff;' height='16px' width='16px' src='$ptm/indicator-making-progress.svg' />";
    $goodimg =  "<img height='16px' width='16px' src='$ptm/indicator-targets-met.svg' />";

    $header = array(
      'Chapter',
      'Score target (trending avg) and std dev.',
      $goodimg,
      $closeimg,
      $nbncimg,
      $badimg,
      //'Score ranges',
      'Overall average',
      'Total question attempts',
      'Distinct Questions Attempted',
      'Last Updated'
    );
    $rows = array();

    foreach ($chapters as $chapter_nid => $chapter_name) {
      $bdata = $basedata[$chapter_nid];
      $cdata = $comparedata[$chapter_nid];
      $score = round($bdata->trending_average - $cdata->trending_average, 1);

      $bad1 = round($bdata->trending_average - ($bdata->sd * SMARTPATH_SD_FAR), 1);
      $bad2 = round($cdata->trending_average - ($cdata->sd * SMARTPATH_SD_FAR), 1);
      $close1 = round($bdata->trending_average - ($bdata->sd * SMARTPATH_SD_CLOSE), 1);
      $close2 = round($cdata->trending_average - ($cdata->sd * SMARTPATH_SD_CLOSE), 1);
      $good1 = round($bdata->trending_average, 1);
      $good2 = round($cdata->trending_average, 1);
      $sd = round($bdata->sd - $cdata->sd, 1);

      // Build table row
      $rows[] = array(
        $chapter_name, // Chapter name

        "$score% <br>SD: $sd%", // Score & SD

        // Score ranges
        "Before: $good1% - 100%<br>After: $good2% - 100%<br>",
        "Before: $close1% - $good1%<br>After: $close2% - $good2%",
        "Before: $bad1% - $close1% <br>After: $bad2% - $close2% ",
        "Before: below $bad1% <br>After: below $bad2%",

        round($bdata->average - $cdata->average, 1) . '%', // Overall average
        $bdata->total_questions - $cdata->total_questions , // Total question attempts
        $bdata->distinct_questions - $cdata->distinct_questions, // Distinct question attempts
        format_date($bdata->updated_on, 'short') . ' - ' . format_date($cdata->updated_on, 'short'), // Last updated
      );
    }

    $vars = array('caption' => $caption, 'header' => $header, 'rows' => $rows);
    $render["table-$section_name"] = array('#markup' => theme('table', $vars));
  }

  $output = render($render);
  return $output;
}

/**
 * Admin tool: For a single user, get's all of their IPQ trending averages and distinct questions answered for
 * all four parts of the exam and displays it in a table compared to the target group.
 *
 * @return string
 * - HTML output
 */
function ipq_common_statistics_simulator() {
  $uid = $_GET['uid'];
  $user = user_load($uid);

  // Get the user's name to display and set the page title
  $display_name = '';
  try {
    // Try to get first and last
    $u_wrapper = entity_metadata_wrapper('user', $user);
    $display_name = $u_wrapper->field_first_name->value() . ' ' . $u_wrapper->field_last_name->value();
  }
  catch (EntityMetadataWrapperException $exc) {
    // Default to username
    $display_name = $user->name;
  }
  drupal_set_title("SmartPath: $display_name");

  // The group whose target numbers we're showing and comparing to
  $comparison_group = !empty($_GET['groupname']) ? $_GET['groupname'] : IPQ_SMARTPATH_COMPARE_GROUP;

  // Target groups
  $comparison_groups  = db_query("SELECT DISTINCT groupname FROM ipq_stats_chapter_avgs")->fetchCol();

  $render = array(
    '#prefix' => '<form method="GET">',
    'uid' => array(
      '#title' => 'User ID',
      '#description' => 'The user ID to inspect.',
      '#type' => 'textfield',
      '#name' => 'uid',
      '#required' => TRUE,
      '#value' => $uid,
    ),
    'targetsgroup' => array(
      '#title' => 'Comparison group',
      '#description' => 'This is the stat group that the user will be compared to. Defaults to the currently configured SmartPath stat group.',
      '#type' => 'select',
      '#name' => 'groupname',
      '#options' => array_combine(array_values($comparison_groups), array_values($comparison_groups)),
      '#required' => TRUE,
      '#value' => $comparison_group,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => 'Submit',
    ),
    '#suffix' => '</form>',
  );

  if(!empty($user)) {
    $render['uid']['#prefix'] = "<h3>Currently showing data for User ID: $uid | Username: {$user->name} | Display name: $display_name</h3>";
  }

  $output = '';

  // Instantiate course tree object
  $ct = new RCPARCourseTree();

  // Foreach AUD, FAR, BEC, & REG
  foreach ($ct->getAllSectionOpts() as $section_tid => $section_name) {
    // Get the chapters for this section
    $chapters = $ct->getChapterOptions($section_tid);

    // Get smartpath data for the chapters in this section, for this user
    $data = ipq_common_user_get_smartpath_data($ct, $section_tid, $uid, $comparison_group);

    // Get the most recent exam attempt by this user for this section
    $exam_score = db_query("SELECT score FROM eck_exam_scores 
      WHERE uid = :uid AND section = :sec 
      ORDER BY exam_date DESC LIMIT 1", array(':uid' => $uid, ':sec' => $section_name))->fetchField();
    if(empty($exam_score)) {
      $exam_score = 'Not reported';
    }

    // Build a table
    $caption = "<h2>$section_name</h2>Exam Score: $exam_score";
    $header = array('Chapter', 'Trending Average', 'Success Trending Avg', 'Distinct Questions', 'Success Distinct Question');
    $rows = array();

    foreach ($data as $chapter_nid => $chapter_data) {

      $good = ' - <span style="background-color:green">Good</span>';
      $close = ' - <span style="background-color:yellow">Close</span>';
      $bad = ' - <span style="background-color:red">Bad</span>';

      // Determine if trending average exceeds the average, is close (within 1/3 a standard deviation), or bad
      if($chapter_data['trending_average'] >= $chapter_data['stats']['trending_average']) {
        $trending_met = $good;
      }
      elseif(abs($chapter_data['trending_average'] - $chapter_data['stats']['trending_average']) <= ($chapter_data['stats']['sd'] / 2)) {
        $trending_met = $close;
      }
      else {
        $trending_met = $bad;
      }

      $distinct_met = $chapter_data['distinct_questions'] >= $chapter_data['stats']['distinct_questions'] ? $good : $bad;

      // Build table row
      $rows[] = array(
        $chapters[$chapter_nid],
        round($chapter_data['trending_average'], 1) . $trending_met,
        round($chapter_data['stats']['trending_average'], 1),
        $chapter_data['distinct_questions'] . $distinct_met,
        $chapter_data['stats']['distinct_questions'],
      );
    }

    $vars = array('caption' => $caption, 'header' => $header, 'rows' => $rows);
    $output .= theme('table', $vars);
  }

  $render['table']['#markup'] = $output;

  return $render;
}

/**
 * Page callback for admin/settings/rcpar/ipq/config/smartpath/performancereport
 * Generates a performance report using the pass rate and performance data of an
 * existing stat group.
 * @return array
 * @throws Exception
 */
function ipq_common_smartpath_performance_report() {
  // Target groups - all available groups
  $targetgroups  = db_query("SELECT DISTINCT groupname FROM ipq_stats_pass_data")->fetchCol();

  // Initialize option values for submitted parameters
  $groupname = !empty($_GET['targetsgroup']) ? $_GET['targetsgroup'] : '';
  $scoresonly = !empty($_GET['scoresonly']) ? 1 : 0;
  $avgsonly = !empty($_GET['avgsonly']) ? 1 : 0;
  $mettargets = !empty($_GET['mettargets']) ? 1 : 0;
  $notmettargets = !empty($_GET['notmettargets']) ? 1 : 0;
  $minmcqcorrect = !empty($_GET['minmcqcorrect']) ? $_GET['minmcqcorrect'] : '';
  $mintbscorrect = !empty($_GET['mintbscorrect']) ? $_GET['mintbscorrect'] : '';
  $tbs_perc = !empty($_GET['tbs_perc']) ? $_GET['tbs_perc'] : '';
  $scorefilter = !empty($_GET['scorefilter']) ? $_GET['scorefilter'] : '';
  $spprogress = !empty($_GET['spprogress']) ? $_GET['spprogress'] : '';

  // Build a little mini form into the render array for quick options selection
  $render = array();
  $render['form'] = array(
    '#prefix' => '<form method="GET">',
    'targetsgroup' => array(
      '#title' => 'Select a group',
      '#description' => 'Display a performance report for this group',
      '#type' => 'select',
      '#name' => 'targetsgroup',
      '#options' => array_combine(array_values($targetgroups), array_values($targetgroups)),
      '#required' => TRUE,
      '#value' => $groupname,
    ),
    'scoresonly' => array(
      '#type' => 'checkbox',
      '#title' => 'Exam scores only',
      '#description' => 'When checked, only students who have reported a CPA exam score will be displayed',
      '#name' => 'scoresonly',
      '#checked' => $scoresonly,
    ),
    'avgsonly' => array(
      '#type' => 'checkbox',
      '#title' => 'Show section averages only',
      '#description' => 'When checked, only the total averages for each section will be displayed',
      '#name' => 'avgsonly',
      '#checked' => $avgsonly,
    ),
    'mettargets' => array(
      '#type' => 'checkbox',
      '#title' => 'Evaluate users that met all targets only.',
      '#description' => 'When checked, only users who met all available targets will be evaluated.',
      '#name' => 'mettargets',
      '#checked' => $mettargets,
    ),
    'notmettargets' => array(
      '#type' => 'checkbox',
      '#title' => 'Evaluate users that did NOT meet all targets only.',
      '#description' => 'When checked, only users who did NOT meet all available targets will be evaluated.',
      '#name' => 'notmettargets',
      '#checked' => $notmettargets,
    ),
    'minmcqcorrect' => array(
      '#type' => 'textfield',
      '#title' => 'Filter MCQ correct',
      '#description' => 'Remove users matching the criteria are displayed. <em>&gt;50</em>,<em>&lt;=90</em>',
      '#name' => 'minmcqcorrect',
      '#value' => $minmcqcorrect,
    ),
    'mintbscorrect' => array(
      '#type' => 'textfield',
      '#title' => 'Filter TBS correct',
      '#description' => 'Remove users matching the criteria are displayed. <em>&gt;50</em>,<em>&lt;=90</em>',
      '#name' => 'mintbscorrect',
      '#value' => $mintbscorrect,
    ),
    'scorefilter' => array(
      '#type' => 'textfield',
      '#title' => 'Filter Exam Score',
      '#description' => 'Remove users matching the criteria are displayed. <em>&gt;50</em>,<em>&lt;=90</em>',
      '#name' => 'scorefilter',
      '#value' => $scorefilter,
    ),
    'spprogress' => array(
      '#type' => 'textfield',
      '#title' => 'Filter SP total progress',
      '#description' => 'Remove users matching the criteria are displayed. <em>&gt;50</em>,<em>&lt;=90</em>',
      '#name' => 'spprogress',
      '#value' => $spprogress,
    ),
    'tbs_perc' => array(
      '#type' => 'textfield',
      '#title' => 'Filter TBS correct percentage',
      '#description' => 'Remove users matching the criteria are displayed. <em>&gt;50</em>,<em>&lt;=90</em>',
      '#name' => 'tbs_perc',
      '#value' => $tbs_perc,
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => 'Show report',
    ),
    '#suffix' => '</form>',
  );

  $ct = new RCPARCourseTree();

  // Generate a report if we have a group requested
  if(!empty($groupname)) {
    // Get the targets group name that this stat group was compared to
    $comparegroup = db_query("SELECT targetsgroup FROM ipq_stats_pass_data WHERE groupname = :group LIMIT 1", array(':group' => $groupname))->fetchField();
    $render['header']['#markup'] = "<h2>Group name: $groupname</h2>Compared against targets for statistics group: $comparegroup";

    foreach (array('AUD', 'BEC', 'FAR', 'REG') as $section_name) {
      $num_passers = 0;
      $result = db_query("SELECT * FROM ipq_stats_pass_data WHERE groupname = :group AND section = :sec", array(':group' => $groupname, ':sec' => $section_name));

      // Get targets from the comparison group
      $section_id = $ct->getSectionIdFromName($section_name);
      $chapters = $ct->getChapterOptions($section_id);
      $num_chapters = sizeof($chapters);
      $targets = ipq_common_stats_get_chapters(array_keys($chapters), $comparegroup);

      // Generate an average of averages for score
      $chapter_score_avgs = array();
      foreach ($targets as $item) {
        $chapter_score_avgs[] = $item->trending_average;
      }
      $section_overall_avg = array_sum($chapter_score_avgs) / sizeof($chapter_score_avgs);


      // Store an array for aggregate/averages
      $averages = array(
        'uid' => 0,
        'section' => 0,
        'exam_score' => 0,
        'scores_met' => 0,
        'questions_met' => 0,
        'both_met' => 0,
        'score_average' => 0,
        'spscoreprogress' => 0,
        'questions_average' => 0,
        'sptotal' => 0,
        'questions_distinct_total' => 0,
        'mcq_total' => 0,
        'mcq_correct' => 0,
        'tbs_total' => 0,
        'tbs_correct' => 0,
        'tbs_percentage' => 0,
        'study_days_total' => 0,
      );
      $num_rows = 0;

      // Build a table
      $caption = '<h2>' . $section_name . '</h2> SmartPath targets: ' . $num_chapters;
      $header = array('User ID', 'Section', 'Exam score', 'Score targets met', 'Question targets met', 'Both met',
        'Score average', 'SP Score progress %', 'SP Questions progress %', 'SP TOTAL progress %', 'Questions total',
        'mcq_total', 'mcq_correct', 'tbs_total', 'tbs_correct', 'tbs_percentage', 'study_days_total'
      );
      $rows = array();

      foreach ($result as $row) {
        // If we're only showing students with scores, skip if the score is not greater than 0
        if($scoresonly) {
          if(!($row->exam_score > 0)) {
            continue;
          }
        }

        // The user's average score per chapter for this section
        $users_section_avg = ($row->score_average / $section_overall_avg) * 100;

        // SmartPath overall progress
        // 1. Get the user's score average for each chapter, average all scores together
        // 2. Get the smartpath target averages for each chapter and average them together
        // These two numbers expressed as an average is how much of the score target the user has met overall.
        // 3. Get the percentage of closeness to the target for questions attempted for each chapter
        // Average them together and you have the closeness to the question targets overall.
        // The section overal score and question averages, averaged together make the percentage of progress that the
        // user has made overall with smartpath. Note: score target and question targets are treated with equal weighting/value.
        $sptotal = ($users_section_avg + $row->questions_average) / 2;

        $tbs_percentage = round($row->tbs_correct / $row->tbs_total * 100, 1);

        /** FILTERS */
        try {
          // If we're only showing users that met all targets, skip users that did not
          if ($mettargets) {
            if($row->both_met < $num_chapters) {
              // Targets not met, don't show
              continue;
            }
          }
          // If we're only showing users that did NOT meet all targets, skip users that met them.
          if ($notmettargets) {
            if($row->both_met >= $num_chapters) {
              // Targets met, don't show
              continue;
            }
          }

          // Eval'd filters
          $filters = array(
            '$row->mcq_correct' => 'minmcqcorrect', // Filter by MCQ correct
            '$row->tbs_correct' => 'mintbscorrect', // Filter by TBS correct
            '$row->exam_score' => 'scorefilter',    // Filter by Exam score
            '$sptotal' => 'spprogress',             // Filter by overall smartpath progress
            '$tbs_percentage' => 'tbs_perc',             // Filter by overall smartpath progress
          );
          foreach ($filters as $rowvar => $filtervar) {
            if(!empty($$filtervar)) {
              $filter = $$filtervar;
              // @todo - Sanitize to prevent injection attacks, although we're only really worried about our own admins
              //$string = preg_replace("/[^ \w]+/", "", $string);

              // Equivalent example: if($row->mcq_correct > 200) { continue; }
              $met = eval("if($rowvar $filter) { return TRUE; }");
              if($met) {
                continue 2;
              }
            }
          }
        }
        catch (ParseError $e) {
          drupal_set_message('There is a syntax error in one of your filters - all filters will be ignored', 'error');
        }

        // Build table row
        $rows[] = array(
          $row->uid,
          $row->section,
          $row->exam_score,
          $row->scores_met,
          $row->questions_met,
          $row->both_met,
          $row->score_average,
          $users_section_avg,
          $row->questions_average,
          $sptotal,
          $row->questions_distinct_total,
          $row->mcq_total,
          $row->mcq_correct,
          $row->tbs_total,
          $row->tbs_correct,
          $tbs_percentage,
          $row->study_days_total,
        );

        if($row->exam_score >= 75) {
          $num_passers++;
        }

        // Add to averages and row count
        $num_rows++;
        $averages['exam_score'] += $row->exam_score;
        $averages['scores_met'] += $row->scores_met;
        $averages['questions_met'] += $row->questions_met;
        $averages['both_met'] += $row->both_met;
        $averages['score_average'] += $row->score_average;
        $averages['spscoreprogress'] += $users_section_avg;
        $averages['questions_average'] += $row->questions_average;
        $averages['sptotal'] += $sptotal;
        $averages['questions_distinct_total'] += $row->questions_distinct_total;
        $averages['mcq_total'] += $row->mcq_total;
        $averages['mcq_correct'] += $row->mcq_correct;
        $averages['tbs_total'] += $row->tbs_total;
        $averages['tbs_correct'] += $row->tbs_correct;
        $averages['tbs_percentage'] += $tbs_percentage;
        $averages['study_days_total'] += $row->study_days_total;
      } // End students in section iterator

      // Student Results table
      if (!$avgsonly) {
        $render['results'][] = array('#theme' => 'table', '#caption' => $caption, '#header' => $header, '#rows' => $rows);
      }

      // Averages table
      $num_users_in_section = sizeof($rows);
      $avg_header = array(
        'Pass Rate',
        'Avg CPA exam score',
        'Avg Score targets met',
        'Avg Question targets met',
        'Avg Both targets met',
        'Avg chapter score',
        'Avg SP Score total progress',
        'Avg SP questions total progress',
        'Avg SmartPath total progress',
        'Avg total questions',
        'mcq_total',
        'mcq_correct',
        'tbs_total',
        'tbs_correct',
        'tbs_percentage',
        'study_days_total',
      );

      // Calculate averages and put in the table row
      $avg_row = array(
        number_format($num_passers / $num_users_in_section * 100, 1) . '%',
        number_format($averages['exam_score'] / $num_users_in_section, 1),
        number_format($averages['scores_met'] / $num_users_in_section, 1),
        number_format($averages['questions_met'] / $num_users_in_section, 1),
        number_format($averages['both_met'] / $num_users_in_section, 1),
        number_format($averages['score_average'] / $num_users_in_section, 1) . '%',
        number_format($averages['spscoreprogress'] / $num_users_in_section, 1) . '%',
        number_format($averages['questions_average'] / $num_users_in_section, 1) . '%',
        number_format($averages['sptotal'] / $num_users_in_section, 1) . '%',
        number_format($averages['questions_distinct_total'] / $num_users_in_section, 1),
        number_format($averages['mcq_total'] / $num_users_in_section, 1),
        number_format($averages['mcq_correct'] / $num_users_in_section, 1),
        number_format($averages['tbs_total'] / $num_users_in_section, 1),
        number_format($averages['tbs_correct'] / $num_users_in_section, 1),
        number_format($averages['tbs_percentage'] / $num_users_in_section, 1),
        number_format($averages['study_days_total'] / $num_users_in_section, 1),
      );

      $render['results'][] = array(
        '#theme' => 'table',
        '#caption' => "$section_name averages. " . sizeof($rows) . ' users.',
        '#header' => $avg_header,
        '#rows' => array($avg_row),
      );

    } // End section iterator

  }

  return $render;
}


/**
 * Page callback for admin/settings/rcpar/ipq/config/smartpath/demoaccount
 * Settings page for SmartPath demo account
 */
function ipq_common_smartpath_demo_settings_form($form, $form_state) {
  $form['description'] = array(
    '#markup' => '<p>Demo accounts can be used to demonstration the look and feel of real SmartPath user.</p>' .
      '<p>Demo accounts will read from a source account - the source account being an actual user, however
      the demo will never write to or change the source account in any way.</p>',
  );

  // We'll make 5 pairs of demo/source accounts
  for ($i = 1; $i <= 5; $i++) {
    $form["fieldset_$i"] = array(
      '#type' => 'fieldset',
      '#title' => "Demo account #$i",
    );

    // Demo account field (target)
    $form["fieldset_$i"]["smartpath_demo_user_$i"] = array(
      '#type' => 'textfield',
      '#title' => "Demo account #$i",
      '#description' => 'The account that will be logged into to show for demonstration purposes.',
      '#size' => 30,
      '#maxlength' => 60,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => variable_get("smartpath_demo_user_$i"),
    );

    // Source account field (target)
    $form["fieldset_$i"]["smartpath_source_user_$i"] = array(
      '#type' => 'textfield',
      '#title' => "Source account #$i",
      '#description' => 'The account that SmartPath data will be read from',
      '#size' => 30,
      '#maxlength' => 60,
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => variable_get("smartpath_source_user_$i"),
    );
  }

  $form['#submit'][] = 'ipq_common_smartpath_demo_settings_form_submit';

  return system_settings_form($form);
}


/**
 * Submit handler for ipq_common_smartpath_demo_settings_form
 */
function ipq_common_smartpath_demo_settings_form_submit($form, &$form_state) {
  // The standard user autocomplete widget stores usernames instead of uids.
  // It's more convenient to store the user id for the demo users so we don't have to do a user load
  // for all end users when they are using SmartPath to check if they are a demo account.
  for ($i = 1; $i <= 5; $i++) {
    if($u = user_load_by_name($form_state['values']["smartpath_demo_user_$i"])) {
      variable_set("smartpath_demo_user_uid_$i", $u->uid);
    }
  }
}

/**
 * Page callback for admin/settings/rcpar/ipq/data/userhistory/%user
 * @param object $user
 * - Drupal user object to get history for
 * @param string $section
 * - Exam section to get data for, like 'AUD', 'BEC', etc
 */
function ipq_common_statistics_user_history($user, $section) {
  $uid = $user->uid;

  // Get the term id of the requested section
  $exam_version = exam_version_get_default_version();
  $ct = new RCPARCourseTree($exam_version);
  $section_id = $ct->getSectionIdFromName($section);

  // Get the timestamp of the earliest IPQ data this user ever produced
  $timestamps = array();
  $timestamps[] = db_query("SELECT MIN(created_on) FROM ipq_saved_session_data 
    WHERE uid = :uid AND section_id = :section_id AND status <> 2 LIMIT 1", array(
    ':uid' => $uid,
    ':section_id' => $section_id
  ))->fetchField();

  // Find every day that this user did some IPQ work
  $time_interval = 60 * 60 * 24; // 1 day
  do {
    // Get the next point in time that this user created data (after an interval of time from that last point measured)
    $t = db_query("SELECT created_on FROM ipq_saved_session_data 
      WHERE uid = :uid AND section_id = :section_id AND created_on > :last_time AND status <> 2  LIMIT 1", array(
      ':uid' => $uid,
      ':section_id' => $section_id,
      ':last_time' => end($timestamps) + $time_interval,
    ))->fetchField();

    // Add to our array if time is valid
    if($t) {
      $timestamps[] = $t;
    }
  }
  while($t);

  // Output a link to the SmartPath page where an admin can view the state of their progress at each point in time
  $output = '';
  foreach ($timestamps as $timestamp) {
    $output .= l(format_date($timestamp), 'ipq/smartpath', array('query' => array(
        'uid' => $uid, 'section' => $section, 'timestamp' => $timestamp
      ), 'attributes' => array('target' => '_blank'))) . '<br>';
  }
  return $output;
}
