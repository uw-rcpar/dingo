<?php

/**
 * Generated pass rate statistics based on SmartPath targets met and other metrics
 */
function ipq_common_smartpath_cache_batch_form($form, &$form_state) {
  $form['help'] = array(
    '#markup' => '<p>Executing this form will build a custom cache for all users in the system with active entitlements
    that stores their SmartPath scores in order to reduce the overhead caused by querying the IPQ session data tables
    for this information on the fly. Users will not be adversely affected by running this batch, it can be run safely
    at any time, however it may incur some performance overhead on the server.</p>',
  );

  // Skip users with cache already built
  $form['skip_cached_users'] = array(
    '#type' => 'checkbox',
    '#title' => 'Skip users already cached',
    '#description' => 'When checked, users that already have a cache record will be skipped. Useful for resuming a batch job.',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Build SmartPath batch for all users',
  );

  return $form;
}

function ipq_common_smartpath_cache_batch_form_submit($form, &$form_state) {
  ipq_common_smartpath_cache_batch($form_state['values']['skip_cached_users']);
}

/**
 * The batch callback.
 */
function ipq_common_smartpath_cache_batch($skip_cached = FALSE) {
  $batch = array(
    'operations' => array(),
    'finished' => 'ipq_common_smartpath_cache_batch_finished',
    'title' => t('SmartPath Cache Build'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'ipq_common') . '/includes/smartpath.batch.cache.inc'
  );


  // Get all active user entitlements
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'user_entitlement_product')
    ->propertyCondition('status', 1)
    ->addMetaData('account', user_load(1))
    ->fieldCondition('field_expiration_date', 'value', REQUEST_TIME, '>')
    ->fieldCondition('field_ipq_access', 'value', 1, '=');
  $result = $query->execute();

  // Make one batch operation per entitlement
  if (isset($result['node'])) {
    foreach ($result['node'] as $nid => $item) {
      if($skip_cached) {
        // Skip this user if they already have a cache record
        $uid = db_query("SELECT uid FROM node WHERE nid = :nid", array(':nid' => $nid))->fetchField();
        $c = new RCPARSmartPathCache($uid, FALSE);
        $exists = db_query("SELECT count(cid) FROM " . RCPARSmartPathCache::CACHE_TABLE . " WHERE cid = :cid",
          array(':cid' => $c->getCacheKey()))->fetchField();
        if($exists) {
          continue;
        }
      }

      $batch['operations'][] = array('ipq_common_smartpath_cache_batch_process', array($nid));
    }
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/rcpar/ipq/config/smartpath/buildcache'); // The path to redirect to when done.
  }
}

/**
 * Process a user's IPQ data for statistics
 * @param object $user
 * - A row object from the eck_exam_scores that represents a user with a passing score
 * @param string $section\
 * - 'AUD', 'BEC', 'FAR', or 'REG'
 * @param $context
 */
function ipq_common_smartpath_cache_batch_process($nid, &$context) {
  // Load the entitlement node
  $node = node_load($nid);
  $uid = $node->uid;

  // Determine what section the entitlement is for
  $section_id = $node->field_course_section[LANGUAGE_NONE][0]['tid'];
  $ct = new RCPARCourseTree();
  $chapters = $ct->getChapterOptions($section_id);

  // Build cache for each of these chapters
  $c = new RCPARSmartPathCache($uid);
  foreach ($chapters as $chapter_id => $chapter_name) {
    $c->setChapter($chapter_id, ipq_common_user_get_chapter_trending_avg_and_questions($uid, $chapter_id));
  }
  $c->saveCache();

  // Store some result data just to show messaging at the end
  $context['results']['uids'][$uid] = $uid;
  $context['results']['eids'][$node->nid] = $node->nid;
}


/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function ipq_common_smartpath_cache_batch_finished($success, $results, $operations){
  if ($success) {
    drupal_set_message("Success");
    drupal_set_message('Built cache for ' . sizeof($results['uids']) . ' users, who had a total of ' . sizeof($results['eids']) . ' entitlements');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}
