<?php

/**
 * Generated pass rate statistics based on SmartPath targets met and other metrics
 */
function ipq_common_statistics_pass_batch_form($form, &$form_state) {
  // Description
  $form['help'] = array(
    '#markup' => '<p>This batch job will generated performance data for users. The data will will show following stats:
     <ul>
     <li>Section (AUD, FAR, etc)</li>
     <li>Exam Score (user\'s most recent CPA exam attempt, if reported)</li>
     <li>Scores met - the number of chapters for which the user met the score target</li>
     <li>Questions met - the number of chapters for which the user met the questions target</li>
     <li>Both met - the number of chapters for which the user met both the score AND questions target</li>
     <li>Score average - score percentage on a per-chapter basis, then averaged together.</li>
     <li>Questions average - number of questions attempted on a per-chapter basis, then averaged together.</li>
     <li>Questions total - total number of distinct questions attempted for all chapters of this section summed together.</li>
     <li>Total number of MCQ questions attempted,</li>
     <li>Total number of MCQ questions answered correctly</li>
     <li>Total number of TBS questions attempted</li>
     <li>Total number of TBS questions answered correctly</li>
     <li>Total number of course videos viewed</li>
     <li>Total minutes of video time viewed</li>
     <li>Total number of days spent studying before exam (video watched or question answered)</li>
     </ul>
     </p>',
  );

  // Group name
  $form['groupname'] = array(
    '#type' => 'textfield',
    '#title' => 'Group name',
    '#description' => 'A name to identify this data set. Current groups: <br>' .
      implode('<br>', db_query("SELECT DISTINCT groupname FROM ipq_stats_pass_data")->fetchCol()),
    '#required' => TRUE,
  );

  // Filter by exam takers
  $form['tookexam'] = array(
    '#type' => 'checkbox',
    '#title' => 'Reported exam scores',
    '#description' => 'When checked, only users who reported their AICPA exam score will be evaluated',
  );

  // Exam taken date
  $form['examdate'] = array(
    '#type' => 'textfield',
    '#title' => 'Exam date',
    '#description' => 'Only users with an exam taken <strong>after</strong> this date will be evaluated. Any human readable format is ok (strtotime)',
  );

  // Minimum questions
  $form['question_threshold'] = array(
    '#type' => 'textfield',
    '#title' => 'Minimum question threshold',
    '#description' => 'Users who have not answered at least this number of IPQ questions in this section will be ignored.
    <br>This filter is intended to help filter out irrelevant data from users who did not use IPQ, only messed around with it,
    or didn\'t use their account at all, etc.',
    '#default_value' => 200,
  );

  // Filter by partner id
  $form['partner'] = array(
    '#type' => 'textfield',
    '#title' => 'Filter by partner ID',
    '#description' => 'Only users that have or have had entitlements with this partner ID will be evaluated. 
     <br>NOTE: currently this option only works when "Reported exam scores" is <em>unchecked</em>',
  );

  // Target groups
  $targetgroups  = db_query("SELECT DISTINCT groupname FROM ipq_stats_chapter_avgs")->fetchCol();
  $form['targetsgroup'] = array(
    '#title' => 'Targets group',
    '#description' => 'This group will be used to compare data against when assessing if exam takers have met their targets',
    '#type' => 'select',
    '#options' => array_combine(array_values($targetgroups), array_values($targetgroups)),
    '#required' => TRUE,
    '#value' => IPQ_SMARTPATH_COMPARE_GROUP,
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Build IPQ Pass Statistics'
  );

  return $form;
}

function ipq_common_statistics_pass_batch_form_submit($form, &$form_state) {
  ipq_common_statistics_pass_batch($form_state['values']);
}

/**
 * The batch callback.
 * @param array $options
 * - This is $form_state['values'] passed directly from the submit handler
 */
function ipq_common_statistics_pass_batch($options) {
  $groupname = $options['groupname'];
  $targetgroup = $options['targetsgroup'];
  $tookexam = $options['tookexam'];
  $examdate = strtotime($options['examdate']);
  $partner = $options['partner'];
  $question_threshold = $options['question_threshold'];

  $batch = array(
    'operations' => array(),
    'finished' => 'ipq_common_statistics_pass_batch_finished',
    'title' => t('IPQ Statistics Pass Stats'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'ipq_common') . '/includes/smartpath.batch.passrates.inc',
  );


  // Analyze only users that took the exam
  if ($tookexam) {
    $query = db_select('eck_exam_scores', 'scores')
      ->fields('scores');

    // Optionally filter by exam taken date
    if ($examdate) {
      $query->condition('exam_date', $examdate, '>');
    }

    // Get all users with an exam score reported
    foreach ($query->execute() as $user) {
      $batch['operations'][] = array('ipq_common_statistics_pass_batch_process', array($user, $targetgroup, $groupname, $question_threshold));
    }
  }
  // Analyze users that did or did not take the exam
  else {
    // Query for all entitlements in the system for each section
    foreach (array('AUD', 'BEC', 'REG', 'FAR') as $section_name) {
      // Query for all entitlements in this section
      $query = db_select('node', 'n');
      $query->addJoin('INNER', 'field_data_field_product_sku', 'f', 'f.entity_id = n.nid');
      $query->condition('n.type', 'user_entitlement_product', '=');
      $query->condition('f.field_product_sku_value', $section_name, '=');
      $query->addField('n', 'uid');

      // Optionally filter by partner
      if($partner) {
        $query->addJoin('INNER', 'field_data_field_partner_id', 'fp', 'fp.entity_id = n.nid');
        $query->condition('fp.field_partner_id_value', $partner, '=');
      }

      // Left join on exam score so that if have it, we can optionally use it.
      $query->leftJoin('eck_exam_scores', 'e', 'e.uid = n.uid');
      $query->addField('e', 'score');

      // Add a batch operation for each user
      $result = $query->execute();
      foreach ($result as $row) {
        // Processing function expects $user to be in this format. This is a legacy thing before we allowed
        // users without exam scores to be processed.
        $user = new stdClass();
        $user->uid = $row->uid;
        $user->section = $section_name;
        $user->score = $row->score;

        // Data queries will filter for data that has occured before this date, so we set it far into the future to make
        // it irrelevant
        $user->exam_date = 32503680000; // Jan 1, 3000

        $batch['operations'][] = array('ipq_common_statistics_pass_batch_process', array($user, $targetgroup, $groupname, $question_threshold));
      }
    }
  }


  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/rcpar/ipq/config/smartpath/passdata'); // The path to redirect to when done.
  }
}

/**
 * Process a user's IPQ data for statistics
 * @param object $user
 * - A row object from the eck_exam_scores that represents a user with a passing score
 * @param string $section\
 * - 'AUD', 'BEC', 'FAR', or 'REG'
 * @param $context
 */
function ipq_common_statistics_pass_batch_process($user, $targetgroup, $groupname, $question_threshold, &$context) {
  $ct = new RCPARCourseTree();
  $section_nid = $ct->getSectionIdFromName($user->section);

  // Determine if the user has enough IPQ data in this section to meet our criteria
  // If not, we will skip them and not use their data. We'll ignore skipped questions
  // NOTE: the batch process runs out of memory or times out if we check this for every user. We can do it during processing
  if($question_threshold > 0) {
    $replacements = array(
      ':uid' => $user->uid,
      ':sectionid' => $section_nid,
    );

    // If we're filtering by exam date, only show data leading up to, but not after, the exam
    if(!empty($user->exam_date)) {
      $examCondition = 'AND created_on < :examdate';
      $replacements[':examdate'] = $user->exam_date;
    }

    $meets_threshold = db_query("SELECT count(*)
    FROM ipq_saved_session_data 
    WHERE uid = :uid AND section_id = :sectionid $examCondition AND status <> 2
    LIMIT $question_threshold", $replacements)->fetchField()
      >= $question_threshold;
    if (!$meets_threshold) {
      return;
    }
  }


  // Get this user's SmartPath data
  $smartpath_data = ipq_common_user_get_smartpath_data($ct, $section_nid, $user->uid, $targetgroup, $user->exam_date, FALSE, FALSE);

  // Calculate the number of days between the first question answered and the exam taken
  // todo - improve by finding a better start date - maybe connected to order or entitlement
  $firstQuestion  = db_query("SELECT MIN(created_on) FROM ipq_saved_session_data WHERE uid =:u AND section_id = :s",
    array(':u' => $user->uid, ':s' => $section_nid))->fetchField();
  $studyTime = $user->exam_date - $firstQuestion;
  $studyDays = round($studyTime / 60 / 60 / 24);

  $user_record = array(
    'uid' => $user->uid,
    'section' => $user->section,
    'exam_score' => $user->score,
    'scores_met' => 0,
    'questions_met' => 0,
    'both_met' => 0,
    'score_average' => 0,
    'questions_average' => 0,
    'questions_distinct_total' => 0,
    'groupname' => $groupname,
    'targetsgroup' => $targetgroup,
    'updated_on' => REQUEST_TIME,
    // Todo - improve question count and video queries by filtering results by date
    'mcq_total' => db_query("SELECT count(id) FROM ipq_saved_session_data WHERE ipq_question_type_id = :t AND uid = :u AND status <> :s AND section_id = :sec",
      array(':t' => 1, ':u' => $user->uid, ':s' => IPQ_STATUS_SKIPPED, ':sec' => $section_nid))->fetchField(),

    'mcq_correct' => db_query("SELECT count(id) FROM ipq_saved_session_data WHERE ipq_question_type_id = :t AND uid = :u AND status = :s AND section_id = :sec",
      array(':t' => 1, ':u' => $user->uid, ':s' => IPQ_STATUS_CORRECT, ':sec' => $section_nid))->fetchField(),

    'tbs_total' => db_query("SELECT count(id) FROM ipq_saved_session_data WHERE ipq_question_type_id <> :t AND uid = :u AND status <> :s AND section_id = :sec",
      array(':t' => 1, ':u' => $user->uid, ':s' => IPQ_STATUS_SKIPPED, ':sec' => $section_nid))->fetchField(),

    'tbs_correct' => db_query("SELECT count(id) FROM ipq_saved_session_data WHERE ipq_question_type_id <> :t AND uid = :u AND status = :s AND section_id = :sec",
      array(':t' => 1, ':u' => $user->uid, ':s' => IPQ_STATUS_CORRECT, ':sec' => $section_nid))->fetchField(),
    'video_total' => db_query("SELECT count(id) FROM eck_video_history WHERE uid = :u AND section = :sec",
      array(':u' => $user->uid, ':sec' => $user->section))->fetchField(),
    'video_minutes' => 0, // todo
    'study_days_total' => $studyDays,
  );

  $context['results'][$user->uid] = array(); // todo - We don't do anything with results, we could count them for the final status page or something

  foreach ($smartpath_data as $chapter_nid => $datum) {
    // Scores met
    if($datum['trending_met']) {
      $user_record['scores_met']++;
    }

    // Questions met
    if($datum['questions_met']) {
      $user_record['questions_met']++;
    }

    // Both met
    if($datum['trending_met'] && $datum['questions_met']) {
      $user_record['both_met']++;
    }

    // Score average (will be divided out after the loop)
    // @todo - it would be more consistent to express score average as a percentage of the target score
    $user_record['score_average'] += $datum['trending_average'];

    // Questions percentage expressed as a percentage of the target number
    // If the user did more than the target, this could be greater than 100%, we'll just reduce it to 100%
    $question_percentage = ($datum['distinct_questions'] / $datum['stats']['distinct_questions']) * 100;
    if($question_percentage > 100) {
      $question_percentage = 100;
    }
    // Questions average (will be divided out after the loop)
    $user_record['questions_average'] += $question_percentage;

    // Total distinct questions
    $user_record['questions_distinct_total'] += $datum['distinct_questions'];
  }

  $num_chapters = sizeof($smartpath_data);

  $user_record['questions_average'] = $user_record['questions_average'] / $num_chapters;
  $user_record['score_average'] = $user_record['score_average'] / $num_chapters;

  // Make a db row - clear out the old one first, if it exists
  db_query("DELETE FROM ipq_stats_pass_data WHERE uid = :uid AND section = :section AND groupname = :groupname",
    array(':uid' => $user->uid, ':section' => $user->section, ':groupname' => $groupname));
  drupal_write_record('ipq_stats_pass_data', $user_record);
}


/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function ipq_common_statistics_pass_batch_finished($success, $results, $operations){
  if ($success) {
    drupal_set_message("Success");
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}
