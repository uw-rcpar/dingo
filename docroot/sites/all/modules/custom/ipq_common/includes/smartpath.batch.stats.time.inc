<?php

/**
 * A form for initiating a batch job to generate large-scale user data for statistics
 */
function ipq_common_statistics_batch_time_form($form, &$form_state) {
  // Get stat groups as table rows
  $rows = array();
  foreach (db_query("SELECT id, name, description FROM stat_groups")->fetchAllAssoc('id') as $item) {
    $rows[] = array($item->id, $item->name, $item->description);
  }

  $form['groupinfo'] = array(
    '#type' => 'fieldset',
    '#title' => 'Existing stat groups',
    '#description' => 'These are the group names that currently exist in the database.',
    'contents' => array(
      '#markup' => theme('table', ['rows' => $rows]),
    ),
  );

  // Group name
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Group name',
    '#default_value' => IPQ_SMARTPATH_COMPARE_GROUP,
    '#description' => 'A name to identify this new data set.' .
      '<br>The current group being used for SmartPath targets is: <strong>' . IPQ_SMARTPATH_COMPARE_GROUP . '</strong>',
    '#required' => TRUE,
  );

  // Group name
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => 'Description',
    '#description' => 'A description for this group, including how to query it.',
  );

  // Clear data
  $form['clear'] = array(
    '#type' => 'checkbox',
    '#title' => 'Clear existing data',
    '#description' => 'Checking this box will wipe out existing data in this group before beginning.',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Build stats',
  );

  // Aggregates
  $form['aggregates'] = array(
    '#type' => 'submit',
    '#value' => 'Build aggregates',
  );

  return $form;
}

/**
 * Form submit handler for ipq_common_statistics_batch_time_form_
 * Executes a job to either build statistics, or form aggregates from existing stat values
 */
function ipq_common_statistics_batch_time_form_submit($form, &$form_state) {
  // Create the intitial record for the stat group
  $id = db_query("SELECT id FROM stat_groups WHERE name = :n", [':n' => $form_state['values']['name']])->fetchField();
  if(!$id) {
    $record = array(
      'name' => $form_state['values']['name'],
      'description' => $form_state['values']['description'],
    );
    drupal_write_record('stat_groups', $record);
    $id = $record['id'];
  }

  // Clear existing values
  if($form_state['values']['clear']) {
    // Delete segment values
    db_query("
      DELETE v.* FROM stat_group_segment_values v 
      INNER JOIN stat_group_segments g ON g.id = v.segment_id
      WHERE g.group_id = :gid", [':gid' => $id]
    );

    // Delete segments
    db_query("DELETE FROM stat_group_segments WHERE group_id = :gid", [':gid' => $id]);
  }


  // Either 'Build aggregates' or 'Build stats'
  switch($form_state['values']['op']) {
    case 'Build aggregates':
      ipq_common_statistics_build_time_group_aggregates($id);
      break;

    case 'Build stats':
    default:
    ipq_common_statistics_batch_time($id);
      break;
  }
}


class RCPARTimeStatsAggregates {

  public $group_id;
  protected $courseData;
  protected $targets;
  protected $sectionStudyWeeks;

  public function __construct($group_id) {
    $this->group_id = $group_id;

    // Get course structure data
    $context = array();
    ipq_common_statistics_batch_setup($context);
    $this->courseData = $context['results']['course_data'];

    // Get SmartPath targets
    $this->targets = db_query("
    SELECT chapter_id, trending_average, distinct_questions FROM ipq_stats_chapter_avgs 
    WHERE groupname = :groupname",
      array(':groupname' => IPQ_SMARTPATH_COMPARE_GROUP)
    )->fetchAllAssoc('chapter_id');
  }

  public function main() {
    $group_id = $this->group_id;
    $targets = $this->targets;

    // Clear out existing aggregates before we begin
    db_query("DELETE FROM stat_group_aggregates WHERE group_id = :gid", [':gid' => $group_id]);

    // Our main data pieces
    $ppm_averages = array();
    $chapter_study_weeks = array();
    $actual_quiz_time = array();
    $actual_vid_time = array();


    // Limit analysis to users who met smartpath targets
    $uids = db_query("SELECT DISTINCT uid FROM ipq_stats_pass_data WHERE score_average >= 100 AND questions_average >= 100")->fetchCol();

    // Load data for each user in the stat group
    $result = db_query("SELECT id, uid FROM stat_group_segments WHERE group_id = :gid", [':gid' => $group_id])->fetchAll();

    // Figure out the study weeks per section for all users
    foreach ($result as $record) {
      // Query for all of this user's data and store in a flat array and clean out empty weeks
      $users_data = $this->getFlattenedData($record->id);
      $users_data = $this->removeEmptyWeeks($users_data);

      // Tally up the number of study weeks for each section
      $this->calculateUsersStudyWeeksPerSection($users_data, $record->uid);

      $all_users[$record->id] = $users_data;
    }

    $min_weeks = 20;
    $max_weeks = 24;
    drupal_set_message("AUD, $min_weeks - $max_weeks weeks");

    foreach ($result as $record) {
      $uid = $record->uid;

      // Skip if not within our study range
      /*$aud_study_weeks = $this->getUsersStudyWeeks('AUD', $uid);
      if (!($aud_study_weeks > $min_weeks && $aud_study_weeks <= $max_weeks)) {
        unset($this->sectionStudyWeeks['AUD'][$uid]);
        continue;
      }*/

      // Skip if this is not a smartpath accomplisher. @todo - this should be checked per section
      if (!empty($uids) && !in_array($uid, $uids)) {
        continue;
      };

      $users_data = $all_users[$record->id];

      if ($uid == 178656) {
        print '';
      }

      // Calculate SP net change and SP points per minute for each chapter of each week
      // Clean up empty weeks of data
      $users_ppm_averages = array();
      $last_weeks = array();
      $last_scores = array();
      foreach ($users_data as $week => $users_week_data) {

        foreach ($users_week_data as $chapter_id => $week_chapter_datum) {
          // Log a week studied for this chapter
          $chapter_study_weeks[$chapter_id][$uid]++;

          // Log actual quiz and vid
          $actual_quiz_time[$chapter_id][$uid] += $week_chapter_datum['quiz_minutes'];
          $actual_vid_time[$chapter_id][$uid] += $week_chapter_datum['video_minutes'];

          // We can't analyze SmartPath data for this chapter unless we have SmartPath targets for it
          if (empty($targets[$chapter_id])) {
            continue;
          }

          $last_scores[$chapter_id] = $week_chapter_datum;

          // Initalize last week's data with 0 if there's nothing there yet.
          $last_weeks[$chapter_id] = empty($last_weeks[$chapter_id]) ? 0 : $last_weeks[$chapter_id];

          // Get SP progress as a single figure
          $sp = $this->calculateSpSingleVal($week_chapter_datum['score'], $week_chapter_datum['questions'], $chapter_id);


          // @todo Calculate the rate that the user is answering NEW questions correctly
          // e.g., for every hour of study, how many correct answers are they getting for questions that they haven't
          // answered correctly before

          // Calculate the net change from the last week that we had data
          $net_change = $sp - $last_weeks[$chapter_id];

          // SP points per minute
          $sp_pts_per_minute = $net_change / ($week_chapter_datum['quiz_minutes'] + $week_chapter_datum['video_minutes']);
          $users_ppm_averages[$chapter_id][] = $sp_pts_per_minute;

          // Replace the 'last week' value with this one for when we compare the next week
          $last_weeks[$chapter_id] = $sp;

          /** DEBUG */
          /*if ($uid == 178656) {
            drupal_set_message("Week: $week | Chapter: $chapter_id<br>
          sp = $sp <br>
          net_change = $net_change <br>
          quiz_minutes = {$week_chapter_datum['quiz_minutes']} <br>
          video_minutes = {$week_chapter_datum['video_minutes']} <br>
          <br>");
          }*/
        }


      } // [End user's weeks data iteration]

      // Now we can get total averages for this users SP per min for each chapter and add it to our global storage
      foreach ($users_ppm_averages as $chapter_id => $values) {
        // Add up the total for all SP ppm values that were logged for each week for this chapter and divide out to get an average.
        $ppm_averages[$chapter_id][$uid] = array_sum($users_ppm_averages[$chapter_id]) / count($users_ppm_averages[$chapter_id]);
      }

      // SP Alternate method
      foreach ($last_scores as $chapter_id => $scores) {
        $total_time = $actual_quiz_time[$chapter_id][$uid] + $actual_vid_time[$chapter_id][$uid];
        $sp = $this->calculateSpSingleVal($scores['score'], $scores['questions'], $chapter_id);
        $ppm_averages[$chapter_id][$uid] = $sp / $total_time;
      }

    } // [END all users iteration]


    /******************************
     **** Tally up the numbers ****
     ******************************/


    // Average study weeks per section overall
    $section_study_weeks = array('AUD' => 0, 'BEC' => 0, 'FAR' => 0, 'REG' => 0);
    foreach ($this->sectionStudyWeeks as $section_name => $section_data) {
      $week_count = 0;
      foreach ($section_data as $uid => $user_count) {
        $week_count += $user_count;
      }

      // Now get the average for this section
      $section_study_weeks[$section_name] = $week_count / sizeof($section_data);
    }

    // Now we can create overall averages for points per minute.
    // @todo divide users into different groups before calculating SP ppm and mtt
    $chapter_ppm_averages = array();
    $chapter_minutes_to_target = array();
    foreach ($ppm_averages as $chapter_id => $values) {
      $chapter_ppm_average = array_sum($ppm_averages[$chapter_id]) / count($ppm_averages[$chapter_id]);
      $chapter_ppm_averages[$chapter_id] = $chapter_ppm_average;

      // Now that we have the overall average, we can calculate how many study minutes are needed to reach the
      // SP target, on average.
      $minutes_to_target = 100 / $chapter_ppm_average;
      $chapter_minutes_to_target[$chapter_id] = $minutes_to_target;

      // @todo - write these values to the DB
    }

    // Calculate study weeks per chapter
    foreach ($chapter_study_weeks as $chapter_id => $values) {
      $chapter_weeks_average = array_sum($values) / count($values);
      $chapter_weeks_averages[$chapter_id] = $chapter_weeks_average;
    }

    // Calculate overall avg quiz time for each chapter
    $actual_quiz_time_averages = array();
    foreach ($actual_quiz_time as $chapter_id => $values) {
      $chapter_avg = array_sum($values) / count($values);
      $actual_quiz_time_averages[$chapter_id] = $chapter_avg;
    }

    // Calculate overall avg quiz time for each chapter
    $actual_vid_time_averages = array();
    foreach ($actual_vid_time as $chapter_id => $values) {
      $chapter_avg = array_sum($values) / count($values);
      $actual_vid_time_averages[$chapter_id] = $chapter_avg;
    }

    // Pretty print
    $output = ipq_common_time_statistics_aggregates_page(array(
      'chapter_ppm_averages' => $chapter_ppm_averages,
      'chapter_minutes_to_target' => $chapter_minutes_to_target,
      'chapter_weeks_averages' => $chapter_weeks_averages,
      'targets' => $targets,
      'actual_vid_time_averages' => $actual_vid_time_averages,
      'actual_quiz_time_averages' => $actual_quiz_time_averages,
      'section_study_weeks' => $section_study_weeks,
    ));
    drupal_set_message(render($output));
  }

  /**
   * Get's the section name (AUD, BEC, etc) for the given chapter ID
   * @param int $chapter_id
   * @return string
   */
  public function getSectionNameFromChapter($chapter_id) {
    foreach ($this->courseData as $section_name => $chapters) {
      if(isset($chapters[$chapter_id])) {
        return $section_name;
      }
    }
    return '';
  }

  protected function getUsersStudyWeeks($section_name, $uid) {
    $study_weeks = $this->sectionStudyWeeks[$section_name][$uid];
    return empty($study_weeks) ? 0 : $study_weeks;
  }

  protected function getFlattenedData($segment_id) {
    $users_data = array();
    foreach (db_query("SELECT * FROM stat_group_segment_values WHERE segment_id = :id", [':id' => $segment_id]) as $item) {
      list(, $week, $type) = explode('|', $item->type);

      switch ($type) {
        case 'questions': $users_data[$week][$item->scope_identifier]['questions'] = $item->value; break;
        case 'score': $users_data[$week][$item->scope_identifier]['score'] = $item->value; break;
        case 'video_minutes': $users_data[$week][$item->scope_identifier]['video_minutes'] = $item->value; break;
        case 'quiz_minutes': $users_data[$week][$item->scope_identifier]['quiz_minutes'] = $item->value;break;
      }
    }

    return $users_data;
  }

  protected function removeEmptyWeeks($users_data) {
    // Clean up empty weeks of data
    foreach ($users_data as $week => $users_week_data) {
      foreach ($users_week_data as $chapter_id => $week_chapter_datum) {
        // Check if the user spent no study time on this chapter this week.
        if($week_chapter_datum['video_minutes'] == 0 && $week_chapter_datum['quiz_minutes'] == 0) {
          unset($users_data[$week][$chapter_id]);
        }
      }

      // If we ended up removing all chapters  because there was no study at all this week, we'll remove the whole week
      if(empty($users_data[$week])) {
        unset($users_data[$week]);
      }
    }

    // Make sure we stay sorted by week
    ksort($users_data);

    return $users_data;
  }

  /**
   * Calculate the SmartPath score as represented by a single value (score & questions are worth 50% of the pie, but
   * no "points" are awarded until at least a 50% is reached).
   *
   * @param int $score
   * - Trending average score for this chapter
   * @param int $questions
   * - Distinct questions answered for this chapter
   * @param int $chapter_id
   * @return float|int
   */
  protected function calculateSpSingleVal($score, $questions, $chapter_id) {
    $sp = (
      (
        ($score / $this->targets[$chapter_id]->trending_average * 100) +
        ($questions / $this->targets[$chapter_id]->distinct_questions * 100)
      ) / 2);

    $sp -= 50;
    if ($sp < 0) {
      $sp = 0;
    }
    $sp = $sp / 50;
    $sp *= 100; // Represent percentage as a full number (80% rather than .8)

    return $sp;
  }

  protected function calculateUsersStudyWeeksPerSection($users_data, $uid) {
    $users_section_study_weeks = array();

    foreach ($users_data as $week => $users_week_data) {
      foreach ($users_week_data as $chapter_id => $week_chapter_datum) {
        // Log a week studied for this section. We won't count more than one week if there are multiple chapters
        // but we will count multiple sections with the same week. This is an unavoidable shortcoming
        $users_section_study_weeks[$this->getSectionNameFromChapter($chapter_id)][$week] = 1;
      }
    }

    // Add study weeks per section to the global totals
    foreach ($users_section_study_weeks as $section_name => $weeks) {
      $this->sectionStudyWeeks[$section_name][$uid] = count($weeks);
    }
  }

}

/**
 * Build aggregates from group values
 */
function ipq_common_statistics_build_time_group_aggregates($group_id) {
  $x = new RCPARTimeStatsAggregates($group_id);
  $x->main();

  drupal_set_message('Done building aggregates');
}

/**
 * Initializes the batch job for statistics gathering
 */
function ipq_common_statistics_batch_time($group_id) {
  $batch = array(
    'operations' => array(),
    'finished' => 'ipq_common_statistics_batch_new_finished',
    'title' => t('IPQ Statistics Stats'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'ipq_common') . '/includes/smartpath.batch.stats.time.inc'
  );

  // First batch operation: get course data
  $batch['operations'][] = array('ipq_common_statistics_batch_setup', array());

  // <DEV> Process a single user for troubleshooting
  /*$batch['operations'][] = array('ipq_common_statistics_batch_setup', array());
  // Find all the users that passed this section
  foreach (db_query("SELECT * FROM eck_exam_scores 
    WHERE score > 74 AND section = :section AND uid = :uid", array(':section' => 'AUD', ':uid' => 28982)) as $user_passing) {
    $batch['operations'][] = array('ipq_common_statistics_batch_time_process', array($user_passing, 'AUD', 1452, $group_id));
  }
  batch_set($batch);
  batch_process('admin/settings/rcpar/ipq/config/smartpath/targetdatanew'); // The path to redirect to when done.
  return;*/
  // </DEV>

  // Resume at the last record processed in the case of errors
  $last_exam_id_processed = db_query("SELECT MAX(exam_id) FROM stat_group_segments WHERE group_id = :gid",
    [':gid' => $group_id])->fetchField();
  $last_exam_id_processed = is_null($last_exam_id_processed) ? 0 : $last_exam_id_processed;

  // Limit analysis to users who met smartpath targets
  $allowed_uids = db_query("SELECT DISTINCT uid FROM ipq_stats_pass_data WHERE score_average >= 100 AND questions_average >= 100")->fetchCol();

  $ct = new RCPARCourseTree();
  foreach ($ct->getAllSectionOpts() as $section_nid => $section) {
    $section_nid = $ct->getSectionIdFromName($section);

    // Find all the users that passed this section
    foreach (db_query("SELECT * FROM eck_exam_scores WHERE score > 74 AND section = :section AND id > :id",
      array(':section' => $section, ':id' => $last_exam_id_processed)) as $user_exam_score) {

      // Skip this user if they're not in the allowed IDs
      if (!in_array($user_exam_score->uid, $allowed_uids)) {
       // continue;
      }

      // Check if we already have this segment from a previous run:
      if(db_query("SELECT id FROM stat_group_segments WHERE uid = :uid AND exam_id = :eid AND group_id = :gid",
        [':uid' => $user_exam_score->uid, ':eid' => $user_exam_score->id, ':gid' => $group_id])->fetchField()) {
        drupal_set_message("Skipping existing segment: uid - {$user_exam_score->uid} | exam attempt id - {$user_exam_score->id}");
        continue;
      }

      $batch['operations'][] = array('ipq_common_statistics_batch_time_process', array($user_exam_score, $section, $section_nid, $group_id));
    }
  }

  batch_set($batch);
  batch_process('admin/settings/rcpar/ipq/config/smartpath/targetdata'); // The path to redirect to when done.
}

/**
 * Process a user's IPQ data for statistics
 * @param object $user
 * - A row object from the eck_exam_scores that represents a user with a passing score
 * @param string $section\
 * - 'AUD', 'BEC', 'FAR', or 'REG'
 * @param $context
 */
function ipq_common_statistics_batch_time_process($user, $section, $section_nid, $group_id, &$context) {
  // Check if the user has enough IPQ data for this section to meet our threshold. If not, they're not worth bothering with.
  module_load_include('inc', 'ipq_common', 'includes/smartpath.batch.stats.new');
  if(!ipq_common_stats_check_sp_threshold($user->uid, $section_nid, $user->exam_date)) {
    return;
  }

  // Get our stored course data
  $course_data = $context['results']['course_data'];

  // Store the group id that this data will be tagged with. @todo - do this once at first operation
  $context['results']['group_id'] = $group_id;

  // Increment the user count
  $context['results']['usercount']++;


  // Compute values, per user, per chapter, per week
  // Week | Chapter Id | Quiz Time | Video Time  | Questions | Score
  // 1    | 4334       | 5.5 hrs   | 6 hrs       | 100       | 88%
  // 1    | 4335       | 1.2 hrs   | 3 hrs       | 5         | 76%
  // 1    | 4336       | 0 hrs     | 0 hrs       | 0         | 0%
  // 1    | 4337       | 0 hrs     | 0 hrs       | 0         | 0%
  // Finally we can determine how many hours input (quiz/video) = how much SmartPath output (questions/score)
  // I expect the input/output ratios to be different based on the ambition/ability of the student; e.g. those
  // who passed quickly vs those who took a long time to pass.
  // We can further divide into buckets: 30 days, 60 days, 90 days, 180+ days.
  // Better yet would be to divide into weeks, and we can discard a week if there is no study effort  there at all,
  // which will help us control for segments of time between a free trial and a real enrollment, or a longer break
  // period.

  // For 63 days, we'll measure 9 weeks

  // Step 1, find the date of the earliest quiz and earliest video ever. This gives us a starting point for measuring data.
  $start_date_ipq = db_query("SELECT created_on FROM ipq_saved_sessions WHERE uid = :u AND section_id = :s ORDER BY created_on ASC LIMIT 1",
    [':u' => $user->uid, ':s' => $section_nid])->fetchField();
  $start_date_video = db_query("SELECT created FROM eck_video_history WHERE uid = :u AND section = :s ORDER BY created ASC LIMIT 1",
    [':u' => $user->uid, ':s' => $section])->fetchField();
  $start_date_video = is_numeric($start_date_video) ? $start_date_video : $start_date_ipq;
  $start_date = min($start_date_ipq, $start_date_video);

  // Step 2, measure SmartPath progress over 7 day increments beginning from the start date until the exam date
  // Iterate through each chapter and measure progress.
  $check_date = $start_date;
  $exam_date = $user->exam_date;
  $sevendays = 60*60*24*7;

  // Store the group segment. This is the umbrella for all the data associated with this user
  $segment_record = array(
    'uid' => $user->uid,
    'date_end' => $user->exam_date,
    'group_id' => $group_id,
    'exam_id' => $user->id,
    'updated_on' => REQUEST_TIME,
  );
  drupal_write_record('stat_group_segments', $segment_record);



  $week = 0;
  while ($check_date < $exam_date) {
    $week++;

    // Iterate through each chapter for this time period
    foreach ($course_data[$section] as $chapter_nid => $topics) {
      /******************
       * Calculate data *
       ******************/

      // SCORE and QUESTIONS: Get user's SmartPath scores and distinct questions for this chapter.
      // @todo - this needs to respect the time period that we're looking in
      $data = ipq_common_user_get_chapter_trending_avg_and_questions($user->uid, $chapter_nid, $check_date + $sevendays);
      $score = $data['trending_average'];
      $questions = $data['questions'];

      // QUIZ TIME: Get time spent in quizzes for this chapter
      $result = db_query("SELECT time FROM ipq_saved_session_data
      WHERE uid = :u AND chapter_id = :cid AND created_on BETWEEN :start AND :end",
        [':u' => $user->uid, ':cid' => $chapter_nid, ':start' => $check_date, ':end' => $check_date + $sevendays]);
      $quiz_time = 0;
      foreach ($result as $item) {
        // Convert seconds into minutes
        $quiz_time += $item->time / 60;
      }

      // VIDEO TIME: Get video minutes for this chapter
      $result = db_query("SELECT created, last_position FROM eck_video_history
      WHERE uid = :u AND chapter_reference = :cid AND created BETWEEN :start AND :end",
        [':u' => $user->uid, ':cid' => $chapter_nid, ':start' => $check_date, ':end' => $check_date + $sevendays]);
      $video_time = 0;
      foreach ($result as $item) {
        // Last position is a quantity of milliseconds. We'll divide it out into minutes.
        $video_time += ($item->last_position / 1000 / 60);
      }


      /**************
       * Store data *
       **************/

      // Common properties
      $common = array(
        'segment_id' => $segment_record['id'],
        'scope' => "chapter",
        'scope_identifier' => $chapter_nid,
      );

      // Score
      $segment_value = $common + array(
        'type' => "week|$week|score",
        'value' => $score,
      );
      drupal_write_record('stat_group_segment_values', $segment_value);

      // Questions
      $segment_value = $common + array(
        'type' => "week|$week|questions",
        'value' => $questions,
      );
      drupal_write_record('stat_group_segment_values', $segment_value);

      // Quiz minutes
      $segment_value = $common + array(
        'type' => "week|$week|quiz_minutes",
        'value' => $quiz_time,
      );
      drupal_write_record('stat_group_segment_values', $segment_value);

      // Video minutes
      $segment_value = $common + array(
        'type' => "week|$week|video_minutes",
        'value' => $video_time,
      );
      drupal_write_record('stat_group_segment_values', $segment_value);

    } // Advance to next chapter

    // Advance to the next week
    $check_date += $sevendays;
  }

}

// @todo note: using include file for: ipq_common_statistics_batch_new_finished this should be made a common func


/**
 * @param $data
    'chapter_ppm_averages' => $chapter_ppm_averages,
    'chapter_minutes_to_target' => $chapter_minutes_to_target,
    'chapter_weeks_averages' => $chapter_weeks_averages,
    'targets' => $targets[$chapter_id]->trending_average | $targets[$chapter_id]->distinct_questions,
    'actual_quiz_time_averages'
    'actual_vid_time_averages'
    'section_study_weeks'
 * @return array
 * - Renderable array
 */
function ipq_common_time_statistics_aggregates_page($data) {
  $output = '';


  $output .= '
    <div>
      Average total study weeks to pass all 4 parts of the exam: <strong>' . ''. '</strong>
    </div>';

  $ct = new RCPARCourseTree('2018');
  foreach ($ct->getAllSectionOpts() as $section_tid => $section_name) {
    $section_totals = 0;
    $rows = array();
    $opts = $ct->getSectionChapterAndTopicOptions($section_tid, TRUE);

    // Build one row per  chapter
    foreach ($opts['chapter_topic_options'] as $chap_id => $topics) {
    /*  $rows[] = [
        'data' => [
          // Cell 1:
          [
            'data' => '<strong>Chapter:' . $opts['chapter_options'][$chap_id] . '</strong>',
            'colspan' => 2,
          ]
        ],
      ];*/

      $rows[] = [
        '<strong>' . $opts['chapter_options'][$chap_id] . '</strong> (' . $chap_id . ')',// Chapter name
        number_format($data['chapter_minutes_to_target'][$chap_id] / 60, 2),  // Hours to target
        number_format($data['chapter_ppm_averages'][$chap_id], 2),            // Points per minute
        number_format($data['chapter_weeks_averages'][$chap_id], 2),          // Study weeks for chapter
        number_format($data['targets'][$chap_id]->trending_average, 1),       // Score target
        number_format($data['targets'][$chap_id]->distinct_questions, 1),     // Questions target
        number_format($data['actual_quiz_time_averages'][$chap_id] / 60 , 1), // Actual quiz hours
        number_format($data['actual_vid_time_averages'][$chap_id] / 60, 1),   // Actual video hours
      ];
      $section_totals += $data['chapter_minutes_to_target'][$chap_id];


    }

    // Print out a table for this exam part
    $vars = array(
      'header' => ['Chapter', 'Predicted study hours for target', 'SP points per minute', 'Avg study weeks', 'Score Target', 'Questions Target',
        'Actual quiz hours', 'Actual video hours'],
      'caption' => $section_name . '| Total predicted study hours: ' . number_format($section_totals / 60, 1)
        . ' Average study weeks: ' . $data['section_study_weeks'][$section_name],
      'rows' => $rows,
    );
    $output .= theme('table', $vars);
  }

  return array('#markup' => $output);
}