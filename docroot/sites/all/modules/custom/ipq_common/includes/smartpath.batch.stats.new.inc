<?php

/**
 * Page callback for /admin/settings/rcpar/ipq/config/smartpath/stats
 * Summarizes the aggregated data in the stat tables on screen.
 * @return array
 * - Renderable array
 */
function ipq_common_statistics_aggregates_page() {
  $output = '';

  $ct = new RCPARCourseTree('2018');
  foreach ($ct->getAllSectionOpts() as $section_tid => $section_name) {
    $rows = array();

    $opts = $ct->getSectionChapterAndTopicOptions($section_tid, TRUE);
    foreach ($opts['chapter_topic_options'] as $chap_id => $topics) {
      $rows[] = [
        'data' => [
          // Cell 1:
          [
            'data' => '<strong>Chapter:' . $opts['chapter_options'][$chap_id] . '</strong>',
            'colspan' => 5,
          ]
        ],
      ];

      foreach ($topics as $topic_id => $topic_name) {
        // Query for all values of this topic
        $vals = db_query("
          SELECT type, value FROM stat_group_aggregates 
          WHERE group_id = :gid AND scope = 'topic' and scope_identifier = :topid
          ORDER BY type ASC",
          [':gid' => 1, ':topid' => $topic_id])->fetchAllKeyed();
        $rows[] = [
          $topic_name, $vals['distinct_questions'], $vals['trending_avg'], $vals['std_dev_trending_avg'], $vals['sample_size'],
        ];
      }
    }

    // Print out a table
    $vars = array(
      'header' => ['Name', 'Distinct Questions', 'Score', 'Std. Deviation (score)', 'Sample size'],
      'caption' => $section_name,
      'rows' => $rows,
    );
    $output .= theme('table', $vars);
  }

  return array('#markup' => $output);
}

/**
 * A form for initiating a batch job to generate large-scale user data for statistics
 */
function ipq_common_statistics_batch_new_form($form, &$form_state) {
  // Get stat groups as table rows
  $rows = array();
  foreach (db_query("SELECT id, name, description FROM stat_groups")->fetchAllAssoc('id') as $item) {
    $rows[] = array($item->id, $item->name, $item->description);
  }

  $form['groupinfo'] = array(
    '#type' => 'fieldset',
    '#title' => 'Existing stat groups',
    '#description' => 'These are the group names that currently exist in the database.',
    'contents' => array(
      '#markup' => theme('table', ['rows' => $rows]),
    ),
  );

  // Group name
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Group name',
    '#default_value' => IPQ_SMARTPATH_COMPARE_GROUP,
    '#description' => 'A name to identify this new data set.' .
      '<br>The current group being used for SmartPath targets is: <strong>' . IPQ_SMARTPATH_COMPARE_GROUP . '</strong>',
    '#required' => TRUE,
  );

  // Group name
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => 'Description',
    '#description' => 'A description for this group, including how to query it.',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Build stats',
  );

  // Aggregates
  $form['aggregates'] = array(
    '#type' => 'submit',
    '#value' => 'Build aggregates',
  );

  return $form;
}

/**
 * Form submit handler for ipq_common_statistics_batch_new_form
 * Executes a job to either build statistics, or form aggregates from existing stat values
 */
function ipq_common_statistics_batch_new_form_submit($form, &$form_state) {
  $id = db_query("SELECT id FROm stat_groups WHERE name = :n", [':n' => $form_state['values']['name']])->fetchField();
  if(!$id) {
    $record = array(
      'name' => $form_state['values']['name'],
      'description' => $form_state['values']['description'],
    );
    drupal_write_record('stat_groups', $record);
    $id = $record['id'];
  }

  // Either 'Build aggregates' or 'Build stats'
  switch($form_state['values']['op']) {
    case 'Build aggregates':
      ipq_common_statistics_build_group_aggregates($id);
      break;

    case 'Build stats':
    default:
      ipq_common_statistics_batch_new($id);
      break;
  }
}

/**
 * Build aggregates from group values
 */
function ipq_common_statistics_build_group_aggregates($group_id) {
  // Clear out existing aggregates before we begin
  db_query("DELETE FROM stat_group_aggregates WHERE group_id = :gid", [':gid' => $group_id]);

  // Aggregate distinct_questions for each topic
  $vals = db_query("SELECT scope_identifier AS topic_id, AVG(value) avg 
        FROM stat_group_segment_values v
        INNER JOIN stat_group_segments s on v.segment_id = s.id
        WHERE s.group_id = :gid AND v.scope = 'topic' AND type = 'distinct_questions'
        GROUP BY scope_identifier", [':gid' => $group_id])->fetchAllKeyed();
  foreach ($vals as $topic_nid => $val) {
    $record = array(
      'group_id' => $group_id,
      'type' => 'distinct_questions',
      'value' => $val,
      'scope' => 'topic',
      'scope_identifier' => $topic_nid,
      'updated_on' => REQUEST_TIME,
    );
    drupal_write_record('stat_group_aggregates', $record);
  }

  // Aggregate trending_avg scores for each topic
  $vals = db_query("SELECT scope_identifier AS topic_id, AVG(value) avg 
        FROM stat_group_segment_values v
        INNER JOIN stat_group_segments s on v.segment_id = s.id
        WHERE s.group_id = :gid AND v.scope = 'topic' AND type = 'trending_avg'
        GROUP BY scope_identifier", [':gid' => $group_id])->fetchAllKeyed();
  foreach ($vals as $topic_nid => $val) {
    $record = array(
      'group_id' => $group_id,
      'type' => 'trending_avg',
      'value' => $val,
      'scope' => 'topic',
      'scope_identifier' => $topic_nid,
      'updated_on' => REQUEST_TIME,
    );
    drupal_write_record('stat_group_aggregates', $record);
  }

  // Get course data (sections, chapters, topics, etc)
  $context = array();
  ipq_common_statistics_batch_setup($context);
  $course_data = $context['results']['course_data'];

  // In order to calculate standard deviations for each topic's scores, we have to load all values and
  // pass them to the std deviation func one topic at a time
  foreach (['AUD', 'BEC', 'FAR', 'REG'] as $section) {
    foreach ($course_data[$section] as $chapter_nid => $topics) {
      foreach ($topics as $topic_nid) {
        // Query all values for this topic
        $vals = db_query("SELECT value 
        FROM stat_group_segment_values v
        INNER JOIN stat_group_segments s on v.segment_id = s.id
        WHERE s.group_id = :gid AND v.scope = 'topic' AND type = 'trending_avg'
        AND scope_identifier = :topid", [':gid' => $group_id, ':topid' => $topic_nid])->fetchCol();

        // Calculate standard deviation
        $sd = 0;
        if(count($vals) > 1) {
          $sd = stats_standard_deviation($vals);
        }

        // Record the SD value in the database
        $record = array(
          'group_id' => $group_id,
          'type' => 'std_dev_trending_avg',
          'value' => $sd,
          'scope' => 'topic',
          'scope_identifier' => $topic_nid,
          'updated_on' => REQUEST_TIME,
        );
        drupal_write_record('stat_group_aggregates', $record);

        // Record sample size value in the database
        $record = array(
          'group_id' => $group_id,
          'type' => 'sample_size',
          'value' => count($vals),
          'scope' => 'topic',
          'scope_identifier' => $topic_nid,
          'updated_on' => REQUEST_TIME,
        );
        drupal_write_record('stat_group_aggregates', $record);
      }
    }
  }

  drupal_set_message('Done building aggregates');
}

/**
 * Initializes the batch job for statistics gathering
 */
function ipq_common_statistics_batch_new($group_id) {
  $batch = array(
    'operations' => array(),
    'finished' => 'ipq_common_statistics_batch_new_finished',
    'title' => t('IPQ Statistics Stats'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'ipq_common') . '/includes/smartpath.batch.stats.new.inc'
  );

  // First batch operation: get course data
  $batch['operations'][] = array('ipq_common_statistics_batch_setup', array());

  // <DEV> Process a single user for troubleshooting
  /*$batch['operations'][] = array('ipq_common_statistics_batch_setup', array());
  // Find all the users that passed this section
  foreach (db_query("SELECT * FROM eck_exam_scores 
    WHERE score > 74 AND section = :section AND uid = :uid", array(':section' => 'AUD', ':uid' => 28982)) as $user_passing) {
    $batch['operations'][] = array('ipq_common_statistics_batch_new_process', array($user_passing, 'AUD', 1452, $group_id));
  }
  batch_set($batch);
  batch_process('admin/settings/rcpar/ipq/config/smartpath/targetdatanew'); // The path to redirect to when done.
  return;*/
  // </DEV>

  $ct = new RCPARCourseTree();
  foreach ($ct->getAllSectionOpts() as $section_nid => $section) {
    $section_nid = $ct->getSectionIdFromName($section);

    // Find all the users that passed this section
    foreach (db_query("SELECT * FROM eck_exam_scores WHERE score > 74 AND section = :section", array(':section' => $section)) as $user_passing) {
      $batch['operations'][] = array('ipq_common_statistics_batch_new_process', array($user_passing, $section, $section_nid, $group_id));
    }
  }

  batch_set($batch);
  batch_process('admin/settings/rcpar/ipq/config/smartpath/targetdata'); // The path to redirect to when done.
}

/**
 * Determine if the user has enough IPQ data in this section to meet our requirements. If not,
 * they are not worth analyzing.
 *
 * @param int $uid
 * @param int $section_nid
 * @param int $timestamp
 * @return bool
 */
function ipq_common_stats_check_sp_threshold($uid, $section_nid, $timestamp) {
  // Determine if the user has enough IPQ data in this section to meet our criteria
  // If not, we will skip them and not use their data. We'll ignore skipped questions
  // NOTE: the batch process runs out of memory or times out if we check this for every user. We can do it during processing
  $minimum_questions_per_section = IPQ_SMARTPATH_STATS_MIN_Q_PER_SEC;
  $count = db_query("SELECT count(*)
    FROM ipq_saved_session_data 
    WHERE uid = :uid AND section_id = :sectionid AND created_on < :examdate AND status <> 2
    LIMIT $minimum_questions_per_section", array(
    ':uid' => $uid,
    ':sectionid' => $section_nid,
    ':examdate' => $timestamp,
  ))->fetchField();

  return $count >= $minimum_questions_per_section;
}

/**
 * Process a user's IPQ data for statistics
 * @param object $user
 * - A row object from the eck_exam_scores that represents a user with a passing score
 * @param string $section\
 * - 'AUD', 'BEC', 'FAR', or 'REG'
 * @param $context
 */
function ipq_common_statistics_batch_new_process($user, $section, $section_nid, $group_id, &$context) {
  // Check if we already have this segment from a previous run:
  $check = db_query("SELECT id FROM stat_group_segments WHERE uid = :uid AND exam_id = :eid AND group_id = :gid",
    [':uid' => $user->uid, ':eid' => $user->id, ':gid' => $group_id])->fetchField();
  if($check) {
    drupal_set_message("Skipping existing segment: uid - {$user->uid} | exam attempt id - {$user->id}");
    return;
  }

  // Check if the user has enough IPQ data for this section to meet our threshold. If not, they're not worth bothering with.
  if(!ipq_common_stats_check_sp_threshold($user->uid, $section_nid, $user->exam_date)) {
    return;
  }

  // Get our stored course data
  $course_data = $context['results']['course_data'];

  // Store the group id that this data will be tagged with. @todo - do this once at first operation
  $context['results']['group_id'] = $group_id;

  // Increment the user count
  $context['results']['usercount']++;

  // Initialize holding variables for _this user_
  $topic_scores = $topic_attempts = $topic_sample_sizes =
  $chapter_scores = $chapter_attempts = $chapter_sample_sizes =
  $question_scores = $question_attempts = $question_sample_sizes = array();

  $where_clause = 'WHERE uid = :uid AND chapter_id = :chapterid AND topic_id = :topicid AND status <> 2 AND created_on < :date';
  foreach ($course_data[$section] as $chapter_nid => $topics) {
    foreach ($topics as $topic_nid) {
      // Query replacements for this chapter/topic
      $where_replacements = array(
        ':uid' => $user->uid,
        ':chapterid' => $chapter_nid,
        ':topicid' => $topic_nid,
        ':date' => $user->exam_date,
      );

      // Get scores for this topic
      $latest_scores = db_query("
        SELECT FLOOR(score / max_score * 100) as avg, ipq_question_id, count(ipq_question_id) attempts
        FROM ipq_saved_session_data i
        INNER JOIN (SELECT MAX(id) maxc FROM ipq_saved_session_data $where_clause GROUP BY ipq_question_id)
                   maxc_table ON (i.id = maxc_table.maxc)
        $where_clause
        GROUP BY ipq_question_id", $where_replacements)->fetchAllAssoc('ipq_question_id');

      // Don't store this users statistics if they didn't do any quizzes on this chapter
      if (count($latest_scores) == 0) {
        continue;
      }

      // We have some scores for this topic, so we can increment the sample size counter
      $topic_sample_sizes[$topic_nid]++;

      // Iterate over each question attempt
      foreach ($latest_scores as $question_id => $score) {
        // Store score average and attempts per question
        $question_scores[$question_id] = $score->avg;
        $question_attempts[$question_id] = $score->attempts;
        $question_sample_sizes[$question_id]++;

        // Add to topic score average
        $topic_scores[$topic_nid] += $score->avg;
        $topic_attempts[$topic_nid]++;
      }

      // Chapter level: Add the topic scores and number of question attempts to the chapter
      $chapter_scores[$chapter_nid] += $topic_scores[$topic_nid];
      $chapter_attempts[$chapter_nid] += $topic_attempts[$topic_nid];
    }

    // If we have any data for this chapter, increment the chapter sample size counter
    $chapter_sample_sizes[$chapter_nid]++;

    // @todo? - get unused/legacy stats: total score averages and total question count (as opposed to trending and distinct)
  } // </chapter foreach>
  
  // Store the group segment
  $segment_record = array(
    'uid' => $user->uid,
    'date_end' => $user->exam_date,
    'group_id' => $group_id,
    'exam_id' => $user->id,
    'updated_on' => REQUEST_TIME,
  );
  drupal_write_record('stat_group_segments', $segment_record);


  // Add chapter scores and question counts to the totals
  foreach ($chapter_scores as $chapter_nid => $chapter_score) {
    // Update the database incrementally

    // Chapter trending average
    $segment_value = array(
      'segment_id' => $segment_record['id'],
      'type' => 'trending_avg',
      'value' => $chapter_score / $chapter_attempts[$chapter_nid],
      'scope' => 'chapter',
      'scope_identifier' => $chapter_nid,
    );
    drupal_write_record('stat_group_segment_values', $segment_value);

    // Chapter distinct questions
    $segment_value = array(
      'segment_id' => $segment_record['id'],
      'type' => 'distinct_questions',
      'value' => $chapter_attempts[$chapter_nid],
      'scope' => 'chapter',
      'scope_identifier' => $chapter_nid,
    );
    drupal_write_record('stat_group_segment_values', $segment_value);
  }

  // Add topic scores and question counts to the totals
  foreach ($topic_scores as $topic_nid => $topic_score) {
    // Topic trending average
    $segment_value = array(
      'segment_id' => $segment_record['id'],
      'type' => 'trending_avg',
      'value' => $topic_score / $topic_attempts[$topic_nid],
      'scope' => 'topic',
      'scope_identifier' => $topic_nid,
    );
    drupal_write_record('stat_group_segment_values', $segment_value);

    // Topic distinct questions
    $segment_value = array(
      'segment_id' => $segment_record['id'],
      'type' => 'distinct_questions',
      'value' => $topic_attempts[$topic_nid],
      'scope' => 'topic',
      'scope_identifier' => $topic_nid,
    );
    drupal_write_record('stat_group_segment_values', $segment_value);
  }

}


/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function ipq_common_statistics_batch_new_finished($success, $results, $operations){
  if ($success) {
    drupal_set_message('Batch completed!');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}