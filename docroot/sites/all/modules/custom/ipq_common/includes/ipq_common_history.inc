<?php

/**
 * Page callback for 'ajax/dashboard/testcenter/history/%/%'
 * Renders a user's IPQ history, used in the context of a professor viewing the IPQ history of another user in the
 * monitoring center.
 * Note that this menu callback is defined in the mc_profesors.module
 *
 * @param $uid
 * - The user ID we're loading history for.
 * @return string
 * - Rendered HTML
 */
function ipq_common_history_ajax($uid){
    $args = func_get_args();
    // on the ajax version, the section comes before the group and it's optional
    if (func_num_args() == 2){ // no group id
        $group_id = null;
        $section = $args[1];
    } else {
        $group_id = $args[1];
        $section = $args[2];
    }

    if (!ipq_common_perms($uid, $section)){
        return MENU_ACCESS_DENIED;
    }

    drupal_add_js(drupal_get_path('module','ipq').'/js/ipq.js');
    return ipq_history($uid, $section);
}

/**
  * Delete all session related data
  */
function ipq_history_del_session_json($sid) {
    if (isset($sid) && $sid != null) {
      // delete session
      db_query("delete from {ipq_saved_sessions} WHERE id = :session_id ", array(":session_id" => $sid));
      // delete saved data
      db_query("delete from {ipq_saved_session_data} WHERE ipq_saved_sessions_id = :session_id ", array(":session_id" => $sid));
      // delete notes
      //db_query("delete from {ipq_notes} WHERE ipq_saved_session_data_id = :session_id ", array(":session_id" => $sid));
      // delete session overview data data
      db_query("delete from {ipq_session_overview} WHERE ipq_saved_sessions_id = :session_id ", array(":session_id" => $sid));
      // send response
      drupal_json_output(array('status' => 'success', 'message' => t('Session deleted successfully.')));
    } else {
        drupal_json_output(array('status' => 'failed'));
    }
}

/**
 * Generates the content for IPQ score history page.
 *
 * Two contexts:
 * 1. Called by the page callback (ipq_user_sections_history() '/ipq/history') - this context is a user viewing their OWN
 * history.
 * 2. Also called by ipq_common_history_ajax() - this is an ajax method used when a professor is viewing a user's history
 * in the monitoring center.
 *
 * @param int $uid
 * - The user whose history we are showing
 * @param string $section
 * - 'AUD', 'FAR', 'BEC', or 'REG'
 * @return string
 * - Rendered HTML
 */
function ipq_history($uid, $section = null) {
    $user = user_load($uid);    
    $user_have_no_quizzes = false;

    if (!ipq_common_perms($uid, $section)){
        return MENU_ACCESS_DENIED;
    }

    if (!$section){
        return " ";
    }
    
    $sessions = ipq_history_get_data($user->uid,$section);
    $data = array(
        'history' =>
            array(
                'account_uid'=>$uid,
                'group_id'=>0,
                'section'=>$section,
                'section_version' => user_entitlements_get_exam_version_for_course($section),
                'sections' => rcpar_dashboard_entitlements_options(),
                'sessions' => $sessions,
            )
    );
    foreach($data['history']['sessions'] as $session) {
      $data['forms'][$session['session_id']] = drupal_get_form('ipq_history_rename_history_form', $session);
    }
    drupal_add_js(drupal_get_path('module','ipq_common') . '/js/ipq-history.js');
    $page_content = theme('ipq_common_history', $data, $form);
    $pager = theme('pager');
    return $page_content . '<div class="ajax-nav-pager">'.$pager.'</div>';
}

/**
 * This form is used to allow users to rename their sessions on the ipq history page
 * @param $form
 * @param $form_state
 * @param $session
 * @return mixed
 */
function ipq_history_rename_history_form($form, &$form_state, $session){
  $form_state['storage']['ipq_session']['id'] = $session['session_id'];
  if(isset($form_state['values']['new_name'])){
    $name = $form_state['values']['new_name'];
  } else {
    $name = $session['session_name'];
  }
  //This flag will not be needed as monitoring system is currently separate from the IPQ. Leaving it in for now for future ref.
  $has_ajax = in_array('ajax', arg()); //setting to true if is coming from monitoring center of professors.
  
  $form['name_container_'.$session['session_id']] = array(
    '#type' => 'container',
    '#prefix' => '<div id="name-container-'.$session['session_id'].'">',
    '#suffix' => '</div>',
  );
  $form['name_container_'.$session['session_id']]['current_name'] = array(
    '#prefix' => '<div id="current-name-container-'.$session['session_id'].'">',
    '#suffix' => '</div>',
    '#markup' =>  $name . ' <span>'.'<a href="" class="rename-session" id="rename_current_session_'.$session['session_id'].'"><i class="fa fa-pencil"></i> Rename</a></span>',
  );
  $form['name_container_'.$session['session_id']]['new_name_container'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="new-name-container-'.$session['session_id'].'" style="display:none">',
    '#suffix' => '</div>',
  );

  $form['name_container_'.$session['session_id']]['new_name_container']['new_name'] = array(
    '#type' => 'textfield',
    '#default_value' => $name,
    '#maxlength' => '100',
  );
  $form['name_container_'.$session['session_id']]['new_name_container']['save_new_name'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('ipq_history_ajax_change_session_name_submit'),
    '#ajax' => array(
      'callback' => 'ipq_history_ajax_change_session_name',
      'wrapper' => 'name-container-'.$session['session_id'].'',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  return $form;
}

/**
 *  ajax submit function for pq history rename session feature
 *
 * @param $form
 * @param $form_state
 */
function ipq_history_ajax_change_session_name_submit($form, &$form_state){
  $session_id = $form_state['storage']['ipq_session']['id'];
  db_update('ipq_saved_sessions')
    ->fields(array('name' => $form_state['values']['new_name']))
    ->condition('id', $session_id, '=')
    ->execute();
  $form_state['storage']['ipq_session']['name'] = $form_state['values']['new_name'];
  $form_state['rebuild'] = TRUE;
}

/**
 *  ajax callback function for 'quiz_details' stage rename session
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function ipq_history_ajax_change_session_name($form, &$form_state){
  $session_id = $form_state['storage']['ipq_session']['id'];
  return $form['name_container_'.$session_id];
}

/**
 * Page callback for ipq/history
 * Displays a user's own IPQ history
 *
 * @return string
 * - Rendered HTML
 */
function ipq_user_sections_history (){
  global $user;
  $current_section =  ipq_common_get_default_section();
  ipq_common_set_section($current_section);  
  return  ipq_history($user->uid, $current_section);  
}

/**
 * Page callback for ipq/history/%/%/%ctools_js
 * A link wih the URL pattern of this callback is generated in ipq_common/templates/ipq_user_sections_history.tpl.php
 * Theme hook ipq_user_sections_history registers this tpl but does not seem to ever be invoked.
 *
 * @todo - This page callback is probably unused
 */
function ipq_user_history($uid, $section){
  ipq_common_set_section($section);
  drupal_goto('ipq/history');
}

/**
 * Get info on all of a user's IPQ sessions for a given section.
 *
 * @param int $uid
 * @param string $section
 * - Exam section, e.g. 'AUD', 'BEC', etc
 * @return array
 * - A numeric array of associative arrays containing IPQ session data
 */
function ipq_history_get_data($uid, $section) {
  $data = array();

  $terms = taxonomy_get_term_by_name($section, 'course_sections');
  $section_term = array_pop($terms);

  // Get active user entitlement for the current section
  $user = new stdClass;
  $user->uid = $uid;
  $entitlement_id = user_entitlements_get_active_course_entitlement_for_section($user, $section_term->tid);

  /** @var PagerDefault $query */
  $query = db_select('ipq_saved_sessions', 'ss')
    ->extend('PagerDefault')
    ->fields('ss', array(
      'id',
      'name',
      'session_type',
      'created_on',
      'section_id',
      'percent_correct',
      'entitlement_id',
      'session_config',
      'finished',
    ))
    ->condition('ss.uid', $uid)
    ->condition('ss.section_id', $section_term->tid)
    ->orderBy('ss.updated_on', 'DESC')
    ->limit(10);
  $query->addField('ss', 'percent_correct', 'percent_correct_ss');
  // This tag allows the query to be altered by rcpar_cp_query_ipq_history_get_data_alter()
  // (and possibly others.)
  $query->addTag('ipq_history_get_data');

  // If entitlement_id exist add the codition to the query
  if (isset($entitlement_id)) {
    $query->condition('ss.entitlement_id', $entitlement_id);
  }

  $result = $query->execute();

  while ($record = $result->fetchObject()) {
    $session_config = drupal_json_decode($record->session_config);
    $exam_version = isset($session_config['exam_version']) ? $session_config['exam_version'] : '2016';
    
    //if we have a missmatch on the counts and the list we should validate we are not missing them on the structure
    if(count($session_config['question_list']) !== count($session_config['questions_counts']) && !$session_config['smart_quiz']){      
      ipq_history_match_question_count($session_config, $record->id);
    }    

    if ($session_config['questions_counts']) {
      // If questions counts has 0 in one option
      if(in_array('0', $session_config['questions_counts'])){
        // Update the session config to show only the selected options
        // avoiding the issue showing zeros.
        ipq_history_clean_question_count($session_config, $record->id);
      }
      $q_types = $session_config['questions_counts'];
    }
    elseif ($session_config['combined_quizlet']) {
      $q_types = $session_config['question_types'];
    }
    else{
      $q_types = ipq_history_get_question_count($record->id);
    }

    $chapters = node_load_multiple($session_config['chapters']);
    $data[] = array(
      'session_id'               => $record->id,
      'session_name'             => $record->name,
      'session_type'             => $record->session_type,
      'timestamp'                => $record->created_on,
      'section'                  => $record->section_id,
      'exam_version'             => $exam_version,
      'exam_version_description' => $exam_version,
      'chapters_covered'         => $chapters,
      'chapters_covered_count'   => count($chapters),
      'q_types'                  => $q_types,
      'total_percent'            => $record->percent_correct,
      'entitlement_id'           => $record->entitlement_id,
      'finished'                 => $record->finished,
    );
  }
  return $data;
}

/**
 *
 * @param $session_id
 *  - the ipq session id
 * @return
 *   - an array of questions types and as a value the count of questions related to it
 */
function ipq_history_get_question_count($session_id) {
  $sql = "SELECT qt.type,count(qt.id) as total
          FROM ipq_saved_session_data sd
          INNER JOIN ipq_question_types qt on qt.id = sd.ipq_question_type_id
          WHERE sd.ipq_saved_sessions_id=:session_id
          GROUP BY qt.id";

  $q_types = array();

  $result = db_query($sql, array(":session_id" => $session_id));
  foreach ($result as $record) {
    $q_types[$record->type] = $record->total;
  }

  return $q_types;
}

/**
 * @param $session_config
 *  - the ipq session data
 * @param $session_id
 *  - the ipq session id
 */
function ipq_history_clean_question_count(&$session_config,$session_id) {
  foreach($session_config['questions_counts'] as $key => $value){
    if(0 == $value){
      // Unset the rows with 0
      unset($session_config['questions_counts'][$key]);
    }
  }
  // If question counts is empty
  // get the correct list of question
  if(empty($session_config['questions_counts'])){
    foreach($session_config['question_types'] as $key => $value){
      if(0 < $value){
        $session_config['questions_counts'][$key] = $value;
      }
    }
  }
  // Update the session config
  $sql = "UPDATE ipq_saved_sessions 
          SET session_config = :session
          WHERE id=:session_id";
  $result = db_query($sql, array(":session_id" => $session_id, ":session" => json_encode($session_config)));
}

/**
 * Updates the session confg counts so we display them properly on the overview
 * @param $session_config
 *  - the ipq session data
 * @param $session_id
 *  - the ipq session id
 */
function ipq_history_match_question_count(&$session_config, $session_id) {
  $questions_counts = array();

  $sql = "SELECT qt.type,count(qt.type) as total FROM ipq_saved_session_data q
          INNER JOIN ipq_question_types qt on qt.`id` = q.`ipq_question_type_id`
          WHERE ipq_saved_sessions_id=:session_id group by qt.type";

  $result = db_query($sql, array(":session_id" => $session_id))->fetchAll();

  //loop over the results to properly set the counts
  foreach ($result as $record) {
    if (in_array($record->type, array('ipq_mcq_question', 'ipq_wc_question'))) {
      $questions_counts[$record->type] = $record->total;
    }
    else {
      $questions_counts['ipq_tbs_question'] = isset($questions_counts['ipq_tbs_question']) ? $questions_counts['ipq_tbs_question'] += $record->total : $record->total;
    }
  }
  $session_config['questions_counts'] = $questions_counts;  
  // Update the session config
  $sql = "UPDATE ipq_saved_sessions 
          SET session_config = :session
          WHERE id=:session_id";
  $result = db_query($sql, array(":session_id" => $session_id, ":session" => json_encode($session_config)));
}
