<?php

/**
 * Form to start executing the process of cleaning orphan's on the 
 * tree structure Course -> Chapter -> Topic
 */
function ipq_common_clean_question_pool_batch_form($form, $form_state) {
  $form['actions']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Start Update Sessions Process'),
    '#submit' => array('ipq_common_clean_pool_batch'),
  );

  return $form;
}

/**
 * Operations of the batch to start cleaning each orphan on 
 * the tree structure Course -> Chapter -> Topic
 */
function ipq_common_clean_pool_batch($form, $form_state) {
  $operations = array();

  // Get the chapters to process.
  $result = db_query("SELECT nid
    FROM {node}
    WHERE type = 'rcpa_chapter'
    AND status = '1'", array()
  );

  foreach ($result as $chapter) {
    $node = node_load($chapter->nid);
    $operations[] = array('ipq_common_clean_questions_pool', array($node));
  }

  // Batch
  $batch = array(
    'operations' => $operations,
    'finished' => 'ipq_common_finished_clean_batch',
    'title' => t('Cleaning Question Pool'),
    'init_message' => t('Inicializing Cleaning Process.'),
    'progress_message' => t("Processed @current out of @total Chapters."),
    'error_message' => t('Error'),
    'file' => drupal_get_path('module', 'ipq_common') . '/includes/ipq_commom.clean_pool.inc',
  );
  batch_set($batch);
}

/**
 * Last message when the batch finish it's procesing.
 */
function ipq_common_finished_clean_batch(){
  drupal_set_message("All Chapters have been processed");
}

/**
 * Get all Topics per Chapter and clean bad reference
 * Using the relationship between topic and the chapter to know if there is any question with problems
 * @param 
 *  - node of type Chapter to check
 */
function ipq_common_clean_questions_pool($node) {
  try {
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $topics = $node_wrapper->field_topic_per_exam_version->value();
    foreach ($topics as $topic) {
      // Delete bad reference on the Question Pool and cou_chap_top_ref_by_exam_version
      ipq_common_delete_question_pool($topic, $node->nid);
    }
  }
  catch (EntityMetadataWrapperException $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}

/**
 * Delete bad reference on the Question Pool and cou_chap_top_ref_by_exam_version
 * Using the relationship between topic and the chapter to know if there is any question with problems.
 * @param
 *  - node of type Topic to be check
 * @param
 *  - int $chapter_id nid of the chapter that its uploading.
 */
function ipq_common_delete_question_pool($node, $chapter_id) {
  try {
    $wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $node);
    $exam_version = $wrapper->field_exam_version_single_val->value();
    $topics = [];
    foreach ($wrapper->field_topic_reference->value() as $topic) {
      // The null values will be ignore.
      if($topic->nid){
        $topics [] = $topic->nid;
      }
    }
    // Set only the Topics with value different that NULL
    $wrapper->field_topic_reference->set($topics);
    $wrapper->save();
    // Delete the reference between Topics and cou_chap_top_ref_by_exam_version.
    ipq_common_delete_topic_reference($exam_version->tid, $topics, $chapter_id);
    // Delete the questions from the pool that have the current chapter and
    //  have a different Topic than the current ones
    $query = "
      DELETE FROM ipq_question_pool 
      WHERE exam_version_id = :exam_version and chapter_id = :chapter_id and topic_id NOT IN (:topics)";
    $result = db_query($query, array(
      ":exam_version" => $exam_version->tid,
      ":chapter_id" => $chapter_id,
      ":topics" => $topics
        )
    );
  }
  catch (EntityMetadataWrapperException $e) {
    watchdog(__FILE__, "function " . __FUNCTION__ . " entity error: " . $e->getMessage());
  }
}
/**
 * Delete the reference between Topics and cou_chap_top_ref_by_exam_version
 * When the list of topics change in the current Chapter.
 * @param 
 *  - int $exam_version the term id of the exam version
 * @param 
 *  - array $topics list of Topics nodes.
 * @param 
 *  - int $chapter_id nid of the Chapter that its checking
 */
function ipq_common_delete_topic_reference($exam_version, $topics, $chapter_id) {
  $query = new EntityFieldQuery();
  // Get all cou_chap_top_ref_by_exam_version entitys with problems
  $query->entityCondition('entity_type', 'node_ref_by_exam_version')
      ->entityCondition('bundle', 'cou_chap_top_ref_by_exam_version')
      ->fieldCondition('field_exam_version_single_val', 'tid', $exam_version)
      ->fieldCondition('field_ipq_chapter', 'target_id', $chapter_id)
      ->fieldCondition('field_ipq_topic', 'target_id', $topics, 'NOT IN'); 
  $result = $query->execute();
  if (isset($result['node_ref_by_exam_version'])) {
    foreach ($result['node_ref_by_exam_version'] as $value) {
      // Load the cou_chap_top_ref_by_exam_version entity
      $entity = array_shift(entity_load('node_ref_by_exam_version', array($value->id)));
      // if the entity has one topic 
      $wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $entity);
      // The value in array will be null if the Topic has been removed.
      if (count($wrapper->field_ipq_topic->value()) == 1 &&
        !$wrapper->field_ipq_topic->value()[0]) {
        // Delete all entity
        entity_delete('node_ref_by_exam_version', $value->id);
      }
      else {
        // Set the current list of Topics
        $wrapper->field_ipq_topic->set($topics);
        $wrapper->save();
      }
    }
  }
}