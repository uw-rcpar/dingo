<?php


/**
 * Action callback for ipq_common_remove_version_action
 * @param $node
 * @param array $context
 * - Contains an array with key 'version', which holds a numeric exam version term id
 */
function ipq_common_remove_version_action($node, $context) {
  $node_wrapper = entity_metadata_wrapper('node', $node);
  ipq_common_remove_version($context['version'], $node_wrapper);
}

/**
 * Utility function to remove an exam version tagging for specified year from any node
 *
 * @param int $remove_version_tid
 * - The term ID of the new exam version.
 * @param $node_wrapper
 * - An entity metadata wrapper of the entity whose version info we're copying. Can be any node type
 * with a supported exam version field.
 */
function ipq_common_remove_version($remove_version_tid, $node_wrapper) {
  // A list of potential fields that hold version data
  $fields = array(
    'field_section_per_exam_version',
    'field_chapter_per_exam_version',
    'field_topic_per_exam_version',
  );

  // Iterate through each of the possible fields that could hold version information and copy it, if possible
  foreach ($fields as $reference_field) {
    try {
      foreach ($node_wrapper->{$reference_field} as $delta => $rw) {
        if ($rw->field_exam_version_single_val->tid->value() == $remove_version_tid) {
          unset($node_wrapper->{$reference_field}[$delta]);
        }
      }
    }
    catch (EntityMetadataWrapperException $e) {
      // Entities that don't have this field will throw an exception, we just catch it and continue on to the next
    }
  }
}

/**
 * Generates the settings form for ipq_common_remove_version().
 */
function ipq_common_remove_version_action_form($context) {
  // Return our form array:
  return array(
    'version' => array(
      '#type' => 'select',
      '#title' => 'Exam version',
      '#description' => 'Choose the exam version tagging to remove.',
      '#options' => array_flip(exam_versions_years()),
    ),
  );
}

/**
 * Submit handler for ipq_common_remove_version_action_form
 * Simply passes the submitted values back to the context array for later use.
 */
function ipq_common_remove_version_action_submit($form, $form_state) {
  // Return the version tid for the action function to use
  return array('version' => $form_state['values']['version']);
}

/**
 * Action callback for ipq_common_add_version_action
 * @param $node
 * @param $context
 * - Holds all needed data to create a new exam reference entity.
 * an associative array with keys:
 * field_exam_version_single_val (term id)
 * field_ipq_chapter (entity id)
 * field_ipq_course_section (term id)
 * field_ipq_topic (entity id)
 */
function ipq_common_add_version_action($node, $context) {
  // create exam version entity and add ID to node
  $node_wrapper = entity_metadata_wrapper('node', $node);

  // Build and save a new exam reference wrapper
  $new_reference_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $context['entity']);
  $new_reference_wrapper->field_exam_version_single_val->set($context['field_exam_version_single_val']);
  $new_reference_wrapper->field_ipq_chapter->set($context['field_ipq_chapter']);
  $new_reference_wrapper->field_ipq_course_section->set($context['field_ipq_course_section']);
  $new_reference_wrapper->field_ipq_topic->set(array($context['field_ipq_topic']));
  $new_reference_wrapper->save();

  // Append the new entity to the entity reference field and save the node
  $node_wrapper->field_section_per_exam_version[] = $new_reference_wrapper->getIdentifier();
}

/**
 * Generates the settings form for ipq_common_add_version_action().
 * This function mimics the widget of the ajaxy exam reference field widget found
 * on IPQ questions. It became so complicated to modify the code in the context of
 * the action settings form that it almost surely would have made more sense to
 * rebuild a new form outside of the content field/inline entity form system.
 * Oh well. -jd
 */
function ipq_common_add_version_action_form($context) {
  $form = $form_state = array();
  $entity = entity_create('node', ['type' => 'ipq_mcq_question']);

  $instances = _field_invoke_get_instances('node', 'ipq_mcq_question', [
    //'field_name' => 'field_section_per_exam_version'
    'default' => TRUE,
    'deleted' => FALSE,
    'language' => "und",
  ]);
  $instance = $instances['field_section_per_exam_version'];
  $field = field_info_field_by_id($instance['field_id']);

  // With "action forms", post data is not passed on to subsequent form_alter functions that rely
  // on it in order to update the widget via ajax, so we inject them in.
  if(!empty($_POST['field_section_per_exam_version'])) {
    $lang = LANGUAGE_NONE;

    // Data massage for exam_versions_inline_entity_form_entity_form_alter()
    // For some reason the post values are not where it expects them to be
    $values = $_POST['field_section_per_exam_version'][$lang]['form'];
    $values['field_exam_version_single_val'][$lang] = array(0 => ['tid' => $values['field_exam_version_single_val'][$lang]]);
    $values['field_ipq_course_section'][$lang] = array(0 => ['tid' => $values['field_ipq_course_section'][$lang]]);
    $values['field_ipq_chapter'][$lang] = array(0 => ['target_id' => $values['field_ipq_chapter'][$lang]]);

    $form_state['values']['field_section_per_exam_version'][$lang]['form'] = $values;
  }

  // IEF does not work with VBO under normal circumstances, and the module uses an alter hook
  // (inline_entity_form_field_widget_properties_alter()) to remove its widget when a VBO view
  // is being rendered. We have to trick it into thinking we're not in a VBO form, so we remove
  // the current view from the cache and then re-insert it when we're done
  $view = views_get_current_view();
  views_set_current_view(new stdClass());
  $form_addition = field_default_form('ipq_mcq_question', $entity, $field, $instance, LANGUAGE_NONE, [], $form, $form_state);
  views_set_current_view($view);

  // This element validator (inline_entity_form_entity_form_validate()) causes problems because IEF gets confused
  // when there is no information about IEF in the form state
  unset($form_addition['field_section_per_exam_version']['und']['form']['#element_validate'][0]);
  unset($form_addition['field_section_per_exam_version']['und']['#element_validate'][1]);

  // Remove IEF button and change fieldset title
  unset($form_addition['field_section_per_exam_version']['und']['form']['actions']);
  $form_addition['field_section_per_exam_version']['und']['form']['#title'] = 'Choose the exam version tagging to apply';

  return $form_addition;
}

/**
 * Submit handler for ipq_common_add_version_action_form
 * Simply passes the submitted values back to the context array for later use.
 */
function ipq_common_add_version_action_submit($form, $form_state) {
  $vals = $form_state['values']['field_section_per_exam_version']['und']['form'];
  return array(
    'entity' => $vals['entity'],
    'field_exam_version_single_val' => $vals['field_exam_version_single_val']['und'][0]['tid'],
    'field_ipq_chapter' => $vals['field_ipq_chapter']['und'][0]['target_id'],
    'field_ipq_course_section' => $vals['field_ipq_course_section']['und'][0]['tid'],
    'field_ipq_topic' => $vals['field_ipq_topic']['und'][0]['target_id'],
  );
}

/**
 * Swap topic form builder.
 * Helps admin replace a topic in the specified exam version and transfer its IPQ questions.
 */
function ipq_common_swap_topic_form($form, &$form_state) {
  // Get a list of ALL available topics in the system.
  $topic_options = array();
  $result = db_query("SELECT nid, title, status FROM node WHERE type = 'rcpa_topic' ORDER BY nid ASC");
  foreach ($result as $item) {
    $topic_options[$item->nid] = "($item->nid) $item->title | Status: $item->status";
  }

  // Instructions
  $form['instructions'] = array(
    '#markup' => '<div>This form allows you to remove a topic from the courseware and replace it with a new topic.<br>
    In the process, all IPQ questions tagged with the old topic will become tagged with the new topic.<br>
    Note that the topic is not deleted, it is simply untagged from its parent chapter.<br>
    <strong>IMPORTANT NOTE: Course stats will need to be rebuilt after running this operation in order to fully take effect.</strong>
    </div>',
  );

  // Old topic field
  $form['old_topic_id'] = array(
    '#title' => 'Old topic',
    '#description' => 'Choose the topic that will be removed and replaced.',
    '#type' => 'select',
    '#options' => $topic_options,
  );

  // New topic field
  $form['new_topic_id'] = array(
    '#title' => 'New topic',
    '#description' => 'The old topic will be replaced with the new topic',
    '#type' => 'select',
    '#options' => $topic_options,
  );

  // Exam Version field
  $form['exam_version_tid'] = array(
    '#title' => 'Exam version',
    '#description' => 'Topic replacement will be limited to only the specified exam version.',
    '#type' => 'select',
    '#options' => array_flip(exam_versions_years()), // array of years keyed by tid
    '#default_value' => exam_version_get_default_version_tid(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

/**
 * Validate callback for ipq_common_swap_topic_form
 */
function ipq_common_swap_topic_form_validate(&$form, &$form_state) {
  // Old and new topic can't be the same
  if($form_state['values']['old_topic_id'] == $form_state['values']['new_topic_id']) {
    form_set_error('new_topic_id', 'New topic cannot be the same as old topic.');
  }
}

/**
 * Submit callback for ipq_common_swap_topic_form
 * Replaces references to the old topic with the new topic.
 */
function ipq_common_swap_topic_form_submit(&$form, &$form_state) {
  set_time_limit(0);

  // Grab our key form values.
  $old_topic_id = $form_state['values']['old_topic_id'];
  $new_topic_id = $form_state['values']['new_topic_id'];
  $exam_version_tid = $form_state['values']['exam_version_tid'];

  // Perform the topic swap in our course chapters..
  ipq_common_chapters_replace_topic($old_topic_id, $new_topic_id, $exam_version_tid);

  // Perform the topic swap in IPQ questions
  ipq_common_questions_replace_topic($old_topic_id, $new_topic_id, $exam_version_tid);

  // Lastly, we need to update the IPQ question pool directly, since simply saving the node_ref_by_exam_version
  // entities is not enough to make this happen. We could also re-save the question nodes, but this is more performant.
  db_query("UPDATE ipq_question_pool SET topic_id = :newt WHERE topic_id = :oldt AND exam_version_id = :extid", [
    ':newt' => $new_topic_id,
    ':oldt' => $old_topic_id,
    'extid' => $exam_version_tid,
  ]);

  // Success messaging.
  $rebuild = l('rebuild course stats', 'admin/settings/rcpar/rebuild-course-stats');
  drupal_set_message("Topic swap is complete. Topic ID $old_topic_id has been replaced with $new_topic_id. It is recommended to $rebuild when you are finished making course adjustments.");
}

/**
 * Swap out an old topic for a new topic in course chapter nodes.
 *
 * @param int $old_topic_id
 * @param int $new_topic_id
 * @param int $exam_version_tid
 */
function ipq_common_chapters_replace_topic($old_topic_id, $new_topic_id, $exam_version_tid) {
  // Find the topic_ref_by_exam_version entities that the old topic is tagged in. This is essentially equivalent
  // to finding all of the chapters that the topic is in, as it is the 'rcpa_chapter' content type that utilizes
  // the topic_ref_by_exam_version entity reference for forming relationships.
  $query = new EntityFieldQuery();
  $ref_entity_type = 'node_ref_by_exam_version';
  $result = $query->entityCondition('entity_type', $ref_entity_type)
                  ->entityCondition('bundle', 'topic_ref_by_exam_version')
                  ->fieldCondition('field_topic_reference', 'target_id', $old_topic_id)
                  ->fieldCondition('field_exam_version_single_val', 'tid', $exam_version_tid)
                  ->execute();

  // If we can find any instances of the old topic, we'll replace those with the new topic.
  if (!empty($result[$ref_entity_type])) {
    $ids = array_keys($result[$ref_entity_type]);
    $entities = entity_load($ref_entity_type, $ids);

    // Remove the old topic from its chapters by taking the loaded  topic_ref_by_exam_version entity and removing the old topic.
    foreach ($entities as $entity) {
      $wrapper = entity_metadata_wrapper($ref_entity_type, $entity);
      foreach ($wrapper->field_topic_reference as $delta => $topic_wrap) {
        if ($topic_wrap->getIdentifier() == $old_topic_id) {
          // Out with the old, in with the new.
          $wrapper->field_topic_reference[$delta] = $new_topic_id;
          $wrapper->save();
        }
      }
    }
  }
}

/**
 * Swap out an old topic for a new topic in IPQ question nodes.
 *
 * @param int $old_topic_id
 * @param int $new_topic_id
 * @param int $exam_version_tid
 */
function ipq_common_questions_replace_topic($old_topic_id, $new_topic_id, $exam_version_tid) {
  // Find the cou_chap_top_ref_by_exam_version  entities that the old topic is tagged in.
  // IPQ questions use this entity to describe their course relationships.
  $query = new EntityFieldQuery();
  $ref_entity_type = 'node_ref_by_exam_version';
  $result = $query->entityCondition('entity_type', $ref_entity_type)
                  ->entityCondition('bundle', 'cou_chap_top_ref_by_exam_version ')
                  ->fieldCondition('field_ipq_topic', 'target_id', $old_topic_id)
                  ->fieldCondition('field_exam_version_single_val', 'tid', $exam_version_tid)
                  ->execute();

  // If we can find any instances of the old topic, we'll replace those with the new topic.
  if (!empty($result[$ref_entity_type])) {
    $ids = array_keys($result[$ref_entity_type]);
    $entities = entity_load($ref_entity_type, $ids);

    // Remove the old topic from this question by taking the loaded  cou_chap_top_ref_by_exam_version entity and removing the old topic.
    foreach ($entities as $entity) {
      $wrapper = entity_metadata_wrapper($ref_entity_type, $entity);
      foreach ($wrapper->field_ipq_topic as $delta => $topic_wrap) {
        if ($topic_wrap->getIdentifier() == $old_topic_id) {
          // Out with the old, in with the new.
          $wrapper->field_ipq_topic[$delta] = $new_topic_id;
          $wrapper->save();
        }
      }
    }
  }
}
