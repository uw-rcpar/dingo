<?php

function ipq_common_update_sessions_batch_form($form, $form_state) {

    $form['actions']['update'] = array(
        '#type' => 'submit',
        '#value' => t('Start Update Sessions Process'),
        '#submit' => array('ipq_common_update_sessions_batch'),
    );

    return $form;
}

/**
 * Process sessions and start the batch
*/
function ipq_common_update_sessions_batch($form, $form_state){
    $operations = array();
    $ipq_id_restriction =  variable_get('ipq_session_last_update', 0);

    // Get the records to process.
    $result = db_query("SELECT id
        FROM {ipq_saved_sessions}
        WHERE section_id <> 0
        AND entitlement_id IS NULL
        AND id > :ipq_id_restriction
        LIMIT 100", array(':ipq_id_restriction' => $ipq_id_restriction));


    foreach ($result as $key => $ipq_session) {
        $operations[] = array('ipq_common_update_entitlement_id', array($ipq_session->id));
    }

    // Batch
    $batch = array(
        'operations' => $operations,
        'finished' => 'ipq_common_update_entilement_id_finish',
        'title' => t('Updating IPQ Sessions'),
        'init_message' => t('Inicializing Update Process.'),
        'progress_message' => t("Processed @current out of @total ipq sessions."),
        'error_message' => t('Error'),
        'file' => drupal_get_path('module', 'ipq_common') . '/includes/ipq_common.batch.inc',
    );

    batch_set($batch);
}

/**
 * Update all existing IPQ session records in order to they are associated
 * with a user entitlement ID.
*/
function ipq_common_update_entitlement_id($ipq_session_id, &$context){
    $context['message'] = "Updating Session : " . $ipq_session_id;
    variable_set('ipq_session_last_update', $ipq_session_id);
    $entitlement_id = NULL;

    if(!isset($context['results']['current_record'])){
      $context['results']['current_record'] = 1;
    }

    try {
        // Get the records to process during this pass.
        $query = db_select('ipq_saved_sessions', 'ipqs');
        $query->fields('ipqs', array('id', 'uid', 'section_id', 'created_on'));
        $query->condition('ipqs.id', $ipq_session_id);
        $result = $query->execute();
        $context['results']['current_record']++;


        foreach ($result as $key => $ipq_session) {
            // Get the user entitlement_id
            if(!isset($context['results']['user_entitlements']) || (isset($context['results']['current_user']) && $context['results']['current_user'] != $ipq_session->uid)) {
                $context['results']['user_entitlements'] = array();
                $context['results']['current_user'] = $ipq_session->uid;

                $entitlements = db_query("SELECT nid, field_course_section_tid as section, created, field_expiration_date_value as expiration
                    FROM {node}
                    INNER JOIN {field_data_field_course_section} fcs ON fcs.entity_id = nid
                    INNER JOIN {field_data_field_expiration_date} fed ON fed.entity_id = nid
                    INNER JOIN {field_data_field_course_type_ref} fct ON fct.entity_id = nid
                    WHERE type = 'user_entitlement_product'
                    AND node.uid = :uid AND fct.field_course_type_ref_tid = :course_type_online",
                    array(':uid' => $ipq_session->uid, 'course_type_online' => COURSE_TYPE_ONLINE_COURSE));
                $context['results']['user_entitlements'] = $entitlements->fetchAll(PDO::FETCH_BOTH);
            }

            $entitlement_id = ipq_common_check_entitlement_id($context['results']['user_entitlements'], $ipq_session);

            // In case that the entitlement was not found
            // we put the entitlement id as 0 in order to avoid
            // that select get the same saved_session again.
            if(is_null($entitlement_id)) {
                $entitlement_id = 0;
            }

            // Update saved session
            db_query("UPDATE {ipq_saved_sessions} SET entitlement_id = :entitlement_id WHERE id = :session_id",
            array(':entitlement_id'=> $entitlement_id, ':session_id' => $ipq_session->id));

            // Update Session Data
            db_query("UPDATE {ipq_saved_session_data} SET entitlement_id = :entitlement_id WHERE ipq_saved_sessions_id = (:ipq_session)",
            array(':entitlement_id'=> $entitlement_id, ':ipq_session' => $ipq_session->id));


            // Update Session Overview
            db_query("UPDATE {ipq_session_overview} SET entitlement_id = :entitlement_id WHERE ipq_saved_sessions_id = (:ipq_session)",
            array(':entitlement_id'=> $entitlement_id, ':ipq_session' => $ipq_session->id));

        }

        // If current record is equal to 100, create and start
        // a new batch to continue processing the ipq sessions
        if ($context['results']['current_record'] == 100) {
            $context['results']['current_record'] = 1;
            $batch = &batch_get();
            $batch_next_set = $batch['current_set'] + 1;
            $batch_set = &$batch['sets'][$batch_next_set];

            // Get the records to process.
            $result = db_query("SELECT id
            FROM {ipq_saved_sessions}
            WHERE section_id <> 0
            AND entitlement_id IS NULL
            AND id > :last_ipq_session
            LIMIT 100", array(':last_ipq_session' => $ipq_session_id));


            foreach ($result as $key => $ipq_session) {
                $batch_set['operations'][] = array('ipq_common_update_entitlement_id', array($ipq_session->id));
            }

            $batch_set['progress_message'] = t('Processed @current out of @total ipq sessions.(Batch #' . $batch_next_set .')');
            $batch_set['file'] = drupal_get_path('module', 'ipq_common') . '/includes/ipq_common.batch.inc';
            $batch_set['total'] = count($batch_set['operations']);
            $batch_set['count'] = $batch_set['total'];

            _batch_populate_queue($batch, $batch_next_set);

            $context['message'] = "Getting other sessions";
        }
    }
    catch(Exception $exc) {
        watchdog('Error IPQ Update Process', 'ipq_session_id:' . $ipq_session->uid . " " . $exc->getMessage());
    }

}

/**
 * Check the user entitlements and check if the ipq session is related.
 * @param array $entitlements array with the user entitlements
 * @param object $ipq_session current ipq session
*/
function ipq_common_check_entitlement_id($entitlements, $ipq_session) {
    $entitlement_id = NULL;
    foreach ($entitlements as $entitlement) {
        if($ipq_session->section_id == $entitlement['section']
        && $ipq_session->created_on >= $entitlement['created']
        && $ipq_session->created_on <= $entitlement['expiration']) {
            return $entitlement['nid'];
        }
    }
    return $entitlement_id;
}

/**
 *
 * @param type $success false if one of the operations fail
 * @param type $results has a insert array of values to create on the table stats
 * @param type $operations
 *
 */
function ipq_common_update_entilement_id_finish($success, $results, $operations) {
    if ($success) {
        drupal_set_message("Update Process Complete Succesfully");
    }
    else {
        // An error occurred.
        // $operations contains the operations that remained unprocessed.
        $error_operation = reset($operations);
        drupal_set_message(
            t('An error occurred while processing @operation with arguments : @args', array(
                '@operation' => $error_operation[0],
                '@args' => print_r($error_operation[0], TRUE))
            ), 'error'
        );
    }
}
