<?php

function ipq_quiz_results() {

  // if user session doesn't exists we redirect the user to the ipq sessions creation page
  if (!isset($_SESSION['ipq_session']['session_id'])) {
    drupal_goto("ipq/quiz/setup");
  }

  // Before we can continue, we need to check if the user has set the exam version for this
  // section
  ipq_common_exam_version_for_session_check($_SESSION['ipq_session']);

  //set defaults
  $ipq_session_id = $_SESSION['ipq_session']['id'];

  // We need to know the exam version settled for this session
  $exam_version = isset($_SESSION['ipq_session']['session_config']['exam_version']) ? $_SESSION['ipq_session']['session_config']['exam_version'] : '2016';
  $ev_term = taxonomy_get_term_by_name($exam_version, 'exam_version');
  $exam_version_tid = key($ev_term);

  $uid = $_SESSION['ipq_session']['uid'];
  $question_ids = array();

  // Let's init an array with the question types for which we should show info
  $question_types = array();
  foreach ($_SESSION['ipq_session']['session_config']['question_types'] as $key => $value) {
    // Deleted questions will not have a key, so check that one exists to avoid including them here.
    if ($key && $value > 0) {
      // We only need to show the types for which the session have questions
      $testlet_name = ipq_comon_get_qt_group_human_name($key);
      if ($testlet_name == "Multiple Choice Questions") {
        $type = 'mcq';
      }
      else {
        if ($testlet_name == "Written Communication Questions" || $testlet_name == "Written Communication") {
          $type = 'wc';
        }
        else {
          $type = 'tbs';
        }
      }
      $question_types[$type] = array(
        'name'      => $testlet_name,
        'correct'   => 0,
        'incorrect' => 0,
        'skipped'   => 0,
        'total_qs'  => 0,
        'flagged'   => 0,
        'notes'     => 0,
      );
    }
  }

  // get the quiz session and overview data
  // note that even as we are joining ipq_session_overview with ipq_saved_session_data
  // and that those two tables hold similar data for the questions
  // we want to pull the data from ipq_saved_session_data when possible (because we are reviewing a
  // particular session)
  //adding AND so.ipq_saved_sessions_id = $ipq_session_id might look redundant but i am making sure the indexing works properly when left join, so even tought
  //we have uid this is not entirely redundant
  $results = db_query("SELECT qt.type, so.ipq_questions_id, sd.chapter_id, sd.is_flagged, so.is_noted, so.partial_score, so.created_on, so.updated_on, sd.status, sd.ipq_question_type_id
        FROM ipq_saved_session_data sd
        INNER JOIN ipq_question_types qt ON qt.id = sd.ipq_question_type_id 
        LEFT JOIN ipq_session_overview so ON sd.ipq_question_id = so.ipq_questions_id AND so.uid = sd.uid AND so.ipq_saved_sessions_id = $ipq_session_id
        WHERE sd.uid = $uid and sd.ipq_saved_sessions_id = $ipq_session_id")->fetchAll();

  //count up correct answers
  foreach ($results as $record) {
    //store the question_ids for querying the overview table
    $question_ids[] = $record->ipq_questions_id;
    //count up mcq correct, incorrect and unanswered
    switch ($record->type) {
      case 'ipq_mcq_question':
        // MCQ
        $type = 'mcq';
        break;
      case 'ipq_wc_question':
        // WC
        $type = 'wc';
        break;
      default:
        // Everything else is TBS
        $type = 'tbs';
    }

    $question_types[$type]['total_qs']++;
    //count question status
    switch ($record->status) {
      case 0 :
        $question_types[$type]['incorrect']++;
        break;
      case 1 :
        $question_types[$type]['correct']++;
        break;
      case 2 :
        $question_types[$type]['skipped']++;
        break;
    }
    //check for flagged
    if ($record->is_flagged == 1) {
      $question_types[$type]['flagged']++;
    }
    //check for noted
    if ($record->is_noted == 1) {
      $question_types[$type]['notes']++;
    }

  }
  //get section from chapter info
  $chapter_numbers = array();
  $overview = array();
  $chapter_nids = $_SESSION['ipq_session']['session_config']['chapters'];

  // We are going to need to number the chapters according to their position on the section chapter list
  // to add the chapter number on the UI
  $section = ipq_common_get_session_section($_SESSION['ipq_session']);
  $course_node = user_entitlements_get_course_node_by_section($section);
  // $section_chapter_list will contain the chapters delta
  $section_chapter_list = ipq_common_get_chapters_by_course_nid($course_node->nid, TRUE, $exam_version);

  $chapters = node_load_multiple($chapter_nids);
  foreach ($chapters as $chapter) {
    $q_info_per_chapter = array();
    foreach ($question_types as $q_type => $q_type_inf) {
      if ($q_type == 'mcq') {
        $q = "
            SELECT count(distinct ipq_questions_id) as total
            FROM ipq_question_pool q
            INNER JOIN node n ON q.ipq_questions_id = n.nid
            INNER JOIN ipq_question_types qt ON q.ipq_question_types_id = qt.id
            INNER JOIN field_data_field_section_per_exam_version section_ent
              ON section_ent.entity_type = 'node' AND section_ent.entity_id = n.nid
            INNER JOIN field_data_field_exam_version_single_val ev
              ON section_ent.field_section_per_exam_version_target_id = ev.entity_id AND ev.entity_type = 'node_ref_by_exam_version'
            WHERE
              chapter_id = $chapter->nid AND qt.type = 'ipq_mcq_question'
            ";
      }
      else {
        if ($q_type == 'wc') {
          $q = "
            SELECT count(distinct ipq_questions_id) as total
            FROM ipq_question_pool q
            INNER JOIN node n ON q.ipq_questions_id = n.nid
            INNER JOIN ipq_question_types qt ON q.ipq_question_types_id = qt.id
            INNER JOIN field_data_field_section_per_exam_version section_ent
              ON section_ent.entity_type = 'node' AND section_ent.entity_id = n.nid
            INNER JOIN field_data_field_exam_version_single_val ev
              ON section_ent.field_section_per_exam_version_target_id = ev.entity_id AND ev.entity_type = 'node_ref_by_exam_version'

            WHERE
              chapter_id = $chapter->nid AND qt.type = 'ipq_wc_question'
            ";
        }
        else {
          $q = "
            SELECT count(distinct ipq_questions_id) as total
            FROM ipq_question_pool q
            INNER JOIN node n ON q.ipq_questions_id = n.nid
            INNER JOIN ipq_question_types qt ON q.ipq_question_types_id = qt.id
            INNER JOIN field_data_field_section_per_exam_version section_ent
              ON section_ent.entity_type = 'node' AND section_ent.entity_id = n.nid
            INNER JOIN field_data_field_exam_version_single_val ev
              ON section_ent.field_section_per_exam_version_target_id = ev.entity_id AND ev.entity_type = 'node_ref_by_exam_version'
            WHERE
              chapter_id = $chapter->nid AND qt.type <> 'ipq_mcq_question' AND qt.type <> 'ipq_wc_question'
            ";
        }
      }
      $r = db_query($q, array(':ev_tid' => $exam_version_tid))->fetchAssoc();
      $q_info_per_chapter[$q_type] = array(
        'total'      => $r['total'],
        'flagged'    => 0,
        'noted'      => 0,
        'correct'    => 0,
        'incorrect'  => 0,
        'unanswered' => 0,
      );
    }
    //get the chapter numbers
    $chapter_number = $section_chapter_list[$chapter->nid]['delta'];
    $chapter_numbers[] = $chapter_number;
    //populate overview data
    // note: we use the $chapter_number here to be able to sort them easily later
    $overview[$chapter_number] = array(
      'chapter_number'  => $chapter_number,
      'chapter_title'   => $chapter->title,
      'nid'             => $chapter->nid,
      'question_counts' => $q_info_per_chapter,
      'total_correct'   => 0,
      'total_incorrect' => 0,
    );
  }
  // we want to show the covered chapters in order
  ksort($overview);
  sort($chapter_numbers);
  // we don't need $overview to be indexed by chapter number, and also, on the logic below
  // gets simpler if we just have indexed by position, so we are removing the keys
  $overview = array_values($overview);

  //format section numbers string
  $section_numbers = implode(", ", $chapter_numbers);
  //under certain conditions $chapter_numbers causes implode to return a comma in the first position. Get rid of it!
  $section_numbers = ltrim($section_numbers, ',');

  //populate overview data
  $i = 0;
  foreach ($overview as $over) {
    //go back through the questions
    foreach ($results as $record) {
      //look for chapter
      if ($over['nid'] == $record->chapter_id) {
        switch ($record->type) {
          case 'ipq_mcq_question':
            // MCQ
            $type = 'mcq';
            break;
          case 'ipq_wc_question':
            // WC
            $type = 'wc';
            break;
          default:
            // Everything else is TBS
            $type = 'tbs';
        }

        //count question status
        switch ($record->status) {
          case 0 :
            $overview[$i]['question_counts'][$type]['incorrect']++;
            $overview[$i]['total_incorrect']++;
            break;
          case 1 :
            $overview[$i]['question_counts'][$type]['correct']++;
            $overview[$i]['total_correct']++;
            break;
          case 2 :
            $overview[$i]['question_counts'][$type]['unanswered']++;
            break;
        }
        //check for flagged
        if ($record->is_flagged == 1) {
          $overview[$i]['question_counts'][$type]['flagged']++;
        }
        //check for noted
        if ($record->is_noted == 1) {
          $overview[$i]['question_counts'][$type]['noted']++;
        }
      }
    }
    $i++;
  }

  //get session data to create the edit form
  $session = db_query("SELECT * FROM ipq_saved_sessions where id = $ipq_session_id")->fetchAssoc();

  $data = array(
    'session'         => $_SESSION['ipq_session'],
    'section'         => $section,
    'section_numbers' => $section_numbers,
    'question_info'   => $question_types,
    'summary'         => _ipq_common_quiz_result_question_summary(),
  );
  //build the form
  $data['form'][$session['session_id']] = drupal_get_form('ipq_quiz_results_rename_session_form', $session);
  $data['overview'] = $overview;

  drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/ipq-history.js');
  return $page_content = theme('ipq_quiz_results', array('data' => $data));
}

/**
 * This form is used to allow users to rename their sessions on the ipq history page
 * @param $form
 * @param $form_state
 * @param $session
 * @return mixed
 */
function ipq_quiz_results_rename_session_form($form, &$form_state, $session) {
  $form_state['storage']['ipq_session']['id'] = $session['id'];
  if (isset($form_state['values']['new_name'])) {
    $name = $form_state['values']['new_name'];
  } else {
    $name = $session['name'];
  }

  $form['name_container_' . $session['id']] = array('#type' => 'container', '#prefix' => '<div id="name-container-' . $session['id'] . '">', '#suffix' => '</div>', );
  $form['name_container_' . $session['id']]['current_name'] = array('#prefix' => '<div id="current-name-container-' . $session['id'] . '" class="quiz-name-review">', '#suffix' => '</div>', '#markup' => $name . ' <span><a href="" class="rename-session" id="rename_current_session_' . $session['id'] . '"><i class="fa fa-pencil"></i> Rename</a></span>', );
  $form['name_container_' . $session['id']]['new_name_container'] = array('#type' => 'container', '#prefix' => '<div id="new-name-container-' . $session['id'] . '" style="display:none">', '#suffix' => '</div>', );

  $form['name_container_' . $session['id']]['new_name_container']['new_name'] = array('#type' => 'textfield', '#default_value' => $name, '#maxlength' => '100', ); 
  $form['name_container_' . $session['id']]['new_name_container']['save_new_name'] = array('#type' => 'submit', '#value' => t('Save'), '#submit' => array('ipq_quiz_result_change_session_name_submit'), '#ajax' => array('callback' => 'ipq_quiz_result_ajax_change_session_name', 'wrapper' => 'name-container-' . $session['id'] . '', 'method' => 'replace', 'effect' => 'fade', ), );
  return $form;
}

/**
 *  ajax submit function for pq history rename session feature
 *
 * @param $form
 * @param $form_state
 */
function ipq_quiz_result_change_session_name_submit($form, &$form_state) {
  $session_id = $form_state['storage']['ipq_session']['id'];

  db_update('ipq_saved_sessions') -> fields(array('name' => $form_state['values']['new_name'])) -> condition('id', $session_id, '=') -> execute();
  $form_state['storage']['ipq_session']['name'] = $form_state['values']['new_name'];
  $form_state['rebuild'] = TRUE;
}

/**
 *  ajax callback function for 'quiz_details' stage rename session
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function ipq_quiz_result_ajax_change_session_name($form, &$form_state) {
  $session_id = $form_state['storage']['ipq_session']['id'];
  return $form['name_container_' . $session_id];
}

/**
 * Function to build question summary data
 */
function _ipq_common_quiz_result_question_summary() {
  $summary = '';
  $i = 1;
  //check for quiz type session
  if ($_SESSION['ipq_session']['session_type'] == 'quiz') {
    $container_type = 'Quizlet';
  }
  else {
    $container_type = 'Testlet';
  }
  foreach ($_SESSION['ipq_session']['session_config']['question_types'] as $key => $value) {
    // Make sure there are questions in the quiz.
    if ($value > 0) {
      // If the quiz is a combined quizlet, list the number of questions and question types.
      if ($_SESSION['ipq_session']['session_config']['combined_quizlet']) {
        $summary = $summary . '<div class="quiz-section">' . $value . ' ' . ipq_comon_get_qt_group_human_name($key) . '</div>';
      }
      // Otherwise list the questions in each quizlet.
      else {
        $testlet_name = ipq_comon_get_qt_group_human_name($key);
        $summary = $summary . '<div class="quiz-section">' . $container_type . ' ' . $i . ': Contained ' . $value . ' ' . $testlet_name . '</div>';
        $i++;
      }
    }
  }
  return $summary;
}
