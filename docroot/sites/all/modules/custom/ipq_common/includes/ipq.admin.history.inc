<?php

/**
 * Clear student history form
 *
 * @param $form
 * @param $form_state
 */
function ipq_common_clear_history_form($form, &$form_state) {
        
  $form = array();

  $form['clear_history'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 25,
    '#description' => t("Enter the username of the student for whom to delete IPQ history. Please be advised that ALL IPQ HISTORY WILL BE DELETED for this user."),
    '#required' => TRUE,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#attributes' => array('onclick' => 'if(!confirm("Seriously, no turning back. Delete IPQ history for this user?")){return false;}'),
    '#submit' => array('ipq_common_clear_history_submit'),
  );

  return $form;
  
}

/**
 * Submit the form and delete the IPQ history
 *
 * @param $form
 * @param $form_state
 */
function ipq_common_clear_history_submit(&$form, &$form_state){
        
    // Get the uid
    $username = $form_state['values']['clear_history'];
    $user = user_load_by_name($username);
    
    // No user no submit
    if (empty($user)) {
      drupal_set_message( 'Sorry, user '.$username. ' does not exist.', 'warning');
      return;
    }
    
    // Delete all IPQ session datat
    ipq_common_clear_ipq_history_for_user($user);
    
    // Tell the user all is cool
    drupal_set_message( 'All IPQ session history has been cleared for '.$username);    
    
    
}

/**
 * Function to delete all the IPQ session data associated with a user
 *
 * @param $user user object
 * 
 */
function ipq_common_clear_ipq_history_for_user($user) {
    // User id
    $user_id = $user->uid;
    
    // Delete session
    db_query("delete from {ipq_saved_sessions} WHERE uid = :user_id ", array(":user_id" => $user_id));
      
    // Delete saved data
    db_query("delete from {ipq_saved_session_data} WHERE uid = :user_id ", array(":user_id" => $user_id));
    
    // Delete session overview data data
    db_query("delete from {ipq_session_overview} WHERE uid = :user_id ", array(":user_id" => $user_id));

    // Clear SmartPath cache
    $c = new RCPARSmartPathCache($user_id, FALSE);
    $c->invalidateCacheRecord();
}
