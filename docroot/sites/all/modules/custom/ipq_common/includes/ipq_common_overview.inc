<?php

/**
 * Page callback for 'ajax/dashboard/testcenter/overview/%/%'
 * Renders a user's IPQ overview data - used in the context of an instructor examining another user's data via the
 * monitoring center.
 * Note that this callback is defined in mc_profesors.module
 *
 * @param int $uid
 * - The user whose IPQ data we're viewing.
 * @return string
 * - Rendered HTML
 */
function ipq_common_overview_ajax($uid){
    $args = func_get_args();
    // on the ajax version, the section comes before the group and it's optional
    if (func_num_args() == 2){ // no group id
        $group_id = null;
        $section = $args[1];
    } else {
        $group_id = $args[1];
        $section = $args[2];
    }

    if (!ipq_common_perms($uid, $section)){
        return MENU_ACCESS_DENIED;
    }
    // we want to load this only if the user have access to this section
    drupal_add_js(drupal_get_path('module','ipq').'/js/ipq.js');
    return ipq_common_overview($uid, $section);
}

/**
 * Renders HTML content for a user's IPQ overview data.
 *
 * Two contexts:
 * 1. Called by ipq_common_user_sections_overview() for '/ipq/overview' when the user views their own data.
 * 2. Called by ipq_common_overview_ajax() when an instructor views another user's data via the monitoring center.
 *
 * @param $uid
 * - The user whose IPQ overview data we're viewing.
 * @param string $section
 * - 'AUD', 'FAR', 'BEC', or 'REG'
 *
 * The following 3 parameters being present indicate the user is trying to review a particular set of questions
 * @param int $chapter_id
 * - Node ID of a chapter
 * @param string $type
 * - 'mcq' or 'tbs'
 * @param string $question_filter
 * e.g. 'not_answered', 'incorrect', etc. @see ipq_common_overview_build_temp_session_for_preview()
 *
 * @return string
 * - Rendered HTML
 */
function ipq_common_overview($uid, $section = null, $chapter_id = null, $type = null, $question_filter = null) {
  $arg = arg();
  $has_ajax = in_array('ajax', $arg);
  if (!ipq_common_perms($uid, $section)) {
    return MENU_ACCESS_DENIED;
  }
  // if we are receiving all the parameters, then the user is trying to review a particular set of questions
  // to allow users to do that, we are going to create a temporary session
  // to simualte a real one with the selected questions
  if ($chapter_id && $type && $question_filter){
      $query = db_select('course_stats', 'cs');
      $query->fields('cs',array('weight'))
          ->condition('nid', $chapter_id);
      $delta_current_course = $query->execute()->fetchColumn();
      if($delta_current_course != 1 && rcpar_dashboard_user_is_freetrial()){
        return MENU_ACCESS_DENIED;
      }
    // possible "types" are 'mcq' or 'tbs'
    // but we need to send the actual types to the
    // ipq_common_build_session

    if ($type == 'mcq'){
      $types = array('ipq_mcq_question');
    } else {
      $types = ipq_commom_get_tbs_question_types();
    }

    $temporary_session = ipq_common_overview_build_temp_session_for_preview($section, $chapter_id, $types, $question_filter);
    $_SESSION['ipq_preview_temp_session'] = $temporary_session;


    drupal_goto("ipq/review");
  }

  // Regular case, we need to construct the content of the overview page

  // We need to know the exam version set for the user for this particular section
  global $user;
  // if we are checking the info about another user
  if($user->uid != $uid){
    $_user = user_load($uid);
    $_user->user_entitlements = user_entitlement_set_entitlement_obj(false, $uid);
  }
  
  $exam_version = ipq_common_get_exam_version($_user, $section);
  $ev_term = taxonomy_get_term_by_name($exam_version, 'exam_version');
  $exam_version_tid = key($ev_term);
  // if the user has restriction 
  // Get the all allow chapters and topics
  $has_restrictions = isset($_user->user_entitlements['products'][$section]['restrictions'][$section]['courses']);
  
  //get the session_id from the form. Default is 0
  $session_id = (isset($_SESSION['ipq_session']['overview_session_id'])) ? $_SESSION['ipq_session']['overview_session_id'] : 0;

  //setup the session form
  //if has ajax means we are comming from Monitoring Center, and we don't need the form
  $form = ($has_ajax) ? '' : drupal_get_form('ipq_common_saved_session_overview_form', $session_id);
  
  // we need to exclude the chapters that we know that don't have any questions on them
  $excluded_chapts = ipq_commom_ignore_chapters();
  $query = db_select('course_stats', 'cs');
  $query->join('node', 'n', 'n.nid = cs.nid'); // we need the node title to know if we need to exclude it
  $query->fields('cs');
  $query->fields('n', array('title'))
      ->condition('cs.type', 'CHAPTER')
      ->condition('course_type', 'Online Course')
      ->condition('n.nid', $excluded_chapts, 'not in')
      ->condition('section', $section)
      ->condition('year', $exam_version_tid)
      ->orderBy('weight', 'ASC');
  // if user has entitlements need to bee added to the query
  // And get the correct values
  if($has_restrictions){
    $allowed_chapters = ipq_common_get_user_allowed_chapters_ids($_user, $section, $exam_version);
    $allowed_topics = ipq_common_get_user_allowed_topics_ids($_user, $section, $exam_version);
    $query->condition('n.nid', $allowed_chapters, 'IN');
  }
  
  $course_info = $query->execute()->fetchAll();
  $chapter_nids = array();
  foreach ($course_info as $info) {
    if ($info->weight != 0) {      
      $chapter_nids[] = $info->nid;
    }
  }

  // Load some chapter data
  $chapter_ents = db_query("SELECT nid, title FROM node WHERE nid IN(:nids)", array(':nids' => $chapter_nids))->fetchAll();

  // List of available question types to display
  $available_categories = array(
    'mcq' => 'Multiple Choice Questions',
    'tbs' => 'Task Based Simulations',
    'wc'  => 'Written Communication'
  );

  // The categories list depends on the section and the exam version
  $exam_version = ipq_common_get_exam_version($_user, $section);
  if ($exam_version == '2016') {
    if ($section == 'BEC') {
      // BEC for 2016 shows only MCQ and WC
      unset($available_categories['tbs']);
    }
    else {
      // The rest of 2016 sections shows MCQ and TBS
      unset($available_categories['wc']);
    }
  }
  else {
    // for 2017 BEC is the only section that shows all tree categories
    // the rest, only shows MCQ and TBS, just like in 2016
    if ($section != 'BEC') {
      unset($available_categories['wc']);
    }
  }

  // For each of the available question types, we calculate the total questions count
  foreach ($available_categories as $type => $name) {
    $arguments = array(':ev_tid' => $exam_version_tid);
    $chapter_list = implode(',', $chapter_nids);
    $q = "SELECT
            count(distinct ipq_questions_id) as total
          FROM ipq_question_pool q
          INNER JOIN node n ON q.ipq_questions_id = n.nid
          INNER JOIN ipq_question_types qt ON q.ipq_question_types_id = qt.id
          WHERE q.exam_version_id = :ev_tid AND chapter_id IN ($chapter_list) ";
    if ($has_restrictions) {
      //get chapter ids to query just this section
      $q .= " AND q.chapter_id IN (:allowed_chapters) AND q.topic_id IN (:allowed_topics)";
      $arguments[':allowed_chapters'] = $allowed_chapters;
      $arguments[':allowed_topics'] = $allowed_topics;
    }
    if ($type == 'mcq') {
      $q .= " AND qt.type = 'ipq_mcq_question' ";
    }
    else if($type == 'wc'){
      $q .= " AND qt.type = 'ipq_wc_question' ";
    }
    else {
      $q .= " AND qt.type <> 'ipq_mcq_question' AND qt.type <> 'ipq_wc_question' ";
    }
    
    $total = db_query($q, $arguments)->fetchAssoc();
    $question_types[$type] = array(
      'name'      => $name,
      'correct'   => 0,
      'incorrect' => 0,
      'skipped'   => 0,
      'total_qs'  => $total['total'],
      'flagged'   => 0,
      'noted'     => 0,
    );
  }

  //get the current section_id
  $term = taxonomy_get_term_by_name($section, 'course_sections');
  $section_tid = array_keys($term);
  $section_tid = $section_tid[0];

  // Get active user entitlement id for this section
  $entitlement_id = user_entitlements_get_active_course_entitlement_for_section($_user, $section_tid);
  if($entitlement_id) {
    $where_condition =  " AND so.entitlement_id = " . $entitlement_id;
  } else {
    $where_condition = "";
  }

  // Now we get information about all the related question and calculate some numbers based on that info
  // if session_id is 0 we get all the results
  if ($session_id == 0) {
    //Get all overview data
    $query = "
    SELECT qt.type, qp.chapter_id, qp.ipq_question_types_id,
    so.is_flagged, so.is_noted, so.status
      FROM ipq_session_overview so
      INNER JOIN ipq_question_pool qp ON qp.ipq_questions_id = so.ipq_questions_id
      INNER JOIN ipq_question_types qt ON qp.ipq_question_types_id = qt.id
      WHERE so.uid = $uid
        AND qp.exam_version_id = $exam_version_tid";
    if($has_restrictions){
        $query .= " AND qp.chapter_id IN (".$chapter_list.")";
    }
    $query .= $where_condition ." GROUP BY so.ipq_questions_id";
    $results = db_query($query)->fetchAll();
  } else {
    //only return the quiz results
    $query = "SELECT qt.type, sd.ipq_question_id as ipq_questions_id, sd.chapter_id, sd.is_flagged, so.is_noted, sd.status, sd.ipq_question_type_id as ipq_question_types_id
		FROM ipq_saved_session_data sd
    INNER JOIN ipq_question_pool qp ON qp.ipq_questions_id = sd.ipq_question_id    
		INNER JOIN ipq_question_types qt ON sd.ipq_question_type_id = qt.id
		JOIN ipq_session_overview so ON so.ipq_questions_id = sd.ipq_question_id AND so.ipq_saved_sessions_id = sd.ipq_saved_sessions_id
		WHERE sd.ipq_saved_sessions_id = $session_id ";
    if($has_restrictions){
      $query .= " AND qp.chapter_id IN ($chapter_list)"; 
    }
		$query .= " GROUP BY sd.ipq_question_id, sd.chapter_id, sd.is_flagged, sd.status, qt.id";
    $results = db_query($query)->fetchAll();
  }

  // Now that we have the information for all the required questions, we iterate through the question info array and
  // calculate some aggregated info
  foreach ($results as $record) {
    // We need to separate the information by question "type" (not the real type, more like the category
    // mcq, wc or tbs)
    switch ($record->type) {
      case 'ipq_mcq_question':
        // MCQ
       $type = 'mcq';
        break;
      case 'ipq_wc_question':
        // WC
        $type = 'wc';
        break;
      default:
        // Everything else is TBS
        $type = 'tbs';
    }

    // There might be some questions types that we are not interested on listing
    // if this question belongs to one of those categories, we just skip it
    if(!in_array($type, array_keys($available_categories))){
      continue;
    }

    //count question status
    switch ($record->status) {
      case 0 :
        $question_types[$type]['incorrect']++;
        break;
      case 1 :
        $question_types[$type]['correct']++;
        break;
      case 2 :
        $question_types[$type]['skipped']++;
        break;
    }
    //check for flagged
    if ($record->is_flagged == 1) {
      $question_types[$type]['flagged']++;
    }
    //check for noted
    if ($record->is_noted == 1) {
      $question_types[$type]['noted']++;
    }
  }

  // Now we the get data by chapter
  $chapters = array();
  $order = 1;
  foreach ($chapter_ents as $chapter_ent) {
    $q_info_per_chapter = array(
      'section' => $section,
      'chapter_name' => $chapter_ent->title,
      'order' => $order,
      'chapter_nid' => $chapter_ent->nid,
    );
    foreach ($question_types as $q_type => $q_type_inf) {
      //get total questions for this chapter
      $q = "
            SELECT
              count(distinct ipq_questions_id) as total
            FROM ipq_question_pool q
            INNER JOIN ipq_question_types qt ON q.ipq_question_types_id = qt.id
            WHERE
              chapter_id = " .$chapter_ent->nid;
      if($has_restrictions){
        $q .= " AND q.chapter_id IN (:allowed_chapters) AND q.topic_id IN (:allowed_topics)";
      }
      $q .= " AND q.exam_version_id = :ev_tid";
      if ($q_type == 'mcq') {
        $q .= " AND qt.type = 'ipq_mcq_question' ";
      }
      else if($q_type == 'wc'){
        $q .= " AND qt.type = 'ipq_wc_question' ";
      }
      else {
        $q .= " AND qt.type <> 'ipq_mcq_question' AND qt.type <> 'ipq_wc_question' ";
      }
      $args [':ev_tid'] = $exam_version_tid;
      if($has_restrictions){
        $args [':allowed_chapters'] = $allowed_chapters;
        $args [':allowed_topics'] = $allowed_topics;
      }
      $r = db_query($q, $args)->fetchAssoc();
      $q_info_per_chapter['question_counts'][$q_type] = array(
        'total_qs'      => $r['total'],
        'flagged'    => 0,
        'noted'      => 0,
        'correct'    => 0,
        'incorrect'  => 0,
        'skipped'    => 0,
      );
    }

    // Again, we iterate through the questions to calculate the numbers
    // Note that we are just looking on the questions that belongs to the current chapter
    // TODO: note that we are going through the result array several times, and this can be done in just one pass
    foreach ($results as $record) {
      if ($record->chapter_id == $chapter_ent->nid) {
        switch ($record->type) {
          case 'ipq_mcq_question':
            // MCQ
            $type = 'mcq';
            break;
          case 'ipq_wc_question':
            // WC
            $type = 'wc';
            break;
          default:
            // Everything else is TBS
            $type = 'tbs';
        }
        // There might be some questions types that we are not interested on listing
        // if this question belongs to one of those categories, we just skip it
        if(!in_array($type, array_keys($available_categories))){
          continue;
        }

        //count question status
        switch ($record->status) {
          case 0 :
            $q_info_per_chapter['question_counts'][$type]['incorrect']++;
            $q_info_per_chapter['total_incorrect']++;
            break;
          case 1 :
            $q_info_per_chapter['question_counts'][$type]['correct']++;
            $q_info_per_chapter['total_correct']++;
            break;
          case 2 :
            $q_info_per_chapter['question_counts'][$type]['skipped']++;
            break;
        }
        //check for flagged
        if ($record->is_flagged == 1) {
          $q_info_per_chapter['question_counts'][$type]['flagged']++;
        }
        //check for noted
        if ($record->is_noted == 1) {
          $q_info_per_chapter['question_counts'][$type]['noted']++;
        }
      }
    }

    // Let's calculate the chapter aggregated information
    $q_info_per_chapter['total_qs'] = 0;
    $q_info_per_chapter['flagged'] = 0;
    $q_info_per_chapter['noted']   = 0;
    $q_info_per_chapter['correct'] = 0;
    $q_info_per_chapter['incorrect']  = 0;
    $q_info_per_chapter['unanswered'] = 0;
    foreach ($q_info_per_chapter['question_counts'] as $type => $type_info) {
      $q_info_per_chapter['total_qs']   += $type_info['total_qs'];
      $q_info_per_chapter['flagged']    += $type_info['flagged'];
      $q_info_per_chapter['noted']      += $type_info['noted'];
      $q_info_per_chapter['correct']    += $type_info['correct'];
      $q_info_per_chapter['incorrect']  += $type_info['incorrect'];
      $q_info_per_chapter['unanswered'] += $type_info['unanswered'];
    }
    $q_info_per_chapter['answered'] = $q_info_per_chapter['correct'] + $q_info_per_chapter['incorrect'];
    if ($q_info_per_chapter['answered']) {
      $q_info_per_chapter['correct_percent'] = round($q_info_per_chapter['correct'] / $q_info_per_chapter['answered'] * 100);
      $q_info_per_chapter['incorrect_percent'] =round($q_info_per_chapter['incorrect'] / $q_info_per_chapter['answered'] * 100);
    }
    else {
      $q_info_per_chapter['correct_percent'] = 0;
      $q_info_per_chapter['incorrect_percent'] = 0;
    }

    $chapters[] = $q_info_per_chapter;
    $order++;
  }
  
  // Initialize $section_overview['general'] with question types counts and info
  $section_overview['general']['per_type'] = $question_types;

  // Calculate general aggregated information
  $section_overview['general']['totals']['total_qs']   = 0;
  $section_overview['general']['totals']['flagged']    = 0;
  $section_overview['general']['totals']['noted']      = 0;
  $section_overview['general']['totals']['correct']    = 0;
  $section_overview['general']['totals']['incorrect']  = 0;
  $section_overview['general']['totals']['unanswered'] = 0;
  foreach ($question_types as $type => $type_info) {
    $section_overview['general']['totals']['total_qs']   += $type_info['total_qs'];
    $section_overview['general']['totals']['flagged']    += $type_info['flagged'];
    $section_overview['general']['totals']['noted']      += $type_info['noted'];
    $section_overview['general']['totals']['correct']    += $type_info['correct'];
    $section_overview['general']['totals']['incorrect']  += $type_info['incorrect'];
    $section_overview['general']['totals']['unanswered'] += $type_info['unanswered'];
  }
  $section_overview['general']['totals']['answered'] = $section_overview['general']['totals']['correct'] + $section_overview['general']['totals']['incorrect'];
  $answered = $section_overview['general']['totals']['answered'];
  if ($answered) {
    $section_overview['general']['totals']['correct_percent'] = round($section_overview['general']['totals']['correct'] / $answered * 100);
    $section_overview['general']['totals']['incorrect_percent'] =round($section_overview['general']['totals']['incorrect'] / $answered * 100);
  }
  else {
    $section_overview['general']['totals']['correct_percent'] = 0;
    $section_overview['general']['totals']['incorrect_percent'] = 0;
  }

  // Get overview data

  $section_overview['chapters'] = $chapters;
  $data = array(
    'overview' => array(
      'account_uid' => $uid,
      'form' => $form,
      'section' => $section,
      'sections' => rcpar_dashboard_entitlements_options(),
      'section_overview' => $section_overview
    )
  );

  //reset the session variable to default
  $_SESSION['ipq_session']['overview_session_id'] = 0;
	drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/ipq-overview.js');
  return theme('ipq_common_overview', $data);
}

/**
 * Page callback for ipq/overview
 * Renders a user's own IPQ overview data.
 * @todo - There does not appear to be an instance where any of the parameters of this function are passed in.
 *
 * @return string
 * - Rendered HTML
 */
function ipq_common_user_sections_overview($section = null, $chapter_id = null, $type = null, $question_filter = null) {
  global $user;
  $current_section = ipq_common_get_default_section();
  ipq_common_set_section($current_section);

  // if exam versioning is toggled on
  if (exam_version_is_versioning_on()) {
    // Before we can continue, we need to check if the user has set the exam version for this section
    $exam_version = user_entitlements_get_exam_version_for_course($current_section);
    if (!$exam_version) {
      if (!isset($_REQUEST['section'])) {
        // On this case user is not trying to access to a specific section
        // so if we don't have the exam version for the default, we might want to redirect it to
        // a section for which he does have the exam version set
        foreach (array('AUD', 'BEC', 'REG', 'FAR') as $new_sec) {
          if (user_entitlements_get_exam_version_for_course($new_sec)) {
            drupal_goto("ipq/overview", array('query' => array('section' => $new_sec)));
          }
        }
        // If the user has not set any of their entitlements, we just redirect it to dashboard on the
        // rcpar_dashboard_force_ev_before_continue
      }
      rcpar_dashboard_force_ev_before_continue($current_section);
      return '';
    }
  }

  return ipq_common_overview($user->uid, $current_section, $chapter_id, $type, $question_filter);
}

/**
 * Creates the "Filter by Score History" form, which is just a single dropdown that appears on
 * the /ipq/overview page. It lists the date, type, and name of every IPQ session that the user
 * has ever created.
 */
function ipq_common_saved_session_overview_form($form, &$form_state, $default = 0) {

  global $user;
  $uid = $user->uid;

  //get the current section_id
  $current_section = ipq_common_get_default_section();
  $term = taxonomy_get_term_by_name($current_section, 'course_sections');

  if (!$term) {
    drupal_not_found();
  }

  //Get section tid
  $tid = array_keys($term);
  $tid = $tid[0];

  //Get active user entitlement for the current section
  $entitlement_id = user_entitlements_get_active_course_entitlement_for_section($user, $tid);
  if($entitlement_id) {
    $where_condition =  "AND entitlement_id = " . $entitlement_id;
  } else {
    $where_condition = "";
  }

  //get the saved sessions
  $query = "SELECT id, updated_on, session_type, name FROM ipq_saved_sessions 
  where uid = $uid and section_id = $tid 
  $where_condition
  ORDER by updated_on DESC";
  $sessions = db_query($query)->fetchAll();

  //set default on dropdown menu to be "All Sessions"
  $saved_sessions = array('0' => 'All Sessions', );
  //populate the dropdown menu with id = name
  foreach ($sessions as $session) {
    $saved_sessions[$session->id] = date('m/d/Y h:i a', $session->updated_on) . " - (" . ucfirst($session->session_type) . ") " .$session->name;
  }

  //create the form
  $form['saved_sessions'] = array('#type' => 'value', '#value' => $saved_sessions, );

  $form['sessions'] = array('#title' => t('Filter by Score History:'), '#type' => 'select',
  //'#description' => "Select the session name.",
  '#options' => $form['saved_sessions']['#value'], '#default_value' => $default);

  $form['submit'] = array('#type' => 'submit', '#value' => t('Go'), '#submit' => array('ipq_common_saved_session_overview_form_submit'), );

  return $form;
}

/**
 * Saved sessions form submit handler
 */
function ipq_common_saved_session_overview_form_submit($form, &$form_state) {
  //get the returned session_id from the form

  //add the session_id to the session
  $_SESSION['ipq_session']['overview_session_id'] = $form_state['values']['sessions'];

}

/**
 * This function is used to create a dummy ipq session to be used to temporary review a set of questions
 * based on the information stored in the session_overview table
 * @param $section
 *  AUD, BEC, etc
 * @param $types
 *  types of the questions that should be added to the temp session
 * @param $question_filter
 *  condition that the selected questions must have to be included in the temp session
 * @return array
 *  temporary session data (note that is just returned, not set to the actual $_SESSION var)
 */
function ipq_common_overview_build_temp_session_for_preview($section, $chapter_id, $types, $question_filter){

  global $user;
  $t = taxonomy_get_term_by_name($section, 'course_sections');
  $t = array_keys($t);
  $section_id = reset($t);

  switch($question_filter){
    case 'not_answered':
      $name = 'Unanswered questions';
      break;
    case 'incorrect':
      $name = 'Incorrect questions';
      break;
    case 'correct':
      $name = 'Correct questions';
      break;
    case 'answered':
      $name = 'Answered questions';
      break;
    case 'flagged':
      $name = 'Flagged questions';
      break;
    case 'noted':
      $name = 'Noted questions';
      break;
  }
  if (exam_version_is_versioning_on()) {
    $exam_version = user_entitlements_get_exam_version_for_course($section);
  } else {
    $exam_version = exam_version_get_default_version();
  }

  $session_data = array(
    'is_new'           => TRUE,
    'is_temporary'     => TRUE,
    'uid'              => $user->uid,
    'session_id'       => $user->sid,
    'name'             => $name,
    'session_type'     => 'quiz',
    'average_time'     => '0',
    'section_id'       => $section_id,
    'percent_complete' => '0',
    'percent_correct'  => '0',
    'created_on'       => REQUEST_TIME,
    'updated_on'       => REQUEST_TIME,
    'finished'         => 0,
  );
  $session_data['session_config'] = array(
    'only_no_seen'          => 0,
    'only_incorrect'        => 0,
    'only_skipped'          => 0,
    'question_types'        => $types,
    'chapters'              => array($chapter_id),
    'hide_timer'            => 0,
    'exam_version'          => $exam_version,
    'score_as_you_go'       => 0,
    'view_solutions'        => 0,
    'explain_answers'       => 0,
    'question_list'         => array(),
    'current_question_type' => null,
    'current_question_id'   => NULL,

    // note that this parameters are specific for this kind of temporary sessions
    'questions_info' => array(),
    'temp_session_question_filter' => $question_filter,

    // TODO: remove this after removing pagination by url on session overview
    'temp_session_question_type' => count($types) == 1 ? 'mcq' : 'tbs',
  );

  $ev_term = taxonomy_get_term_by_name($exam_version, 'exam_version');
  $exam_version_tid = key($ev_term);

  $query = db_select('ipq_session_overview', 'so');
  $query->join('ipq_question_pool', 'qp', 'so.ipq_questions_id = qp.ipq_questions_id');
  $query->join('ipq_question_types', 'qt', 'qp.ipq_question_types_id = qt.id');
  $query->fields('so', array('ipq_questions_id', 'ipq_saved_sessions_id'))
    ->condition('so.uid', $user->uid, '=')
    ->condition('qp.chapter_id', $chapter_id, '=')
    ->condition('qp.exam_version_id', $exam_version_tid, '=')
    ->groupBy('so.ipq_questions_id')
    ->addTag('build_temp_session_for_preview');
  // by rcpar_cp_query_build_temp_session_for_preview_alter() (and possibly others)
  // Get if exist a active entitlement.
  $entitlement_id = user_entitlements_get_active_course_entitlement_for_section($user, $section_id);
  if($entitlement_id) {
    // Add the condition on the current query.
    $query->condition( "so.entitlement_id = ",  $entitlement_id, '=');
  }
  switch($question_filter){
    case 'not_answered':
      // skipped questions only
      $session_data['session_config']['only_skipped'] = 1;
      $query->condition('so.status', 2, '=');
      break;
    case 'answered':
      // all but skipped
      $query->condition('so.status', 2, '<>');
      break;
    case 'correct':
      $query->condition('so.status', 1, '=');
      break;
    case 'incorrect':
      $session_data['session_config']['only_incorrect'] = 1;
      $query->condition('so.status', 0, '=');
      break;
    case 'flagged':
      $query->condition('so.is_flagged', 1, '=');
      break;
    case 'noted':
      $query->condition('so.is_noted', 1, '=');
      break;
  }

  $session_data['session_config']['question_list'] = array();
  foreach ($session_data['session_config']['question_types'] as $type) {
    $question_types = get_question_types_from_quizlet_name($type);
    $ql = array();
    foreach($question_types as $question_type){
      if (in_array($question_type, $types)){
        // now we need to apply the filter and see which of the questions of the current type
        $query_copy = clone $query;
        $query_copy->condition('qt.type', $question_type, '=');
        $results = $query_copy->execute()->fetchall();
        foreach ($results as $result) {
          $qid = $result->ipq_questions_id;
          $ql[] = $qid;
          if (!isset($session_data['session_config']['questions_info'][$type])){
            $session_data['session_config']['questions_info'][$type] = array();
          }
          $session_data['session_config']['questions_info'][$type][$qid] = db_select('ipq_saved_session_data', 'sd')
            ->fields('sd')
            ->condition('ipq_saved_sessions_id', $result->ipq_saved_sessions_id, '=')
            ->condition('ipq_question_id', $result->ipq_questions_id, '=')
            ->execute()->fetchAssoc();
        }
      }
    }
    if (count($ql) > 0){
      $session_data['session_config']['question_list'][$type] = $ql;
    }
  }
  // since the order of the questions might have changed, we need to recreate the 
  // tooltip information
  foreach($session_data['session_config']['question_list'] as $index => $ql){
    $tooltips = _ipq_common_get_tooltip_info($ql);
    $titles = _ipq_common_get_question_titles($ql);
    $session_data['session_config']['question_tooltips'][$index] = $tooltips;
    $session_data['session_config']['question_titles'][$index] = $titles;
  }

  $first_qt_list = reset($session_data['session_config']['question_list']);
  $first_qt = key($session_data['session_config']['question_list']);
  $first_q_id = reset($session_data['session_config']['question_list'][$first_qt]);
  $session_data['session_config']['current_question_type'] = $first_qt;
  $session_data['session_config']['current_question_id'] = $first_q_id;

  return $session_data;
}