<?php

/**
 * Ajax page callback for ipq/help/ajax/%node
 * Provides ajax commands to display the content of a node in the IPQ help modal's content area
 * @param object $node
 * - Node loaded via menu param
 */
function ipq_common_help_ajax_page_callback($node) {
  // We only allow knowledge base articles to be rendered through this mechanism.
  // Allowing other node types would be a security breach.
  // @todo - also ensure that the article comes from the ipq help kb for even better security
  if($node->type != 'knowledge_base_article') {
    print 'Access denied.';
    drupal_exit();
  }

  // Render the node output
  // Tell tpl not to link the node title
  $node->no_title_link = TRUE;
  $render = node_view($node);
  $output = render($render);

  print $output;
  drupal_exit();
}

/**
 * Render the content of the IPQ help modal
 * @param string $type
 * @return string
 * - HTML output
 */
function ipq_common_help($type = 'exam') {
  $vocab = taxonomy_vocabulary_machine_name_load('knowledge_base');

  // KB for exam
  if($type == 'exam') {
    // Default help article
    $article_node = node_load(5459681);
    // Knowledgebase for source articles
    $kb = rcpar_kb_load('ipq_help');
  }
  // Quiz version
  else {
    // Default help article
    $article_node = node_load(5871541);
    // Knowledgebase for source articles
    $kb = rcpar_kb_load('ipq_help_quiz_');
  }

  $parent_tid = $kb->getTermId();

  $vid = $vocab->vid;

  // Get a regular taxonomy tree
  $tree = taxonomy_get_tree($vid, $parent_tid);

  // Get the tree in a nested format
  $tree = rcpar_kb_taxonomy_get_nested_tree($tree, NULL, $parent_tid);

  // Build a render array
  $ptt = drupal_get_path('theme', 'bootstrap_rcpar'); // path to theme
  $ptm = drupal_get_path('module', 'ipq_common'); // path to module
  $render = array();
  $render['#items'] = rcpar_kb_render_nested_tree($tree, TRUE, TRUE, $parent_tid);
  $render['#theme'] = 'item_list';
  $render['#attributes'] = array('class' => array('kb-tree'));
  $render['#attached']['js'] = array(
    $ptt . '/js/jquery-equalheights/jquery.equalheights.min.js',
    $ptm . '/js/ipq-help.js',
  );
  $render['#attached']['css'] = array($ptm . '/css/ipq-help.css');

  // Tell tpl not to link the node title
  $article_node->no_title_link = TRUE;
  $article_node = node_view($article_node);


  // Get HTML output from theme template
  $variables = array('data' => array(
    'tree' => $render,
    'article' => $article_node,
  ));

  $output = theme('ipq_help', $variables);
  return $output;
}