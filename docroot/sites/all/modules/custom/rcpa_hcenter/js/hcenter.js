(function($) {

  $(document).ready(function() {
    
    $('#mod_approve').click(function() {
      nid = $(this).attr('nid');
      if(nid) {
        modApprove(nid);
      }
    });
    
  });

  function modApprove(nid) {
    //var post;
    $.ajax({
      'url': '/homework-help-center/mod/approve/' + nid,
      'type': 'POST',
      'dataType': 'json',
      'success': function(data) {
        if (data == "success") {
            $('#mod_actions').html('');
            $('#mod_actions_status').html("Question has been marked as approved.");
            $('#mod_actions_status').addClass('messages status');
        } else {
            //$('#mod_actions_status').html('');
            $('#mod_actions_status').html("Error occured - unknown reason.");
        }
      },
      beforeSend: function() {
        $(document).ready(function() {
            $('#mod_actions_status').html('<img src="/sites/all/themes/sensation/images/circular_throbber.gif"> Processing please wait...');
        });
      },
      'error': function(data) {
        $(document).ready(function() {
            $('#mod_actions_status').html("Error Occured");
        });
      }
    });
  }

})(jQuery);