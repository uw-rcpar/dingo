<?php
$modal_id = 'hhc-question-guidelines';
$aria_label = 'Close Question Guidelines Popup';
$header_h3_id = 'termsLabel';

$block = block_load('boxes', 'hhc_student_instructions');
$header_h3_title = $block->title;

// Get the body without the title
$block_render = rcpar_mods_render_block('boxes', 'hhc_student_instructions', TRUE);
$block_render['boxes_hhc_student_instructions']['#block']->subject = '';
$modal_body = drupal_render($block_render);

$button_txt = (($variables['show_checkbox']) ? "Got it, let's get started!" : 'Got it!');
$modal_footer = '<button type="button" data-dismiss="modal" class="btn btn-success pull-right">' . $button_txt . '</button>';
if ($variables['show_checkbox']) {
  $modal_id .= '-w-checkbox';
  $header_h3_id .= 'WithShowCb';
  $modal_footer .= '
<div class="dont_show_checkbox">
  <label><input type="checkbox" value="">Don\'t show this again</label>
</div>
';
}
$m = new RCPARModal($modal_id, 'hhc-question-guidelines', array(
  'data-backdrop' => 'true',
  'role' => 'dialog',
  'aria-labelledby' => 'termsLabel',
  'tabindex' => '-1',
));
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal"  aria-label="' . $aria_label . '">x</button><h3 id="' . $header_h3_id . '" class="modal-title">' . $header_h3_title . '</h3>')
  ->setBodyContent($modal_body)
  ->setFooterContent($modal_footer);
print $m->render();
?>