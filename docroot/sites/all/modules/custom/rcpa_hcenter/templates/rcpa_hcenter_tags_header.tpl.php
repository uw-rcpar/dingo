<?php
$sort_by = is_null($variables['sort_by']) ? 'nid' : $variables['sort_by'];
?>
<div class="tags-header-wrapper">
  <div class="tags-sort"><a href="/homework-help-center/tags?sort_by=nid&sort_order=DESC" class="term-sort pop
    <?php if (isset($sort_by) && $sort_by == 'nid') { ?> active<?php } ?>">Popular</a> <a href="/homework-help-center/tags?sort_by=name&sort_order=ASC" class="term-sort alpha
    <?php if (isset($sort_by) && $sort_by == 'name') { ?> active<?php } ?>">Alphabetical</a></div>
</div>
<div class="tags-definition">A tag is a keyword or label that categorizes your question with other, similar questions. Using the right tags makes it easier for others to find and answer your question.</div>