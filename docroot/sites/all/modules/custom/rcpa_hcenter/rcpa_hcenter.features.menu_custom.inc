<?php
/**
 * @file
 * rcpa_hcenter.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function rcpa_hcenter_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-homework-help-center.
  $menus['menu-homework-help-center'] = array(
    'menu_name' => 'menu-homework-help-center',
    'title' => 'Homework Help Center',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Homework Help Center');

  return $menus;
}
