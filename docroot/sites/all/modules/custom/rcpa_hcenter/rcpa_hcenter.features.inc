<?php
/**
 * @file
 * rcpa_hcenter.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rcpa_hcenter_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "apachesolr_search" && $api == "apachesolr_search_defaults") {
    return array("version" => "3");
  }
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function rcpa_hcenter_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function rcpa_hcenter_image_default_styles() {
  $styles = array();

  // Exported image style: hhc_user_img.
  $styles['hhc_user_img'] = array(
    'label' => 'hhc_user_img',
    'effects' => array(
      27 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 35,
          'height' => 35,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function rcpa_hcenter_node_info() {
  $items = array(
    'helpcenter_question' => array(
      'name' => t('Help Center Question'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Subject'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
