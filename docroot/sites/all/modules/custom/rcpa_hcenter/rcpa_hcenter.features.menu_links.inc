<?php
/**
 * @file
 * rcpa_hcenter.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function rcpa_hcenter_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-homework-help-center_my-questions:homework-help-center/my-posts.
  $menu_links['menu-homework-help-center_my-questions:homework-help-center/my-posts'] = array(
    'menu_name' => 'menu-homework-help-center',
    'link_path' => 'homework-help-center/my-posts',
    'router_path' => 'homework-help-center/my-posts',
    'link_title' => 'My Questions',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-homework-help-center_my-questions:homework-help-center/my-posts',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-homework-help-center_search:homework-help-center.
  $menu_links['menu-homework-help-center_search:homework-help-center'] = array(
    'menu_name' => 'menu-homework-help-center',
    'link_path' => 'homework-help-center',
    'router_path' => 'homework-help-center',
    'link_title' => 'Search',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-homework-help-center_search:homework-help-center',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-homework-help-center_tags:homework-help-center/tags.
  $menu_links['menu-homework-help-center_tags:homework-help-center/tags'] = array(
    'menu_name' => 'menu-homework-help-center',
    'link_path' => 'homework-help-center/tags',
    'router_path' => 'homework-help-center/tags',
    'link_title' => 'Tags',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'query' => array(
        'sort_by' => 'nid',
        'sort_order' => 'DESC',
      ),
      'identifier' => 'menu-homework-help-center_tags:homework-help-center/tags',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('My Questions');
  t('Search');
  t('Tags');

  return $menu_links;
}
