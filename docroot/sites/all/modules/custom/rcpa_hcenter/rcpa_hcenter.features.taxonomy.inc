<?php
/**
 * @file
 * rcpa_hcenter.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function rcpa_hcenter_taxonomy_default_vocabularies() {
  return array(
    'hhc_general_topics' => array(
      'name' => 'HHC General Topics',
      'machine_name' => 'hhc_general_topics',
      'description' => 'Topics the user can select from that indicate the high-level idea being asked.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
