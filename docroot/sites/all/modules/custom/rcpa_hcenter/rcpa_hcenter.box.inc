<?php
/**
 * @file
 * rcpa_hcenter.box.inc
 */

/**
 * Implements hook_default_box().
 */
function rcpa_hcenter_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'hhc_cpa_exam_structure';
  $box->plugin_key = 'simple';
  $box->title = 'Instant Answers: Get the info you need without the wait';
  $box->description = 'HHC - CPA Exam Structure';
  $box->options = array(
    'body' => array(
      'value' => '<p>Since the Homework Help Center is dedicated to answering questions specific to course content, rather than post your question about CPA Exam structure here please check out the following information-packed resources to get your question answered right away:</p><p><strong>CPA Exam Structure</strong></p><ul><li><a target="_blank" href="/cpa-exam/content-and-structure">Learn about the CPA Exam: CPA Exam Content &amp; Structure</a></li><li><a target="_blank" href="/qa/c/cpa-exam">Questions &amp; Answers: CPA Exam</a></li></ul><p>If you&rsquo;re unable to get the information you need, simply reach out to the <a href="/contact">customer care team</a> and we&rsquo;ll get you squared away and back on track to productive studying!</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'hhc-info-box',
  );
  $export['hhc_cpa_exam_structure'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'hhc_cpa_exam_updates';
  $box->plugin_key = 'simple';
  $box->title = 'Instant Answers: Get the info you need without the wait';
  $box->description = 'HHC - CPA Exam Updates';
  $box->options = array(
    'body' => array(
      'value' => '<p>Since the Homework Help Center is dedicated to answering questions specific to course content, rather than post your question about CPA Exam updates here please check out the following information-packed resources to get your question answered right away:</p><p><strong>CPA Exam Updates</strong></p><ul><li><a target="_blank" href="/cpa-exam/changes">Learn about the CPA Exam: CPA Exam Changes</a></li></ul><p>If you&rsquo;re unable to get the information you need, simply reach out to the <a href="/contact">customer care team</a> and we&rsquo;ll get you squared away and back on track to productive studying!</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'hhc-info-box',
  );
  $export['hhc_cpa_exam_updates'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'hhc_exam_and_licensure';
  $box->plugin_key = 'simple';
  $box->title = 'Instant Answers: Get the info you need without the wait';
  $box->description = 'HHC - CPA Exam and Licensure Requirements';
  $box->options = array(
    'body' => array(
      'value' => '<p>Since the Homework Help Center is dedicated to answering questions specific to course content, rather than post your question about CPA Exam and licensure requirements here please check out the following information-packed resources to get your question answered right away:</p><p><strong>CPA Exam and Licensure Requirements</strong></p><ul><li><a target="_blank" href="/cpa-exam/cpa-license">Learn about the CPA Exam: CPA License Requirements</a></li><li><a target="_blank" href="/qa/c/cpa-licensure">Questions &amp; Answers: CPA Licensure</a></li></ul><p>If you&rsquo;re unable to get the information you need, simply reach out to the <a href="/contact">customer care team</a> and we&rsquo;ll get you squared away and back on track to productive studying!</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'hhc-info-box',
  );
  $export['hhc_exam_and_licensure'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'hhc_heading';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'HHC heading';
  $box->options = array(
    'body' => array(
      'value' => '<h1 class="hhc-page-title"><img src="/sites/default/files/icon-hhc.svg" width="60px" alt="Homework Help Center icon" /> Homework Help Center</h1>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['hhc_heading'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'hhc_student_instructions';
  $box->plugin_key = 'simple';
  $box->title = 'Got a question? Here\'s some guidelines before submitting a post.';
  $box->description = 'Homework Help Center instructions';
  $box->options = array(
    'body' => array(
      'value' => '<div class="hhc-instructions" style="margin-top: 20px; margin-bottom: 20px;">
<ul>
<li>
<strong>The Homework Help Center is a student community resource dedicated to answering questions specific to course content.</strong> For non-content related topics such as basic course use, account issues, or recent orders, please contact our <a href="/contact">customer care team</a>.
</li>
<li>
<strong>To find the answer to your question, start by using the forum search tools to see if your question has already been resolved in a previous post.</strong> This is your best bet to getting an instant answer and helps maintain the community by decreasing repetitive posts. You may also want to consider running a general web search, since authoritative information is available in abundance from sources such as the IRS, FASB, COSO, and the AICPA. Researching in this way is a valuable tool in preparing for the CPA Exam. 
</li>
<li>
<strong>If you\'re unable to find an existing answer to your question, please be sure to supply the relevant section, topic, or question ID when submitting a post.</strong> This will help our community moderators provide you with as efficient a response as possible. 
</li>
<li>
<strong>During high volume periods, response times can take up to 4 business days for a moderator to respond to your questions.</strong> Please note that this is not the norm, and we will do everything in our power to provide you with a quick response to your questions. 
</li>
<li>
<strong>We ask that you try to avoid submitting more than one question per day.</strong> If do you submit multiple posts in a day, please keep in mind that any posts beyond your first one will be put on hold until other students’ questions submitted on the same day have been addressed.
</li>
</ul>
</div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['hhc_student_instructions'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'hhc_study_tech_and_strategies';
  $box->plugin_key = 'simple';
  $box->title = 'Instant Answers: Get the info you need without the wait';
  $box->description = 'HHC - Study Techniques and Strategies';
  $box->options = array(
    'body' => array(
      'value' => '<p>Since the Homework Help Center is dedicated to answering questions specific to course content, rather than post your question about study techniques and strategies here please check out the following information-packed resources to get your question answered right away:</p><p><strong>Study Techniques and Strategies</strong></p><ul><li><a target="_blank" href="/qa/c/cpa-exam-review-&amp;-preparation">Questions &amp; Answers: CPA Exam Review &amp; Preparation</a></li></ul><p>If you&rsquo;re unable to get the information you need, simply reach out to the <a href="/contact">customer care team</a> and we&rsquo;ll get you squared away and back on track to productive studying!</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'hhc-info-box',
  );
  $export['hhc_study_tech_and_strategies'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'hhc_using_your_course';
  $box->plugin_key = 'simple';
  $box->title = 'Instant Answers: Get the info you need without the wait';
  $box->description = 'HHC - Using Your Course';
  $box->options = array(
    'body' => array(
      'value' => '<p>Since the Homework Help Center is dedicated to answering questions specific to course content, rather than post your question about using your course here please check out the following information-packed resources to get your question answered right away:</p><p><strong>Using Your Course</strong></p><ul><li><a href="/frequently-asked-questions" target="_blank">Frequently Asked Questions</a></li></ul><p>If you&rsquo;re unable to get the information you need, simply reach out to the <a href="/contact">customer care team</a> and we&rsquo;ll get you squared away and back on track to productive studying!</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'hhc-info-box',
  );
  $export['hhc_using_your_course'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'homework_help_center_box';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'hcc landing page';
  $box->options = array(
    'body' => array(
      'value' => '<h2 id="browse-header">Browse by Exam Part</h2>
<ul role="menu" aria-labelledby="browse-header" class="browse-links clearfix hhc-category-links">
<li role="menu-item" class="aud-category-link"><a href="">AUD<span>Auditing<br />& Attestation</span></a></li>
<li role="menu-item" class="bec-category-link"><a href="">BEC<span>Business Environment<br />& Concepts</span></a></li>
<li role="menu-item" class="far-category-link"><a href="">FAR<span>Financial Accounting<br />& Reporting</span></a></li>
<li role="menu-item" class="reg-category-link"><a href="">REG<span>Regulation</span></a></li>
</ul>
<div class="hhc-info-box clearfix">
<div class="hhc-info-icon"><img src="/sites/default/files/icon-info.svg" alt="Information Icon" width="36" /> </div>
<div class="hhc-info-copy">
<p id="hhc-info"><strong>The Homework Help Center is a student community resource dedicated to answering questions specific to course content.</strong> Please contact our <a href="/contact">customer care team</a> for non-content related topics such as the following:</p>
<ul aria-labelledby="hhc-info" class="hhc-info-list">
<li>Technical support</li>
<li>The order in which you should sit for exam sections</li>
<li>How to study</li>
<li>How the exam is scored</li>
<li>Basic course use</li>
<li>Account issues</li>
<li>Recent orders</li>
</ul>
</div>
</div>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'node',
  );
  $export['homework_help_center_box'] = $box;

  return $export;
}
