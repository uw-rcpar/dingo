<?php
/**
 * @file
 * rcpa_hcenter.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function rcpa_hcenter_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_exam_options|node|helpcenter_question|form';
  $field_group->group_name = 'group_exam_options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'helpcenter_question';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'CPA EXAM OPTIONS',
    'weight' => '1',
    'children' => array(
      0 => 'field_course_section',
      1 => 'field_course_chapter',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'CPA EXAM OPTIONS',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-exam-options field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_exam_options|node|helpcenter_question|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('CPA EXAM OPTIONS');

  return $field_groups;
}
