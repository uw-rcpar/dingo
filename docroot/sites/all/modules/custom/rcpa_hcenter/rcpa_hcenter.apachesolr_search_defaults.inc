<?php
/**
 * @file
 * rcpa_hcenter.apachesolr_search_defaults.inc
 */

/**
 * Implements hook_apachesolr_search_default_searchers().
 */
function rcpa_hcenter_apachesolr_search_default_searchers() {
  $export = array();

  $searcher = new stdClass();
  $searcher->disabled = FALSE; /* Edit this to true to make a default searcher disabled initially */
  $searcher->api_version = 3;
  $searcher->page_id = 'homework_help_center';
  $searcher->label = 'Homework Help Center';
  $searcher->description = '';
  $searcher->search_path = 'homework-help-center/search';
  $searcher->page_title = 'Homework Help Center';
  $searcher->env_id = 'acquia_search_server_1';
  $searcher->settings = array(
    'fq' => array(
      0 => 'bundle:helpcenter_question',
    ),
    'apachesolr_search_custom_enable' => 1,
    'apachesolr_search_search_type' => 'custom',
    'apachesolr_search_per_page' => 10,
    'apachesolr_search_spellcheck' => 0,
    'apachesolr_search_search_box' => 1,
    'apachesolr_search_allow_user_input' => 0,
    'apachesolr_search_browse' => 'results',
  );
  $export['homework_help_center'] = $searcher;

  return $export;
}
