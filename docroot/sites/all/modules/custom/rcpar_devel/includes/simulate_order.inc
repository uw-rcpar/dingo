<?php

/**
 * Page callback for rcpar-devel/simulate-order
 */
function rcpar_devel_simulate_order_form($form, &$form_state) {
  // Don't want this cached when used by anonymous users
  drupal_page_is_cacheable(FALSE);

  $form['help'] = array(
    '#markup' => '
    <div class="jumbotron" style="padding-top: 16px; padding-bottom: 24px;">
      <h1 style="font-size: 42px">Quickie enrollment simulator!</h1>
      <p class="lead">This is a super quick and handy way to simulate the enrollment process of a new user placing an order.</p>
    </div>
      <p>The form will:
      <ol>
        <li>Create a new user with the specified password with the roles: 
          <ul><li>authenticated</li><li>student enrolled</li></ul>
        </li>
        <li>Log you in as that user</li>
        <li>Add the specified product to their shopping cart</li>
        <li>Simulate the commerce events for "checkout_complete", "pending", and "complete"</li>
        <li>These events trigger the user entitlement creation process in the same way that a regular purchase does</li>
        <li>You are then redirected to the page of your choice. Stay on this page for rapidly debugging the enrollment process repeatedly</li>
        <li>The username is <em>timestamped</em> so that you don\'t need to re-edit it on each submit.</li>
      </ol>
      
      Note: The simulator does <strong>not:</strong>
      <ul>
        <li>Create user profile information (how did you hear about us, college, shipping address)</li>
        <li>Create a payment transaction</li>
      </ul>
      This page can be used as anonymous or authenticated and you may resubmit the form repeatedly without logging out
      (you will be logged in as the new user each time).<br>
      <strong>Security note:</strong> Being part of the rcpar_devel module, this page should never be accessible to production site users.
      </p>
      
      <div class="alert alert-secondary">
        <em>Developers: add more handy scenarios to this to make testing and developing easier for all!</em>
      </div>
      
      <hr class="my-4">',
  );

  // Email address
  $form['email'] = array(
    '#title' => 'Email',
    '#type' => 'textfield',
    '#description' => 'Can also be the email address of an existing user, in which case an order will be created for them.',
    '#default_value' => 'rcpardevel' . date('h_i_s') . '@rcparlabs.com',
  );

  // Password
  $form['pass'] = array(
    '#title' => 'Password',
    '#type' => 'textfield',
    '#description' => 'Ignored if this is an existing user.',
    '#default_value' => 'password',
  );

  $form['partner_id'] = array(
    '#title' => 'Partner ID',
    '#type' => 'textfield',
    '#description' => 'If you want the order and user entitlements to be associated with a partner ID, enter it here.',
  );

  // Product type being "purchased"
  $form['type'] = array(
    '#title' => 'Purchase type',
    '#type' => 'radios',
    '#default_value' => 'full-prem',
    '#options' => array(
      // Course packages
      'FULL' => 'Select Course',
      'FULL-PREM' => 'Premier Course',
      'FULL-ELITE' => 'Elite Course',

      // Online course - single parts
      'AUD' => 'AUD Online Course',
      'FAR' => 'FAR Online Course',

      // CRAM
      'FULL-CRAM' => 'Full Online CRAM Course',
      'AUD-CRAM' => 'AUD CRAM',
      'FAR-CRAM' => 'FAR CRAM',

      // Mobile offline access
      'FULL-MOB-OFF' => 'FULL - Mobile Offline Access',
      'AUD-MOB-OFF' => 'AUD - Mobile Offline Access',
      'FAR-MOB-OFF' => 'FAR - Mobile Offline Access',

      // Audio lectures
      'FULL-AUD' => 'FULL - Audio Lectures',
      'AUD-AUD' => 'AUD - Audio lectures',
      'FAR-AUD' => 'FAR - Audio lectures',

      // Offline lectures
      'FULL-OFF-LEC' => 'FULL - Offline Lectures',
      'AUD-OFF-LEC' => 'AUD - Offline Lectures',
      'FAR-OFF-LEC' => 'FAR - Offline Lectures',
    ),
  );

  // Redirect option
  $form['redirect'] = array(
    '#title' => 'Redirect on complete',
    '#type' => 'select',
    '#options' => array(
      0 => 'Nowhere',
      'dashboard' => 'Dashboard',
      'ipq' => 'IPQ',
      'dashboard/my-courses' => 'My Courses',
      'dashboard/my-cram-courses' => 'My CRAM Courses',
    ),
    '#default_value' => 'dashboard',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}


/**
 * Submit callback for rcpar_devel_simulate_order_form.
 */
function rcpar_devel_simulate_order_form_submit(&$form, &$form_state) {
  // Try to load an existing user
  $account = user_load_by_mail($form_state['values']['email']);
  if(!$account) {
    // Create a new user
    $account = rcpar_devel_create_student_user($form_state['values']['email'], $form_state['values']['pass']);
  }

  // Log the user in
  RCPAR\User::drupalLogin($account->uid);

  // Clear the cart, and add our product(s) to it.
  enroll_flow_clear_cart();
  $product = commerce_product_load_by_sku($form_state['values']['type']);
  // Add bundle
  if (RCPARCommerceBundle::isProductIdBundle($product->product_id)) {
    $rcb = new RCPARCommerceBundle($product->product_id);
    $rcb->addToCart();
  }
  // Add individual product
  else {
    $line_item = commerce_product_line_item_new($product, 1);
    commerce_cart_product_add($account->uid, $line_item);
  }

  // Get the order in our cart.
  $order = commerce_cart_order_load($account->uid);
  $o = new RCPARCommerceOrder($order);

  // Inject partner if we have it, otherwise clear it out
  if(!empty($form_state['values']['partner_id'])) {
    $partner = node_load($form_state['values']['partner_id']);
    $_SESSION['rcpar_partner'] = $partner;
    $o->setPartnerProfile($partner->nid);
  }
  // Make sure there is NO partner in the session.
  else {
    rcpar_partners_unset_partner_sess();
  }

  // Invoke checkout complete events for the new order.
  commerce_order_status_update($order, 'checkout_complete');
  commerce_checkout_complete($order);
  commerce_order_status_update($order, 'pending');
  commerce_order_status_update($order, 'completed');
  commerce_order_save($order);

  // Redirect the user to the location of their choosing.
  if ($form_state['values']['redirect']) {
    $form_state['redirect'] = $form_state['values']['redirect'];
  }

  // Status message.
  drupal_set_message('Created user: ' . $account->name . ' | uid: ' . $account->uid);
}

/**
 * Create a Drupal user
 *
 * @see rcpar_asl_create_account_form_submit()
 * - A bunch of this code was hastily copied from this function. Too bad there weren't better abstractions or we
 * probably could have used it as-is. Maybe we can make a refactor in the future so that creating a user is only
 * done by one function.
 *
 * @param string $email
 * - User's email address
 * @param string $pass
 * - User's password
 * @return object
 *  - A Drupal user object
 */
function rcpar_devel_create_student_user($email, $pass) {
  // as the user only inputs his email, and drupal force us to use a username when creating a new user
  // we are autogenerating one based on the email
  // so we will need check for the availability of that username

  // note that the discount_verification_generate_username_from_email function
  // belongs to the discount_verification module hence the dependency on .info file
  $username = discount_verification_generate_username_from_email($email);
  // Now, let's ensure that the autogenerated username is not in use
  // and in case it's in use, we are going to change it a little and try again
  $base_username = $username;
  $attempts = 0;
  do {
    $taken = (bool) db_select('users')->fields('users', array('uid'))
      ->condition('name', db_like($username), 'LIKE')
      ->range(0, 1)->execute()->fetchField();
    if ($taken) {
      // The username is already in use, let's add some random numbers and try again
      $username = $base_username . rand(0, 1000);
      $attempts++;
    }
  } while ($taken && $attempts < 20);
  if ($taken) {
    // In the case we were unlucky enough to try this 20 times without success, we print and error and fail
    drupal_set_message(t('There was a problem creating the user.'), 'error');
    return;
  }

  // Fields for the user object
  $fields = array(
    'name' => $username,
    'mail' => $email,
    'pass' => $pass,
    'status' => 1,
    'init' => 'email address',
    'roles' => array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      4 => 'student enrolled',
    ),
  );
  // the first parameter is left blank so a new user is created
  $account = user_save(NULL, $fields);

  // Somehow, when we create the user the role 'student not enrolled' (rid 8) is added to the account
  // and as we are adding entitlements to the user, that would't be correct
  // also, that role creates visualization issues when users access to the dashboard
  // Note that this function does not return the updated $account object, so we reload it to ensure
  // we have an updated account when invoking hooks
  user_multiple_role_edit(array($account->uid), 'remove_role', 8);
  $account = user_load($account->uid);

  return $account;
}