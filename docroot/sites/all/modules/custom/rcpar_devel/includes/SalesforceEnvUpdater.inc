<?php

class SalesforceEnvUpdater {
  
  /**
   * @var array
   */
  private $metadata;
  
  /**
   * @var string
   */
  private $env;
  
  /**
   * SalesforceEnvUpdater constructor.
   *
   * @param string $env
   */
  public function __construct($env) {
    $metadata_json = file_get_contents(drupal_get_path('module', 'rcpar_devel') . '/includes/salesforce_env_mapping.json');
    $this->metadata = json_decode($metadata_json, true);
    $this->env = $env;
  }
  
  public function run() {
    if ($this->metadata) {
      $this->updateSalesforceVariables();
      $this->updateSalesforceMappingId();
      $this->unmapSalesforceEntities();
      $this->resaveSalesforceEntities();
    }
    else {
      drupal_set_message("Error parsing JSON input", "error");
    }
  }
  
  /**
   * Reads drupal variables to override from json config file
   */
  private function updateSalesforceVariables() {
    $metadata = $this->metadata;
    $env = $this->env;
    drupal_set_message("[salesforce] Overriding global variables for env '$env'");
    if ($metadata[$env] && $metadata[$env]['drupal_variables']) {
      foreach ($metadata[$env]['drupal_variables'] as $key => $val) {
        drupal_set_message("[salesforce] setting variable $key = $val");
        variable_set($key, $val);
      }
    }
  }
  
  /**
   * Removes sf entity mappings - read from metadata
   * @deprecated - resaveSalesforceEntities is doing this this internally
   * @param string $env
   * @param array $metadata
   */
  private function unmapSalesforceEntities() {
    $metadata = $this->metadata;
    $env = $this->env;
    drupal_set_message("[salesforce] rcpar_devel_unmap_sf_entities for env '$env'");
    if ($metadata[$env] && $metadata[$env]['entities_to_unmap']) {
      foreach ($metadata[$env]['entities_to_unmap'] as $entity_type => $entity_ids) {
        if (!empty($entity_ids)) {
          foreach ($entity_ids as $entity_id) {
            $this->entityUnmap($entity_type, $entity_id);
          }
        }
      }
    }
  }
  
  /**
   * Resaves sf entity mappings - read from metadata
   *
   * @param $env
   * @param array $metadata
   */
  private function resaveSalesforceEntities() {
    $metadata = $this->metadata;
    $env = $this->env;
    drupal_set_message("[salesforce] rcpar_devel_resave_sf_entities for env '$env'");
    if ($metadata[$env] && $metadata[$env]['entities_to_resave']) {
      foreach ($metadata[$env]['entities_to_resave'] as $entity_type => $entity_ids) {
        if (!empty($entity_ids)) {
          foreach ($entity_ids as $entity_id) {
            $this->entityResave($entity_type, $entity_id);
          }
        }
      }
    }
  }
  
  /**
   * Override drupal mappings
   */
  function updateSalesforceMappingId() {
    $metadata = $this->metadata;
    $env = $this->env;
    drupal_set_message("[salesforce] Overriding mappings for env '$env'");
    if ($metadata[$env] && $metadata[$env]['sf_id_mapping']) {
      foreach ($metadata[$env]['sf_id_mapping'] as $originalId => $destId) {
        drupal_set_message("[salesforce] updating mapping  $originalId => $destId");
        
        db_update('salesforce_mapping_object')
          ->fields(array(
            'salesforce_id' => $destId,
          ))
          ->condition('salesforce_id', $originalId)
          ->execute();
        
        db_update('salesforce_mapping_object_revision')
          ->fields(array(
            'salesforce_id' => $destId,
          ))
          ->condition('salesforce_id', $originalId)
          ->execute();
      }
    }
  }
  
  /**
   * Resaves the entity
   *
   * @param $entity_type
   * @param $entity_id
   */
  private function entityResave($entity_type, $entity_id) {
    if ($entities = entity_load($entity_type, [$entity_id])) {
      if (!empty ($entities)) {
        $entity = reset($entities);
        drupal_set_message("Resave entity $entity_type $entity_id FOUND. Saving..");
        entity_save($entity_type, $entity);
      }
    }
  }
  
  /**
   * @param $entity_type
   * @param $entity_id
   */
  private function entityUnmap($entity_type, $entity_id) {
    return db_query("DELETE smo , smor
    FROM salesforce_mapping_object smo
    LEFT JOIN salesforce_mapping_object_revision smor
    ON smo.salesforce_mapping_object_id = smor.salesforce_mapping_object_id
    WHERE smo.entity_type = :entity_type
    AND smo.entity_id  =:entity_id",
      [
        ':entity_type' => $entity_type,
        ':entity_id' => $entity_id
      ]
    );
  }
  
}