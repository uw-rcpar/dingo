<?php

/**
 * Implements hook_default_rules_configuration()
 * Remove authorize.net and Affirm payment rules and replace with defaults that use developer credentials/configuration
 *
 * @return array
 */
function rcpar_devel_default_rules_configuration() {
  // Remove the existing authorize.net payment rule
  db_delete('rules_config')
    ->condition('name', 'commerce_payment_authnet_aim', '=')
    ->execute();

  // Remove the existing Affirm payment rule
  db_delete('rules_config')
    ->condition('name', 'commerce_payment_affirm', '=')
    ->execute();

  $items = array();

  /////////////// AUTHORIZE.NET /////////////////////////
	$items['commerce_payment_authnet_aim'] = entity_import('rules_config', '{ "commerce_payment_authnet_aim" : {
	  "LABEL" : "Authorize.Net AIM - Credit Card",
	  "PLUGIN" : "reaction rule",
	  "WEIGHT" : "-2",
	  "OWNER" : "rules",
	  "TAGS" : [ "Commerce Payment" ],
	  "REQUIRES" : [ "commerce_price", "commerce_payment" ],
	  "ON" : { "commerce_payment_methods" : [] },
	  "IF" : [
	    { "commerce_price_compare_price" : {
	        "first_price" : [ "commerce-order:commerce-order-total" ],
	        "operator" : "\\u003E=",
	        "second_price" : { "value" : { "amount" : 100, "currency_code" : "USD" } }
	      }
	    }
	  ],
	  "DO" : [
	    { "commerce_payment_enable_authnet_aim" : {
	        "commerce_order" : [ "commerce-order" ],
	        "payment_method" : { "value" : {
	            "method_id" : "authnet_aim",
	            "settings" : {
	              "login" : "5eK6Jf7T",
	              "tran_key" : "477JQcz9y4Xhr544",
	              "txn_mode" : "developer",
	              "txn_type" : "auth_capture",
	              "card_types" : {
	                "visa" : "visa",
	                "mastercard" : "mastercard",
	                "amex" : "amex",
	                "discover" : "discover",
	                "dc" : 0,
	                "dci" : 0,
	                "cb" : 0,
	                "jcb" : 0,
	                "maestro" : 0,
	                "visaelectron" : 0,
	                "laser" : 0,
	                "solo" : 0,
	                "switch" : 0
	              },
	              "email_customer" : 0,
	              "log" : { "request" : 0, "response" : 0 }
	            }
	          }
	        }
	      }
	    }
	  ]
	}
	}');

  /////////////// AFFIRM /////////////////////////
  /*
   * SHUTTING OFF AFFIRM

  $items['commerce_payment_affirm'] = entity_import('rules_config', '{ "commerce_payment_affirm" : {
    "LABEL" : "Affirm Payment",
    "PLUGIN" : "reaction rule",
    "WEIGHT" : "-1",
    "OWNER" : "rules",
    "TAGS" : [ "Commerce Payment" ],
    "REQUIRES" : [ "rules", "commerce_payment" ],
    "ON" : { "commerce_payment_methods" : [] },
    "IF" : [
      { "data_is" : {
          "data" : [ "commerce-order:commerce-order-total:currency-code" ],
          "value" : "USD"
        }
      }
    ],
    "DO" : [
      { "commerce_payment_enable_affirm" : {
          "commerce_order" : [ "commerce-order" ],
          "payment_method" : { "value" : {
              "method_id" : "affirm",
              "settings" : {
                "txn_mode" : "sandbox",
                "private_key" : "weduU8GsBkmuH5nPRCc6Zm0YE8mqMbto",
                "public_key" : "OUT8WYJUZPOL0VXY",
                "financial_key" : "0KPABAMSE6YN03WS",
                "txn_type" : "auth_capture",
                "log" : 1
              }
            }
          }
        }
      }
    ]
  }
  }');
  */

  return $items;
}
