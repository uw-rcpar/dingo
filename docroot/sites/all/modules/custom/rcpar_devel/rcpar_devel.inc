<?php

/**
 * Implements hook_init() 
 * 
 * NOTE HACKY WEIRDNESS: We are creating a core module function that does not exist. 
 * We have included this module code in setup.php.
 * Thus, this function is called regardless of whether or not rcpar_devel module is enabled.
 * This allows us to force rcpar_devel to be enabled in all dev environments.
 */
function node_init(){
  if (!module_exists('rcpar_devel')) {
    module_enable(array('rcpar_devel'));
    drupal_set_message(t('You are working in a development environment. RCPAR Devel module has been enabled.'));

    // Set some variables that can be reconfigured - variable_set allows them to be changed later
    // UWorld API base URL:
    variable_set('uworld_api_url', 'https://api.zeqo.com');
    variable_set('rcpar_saml_idp_login_url', 'https://dev.zeqo.com/app/index.html#/rogercpalogin/');
    variable_set('rcpar_saml_idp_logout_url', 'https://dev.zeqo.com/IDP/logout.aspx');
    variable_set('rcpar_saml_idp_issuer', 'https://dev.zeqo.com/Metadata.aspx');
  }
  // Disable ES event module
  if (module_exists('es_events')) {
    module_disable(array('es_events'), $disable_dependents = TRUE);
    drupal_set_message("You are working on a development environment. ES event module has been disabled !", 'status');
  }
}
