<?php

/**
 * Implements hook_drush_command().
 */
function rcpar_devel_drush_command() {
  $items['rcpar-devel-salesforce-env-updater'] = array(
    'description' => 'Cron wrapper for SalesforceEnvUpdater',
  );

  $items['search-and-replace'] = array(
    'aliases' => array('sar'),
    'callback' => 'drush_sar_replace',
    'description' => dt('Replace strings in text fields in all content.'),
    'arguments' => array(
      'search' => dt('Existing text.'),
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * Drush command around rcpar_devel_salesforce_env_updater_run()
 */
function drush_rcpar_devel_salesforce_env_updater() {
  // Setup the allowed SITE_ENVIRONMENT for salesforce updates
  drupal_set_message("rcpar_devel_salesforce_env_updater() starts env = " . $_ENV['AH_SITE_ENVIRONMENT']);
  rcpar_devel_salesforce_env_updater_run();
}

/**
 * Command callback.
 *
 * This callback checks the version of Drupal it's operating on and runs an
 * appropriate function to do the actual work. If an unsupported version is
 * detected, it will exit.
 */
function drush_sar_replace() {
  list ($search) = func_get_args();

  drush_print(dt('Going perform a search on all text fields in all bundles and all custom blocks on @site', array('@site' => variable_get('site_name', 'Drupal'))));
  drush_print(dt('  !search', array('!search' => $search)));
  drush_print(dt(' '));
  $continue = drush_confirm(dt('There is no undo. Have you just created a database backup?'));
  if (empty($continue)) {
    drush_log(dt('Aborting'), 'warning');
    return 0;
  }
  return _drush_sar_replace_d7($search);
}

/**
 * Does the search and replace for Drupal 7 sites.
 */
function _drush_sar_replace_d7($search) {
  // Grab all defined fields, then cycle through them and run queries.
  $fields = field_info_fields();
  foreach ($fields as $field) {
    // Skip fields that aren't provided by the text module.
    if ($field['module'] != 'text') {
      continue;
    }

    // Use (private, naughty) API calls to grab the table and column names.
    $data_table = _field_sql_storage_tablename($field);
    $column = _field_sql_storage_columnname($field['field_name'], 'value');

    $bundles = array();
    foreach ($field['bundles'] as $entity => $bundle) {
      $bundles += array_values($bundle);
    }

    drush_log(dt('Processing field @name (@bundles)', array('@name' => $field['field_name'], '@bundles' => implode(', ', $bundles))), 'ok');

    // Update data in FIELD_LOAD_CURRENT.
    $updated = db_select($data_table)
        ->fields('c')
        ->condition($column, $search)
        ->countQuery()
        ->execute()
        ->fetchField();

    drush_log(dt('  Find @bundle.', array('@bundle' => format_plural($updated, '1 bundle', '@count bundles'))), 'ok');
  }

  // Do the blocks too.
  $updated = db_select('block_custom')
      ->fields('c')
      ->condition('body', $search)
      ->countQuery()
      ->execute()
      ->fetchField();
  drush_log(dt('  find @block.', array('@block' => format_plural($updated, '1 custom block', '@count custom blocks'))), 'ok');

  return 1;
}
