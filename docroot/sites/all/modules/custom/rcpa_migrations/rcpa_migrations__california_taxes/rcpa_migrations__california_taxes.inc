<?php

class TaxMigration extends BaseRcpaMigration {
  public function __construct($arguments) {
    parent::__construct($arguments);


    $this->description = 'Taxes for California, this gets updated from a csv file';

    $table_name = 'rcpa_migrations__california_taxes';
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'city' => array(
          'type' => 'varchar',
          'length' => 64,
          'not null' => TRUE
        ),
        'county' => array(
          'type' => 'varchar',
          'length' => 64,
          'not null' => TRUE
        ),
      ),
      MigrateDestinationTable::getKeySchema($table_name)
    );
    $columns = array(
      0 => array('city', 'Tax City'),
      1 => array('rate', 'Tax Rate'),
      2 => array('county', 'Tax County')
    );
    $this->source = new MigrateSourceCSV('public://City_Rates.csv', $columns, array('header_rows' => 7, 'delimiter' => ";"));
    $this->destination = new MigrateDestinationTable($table_name);

    // Mapped fields
    $this->addFieldMapping('city', 'city')
      ->description("City of the State that will hold the sales tax");
    $this->addFieldMapping('county', 'county')
      ->description("City County that will reign over the tax rate");
    $this->addFieldMapping('rate', 'rate')
      ->defaultValue(0.0)
      ->description("Tax rate, represented as a float number without the % symbol");
  }

  /**
   * Prepare some of our rows, delete non needed characters from the data set
   *
   * @param $row
   *
   * @return bool
   */
  function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    // Process rate items
    $row->rate = floatval(str_replace(',', '.', $row->rate));
    $row->city = str_replace('*', '', $row->city);

    return TRUE;
  }
}
