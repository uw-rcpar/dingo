<?php

function rcpa_migrations__california_taxes_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'rcpa' => array(
        'title' => t('Roger CPA Migrations'),
      )
    ),
    'migrations' => array(
      'CaliforniaTaxes' => array(
        'class_name' => 'TaxMigration',
        'group_name' => 'rcpa',
      )
    )
  );
  return $api;
}
