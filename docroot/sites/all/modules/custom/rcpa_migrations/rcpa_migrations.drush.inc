<?php

/**
 * Implements hook_drush_command().
 */
function rcpa_migrations_drush_command() {
  $items['uworld-user-import'] = array(
    'description' => 'Import the users from uWorld using a csv, with two columns "UWorldUserId" and 	"RogerUserId".',
    'aliases' => array('uwui'),
    'arguments' => array(
      'file' => 'Full path to the csv file including the filename.',
    ),
    'examples' => array(
      'drush uwui myfile.csv' => 'Mapps the field uWorld id from the csv file using the UID provided on the roger column.'
    ),
  );

  return $items;
}

/**
 * Callback for the drush command
 * @param  $file
 *   - The string meaning the file name or full path plus the name
 */
function drush_rcpa_migrations_uworld_user_import($file = FALSE) {
  if (file_exists($file)) {
    if (($handle = fopen($file, 'r')) !== FALSE) {
      $header = TRUE;
      while (($row = fgetcsv($handle)) !== FALSE) {
        if ($header) {
          $header = FALSE;
        }
        else {
          $roger_uid = $row[1];
          $uworld_uid = $row[0];
          $user = user_load($roger_uid);
          $user_wrapper = entity_metadata_wrapper('user', $user);
          try {
            $user_wrapper->field_uwuid->set($uworld_uid);
            $user_wrapper->save();
            drush_print_r($user_wrapper->getIdentifier());
          }
          catch (Exception $ex) {
            drush_print_r($ex->getMessage());
            watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . 'ERROR: ' . $ex->getMessage());
          }
        }
      }

      fclose($handle);
    }
  }
}
