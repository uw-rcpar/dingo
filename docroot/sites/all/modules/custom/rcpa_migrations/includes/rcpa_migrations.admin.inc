<?php

function rcpa_migration_email_migraions_list() {
  $form = array();


  $form['course_config']['migration_report_emails'] = array(
    '#type' => 'textfield',
    '#title' => t('Email List'),
    '#description' => t('Comma Separated Email List of migrations csv report.'),
    '#default_value' => variable_get('migration_report_emails', ''),
  );


  return system_settings_form($form);
}
