<?php

function rcpa_migrations_views_default_views() {
  $view = new view();
  $view->name = 'migration_details';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'enroll_flow_uw_teleport';
  $view->human_name = 'Migration Details';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Migration Details';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    12 => '12',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'input_required' => 0,
      'text_input_required' => array(
        'text_input_required' => array(
          'value' => 'Select any filter and click on Apply to see results',
          'format' => 'filtered_html',
        ),
      ),
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'secondary_collapse_override' => '0',
    ),
    'teleported_time' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
        'datepicker_options' => '',
      ),
    ),
    'teleported_time_1' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
        'datepicker_options' => '',
      ),
    ),
    'teleported' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table_megarows';
  $handler->display->display_options['row_plugin'] = 'views_bootstrap_carousel_plugin_rows';
  /* Relationship: UWorld migration data: User ID */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = TRUE;
  /* Field: UWorld migration data: ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  /* Field: User: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'users';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['relationship'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = 'UID';
  $handler->display->display_options['fields']['uid']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['uid']['alter']['path'] = '/customercare-screen/[uid]';
  $handler->display->display_options['fields']['uid']['link_to_user'] = FALSE;
  /* Field: UWorld migration data: Has teleported field */
  $handler->display->display_options['fields']['teleported']['id'] = 'teleported';
  $handler->display->display_options['fields']['teleported']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['fields']['teleported']['field'] = 'teleported';
  $handler->display->display_options['fields']['teleported']['label'] = 'Teleported';
  $handler->display->display_options['fields']['teleported']['not'] = 0;
  /* Field: UWorld migration data: Teleported date */
  $handler->display->display_options['fields']['teleported_time']['id'] = 'teleported_time';
  $handler->display->display_options['fields']['teleported_time']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['fields']['teleported_time']['field'] = 'teleported_time';
  $handler->display->display_options['fields']['teleported_time']['date_format'] = 'short';
  $handler->display->display_options['fields']['teleported_time']['second_date_format'] = 'long';
  $handler->display->display_options['fields']['teleported_time']['format_date_sql'] = 0;
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = 'Ineligible Reason';
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_output'] = '<?php echo enroll_flow_migration_ineligibility_reason($row->uid); ?>
';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Field: Bulk operations: User */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_user';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['relationship'] = 'uid';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::rcpa_migrations_data_export' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'skip_permission_check' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  $handler->display->display_options['filter_groups']['operator'] = 'OR';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: UWorld migration data: Teleported date */
  $handler->display->display_options['filters']['teleported_time']['id'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['filters']['teleported_time']['field'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['operator'] = '>=';
  $handler->display->display_options['filters']['teleported_time']['group'] = 1;
  $handler->display->display_options['filters']['teleported_time']['exposed'] = TRUE;
  $handler->display->display_options['filters']['teleported_time']['expose']['operator_id'] = 'teleported_time_op';
  $handler->display->display_options['filters']['teleported_time']['expose']['label'] = 'Migrated start date';
  $handler->display->display_options['filters']['teleported_time']['expose']['operator'] = 'teleported_time_op';
  $handler->display->display_options['filters']['teleported_time']['expose']['identifier'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: UWorld migration data: Teleported date */
  $handler->display->display_options['filters']['teleported_time_1']['id'] = 'teleported_time_1';
  $handler->display->display_options['filters']['teleported_time_1']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['filters']['teleported_time_1']['field'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time_1']['operator'] = '<=';
  $handler->display->display_options['filters']['teleported_time_1']['group'] = 1;
  $handler->display->display_options['filters']['teleported_time_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['teleported_time_1']['expose']['operator_id'] = 'teleported_time_1_op';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['label'] = 'Migrated end date';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['operator'] = 'teleported_time_1_op';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['identifier'] = 'teleported_time_1';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: UWorld migration data: Has teleported field */
  $handler->display->display_options['filters']['teleported']['id'] = 'teleported';
  $handler->display->display_options['filters']['teleported']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['filters']['teleported']['field'] = 'teleported';
  $handler->display->display_options['filters']['teleported']['group'] = 1;
  $handler->display->display_options['filters']['teleported']['exposed'] = TRUE;
  $handler->display->display_options['filters']['teleported']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['teleported']['expose']['label'] = 'Has teleported';
  $handler->display->display_options['filters']['teleported']['expose']['operator'] = 'teleported_op';
  $handler->display->display_options['filters']['teleported']['expose']['identifier'] = 'teleported';
  $handler->display->display_options['filters']['teleported']['expose']['remember_roles'] = array(
    2 => '2',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'input_required' => 0,
      'text_input_required' => array(
        'text_input_required' => array(
          'value' => 'Select any filter and click on Apply to see results',
          'format' => 'filtered_html',
        ),
      ),
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'secondary_collapse_override' => '0',
    ),
    'teleported_time' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
        'datepicker_options' => '',
      ),
    ),
    'teleported_time_1' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
        'datepicker_options' => '',
      ),
    ),
    'teleported' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => 'IF you select No on this filter but you enter date you will get Teleported users, since dates are only applicable who those that has been teleported already.',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table_megarows';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'uid' => 'uid',
    'teleported' => 'teleported',
    'teleported_time' => 'teleported_time',
    'php' => 'php',
    'views_bulk_operations' => 'views_bulk_operations',
  );
  $handler->display->display_options['style_options']['class'] = '';
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'teleported' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'teleported_time' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'php' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['scroll_padding'] = '120';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'views_bootstrap_carousel_plugin_rows';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['filter_groups']['operator'] = 'OR';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: UWorld migration data: Teleported date */
  $handler->display->display_options['filters']['teleported_time']['id'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['filters']['teleported_time']['field'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['operator'] = '>=';
  $handler->display->display_options['filters']['teleported_time']['group'] = 1;
  $handler->display->display_options['filters']['teleported_time']['exposed'] = TRUE;
  $handler->display->display_options['filters']['teleported_time']['expose']['operator_id'] = 'teleported_time_op';
  $handler->display->display_options['filters']['teleported_time']['expose']['label'] = 'Migrated start date';
  $handler->display->display_options['filters']['teleported_time']['expose']['operator'] = 'teleported_time_op';
  $handler->display->display_options['filters']['teleported_time']['expose']['identifier'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: UWorld migration data: Teleported date */
  $handler->display->display_options['filters']['teleported_time_1']['id'] = 'teleported_time_1';
  $handler->display->display_options['filters']['teleported_time_1']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['filters']['teleported_time_1']['field'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time_1']['operator'] = '<=';
  $handler->display->display_options['filters']['teleported_time_1']['group'] = 1;
  $handler->display->display_options['filters']['teleported_time_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['teleported_time_1']['expose']['operator_id'] = 'teleported_time_1_op';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['label'] = 'Migrated end date';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['operator'] = 'teleported_time_1_op';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['identifier'] = 'teleported_time_1';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: UWorld migration data: Has teleported field */
  $handler->display->display_options['filters']['teleported']['id'] = 'teleported';
  $handler->display->display_options['filters']['teleported']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['filters']['teleported']['field'] = 'teleported';
  $handler->display->display_options['filters']['teleported']['value'] = 'All';
  $handler->display->display_options['filters']['teleported']['group'] = 2;
  $handler->display->display_options['filters']['teleported']['exposed'] = TRUE;
  $handler->display->display_options['filters']['teleported']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['teleported']['expose']['label'] = 'Has teleported';
  $handler->display->display_options['filters']['teleported']['expose']['operator'] = 'teleported_op';
  $handler->display->display_options['filters']['teleported']['expose']['identifier'] = 'teleported';
  $handler->display->display_options['filters']['teleported']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['path'] = 'admin/reports/migration-details';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    12 => '12',
  );
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['filter_groups']['operator'] = 'OR';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: UWorld migration data: Teleported date */
  $handler->display->display_options['filters']['teleported_time']['id'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['filters']['teleported_time']['field'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['operator'] = '>=';
  $handler->display->display_options['filters']['teleported_time']['value']['value'] = '-1 week';
  $handler->display->display_options['filters']['teleported_time']['value']['type'] = 'offset';
  $handler->display->display_options['filters']['teleported_time']['group'] = 1;
  $handler->display->display_options['filters']['teleported_time']['expose']['operator_id'] = 'teleported_time_op';
  $handler->display->display_options['filters']['teleported_time']['expose']['label'] = 'Migrated start date';
  $handler->display->display_options['filters']['teleported_time']['expose']['operator'] = 'teleported_time_op';
  $handler->display->display_options['filters']['teleported_time']['expose']['identifier'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: UWorld migration data: Teleported date */
  $handler->display->display_options['filters']['teleported_time_1']['id'] = 'teleported_time_1';
  $handler->display->display_options['filters']['teleported_time_1']['table'] = 'enroll_flow_uw_teleport';
  $handler->display->display_options['filters']['teleported_time_1']['field'] = 'teleported_time';
  $handler->display->display_options['filters']['teleported_time_1']['operator'] = '<=';
  $handler->display->display_options['filters']['teleported_time_1']['value']['value'] = '+1 day';
  $handler->display->display_options['filters']['teleported_time_1']['group'] = 1;
  $handler->display->display_options['filters']['teleported_time_1']['expose']['operator_id'] = 'teleported_time_1_op';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['label'] = 'Migrated end date';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['operator'] = 'teleported_time_1_op';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['identifier'] = 'teleported_time_1';
  $handler->display->display_options['filters']['teleported_time_1']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['path'] = 'admin/reports/migration-details.csv';
  $handler->display->display_options['displays'] = array(
    'default' => 0,
    'page' => 0,
  );

  // Add view to list of views to provide.
  $views [$view->name] = $view;


  // At the end, return array of default views.
  return $views;
}
