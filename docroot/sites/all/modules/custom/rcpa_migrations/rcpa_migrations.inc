<?php

abstract class BaseRcpaMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    /** general options for all migrations */
    ini_set('auto_detect_line_endings', TRUE);
  }
}
