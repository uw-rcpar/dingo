<?php

class rcpar_commerce_handler_field_rcpar_taxed_authnet_order_part extends commerce_payment_handler_field_amount {
  function label() {
    if (!isset($this->options['label'])) {
      return $this->ui_name();
    }
    return $this->options['label'];
  }
  
  function query() {
    $this->ensure_my_table();

    if (isset($this->aliases['rcpar_authnet_taxed'])){
      $this->field_alias = $this->aliases['rcpar_authnet_taxed'];
    } else {
      $this->field_alias = 'rcpar_authnet_taxed';
    }

    $sql = "
      IF(transaction_status IN('Settled Successfully','Credited'),
        (SELECT SUM(t.taxed) as taxable
        FROM rcpar_commerce_reports_tax t
        WHERE t.order_id = " . $this->table_alias . ".order_id)
      ,0)
    ";
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    if (method_exists($this->query, 'add_field')) {
      $this->field_alias = $this->query->add_field('', $sql, $this->field_alias, $params);
    }
  }
}
