<?php

/**
 * Implements hook_views_data().
 */
function rcpar_authnet_report_temp_views_data() {
    $data = array();


    // Base information.
    $data['rcpar_authnet_temp']['table']['group'] = t('RCPAR Authorize.net temp');

    $data['rcpar_authnet_temp']['table']['base'] = array(
        'field'            => 'transaction_id',
        'title'            => t('RCPAR Commerce Tax Information'),
        'help'             => t('The tax information gathered by RCPAR Commerce Tax Reports.'),
        'access query tag' => 'commerce_reports_access',
    );

    $data['rcpar_authnet_temp']['transaction_status'] = array(
        'title' => t('Transaction Status'),
        'help'  => t('Imported transaction status.'),
        'field' => array(
            'handler'        => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );


    $data['rcpar_authnet_temp']['settlement_amount'] = array(
        'title' => t('Settlement Amount'),
        'help'  => t('Imported settlement amount.'),
        'field' => array(
            'handler'        => 'commerce_payment_handler_field_amount',
            'click sortable' => TRUE,
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );
    $data['rcpar_authnet_temp']['authorization_amount'] = array(
        'title' => t('Authorization Amount'),
        'help'  => t('Imported authorization amount.'),
        'field' => array(
            'handler'        => 'commerce_payment_handler_field_amount',
            'click sortable' => TRUE,
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );
    $data['rcpar_authnet_temp']['total_amount'] = array(
        'title' => t('Total Amount'),
        'help'  => t('Imported total amount.'),
        'field' => array(
            'handler'        => 'commerce_payment_handler_field_amount',
            'click sortable' => TRUE,
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );
    $data['rcpar_authnet_temp']['approved_amount'] = array(
        'title' => t('Approved amount'),
        'help'  => t('Imported approved amount.'),
        'field' => array(
            'handler'        => 'commerce_payment_handler_field_amount',
            'click sortable' => TRUE,
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );


    $data['commerce_order']['rcpar_taxable_amount_2'] = array(
        'title' => t('RCPAR - Authnet Taxable'),
        'help' => t('RCPAR order taxable part if the order was correctly processed, 0 in other cases'),
        'field' => array(
            'handler' => 'rcpar_commerce_handler_field_rcpar_taxable_authnet_order_part',
            'click sortable' => TRUE,
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );

    $data['commerce_order']['rcpar_taxed_amount_2'] = array(
        'title' => t('RCPAR - Authnet Taxed'),
        'help' => t('RCPAR order taxed part if the order was correctly processed, 0 in other cases'),
        'field' => array(
            'handler' => 'rcpar_commerce_handler_field_rcpar_taxed_authnet_order_part',
            'click sortable' => TRUE,
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );

    $data['commerce_order']['rcpar_total_amount_2'] = array(
        'title' => t('RCPAR - Authnet Total'),
        'help' => t('RCPAR order total amount if the order was correctly processed, 0 in other cases'),
        'field' => array(
            'handler' => 'rcpar_commerce_handler_field_rcpar_total_authnet_order_part',
            'click sortable' => TRUE,
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );

    $data['rcpar_authnet_temp']['transaction_status'] = array(
        'title' => t('Authnet Transaction Status'),
        'help'  => t('Authorize.net order transaction status.'),
        'field' => array(
            'handler'        => 'views_handler_field',
            'click sortable' => TRUE,
        ),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
        ),
        'sort'  => array(
            'handler' => 'views_handler_sort',
        ),
    );

    return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function rcpar_authnet_report_temp_views_data_alter(&$data) {
    // Define relationship from commerce_order to commerce_payment_transaction.
    /*
    $data['commerce_product']['table']['join']['rcpar_commerce_reports_tax'] = array(
        'left_field' => 'product_id',
        'field'      => 'product_id',
    );
    */

    $data['commerce_payment_transaction']['rcpar_authnet_temp']['relationship'] = array(
        'title'      => t('RCPAR Authorize.net temp'),
        'help'       => t("Relate this payment to its Authorize.net data, if exists and it's loaded into drupal."),
        'handler'    => 'views_handler_relationship',
        'base'       => 'rcpar_authnet_temp',
        'base field' => 'transaction_id',
        'field'      => 'remote_id',
        'label'      => t('Authnet data', array(),
            array('context' => 'a authnet imported record')),
    );
}


function rcpar_authnet_report_temp_views_query_alter(&$view, &$query){
    // on this particular view, views gets confused on this field and tries to use a
    // field that doesn't exists, making the query fail
    // we fix setting the correct name by hand here:
    if ($view->name == 'ca_quarterly_tax_authnet'){
        $pattern = '/commerce_order\.order_id(\d+)/';
        $subs = 'commerce_order.order_id';
        $query->table_queue['commerce_payment_transaction_commerce_order']['join']->left_query = preg_replace($pattern, $subs, $query->table_queue['commerce_payment_transaction_commerce_order']['join']->left_query);
    }
}
