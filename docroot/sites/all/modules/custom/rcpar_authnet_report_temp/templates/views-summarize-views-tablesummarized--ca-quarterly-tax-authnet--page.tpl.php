<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */

    drupal_add_js(drupal_get_path('module', 'rcpar_mods') . '/js/reports.js');
    drupal_add_css(drupal_get_path('module', 'rcpar_mods') . '/css/reports.css');
?>

<table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
    <?php if (!empty($title)) : ?>
        <caption><?php print $title; ?></caption>
    <?php endif; ?>
    <thead>
    <tr>
        <?php foreach ($header as $field => $label): ?>
            <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
                <?php print $label; ?>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <?php if (empty($summary_only)): ?>
        <tbody>
        <?php foreach ($rows as $count => $row): ?>

            <?php
                if ($row['rcpar_total_amount_2'] == '$0.00' && $row['rcpar_taxable_amount_2'] == '$0.00' && $row['rcpar_taxed_amount_2'] == '$0.00'){
                    continue;
                }
            ?>

            <tr class="ca-quaterly-table collapser <?php print implode(' ', $row_classes[$count]); ?>">
                <?php foreach ($row as $field => $content): ?>
                    <td <?php if ($field_classes[$field][$count]) { print 'class="'. $field_classes[$field][$count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$count]); ?>>
                        <?php print $content; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
            <tr  class=" ca-quaterly-table collapsable <?php print implode(' ', $row_classes[$count]); ?>">
                <td colspan="<?php echo count($row); ?>">
                    <?php

                    $orders_view = views_get_view('ca_quarterly_tax_authnet', true);
                    $display = 'block_1';
                    $orders_view->set_arguments(array($row['commerce_customer_address_locality']));

                    // we need to manualy set values for this filter, because it
                    // doesn't get populated automatically using the url
                    $input = $view->get_exposed_input();
                    $filter = $orders_view->get_item('default', 'filter', 'date_filter');
                    if (isset($input['date_filter']['min']['date'])) {
                        $filter['value']['min'] = (date('Y-m-d', strtotime($input['date_filter']['min']['date'])));
                    }
                    if (isset($input['date_filter']['max']['date'])) {
                        $filter['value']['max'] = (date('Y-m-d', strtotime($input['date_filter']['max']['date'])));
                    }
                    $orders_view->set_item('default', 'filter', 'date_filter', $filter);

                    $filter = $orders_view->get_item('default', 'filter', 'state');
                    if (isset($input['state'])) {
                        $vals = array();
                        foreach($input['state'] as $s){
                            $vals[$s] = $s;
                        }
                        $filter['value'] = $vals;
                    }
                    $orders_view->set_item('block_1', 'filter', 'state', $filter);

                    $filter = $orders_view->get_item('default', 'filter', 'field_direct_bill_value');

                    if (isset($input['field_direct_bill_value'])) {
                        if($input['field_direct_bill_value'] == 'All'){
                            unset($orders_view->display['default']->display_options['filters']['field_direct_bill_value']);
                            unset($orders_view->display['default']->handler->options['filters']['field_direct_bill_value']);
                        } else {
                            $filter['value'] = array($input['field_direct_bill_value'] => $input['field_direct_bill_value']);
                            $orders_view->set_item('default', 'filter', 'field_direct_bill_value', $filter);
                        }
                    }

                    $orders_view->build($display);
                    $content = $orders_view->render($display);
                    echo $content;
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php endif; ?>
    <tfoot>
    <tr class="summary">
        <?php foreach ($header as $field => $label): ?>
            <td><?php if (!empty($summarized[$field])) { echo $summarized[$field]; } ?></td>
        <?php endforeach; ?>
    </tr>
    </tfoot>
</table>


<h3>Refunds</h3>
<?php
    $orders_view = views_get_view('ca_quarterly_tax_authnet', true);
    $display = 'block_2';

    // we need to manualy set values for this filter, because it
    // doesn't get populated automatically using the url
    $input = $view->get_exposed_input();
    $filter = $orders_view->get_item('block_2', 'filter', 'date_filter');
    if (isset($input['date_filter']['min']['date'])) {
        $filter['value']['min'] = (date('Y-m-d', strtotime($input['date_filter']['min']['date'])));
    }
    if (isset($input['date_filter']['max']['date'])) {
        $filter['value']['max'] = (date('Y-m-d', strtotime($input['date_filter']['max']['date'])));
    }

    $orders_view->set_item('block_2', 'filter', 'date_filter', $filter);

    $filter = $orders_view->get_item('block_2', 'filter', 'state');
    if (isset($input['state'])) {
        $vals = array();
        foreach($input['state'] as $s){
            $vals[$s] = $s;
        }
        $filter['value'] = $vals;
    }
    $orders_view->set_item('block_2', 'filter', 'state', $filter);

    $filter = $orders_view->get_item('block_2', 'filter', 'field_direct_bill_value');

    if (isset($input['field_direct_bill_value'])) {
        if($input['field_direct_bill_value'] == 'All'){
            unset($orders_view->display['default']->display_options['filters']['field_direct_bill_value']);
            unset($orders_view->display['default']->handler->options['filters']['field_direct_bill_value']);
        } else {
            $filter['value'] = array($input['field_direct_bill_value'] => $input['field_direct_bill_value']);
            $orders_view->set_item('block_2', 'filter', 'field_direct_bill_value', $filter);
        }
    }

    $orders_view->build($display);
    $content = $orders_view->render($display);
    echo $content;
?>
