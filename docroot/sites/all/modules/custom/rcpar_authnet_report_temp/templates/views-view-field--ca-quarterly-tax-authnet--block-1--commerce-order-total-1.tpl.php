<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php
    $taxable = $row->rcpar_taxable_1;
    $taxed   = $row->rcpar_authnet_taxed;

    $real_tax_rate = '0%';
    if ($taxed){
        $real_tax_rate = $taxed / $taxable;
        $real_tax_rate *= 100;
        $real_tax_rate .= '%';
    }

    try {
        $order = $row->_field_data['order_id_1']['entity'];
        $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
        $comp = $order_wrapper->commerce_order_total->data->value();
        $tax_rate = '0%';

        foreach($comp['components'] as $c){
            if ($c['name'] == 'tax|california_tax_9'){
                $tax_rate = $c['price']['data']['tax_rate']['rate'];
                $tax_rate *= 100;
                $tax_rate .= '%';
            }
        }
    } catch (EntityMetadataWrapperException $ex) {
        watchdog('rcpar_authnet_report_temp', t("There was a problem loading tax info: ". $ex->getMessage()), array(), WATCHDOG_ERROR);
    }


    if ($tax_rate){
        print $tax_rate;
    }

    if ($real_tax_rate){
        print ' real: '. $real_tax_rate;
    }
?>
