<?php

/**
 * Build the table of tax information.
 * same as _commerce_reports_tax_generate
 * but catching EntityMetadataWrapperException to avoid blocking on processing
 * the orders
 */
function _rcpar_commerce_reports_tax_generate_ctt($order_id) {
  if(is_int($order_id)){
    db_delete('rcpar_commerce_reports_tax')
    ->condition('order_id', $order_id)
    ->execute();
    $order = commerce_order_load($order_id);
    rcpar_commerce_views_order_save($order);
  }
}

/**
 * Finishes the batch process and stores any remaining information.
 */
function _rcpar_commerce_reports_tax_finished_ctt($success, $results, $operations) {
    if ($success) {        
        drupal_set_message(t('Successfully generated tax report.'));
    }
    else {
        drupal_set_message(t('There was a problem whilst generating the tax report. The information might be incomplete or erroneous.'), 'error');
    }
}
