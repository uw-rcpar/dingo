<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */

    drupal_add_js(drupal_get_path('module', 'rcpar_mods') . '/js/reports.js');
    drupal_add_css(drupal_get_path('module', 'rcpar_mods') . '/css/reports.css');
?>

<table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
    <?php if (!empty($title)) : ?>
        <caption><?php print $title; ?></caption>
    <?php endif; ?>
    <thead>
    <tr>
        <?php foreach ($header as $field => $label): ?>
            <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
                <?php print $label; ?>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <?php if (empty($summary_only)): ?>
        <tbody>

        <?php foreach ($rows as $count => $row): ?>
            <tr class="  <?php print implode(' ', $row_classes[$count]); ?>">
                <?php foreach ($row as $field => $content): ?>

                    <td <?php if ($field_classes[$field][$count]) { print 'class="'. $field_classes[$field][$count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$count]); ?>>
                        <?php print $content; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php endif; ?>
    <tfoot>
    <tr class="summary">

        <?php
            $summarized_fields = array(
                'quantity' => 'commerce_line_item_quantity',
                'taxed' => 'taxed',
                'total_no_tax' => 'rcpar_commerce_reports_tax_commerce_line_item_total_no_tax',
                'total' => 'rcpar_commerce_reports_tax_commerce_line_item_total',

                'shipping' => 'rcpar_commerce_reports_tax_commerce_line_item_shipping_part'
            );

            $summarized_values = array(
                'quantity' => 0,
                'taxed' => 0,
                'total_no_tax' => 0,
                'total' => 0,

                'shipping' => 0,
                'total_inc_shipping' => 0
            );
            // we need to get the total shipping
            // and the total with shipping
            // and the agregated values for some of the values
            $total_shipping = 0;
            $total_inc_shipping = 0;
            $total_no_tax = 0;
            foreach($view->result as $r){
                foreach($summarized_fields as $field_title => $field_name){
                    $summarized_values[$field_title] += $r->{$field_name};
                }
            }
            $summarized_values['total_inc_shipping'] = $summarized_values['total'] + $summarized_values['shipping'];
        ?>
        <?php foreach ($header as $field => $label): ?>

            <td><?php
                if (!empty($summarized_fields[$field])) {
                    echo '<div class="total">Total:</div>';
                    if ($field != 'quantity'){
                        echo commerce_currency_format($summarized_values[$field ], 'USD');
                    } else {
                        echo $summarized_values[$field ];
                    }
                }
                if ($field == 'total'){
                    echo '<div class="total">Total Shipping:</div>';
                    echo commerce_currency_format($summarized_values['shipping'], 'USD');

                    echo '<div class="total">Total + Shipping:</div>';
                    echo commerce_currency_format($summarized_values['total_inc_shipping'], 'USD');
                }
                ?></td>
        <?php endforeach; ?>
    </tr>
    </tfoot>
</table>

<h3>Refunds</h3>
<?php
    $orders_view = views_get_view('rcpar_product_total_report', true);
    $display = 'block_2';

    // we need to manually set values for the date and sku filters, because they
    // doesn't get populated automatically using the url
    $input = $view->get_exposed_input();
    $filter = $orders_view->get_item('block_2', 'filter', 'date_filter');

    if (isset($input['date_filter_1']['min']['date'])) {
        $filter['value']['min'] = (date('Y-m-d', strtotime($input['date_filter_1']['min']['date'])));
    }
    if (isset($input['date_filter_1']['max']['date'])) {
        $filter['value']['max'] = (date('Y-m-d', strtotime($input['date_filter_1']['max']['date'])));
    }
    $orders_view->set_item('block_2', 'filter', 'date_filter', $filter);

    $filter = $orders_view->get_item('block_2', 'filter', 'sku');
    if (isset($input['sku'])) {
        $filter['value'] = $input['sku'];
    }
    $orders_view->set_item('block_2', 'filter', 'sku', $filter);


    $filter = $orders_view->get_item('block_2', 'filter', 'payment_method');

    if (isset($input['payment_method'])) {
        $filter = $orders_view->get_item('block_2', 'filter', 'payment_method');

        if (isset($input['payment_method'])) {
            $vals = array();
            foreach($input['payment_method'] as $s){
                $vals[$s] = $s;
            }
            $filter['value'] = $vals;
        }
        $orders_view->set_item('block_2', 'filter', 'payment_method', $filter);
    }

    $filter = $orders_view->get_item('block_2', 'filter', 'commerce_customer_address_country_international');
    if (isset($input['commerce_customer_address_country_international'])) {
        if ($input['commerce_customer_address_country_international'] != 'All') {
            $filter['value'] = array($input['commerce_customer_address_country_international'] => $input['commerce_customer_address_country_international']);
        } else {
            $filter['value'] = array();
        }
    }
    $orders_view->set_item('block_2', 'filter', 'commerce_customer_address_country_international', $filter);

    $orders_view->build($display);
    $content = $orders_view->render($display);
    echo $content;
?>


<div class="ca-quaterly-table collapser">
    <h3><a href="#">Show/hide Order list</a></h3>
</div>
<div class="ca-quaterly-table collapsable">

    <?php
        $orders_view = views_get_view('rcpar_product_total_report', true);
        $display = 'block_1';

        // we need to manually set values for the date and sku filters, because they
        // doesn't get populated automatically using the url
        $input = $view->get_exposed_input();
        $filter = $orders_view->get_item('block_1', 'filter', 'date_filter');

        if (isset($input['date_filter_1']['min']['date'])) {
            $filter['value']['min'] = (date('Y-m-d', strtotime($input['date_filter_1']['min']['date'])));
        }
        if (isset($input['date_filter_1']['max']['date'])) {
            $filter['value']['max'] = (date('Y-m-d', strtotime($input['date_filter_1']['max']['date'])));
        }
        if (isset($input['date_filter_1']['min']['date']) || isset($input['date_filter_1']['max']['date'])){
            $orders_view->set_item('block_1', 'filter', 'date_filter', $filter);
        }

        $filter = $orders_view->get_item('block_1', 'filter', 'sku');
        if (isset($input['sku'])) {
            $filter['value'] = $input['sku'];
        }
        $orders_view->set_item('block_1', 'filter', 'sku', $filter);


        $filter = $orders_view->get_item('block_1', 'filter', 'payment_method');

        if (isset($input['payment_method'])) {
            $filter = $orders_view->get_item('block_1', 'filter', 'payment_method');

            if (isset($input['payment_method'])) {
                $vals = array();
                foreach($input['payment_method'] as $s){
                    $vals[$s] = $s;
                }
                $filter['value'] = $vals;
            }
            $orders_view->set_item('block_1', 'filter', 'payment_method', $filter);
        }

        $filter = $orders_view->get_item('block_1', 'filter', 'commerce_customer_address_country_international');
        if (isset($input['commerce_customer_address_country_international'])) {
            if ($input['commerce_customer_address_country_international'] != 'All') {
                $filter['value'] = array($input['commerce_customer_address_country_international'] => $input['commerce_customer_address_country_international']);
            } else {
                $filter['value'] = array();
            }
        }
        $orders_view->set_item('block_1', 'filter', 'commerce_customer_address_country_international', $filter);

        $orders_view->build($display);
        $content = $orders_view->render($display);
        echo $content;
    ?>

</div>

