<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */

    drupal_add_js(drupal_get_path('module', 'rcpar_mods') . '/js/reports.js');
    drupal_add_css(drupal_get_path('module', 'rcpar_mods') . '/css/reports.css');
?>

<table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
    <?php if (!empty($title)) : ?>
        <caption><?php print $title; ?></caption>
    <?php endif; ?>
    <thead>
    <tr>
        <?php foreach ($header as $field => $label): ?>
            <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
                <?php print $label; ?>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <?php if (empty($summary_only)): ?>
        <tbody>

        <?php foreach ($rows as $count => $row): ?>
            <tr class="ca-quaterly-table collapser <?php print implode(' ', $row_classes[$count]); ?>">
                <?php foreach ($row as $field => $content): ?>

                    <td <?php if ($field_classes[$field][$count]) { print 'class="'. $field_classes[$field][$count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$count]); ?>>
                        <?php print $content; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php endif; ?>
    <tfoot>
    <tr class="summary">
        <?php
            // we need to get the total shipping
            // and the total with shipping
            $total_shipping = 0;
            $total_inc_shipping = 0;
            foreach($view->result as $r){
                $total_shipping  += $r->rcpar_commerce_reports_tax_commerce_line_item_shipping_part;
                $total_inc_shipping  += $r->rcpar_commerce_reports_tax_commerce_line_item_total;
            }
            $total_inc_shipping += $total_shipping;
        ?>
        <?php foreach ($header as $field => $label): ?>

            <td><?php
                if (!empty($summarized[$field])) {
                    echo $summarized[$field];
                }
                if ($field == 'total'){
                    echo '<div class="total">Total Shipping</div>';
                    echo commerce_currency_format($total_shipping, 'USD');

                    echo '<div class="total">Total + Shipping</div>';
                    echo commerce_currency_format($total_inc_shipping, 'USD');
                }
                ?></td>
        <?php endforeach; ?>
    </tr>
    </tfoot>
</table>
