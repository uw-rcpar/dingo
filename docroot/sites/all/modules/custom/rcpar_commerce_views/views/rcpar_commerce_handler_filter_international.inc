<?php

class rcpar_commerce_handler_filter_international extends addressfield_views_handler_filter_country {

    function get_value_options() {
        $field = field_info_field($this->definition['field_name']);
        $value_options = list_allowed_values($field);

        $value_options['US'] = t('USA');
        $value_options['NON-US'] = t('Non USA');

        $this->value_options = $value_options;
    }

    function query() {
        $this->ensure_my_table();
        $info = $this->operators();

        // special case
        if (count($this->value) == 1 && reset($this->value) == 'NON-US') {
            $operator = '<>';
            $value = 'US';
            $placeholder = $this->placeholder();
            $this->query->add_where_expression($this->options['group'], "$this->table_alias.$this->real_field $operator $placeholder ", array($placeholder => $value));
        } else { // Default behaviour
            if (!empty($info[$this->operator]['method'])) {
                $this->{$info[$this->operator]['method']}();
            }
        }
    }
}

