<?php

/**
 * Implements hook_views_data().
 */
function rcpar_commerce_views_views_data() {
  $data = array();

  // Base information.
  $data['rcpar_commerce_reports_tax']['table']['group'] = t('RCPAR Commerce Tax Reports');

  $data['rcpar_commerce_reports_tax']['table']['base'] = array(
    'field'            => 'product_id',
    'title'            => t('RCPAR Commerce Tax Information'),
    'help'             => t('The tax information gathered by RCPAR Commerce Tax Reports.'),
    'access query tag' => 'commerce_reports_access',
  );

  $data['rcpar_commerce_reports_tax']['tax_rate'] = array(
    'title' => t('Tax rate'),
    'help'  => t('The applied tax rate.'),
    'field' => array(
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort'  => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rcpar_commerce_reports_tax']['tax_rate_display_name'] = array(
    'title' => t('Tax rate display name'),
    'help'  => t('The tax rate display name.'),
    'field' => array(
      'handler'        => 'commerce_reports_tax_handler_field_tax_rate_display_name',
      'click sortable' => TRUE,
    ),
    'sort'  => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the Order ID.
  $data['rcpar_commerce_reports_tax']['order_id'] = array(
    'title'        => t('Order ID', array(),
      array('context' => 'a drupal commerce order')),
    'help'         => t('The unique internal identifier of the order.'),
    'field'        => array(
      'handler'        => 'commerce_order_handler_field_order',
      'click sortable' => TRUE,
    ),
    'filter'       => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort'         => array(
      'handler' => 'views_handler_sort',
    ),
    'argument'     => array(
      'handler'       => 'commerce_order_handler_argument_order_order_id',
      'name field'    => 'order_number',
      'numeric'       => TRUE,
      'validate type' => 'order_id',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base'    => 'commerce_order',
      'field'   => 'order_id',
      'label'   => t('Order', array(),
        array('context' => 'a drupal commerce order')),
    ),
  );

  // Expose the Product ID.
  $data['rcpar_commerce_reports_tax']['product_id'] = array(
      'title'        => t('Product ID', array(),
          array('context' => 'a drupal commerce product')),
      'help'         => t('The unique internal identifier of the product.'),
      'field'        => array(
          'handler'        => 'commerce_product_handler_field_product',
          'click sortable' => TRUE,
      ),
      'filter'       => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort'         => array(
          'handler' => 'views_handler_sort',
      ),
      'argument'     => array(
          'handler'       => 'commerce_product_handler_argument_product_id',
          'name field'    => 'product_id',
          'numeric'       => TRUE,
          'validate type' => 'product_id',
      ),
      'relationship' => array(
          'handler' => 'views_handler_relationship',
          'base'    => 'commerce_product',
          'field'   => 'product_id',
          'label'   => t('Order', array(),
              array('context' => 'a drupal commerce product')),
      ),
  );

  // Expose the Product ID.
  $data['rcpar_commerce_reports_tax']['order_line_id'] = array(
      'title'        => t('Order Line ID', array(),
          array('context' => 'a drupal commerce order line')),
      'help'         => t('The unique internal identifier of a order line.'),
      'field'        => array(
          'handler'        => 'commerce_line_item_handler_field_line_item_title',
          'click sortable' => TRUE,
      ),
      'filter'       => array(
          'handler' => 'views_handler_filter_numeric',
      ),
      'sort'         => array(
          'handler' => 'views_handler_sort',
      ),
      'argument'     => array(
          'handler'       => 'commerce_line_item_handler_argument_line_item_line_item_id',
          'name field'    => 'product_id',
          'numeric'       => TRUE,
          'validate type' => 'product_id',
      ),
      'relationship' => array(
          'handler' => 'views_handler_relationship',
          'base'    => 'commerce_line_item',
          'field'   => 'line_item_id',
          'label'   => t('Order', array(),
              array('context' => 'a drupal commerce product')),
      ),
  );

  // Expose the transaction currency.
  $data['rcpar_commerce_reports_tax']['currency_code'] = array(
    'title'    => t('Currency'),
    'help'     => t('The currency of the transaction.'),
    'field'    => array(
      'handler'        => 'commerce_payment_handler_field_currency_code',
      'click sortable' => TRUE,
    ),
    'filter'   => array(
      'handler' => 'commerce_payment_handler_filter_currency_code',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rcpar_commerce_reports_tax']['taxable'] = array(
    'title' => t('Taxable'),
    'help'  => t('The taxable amount on the order.'),
    'field' => array(
      'handler'        => 'commerce_payment_handler_field_amount',
      'click sortable' => TRUE,
    ),
    'sort'  => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rcpar_commerce_reports_tax']['taxed'] = array(
    'title' => t('Taxed'),
    'help'  => t('The taxed amount on the order.'),
    'field' => array(
      'handler'        => 'commerce_payment_handler_field_amount',
      'click sortable' => TRUE,
    ),
    'sort'  => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rcpar_commerce_reports_tax']['total'] = array(
      'title' => t('Total (discounts applied)'),
      'help'  => t('The price of the product (with discounts applied).'),
      'field' => array(
          'handler'        => 'commerce_payment_handler_field_amount',
          'click sortable' => TRUE,
      ),
      'sort'  => array(
          'handler' => 'views_handler_sort',
      ),
  );

  $data['rcpar_commerce_reports_tax']['total_no_tax'] = array(
      'title' => t('Total (no taxes)'),
      'help'  => t('The price of the product (with discounts applied) with no taxes applied.'),
      'field' => array(
          'handler'        => 'commerce_payment_handler_field_amount',
          'click sortable' => TRUE,
      ),
      'sort' => array(
          'handler' => 'views_handler_sort',
      ),
  );

  $data['rcpar_commerce_reports_tax']['taxed'] = array(
      'title' => t('Taxed'),
      'help'  => t('The taxed amount on the order.'),
      'field' => array(
          'handler'        => 'commerce_payment_handler_field_amount',
          'click sortable' => TRUE,
      ),
      'sort'  => array(
          'handler' => 'views_handler_sort',
      ),
  );

  $data['rcpar_commerce_reports_tax']['shipping_part'] = array(
      'title' => t('Shipping (item)'),
      'help'  => t('The shipping value per shippable item.'),
      'field' => array(
          'handler'        => 'commerce_payment_handler_field_amount',
          'click sortable' => TRUE,
      ),
      'sort'  => array(
          'handler' => 'views_handler_sort',
      ),
  );

  $data['commerce_order']['rcpar_taxable_amount'] = array(
    'title' => t('RCPAR Taxable'),
    'help' => t('RCPAR order taxable part (similar to Commerce Tax Reports: Taxable.'),
    'field' => array(
        'handler' => 'rcpar_commerce_handler_field_rcpar_taxable_order_part',
        'click sortable' => TRUE,
    ),
    'sort'  => array(
        'handler' => 'views_handler_sort',
    ),
  );

  $data['commerce_order']['rcpar_taxed_amount'] = array(
    'title' => t('RCPAR Taxed'),
    'help' => t('RCPAR order taxed part (similar to Commerce Tax Reports: Taxed).'),
    'field' => array(
        'handler' => 'rcpar_commerce_handler_field_rcpar_taxed_order_part',
        'click sortable' => TRUE,
    ),
    'sort'  => array(
        'handler' => 'views_handler_sort',
    ),
  );
  
  $data['commerce_order']['rcpar_taxed_refund'] = array(
    'title' => t('RCPAR Taxed Refund'),
    'help' => t('RCPAR order taxed part refunded from the order.'),
    'field' => array(
        'handler' => 'rcpar_commerce_handler_field_taxed_refund',
        'click sortable' => TRUE,
    ),
    'sort'  => array(
        'handler' => 'views_handler_sort',
    ),
  );

  
  $data['rcpar_line_item']['table']['group'] = t('Rcpar Line Item Data');
  $data['rcpar_line_item']['table']['join'] = array(
    '#global' => array(),
  );
  
  //this handler will show the product price when the user originally bought it
  $data['rcpar_line_item']['price'] = array(
    'title' => t('RCPAR Product Price'),
    'help' => t('Calculate the original price of the product, at the moment the order was created.'),
    'field' => array(
        'handler' => 'rcpar_commerce_handler_rcpar_product_version_price',
        'click sortable' => TRUE,
    ),
    'sort'  => array(
        'handler' => 'views_handler_sort',
    ),
  );
  
  //this handler will show the discount applied per line item
  $data['rcpar_line_item']['discount'] = array(
    'title' => t('RCPAR Order Discount'),
    'help' => t('Discount applied to a line item when the product was buy it'),
    'field' => array(
        'handler' => 'rcpar_commerce_handler_rcpar_product_version_discount',
        'click sortable' => TRUE,
    ),
    'sort'  => array(
        'handler' => 'views_handler_sort',
    ),
  );    
  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function rcpar_commerce_views_views_data_alter(&$data) {
  // Define relationship from commerce_order to commerce_payment_transaction.
  $data['commerce_product']['table']['join']['rcpar_commerce_reports_tax'] = array(
    'left_field' => 'product_id',
    'field'      => 'product_id',
  );

  $data['commerce_product']['payment_transaction']['relationship'] = array(
    'title'      => t('RCPAR Tax Reports'),
    'help'       => t("Relate this order to its tax reports. This relationship will cause duplicated records if there are multiple tax rates per order."),
    'handler'    => 'views_handler_relationship',
    'base'       => 'rcpar_commerce_reports_tax',
    'base field' => 'product_id',
    'field'      => 'product_id',
    'label'      => t('Tax', array(),
      array('context' => 'a drupal rcpar commerce tax report')),
  );

  // Define relationship from commerce_order to commerce_payment_transaction.
  $data['commerce_line_item']['table']['join']['rcpar_commerce_reports_tax'] = array(
      'left_field' => 'product_id',
      'field'      => 'product_id',
  );

  $data['commerce_line_item']['payment_transaction']['relationship'] = array(
      'title'      => t('RCPAR  Tax Reports'),
      'help'       => t("Relate this line item to its rcpar tax reports."),
      'handler'    => 'views_handler_relationship',
      'base'       => 'rcpar_commerce_reports_tax',
      'base field' => 'order_line_id',
      'field'      => 'line_item_id',
      'label'      => t('Tax', array(),
          array('context' => 'a drupal rcpar commerce tax report')),
  );

  $data['commerce_order']['payment_transactions']['relationship'] = array(
    'title'      => t('Payment Transaction - Order'),
    'help'       => t("Relate this order to its payment transactions. This relationship will cause duplicated records if there are multiple transactions per order."),
    'handler'    => 'views_handler_relationship',
    'base'       => 'commerce_payment_transaction',
    'base field' => 'order_id',
    'field'      => 'order_id',
    'label'      => t('Payment Transaction', array(),
        array('context' => 'a drupal rcpar payment transaction')),
  );



  $data['field_data_commerce_customer_address']['commerce_customer_address_country_international'] = $data['field_data_commerce_customer_address']['commerce_customer_address_country'];
  $data['field_data_commerce_customer_address']['commerce_customer_address_country_international']['title'] = 'Address - Country (filter USA - Non USA)';
  $data['field_data_commerce_customer_address']['commerce_customer_address_country_international']['filter']['handler'] = 'rcpar_commerce_handler_filter_international';
}


function rcpar_commerce_views_views_query_alter(&$view, &$query){

    // for some reason, the commerce_line_item shows some repeated entries for some products
    // eg: if you run the query "select * from commerce_line_item where order_id = 127347;"
    // you will see two item lines for the FAR product
    // however, if you do:
    //   $order = commerce_order_load(127347);
    //   dpm($order);
    // you will only see one item line for the FAR product (which will only be added once to the user that
    // did the purchase)
    // so what we are doing here is to pick the most recently added item line for each product on the orders
    // to resemble what you will get if you do the dpm($order);
    // please note that this is a workaround, what we should do, is to detect why are we
    // getting more than one item lime per product per order in the commerce_line_item table
    // but as the enrollment process is being reimplemented, and this report is needed ASAP
    // I recomend to leave this workarround until the new enrolment is ready and then check if this is
    // still necessary

    // TODO: check if this is still necessary after the new enrolment is implemented
    if($view->name == 'commerce_reports_products'){
      $query->add_where_expression('AND', "
        # custom where clause, added in rcpar_commerce_views_views_query_alter function
        NOT EXISTS (
         SELECT line_item_id FROM commerce_line_item cli
         WHERE
        cli.order_id = commerce_line_item.order_id AND
        cli.line_item_label = commerce_line_item.line_item_label AND
        cli.line_item_id > commerce_line_item.line_item_id
        )
      ");
    }



    // on this particular views, views gets confused on this field and tries to use a
    // field that doesn't exists, making the query fail
    // we fix setting the correct name by hand here:

    if ($view->name == 'ca_quarterly_tax_v2' || $view->name == 'rcpar_product_total_report' || $view->name == 'ca_use_tax_report' ){

        if ($view->name == 'ca_quarterly_tax_v2' || ($view->name == 'ca_use_tax_report' && $view->current_display != 'page')){
            $query->fields['currency_code']['field'] = 'commerce_payment_transaction_commerce_order.currency_code';
        }

        if ($view->name == 'rcpar_product_total_report' || $view->name == 'ca_use_tax_report'){
            $query->fields['currency_code']['field'] = 'rcpar_commerce_reports_tax_commerce_line_item.currency_code';
            $tables = array('commerce_payment_transaction_commerce_order', 'commerce_payment_transaction_commerce_order_1');
            foreach($tables as $t){
                if(isset($query->table_queue[$t])){
                    // wasn't able to make this approach work, TODO: find out why!
                    /*
                    $pattern = '/commerce_order\.order_id(\d+)/';
                    $subs = 'commerce_order_commerce_line_item.order_id';
                    $query->table_queue[$t]['join']->left_query = preg_replace($pattern, $subs, $query->table_queue[$t]['join']->left_query);
                    */

                    // on the block, it cames as commerce_order.order_id1 and in the page commerce_order.order_id
                    $query->table_queue[$t]['join']->left_query = str_replace('commerce_order.order_id2', 'commerce_order_commerce_line_item.order_id', $query->table_queue[$t]['join']->left_query);
                    $query->table_queue[$t]['join']->left_query = str_replace('commerce_order.order_id1', 'commerce_order_commerce_line_item.order_id', $query->table_queue[$t]['join']->left_query);
                    $query->table_queue[$t]['join']->left_query = str_replace('commerce_order.order_id', 'commerce_order_commerce_line_item.order_id', $query->table_queue[$t]['join']->left_query);
                }
            }
        }
        else if ($view->name == 'ca_quarterly_tax_v2'){
            $tables = array('commerce_payment_transaction_commerce_order', 'commerce_payment_transaction_commerce_order_1');
            foreach($tables as $t){
                if(isset($query->table_queue[$t]['join']->left_query)){
                    $pattern = '/commerce_order\.order_id(\d+)/';
                    $subs = 'commerce_order.order_id';
                    $query->table_queue[$t]['join']->left_query = preg_replace($pattern, $subs, $query->table_queue[$t]['join']->left_query);
                }
            }
        }

        // I swear that I tried to do this in a nicer way (setting a sub view in the relationship UI)
        // but for some reason, the generated query used the incorrect parameters, probably because of a bug
        if ($view->name == 'rcpar_product_total_report' && $view->current_display == 'block_2'){

            $table = 'commerce_payment_transaction_commerce_order_1';
            // for this block, we also need to do an extra tweak to avoid sql errors
            $query->table_queue[$table]['join']->left_query = str_replace('commerce_order.order_id', 'commerce_order_commerce_line_item.order_id', $query->table_queue['commerce_payment_transaction_commerce_order_1']['join']->left_query);
        } else {
            $table = 'commerce_payment_transaction_commerce_order';
        }

        $exposed_input = $view->get_exposed_input();

        $replacement = 'commerce_orderINNER ON commerce_payment_transactionINNER.order_id = commerce_orderINNER.order_id
              AND commerce_payment_transactionINNER.status = \'success\'
              ';

        if (isset($exposed_input['payment_method'])) {
            $payment_methods = $exposed_input['payment_method'];
        } else {
            $payment_methods = array('authnet_aim');
        }

        $methods = '';
        foreach($payment_methods as $payment_method){
            if ($methods != ''){
                $methods .= ' OR ';
            }
            $methods .= '
                (
                    commerce_payment_transactionINNER.payment_method = \''.$payment_method.'\'
                ';
            if ($payment_method == 'authnet_aim'){
                $methods .= '
                    AND
                    (
                        commerce_payment_transactionINNER.remote_status = \'capture\'
                        OR
                        commerce_payment_transactionINNER.remote_status = \'auth_capture\'
                    )';
            }
            $methods .= '
                )';
        }
        if ($methods != ''){
            $methods = ' AND ('. $methods.')';
        }

        $join_table = $query->table_queue[$table]['join']->left_table;

        $query->table_queue[$table]['join']->left_query = '

        SELECT commerce_payment_transactionINNER.transaction_id AS transaction_idINNER
        FROM
        {commerce_payment_transaction} commerce_payment_transactionINNER use index (order_id)
        WHERE (( (commerce_payment_transactionINNER.order_id = '.$join_table.'.order_id ) ))
        AND commerce_payment_transactionINNER.status = \'success\'
          '. $methods .'
        # ORDER BY commerce_payment_transactionINNER.remote_id DESC
        LIMIT 1 OFFSET 0';
    }
}

