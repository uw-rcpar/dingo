<?php

class rcpar_commerce_handler_filter_direct_bill extends views_handler_filter_field_list_boolean {

    function get_value_options() {
        $field = field_info_field($this->definition['field_name']);
        $value_options = list_allowed_values($field);

        $value_options[0] = t('No Direct Bill');
        $value_options[1] = t('Direct Bill');
        $this->value_options = $value_options;
    }

    // Default ensure_my_table method add some conditions on the join clause
    // that we don't want when we search for 'No Direct Bill'
    // so, we need to threat that case in a special way
    // for the rest of the cases, the default behaviour is ok
    function ensure_my_table() {
        // Defer to helper if the operator specifies it.
        $info = $this->operators();

        if ($this->operator == 'or') {
            if (count($this->value) == 1 && reset($this->value) == 0) {
                if (!isset($this->table_alias)) {
                    if (!method_exists($this->query, 'ensure_table')) {
                        vpr(t('Ensure my table called but query has no ensure_table method.'));
                        return;
                    }
                    $this->table_alias = $this->query->ensure_table($this->table, $this->relationship);
                }
                return $this->table_alias;
            }
        }
        return parent::ensure_my_table();
    }

    function query() {
        $this->ensure_my_table();
        $field = "$this->table_alias.$this->real_field";

        $info = $this->operators();

        // NOTE: we need the empty($this->options['reduce_duplicates']) to be enabled via the UI
        // to prevent the usage of inner join for this table (we need to use a left join)
        if ($this->operator == 'or' /*&& empty($this->options['reduce_duplicates'])*/) {
            if (count($this->value) == 1 && reset($this->value) == 0) {
                $value = reset($this->value);

                $clause = db_or();
                $clause->condition("$this->table_alias.$this->real_field", $value, "=")
                ->isNull("$this->table_alias.$this->real_field");
                //$this->query->add_where($this->options['group'], $clause);

                $payment_table = '';
                //  if we have the commerce_payment_transaction table
                if (isset($this->query)) {
                    foreach ($this->query->relationships as $table_name => $r) {
                        if (isset($r['base']) && $r['base'] == 'commerce_payment_transaction') {
                            $payment_table = $table_name;
                        }
                    }
                }
                if ($payment_table){
                    //commerce_payment_transaction_commerce_order
                    $clause = db_or();
                    $clause->condition($payment_table.'.payment_method', 'rcpar_partners_payment', "<>")
                        ->isNull($payment_table.'.payment_method');
                    //$this->query->add_where($this->options['group'], $clause);
                }

                return;
            }
        }
        // default behaviour
        if (!empty($info[$this->operator]['method'])) {
            $this->{$info[$this->operator]['method']}($field);
        }
    }
}

