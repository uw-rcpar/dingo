<?php

class rcpar_commerce_handler_field_taxed_refund extends commerce_payment_handler_field_amount {
  function label() {
    if (!isset($this->options['label'])) {
      return $this->ui_name();
    }
    return $this->options['label'];
  }
  
  function query() {        
    $this->ensure_my_table();

    if (isset($this->aliases['rcpar_refund'])){
      $this->field_alias = $this->aliases['rcpar_refund'];
    } else {
      $this->field_alias = 'rcpar_refund';
    }

    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    if (method_exists($this->query, 'add_field')) {
      $this->field_alias = $this->query->add_field('', 'commerce_order.order_id', $this->field_alias, $params);
    }
  }

  function render($values) {
    $order_id = $this->get_value($values);
    // call custom avatax fucntion for refunds info
    $value = rcpar_avatax_get_refunds($order_id);
    $currency_code = 'USD';

    switch ($this->options['display_format']) {
      case 'formatted':
        return commerce_currency_format($value, $currency_code);

      case 'raw':
        // First load the full currency array.
        $currency = commerce_currency_load($currency_code);

        // Format the price as a number.
        return number_format(commerce_currency_round(commerce_currency_amount_to_decimal($value, $currency_code), $currency), $currency['decimals']);
    }
  }


}
