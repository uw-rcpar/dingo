<?php

/**
 * this handler will show the product price when the user originally bought it
 */
class rcpar_commerce_handler_rcpar_product_version_price extends commerce_payment_handler_field_amount {

  function label() {
    if (!isset($this->options['label'])) {
      return $this->ui_name();
    }
    return $this->options['label'];
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function render($values) {
    //gets the original product unit price that product has 
    //when the order was put in place
    $field_data = $values->_field_data;
    $line_item_entity = $field_data['line_item_id']['entity'];
    $amount = 0;
    try {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item_entity);
      $base_price = $line_item_wrapper->commerce_unit_price->data->value();
      $components = $line_item_wrapper->commerce_unit_price->data->value();
      $price_data = $components['components'][0]['price'];
      $amount = $price_data['amount'];
      $currency_code = $price_data['currency_code'];
    }
    catch (EntityMetadataWrapperException $exc) {
      watchdog(__FILE__, "function " . __FUNCTION__ . " entity error" . $exc->getMessage());
    }

    switch ($this->options['display_format']) {
      case 'formatted':
        return commerce_currency_format($amount, $currency_code);

      case 'raw':
        // First load the full currency array.
        $currency = commerce_currency_load($currency_code);

        // Format the price as a number.
        return number_format(commerce_currency_round(commerce_currency_amount_to_decimal($amount, $currency_code), $currency), $currency['decimals']);
    }
  }

}
