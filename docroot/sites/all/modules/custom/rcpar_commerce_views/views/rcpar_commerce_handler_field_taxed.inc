<?php

class rcpar_commerce_handler_field_rcpar_taxed_order_part extends commerce_payment_handler_field_amount {
  function label() {
    if (!isset($this->options['label'])) {
      return $this->ui_name();
    }
    return $this->options['label'];
  }
  
  function query() {
    $this->ensure_my_table();

    if (isset($this->aliases['rcpar_taxed'])){
      $this->field_alias = $this->aliases['rcpar_taxed'];
    } else {
      $this->field_alias = 'rcpar_taxed';
    }

    $sql = "
      (SELECT SUM(t.taxed) as taxed
      FROM rcpar_commerce_reports_tax t
      INNER JOIN field_data_commerce_line_items li on li.commerce_line_items_line_item_id = t.order_line_id
      WHERE t.order_id = " . $this->table_alias . ".order_id
      )
    ";

    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();
    if (method_exists($this->query, 'add_field')) {
      $this->field_alias = $this->query->add_field('', $sql, $this->field_alias, $params);
    }
  }

  function render($values) {
    $value = $this->get_value($values);

    // the way commerce_payment_handler_field_amount handlers get the currency code
    // it's a little bit buggy, and we are always using USD as currency, so...
    //$currency_code = $this->get_value($values, 'currency_code');
    $currency_code = 'USD';

    switch ($this->options['display_format']) {
      case 'formatted':
        return commerce_currency_format($value, $currency_code);

      case 'raw':
        // First load the full currency array.
        $currency = commerce_currency_load($currency_code);

        // Format the price as a number.
        return number_format(commerce_currency_round(commerce_currency_amount_to_decimal($value, $currency_code), $currency), $currency['decimals']);
    }
  }


}
