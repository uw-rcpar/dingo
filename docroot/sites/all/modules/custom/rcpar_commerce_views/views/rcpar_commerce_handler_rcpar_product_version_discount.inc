<?php

/**
 * this handler will show the discount applied per line item when the user
 * originally bought it.
 */
class rcpar_commerce_handler_rcpar_product_version_discount extends commerce_payment_handler_field_amount {

  function label() {
    if (!isset($this->options['label'])) {
      return $this->ui_name();
    }
    return $this->options['label'];
  }

  function query() {
    // do nothing -- to override the parent query. we no need query info
  }

  function render($values) {
    //gets the line item amount of the discount that the user was applied
    //when the original order was put in place.
    $field_data = $values->_field_data;
    $line_item_entity = $field_data['line_item_id']['entity'];
    try {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item_entity);
      $unit_price = $line_item_wrapper->commerce_unit_price->amount->value();
      if ($unit_price > 0) {
        $components = $line_item_wrapper->commerce_unit_price->data->value();
        $amount = 0;
        //extracts the discounted part to be displayed
        foreach ($components['components'] as $component) {
          if (strpos($component['name'], 'commerce_coupon_pct_') === 0) {
            $amount += -1 * $component['price']['amount'];
          }
        }
      }
      else {
        return '';
      }
    }
    catch (EntityMetadataWrapperException $exc) {
      watchdog(__FILE__, "function " . __FUNCTION__ . " entity error");
    }

    $currency_code = 'USD';

    switch ($this->options['display_format']) {
      case 'formatted':
        return commerce_currency_format($amount, $currency_code);

      case 'raw':
        // First load the full currency array.
        $currency = commerce_currency_load($currency_code);

        // Format the price as a number.
        return number_format(commerce_currency_round(commerce_currency_amount_to_decimal($amount, $currency_code), $currency), $currency['decimals']);
    }
  }

}
