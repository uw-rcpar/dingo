<?php

/**
 * contact form to log time 
 */



/**
 * Contact form to sync to Desk.com
 */
function rcpar_deskcom_contact_form($form, $form_state) {
  global $user;
  $user_wrapper = entity_metadata_wrapper('user', $user);

  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#default_value' => $user_wrapper->field_first_name->value(),
    '#prefix' => '<div class="col-sm-6 webform-left"><div class="webform-inner">',
    '#suffix' => '</div></div>',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#default_value' => $user_wrapper->field_last_name->value(),
    '#prefix' => '<div class="col-sm-6 webform-right"><div class="webform-inner">',
    '#suffix' => '</div></div>',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => $user_wrapper->mail->value(),
    '#prefix' => '<div class="col-sm-6 webform-left"><div class="webform-inner">',
    '#suffix' => '</div></div>',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#default_value' => $user_wrapper->field_phone_number->value(),
    '#prefix' => '<div class="col-sm-6 webform-right"><div class="webform-inner">',
    '#suffix' => '</div></div>',
    '#size' => 60,
    '#required' => TRUE,
    '#maxlength' => 128,
  );

  $form['subject'] = array(
    '#type' => 'select',
    '#title' => t('Subject'),
    '#required' => TRUE,
    '#options' => array(
      "Account Inquiry/Assistance" => "Account Inquiry/Assistance", 
      "CPA Exam Inquiry" => "CPA Exam Inquiry",
      "Products and Pricing" => "Products and Pricing", 
      "Sign-up/Checkout Inquiry"=> "Sign-up/Checkout Inquiry", 
      "Other" => "Other"
    )
  );

  $form['how_cihy'] = array(
    '#title' => t('How Can We Help You?'),
    '#type' => 'textarea',
    '#required' => TRUE,
  );

  $form['form_captcha'] = array(
    '#type' => 'captcha',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Your Question')
  );
  
  return $form;
}

/**
 * Sync the information from the form submittion to Desk.com
 */
function rcpar_deskcom_contact_form_submit($form, $form_state) {    
  extract($form_state['values']);
  $str_description = "Submission id: ".REQUEST_TIME."  |  ";
  $customer_info = $case_info = array();
  
  if (isset($form_state['values']['first_name'])) {
    $customer_info['first_name'] = $form_state['values']['first_name'];
  }
  if (isset($form_state['values']['last_name'])) {
    $customer_info['last_name'] = $form_state['values']['last_name'];
  }
  if (isset($form_state['values']['email'])) {    
    $from = trim($form_state['values']['email']);
    $customer_info['emails'] = $form_state['values']['email'];
  }
  if (isset($form_state['values']['phone'])) {
    $customer_info['phone_numbers'] = $form_state['values']['phone'];
  }
  
  if (isset($form_state['values']['how_cihy'])) {
    $how_cihy = $form_state['values']['how_cihy'];
  }
  

  $params = array(
    'subject' => $form_state['values']['subject'],
    'body' => "{$str_description} \n {$how_cihy}",
    'customer_info' => $customer_info,
     
  );
  
    
  drupal_mail('rcpar_deskcom', 'rcpar_contact', variable_get('rcpar_deskcom_contact_form_email'), language_default(), $params, $from);
  
}
