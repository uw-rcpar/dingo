<?php

/**
 * Form to save configurations to connect with Desk.com
 */
function rcpar_deskcom_admin_settings() {
  $form = array();
  
  $form['rcpar_deskcom_contact_form_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Us Form Email'),
    '#size' => 30,
    '#default_value' => variable_get('rcpar_deskcom_contact_form_email',''),
    '#maxlength' => 128,
    '#description' => t('Email account to use for getting the Contact form information.'),
  );
  
  return system_settings_form($form);
}
