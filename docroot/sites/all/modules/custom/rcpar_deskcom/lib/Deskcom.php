<?php

/**
 * This class uses the api of Desk.com listed on 
 * http://dev.desk.com/API/
 * TODO:
 *  --all methods to connect to the api
 * 
 */

class Deskcom {

  // Defines the base URL of the API.
  protected $baseUrl;
  // Define the email to use on the api calls.
  protected $email;
  // Define the password of the api calls.
  protected $password;

  // api information args
  const API_ARGS = 'api/v2/';

  // Manage a single cURL handle used to submit API requests.
  protected $ch;

  public function __construct($baseUrl, $email, $password) {
    $this->baseUrl = $baseUrl;
    $this->email = $email;
    $this->password = $password;
    $this->ch = curl_init();
  }

  /**
   * Returns the base URL for the API.
   *
   * @return string
   *   The base URL for the API that query parameters will be appended to when
   *   submitting API requests.
   */
  public function baseUrl() {
    return "https://{$this->baseUrl}.desk.com/" . self::API_ARGS;
  }

  
  /**
   * Conects to the api to create a case based on documentation api:
   * http://dev.desk.com/API/cases/#create
   * 
   * @param <array> $fields list of values to sent and create case
   * @param <string> $customer_link relation to the customer
   * @return <array> information about the request of the api
   */
  public function createCase($fields, $customer_link = NULL) {
    $headers = array();
    $headers[] = "Accept: application/json";
    $headers[] = "Content-Type: application/json";
    $url = $this->baseUrl() . "cases";
    $fields['message']['direction'] = 'in';
    $fields['message']['status'] = 'pending';
    $fields+= array(
      'type' => 'email', //this might chaange on future requirements
      'labels' => array(),
      'status' => 'new'
    );

    return $this->doRequest('POST', $url, $fields, $headers);
  }

  /**
   *  we create the customer based on the api requirements listed
   * here: http://dev.desk.com/API/customers/#create
   * 
   * @param <array> $fields fields to add customer
   * @return <array> information about the api success
   */
  public function createCustomer($fields) {
    $headers = array();
    $headers[] = "Accept: application/json";
    $headers[] = "Content-Type: application/json";
    $url = $this->baseUrl() . "customers";
    return $this->doRequest('POST', $url, $fields, $headers);    
  }

  /**
   * load a case from the api based on the case id;
   * @param <int> $caseId the id of the case to load
   * @return <array> with the result information whether it fails or not
   */
  public function getCase($caseId) {    
    $url = $this->baseUrl() . "cases/{$caseId}";
    $result = $this->doRequest('GET', $url);
    return $result['result'];
  }

  /**
   * load the full description or message from the case
   * @param <int> $caseId the id of the case to load
   * @return <array> with the result information whether it fails or not
   */
  public function getCaseMessage($caseId) {    
    $url = $this->baseUrl() . "cases/{$caseId}/message";
    $result = $this->doRequest('GET', $url);
    return $result['result'];
  }

  /**
   * load the cases from the api based on the pagination;
   * @param <int> $page pagination number
   * @return <array> with the result information whether it fails or not
   */
  public function getCases($page = 1) {    
    $url = $this->baseUrl() . "cases?page={$page}&per_page=50";
    $result = $this->doRequest('GET', $url);
    return $result['result'];
  }
  
  /**
   * load the Customer List from the api based on the pagination;
   * @param <int> $since_id The ID to start
   * @param <int> $page The page number to start
   * @return <array> with the result information whether it fails or not
   */
  public function getCustomersList($since_id = 0, $page = 1){
    $url = $this->baseUrl() . "customers?since_id={$since_id}&page={$page}";
    $result = $this->doRequest('GET', $url);
    return $result['result'];    
  }
  
  /**
   * load the cases from the api based on the customer id;
   * @param <int> $customerID the id of the customer to get the cases
   * @return <array> with the result information whether it fails or not
   */
  public function getCustomerCases($customerID = 0){
    $url = $this->baseUrl() . "customers/{$customerID}/cases";
    $result = $this->doRequest('GET', $url);
    return $result['result'];    
  }
  
  /**
   * load the element information based on the case id;
   * @param <int> $caseID the id of the case to load
   * @param <int> $element part of the case to load
   * @return <array> with the result information whether it fails or not
   */
  public function getElementByCase($caseID, $element){
    $url = $this->baseUrl() . "cases/{$caseID}/{$element}";
    $result = $this->doRequest('GET', $url);
    return $result['result'];    
  }
  
  /**
   * load the replies from the api based on the case id;
   * @param <int> $caseID the id of the case to load
   * @return <array> with the result information whether it fails or not
   */
  public function getRepliesByCase($caseID){
    return $this->getElementByCase($caseID, "replies"); 
  }
  
  /**
   * load the notes from the api based on the case id;
   * @param <int> $caseID the id of the case to load
   * @return <array> with the result information whether it fails or not
   */
  public function getNotesByCase($caseID){
    return $this->getElementByCase($caseID, "notes"); 
  }
  
  /**
   * load a customer from the api based on the customer id;
   * @param <int> $customerID the id of the customer to load
   * @return <array> with the result information whether it fails or not
   */
  public function getCustomerById($customerID = 0){
    $url = $this->baseUrl() . "customers/{$customerID}";
    $result = $this->doRequest('GET', $url);
    return $result['result'];    
  }
  /**
   * load customers from the api based on the customer id;
   * @param <array> $mail_list the mail list of the customers to load
   * @return <array> with the result information whether it fails or not
   */
  public function getCustomerByMail($mail_list = 0){
    $url = $this->baseUrl() . "customers/search?email=".implode(',', $mail_list);
    $result = $this->doRequest('GET', $url);
    return $result['result'];    
  }

  /**
   * returns a list of customers matching the provided emails
   * referenced to documentation on:  http://dev.desk.com/API/customers/#search
   * 
   * 
   * @param <string> $email_list list of emails comma separated 
   * -- example: email1@mymail.com,email2@mymail.com,email3@mymail.com
   * 
   * @return <array> with the result information whether it fails or not
   */
  public function getCustomers($email_list){
    $url = $this->baseUrl() . "customers/search?email={$email_list}";
    return $this->doRequest('GET', $url);    
  }
  /**
   * 
   * @param type $message
   * @param <array> variables, all the information to log
   * @param type $severity
   * - WATCHDOG_EMERGENCY: Emergency, system is unusable.
   * - WATCHDOG_ALERT: Alert, action must be taken immediately.
   * - WATCHDOG_CRITICAL: Critical conditions.
   * - WATCHDOG_ERROR: Error conditions.
   * - WATCHDOG_WARNING: Warning conditions.
   * - WATCHDOG_NOTICE: (default) Normal but significant conditions.
   * - WATCHDOG_INFO: Informational messages.
   * - WATCHDOG_DEBUG: Debug-level messages.
   */
  public function logMessage($message, $variables, $severity = WATCHDOG_INFO) {
    watchdog('Desk.com', $message, $variables, $severity);
  }

  /**
   * Closes the cURL handle when the object is destroyed.
   */
  public function __destruct() {
    if (is_resource($this->ch)) {
      curl_close($this->ch);
    }
  }

   /**
   * Performs a request.
   *
   * @param string $method
   *   The HTTP method to use. One of: 'GET', 'POST', 'PUT', 'DELETE'.
   * @param string $path
   *   The remote path. The base URL will be automatically appended.
   * @param array $fields
   *   An array of fields to include with the request. Optional.
   *
   * @return array
   *   An array with the 'success' boolean and the result. If 'success' is FALSE
   *   the result will be an error message. Otherwise it will be an array
   *   of returned data.
   */
  protected function doRequest($method, $url, array $fields = array(), array $headers = array("Accept: application/json")) {
    $return = array();    
    // Set the request URL and method.
    
    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);    
    curl_setopt($this->ch, CURLOPT_USERPWD, $this->email . ":" . $this->password);
    if (!empty($fields)) {
      // JSON encode the fields and set them to the request body.
      $fields = json_encode($fields);      
      curl_setopt($this->ch, CURLOPT_POSTFIELDS, $fields);
      // Log the API request with the JSON encoded fields.
    }
    
    $result = curl_exec($this->ch);  
    
    $response_code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
    $success = in_array($response_code, array(200, 201));
    $severity = $success ?   WATCHDOG_INFO : WATCHDOG_ERROR;  

    // Log information about the request.
    $this->logMessage('Request info: !url !headers !response !meta !method', array(
      '!url' => "<pre>URL : $url</pre>",
      '!headers' => "<pre>Request Headers:\n" . var_export(curl_getinfo($this->ch, CURLOPT_HTTPHEADER), TRUE) . '</pre>',
      '!response' => "<pre>Response:\n" . var_export($result, TRUE) . '</pre>',
      '!meta' => "<pre>Response Meta:\n" . var_export(curl_getinfo($this->ch), TRUE) . '</pre>',
      '!method' => "<pre>Method:\n" . __FUNCTION__ . '</pre>',
    ),$severity);
    
    if (!$success) {      
      $error = 'Error ' . $response_code;

      // Return the error message if it exists.
      if (!empty($result)) {
        $decoded_result = json_decode($result, TRUE);
        $error.= ": " .  $decoded_result['message'];
        $result = $error;
        // Return the error message if it's there.
        if (isset($decoded_result['error'])) {
          $return['error'] = $decoded_result['error'];
        }
      }
    }
    elseif ($success && !empty($result)) {
      $result = json_decode($result, TRUE);
    }

    $return += array(
      'success' => $success,
      'result' => $result,
      'response_code' => $response_code,
    );

    return $return;
  }



}
