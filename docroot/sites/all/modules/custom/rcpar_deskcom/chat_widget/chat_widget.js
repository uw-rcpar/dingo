(function ($) {
  $(document).ready(function() {
    // Custom validation for chat fields
    if($("#customer_widget_main").length > 0) {
      // Last name as required
      $("#customer_custom1").rules("add", { required:true, messages: { required: "Last name is required." } });
      // Email as required
      $("#interaction_email").rules("add", { required:true, messages: { required: "Email is required." } });
    }

    // Change to Email widget label if chat is not available
    if($("#assistly-widget-1").length) {
      var att = $('.a-desk-widget-chat').attr('onclick');
      if(!att){
        // Wait untill widget is ready
        setTimeout(function(){
          att = $('.a-desk-widget-chat').attr('onclick');
          changeWidgetLabel(att);
        }, 3000);
      } else {
        changeWidgetLabel(att);
      }
    }
  });

  // Custom function for change Chat widget label 
  function changeWidgetLabel(att){
    if (att.indexOf("widget/emails/new") > 0) {
      $(".a-desk-widget-chat").html('Email Us');
    } else {
      $(".a-desk-widget-chat").html('Chat now!');
    }
  }

}(jQuery));
