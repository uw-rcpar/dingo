<?php

define('UPS_XML_API_URL', variable_get('ups_labels_endpoint_url', 'https://wwwcie.ups.com/ups.app/xml/'));

/**
 * Menu callback for admin/config/zoho. Admin settings form.
 */
function rcpar_ups_get_credentials() {

  $credentials = array();

  $credentials['ACCESS_LICENCE_NUMBER'] = variable_get('commerce_ups_access_key', '');
  $credentials['UPS_USERNAME'] = variable_get('commerce_ups_user_id', '');
  $credentials['UPS_PASSWORD'] = variable_get('commerce_ups_password', '');
  $credentials['UPS_ACCOUNT_ID'] = variable_get('commerce_ups_account_id', '');

  return $credentials;
}

function rcpar_ups_ship_confirm($data = NULL) {

  $url = UPS_XML_API_URL . "ShipConfirm";
  $xmlRequest = rcpar_ups_ship_confirm_xml($data);
  $ShipmentDigest = '';

  $xmlResponse = drupal_http_request($url, array(
      'method' => 'POST',
      'data' => $xmlRequest,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8'),
    )
  );

  if ($xmlResponse->code == 200) {

    $xml = simplexml_load_string($xmlResponse->data, 'SimpleXMLElement', LIBXML_NOCDATA);
    $array_Response = json_decode(json_encode($xml), true);    
    
    if(isset($array_Response['ShipmentDigest'])){          
      $ShipmentDigest = $array_Response['ShipmentDigest'];    
      return $ShipmentDigest;
    }elseif($array_Response['Response']['Error']['ErrorDescription']){
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " Error: " . $array_Response['Response']['Error']['ErrorDescription']);
    }
  }

}

function rcpar_ups_ship_confirm_xml($data = NULL) {
  $credentials = array();
  $credentials = rcpar_ups_get_credentials();
  
  if(!isset($data)){
    return;
  }

  $xml = '<?xml version="1.0"?>
<AccessRequest xml:lang="en-US">
<AccessLicenseNumber>' . $credentials['ACCESS_LICENCE_NUMBER'] . '</AccessLicenseNumber>
<UserId>' . $credentials['UPS_USERNAME'] . '</UserId>
<Password>' . $credentials['UPS_PASSWORD'] . '</Password>
</AccessRequest>
<?xml version="1.0"?>
<ShipmentConfirmRequest xml:lang="en-US">
<Request>
<TransactionReference>
<CustomerContext>Customer Comment</CustomerContext>
<XpciVersion/>
</TransactionReference>
<RequestAction>ShipConfirm</RequestAction>
<RequestOption>validate</RequestOption>
</Request>
<LabelSpecification>
<LabelPrintMethod>
<Code>GIF</Code>
<Description>gif file</Description>
</LabelPrintMethod>
<HTTPUserAgent>Mozilla/4.5</HTTPUserAgent>
<LabelImageFormat>
<Code>GIF</Code>
<Description>gif</Description>
</LabelImageFormat>
</LabelSpecification>
<Shipment>
<RateInformation>
<NegotiatedRatesIndicator/>
</RateInformation>
<Description/>
<Shipper>
<Name>Roger CPA Review</Name>
<PhoneNumber>415-346-4272</PhoneNumber>
<ShipperNumber>' . $credentials['UPS_ACCOUNT_ID'] . '</ShipperNumber>
<TaxIdentificationNumber>1234567890</TaxIdentificationNumber>
<Address>
<AddressLine1>3330 Geary Blvd</AddressLine1>
<City>San Francisco</City>
<StateProvinceCode>California</StateProvinceCode>
<PostalCode>94118</PostalCode>
<PostcodeExtendedLow></PostcodeExtendedLow>
<CountryCode>'.$data['country'].'</CountryCode>
</Address>
</Shipper>
<ShipTo>
<CompanyName>'.$data['name'].'</CompanyName>
<AttentionName>'.$data['name'].'</AttentionName>
<PhoneNumber>'.$data['phone'].'</PhoneNumber>
<EMailAddress>'.$data['email'].'</EMailAddress>  
<Address>
<AddressLine1>'.$data['address_line'].'</AddressLine1>
<AddressLine2>'.$data['address_line2'].'</AddressLine2>
<City>'.$data['city'].'</City>
<StateProvinceCode>'.$data['state'].'</StateProvinceCode>
<PostalCode>'.$data['postal_code'].'</PostalCode>
<CountryCode>'.$data['country'].'</CountryCode>
</Address>
</ShipTo>
<ShipFrom>
<CompanyName>Roger CPA Review</CompanyName>
<AttentionName>Roger CPA Review</AttentionName>
<PhoneNumber>415-346-4272</PhoneNumber>
<TaxIdentificationNumber>1234567877</TaxIdentificationNumber>
<Address>
<AddressLine1>3330 Geary Blvd</AddressLine1>
<City>San Francisco</City>
<StateProvinceCode>CA</StateProvinceCode>
<PostalCode>94118</PostalCode>
<CountryCode>US</CountryCode>
</Address>
</ShipFrom>
<PaymentInformation>
<Prepaid>
<BillShipper>
<AccountNumber>' . $credentials['UPS_ACCOUNT_ID'] . '</AccountNumber>
</BillShipper>
</Prepaid>
</PaymentInformation>
<Service>
<Code>'.rcpar_ups_codes($data['shipping_service']['name']).'</Code>
<Description>'.$data['shipping_service']['title'].'</Description>
</Service>
<ShipmentServiceOptions>
<Notification>
<NotificationCode>6</NotificationCode>
<EMailMessage>
<EMailAddress>'.$data['email'].'</EMailAddress>
</EMailMessage>
</Notification>
</ShipmentServiceOptions>
<Package>
<PackagingType>
<Code>02</Code>
<Description>Customer Supplied</Description>
</PackagingType>
<Description>Offline Lectures CPA</Description>
<ReferenceNumber>
<Code>CP</Code>
<Value>Package</Value>
</ReferenceNumber>
<PackageWeight>
<UnitOfMeasurement/>
<Weight>'.$data['order_weight']['weight'].'</Weight>
</PackageWeight>
<AdditionalHandling>0</AdditionalHandling>
</Package>
</Shipment>
</ShipmentConfirmRequest>
';

  return $xml;
}

function rcpar_ups_ship_accept($shipper_digest_data = '') {

  $url = UPS_XML_API_URL . "ShipAccept";
  $xmlRequest = rcpar_ups_ship_accept_xml($shipper_digest_data);

  $xmlResponse = drupal_http_request($url, array(
      'method' => 'POST',
      'data' => $xmlRequest,
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8'),
    )
  );

  if ($xmlResponse->code == 200) {

    $xml = simplexml_load_string($xmlResponse->data, 'SimpleXMLElement', LIBXML_NOCDATA);
    $array_Response = json_decode(json_encode($xml), true);    
    
    if(isset($array_Response['ShipmentResults']['PackageResults'])){    
      // Hold tracking number to save it later
      $_SESSION['track_id_session'] = $array_Response['ShipmentResults']['PackageResults']['TrackingNumber'];
      $GraphicImage = $array_Response['ShipmentResults']['PackageResults']['LabelImage']['GraphicImage'];    
      return base64_decode($GraphicImage);      
    }
    
    if(isset($array_Response['Response']['Error']['ErrorDescription'])){
      watchdog('ups', "Error getting image label".$array_Response['Response']['Error']['ErrorDescription']);
    }
  }
}

function rcpar_ups_ship_accept_xml($shipper_digest_data = '') {

  if ($shipper_digest_data == '') {
    return t('Error on Shipper Digest Data. The value is empty.');
  }

  $credentials = array();
  $credentials = rcpar_ups_get_credentials();

  $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>
<AccessRequest>
<AccessLicenseNumber>' . $credentials['ACCESS_LICENCE_NUMBER'] . '</AccessLicenseNumber>
<UserId>' . $credentials['UPS_USERNAME'] . '</UserId>
<Password>' . $credentials['UPS_PASSWORD'] . '</Password>
</AccessRequest>
<?xml version="1.0" encoding="ISO-8859-1"?>
<ShipmentAcceptRequest>
<Request>
<TransactionReference>
<CustomerContext>Customer Comment</CustomerContext>
</TransactionReference>
<RequestAction>ShipAccept</RequestAction>
<RequestOption>1</RequestOption>
</Request>
<ShipmentDigest>' . $shipper_digest_data . '</ShipmentDigest>
</ShipmentAcceptRequest>
';

  return $xml;
}

function rcpar_ups_get_label($order_id) {
 // rcpar_ups_print_label('pete_printer_test.html', 'html');
  $data = rcpar_ups_get_orders_data($order_id);
  return rcpar_ups_ship_accept(rcpar_ups_ship_confirm($data));
}

/**
 * Get the saved tracking numbre from ups
 * @param Int $order_id
 * @return String Tracking_number
 */
function rcpar_ups_get_tracking_id($order_id) {
  $select = db_select('rcpar_ups_labels_log', 'l');
  $select->addField('l', 'tracking_number');
  $select->condition('order_id', $order_id);
  $result = $select->execute()->fetchColumn();
  return $result ? $result : NULL;
}


function rcpar_ups_print_admin($form, &$form_state){
  $form['rcpar_ups_ssh_cert_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Printer server ssl certificate path'),
    '#default_value' => variable_get('rcpar_ups_ssh_cert_path', ''),
    '#size' => 80,
    '#description' => t('Path to the cert. file used for scp the files to the remote server.'),
    '#required' => TRUE,
  );

  $form['rcpar_ups_ssh_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Printer server user name used for the SCP file transfer'),
    '#default_value' => variable_get('rcpar_ups_ssh_username', ''),
    '#size' => 50,
    '#description' => t('Username to be used for scp the files to the remote server.'),
    '#required' => TRUE,
  );

  $form['rcpar_ups_printer_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Printer server URL used for the SCP file transfer'),
    '#default_value' => variable_get('rcpar_ups_printer_server_url', ''),
    '#size' => 80,
    '#description' => t('URL to the remote server to which are we going to move the files.'),
    '#required' => TRUE,
  );

  $form['rcpar_ups_printer_file_dst_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination path on the remote server for the labels files to be placed'),
    '#default_value' => variable_get('rcpar_ups_printer_file_dst_path', ''),
    '#size' => 80,
    '#description' => t('Destination path to place the files on the remote server.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}