<?php

  define('RCPAR_UPS_ENDPOINT', variable_get('ups_pickup_endpoint_url', 'https://wwwcie.ups.com/webservices/Pickup'));

  function _createProcessPickupCreationRequest($order)
  {
    $order_wrapper = entity_metadata_wrapper("commerce_order", $order);
    $shipping_profile =   $order_wrapper->commerce_customer_shipping->value();
    $order_line_items = $order_wrapper->commerce_line_items->value();    
    $weight= 0;
    foreach ($order_line_items as $lineitem) {      
      $line_wrapper = entity_metadata_wrapper('commerce_line_item', $lineitem);
      if ($line_wrapper->getBundle() == "product") {
        $product = $line_wrapper->commerce_product->value();   
        $weight = $weight + $product->field_weight[LANGUAGE_NONE][0]['weight'];
      }
    }

    //create soap request
    $requestoption['RequestOption'] = '1';
    $request['Request'] = $requestoption;
    $request['RatePickupIndicator'] = 'N';
    $request['TaxInformationIndicator'] = 'N';
    $request['Shipper'] = '0';

    $request['PickupDateInfo']['ReadyTime'] = '1600';
    $request['PickupDateInfo']['CloseTime'] = '1800';
    $request['PickupDateInfo']['PickupDate'] = date("Ymd");

    $request['PickupAddress']['CompanyName'] = 'Roger CPA Review';
    $request['PickupAddress']['ContactName'] = 'Shipping Department';
    $request['PickupAddress']['AddressLine'] = '3330 Geary Blvd';
    $request['PickupAddress']['City'] = 'San Francisco';
    $request['PickupAddress']['StateProvince'] = 'CA';
    $request['PickupAddress']['Urbanization'] = '3330 Geary Blvd';
    $request['PickupAddress']['PostalCode'] = '94118';
    $request['PickupAddress']['CountryCode'] = 'US';
    $request['PickupAddress']['ResidentialIndicator'] = 'N';
    $request['PickupAddress']['Phone']['Number'] = '4153464272';
    $request['PickupAddress']['Phone']['Extension'] = '';

    $request['AlternateAddressIndicator'] = 'N';

    $request['PickupPiece']['ServiceCode'] = '001';
    $request['PickupPiece']['Quantity'] = '1';
    $request['PickupPiece']['DestinationCountryCode'] = 'US';
    $request['PickupPiece']['ContainerCode'] = '01';

    $request['TotalWeight']['Weight'] = '1';
    $request['TotalWeight']['UnitOfMeasurement'] = 'LBS';
    /*
    $request['OverweightIndicator'] = 'N';
    $request['TrackingData'] = 'N';
    */
    $request['PaymentMethod'] = '00';
    /*
    $request['SpecialInstruction'] = 'N';
    $request['ReferenceNumber'] = 'N';
    $request['Notification'] = 'N';
    $request['FreightOptions'] = 'N';
    */
    return $request;
  }

  function callUPSPickup($order){
    $mode = array(
      'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
      'trace' => 1
    );
    $module_path = drupal_get_path("module", "rcpar_ups");
    $wsdl = $module_path . "/SCHEMAS-WSDLs/Pickup.wsdl";

    try{
      // initialize soap client
      $client = new SoapClient($wsdl , $mode);

      //set endpoint url
      $client->__setLocation(RCPAR_UPS_ENDPOINT);

      //create soap header
      $usernameToken['Username'] = variable_get('commerce_ups_user_id', '');
      $usernameToken['Password'] = variable_get('commerce_ups_password', '');
      $serviceAccessLicense['AccessLicenseNumber'] = variable_get('commerce_ups_access_key', '');
      $upss['UsernameToken'] = $usernameToken;
      $upss['ServiceAccessToken'] = $serviceAccessLicense;

      $header = new SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
      $client->__setSoapHeaders($header);

      $resp = $client->__soapCall('ProcessPickupCreation',array(_createProcessPickupCreationRequest($order)));
      watchdog("rcpar_ups", "UPS call response status: ". $resp->Response->ResponseStatus->Description, array(), WATCHDOG_INFO);
    } catch(Exception $ex){
      watchdog("rcpar_ups", "error when calling UPS pickup: ". $ex, array(), WATCHDOG_ERROR);
    }
  }

