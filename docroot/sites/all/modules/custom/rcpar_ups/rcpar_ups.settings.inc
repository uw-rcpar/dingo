<?php


function rcpar_ups_system_ups_settings(){
  $form = array();
  
  $form['ups_config'] = array(
  '#type' => 'fieldset',
  '#title' => t('UPS Settings'),
  '#collapsible'=>true,
  '#collapsed'=>FALSE,
  );
  $form['ups_config']['ups_labels_endpoint_url'] = array(
  '#type' => 'textfield',
  '#title' => t('Shipping Labels XML Webservice URI'),
  '#description' => t('URL used for UPS XML End-Points<br>Example: PROD: https://onlinetools.ups.com/ups.app/xml/<br>
DEV: https://wwwcie.ups.com/ups.app/xml/'),
  '#default_value' => variable_get('ups_labels_endpoint_url', ''),
  );
  $form['ups_config']['ups_pickup_endpoint_url'] = array(
  '#type' => 'textfield',
  '#title' => t('Pickup Webservice URI'),
  '#description' => t('URL used for UPS End-Points<br>Example: PROD: https://onlinetools.ups.com/webservices/Pickup<br>
DEV: https://wwwcie.ups.com/webservices/Pickup'),
  '#default_value' => variable_get('ups_pickup_endpoint_url', ''),
  );

  return system_settings_form($form);
}



function rcpar_ups_system_ups_process(){
  $form = array();
  $form['submit'] = array('#type' => 'submit', '#value' => t('Import'));
  $form['#submit'][] = 'rcpar_ups_process_labels';
  return $form;
}