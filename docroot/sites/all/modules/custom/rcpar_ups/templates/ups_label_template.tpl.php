<?php global $base_url?>
<html>
<head>
<title>Roger CPA Review</title>
</head>
<body>

<table cellpadding="0" cellspacing="0" width="100%">
  <tr>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						
		<td><img width="800" src="<?php echo $img?>"></td>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
		<td><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top"><img src="<?php echo $logo?>" alt="Roger CPA Review" ></td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td valign="middle">
		  	<font face="Arial" size="2">
				<b>Order Number <?php echo $data['order_id'];?></b><br>
				<b>Order Date <?php echo $data['date'];?></b>
				<?php
				if ($partner = rcpar_partners_get_partner_object()) {
				  echo "<br><b>Partner " . $partner->field_abbreviated_name->value() . "</b>";
				} ?>
			</font>	
		</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>
			<font face="Arial" size="2">
                 Ship To:<br>
                  <?php print $data['name'];?><br>
                  <?php print $data['address_line'];?><br>
                  <?php print $data['address_line2'];?><br>
                  <?php print $data['city'] . ", " . $data['state']. " " . $data['postal_code']?><br>
                  <?php print $data['country'];?>
            </font>
         </td>
	</tr>
</table>

<?php echo $table;?>
<?php
	switch ($data['shipping_worflow_type']){
		case 'no-shipping':
			echo t('NO SHIPPING REQUIRED');
			break;
		case 'pre-approval':
			echo t('SHIPPING REQUIRES APPROVAL');
			break;
	}
?>
 <?php if($data['office_location']){ ?> 
        <br>
        <b> Office: </b> <?php print $data['office_location']; ?>
 <?php } ?>
 <input type="hidden" name="amount" value="<?php print $data['paid'];?>"/>
</body>
</html>

