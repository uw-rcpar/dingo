<?php


/**
 * Implements hook_views_data_alter().
 */
function rcpar_mods_views_data_alter(&$data) {
	$data['node']['taxable'] = array(
		'title' => 'Taxable (order total less shipping)',
		'help' => 'Use it in View Taxable Per State And Product',
		'field' => array(
		  'handler' => 'rcpar_mods_handler_taxable',
		),
	);


	$data['node']['all_line_item_prices_in_view'] = array(
		'title' => 'Save the price of each produc in the order',
		'help' => 'Excecute the function to save the price of each produc in the order',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_all_line_item_prices_in_view',
		),
	);


	$data['node']['6_month_aud'] = array(
		'title' => '6 month extension AUD',
		'help' => '6 month extension AUD',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_6_month_aud',
		),
	);

	$data['node']['6_month_bec'] = array(
		'title' => '6 month extension BEC',
		'help' => '6 month extension BEC',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_6_month_bec',
		),
	);

	$data['node']['6_month_reg'] = array(
		'title' => '6 month extension REG',
		'help' => '6 month extension REG',
		'field' => array(
		 	 'handler' => 'rcpar_mods_handler_6_month_reg',
		),
	);

	$data['node']['6_month_far'] = array(
		'title' => '6 month extension FAR',
		'help' => '6 month extension FAR',
		'field' => array(
		 	 'handler' => 'rcpar_mods_handler_6_month_far',
		),
	);


	$data['node']['offline_lectures_aud'] = array(
		'title' => 'Offline Lectures AUD',
		'help' => 'Offline Lectures AUD',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_offline_lectures_aud',
		),
	);

	$data['node']['offline_lectures_bec'] = array(
		'title' => 'Offline Lectures BEC',
		'help' => 'Offline Lectures BEC',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_offline_lectures_bec',
		),
	);

	$data['node']['offline_lectures_reg'] = array(
		'title' => 'Offline Lectures REG',
		'help' => 'Offline Lectures REG',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_offline_lectures_reg',
		),
	);

	$data['node']['offline_lectures_far'] = array(
		'title' => 'Offline Lectures FAR',
		'help' => 'Offline Lectures FAR',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_offline_lectures_far',
		),
	);


	$data['node']['flashcards_aud'] = array(
		'title' => 'Flashcards AUD',
		'help' => 'Flashcards AUD',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_flashcards_aud',
		),
	);

	$data['node']['flashcards_bec'] = array(
		'title' => 'Flashcards BEC',
		'help' => 'Flashcards BEC',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_flashcards_bec',
		),
	);

	$data['node']['flashcards_reg'] = array(
		'title' => 'Flashcards REG',
		'help' => 'Flashcards REG',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_flashcards_reg',
		),
	);

	$data['node']['flashcards_far'] = array(
		'title' => 'Flashcards FAR',
		'help' => 'FlashcardsFAR',
		'field' => array(
		  	'handler' => 'rcpar_mods_handler_flashcards_far',
		),
	);


	$data['node']['audio_lectures_aud'] = array(
		'title' => 'Audio Lectures AUD',
		'help' => 'Audio Lectures AUD',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_audio_lectures_aud',
		),
	);

	$data['node']['audio_lectures_bec'] = array(
		'title' => 'Audio Lectures BEC',
		'help' => 'Audio Lectures BEC',
		'field' => array(
			'handler' => 'rcpar_mods_handler_audio_lectures_bec',
		),
	);

	$data['node']['audio_lectures_reg'] = array(
		'title' => 'Audio Lectures REG',
		'help' => 'Audio Lectures REG',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_audio_lectures_reg',
		),
	);

	$data['node']['audio_lectures_far'] = array(
		'title' => 'Audio Lectures FAR',
		'help' => 'Audio Lectures FAR',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_audio_lectures_far',
		),
	);	


	$data['node']['course_textbook_aud'] = array(
		'title' => 'Course Textbook AUD',
		'help' => 'Course Textbook AUD',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_course_textbook_aud',
		),
	);

	$data['node']['course_textbook_bec'] = array(
		'title' => 'Course Textbook BEC',
		'help' => 'Course Textbook BEC',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_course_textbook_bec',
		),
	);

	$data['node']['course_textbook_reg'] = array(
		'title' => 'Course Textbook REG',
		'help' => 'Course Textbook REG',
		'field' => array(
			'handler' => 'rcpar_mods_handler_course_textbook_reg',
		),
	);

	$data['node']['course_textbook_far'] = array(
		'title' => 'Course Textbook FAR',
		'help' => 'Course Textbook FAR',
		'field' => array(
			'handler' => 'rcpar_mods_handler_course_textbook_far',
		),
	);


	$data['node']['flash_cram_course_aud'] = array(
		'title' => 'Flash CRAM Course AUD',
		'help' => 'Flash CRAM Course AUD',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_flash_cram_course_aud',
		),
	);

	$data['node']['flash_cram_course_bec'] = array(
		'title' => 'Flash CRAM Course BEC',
		'help' => 'Flash CRAM Course BEC',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_flash_cram_course_bec',
		),
	);

	$data['node']['flash_cram_course_reg'] = array(
		'title' => 'Flash CRAM Course REG',
		'help' => 'Flash CRAM Course REG',
		'field' => array(
			'handler' => 'rcpar_mods_handler_flash_cram_course_reg',
		),
	);

	$data['node']['flash_cram_course_far'] = array(
		'title' => 'Flash CRAM Course FAR',
		'help' => 'Flash CRAM Course FAR',
		'field' => array(
			'handler' => 'rcpar_mods_handler_flash_cram_course_far',
		),
	);	


	$data['node']['cpa_exam_review_aud'] = array(
		'title' => 'CPA Exam Review AUD',
		'help' => 'CPA Exam Review AUD',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_cpa_exam_review_aud',
		),
	);

	$data['node']['cpa_exam_review_bec'] = array(
		'title' => 'CPA Exam Review BEC',
		'help' => 'CPA Exam Review BEC',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_cpa_exam_review_bec',
		),
	);

	$data['node']['cpa_exam_review_reg'] = array(
		'title' => 'CPA Exam Review REG',
		'help' => 'CPA Exam Review REG',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_cpa_exam_review_reg',
		),
	);

	$data['node']['cpa_exam_review_far'] = array(
		'title' => 'CPA Exam Review FAR',
		'help' => 'CPA Exam Review FAR',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_cpa_exam_review_far',
		),
	);


	$data['node']['online_cram_courses_aude'] = array(
		'title' => 'Online CRAM Courses AUD',
		'help' => 'Online CRAM Courses AUD',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_online_cram_courses_aud',
		),
	);

	$data['node']['online_cram_courses_bec'] = array(
		'title' => 'Online CRAM Courses BEC',
		'help' => 'Online CRAM Courses BEC',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_online_cram_courses_bec',
		),
	);

	$data['node']['online_cram_courses_reg'] = array(
		'title' => 'Online CRAM Courses REG',
		'help' => 'Online CRAM Courses REG',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_online_cram_courses_reg',
		),
	);

	$data['node']['online_cram_courses_far'] = array(
		'title' => 'Online CRAM Courses FAR',
		'help' => 'Online CRAM Courses FAR',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_online_cram_courses_far',
		),
	);


	$data['node']['full_flashcard_discount'] = array(
		'title' => 'Full Flashcard Discount',
		'help' => 'Full Flashcard Discount',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_full_flashcard_discount',
		),
	);

	$data['node']['full_audio_discount'] = array(
		'title' => 'Full Audio Discount',
		'help' => 'Full Audio Discount',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_full_audio_discount',
		),
	);

	$data['node']['full_flash_drive_cram_discount'] = array(
		'title' => 'Full Flash Drive Cram Course Discount',
		'help' => 'Full Flash Drive Cram Course Discount',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_full_flash_drive_cram_course_discount',
		),
	);

	$data['node']['elite_package_discount'] = array(
		'title' => 'Elite Package Discount',
		'help' => 'Elite Package Discount',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_elite_package_discount',
		),
	);

	$data['node']['premier_package_discount'] = array(
		'title' => 'Premier Package Discount',
		'help' => 'Premier Package Discount',
		'field' => array(
		 	'handler' => 'rcpar_mods_handler_premier_package_discount',
		),
	);

}
/**
 * Implements hook_views_data
 * Allow us to use a view on our custom table rcpar_feedback
 */
function rcpar_mods_views_data() {
  $data = array();
  $data['rcpar_feedback_notes']['table']['group'] = t('RCPAR Feedback notes');
  $data['rcpar_feedback_notes']['table']['base'] = array(
    'title' => t('RCPAR Feedback notes'),
  );
  // The ID field
  $data['rcpar_feedback_notes']['id'] = array(
    'title' => t('ID'),
    'help' => t('The record ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  // The feedback id field
  $data['rcpar_feedback_notes']['feedback_id'] = array(
    'title' => t('Related feedback ID'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // The Type field
  $data['rcpar_feedback_notes']['note'] = array(
    'title' => t('Note'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['rcpar_feedback']['table']['group'] = t('RCPAR Feedback');
  $data['rcpar_feedback']['table']['base'] = array(
    'title' => t('RCPAR Feedback'),
  );
  // The ID field
  $data['rcpar_feedback']['id'] = array(
    'title' => t('ID'),
    'help' => t('The record ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('Feedback Note id'),
      'base' => 'rcpar_feedback_notes',
      'base field' => 'feedback_id',
      'skip base' => array(),
    ),
  );
  // The uid field
  $data['rcpar_feedback']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The user ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('User ID'),
      'base' => 'users',
      'base field' => 'uid',
      'skip base' => array(),
    ),
  );
  // The Type field
  $data['rcpar_feedback']['type'] = array(
    'title' => t('Type'),
    'help' => t('The Type of feedback (IPQ, Lecture, etc.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // The Question Type field
  $data['rcpar_feedback']['question_type'] = array(
    'title' => t('Question Type'),
    'help' => t('IPQ Question type'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // Topic id field.
  $data['rcpar_feedback']['chapter_id'] = array(
    'title' => t('Chapter id'),
    'help' => t('Chapter id.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Chapter id relationship'),
      'title' => t('Chapter relationship'),
    ),
  );
  // Topic ids field.
  $data['rcpar_feedback']['topic_ids'] = array(
    'title' => t('Topic ids'),
    'help' => t('Topic ids.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // Question id field.
  $data['rcpar_feedback']['question_id'] = array(
    'title' => t('Question id'),
    'help' => t('Question id.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'ipq_question_pool',
      'base field' => 'ipq_questions_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Question id relationship'),
      'title' => t('Question relationship'),
    ),
  );
  // The Chapter field
  $data['rcpar_feedback']['chapter'] = array(
    'title' => t('Chapter'),
    'help' => t('Chapter'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // The Topics field
  $data['rcpar_feedback']['topics'] = array(
    'title' => t('Topics'),
    'help' => t('Topics'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // Is Technical field
  $data['rcpar_feedback']['is_technical'] = array(
    'title' => t('Is Technical field'),
    'help' => t('Just an on/off field.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Is Technical'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  // Is Technical eidt
  $data['rcpar_feedback']['is_technical_edit'] = array(
    'title' => t('Change technical boolean'),
    'help' => t('Edit is software field.'),
    'field' => array(
      'handler' => 'rcpar_mods_handler_technical_edit',
    ),
  );
  // The Author field
  $data['rcpar_feedback']['author'] = array(
    'title' => t('Author'),
    'help' => t('The author of the question node'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // The Feedback field
  $data['rcpar_feedback']['feedback'] = array(
    'title' => t('Feedback'),
    'help' => t('Student feedback submitted'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  // The Created Date field
  $data['rcpar_feedback']['created'] = array(
    'title' => t('Date Created'),
    'help' => t('Date of the log creation'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  // The Updated Date field
  $data['rcpar_feedback']['updated'] = array(
    'title' => t('Date Updated'),
    'help' => t('Date of the last update'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  // The Status field
  $data['rcpar_feedback']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status of feedback'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // The Status form field
  $data['rcpar_feedback']['status_edit'] = array(
    'title' => t('Status edit'),
    'help' => t('Change Status of feedback'),
    'field' => array(
      'handler' => 'rcpar_mods_handler_status_edit',
    ),
  );


  // Begin migration table
  $data['enroll_flow_uw_teleport']['table']['group'] = t('UWorld migration data');
  $data['enroll_flow_uw_teleport']['table']['base'] = array(
    'title' => t('UWorld migration data'),
  );
  // The ID field
  $data['enroll_flow_uw_teleport']['id'] = array(
    'title' => t('ID'),
    'help' => t('The record ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // The uid field
  $data['enroll_flow_uw_teleport']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The user ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'title' => t('Name'),
      'handler' => 'views_handler_filter_user_name',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'label' => t('User ID'),
      'base' => 'users',
      'base field' => 'uid',
      'skip base' => array(),
    ),
  );

  // Teleported boolean field
  $data['enroll_flow_uw_teleport']['teleported'] = array(
    'title' => t('Has teleported field'),
    'help' => t('Just a yes/no field.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Has teleported'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // phase1_eligible field
  $data['enroll_flow_uw_teleport']['phase1_eligible'] = array(
    'title' => t('Phase1 Eligible field'),
    'help' => t('Just a yes/no field.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Phase1 Eligible'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // phase2_eligible field
  $data['enroll_flow_uw_teleport']['phase2_eligible'] = array(
    'title' => t('Phase2 Eligible field'),
    'help' => t('Just a yes/no field.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Phase2 Eligible'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // phase3_eligible field
  $data['enroll_flow_uw_teleport']['phase3_eligible'] = array(
    'title' => t('Phase3 Eligible field'),
    'help' => t('Just a yes/no field.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Phase3 Eligible'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // The Created Date field
  $data['enroll_flow_uw_teleport']['teleported_time'] = array(
    'title' => t('Teleported date'),
    'help' => t('Date of the migration'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}