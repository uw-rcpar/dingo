<?php

/**
 * Override the entity view output for Form entities.
 * @param $build
 * @param $type
 */
function rcpar_mods_entity_view($entity, $type, $view_mode, $langcode) {
  if ($type == 'form') {
    // Display our form which will submit to Hubspot.
    // Note that we are appending the entity id to the form id in order to use multiple instances of the
    // same entity lead form on the same page. See rcpar_mods_forms() hook for more information
    $entity->content['form'] = drupal_get_form('rcpar_mods_entity_form_'. $entity->id, $entity);
  }
}

/**
 * Form callback.
 * Generates the user-facing form presentation for a form entity.
 */
function rcpar_mods_entity_form($form, &$form_state, $entity) {
  // Build an array of properties that shouldn't be displayed in the form by default.
  $properties_to_hide = array('id', 'changed', 'created', 'type', 'language', 'uid', 'title', 'machine_name', 'email_submissions', 'email_page_urls');

  // Get stored weight values
  $weights = json_decode($entity->weight, TRUE);
  if(is_null($weights)) {
    $weights = array();
  }

  // Create an entity wrapper and loop through all properties
  $entity_wrapper = entity_metadata_wrapper('form', $entity);
  foreach ($entity_wrapper->getIterator() as $wrapper) {
    $field_info = array();

    // Get the property info.
    $property_info = $wrapper->info();

    // Do not show fields for properties that should be hidden or not included in the form.
    if (in_array($property_info['name'], $properties_to_hide) || $wrapper->value() == '0') {
      continue;
    }

    // Determine field types for each property.
    switch ($property_info['name']) {
      case 'state':
        $field_info = array(
          '#type' => 'select',
          '#title' => t('Residing State <span class="form-required" title="This field is required.">*</span>'),
          '#options' => array( // todo - need finalized list
            'International' => '<International>',
            'Japan' => 'Japan',
            'UAE' => 'UAE',
            'Other' => 'Other',
            'US Territories' => '<US and Territories>',
            'Alabama' => 'Alabama',
            'Alaska' => 'Alaska',
            'Arizona' => 'Arizona',
            'Arkansas' => 'Arkansas',
            'California' => 'California',
            'Colorado' => 'Colorado',
            'Connecticut' => 'Connecticut',
            'Delaware' => 'Delaware',
            'District of Columbia' => 'District of Columbia',
            'Florida' => 'Florida',
            'Georgia' => 'Georgia',
            'Guam' => 'Guam',
            'Hawaii' => 'Hawaii',
            'Idaho' => 'Idaho',
            'Illinois' => 'Illinois',
            'Indiana' => 'Indiana',
            'Iowa' => 'Iowa',
            'Kansas' => 'Kansas',
            'Kentucky' => 'Kentucky',
            'Louisiana' => 'Louisiana',
            'Maine' => 'Maine',
            'Maryland' => 'Maryland',
            'Massachusetts' => 'Massachusetts',
            'Michigan' => 'Michigan',
            'Minnesota' => 'Minnesota',
            'Mississippi' => 'Mississippi',
            'Missouri' => 'Missouri',
            'Montana' => 'Montana',
            'Nebraska' => 'Nebraska',
            'Nevada' => 'Nevada',
            'New Hampshire' => 'New Hampshire',
            'New Jersey' => 'New Jersey',
            'New Mexico' => 'New Mexico',
            'New York' => 'New York',
            'North Carolina' => 'North Carolina',
            'North Dakota' => 'North Dakota',
            'Ohio' => 'Ohio',
            'Oklahoma' => 'Oklahoma',
            'Oregon' => 'Oregon',
            'Pennsylvania' => 'Pennsylvania',
            'Puerto Rico' => 'Puerto Rico',
            'Rhode Island' => 'Rhode Island',
            'South Carolina' => 'South Carolina',
            'South Dakota' => 'South Dakota',
            'Tennessee' => 'Tennessee',
            'Texas' => 'Texas',
            'Utah' => 'Utah',
            'Vermont' => 'Vermont',
            'Virgin Islands' => 'Virgin Islands',
            'Virginia' => 'Virginia',
            'Washington' => 'Washington',
            'West Virginia' => 'West Virginia',
            'Wisconsin' => 'Wisconsin',
            'Wyoming' => 'Wyoming',
          ),
        );
        break;
      case 'country':
        $field_info = array(
          '#type' => 'select',
          '#title' => t($property_info['description'] . ' <span class="form-required" title="This field is required.">*</span>'),
          '#options' => array( // todo - need finalized list
            'International' => '<Country>',
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'VG' => 'British Virgin Islands',
            'BN' => 'Brunei',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'BQ' => 'Caribbean Netherlands',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo (Brazzaville)',
            'CD' => 'Congo (Kinshasa)',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CW' => 'Curaçao',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island and McDonald Islands',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong S.A.R., China',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'CI' => 'Ivory Coast',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Laos',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao S.A.R., China',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'KP' => 'North Korea',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russia',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthélemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts and Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin (French part)',
            'PM' => 'Saint Pierre and Miquelon',
            'VC' => 'Saint Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SX' => 'Sint Maarten',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia and the South Sandwich Islands',
            'KR' => 'South Korea',
            'SS' => 'South Sudan',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard and Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syria',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands',
            'TV' => 'Tuvalu',
            'VI' => 'U.S. Virgin Islands',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Minor Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VA' => 'Vatican',
            'VE' => 'Venezuela',
            'VN' => 'Vietnam',
            'WF' => 'Wallis and Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
          ),
        );
        break;

      case 'audience_type':
        $field_info = array(
          '#type' => 'select',
          '#title' => t($property_info['description']),
          '#options' => array(
            'Student' => 'Student',
            'Professional' => 'Professional',
          ),
        );
        break;

      case 'what_subject_do_you_teach':
        $field_info = array(
          '#type' => 'select',
          '#title' => t($property_info['description'] . ' <span class="form-required" title="This field is required.">*</span>'),
          '#multiple' => TRUE,
          '#options' => array(
            'Accounting Info Systems' => 'Accounting Info Systems',
            'Advanced Accounting' => 'Advanced Accounting',
            'Auditing' => 'Auditing',
            'Cost Accounting' => 'Cost Accounting',
            'Gov/Non Profit' => 'Gov/Non Profit',
            'Intermediate 1,2 or 3' => 'Intermediate 1,2 or 3',
            'Tax' => 'Tax',
            'Nothing currently' => 'Nothing currently',
            'My course is not listed (please list in comments)' => 'My course is not listed (please list in comments)',
          ),
        );
        break;

      case 'what_are_you_interested_in_learning_more_about':
        $field_info = array(
          '#type' => 'select',
          '#title' => t($property_info['description'] . ' <span class="form-required" title="This field is required.">*</span>'),
          '#multiple' => TRUE,
          '#options' => array(
            'Curriculum Supplements' => 'Curriculum Supplements',
            'Discounts' => 'Discounts',
            'Information on the CPA Exam' => 'Information on the CPA Exam',
            'Offering Roger CPA Review on campus' => 'Offering Roger CPA Review on campus',
            'Partnership Information' => 'Partnership Information',
            'Roger CPA Review for Credit Programs' => 'Roger CPA Review for Credit Programs',
            'Taking the CPA Exam' => 'Taking the CPA Exam',
            'Courses' => 'Courses',
          ),
        );
        break;

      case 'phone':
        $field_info = array(
          '#type' => 'textfield',
          '#title' => t($property_info['description']),
          '#title_display' => 'invisible',
          '#attributes' => array('placeholder' => $property_info['description'] . '*','pattern' => '[0-9]*'),
        );
        break;

      case 'college_id':
        // Invoke the sales funnel widget to get the autocomplete inputs for college.
        // We'll need to remove a few other unnecessary fields that it adds on
        sales_funnel_widget_append_sales_funnel_widget($form, $form_state, $GLOBALS['user']);
        unset($form['sales_funnel_widget']['#title']);
        $form['sales_funnel_widget']['field_did_you_graduate_college']['#access'] = FALSE;
        $form['sales_funnel_widget']['field_graduation_month_and_year']['#access'] = FALSE;
        $form['sales_funnel_widget']['field_when_do_you_plan_to_start']['#access'] = FALSE;

        // This field uses the weight configured for college_id
        if(isset($weights['college_id'])) {
          $form['sales_funnel_widget']['#weight'] = $weights['college_id'];
        }

        // No field widget - this is derived from the sales funnel widget
        $field_info = array();
        break;

      case 'graduation_date':
      case 'study_date':
        $field_info = array(
          '#title' => t($property_info['description'] . ' <span class="form-required" title="This field is required.">*</span>'),
          '#type' => 'textfield',
          '#attributes' => array('class' => array('monthpicker')),
        );
        break;

      case 'comments':
        $field_info = array(
          '#type' => 'textarea',
          '#title' => t($property_info['description']),
          '#required' => FALSE,
        );
        break;

      // Plain textfields
      case 'firstname' :
      case 'lastname' :
      case 'email' :
      case 'campus' :
      case 'firm' :
      case 'jobtitle' :
      case 'zipcode' :
      case 'publication_or_website' :
      case 'organization' :
        $field_info = array(
          '#type' => 'textfield',
          '#title' => t($property_info['description']),
          '#title_display' => 'invisible',
          '#attributes' => array('placeholder' => $property_info['description'] . '*'),
        );
        break;
    }

    // Get corresponding weight
    if(isset($weights[$property_info['name']])) {
      $field_info['#weight'] = $weights[$property_info['name']];
    }

    $form[$property_info['name']] = $field_info;
  }
  // Add hidden field to track page URL in Hubspot
  $form['page_url'] = array(
    '#type' => 'hidden',
    '#value' => url(drupal_get_path_alias($_GET['q']), array('absolute' => TRUE))
  );
  // Add hidden field for Sales Cloud (Salesfront) Campaign Tracker
  $form['cloudamp__data__c'] = array(
    '#type' => 'hidden',
  );
  // On an AJAX rebuild from the college widget, we might overwrite our original value with 'system/ajax', so if
  // there is a submitted value in the form already, we'll use it to preserve the original.
  if (!empty($form_state['values']['page_url'])) {
    $form['page_url']['#value'] = $form_state['values']['page_url'];
  }

  
  //if the entity form is the newsletter on the footer we do not sent the url
  if($entity->machine_name == 'footer_email'){
    $form['page_url']['#value'] = 'newsletter-footer-signup';
  }
  
  // Add hidden field for bdr_event and original_path to track in Salesforce.
  if (!empty($_GET['bdr_event'])) {
    $bdr_value = 'bdr_event='. $_GET['bdr_event'];
  }
  if (!empty($_GET['original_path'])) {
    $bdr_value .= ' original_path=' . $_GET['original_path'];
  }
  // On an AJAX rebuild from the college widget, our original value may no longer be present,
  // so use the previously submitted value.
  if (!empty($form_state['values']['bdr_event'])) {
    $bdr_value = $form_state['values']['bdr_event'];
  }
  $form['bdr_event'] = array(
    '#type' => 'hidden',
    '#value' => $bdr_value,
  );

  // Do some special form hacks for bdr_event_form
  if ($entity_wrapper->machine_name->value() == 'bdr_event_form') {
    // phone number is not required
    $form['phone']['#attributes']['placeholder'] = 'Phone Number';

    // Add hidden fields for audience type, residing state, and college info
    if (!empty($_REQUEST['audience_type'])) {
      $audience_type = $_REQUEST['audience_type'];
      $form['audience_type'] = array(
        '#type' => 'hidden',
        '#value' => $audience_type,
      );
    }

    if (!empty($_REQUEST['state'])) {
      $state = $_REQUEST['state'];
      $form['state'] = array(
        '#type' => 'hidden',
        '#value' => $_REQUEST['state'],
      );
    }

    if (!empty($_REQUEST['college_id'])) {
      $college_id = $_REQUEST['college_id'];

      $sf_account_entity = entity_load('sf_account', array($college_id));
      $college_state = $sf_account_entity[$college_id]->field_college_state_list['und'][0]['value'];
      $sf_obj = salesforce_mapping_object_load_by_drupal('sf_account', $college_id);

      $form['college_id'] = array(
        '#type' => 'hidden',
        '#value' => $college_id,
      );
      $form['college_sfid'] = array(
        '#type' => 'hidden',
        '#value' => $sf_obj->salesforce_id,
      );
      $form['college_state'] = array(
        '#type' => 'hidden',
        '#value' => $college_state,
      );
    }


  }


  // Add partner id to b2b_partner_lead_form lead form on partner landing pages
  if ($entity_wrapper->machine_name->value() == 'b2b_partner_lead_form') {
    $partner_helper = new RCPARPartner();
    if ($partner_helper->isLoaded()) {
      $partner = $partner_helper->getWrapper();
      $partner_class = new RCPARPartner($partner);
      $partner_id = $partner_class->getSalesForceAssociatedAccId();
      // On an AJAX rebuild from the college widget, our original value may no longer be present,
      // so use the previously submitted value.
      if (!empty($form_state['values']['partner_id'])) {
        $partner_id = $form_state['values']['partner_id'];
      }
      $form['partner_id'] = array(
        '#type' => 'hidden',
        '#value' => $partner_id,
      );
      // Build an array to display course options
      $products = $partner_class->getProductsInfo();
      $course_options = array();
      if ($partner_class->showBundlePackage()) {
        $course_options['4-Part CPA Review Package'] = '4-Part CPA Review Package';
        if (isset($products['stand_alone_parts'])) {
          foreach ($products['stand_alone_parts'] as $product_id => $info) {
            $course_options[$info['title']] = $info['title'];
          }
        }
        $form['course_options'] = array(
          '#type' => 'select',
          '#title' => t('Select Your Course'),
          '#multiple' => FALSE,
          '#required' => TRUE,
          '#options' => $course_options,
          '#default_value' => '4-Part CPA Review Package',
        );
      }
      // Show office location drop down if partner requires it
      if ($partner_class->showOfficeLocations()) {
        $office_location = explode("\n", $partner_class->getOfficeLocations());
        $office_location_array = array('' => 'Choose One');
        foreach($office_location as $key => $value) {
          $office_location_array[str_replace("\r", '', $value)] = str_replace("\r", '', $value);
        }
        $form['office_location'] = array(
          '#type' => 'select',
          '#title' => t('Office Location'),
          '#multiple' => FALSE,
          '#required' => TRUE,
          '#options' => $office_location_array,
        );
      }
      // Show file upload field if partner requires it.
      if ($partner_class->showUploadDocument()) {
        $instructions = $partner_class->getUploadDocumentInstructions();
        $form['upload_document'] = array(
          '#type' => 'file',
          '#title' => t('Attach Partner Authorization'),
          '#description' => t('We accept jpeg, jpg, png, pdf, and gif file formats. Files must be less than 2 MB.' . $instructions['safe_value']),
          '#required' => TRUE,
        );
      }

    }
  }

  // Special case: Japanese inquiry form needs Japanese labels.
  if ($entity_wrapper->machine_name->value() == 'inquiry' && drupal_lookup_path('alias',current_path()) == 'international/japan') {
    $form['firstname']['#title'] = 'お名前';
    $form['firstname']['#attributes']['placeholder'] = '';
    $form['firstname']['#title_display'] = 'before';
    $form['lastname']['#title'] = '&nbsp;';
    $form['lastname']['#attributes']['placeholder'] = '';
    $form['lastname']['#title_display'] = 'before';
    $form['email']['#title'] = 'Email アドレス';
    $form['email']['#attributes']['placeholder'] = '';
    $form['email']['#title_display'] = 'before';
    $form['phone']['#title'] = '電話番号';
    $form['phone']['#attributes']['placeholder'] = '';
    $form['phone']['#title_display'] = 'before';
    $form['comments']['#title'] = 'お問い合わせ内容';
  }

  // Email footer form needs a different placeholder
  if ($entity_wrapper->machine_name->value() == 'footer_email') {
    $form['email']['#attributes']['placeholder'] = 'Email signup';
  }

  // If user is logged in, add a hidden field to track UID in Hubspot.
  if (user_is_logged_in()) {
    global $user;
    $form['uid'] = array(
      '#type' => 'hidden',
      '#value' => $user->uid,
    );
  }

  // Layout values for theming
  $form['#layout'] = json_decode($entity->layout, TRUE);

  // Map the layout and weight of sales funnel widget to college_id
  if (!empty($form['#layout']['college_id'])) {
    $form['#layout']['sales_funnel_widget'] = $form['#layout']['college_id'];
  }

  // Organize fields into fieldsets for multi-step forms
  if ($entity->multistep) {
    // Add a class to the form
    $form['#attributes'] = array('class' => 'multistep-form');

    // Get the fields and their steps
    $fields = json_decode($entity->steps, TRUE);

    // Create necessary fieldsets
    $stepnumbers = array_unique($fields);
    foreach ($stepnumbers as $fieldname => $step) {
      $form["step_$step"] = array(
        '#type' => 'fieldset',
        '#title' => 'Step ' . $step,
        '#weight' => 10,
        '#attributes' => array('class' => array('step-wrapper')),
      );

      // Add markup for validation errors to fieldsets
      $form["step_$step"]["message_$step"] = array(
        '#type' => 'item',
        '#markup' => '<div class="validation-message"></div>',
        '#weight' => 20,
      );

      // Add "previous" buttons to fieldsets
      // but not to the first one
      if ($step != '1') {
        $form["step_$step"]["previous_$step"] = array(
          '#type' => 'item',
          '#prefix' => '<div class="multi-step-prev">',
          '#suffix' => '</div>',
          '#markup' => '<a id="prev-' . $step . '" class="prev-link btn btn-primary"><i class="fa fa-caret-left" aria-hidden="true"></i>
 Back</a>',
          '#weight' => 20,
        );
      }
      // Add "next" buttons to fieldsets
      // but not to the last one
      if ($step != max($stepnumbers)) {
        $form["step_$step"]["next_$step"] = array(
          '#type' => 'item',
          '#prefix' => '<div class="multi-step-next">',
          '#suffix' => '</div>',
          '#markup' => '<a id="next-' . $step . '" class="next-link btn btn-primary">Next <i class="fa fa-caret-right" aria-hidden="true"></i>
</a>',
          '#weight' => 20,
        );
      }
    }
    // Loop through fields and move to correct fieldset
    foreach ($fields as $fieldname => $step) {
      // sales funnel widget needs to go wherever college_id belongs
      if ($fieldname == 'college_id') {
        $form["step_{$step}"]['sales_funnel_widget'] = $form['sales_funnel_widget'];
        $form_state['storage']['sales_funnel_widget']['parent_element'] = "step_{$step}";
        unset($form['sales_funnel_widget']);
      }
      $form["step_{$step}"][$fieldname] = $form[$fieldname];
      unset($form[$fieldname]);
    }
    // If form contains conditional fields, create select with options
    if ($entity_wrapper->conditional_fields->value()) {
      $conditional_fields = json_decode($entity->dependent_fields, TRUE);
      $form['step_2']['conditional_fields'] =  array(
        '#title' => 'Please choose one so that we can personalize your experience',
        '#weight' => 2,
        '#type' => 'select',
        '#options' => array(
          '' => 'Choose One',
          'student' => 'Student interested in pursuing the CPA',
          'professional' => 'Professional interested in pursuing the CPA ',
          'firm' => 'Firm Administrator (HR, L&D, Recruiter, Partner, etc) ',
          'professor' => 'Professor',
        ),
        '#default_value' => '',
        '#prefix' => '<div class="audience-type-wrapper">',
        '#suffix' => '</div>',
      );
      // Use form API #states option to show and hide the conditional fields
      foreach ($conditional_fields as $field => $value) {
        $states = array();
        foreach ($value as $condition) {
          if ($condition) {
            $states[] = array('select[name="conditional_fields"]' => array('value' => $condition));
          }
        }
        $form['step_2'][$field]['#states'] = array(
          'visible' => array(
            $states,
          ),
        );
        // College id field is in a different place
        if ($field == 'college_id') {
          $form['step_2']['sales_funnel_widget']['#states'] =  array(
            'visible' => array(
              $states,
            ),
          );
          // Add a class to the sales funnel widget to target it in JS
          $form['step_2']['sales_funnel_widget']['#attributes']['class'][] = 'conditional';
        }
      }
    }

    // Add multi-step css and js
    $form['#attached']['css'][] = drupal_get_path('module', 'rcpar_mods') . '/css/multi-step.css';
    $form['#attached']['js'][] = drupal_get_path('module', 'rcpar_mods') . '/js/multi-step.js';
    // Include the highest step
    $form['#attached']['js'][] = array(
      'data' => array('rcpar_mods_lead_forms' => array('highest_step' => max($stepnumbers))),
      'type' => 'setting'
    );
  }

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => !empty($entity->submit_val) ? $entity->submit_val : t('Submit'),
    '#weight' => 200,
  );

  // Stylesheets
  $form['#attached']['css'][] = drupal_get_path('module', 'rcpar_mods') . '/css/marketing-form.css';
  $form['#attached']['css'][] = drupal_get_path('module', 'rcpar_mods') . '/css/token-webform.css';
  $form['#attached']['css'][] = drupal_get_path('module', 'rcpar_mods') . '/css/token-custom-form.css';
  // Monthpicker css and js
  $form['#attached']['js'][] = '/sites/all/libraries/jquery.monthpicker/jquery.mtz.monthpicker.js';
  $form['#attached']['js'][] = drupal_get_path('module', 'rcpar_mods') . '/js/month-picker.js';

  // Inline markup/css
  $form['css'] = array('#markup' => $entity->css);

  // Ultimately invokes templates/form--marketing_form.tpl.php
  $form['#theme'] = array('marketing_form');
  
  // Submit and validate handlers. This is necessary because the sales_funnel_widget adds some of its own
  $form['#submit'][] = 'rcpar_mods_entity_form_submit';
  $form['#validate'][] = 'rcpar_mods_entity_form_validate';

  return $form;
}

/**
 * User-facing entity form submit validator
 * - validates form fields
 */
function rcpar_mods_entity_form_validate(&$form, &$form_state) {
  // We are validating the form via jquery so this is a back up.
  // If the form is using conditional fields we only want to validate the fields
  // displayed to the user.
  $entity = $form_state['build_info']['args'][0];
  if ($entity->conditional_fields) {
    // Remove form errors for college sf account generated by sales_funnel_user_fields_widget_validate()
    // We will validate it in a bit.
    if (array_key_exists('sales_funnel_widget][field_college_sf_account][und', form_get_errors())) {
      sales_funnel_widget_form_unset_error('College field is required.');
    }
    if (array_key_exists('sales_funnel_widget][field_college_state_list][und', form_get_errors())) {
      sales_funnel_widget_form_unset_error('What state is your campus located in? field is required.');
    }
    $conditional_fields = json_decode($entity->dependent_fields, TRUE);
    $choice = $form_state['values']['conditional_fields']; // ie, student, professor, etc
    foreach ($conditional_fields as $field => $value) {
      if (in_array($choice, $value) && $value[$choice]) {
        if ($field == 'comments') { // comments field is not required
          break;
        }
        // validate college id field
        if ($field == 'college_id') {
          $college = SalesFunnelWidgetCollege::createFromFormVals($form_state);
          if (empty($college->getId()) && empty($college->getName())) {
            form_set_error($field, 'College is a required field.');
          }
          break;
        }
        // validate other fields
        if (empty($form_state['values'][$field])) {
          form_set_error($field, $form['step_2'][$field]['#title'] . ' is a required field.');
        }
      }
    }
  }
  // Otherwise validate all fields in the form.
  else {
    $entity_wrapper = entity_metadata_wrapper('form', $entity);
    foreach ($entity_wrapper->getIterator() as $wrapper) {
      $field_name = $wrapper->info()['name'];
      // Special handling for bdr_event_form - phone number field is not required.
      if ($entity_wrapper->machine_name->value() == 'bdr_event_form') {
        if ($field_name == 'phone') {
          continue;
        }
      }

      if (isset($form[$field_name]) && isset($form[$field_name]['#type']) && $form_state['values'][$field_name] == '') {
        form_set_error($field_name, $form[$field_name]['#title'] . ' is a required field.');
      }
    }
    // Validate email address
    if (!empty($form_state['values']['email']) && !valid_email_address($form_state['values']['email'])) {
      form_set_error('email', 'Please enter a valid email address.');
    }
    // Don't allow user to choose <International> or <US and Territories> from state dropdown.
    if ($form_state['values']['state'] == 'US Territories' || $form_state['values']['state'] == 'International') {
      form_set_error('state', 'Please choose a state.');
    }
    // Don't allow user to choose <Country> from country dropdown.
    if ($form_state['values']['country'] == 'International') {
      form_set_error('country', 'Please choose a country.');
    }
  }
  // File uploads validation
  if (isset($form_state['values']['upload_document']) && !strstr(request_uri(), 'system/ajax')) {
    $file = file_save_upload('upload_document', array(
      'file_validate_extensions' => array('jpeg jpg png pdf gif'),
      'file_validate_size' => array(2 * 1024 * 1024),
    ));
    $file->status = FILE_STATUS_PERMANENT;
    // Move the file to private directory.
    if ($file) {
      if ($file = file_move($file, 'private://partners/employment_verifications', FILE_EXISTS_RENAME)) {
        $form_state['values']['upload_document'] = $file;
      }
      else {
        form_set_error('upload_document', t('Failed to upload the file.'));
      }
    }
  }
}

/**
 * User-facing entity form submit handler
 * - Redirect form
 * - Display success message
 * - Send form values to Hubspot.
 */
function rcpar_mods_entity_form_submit($form, &$form_state) {
  $entity = $form_state['build_info']['args'][0];
  // Create an array of properties to be submitted to Hubspot.
  // Set redirect
  if ($entity->redirect) {
    $form_state['redirect'] = $entity->redirect;
  }

  // handle email of form submissions
  if ($entity->email_submissions) {
    $email_addresses = $entity->email_submissions;
    $emails = explode("\r\n", $email_addresses);
    $pages = $entity->email_page_urls;
    $form_state_values_to_ignore = array('form_build_id', 'form_id', 'form_token', 'op', 'submit', 'cloudamp__data__c', 'sales_funnel_widget');
    foreach ($form_state['values'] as $form_field => $value) {
      if (in_array($form_field, $form_state_values_to_ignore) || $value == '') {
        continue;
      }
      $params[$form_field] = $value;
    }
    // handle college data
    $s = SalesFunnelWidgetCollege::createFromFormVals($form_state);
    $params['college_state'] = $s->getState();
    $params['college'] = $s->getName();
    
    if (drupal_match_path(drupal_get_path_alias(),$pages) || $pages == '') {
      foreach ($emails as $email) {
        drupal_mail('rcpar_mods', 'entity_form_submission', $email, language_default(), $params);
      }
    }
  }

  // handle emailing of partner lead data
  if (!empty($form_state['values']['partner_id'])) {
    $partner_helper = new RCPARPartner();
    if ($partner_helper->isLoaded()) {
      $partner = $partner_helper->getWrapper();
      try {
        // Check if partner notification is enabled
        if ($partner->field_notify_new_enrollment->value()) {
          $field_notification_contacts = $partner->field_notification_contacts->value();
          if (count($field_notification_contacts)) {
            // Send lead submission to partner emails
            $params = $form_state['values'];
            $params['partner_name'] = $partner_helper->getNameToDisplay();
            $params['client_name'] = ($partner_helper->isPartnerTypeUniversity() || $partner_helper->isPartnerTypePublisher()) ? 'student' : 'employee';
            $params['is_partner_approval_required'] = 'No';
            if ($partner_helper->getShippingFlow() == 'pre-approval') {
              $params['is_partner_approval_required'] = 'Yes';
            }
            $to = implode(', ', $field_notification_contacts);
            drupal_mail('rcpar_partners', 'partner_lead', $to, language_default(), $params);
          }
        }
      }
      catch
      (EntityMetadataWrapperException $exc) {
        watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
    }
    if (isset($form_state['values']['upload_document'])) {
      $file = $form_state['values']['upload_document'];
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
    }
  }
  // save form submission in form storage entity
  // get id of submitted form entity and load it so we can get it's title later
  $form_entity_id = end(explode('_',$form_state['values']['form_id']));
  $form_submitted_entity = entity_load('form', array($form_entity_id));

  // create a new form storage entity
  $storage_entity = entity_create('form_storage', array('type' => 'form_storage'));
  $storage_wrapper = entity_metadata_wrapper('form_storage', $storage_entity);
  // give it a title
  $storage_wrapper->title = $form_submitted_entity[$form_entity_id]->title;
  // loop through submitted form values and save in form storage entity
  foreach ($form_state['values'] as $field_name => $field_value) {
    // add prefix for field
    $field = 'field_' . $field_name;
    // handle form values that are arrays
    if (is_array($field_value)) {
      // sales funnel widget is special
      if ($field_name == 'sales_funnel_widget') {
        $college = SalesFunnelWidgetCollege::createFromFormVals($form_state);
        $storage_wrapper->field_college_id = $college->getName();
      }
      // what are you interested in is special
      if ($field_name == 'what_are_you_interested_in_learning_more_about') {
        $storage_wrapper->field_what_are_you_interested_in = implode(', ', $field_value);
      }
      // all other array fields are not special
      else {
        $field_value = implode(', ', $field_value);
      }
    }
    // special handling for partner id field
    if ($field_name == 'partner_id') {
      $field_value = $partner_helper->getNid();
    }
    // special handling for country field
    if ($field_name == 'country') {
      $storage_wrapper->field_country_international = $field_value;
    }
    if (isset($storage_wrapper->$field)) {
      // special handling for document upload field
      if ($field_name == 'upload_document') {
        $storage_wrapper->$field = $field_value->fid;
      }
      // all the other fields
      else {
        $storage_wrapper->$field = $field_value;
      }
    }
  }
  // If hubspot is exist, the form needs to be mapping
  if(module_exists("hubspot")){
    hubspot_custom_form_sent($form_state);
    // TODO: The Ticket DEV-1946 will change the forms to iframe and this code needs to be remove.
  }
  $storage_wrapper->save();
  

  // Success message
  drupal_set_message($entity->submit_msg);
}
