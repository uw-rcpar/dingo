<?php
/**
 * @file
 * This file is intended to separate out code to update order totals for free access orders using a batch process
 */

/**
 * Custom batch process
 * Update free orders with total > 0
 */
function rcpar_mods_fix_free_orders_form()
{
  $form = array();
  $form['text'] = array('#type' => 'item', '#markup' => t('Go on, push that button'));
  $form['submit'] = array('#type' => 'submit', '#value' => t('Click here to Start'));
  return $form;
}

/**
 * Custom batch process submit
 * Update free orders with incorrect totals
 */
function rcpar_mods_fix_free_orders_form_submit($form, $form_state)
{
  $batch = array(
      'title' => t('Processing ...'),
      'operations' => array(),
      'init_message' => t('Initializing....'),
      'progress_message' => t('Processed @current out of @total.'),
      'error_message' => t('An error occurred during processing'),
      'finished' => 'rcpar_mods_fix_free_orders_batch_finished',
  );
  $progress = 0; // Where to start
  $limit = 10; // How many to process for each run
  // Get the naughty orders. NOTE: this view returns free access orders with no associated credit card or affirm payments
  // TODO sort through free access orders that have payment information and decide how they should be modified
  $view = views_get_view('asl_orders');
  $view->execute('page');

  // Loop over the naughty orders and send to batch
  foreach ($view->result as $result) {
    $batch['operations'][] = array('rcpar_mods_fix_free_orders_process', array($progress, $limit, $result));
    $progress = $progress + $limit;

  }
  // Batch-slap the troublesome orders yo
  batch_set($batch);
  batch_process('admin/settings/rcpar/system_settings/fix-free-orders'); // page to return to after completes

}


/**
 * Dig into the orders here. Progress and limit is updated during each run
 * @param $progress int
 * @param $limit int
 * @param $result obj passed by view
 * @param $context
 */
function rcpar_mods_fix_free_orders_process($progress, $limit, $result, &$context)
{
  $order = commerce_order_load($result->order_id); // Load the order
  $o = new RCPARCommerceOrder($order); // Leverage the custom class goodies
  // Wrap that rascal so we can get to the line items
  $order_wrapper = entity_metadata_wrapper('commerce_order', $result->order_id);
  $order_line_items = $order_wrapper->commerce_line_items->value();

  // Need to initialize some stuff
  $discount = 0;
  $line_item_id = 0;
  // Loop through each line item in order
  foreach ($order_line_items as $order_line_item) {
    $price = 0;

    // Quantity should always be 1
    if ($order_line_item->quantity > 1) {
      $order_line_item->quantity = 1;
      $price = $order_line_item->commerce_unit_price['und']['0']['amount'];
      $order_line_item->commerce_total['und']['0']['amount'] = $price;
      $order_line_item->commerce_total['und']['0']['data']['components']['0']['price']['amount'] = $price;
      commerce_line_item_save($order_line_item);
    }

    // None of these orders should have been shipped. Set shipping cost to $0
    if ($order_line_item->type == 'shipping' && $order_line_item->commerce_unit_price['und']['0']['amount'] > 0) {
      $order_line_item->commerce_unit_price['und']['0']['amount'] = 0;
      $order_line_item->commerce_total['und']['0']['data']['components']['0']['price']['amount'] = 0;
      commerce_line_item_save($order_line_item);
    }

    // Free access used several different discount types. Need to note the line item id of the discount
    switch ($order_line_item->line_item_label) {
      case 'PARTNER-DIS':
        $line_item_id = $order_line_item->line_item_id;
        break;
      case 'FREETRIAL-BUNDLE':
        $line_item_id = $order_line_item->line_item_id;
        break;
      case 'FULL-DIS':
        $line_item_id = $order_line_item->line_item_id;
        break;
      default:
        // Count up all line items amounts that are not a discount
        $price = $order_line_item->commerce_unit_price['und']['0']['amount'];
        // We want the discount to equal the sum of all the items in the order
        // Only increment the discount amount if we have a positive line item price. In edge cases and mythology, negative line items are known to exist
        if ($price > 0) {
          $discount += $price;
        }
    }
  }

  // No discount line item makes the baby jesus cry. Let's add our own
  if ($line_item_id == 0) {

    // Fear of commitment motivates us to use the sku name PARTNER-DIS instead of hard coding an ID
    $product = commerce_product_load_by_sku('PARTNER-DIS');
    // Setup the new line item
    $line_item = commerce_product_line_item_new($product, 1, $result->order_id);
    // Save the line
    commerce_line_item_save($line_item);
    // Add it to the line items
    $order_wrapper->commerce_line_items[] = $line_item;
    // There are several ways to save this line to the order, but this one worked best without applying total calculations
    $order_wrapper->save();
    // Get the newly generated discount line item id
    $line_item_id = $line_item->line_item_id;

  }

  // Discount needs to be a negative number
  $discount = $discount * -1;
  $line_item = commerce_line_item_load($line_item_id);
  // Apply the discount to the line item
  $line_item->commerce_unit_price['und']['0']['amount'] = $discount;
  $line_item->commerce_total['und']['0']['amount'] = $discount;
  $line_item->commerce_total['und']['0']['data']['components']['0']['price']['amount'] = $discount;
  commerce_line_item_save($line_item); // Save it

  // Finally we need to adjust the order total to $0
  $order->commerce_order_total['und']['0']['amount'] = 0;
  $i = 0;
  // There can be extra items like shipping and tax on the order total
  // We will loop over them and set them all to $0
  foreach ($order->commerce_order_total['und']['0']['data']['components'] as $component) {
    $order->commerce_order_total['und']['0']['data']['components'][$i]['price']['amount'] = 0;
    $i++;
  }
  // Once again, commerce_order_save did funky calculations we don't want. This update function works better
  field_attach_update('commerce_order', $order);


  // Update progress for message
  $progress = $progress + $limit;
  // Add the order id to the results to output when batch process is finished
  $context['results'][] = $result->order_id;
  // Update message during each run so you know where you are in the process
  $context['message'] = 'Now processing ' . $progress . ' modified.';

}

// Finish up the batch process and let the user know if everything is cool or blown up
function rcpar_mods_fix_free_orders_batch_finished($success, $results, $operations)
{
  if ($success) {
    drupal_set_message('Import is complete');
    // Dump out all the order ids
    foreach ($results as $result) {
      drupal_set_message($result);
    }
  } else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}
