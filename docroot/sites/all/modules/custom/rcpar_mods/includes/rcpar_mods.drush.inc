<?php

/**
 * Implementation of hook_drush_command().
 */
function rcpar_mods_drush_command() {
  $items = array();
  $items['export-feedback'] = array(
    'callback' => 'rcpar_mods_feedback_generate_report',
    'description' => 'Drush command to export weekly feedback.',
    'aliases' => array('ef'),
  );
  return $items;
}

/**
 * Create a sql dump of the last week's feedback and replace escape characters not supported by UWorld
 */
function rcpar_mods_feedback_generate_report() {
  // Get database credentials
  global $databases;
  $un = $databases['default']['default']['username'];
  $pw = $databases['default']['default']['password'];
  $db = $databases["default"]["default"]["database"];
  $h = $databases["default"]["default"]["host"];
  // This cron job will run every Monday morning, so we'll add a where clause to our query for the last seven days
  $where = '--where "created >= UNIX_TIMESTAMP(NOW() - INTERVAL 7 DAY)"';
  // Location for our dump files
  $backup_dir = drupal_realpath('public://');
  // Assemble the mysql dump command
  $cmd = "mysqldump -u $un -p$pw -h $h $db rcpar_feedback $where > $backup_dir/rcpar_feedback.sql";
  // Tell PHP to tell MySQL we want a dump file
  exec($cmd,$output, $return);

  // Return will return non-zero on an error
  if (!$return) {
    // If all went well write the file to a string variable
    $str = file_get_contents($backup_dir.'/rcpar_feedback.sql');
    // Replace escaped single quote with double single quotes
    $str = str_replace("\'","''", $str);
    // Remove the backslash escape character for double quotes
    $str = str_replace('\"','"', $str);
    // Name the dump file with today's date
    $sufix = date("Y-m-d");
    $filename = 'rcpar_feedback'. $sufix . '.sql';
    $file = $backup_dir . '/' . $filename;
    // Write it to the disc
    file_put_contents($file, $str);
    // Send it to whoever needs it
    rcpar_mods_feedback_notify_content_team($backup_dir, $filename);
    drush_print($filename . ' successfully generated.');
  }
  else {
    // Oops
    drush_log('Suckage detected: Error '.$return. ' on mysqldump.', 'error');
  }
}
/**
 * Called by drush weekly on a cron job to send a feedback report to UWorld
 * @param $filepath <sting> the path to the file
 * @param $filename <sting> filename
 * return <void>
 */
function rcpar_mods_feedback_notify_content_team($filepath, $filename) {

  // Fill out our postcard for the mail
  $recipients = variable_get('content_recipients', '');
  $to = str_replace("\r\n", ",", $recipients);
  $from = variable_get('site_mail', ''); //admin's mail address

  $body = "Hello Team, attached please find this week's feedback. Thank you.";

  // Attach the report
  $attachment = array(
    'filecontent' => file_get_contents($filepath.'/'.$filename),
    'filename' => $filename,
    'filemime' => 'text/csv'
  );

  $params = array(
    'key' => 'feedback',
    'to' => $to,
    'from' => $from,
    'subject' => 'Student Feedback Report',
    'body' => $body,
    'attachment' => $attachment
  );

  // Go postal
  drupal_mail('rcpar_mods', 'feedback', $to, language_default(), $params, $from);
  drush_print($filename . ' successfully generated and mailed.');
}