<?php

/**
 * Feedback form appears on IPQ quizes and Courseware lectures
 * @param $form
 * @param $form_state
 * @return array
 */
function rcpar_mods_feedback_form($form, &$form_state) {
  $form = array();
  $form['#prefix'] = '<div id="feedback"><div id="feedback-form" style="display:none;" class="col-xs-4 col-md-4 panel panel-default">';
  $form['#suffix'] = '</div><div id="feedback-tab"><i class="fa fa-comment fa-flip-horizontal"></i> Feedback</div></div>';
  $form['feedback'] = array(
    '#type' => 'textarea',
    '#title' => t('<i class="fa fa-comment fa-flip-horizontal"></i> Feedback <span class="feedback-close" >x</span>'),
    '#attributes' => array(
      'placeholder' => t('Share your feedback on our content, questions and software. Please provide as much detail as possible, and avoid simplistic feedback such as “Good Question,” “Hard Question,” etc. For CPA Exam tutoring and support, please visit the Homework Help Center located in your Student Dashboard (available for Premier and Elite Course students).'),
    ),
  );
  // Storing the value of the current path
  $form['path'] = array(
    '#type' => 'value',
    '#value' => current_path(),
  );
  $form['is_technical'] = array(
    '#type' => 'checkbox',
    '#title' => t('This is a software issue'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#id' => 'confirm_submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Submit callback for the feedback form
 * @param $form
 * @param $form_state
 * @throws Exception
 */
function rcpar_mods_feedback_form_submit($form, &$form_state){
  global $user;
  // Set some defaults
  $type = '';
  $question_type = '';
  $chapter_id = 0;
  $topic_ids = '';
  $question_id = 0;
  $chapter = '';
  $topics = '';
  $version = 0;
  $author = 'NA';
  $is_technical = $form_state['values']['is_technical'];
  $current_path = $form_state['values']['path'];
  $feedback = $form_state['values']['feedback'];
  $created = strtotime("now");
  $student_email = $user->mail;

  // Default version tid
  $exam_version = exam_version_get_default_version();
  $exam_years = exam_versions_years();
  $version = $exam_years[$exam_version];


  // Check for courseware feedback
  if (strpos($current_path,'study/') !== false || strpos($current_path,'study-cram/') !== false) {
    $type = (strpos($current_path,'cram') !== false) ? 'Online Cram Course' : 'Online Course';
    $slugs = explode('/', $current_path);
    $topic_data =_courseware_load_topic_by_slugs($type, $slugs[1], $slugs[2], $slugs[3], $exam_version='');
    $topic_ids = $topic_data['topic']->nid;
    // Get the section and topic number from the url
    $topic_number = $slugs[1] . ' ' . $slugs[3];
    $topics = $topic_number . ' ' .$topic_data['topic']->title;
    $result = db_query("SELECT n.nid
    FROM node n
    INNER JOIN field_data_field_topic_per_exam_version echapter on echapter.entity_id = n.nid
    INNER JOIN field_data_field_topic_reference topic on topic.entity_id = echapter.field_topic_per_exam_version_target_id
    WHERE n.type = 'rcpa_chapter'
    AND n.status = 1
    AND topic.field_topic_reference_target_id = :nid",
      array(':nid' => $topic_ids));

    $chapter = null;
    foreach ($result as $record) {
      $chapter_id = $record->nid;
      $chapter  = node_load($record->nid)->title;
      break;
    }
  };
  // Check for IPQ feedback
  if (strpos($current_path,'ipq/') !== false && isset($_SESSION['ipq_session'])) {
    $topics_array = array();
    $type = 'IPQ';
    $question_id = $_SESSION['ipq_session']['session_config']['current_question_id'];
    $question = node_load($question_id);
    $result  = db_query("SELECT chapter_id, topic_id FROM ipq_question_pool WHERE ipq_questions_id = :qid AND exam_version_id = :exid",
      array(':qid' => $question_id, ':exid' => $version));

    foreach ($result as $record) {
      // Need chapter info to assemble section and topic number
      $course_info = db_select('course_stats', 'cs')
        ->fields('cs')
        ->condition('type', 'CHAPTER')
        ->condition('nid', $record->chapter_id)
        ->execute()->fetchObject();

      $chapter_id = $record->chapter_id;
      $topics_array['id'][] = $record->topic_id;
      $node = node_load($record->topic_id);
      // Section and topic number
      $topic_number = rcpar_mods_feedback_get_related_topic_number($question, $exam_version);
      $topics_array['title'][] = "{$course_info->section} {$course_info->weight}{$topic_number} {$node->title}";
    }
    $chapter = node_load($chapter_id)->title;
    $question_node = node_load($question_id);
    $question_type = $question_node->type;
    $author = $question_node->name;
    $topic_ids = implode(',',$topics_array['id']);
    $topics = implode(',', $topics_array['title']);
  }

  // Pack it up
  $data = array(
    'uid' => $user->uid,
    'type' => $type,
    'question_type' => $question_type,
    'chapter_id' => $chapter_id,
    'topic_ids' => $topic_ids,
    'question_id' => $question_id,
    'chapter' => $chapter,
    'topics'=> $topics,
    'version' => $version,
    'is_technical' => $is_technical,
    'author' => $author,
    'feedback' => $feedback,
    'status' => 'Open',
    'created' => $created,
    'student_email' => $student_email,
  );

  // Stick it in the fridge
  db_insert('rcpar_feedback')
    ->fields($data)
    ->execute();

  // Software issue? The kids in Customer Care can't wait to hear about it
  if ($data['is_technical'] == 1) {
    rcpar_mods_feedback_notify_customer_care($data);
  }
}

/**
 * Get the topic number based on the question node
 * @param $question node
 * @param $exam_version string
 * @return string
 */
function rcpar_mods_feedback_get_related_topic_number($question, $exam_version) {
  $topic_number = '';
  try{
    $question_wrapper = entity_metadata_wrapper('node', $question);
    foreach ($question_wrapper->field_section_per_exam_version as $sec_per_ev) {
      if ($sec_per_ev->field_exam_version_single_val->name->value() == $exam_version) {
        // We load the chapter since the weight to display is not based on
        // the question but based on the chapter reference
        $chapter = ipq_common_get_chapter_by_question($question, $exam_version);
        if (!is_null($chapter)) {
          // Load all topics related to our chapter and find the referenced number
          // to that particular topic.
          $topics = ipq_common_get_topics_by_chapter_nid($chapter->nid, $exam_version);
          foreach ($sec_per_ev->field_ipq_topic->getIterator() as $t) {
            $t_delta = '';
            foreach ($topics as $delta => $topic) {
              if ($topic->title == $t->title->value()) {
                // We add 1 to delta since the desired reference is from 1
                // and the array keys start on 0
                $t_delta = '.' . sprintf("%02s", $delta + 1);
              }
            }
            // Got it
            $topic_number = $t_delta;
          }
        }
      }
    }
  } catch (EntityMetadataWrapperException $e) {
    watchdog(__FILE__, "function ".__FUNCTION__." entity error");
  }
  return $topic_number;
}

/**
 * Notification emails that go out to customer care
 * @param $data <array> - all the data that was gathered and stored with rcpar_mods_feedback_form_submit()
 */
function rcpar_mods_feedback_notify_customer_care($data) {
  if (isset($data['uid'])) {
    $user = user_load($data['uid']);
  }
  else {
    global $user;
  }

  global $base_url;

  // Technical feedback goes to customer care
  $team = 'Tech Team Member';
  $feedback_details = '';

  if ($data['question_id'] != 0) {
    $node = node_load($data['question_id']);
    $feedback_details = '<p><strong>Question link: </strong><a href="' . $base_url . '/node/'.$data['question_id'].'/edit" > ' . $node->title . '</a></p>';
    $feedback_details .= '<p><strong>Question type: </strong> '. $node->type .'</p>';
  }

  if ($data['topics']) {
    $feedback_details .= '<p><strong>Topic(s): </strong> ' .$data['topics']. '</p>';
  }


  $body = '<p>Hello ' . $team . ',</p>';
  $body .= '<p>A student has posted feedback regarding the '. $data["type"] . ' on Roger CPA Review.</p>';
  $body .= '<p><strong>Reporting student: </strong>' . $user->mail . '</p>';
  $body .= $feedback_details;
  $body .= '<p><strong>Feedback:</strong></p>';
  $body .= '<p>' . $data['feedback'] . '</p>';
  $body .= '<p><em><strong>Roger CPA Review Team</strong></em></p>';


  //prepare and send email
  $recipients = variable_get('technical_recipients', '');
  $to = str_replace("\r\n", ",", $recipients);
  $from = variable_get('site_mail', ''); //admin's mail address


  drupal_mail('rcpar_mods', 'feedback', $to, language_default(), array(
    'body'    => $body,
    'subject' => 'Student Feedback'
  ), $from);
}

/**
 * This is a detail form accessible from the feedback view. It will display and add notes to feedback submissions.
 * @param $form
 * @param $form_state
 * @param $feedback_id <int> The id from the custom feedback table, passed in the url
 * @return mixed
 */
function rcpar_mods_feedback_detail_form($form, &$form_state, $feedback_id) {

  $note_html = '';
  // Get the feedback
  $record = db_query("SELECT uid, type, feedback FROM rcpar_feedback where id = :feedback_id;",
    array(':feedback_id' => $feedback_id))->fetchAssoc();

  // Get user email
  $user = user_load($record['uid']);

  $html = '<br><strong>User: </strong> ' .$user->mail . '<br>';
  $html .= '<strong>Type: </strong>' .$record['type'] . '<br>';
  $html .= '<strong>Feedback: </strong><br>';
  $html .= $record['feedback'] . '</p>';
  // Display the feedback info
  $form['markup'] = array(
    '#markup' => $html,
  );
  // Get the associated notes
  $notes = $record = db_query("SELECT uid, note, created FROM rcpar_feedback_notes WHERE feedback_id = :feedback_id;",
    array(':feedback_id' => $feedback_id));

  // If there are notes show em
  if ($notes) {
    $note_html = '<h3>Notes</h3>';
    // Format the notes
    foreach ($notes as $note) {
      $account = user_load($note->uid);
      $note_html .= '<br><strong>Author: </strong> ' .$account->mail . '<br>';
      $note_html .= '<strong>Created: </strong>' .$note->created . '<br>';
      $note_html .= '<strong>Note: </strong><br>';
      $note_html .= $note->note . '</p>';
    }
  }
  // Display notes already submitted
  $form['note_markup'] = array(
    '#markup' => $note_html,
  );

  // Store the feedback_id
  $form['feedback_id'] = array(
    '#type' => 'value',
    '#value' => $feedback_id,
  );

  //Add note
  $form['note'] = array(
    '#type' => 'textarea',
    '#title' => t('Please enter your notes'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Submit handler to store notes associated with feedback
 *
 * @param $form
 * @param $form_state
 * @throws Exception
 */
function rcpar_mods_feedback_detail_form_submit($form, &$form_state) {
  global $user;
  $uid = $user->uid;
  $created = strtotime("now");

  $data = array(
    'feedback_id' => $form_state['values']['feedback_id'],
    'uid' => $uid,
    'note' => $form_state['values']['note'],
    'created' => $created,
  );

  db_insert('rcpar_feedback_notes')
    ->fields($data)
    ->execute();
}


function rcpar_mods_status_form($form, &$form_state) {
  $form['status_options'] = array(
    '#type' => 'value',
    '#value' => array('Open' => t('Open'),
      'Resolved' => t('Resolved'),
      'Not Relevant' => t('Not Relevant'))
  );
  $form['status'] = array(
    '#title' => t('Status'),
    '#type' => 'select',
    '#options' => $form['type_options']['#value'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
}








