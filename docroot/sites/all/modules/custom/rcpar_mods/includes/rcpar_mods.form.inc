<?php

/**
 * Validate machine name.
 */
function rcpar_mods_form_machine_name_validate($element, &$form_state) {
  // Check for valid formatting first
  if (!preg_match('!^[a-z0-9_]+$!', $form_state['values']['machine_name'])) {
    form_set_error('machine_name', t('The machine-readable name must contain only lowercase letters, numbers, and underscores.'));
    return;
  }

  $query = db_select('eck_form')
    ->condition('machine_name', $form_state['values']['machine_name'])
    ->range(0, 1);

  // If this is an entity that already exists, make sure we don't include its own machine name in the search
  if (!empty($form_state['form']->id)) {
    $query->condition('id', $form_state['form']->id, '<>');
  }

  // If we have any query results, the machine name already exists
  $count = $query->countQuery()->execute()->fetchField();
  if ($count > 0) {
    form_set_error('machine_name', t('The machine-readable name has been taken. Please pick another one.'));
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Overrides for form display when editing a 'form' entity as an admin.
 * This simply calls the alter hook for the 'create' version of the form, which makes the
 * same needed modifications
 */
function rcpar_mods_form_eck__entity__form_edit_form_form_alter(&$form, &$form_state) {
  rcpar_mods_form_eck__entity__form_add_form_form_alter($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter().
 * Overrides for form display for an admin when creating a 'form' entity as an admin.
 */
function rcpar_mods_form_eck__entity__form_add_form_form_alter(&$form, &$form_state) {
  // Load the entity wrapper.
  $entity_type = 'form';
  $entity = $form_state['form'];
  $entity_wrapper = entity_metadata_wrapper($entity_type, $entity);

  // Add a validator for machine name
  $form['machine_name']['#element_validate'] = array('rcpar_mods_form_machine_name_validate');
  $form['machine_name']['#description'] = 'A unique ID to identify this form. Lowercase letters and underscores only.';

  // Submit button value - sensible default
  if(empty($entity->submit_val)) {
    $form['submit_val']['#default_value'] = 'Submit';
  }

  // CSS - make textarea and add description
  $form['css']['#type'] = 'textarea';
  $form['css']['#description'] = 'Arbitrary CSS or HTML injected before the form. Use &lt;style&gt; tags if needed.';

  // Email submissions - make textarea and add description
  $form['email_submissions']['#type'] = 'textarea';
  $form['email_submissions']['#title'] = 'Email addresses that should receive form submissions';
  $form['email_submissions']['#rows'] = '3';
  $form['email_submissions']['#description'] = 'Enter email addresses that should receive form submissions, one on each line. If left blank, form submissions will not be emailed to anyone.';


  // Email page urls - make textarea and add description
  $form['email_page_urls']['#type'] = 'textarea';
  $form['email_page_urls']['#title'] = 'Page URLs from which to email form submissions';
  $form['email_page_urls']['#rows'] = '3';
  $form['email_page_urls']['#description'] = 'Limit which pages the form submissions are emailed from by entering their path(s), one on each line. Do not include 
    a leading slash. If left blank, and <strong>Email addresses that should receive form submissions</strong> field above has an email address in it, the form\'s submissions will be emailed from all pages that contains it.';

  // Improved form field descriptions
  $form['redirect']['#description'] = 'The page to redirect the user to after a successful submission. Do not include 
    a leading slash. If left blank, the user will stay on the same page. Example: <em>cpa-courses/mobile-app</em>';
  $form['description']['#description'] = 'A short description explaining the general purpose of this form. This value is not 
    seen by users.';
  $form['submit_msg']['#description'] = 'A message that will be shown to users after a successful submission. HTML is allowed.';
  $form['submit_msg']['#maxlength'] = '255';
  $form['title']['#description'] = 'A human-readable label for admins to identify this form. Not shown to users.';

  // Fields not required
  $form['description']['#required'] = FALSE;
  $form['redirect']['#required'] = FALSE;
  $form['css']['#required'] = FALSE;
  $form['email_submissions']['#required'] = FALSE;
  $form['email_page_urls']['#required'] = FALSE;

  // Disable the default textfield widget for some fields
  $form['layout']['#type'] = 'hidden';
  $form['layout']['#required'] = FALSE;
  $form['weight']['#type'] = 'hidden';
  $form['weight']['#required'] = FALSE;
  $form['steps']['#type'] = 'hidden';
  $form['steps']['#required'] = FALSE;
  $form['dependent_fields']['#type'] = 'hidden';
  $form['dependent_fields']['#required'] = FALSE;

  // Fieldset for organization
  $form['field_options'] = array(
    '#type' => 'fieldset',
    '#title' => 'Field options',
    '#description' => 'Determines which fields on the form will be shown to the user.',
    '#weight' => 500,
  );

  // Move the save button to the bottom of the form
  $form['actions']['#weight'] = 10000;

  // Multi step field:
  $form['multistep']['#weight'] = 30; // adjust weight
  $form['multistep']['#type'] = 'radios'; // change the form field type to radios
  $form['multistep']['#options'] = array('No', 'Yes'); // give the radios yes and no options
  // If this is the edit form, set the default value to the value in the form state.
  if (isset($form_state['form']->multistep)) {
    $form['multistep']['#default_value'] = $form_state['form']->multistep;
  }
  // Otherwise give a default value of 0 (No).
  else {
    $form['multistep']['#default_value'] = 0;
  }

  // Conditional fields
  $form['conditional_fields']['#weight'] = 30; // adjust weight
  $form['conditional_fields']['#type'] = 'radios'; // change the form field type to radios
  $form['conditional_fields']['#options'] = array('No', 'Yes'); // give the radios yes and no options
  $form['conditional_fields']['#states'] = array( // make its visibility dependent on multi-step field.
    'visible' => array(
      ':input[name="multistep"]' => array('value' => 1),
    ),
  );
  // If this is the edit form, set the default value to the value in the form state.
  if (isset($form_state['form']->conditional_fields)) {
    $form['conditional_fields']['#default_value'] = $form_state['form']->conditional_fields;
  }
  // Otherwise give a default value of 0 (No).
  else {
    $form['conditional_fields']['#default_value'] = 0;
  }

  // Get saved values for the field layouts from JSON, if we have them
  $layout_defaults = !empty($entity->layout) ? json_decode($entity->layout, TRUE) : array();
  $steps_defaults = !empty($entity->steps) ? json_decode($entity->steps, TRUE) : array();
  $weight_defaults = !empty($entity->weight) ? json_decode($entity->weight, TRUE) : array();
  $conditional_fields_defaults = !empty($entity->dependent_fields) ? json_decode($entity->dependent_fields, TRUE) : array();
  $email_submissions = !empty($entity->email_submissions) ? $entity->email_submissions : array();
  $email_page_urls = !empty($entity->email_page_urls) ? $entity->email_page_urls : array();

  // If this is the edit form, set the default value for email_submissions and email_page_urls
  if (isset($form_state['form']->email_submissions)) {
    $form['email_submissions']['#default_value'] = $email_submissions;
  }
  if (isset($form_state['form']->email_page_urls)) {
    $form['email_page_urls']['#default_value'] = $email_page_urls;
  }

  // Iterate through the properties of the entity and override form fields.
  foreach ($entity_wrapper->getIterator() as $valueWrapper) {
    // Get the property info.
    $property_info = $valueWrapper->info();
    // Get the property name.
    $property_name = $property_info['name'];

    // Don't mess with certain properties
    if (in_array($property_name, [
      'title', 'machine_name', 'uid', 'created', 'changed', 'description',
      'submit_msg', 'redirect', 'submit_val', 'css', 'email_submissions', 'email_page_urls', 'layout', 'weight', 'multistep', 'steps', 'conditional_fields', 'dependent_fields'])) {
      continue;
    }

    // If the property name exists in the form:
    if (isset($form[$property_name]['#type'])) {
      $form[$property_name]['#type'] = 'radios'; // change the form field type to radios
      $form[$property_name]['#options'] = array('No', 'Yes'); // give the radios yes and no options
      // If this is the edit form, set the default value to the value in the form state.
      if (isset($form_state['form']->$property_name)) {
        $form[$property_name]['#default_value'] = $form_state['form']->$property_name;
      }
      // Otherwise give a default value of 1 (Yes).
      else {
        $form[$property_name]['#default_value'] = 1;
      }
      // Make a layout option for this field
      $form[$property_name]["{$property_name}_layout"] = array(
        // Layout option is only visible if "yes" is selected on the form option
        '#states' => array(
          'visible' => array(
            ':input[name="' . $property_name . '"]' => array('value' => 1),
          ),
        ),
        '#title' => 'Layout',
        '#weight' => 20,
        '#type' => 'radios',
        '#options' => array(
          'l' => 'Left',
          'c' => 'Center',
          'r' => 'Right',
        ),
        '#default_value' => 'c',
      );

      // Set the default value from the saved value if there is one
      if(!empty($layout_defaults[$property_name])) {
        $form[$property_name]["{$property_name}_layout"]['#default_value'] = $layout_defaults[$property_name];
      }

      // Make a weight option for this field
      $form[$property_name]["{$property_name}_weight"] = array(
        // Layout option is only visible if "yes" is selected on the form option
        '#states' => array(
          'visible' => array(
            ':input[name="' . $property_name . '"]' => array('value' => 1),
          ),
        ),
        '#title' => 'Weight',
        '#weight' => 25,
        '#type' => 'textfield',
      );

      // Set the default value from the saved value if there is one
      if(!empty($weight_defaults[$property_name])) {
        $form[$property_name]["{$property_name}_weight"]['#default_value'] = $weight_defaults[$property_name];
      }

      // Make a dependency option for this field
      $form[$property_name]["{$property_name}_dependent_fields"] = array(
        // Dependency option is only visible if "yes" is selected on the form option
        '#states' => array(
          'visible' => array(
            ':input[name="' . $property_name . '"]' => array('value' => 1),
          ),
        ),
        '#title' => 'Dependency',
        '#weight' => 30,
        '#type' => 'select',
        '#options' => array(
          '0' => 'None',
          '1' => 'Expanded Audience Type',
        ),
        '#default_value' => '0',
        '#prefix' => '<div class="audience-wrapper">',
        '#suffix' => '</div>',
      );


      // Make an Expanded Audience Type option for this field
      $form[$property_name]["{$property_name}_expanded_audience"] = array(
        '#title' => 'Type',
        '#weight' => 35,
        '#type' => 'checkboxes',
        '#options' => array(
          'student' => 'Student interested in a CPA',
          'professional' => 'Professional interested in a CPA',
          'professor' => 'Professor',
          'firm' => 'Firm Administrator',
        ),
        '#default_value' => 'student',
        '#prefix' => '<div class="type-wrapper edit-' . drupal_html_class($property_name) . '-dependent-fields">',
        '#suffix' => '</div>',
      );

      // Set the default value from the saved value if there is one
      if(!empty($conditional_fields_defaults[$property_name])) {
        $form_state_dependent_fields = json_decode($form_state['form']->dependent_fields, TRUE);
        $form[$property_name]["{$property_name}_dependent_fields"]['#default_value'] = '0';
        foreach ($form_state_dependent_fields[$property_name] as $key => $value) {
          if ($value) {
            $form[$property_name]["{$property_name}_dependent_fields"]['#default_value'] = '1';
            $form[$property_name]["{$property_name}_expanded_audience"]['#default_value'] = $form_state_dependent_fields[$property_name];
            break;
          }
        }
      }


      // Make a steps option for this field
      $form[$property_name]["{$property_name}_steps"] = array(
        // Steps option is only visible if "yes" is selected on the form option
        '#states' => array(
          'visible' => array(
            ':input[name="' . $property_name . '"]' => array('value' => 1),
          ),
        ),
        '#title' => 'Step',
        '#weight' => 20,
        '#type' => 'select',
        '#options' => array(
          '1' => 'Step One',
          '2' => 'Step Two',
          '3' => 'Step Three',
        ),
        '#default_value' => '1',
        '#prefix' => '<div class="steps-wrapper">',
        '#suffix' => '</div>',

      );

      // Set the default value from the saved value if there is one
      if(!empty($steps_defaults[$property_name])) {
        $form[$property_name]["{$property_name}_steps"]['#default_value'] = $steps_defaults[$property_name];
      }
      // Move to the fieldset, except for Multi Step form option and Conditional Fields option.
      if ($property_name != 'multistep' || $property_name != 'conditional_fields') {
        $form['field_options'][$property_name] = $form[$property_name];
        unset($form[$property_name]);
      }

    }
  }

  // Attach js file
  $form['#attached']['js'][] = drupal_get_path('module', 'rcpar_mods') . '/js/lead-forms.js';
  // Add a custom submit handler
  $form['#validate'][] = 'rcpar_mods_entity_form_admin_validate';
}

/**
 * Custom submit handler for forms:
 * eck__entity__form_add_form
 * eck__entity__form_edit_form
 *
 * Process layout options into JSON for storage before submit. Apparently validate hooks are the best
 * place to programmatically alter form values (via form_set_value())
 */
function rcpar_mods_entity_form_admin_validate($form, &$form_state) {
  $entity = entity_create('form', array('type' => $form['#bundle']));
  $entity_wrapper = entity_metadata_wrapper('form', $entity);

  // Initialize layout, weight and conditional fields JSON
  $layout_json = array();
  $weight_json = array();
  $conditional_fields_json = array();
  $email_submissions_json = array();
  $email_page_urls_json = array();

  foreach ($entity_wrapper->getIterator() as $valueWrapper) {
    // Get the property name from its info
    $property_name = $valueWrapper->info()['name'];
    if ($valueWrapper->info()['name']=='email_submissions') {
      $email_submissions = $form['email_submissions']['#value'];
    }
    if ($valueWrapper->info()['name']=='email_page_urls') {
      $email_page_urls = $form['email_page_urls']['#value'];
    }
    // Store a layout value only if its corresponding field is activated
    if(isset($form_state['values']["{$property_name}_layout"]) && !empty($form_state['values'][$property_name])) {
      $layout_json[$property_name] = $form_state['values']["{$property_name}_layout"];
    }

    // Store a steps value only if its corresponding field is activated
    if(isset($form_state['values']["{$property_name}_steps"]) && !empty($form_state['values'][$property_name])) {
      $steps_json[$property_name] = $form_state['values']["{$property_name}_steps"];
    }

    // Store a weight value only if its corresponding field is activated
    if(isset($form_state['values']["{$property_name}_weight"]) && !empty($form_state['values'][$property_name])) {
      $weight_json[$property_name] = $form_state['values']["{$property_name}_weight"];
    }
    // Store a conditional fields value only if its corresponding field is activated
    if(isset($form_state['values']["{$property_name}_dependent_fields"]) && !empty($form_state['values'][$property_name])) {
      $conditional_fields_json[$property_name] = $form_state['values']["{$property_name}_expanded_audience"];
    }
  }

  // Inject values into the hidden layout field
  form_set_value($form['layout'], json_encode($layout_json), $form_state);
  form_set_value($form['steps'], json_encode($steps_json), $form_state);
  form_set_value($form['weight'], json_encode($weight_json), $form_state);
  form_set_value($form['dependent_fields'], json_encode($conditional_fields_json), $form_state);
  form_set_value($form['email_submissions'], $email_submissions, $form_state);
  form_set_value($form['email_page_urls'], $email_page_urls, $form_state);
}

/**
 * Implements hook_eck_bundle_info().
 */
function rcpar_mods_eck_bundle_info() {
  $items = array(
    'form' => array(
      'machine_name' => 'form',
      'entity_type' => 'form',
      'name' => 'form',
      'label' => 'Lead Form',
      'config' => array(),
    ),
  );
  return $items;
}
