<?php

/**
 * Class RCPARModal
 */
class RCPARModal {

  /**
   * @var array
   * Stores a Drupal renderable array that is the entirety of the modal
   */
  protected $modalRenderArray;

  /**
   * @var
   * This is the HTML id of the modal
   */
  private $id;

  /**
   * @var
   * This is the CSS class(es) id of the modal
   */
  private $cssClasses;


  /**
   * RCPARModal constructor.
   * @param string $id
   * @param string|array $cssClasses
   * @param array $options
   */
  public function __construct($id, $cssClasses, $options) {
    $this->modalRenderArray = $this->setScaffold();

    // Set the outer HTML Id
    $this->setId($id);
    $this->modalRenderArray['#attributes']['id'] = $id;

    // Set the outer HTML CSS Classes
    $this->setCssClasses($cssClasses);
    if(is_array($cssClasses)) {
      // treat like an array
      foreach ($cssClasses as $cssClass) {
        $this->modalRenderArray['#attributes']['class'][] = $cssClass;
      }
    }
    else {
      // treat like a string
      $this->modalRenderArray['#attributes']['class'][] = $cssClasses;
    }

    // set the attributes/options for the modal
    // this can be used to set data-attributes, role, etc
    foreach ($options as $attribute => $option) {
      if (array_key_exists($attribute,$options)) {
        $this->modalRenderArray['#attributes'][$attribute] = $option;
      }
    }

  }

  /**
   * @return array
   */
  private function setScaffold() {
    // create the scaffold array
    $scaffold = array(
      '#type'         => 'container',
      // default attributes
      '#attributes'   => array('id' => 'rcpar-modal', 'class' => array('modal')),
      'inner-wrapper' => array(
        '#type'       => 'container',
        '#attributes' => array('class' => array('modal-content')),
        'modal-header' => array(
          '#type'       => 'container',
          '#attributes' => array('class' => array('modal-header')),
          'contents'    => array('#markup' => ''),
        ),
        'modal-body' => array(
          '#type'       => 'container',
          '#attributes' => array('class' => array('modal-body')),
          'contents'    => array('#markup' => ''),
        ),
        'modal-footer' => array(
          '#type'       => 'container',
          '#attributes' => array('class' => array('modal-footer')),
          'contents'    => array('#markup' => ''),
        ),
      ),
    );
    return $scaffold;
  }

  /**
   * @param string $content
   * @return $this
   * Sets the header content of the modal
   */
  public function setHeaderContent($content) {
    $this->modalRenderArray['inner-wrapper']['modal-header']['contents']['#markup'] = $content;
    return $this;
  }

  /**
   * @param string $content
   * @return $this
   * Sets the body content of the modal
   */
  public function setBodyContent($content) {
    $this->modalRenderArray['inner-wrapper']['modal-body']['contents']['#markup'] = $content;
    return $this;
  }

  /**
   * @param string $html
   * @return $this
   * Adds content to the modal body
   */
  public function addToBody($html) {
    $this->modalRenderArray['inner-wrapper']['modal-body']['contents'][] = array('#markup' => $html);
    return $this;
  }

  /**
   * @param string $content
   * @return $this
   * Sets the footer content of the modal
   */
  public function setFooterContent($content) {
    $this->modalRenderArray['inner-wrapper']['modal-footer']['contents']['#markup'] = $content;
    return $this;
  }


  /**
   * Return HTML output for the modal
   */
  public function render() {
    drupal_alter('rcpar_modal', $this->modalRenderArray);
    // if the modal header, body or footer are empty,
    // do not include them in the render array
    if (empty($this->modalRenderArray['inner-wrapper']['modal-header']['contents']['#markup'])) {
      $this->modalRenderArray['inner-wrapper']['modal-header'] = '';
    }
    if (empty($this->modalRenderArray['inner-wrapper']['modal-body']['contents']['#markup'])) {
      $this->modalRenderArray['inner-wrapper']['modal-body'] = '';
    }
    if (empty($this->modalRenderArray['inner-wrapper']['modal-footer']['contents']['#markup'])) {
      $this->modalRenderArray['inner-wrapper']['modal-footer'] = '';
    }
    return render($this->modalRenderArray);
  }

  /**
   * @return mixed
   * Gets the ID of the modal
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param mixed $id
   * Sets the ID of the modal
   */
  public function setId($id) {
    $this->id = $id;
  }
  /**
   * @return mixed
   * Gets the CSS classes of the modal
   */
  public function getCssClasses() {
    return $this->cssClasses;
  }

  /**
   * @param mixed $id
   * Sets the CSS classes of the modal
   */
  public function setCssClasses($modal_classes) {
    $this->modalClasses = $modal_classes;
  }

}