<?php

/**
 * Class RCPARCourseTree
 * A class of Course Tree utility functions to be shared over different parts of the RCPAR site
 * @see rcpa_forms.module->rcpa_dependent_course_dropdown_callback() for an example.
 * NOTE: This implements an idea for handling questions with multiple topics that will require
 * refactoring ACT quizzes before they can be implemented
 */
class RCPARCourseTree {
  protected $sectionOpts;
  protected $examVersion;
  protected $sectionChapterAndTopicOptions = array();

  /**
   * RCPARCourseTree constructor.
   *
   * @param string $exam_version
   * - '2017' or '2018', etc
   */
  function __construct($exam_version = '') {
    // Set the exam version that we will use, or supply a default if none was provided
    if(empty($exam_version)) {
      $exam_version = exam_version_get_default_version();
    }
    $this->setExamVersion($exam_version);
  }

  /**
   * Defines the sections available in the course tree (AUD, BEC, FAR, REG) for future retrieval.
   *
   * These values are the same for all exam versions.
   */
  protected function setAllSectionOpts() {
    $all_section_opts = array();
    // options for course_section select are all possible values of course minus 'FULL'
    $voc = taxonomy_vocabulary_machine_name_load('course_sections');
    $tree = taxonomy_get_tree($voc->vid);
    foreach ($tree as $term) {
      if ($term->name != 'FULL') {
        $all_section_opts[$term->tid] = $term->name;
      }
    }
    natsort($all_section_opts);
    $this->sectionOpts = $all_section_opts;
  }


  /**
   * Gets array of section_tid => section name in natsort order
   * @return array like:
   * array (
   *   1452 => 'AUD',
   *   1455 => 'BEC',
   *   1453 => 'FAR',
   *   1454 => 'REG',
   * )
   */
  public function getAllSectionOpts() {
    if(empty($this->sectionOpts)) {
      $this->setAllSectionOpts();
    }
    return $this->sectionOpts;
  }

  /**
   * Returns the node ID of the given section name
   * @param string $sectionName
   * - 'AUD', 'BEC', 'FAR', or 'REG'
   */
  public function getSectionIdFromName($sectionName) {
    $sectionOpts = $this->getAllSectionOpts();
    return array_search($sectionName, $sectionOpts);
  }

  /**
   * Returns the name of the given section id
   * @param string $sectionID
   * - '1452', '1453', '1454', or '1455'
   */
  public function getNameFromSectionID($sectionId) {
    $sectionOpts = $this->getAllSectionOpts();
    return $sectionOpts[$sectionId];
  }

  /**
   * Returns chapter options for a course section
   *
   * @param $section_id
   * - Taxonomy term ID for a course section
   * @return array
   * - An array of chapter names keyed by chapter node id.
   */
  public function getChapterOptions($section_id) {
    return $this->getSectionChapterAndTopicOptions($section_id)['chapter_options'];
  }

  /**
   * @param string $name
   * - An exam version name, like '2017' or '2016
   * @return int
   * - Taxonomy term ID
   */
  public function getExamVersionIdFromName($name = NULL) {
    if (empty($name)) {
      $name = $this->getExamVersion();
    }

    $terms = taxonomy_get_term_by_name($name, 'exam_version');
    $term = reset($terms);
    return $term->tid;
  }


  /**
   * Returns array: of section options, and topic options (keyed by section options - orig chapter_id) for a given course (originally called chapter options for a section)
   * The arrays will only include options to show only sections and chapters including one or more questions, and an option to limit the options to a subset of question nids.
   *
   * @param $section_tid
   *  - the course tid
   * @param boolean $include_options_without_questions
   *  - If TRUE, include sections and topics without questions NOTE: if $limit_to_question_nids is not empty this is always considered FALSE
   * @param array $limit_to_question_nids
   *  - if empty show sections with any question, otherwise only show sections containing the question nids in this flat array (for quizzes with quiz pools)
   *
   * @param string $exam_version
   *  - currently '2017' used for transitioning between CPA Review versions
   *
   * @return array of 2 arrays, both in in natsort
   * - chapter_options is keyed on chapter_id w/ vals of the labels for the chapter select
   * - chapter_topic_options is an array of arrays keyed on chapter ids, each of that chapters topic select label keyed on the topic id
   *
   * i.e.:
   * array (
   *   'chapter_options' =>
   *     array (
   *       4335 => 'AUD 1: Audit Standards & Engagement Planning',
   *       4336 => 'AUD 2: Professional Responsibilities and Ethics',
   *       4337 => 'AUD 3: Internal Control',
   *       4338 => 'AUD 4: Audit Evidence',
   *       4339 => 'AUD 5: Audit Sampling',
   *       4340 => 'AUD 6: Audit Reports',
   *       4341 => 'AUD 7: Compilations and Reviews - SSARS',
   *       4342 => 'AUD 8: Other Services and Reports',
   *       4343 => 'AUD 9: Information Technology',
   *     ),
   *   'chapter_topic_options' =>
   *     array (
   *       4335 =>
   *         array (
   *           3792 => 'AUD 1.01: Types of Audits',
   *           3793 => 'AUD 1.02: Clarity Standards',
   *           3794 => 'AUD 1.03: Generally Accepted Auditing Standards',
   *           3795 => 'AUD 1.04: Responsibilities of the Auditor Under Clarity Standards',
   *           3797 => 'AUD 1.06: Steps in the Audit Process',
   *           3798 => 'AUD 1.07: Engagement Letter',
   *           3800 => 'AUD 1.09: Planning Procedures',
   *           3802 => 'AUD 1.11: Audit Risk, Fraud and Errors',
   *           3803 => 'AUD 1.12: Consideration of Fraud in the Financial Statements and Fraud Risk Factors',
   *           3805 => 'AUD 1.14: Quality Control',
   *           4047396 => 'AUD 1.15: Research Task Format – Audit – 2017',
   *         ),
   *       4336 =>
   *         array (
   *           3807 => 'AUD 2.01: Professional Responsibilities and Ethics',
   *           4047421 => 'AUD 2.02: AICPA Code of Professional Conduct – Continued – 2017',
   *           4047426 => 'AUD 2.03: AICPA Code of Professional Conduct – Continued – 2017 ',
   *    etc...
   *
   */
  public function getSectionChapterAndTopicOptions($section_tid, $include_options_without_questions = FALSE, $limit_to_question_nids = array(), $cram = FALSE) {
    // Set data for this section ID if we don't have it already - this is an expensive operation
    if(!isset($this->sectionChapterAndTopicOptions[$section_tid])) {
      $this->setSectionChapterAndTopicOptions($section_tid, $include_options_without_questions, $limit_to_question_nids, $cram);
    }

    return $this->sectionChapterAndTopicOptions[$section_tid];
  }

  /**
   * Get Chapter ID (parent) from a Topic ID (child).
   * @param $section_id
   * @param $topic_id
   * @return int|string
   */
  public function getChapterIdFromTopic($section_id, $topic_id, $cram = FALSE) {
    $options = $this->getSectionChapterAndTopicOptions($section_id, true, array(), $cram);
    foreach ($options["chapter_topic_options"] as $chapter_id => $topic) {
      foreach ($topic as $tid => $topic_name) {
        if ($tid == $topic_id) {
          return $chapter_id;
          break;
        }
      }
    }
    return false;
  }

  /**
   * Returns a chapter weight.
   * @param $chapter_id
   * @param $exam_version
   * @return mixed
   */
  public static function getChapterWeightFromId($chapter_id, $exam_version) {
    // Get chapter weight from course_stats table.
    $chapter_query = "SELECT weight as delta, section FROM course_stats WHERE nid = :nid AND year = :year";
    $chapter_conditions = array(
      ':nid'    => $chapter_id,
      ':year'   => $exam_version,
    );
    $chapter_weight = db_query($chapter_query, $chapter_conditions)->fetchAll();
    return $chapter_weight[0]->delta;
  }

  /**
   * Returns a properly formatted topic slug.
   * @param $section_id
   * @param $topic_id
   * @param $exam_version
   * @return string
   */
  public static function getTopicSlug($section_id, $topic_id, $exam_version) {
    // Get topic weight from course_stats table.
    $topic_query = "SELECT weight as delta, section FROM course_stats WHERE nid = :nid AND year = :year";
    $topic_conditions = array(
      ':nid'    => $topic_id,
      ':year'   => $exam_version,
    );
    $topic_weight = db_query($topic_query, $topic_conditions)->fetchAll();

    // Format slug from weight
    $topic_integer = $topic_weight[0]->delta + 1;
    $topic_delta = sprintf('%02d', $topic_integer);

    return $topic_delta;
  }

  /**
   * Sets data for this section to be retrieved later. This is an expensive operation, so it allows us to retrieve
   * it multiple times from the object without penalty.
   *
   * @param $section_tid
   *  - the course tid
   * @param boolean $include_options_without_questions
   *  - If TRUE, include sections and topics without questions NOTE: if $limit_to_question_nids is not empty this is always considered FALSE
   * @param array $limit_to_question_nids
   *  - if empty show sections with any question, otherwise only show sections containing the question nids in this flat array (for quizzes with quiz pools)
   *
   * @param string $exam_version
   *  - currently '2017' used for transitioning between CPA Review versions
   *
   * @return NULL
   * - Sets a value for a class property and returns nothing.
   */
  public function setSectionChapterAndTopicOptions($section_tid, $include_options_without_questions = FALSE, $limit_to_question_nids = array(), $cram = FALSE) {
    // TODO: Consider caching this per section_tid, to be cleared on chapter & topic update.

    // TODO: Figure out exam versioning implications
    $exam_version = $this->getExamVersion();

    // When limiting options to a subset of question nids...
    if (!empty($limit_to_question_nids)) {
      // ... this option is set to false.
      $include_options_without_questions = FALSE;
    }

    // Get a course node for this section. Course nodes correspond to the exam parts (regular and cram) and have
    // node relationships to the chapters.
    if ($cram) {
      $course = ipq_common_get_cram_course_by_section_tid($section_tid);
    }
    else {
      $course = ipq_common_get_online_course_by_section_tid($section_tid); 
    }
    $chapters = ipq_common_get_chapters_by_course_nid($course->nid, TRUE, $exam_version);

    // Get the course name used for building the options.
    $course_names = $this->getAllSectionOpts();
    $course_name = $course_names[$section_tid];

    // Will hold the chapter options returned.
    $return_chapter_options = array();
    // Will hold the chapter topic options returned.
    $chapter_topics_arg = array();

    if ($include_options_without_questions) {
      foreach ($chapters as $key => $val) {
        // This pattern is repeated twice in this function (search for this comment.)
        $return_chapter_options[$key] = $course_name . ' ' . $val['delta'] . ': ' . $val['title'];
        // Produces a label like 'AUD 1: Audit Standards & Engagement Planning'
      }

    }
    else {
      // Limiting results to chapters that contain questions.
      $chapter_topics_arg = $this->getSectionChapterTopicNids($section_tid, $limit_to_question_nids);
      $chapters_with_questions = array_keys($chapter_topics_arg);

      foreach ($chapters as $key => $val) {
        if (in_array($key, $chapters_with_questions)) {
          // This pattern is repeated twice in this function (search for this comment.)
          $return_chapter_options[$key] = $course_name . ' ' . $val['delta'] . ': ' . $val['title'];
          // Produces a label like 'AUD 1: Audit Standards & Engagement Planning'
        }
      }

    }

    natsort($return_chapter_options);

    if ($include_options_without_questions) {
      foreach ($return_chapter_options AS $selected_chapter_id => $chapter_option) {
        $topic_opts = array();
        $topics = ipq_common_get_topics_by_chapter_nid($selected_chapter_id, $exam_version);
        foreach ($topics as $delta => $t) {
          // Pattern for topic repeated twice in this function (search for this comment.)
          $topic_opts[$t->nid] = $course_name . ' ' . sprintf("%s.%02s: %s", $chapters[$selected_chapter_id]['delta'], $delta + 1, $t->title);
          // Produces a label like "AUD 4.10: Financial Statement Accounts: Inventories"
        }
        natsort($topic_opts);
        $chapter_topics_arg[$selected_chapter_id] = $topic_opts;
      }
    }
    else {
      // Limiting results to chapters/topics that contain questions.
      foreach ($chapter_topics_arg AS $selected_chapter_id => &$chapter_topic_vals) {
        if (in_array($selected_chapter_id, $chapters_with_questions)) {
          $topic_opts = array();
          $topics = ipq_common_get_topics_by_chapter_nid($selected_chapter_id, $exam_version);
          foreach ($topics as $delta => $t) {
            // Pattern for topic repeated twice in this function (search for this comment.)
            $topic_opts[$t->nid] = $course_name . ' ' . sprintf("%s.%02s: %s", $chapters[$selected_chapter_id]['delta'], $delta + 1, $t->title);
            // Produces a label like "AUD 4.10: Financial Statement Accounts: Inventories"
          }
          // add names for our topic options
          foreach ($chapter_topic_vals AS $chapter_topic_id => &$chapter_topic_val) {
            // Limit the options to those that have questions in them. Some topics will be empty when we limit chapters to those with questions.
            if (empty($topic_opts[$chapter_topic_id])) {
              unset($chapter_topic_vals[$chapter_topic_id]);
            }
            else {
              $chapter_topic_val = $topic_opts[$chapter_topic_id];
            }
          }
          natsort($chapter_topic_vals);
        }
        else {
          unset($chapter_topics_arg[$selected_chapter_id]);
        }
      }
    }

    $this->sectionChapterAndTopicOptions[$section_tid] = array(
      'chapter_options' => $return_chapter_options,
      'chapter_topic_options' => $chapter_topics_arg
    );
  }

  /**
   * Gets an array of arrays of topic_ids having questions keyed on chapter_id for a given section
   * NOTE: unpublished questions are included
   *
   * @param integer $section_tid
   * - the section tid
   * @param array $limit_to_question_nids
   *  - if empty show sections with any question, otherwise only show sections containing the question nids in this flat array (for quizzes with quiz pools)
   * @return array like:
   * array (
   *   4335 =>
   *   array (
   *     3792 => '3792',
   *     3800 => '3800',
   *     3802 => '3802',
   *     3797 => '3797',
   *     3805 => '3805',
   *     3803 => '3803',
   *     4047396 => '4047396',
   *     3793 => '3793',
   *     3794 => '3794',
   *     3795 => '3795',
   *     3798 => '3798',
   *   ),
   *   4336 =>
   *   array (
   *     3810 => '3810',
   *     3809 => '3809',
   *     etc...
   */
  public function getSectionChapterTopicNids($section_tid, $limit_to_question_nids = array()) {
    $exam_version = $this->getExamVersion();

    // Build a query string to find quesions
    $query_str = "SELECT q.chapter_id, GROUP_CONCAT(DISTINCT q.topic_id) AS topic_ids
                  FROM {ipq_question_pool} q
                  INNER JOIN {taxonomy_term_data} t ON t.tid = q.exam_version_id
                  WHERE q.section_id = :section_id";

    if (!empty($limit_to_question_nids)) {
      $query_str .= "
                  AND q.ipq_questions_id IN (:qnids_arr)";
    }
    $query_str .= "
                  AND t.name = :exam_version
                  GROUP BY q.chapter_id";

    $results = db_query($query_str,
      array(
        ':section_id' => $section_tid,
        ':qnids_arr' => $limit_to_question_nids,
        ':exam_version' => $exam_version
      ));

    $return_chapter_topic_nids = array();
    foreach ($results AS $result) {
      $topic_arg = explode(',', $result->topic_ids);
      // Adds an array with keys and values the same of topic ids, keyed on the chapter id like:
      // 4343 =>
      // array (
      //   3880 => '3880',
      //   3879 => '3879',
      // ),
      $return_chapter_topic_nids[$result->chapter_id] = array_combine($topic_arg, $topic_arg);
    }

    return $return_chapter_topic_nids;
  }

  /**
   * Checks by exam version for topics listed in > that one section (orig.
   * called chapter) and returns a string of error if it finds one. Otherwise,
   * returns the an array of arrays w one item each: (section_tids,
   * section_tids, chapter_delta-topic_deltas) and chapter_hhc_val, & topic_val
   * keyed on topic_nid
   *
   * @return array
   *  - like:
   *  array (
   *    4047381 =>
   *      array (
   *        'chapter_nid-topic_nids' =>
   *          array (
   *            0 => '4334-4047381',
   *          ),
   *        'section_tids' =>
   *          array (
   *            0 => 1452,
   *          ),
   *        'chapter_delta-topic_deltas' =>
   *          array (
   *            0 => '0-1',
   *          ),
   *        'chapter_hhc_val' => '_none',
   *        'topic_val' => '_none',
   *      ),
   *    etc...
   *
   * TODO: add as a check on hook_node_update if type == 'rcpa_chapter'
   *
   * When a string is returned it could be used like:
   *  if (!is_array($topic_vals)){
   *    // An error string will be returned if it finds a topic is in more than
   *   one chapter,
   *    // show it and quit out.
   *    drupal_set_message($topic_vals, 'error');
   *    drupal_goto(current_path());
   *  }
   */
  public function getTopicsWithValues() {
    $exam_version = $this->getExamVersion();

    $course_names = $this->getAllSectionOpts();
    // Store values of chapters & sections of each topic.
    $topic_vals = [];

    // If a topic is found in more than one chapter, we'll track it here.
    $multiple_chapter_topic_nids = [];
    foreach ($course_names AS $section_tid => $course_name) {
      $course = ipq_common_get_online_course_by_section_tid($section_tid);
      $chapters = ipq_common_get_chapters_by_course_nid($course->nid, TRUE, $exam_version);
      // Looks like this:
      // $chapters[$chapter_ref->nid->value()] = array(
      //   'title' => $title,
      //   'delta' => $delta,
      // );
      foreach ($chapters AS $chapter_nid => $chapter_vals) {
        $topics = ipq_common_get_topics_by_chapter_nid($chapter_nid, $exam_version);
        foreach ($topics as $delta => $t) {
          if (!array_key_exists($t->nid, $topic_vals)) {
            $topic_vals[$t->nid] = [
              // These are arrays only to assist in diagnosing when topics are found to be in > 1 chapter.
              'chapter_nid-topic_nids'     => [],
              'section_tids'               => [],
              'chapter_delta-topic_deltas' => [],
            ];
          }
          else {
            $multiple_chapter_topic_nids[] = $t->nid;
          }
          $topic_vals[$t->nid]['chapter_nid-topic_nids'][] = $chapter_nid . '-' . $t->nid;
          $topic_vals[$t->nid]['section_tids'][] = $section_tid;
          // Remember that the topic name adds 1 to the delta.
          $topic_vals[$t->nid]['chapter_delta-topic_deltas'][] = $chapter_vals['delta'] . '-' . ($delta + 1);
          if ($chapter_vals['delta'] == 0 && $delta == 0) {
            // Per HHC-36, we'll set default vals of new chapter & topic fields to _none for 00.01 topics
            $topic_vals[$t->nid]['chapter_hhc_val'] = '_none';
            $topic_vals[$t->nid]['topic_val'] = '_none';
          }
          else {
            $topic_vals[$t->nid]['chapter_hhc_val'] = $chapter_nid;
            $topic_vals[$t->nid]['topic_val'] = $t->nid;
          }
        }

      }

    }
    if (!empty($multiple_chapter_topic_nids)) {
      $err = '<pre>Unexpected error: There are topics that are in more than one chapter:<br>';
      foreach ($multiple_chapter_topic_nids as $multiple_chapter_topic_nid) {
        $err .= var_export($topic_vals[$multiple_chapter_topic_nid], TRUE);
      }
      $err .= '</pre>';
      drupal_set_message($err, 'error');
      drupal_goto(current_path());
    }
    else {
      return $topic_vals;
    }

  }

  /**
   * The exam version currently in use for the object
   * @return string
   * - '2017' or '2018', etc
   */
  public function getExamVersion() {
    return $this->examVersion;
  }

  /**
   * @param string $examVersion
   * - '2017' or '2018', etc
   */
  public function setExamVersion($examVersion) {
    $this->examVersion = $examVersion;
  }

  /**
   * Given a chapter node id, this function returns the section of the exam that it's in.
   * This is less straightforward than it sounds because we have to look through all of the
   * "Node ref by exam ver" entities that the chapter is referenced, then determine which course
   * node that version those entities relate to, and finally extrapolate the course section
   * taxonomy term from the field course node.
   * @param int $chapter_id
   * - Node ID of a chapter
   * @return int
   * - Taxonomy term ID of the section
   * - NodAn array of term names ('2017', '2018', etc) keyed by term id for the exam version.
   */
  static function getSectionForChapter($chapter_id) {
    $versions = array();

    // Grab all of the chapter reference entities that this chapter is referenced in
    $result = db_query("
      SELECT entity_id, entity_type 
      FROM field_data_field_topic_exam_chapter_ref 
      WHERE bundle = 'chapter_ref_by_exam_version' AND field_topic_exam_chapter_ref_target_id = :chapterid",
      array(':chapterid' => $chapter_id));

    foreach ($result as $item) {
      // Load the corresponding chapter reference entity
      $chapter_entity = entity_load_single($item->entity_type, $item->entity_id);
      $chapter_wrapper = entity_metadata_wrapper($item->entity_type, $chapter_entity);
      try {
        // Get the course node associated this chapter reference entity. We can assume an entity will only be
        // related to one course node (although a course node will have multiple chapter reference entities)
        $courseResult = db_query("
          SELECT entity_id, entity_type FROM field_data_field_chapter_per_exam_version
          WHERE field_chapter_per_exam_version_target_id = :id",
          array(':id' => $chapter_wrapper->getIdentifier()))->fetch();
        $course_entity = entity_load_single($courseResult->entity_type, $courseResult->entity_id);
        $course_wrapper = entity_metadata_wrapper($courseResult->entity_type, $course_entity);

        $section = $course_wrapper->field_course_section;
        return $section->getIdentifier();
      }
      catch (EntityMetadataWrapperException $e) {}
    }
  }

}