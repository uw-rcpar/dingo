

<?php
$m = new RCPARModal('form-popup-modal', 'fade free-trial-popup', [
'data-backdrop'   => 'true',
'role'            => 'dialog',
'aria-hidden'     => 'true',
'tabindex'        => '-1',
]);
$modal_header = '<h2 class="modal-title">Free CPA Review Trial <button type="button" class="close" data-dismiss="modal">&times;</button></h2><div class="free-trial-subheading">Study for the CPA Exam with Roger CPA Review - FREE for 2 days!</div>';

$partner = node_load(134); // Free trial partner node ID = 134.

// Display the Free Trial create account form. Free trial partner node ID = 134.
$form = drupal_get_form('rcpar_asl_create_account_form', $partner);
// Populate the user's email address via variable passed from module.
if ($user_email) {
  $form['email_new']['#value'] = $user_email;
}
$modal_body = render($form);

// Set the header and body content for modal and render away.
$m->setHeaderContent($modal_header)
->setBodyContent($modal_body);
print $m->render();

// Add a modal used to display a "processing" message.
$m_processing = new RCPARModal('processing-modal', ['rcpar-mod-typical-modal', 'processing-modal'], [
  'data-backdrop'   => 'static',
  'keyboard'        => 'false',
  'role'            => 'dialog',
  'aria-labelledby' => 'h3',
  'tabindex'        => '-1',
  'data-backdrop-class' => 'secondary',
]);
$m_processing->setHeaderContent('<h3 id="processing-modal-header" class="modal-title"><img src="/sites/default/files/images/icon-hourglass.svg" alt="Hourglass Icon" width="30" /> This will just take a moment...</h3>')
  ->setBodyContent('We\'re currently activating your Free Trial. You\'ll be able to get started in a few short seconds.');
print $m_processing->render();


?>

