<div class="container-fluid marketing-lead-form">
  <?php

  // Show inline style/markup first
  print render($form['css']);

  $render_later_keys = array();

  // Iterate through each form element and render it according to configured layout
  foreach (element_children($form, TRUE) as $element_child) {
    if (isset($form['#layout'][$element_child])) {
      switch ($form['#layout'][$element_child]) {
        // Left column - begin new row and write column
        case 'l':
          print '<div class="col-xs-6 left-col ' . drupal_html_class($element_child) . '">';
          print drupal_render($form[$element_child]);
          print '</div>'; // end div.col
          break;

        // Right column - write column and end row
        case 'r':
          print '<div class="col-xs-6 right-col ' . drupal_html_class($element_child) . '">';
          print drupal_render($form[$element_child]);
          print '</div>'; // end div.col
          break;

        // Center - write full row and column
        case 'c':
          print '<div class="row">'; // begin div.row
          print '<div class="col-xs-12 center-col ' . drupal_html_class($element_child) . '">';
          print drupal_render($form[$element_child]);
          print '</div>'; // end div.col
          print '</div>'; // end div.row
          break;
      }
    }

    // If form is a multi-step form, the fields are nested in fieldsets.
    // Loop through children of each fieldset and assign prefix and suffix to each.
    if (strpos($element_child, 'step_') !== FALSE) {
      foreach (element_children($form[$element_child], TRUE) as $step_child) {
        if (isset($form['#layout'][$step_child])) {
          switch ($form['#layout'][$step_child]) {
            // Left column - begin new row and write column
            case 'l':
              $form[$element_child][$step_child]['#prefix'] = '<div class="col-xs-6 left-col ' . drupal_html_class($step_child) . '">';
              $form[$element_child][$step_child]['#suffix'] = '</div>';
              break;

            // Right column - write column and end row
            case 'r':
              $form[$element_child][$step_child]['#prefix'] = '<div class="col-xs-6 right-col ' . drupal_html_class($step_child) . '">';
              $form[$element_child][$step_child]['#suffix'] = '</div>'; // end div.col
              break;

            // Center - write full row and column
            case 'c':
              $form[$element_child][$step_child]['#prefix'] = '<div class="row"><div class="col-xs-12 center-col ' . drupal_html_class($step_child) . '">';
              $form[$element_child][$step_child]['#suffix'] = '</div></div>'; // end div.col
              break;
          }
        }
      }
    }
  }

  // Render the rest of the form guts
  print drupal_render_children($form);
  ?>
</div>