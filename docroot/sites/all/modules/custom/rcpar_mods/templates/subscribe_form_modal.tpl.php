<!-- What kind of learner are you Modal -->
<div class="modal fade" id="form-popup-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header form-modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      </div>

      <div class="modal-body modal-body-celebrate">
        <div class="typeform-widget" data-url="https://rogercpareview.typeform.com/to/t7TkF3" style="width: 100%; height: 500px;" > </div> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script> <div style="font-size: 12px;color: #999;opacity: 0.5; padding-top: 5px;" > powered by <a href="https://www.typeform.com/examples/?utm_campaign=t7TkF3&amp;utm_source=typeform.com-350031-Pro&amp;utm_medium=typeform&amp;utm_content=typeform-embedded-poweredbytypeform&amp;utm_term=EN" style="color: #999" target="_blank">Typeform</a> </div>
      </div>

    </div>
  </div>
</div>


<!-- Email subscribe Modal
<div class="modal fade" id="form-popup-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header form-modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <div class="modal-title-wrapper"><h1 class="form-modal-title" id="myModalLabel">Be the First to Know </h1></div>
      </div>

      <div class="modal-body modal-body-celebrate">
        <p class="form-modal-description">Join our email list for exclusive discounts and <br />all the latest news about the CPA Exam!</p>
        <div class="form-modal-container">
           <?php
        //$block = module_invoke('webform', 'block_view', 'client-block-15904');
       // print render($block['content']);
        ?>
        </div
      </div>

    </div>
  </div>
</div>-->