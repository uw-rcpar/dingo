<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */

    drupal_add_js(drupal_get_path('module', 'rcpar_mods') . '/js/reports.js');
    drupal_add_css(drupal_get_path('module', 'rcpar_mods') . '/css/reports.css');
?>

<table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
    <?php if (!empty($title)) : ?>
        <caption><?php print $title; ?></caption>
    <?php endif; ?>
    <thead>
    <tr>
        <?php foreach ($header as $field => $label): ?>
            <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
                <?php print $label; ?>
            </th>
        <?php endforeach; ?>
        <th>Order Ids</th>
    </tr>
    </thead>
    <?php if (empty($summary_only)): ?>
        <tbody>
        <?php foreach ($rows as $count => $row): ?>
            <tr class="ca-quaterly-table collapser <?php print implode(' ', $row_classes[$count]); ?>" colspan="5">
                <?php foreach ($row as $field => $content): ?>
                    <td <?php if ($field_classes[$field][$count]) { print 'class="'. $field_classes[$field][$count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$count]); ?>>
                        <?php print $content; ?>
                    </td>
                <?php endforeach; ?>
                <?php
                    $orders_view = views_get_view('ca_quarterly_tax_v2', true);
                    $display = 'block_1';
                    $orders_view->set_arguments(array($row['commerce_customer_address_locality']));

                    // we need to manualy set values for this filter, because it
                    // doesn't get populated automatically using the url
                    $input = $view->get_exposed_input();
                    $filter = $orders_view->get_item($display, 'filter', 'date_filter_1');
                    if (isset($input['date_filter_1']['min']['date'])) {
                        $filter['value']['min'] = (date('Y-m-d', strtotime($input['date_filter_1']['min']['date'])));
                    }
                    if (isset($input['date_filter_1']['max']['date'])) {
                        $filter['value']['max'] = (date('Y-m-d', strtotime($input['date_filter_1']['max']['date'])));
                    }
                    $orders_view->set_item($display, 'filter', 'date_filter_1', $filter);

                    $filter = $orders_view->get_item($display, 'filter', 'payment_method');

                    if (isset($input['payment_method'])) {
                        $filter = $orders_view->get_item($display, 'filter', 'payment_method');

                        if (isset($input['payment_method'])) {
                            $vals = array();
                            foreach($input['payment_method'] as $s){
                                $vals[$s] = $s;
                            }
                            $filter['value'] = $vals;
                        }
                        $orders_view->set_item($display, 'filter', 'payment_method', $filter);
                    }
                    
                    $filter = $orders_view->get_item($display, 'filter', 'commerce_customer_address_administrative_area');
                    if(isset($input['commerce_customer_address_administrative_area'])){
                      $filter['value'] = $input['commerce_customer_address_administrative_area'];
                      $orders_view->set_item($display, 'filter', 'commerce_customer_address_administrative_area', $filter);
                    }
                    
                    $filter = $orders_view->get_item($display, 'filter', 'status');
                    if(isset($input['status'])){
                      $filter['value'] = $input['status'];
                      $orders_view->set_item($display, 'filter', 'status', $filter);
                    }


                    $orders_view->build($display);
                    $orders_view->pre_execute();
                    $content = $orders_view->render($display);                    
            ?>
            <td>
                <?php
                  $order_ids = array();
                  foreach($orders_view->result as $result){
                    $order_ids[] = $result->order_id;
                  }
                  echo implode(',', $order_ids);
                ?>
            </td>
            </tr>
            <tr  class=" ca-quaterly-table collapsable <?php print implode(' ', $row_classes[$count]); ?>">
                <td colspan="<?php echo count($row); ?>">
                    <?php  echo $content; ?>
                </td>
                <td></td>  
            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php endif; ?>
    <tfoot>
    <tr class="summary">
        <?php foreach ($header as $field => $label): ?>
            <td><?php if (!empty($summarized[$field])) { echo $summarized[$field]; } ?></td>
        <?php endforeach; ?>
        <td></td>    
    </tr>
    </tfoot>
</table>

<h3>Refunds</h3>
<?php
    $orders_view = views_get_view('ca_quarterly_tax_v2', true);
    $display = 'block_2';

    // we need to manually set values for this filter, because it
    // doesn't get populated automatically using the url
    $input = $view->get_exposed_input();

    $filter = $orders_view->get_item('block_2', 'filter', 'date_filter_1');
    if (isset($input['date_filter_1']['min']['date'])) {
        $filter['value']['min'] = (date('Y-m-d', strtotime($input['date_filter_1']['min']['date'])));
    }
    if (isset($input['date_filter_1']['max']['date'])) {
        $filter['value']['max'] = (date('Y-m-d', strtotime($input['date_filter_1']['max']['date'])));
    }
    $orders_view->set_item('block_2', 'filter', 'date_filter_1', $filter);


    $filter = $orders_view->get_item('block_2', 'filter', 'payment_method');

    if (isset($input['payment_method'])) {
        $filter = $orders_view->get_item('block_2', 'filter', 'payment_method');

        if (isset($input['payment_method'])) {
            $vals = array();
            foreach($input['payment_method'] as $s){
                $vals[$s] = $s;
            }
            $filter['value'] = $vals;
        }
        $orders_view->set_item('block_2', 'filter', 'payment_method', $filter);
    }
    
    $filter = $orders_view->get_item('block_2', 'filter', 'commerce_customer_address_administrative_area');
    if(isset($input['commerce_customer_address_administrative_area'])){
      $filter['value'] = $input['commerce_customer_address_administrative_area'];
      $orders_view->set_item('block_2', 'filter', 'commerce_customer_address_administrative_area', $filter);
    }

    $filter = $orders_view->get_item('block_2', 'filter', 'status');
    if(isset($input['status'])){
      $filter['value'] = $input['status'];
      $orders_view->set_item('block_2', 'filter', 'status', $filter);
    }


    $orders_view->build($display);
    $orders_view->pre_execute();
    $content = $orders_view->render($display);
    echo $content;
?>