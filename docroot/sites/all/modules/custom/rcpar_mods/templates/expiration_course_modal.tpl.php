<!-- Modal -->
<div class="modal fade" id="form-popup-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header form-modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
              <div class="modal-body">
                 <p>Your course is set to expire in <?php print $days ?>  day(s), and we see that you have not yet purchased extensions.  If you are interested in having an additional six months of course access, you still have time to <a href="https://www.rogercpareview.com/cpa-courses/6-month-extension" target="_blank">purchase those extensions</a>!</p>
              </div>
            </div>
        </div>
    </div>
</div>