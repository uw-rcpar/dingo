<?php

/**
 * A customplugin for Autocomplete behavior originally provided by EntityReference
 */
class RCPARMODSEntityReference_SelectionHandler extends EntityReference_SelectionHandler_Generic {

  /**
   * Implements EntityReferenceHandler::getInstance().
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    return new RCPARMODSEntityReference_SelectionHandler($field, $instance, $entity_type, $entity);
  }

  /**
   * Implements EntityReferenceHandler::getReferencableEntities().
   * Customize output titles for autocomplete 
   */
  public function getReferencableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $options = array();
    $entity_type = $this->field['settings']['target_type'];

    $query = $this->buildEntityFieldQuery($match, $match_operator);
    if ($limit > 0) {
      $query->range(0, $limit);
    }
    $results = $query->execute();

    if (!empty($results[$entity_type])) {
      $entities = entity_load($entity_type, array_keys($results[$entity_type]));
      foreach ($entities as $entity_id => $entity) {
        $sku = isset($entity->sku) ? $entity->sku : 'no sku';
        list(,, $bundle) = entity_extract_ids($entity_type, $entity);
        // Attach SKU to title results
        $options[$bundle][$entity_id] = check_plain($this->getLabel($entity)) . " ( {$sku}) ";
      }
    }
    return $options;
  }
 
}

