<?php
/**
 * @file
 * The basic formatter behavior makes the property behavior like a simple field.
 * 
 * 1) The property can be entered by the user with a text field.
 * 2) The property is displayed as raw markup.
 */

$plugin = array(
  'label' => "Basic Formatter",
  'entity_view' => 'rcpar_mods_basic_formatter_property_entity_view',
  'entity_info' => 'rcpar_mods_basic_formatter_property_entity_info',
  'default_widget' => 'rcpar_mods_basic_formatter_property_widget',
  'default_formatter' => 'rcpar_mods_basic_formatter_property_formatter',
);

/**
 * Form widget for inputting this field - basic textfield
 */
function rcpar_mods_basic_formatter_property_widget($property, $vars) {
  $entity = $vars['entity'];

  // Get the value of this property
  $val = '';
  if (isset($entity->{$property})) {
    $val = $entity->{$property};
  }

  return array(
    '#type' => 'textfield',
    '#title' => $vars['properties'][$property]['label'],
    '#default_value' => $val,
    '#required' => TRUE,
  );
}

/**
 * Display property as basic markup
 */
function rcpar_mods_basic_formatter_property_formatter($property, $vars) {
  $entity = $vars['entity'];

  // Get the value of this property
  $val = '';
  if (isset($entity->{$property})) {
    $val = $entity->{$property};
  }

  return array('#markup' => $val);
}

/**
 * Unused. Do things when the entity is viewed.
 */
function rcpar_mods_basic_formatter_property_entity_view($property, $vars) {
}

/**
 * Unused. Entity info alterations. Change label callbacks or other entity info
 */
function rcpar_mods_basic_formatter_property_entity_info($property, $var) {
}
