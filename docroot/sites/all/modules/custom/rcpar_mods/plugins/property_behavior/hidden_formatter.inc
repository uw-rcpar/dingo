<?php
/**
 * @file
 * The hidden formatter allows a property to be edited, but not displayed.
 */

$plugin = array(
  'label' => "Hidden Formatter",
  'default_widget' => 'rcpar_mods_hidden_formatter_property_widget',
);

/**
 * Form widget for inputting this field - basic textfield
 */
function rcpar_mods_hidden_formatter_property_widget($property, $vars) {
  $entity = $vars['entity'];

  // Get the value of this property
  $val = '';
  if (isset($entity->{$property})) {
    $val = $entity->{$property};
  }

  return array(
    '#type' => 'textfield',
    '#title' => $vars['properties'][$property]['label'],
    '#default_value' => $val,
    '#required' => TRUE,
  );
}
