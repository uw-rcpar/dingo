<?php

/**
 * This field handler display the 6 month extension AUD
 *
 */
class rcpar_mods_handler_6_month_aud extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'AUD-6MEXT');
  }
}

/**
 * This field handler display the 6 month extension BEC
 *
 */
class rcpar_mods_handler_6_month_bec extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'BEC-6MEXT');
  }
}

/**
 * This field handler display the 6 month extension REG
 *
 */
class rcpar_mods_handler_6_month_reg extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'REG-6MEXT');
  }
}

/**
 * This field handler display the 6 month extension FAR
 *
 */
class rcpar_mods_handler_6_month_far extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FAR-6MEXT');
  }
}