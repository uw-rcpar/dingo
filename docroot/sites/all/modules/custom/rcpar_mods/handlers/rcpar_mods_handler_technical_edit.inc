<?php
/**
 * @file
 * Definition of rcpar_mods_handler_technical_edit
 */

/**
 * Provides a custom views field technical_edit.
 */
class rcpar_mods_handler_technical_edit extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function views_form(&$form, &$form_state) {
    // Create a container for our replacements
    $form[$this->options['id']] = array(
      '#type' => 'container',
      '#tree' => TRUE,
    );

    $form['is_technical_options'] = array(
      '#type' => 'value',
      '#value' => array(
        '1' => t('Yes'),
        '0' => t('No'),
        ),
    );

    // Iterate over the result and add our custom fields to the form.
    foreach($this->view->result as $row_index => $row) {
      // Add a select field to the form.
      $default = $row->rcpar_feedback_is_technical;
      $form[$this->options['id']][$row_index] = array(
        '#type' => 'select',
        '#options' => $form['is_technical_options']['#value'],
        '#default_value' => $default,
      );
    }
  }

  /**
   * Form submit method.
   */
  function views_form_submit($form, &$form_state) {
    $updated = strtotime("now");
    // Iterate over the view result.
    foreach($this->view->result as $row_index => $row) {
      // Grab the correspondingly submitted form value.
      $value = $form_state['values'][$this->options['id']][$row_index];
      // If the submitted value is different from the original status value
      // we're gonna update the is_technical field
      if ($row->rcpar_feedback_is_technical != $value) {
        $fields = array(
          'is_technical' => $value,
          'updated' => $updated,
        );
        // If is_technical changed to yes, give a shout out to the CC crew
        if ($fields['is_technical'] == 1) {
          $data = array(
            'uid' => $row->users_rcpar_feedback_uid,
            'question_id' => $row->rcpar_feedback_question_id,
            'topics' => $row->rcpar_feedback_topics,
            'type' => $row->rcpar_feedback_type,
            'feedback' => $row->rcpar_feedback_feedback,
          );
          rcpar_mods_feedback_notify_customer_care($data);
        }

        // Put a bow on it
        db_update('rcpar_feedback')
          ->fields($fields)
          ->condition('id', $row->rcpar_feedback_id, '=')
          ->execute();
      }
    }
  }

  function render($data) {
    // Drupal digs this wrapper for form stuff cause that's how it rolls
    $output = '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
    return $output;
  }
}