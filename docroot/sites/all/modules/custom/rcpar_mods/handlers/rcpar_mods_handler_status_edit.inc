<?php
/**
 * @file
 * Definition of rcpar_mods_handler_status_edit
 */

/**
 * Provides a custom views field status_edit.
 */
class rcpar_mods_handler_status_edit extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function views_form(&$form, &$form_state) {
    // Create a container for our replacements
    $form[$this->options['id']] = array(
      '#type' => 'container',
      '#tree' => TRUE,
    );

    $form['status_options'] = array(
      '#type' => 'value',
      '#value' => array('Open' => t('Open'),
        'Resolved' => t('Resolved'),
        'Not Relevant' => t('Not Relevant'),
        'High Priority' => t('High Priority'),
        'Med Priority' => t('Med Priority'),
        'Low Priority' => t('Low Priority'),
      ),
    );

    // Iterate over the result and add our custom fields to the form.
    foreach($this->view->result as $row_index => $row) {
      // Add a select field to the form.
      $default = $row->rcpar_feedback_status;
      $form[$this->options['id']][$row_index] = array(
        '#type' => 'select',
        '#options' => $form['status_options']['#value'],
        '#default_value' => $default,
      );
    }
  }

  /**
   * Form submit method.
   */
  function views_form_submit($form, &$form_state) {
    $updated = strtotime("now");
    // Iterate over the view result.
    foreach($this->view->result as $row_index => $row) {
      // Grab the correspondingly submitted form value.
      $value = $form_state['values'][$this->options['id']][$row_index];
      // If the submitted value is different from the original status value
      // we're gonna take this puppy home
      if ($row->rcpar_feedback_status != $value) {
         $fields = array(
          'status' => $value,
          'updated' => $updated,
          );
         // Go home puppy
        db_update('rcpar_feedback')
          ->fields($fields)
          ->condition('id', $row->rcpar_feedback_id, '=')
          ->execute();
      }
    }
  }

  function render($data) {
    // Drupal digs this wrapper for form stuff cause that's how it rolls
    $output = '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
    return $output;
  }
}