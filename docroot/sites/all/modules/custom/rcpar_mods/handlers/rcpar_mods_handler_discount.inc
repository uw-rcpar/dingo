<?php

/**
 * This field handler display the Full Flashcard Discount
 *
 */
class rcpar_mods_handler_full_flashcard_discount extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FULL-EFC-DIS');
  }
}

/**
 * This field handler display the Full Audio Discount
 *
 */
class rcpar_mods_handler_full_audio_discount extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FULL-AUD-DIS');
  }
}

/**
 * This field handler display the Full Flash Drive Cram Course
 *
 */
class rcpar_mods_handler_full_flash_drive_cram_course_discount extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FULL-SSD-DIS');
  }
}

/**
 * This field handler display the Elite Package Discount
 *
 */
class rcpar_mods_handler_elite_package_discount extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FULL-ELITE-DIS');
  }
}

/**
 * This field handler display the Premier Package Discount
 *
 */
class rcpar_mods_handler_premier_package_discount extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FULL-PREM-DIS');
  }
}
