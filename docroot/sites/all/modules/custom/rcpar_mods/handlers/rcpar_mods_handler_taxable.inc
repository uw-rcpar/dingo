<?php


/**
 * This field handler displays taxable amount of product.
 *
 */
class rcpar_mods_handler_taxable extends views_handler_field_entity {

  function render($values) {
    $currency_code = $values->commerce_payment_transaction_commerce_order_currency_code;
    $order = $values->_field_data['order_id']['entity'];
    if(isset($order->data['commerce_avatax'])){
      $avatax = $order->data['commerce_avatax'];
      $taxable = $avatax['response']['result']['totalTaxable'] * 100;
    } 
    else {
      $total_total = $values->field_data_commerce_order_total_commerce_order_total_amount;
      $shipping = $values->commerce_order_shipping_line_item_representiative__field_dat;
      $taxable = $total_total - $shipping;
    }

    return commerce_currency_format($taxable, $currency_code) ;
  }
}

/**
 * This field handler save in $_SESSION the list of sku and price
 *
 */
class rcpar_mods_handler_all_line_item_prices_in_view extends views_handler_field_entity {
 
  function render($values) {
    try {
        unset($_SESSION['price_line_item']);
        foreach ($values->_field_data['order_id']['entity']->commerce_line_items['und'] as $key => $line_item_id) {
            
            $line_item = commerce_line_item_load($line_item_id['line_item_id']);
            $sku = $line_item->line_item_label;
            $wrapper_line_item = entity_metadata_wrapper('commerce_line_item', $line_item);
            $currency_code = $wrapper_line_item->commerce_total->currency_code->value(); 
            $amount        = $wrapper_line_item->commerce_total->amount->value();

            if ($amount < 0) { 
                $amount = $amount * -1;
            } 
            $_SESSION['price_line_item'][$sku] = commerce_currency_format($amount, $currency_code);          
        }
    } catch (EntityMetadataWrapperException $ex) {
        watchdog('rcpar_mods', t("Problem on rcpar_mods_all_prices_in_order: ". $ex->getMessage()), array(), WATCHDOG_ERROR);
        return 'ERROR ' . $ex->getMessage() ;
    }

    return 0;
  }
}

