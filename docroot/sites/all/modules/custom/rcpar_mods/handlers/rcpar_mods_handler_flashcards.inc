<?php


/**
 * This field handler display the Flashcards AUD
 *
 */
class rcpar_mods_handler_flashcards_aud extends views_handler_field_entity {
 
  function render($values) {    
  	return rcpar_mods_get_product_price_in_order($values, 'AUD-EFC');
  }
}

/**
 * This field handler display the Flashcards BEC
 *
 */
class rcpar_mods_handler_flashcards_bec extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'BEC-EFC');
  }
}

/**
 * This field handler display the Flashcards REG
 *
 */
class rcpar_mods_handler_flashcards_reg extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'REG-EFC');
  }
}

/**
 * This field handler display the Flashcards FAR
 *
 */
class rcpar_mods_handler_flashcards_far extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FAR-EFC');
  }
}