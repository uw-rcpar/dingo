<?php

/**
 * This field handler display the Audio Lectures AUD
 *
 */
class rcpar_mods_handler_audio_lectures_aud extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'AUD-AUD');
  }
}

/**
 * This field handler display the Audio Lectures BEC
 *
 */
class rcpar_mods_handler_audio_lectures_bec extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'BEC-AUD');
  }
}

/**
 * This field handler display the Audio Lectures REG
 *
 */
class rcpar_mods_handler_audio_lectures_reg extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'REG-AUD');
  }
}

/**
 * This field handler display the Audio Lectures FAR
 *
 */
class rcpar_mods_handler_audio_lectures_far extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FAR-AUD');
  }
}