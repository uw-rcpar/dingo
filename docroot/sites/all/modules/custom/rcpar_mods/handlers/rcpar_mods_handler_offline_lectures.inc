<?php



/**
 * This field handler display the Offline Lectures AUD
 *
 */
class rcpar_mods_handler_offline_lectures_aud extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'AUD-OFF-LEC');
  }
}

/**
 * This field handler display the Offline Lectures BEC
 *
 */
class rcpar_mods_handler_offline_lectures_bec extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'BEC-OFF-LEC');
  }
}

/**
 * This field handler display the Offline Lectures REG
 *
 */
class rcpar_mods_handler_offline_lectures_reg extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'REG-OFF-LEC');
  }
}

/**
 * This field handler display the Offline Lectures FAR
 *
 */
class rcpar_mods_handler_offline_lectures_far extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FAR-OFF-LEC');
  }
}