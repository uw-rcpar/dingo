<?php

/**
 * This field handler display the Online CRAM courses AUD
 *
 */
class rcpar_mods_handler_online_cram_courses_aud extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'AUD-CRAM');
  }
}

/**
 * This field handler display the Online CRAM courses BEC
 *
 */
class rcpar_mods_handler_online_cram_courses_bec extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'BEC-CRAM');
  }
}

/**
 * This field handler display the Online CRAM courses REG
 *
 */
class rcpar_mods_handler_online_cram_courses_reg extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'REG-CRAM');
  }
}

/**
 * This field handler display the Online CRAM courses FAR
 *
 */
class rcpar_mods_handler_online_cram_courses_far extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FAR-CRAM');
  }
}