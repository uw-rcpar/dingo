<?php

/**
 * This field handler display the Course Textbook AUD
 *
 */
class rcpar_mods_handler_course_textbook_aud extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'AUD-BK');
  }
}

/**
 * This field handler display the Course Textbook BEC
 *
 */
class rcpar_mods_handler_course_textbook_bec extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'BEC-BK');
  }
}

/**
 * This field handler display the Course Textbook REG
 *
 */
class rcpar_mods_handler_course_textbook_reg extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'REG-BK');
  }
}

/**
 * This field handler display the Course Textbook FAR
 *
 */
class rcpar_mods_handler_course_textbook_far extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FAR-BK');
  }
}