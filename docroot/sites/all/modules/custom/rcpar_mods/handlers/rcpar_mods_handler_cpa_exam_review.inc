<?php

/**
 * This field handler display the CPA Exam Review AUD
 *
 */
class rcpar_mods_handler_cpa_exam_review_aud extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'AUD');
  }
}

/**
 * This field handler display the CPA Exam Review BEC
 *
 */
class rcpar_mods_handler_cpa_exam_review_bec extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'BEC');
  }
}

/**
 * This field handler display the CPA Exam Review REG
 *
 */
class rcpar_mods_handler_cpa_exam_review_reg extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'REG');
  }
}

/**
 * This field handler display the CPA Exam Review FAR
 *
 */
class rcpar_mods_handler_cpa_exam_review_far extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FAR');
  }
}