<?php

/**
 * This field handler display the Flash Cram Course AUD
 *
 */
class rcpar_mods_handler_flash_cram_course_aud extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'AUD-EFC');
  }
}

/**
 * This field handler display the Flash Cram Course BEC
 *
 */
class rcpar_mods_handler_flash_cram_course_bec extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'BEC-EFC');
  }
}

/**
 * This field handler display the Flash Cram Course REG
 *
 */
class rcpar_mods_handler_flash_cram_course_reg extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'REG-EFC');
  }
}

/**
 * This field handler display the Flash Cram Course FAR
 *
 */
class rcpar_mods_handler_flash_cram_course_far extends views_handler_field_entity {
 
  function render($values) {
  	return rcpar_mods_get_product_price_in_order($values, 'FAR-EFC');
  }
}