(function ($) {
    Drupal.behaviors.rcpar_mods_subscribe_popup = {
        attach: function (context, settings) {

            function is_touch_device() {
                return 'ontouchstart' in window // works on most browsers 
                        || 'onmsgesturechange' in window; // works on ie10
            }

            $(document).ready(function () {
                // only display modal for desktop
                // Check if cookie does not exist. 
                // It means we will display the popup only before creating the cookie
                var getCookie = document.cookie.indexOf('frontPopup=');
            	if (!is_touch_device() && getCookie == -1) {
                    var content = Drupal.settings.mods.content;
                    $("#container-form-popup").html(content);
                    $("#form-popup-modal-button").trigger("click");
                    $(".form-modal-container #edit-submitted-email").attr('placeholder', 'Email Address');
                    $(".form-modal-container .webform-client-form-15904 .button-primary").text('Submit');
                    // Create cookie 
                    document.cookie = "frontPopup=1";
                }
            });
        }
    };
}(jQuery));
