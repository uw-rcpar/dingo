(function ($) {
  $(document).ready(function () {

    // We have two blocks on the page with different priorities for being shown.
    // If either blocks have been closed (using the close block setting), neither should show.
    // But if only the high priority block has been closed, the low priority block should be shown.

    // Hide the low priority block by default.
    $('.priority-low').hide();

    var high_priority_is_closed = false;
    var low_priority_is_closed = false;

    // Loop through blocks that have been closed.
    if (Drupal.settings.rcpar_mods.blocks !== null) {
      var closedBlocks = Drupal.settings.rcpar_mods.blocks.closed_blocks;
      var arrayLength = closedBlocks.length;
      for (var i = 0; i < arrayLength; i++) {
        var block_class = closedBlocks[i].block_class;
        var block_id = closedBlocks[i].bid;
        // Determind if high priority block has been closed by user.
        if ($('#'+block_id).length && $('#'+block_id).hasClass('priority-high')) {
          high_priority_is_closed = true;
        }
        // Determind if low priority block has been closed by user.
        if ($('#'+block_id).length && $('#'+block_id).hasClass('priority-low')) {
          low_priority_is_closed = true;
        }
      }
      // Show the low priority block if it hasn't been closed but the high priority block has.
      if (high_priority_is_closed && !low_priority_is_closed) {
        $('.priority-low').show();
      }
    }
  });
}(jQuery));
