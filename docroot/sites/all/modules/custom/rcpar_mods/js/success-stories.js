(function ($) {

  Drupal.behaviors.rcpar_mods_success_stories = {
    attach: function (context, settings) {

      // Adding touch/swipe functionality to Success Stories JCarousel view block
      $('.view-success-stories .jcarousel-container', context).touchwipe({
        wipeLeft: function () {
          $('.jcarousel-next').click();
        },
        wipeRight: function () {
          $('.jcarousel-prev').click();
        },
        min_move_x: 20,
        min_move_y: 20,
        preventDefaultEvents: true
      });


    }
  }

})(jQuery);