(function ($) {

  $(document).ready(function () {
    // On page load, show first fieldset.
    $('#edit-step-1').show();
  });


  Drupal.behaviors.rcpar_mods_lead_forms = {
    attach: function (context, settings) {

      var lastStep = settings.rcpar_mods_lead_forms.highest_step;
      var previousToLastStep = parseInt(lastStep)-1;

      // Hide the submit button until the last fieldset is shown.
      $('form.multistep-form .form-submit', context).hide();


      // Next links
      $('.next-link', context).click(function () {
        // Get the last character of the id of the link that was clicked.
        // This will be 1, 2 or 3.
        var clicked = $(this).attr('id').substr(-1);

        // Validate fields in current fieldset.
        var currentFieldsetID = $('#edit-step-' + clicked, context).attr('id');
        var emptyRequiredFields = validateFields(currentFieldsetID);

        // If any empty required fields exist, get error message and display it on the page.
        if (emptyRequiredFields.length > 0) {
          var errorMsg = getErrorMsg(currentFieldsetID, emptyRequiredFields);
          $('#' + currentFieldsetID + ' .validation-message').html(errorMsg);
          $('#' + currentFieldsetID + ' #edit-message-' + clicked).show();
        }
        // If all required fields validate:
        else {
          // Empty any error message that exists.
          $('#' + currentFieldsetID + ' .validation-message').html('');
          // Hide any error message div exists.
          $('#' + currentFieldsetID + ' #edit-message-' + clicked).hide();

          // Remove error styling from fields.
          removeErrorStyling();

          // Hide all step fieldsets.
          $('.multistep-form fieldset.step-wrapper').hide();

          // Show the next fieldset.
          var next = parseInt(clicked)+1;
          $('#edit-step-' + next, context).show();

          // Show submit button if next link on previous to last fieldset is clicked,
          // but only if it's not a conditional form.
          if ($(this).attr('id').substr(-1) == previousToLastStep && $('.audience-type-wrapper').length == 0) {
            $('form.multistep-form .form-submit', context).show();
          }
          // If it is a conditional form, only show the submit button if the audience type has been chosen.
          else if ($('.audience-type-wrapper').length > 0 && $('.audience-type-wrapper select').val() !==''){
            $('form.multistep-form .form-submit', context).show();
          }
        }
      });

      // Previous links
      $('.prev-link', context).click(function () {

        // Get the last character of the id of the link that was clicked.
        // This will be 1, 2 or 3.
        var clicked = $(this).attr('id').substr(-1);

        // Subtract 1 from it.
        var prev = parseInt(clicked)-1;

        // Hide all step fieldsets.
        $('.multistep-form fieldset.step-wrapper').hide();

        // Show the previous one.
        $('#edit-step-' + prev, context).show();

        // Hide submit button if previous link on last fieldset is clicked.
        if ($(this).attr('id').substr(-1) == lastStep) {
          $('form.multistep-form .form-submit', context).hide();
        }

        removeErrorStyling();
        $('.validation-message').html('');
        $('#edit-message-' + lastStep).hide();
      });

      // Show submit button after choosing audience type
      // and clear any validation errors if the audience type is changed.
      $('.audience-type-wrapper select', context).change(function () {
        if ($(this).val() !== '') {
          $('form.multistep-form .form-submit', context).show();
        }
        else {
          $('form.multistep-form .form-submit', context).hide();
        }
        removeErrorStyling();
        $('.validation-message').html('');
        $('#edit-message-' + lastStep).hide();
      });

      // Validate the fields in the last fieldset before submitting the form.
      $('.multistep-form .form-submit', context).click(function () {
        // The current fieldset id will be the lastStep.
        var currentFieldsetID = $('#edit-step-' + lastStep, context).attr('id');
        var emptyRequiredFields = validateFields(currentFieldsetID);
        if (emptyRequiredFields.length > 0) {
          var errorMsg = getErrorMsg(currentFieldsetID, emptyRequiredFields);
          $('#' + currentFieldsetID + ' .validation-message').html(errorMsg);
          $('#' + currentFieldsetID + ' #edit-message-' + lastStep).show();
          return false;
        }
      });

      // Function to validate required fields within a specific fieldset.
      // Applies error class to the fields and returns an array containing the human-readable name of the field.
      function validateFields(fieldsetId){
        var emptyRequiredFields = new Array();
        // Loop through each required input within the current fieldset.
        $.each($('#' + fieldsetId + ' .form-control').not('div'), function () {
          // Ignore form control elements without IDs or that are hidden--or the comments field
          if ($(this).attr('id') === 'undefined' || $(this).attr('id') == '' || $(this).is(":hidden") || $(this).attr('id')=='edit-comments') {
            return true;
          }
          // If the required input doesn't have a value,
          if ($(this).val() == '' || $(this).val() == 'International' || $(this).val() == 'US Territories' || $(this).val() == null) {
            // add an error class
            $(this).addClass('error');
            // selectpickers need the error class added to the bootstrap-select div
            if ($(this).hasClass('selectpicker')) {
              $(this).closest('.bootstrap-select').addClass('error');
            }
            // Get the "name" of the field (from it's label or placeholder)
            if ($("label[for='"+this.id+"']").text() !== '') {
              // Remove the asterisk that denotes required field, we only want the name
              var label = $("label[for='"+this.id+"']").text().replace(' *', '')
              emptyRequiredFields.push(label);
            }
            else if ($(this).attr('placeholder') !== 'undefined') {
              emptyRequiredFields.push($(this).attr('placeholder'));
            }
          }
          else {
            // If all fields validate, remove any error classes
            // If the required input doesn't have a value,
            if ($(this).val() != '' || $(this).val() != 'International' || $(this).val() != 'US Territories') {
              $(this).removeClass('error');
              // selectpickers need the error class added to the bootstrap-select div
              if ($(this).hasClass('selectpicker')) {
                $(this).closest('.bootstrap-select').removeClass('error');
              }
            }
          }
          // For email field, we need to validate that it's a valid email address.
          if ($(this).attr('id').indexOf('email') !== -1 && $(this).val() !== '') {
            if (!isValidEmailAddress($(this).val())) {
              $(this).addClass('error');
              emptyRequiredFields.push('You must enter a valid email address');
            }
            else {
              // Otherwise remove error class
              $(this).removeClass('error');
            }
          }
          // For phone field, we need to validate that it's a valid phone number.
          if ($(this).attr('id').indexOf('phone') !== -1 && $(this).val() !== '') {
            if (!isValidPhoneNumber($(this).val())) {
              $(this).addClass('error');
              emptyRequiredFields.push('Phone numbers must be formatted as XXXXXXXXXX (numbers only)');
            }
            else {
              // Otherwise remove error class
              $(this).removeClass('error');
            }
          }
        });
        // Return array of the names (label or placeholder) of required fields that are empty.
        return emptyRequiredFields;
      }

      // Function to remove error class from any required fields.
      function removeErrorStyling() {
        $.each($('.error'), function () {
          $(this).removeClass('error');
        });
      }

      // Function to generate an error message for required fields within a specific fieldset.
      // Returns the error message as a string.
      function getErrorMsg(fieldsetId, emptyRequiredFields) {
        var validationMsg = 'The following fields are required: ';
        // Loop through empty required fields and add them to the validation message.
        $.each(emptyRequiredFields, function (index, field) {
          validationMsg = validationMsg + field.trim();
          // Add a period after the last field.
          if (index == emptyRequiredFields.length -1) {
            validationMsg = validationMsg + '.';
          }
          // Otherwise add a comma.
          else {
            validationMsg = validationMsg + ', ';
          }
        });
        // Return validation message.
        return validationMsg;
      }
      // Function to determine if email address is valid.
      function isValidEmailAddress(emailAddress) {
        var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return pattern.test(emailAddress);
      }
      // Function to determine if phone number is valid.
      function isValidPhoneNumber(phoneNumber) {
        var pattern = new RegExp(/^[0-9]*$/);
        return pattern.test(phoneNumber);
      }
    }
  };

}(jQuery));