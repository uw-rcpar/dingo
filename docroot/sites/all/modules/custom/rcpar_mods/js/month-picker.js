(function ($) {
  Drupal.behaviors.monthPicker = {
    attach: function (context, settings) {

      // Initiate the monthpicker(s) with our options:
      // Display years: five years ago - five years from now.
      $('.monthpicker', context).monthpicker({
        startYear: '1990',
        finalYear: new Date().getFullYear() + 5,
      });

      // For ipads and iphones, when the monthpicker is shown,
      // the year select is focused, causing the year dropdown to be expanded
      // as soon as the monthpicker is visible. This will prevent that.
      if (RCPAR.isMobileBrowser()) {
        $('input.monthpicker', context).focus(function () {
          $('.ui-widget-content select').blur();
        });
      }
    }
  };
}(jQuery));
