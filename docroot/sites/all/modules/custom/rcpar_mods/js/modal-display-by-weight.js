(function ($) {

  $(document).ready(function () {

    // Only display modal for desktop.
    var isMobile = false;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      // You are in mobile browser
      isMobile = true;
    }

    try {

      // If user has seen a modal within the last hour then we are all done here
      if (!RCPAR.modals.modalCanBeShown()) {
        return false;
      }

      // If this is the first time a free trial user lands on the dashboard,
      // we don't want to show any weighted modals until they have seen the dashboard tour modals.
      if (Drupal.settings.rcpar_dasboard_tour_values && Drupal.settings.rcpar_dashboard_user_tour_ended === "0") {
        return false;
      }

      // Make our modal list unique (the array from Drupal contains multiple of the same object).
      modal_list = Drupal.settings.rcpar_mods.blocks.modals;
      var modals = modal_list.reduce(function(index, block){
        var matches = index.filter(function(block2){
          return block.block_delta == block2.block_delta
        })
        if (matches.length == 0)
          index.push(block)
        return index;
      }, [])


      // Sort high to low priority based on weight
      modals.sort(function (a, b) {
        return a.modal_display_weight - b.modal_display_weight;
      });
      // Get the modals the user has already seen.
      var modal_data = RCPAR.modals.getModalData();

      // Loop through our modal blocks and look for the first popup that we need to see
      $.each(modal_list, function (index, value) {

        // Modals do not show on mobile devices unless they have the .show-mobile class
        var show_mobile = ($('.' + value.modal_display + ' .show-mobile').length) ? true : false;

        // Used to decide if this popup is eligible for rendering based on platform considerations
        var renderable = (!isMobile || show_mobile) ? true : false;

        // Block machine name
        var block_machine_name = value.block_machine_name;

        // Check to see if this modal has been seen before
        // and no popup has been seen before the last hour
        var showModal = true;
        // Loop through the modals the user has already seen
        if (modal_data) {
          $.each(modal_data, function(id, data) {
            if ($.isNumeric(id)) {
              // If the user has seen a modal within the last hour, we don't want to show another one.
              if (Math.floor(Date.now() / 1000) - parseInt(data.timestamp) < 3600) { // 3600 seconds in an hour
                showModal = false;
                return false;
              }
            }
          });
          // If the modal that we are trying to display has already been seen,
          // Then we can't show it.
          var modals_to_check = [value.block_machine_name];
          if (RCPAR.modals.userHasSeenModals(modals_to_check)) {
            showModal = false;
          }
        }

        // Show the modal and set tracking stuff.
        if (showModal && renderable) {

          var trackingName = 'BlockPopupSeen.' + value.block_machine_name;

          // Only show the modal if one isn't already visible, such as the NPS modal.
          setTimeout(function () {
            if (!RCPAR.modals.modalIsVisible()) {
              $('.' + value.modal_display + ' .modal').modal();

              // Get the user's token
              var jwt = RCPAR.jwt.getJwt();

              // Record modal as seen in database
              RCPAR.modals.setModalData(value.block_machine_name, jwt);

            }

          }, 500);


          // Tell GA we saw the popup
          rcparSendGA({
            hitType: 'event',
            eventCategory: trackingName,
            eventAction: 'view',
            eventLabel: value.modal_page
          });

          // Save some tracking data with their account
          rcparUserTrackingSaveCookie(trackingName, {
            eventType: 'BlockPopupSeen',
            blockId: value.modal_block,
            blockMachineName: value.block_machine_name,
            referringPage: value.modal_page,

            // Properties forwarded to GA after a registration completes
            eventCategory: trackingName,
            eventLabel: value.modal_page,
            eventAction: 'enroll', // This will be modified by the back end
            eventValue: ''// This will be populated by the back end
          });

          // Send a custom google analytics event when the call to action link is clicked
          $('.' + value.modal_display + ' a').click(function (e) {

            var trackingName = 'BlockPopupClick.' + value.block_machine_name;

            // Send a GA event for the click on this modal's call to action
            rcparSendGA({
              hitType: 'event',
              eventCategory: trackingName,
              eventAction: 'click',
              eventLabel: value.modal_page,
              transport: 'beacon'
            });

            // Save this user data if the user enrolls
            rcparUserTrackingSaveCookie(trackingName, {
              eventType: 'BlockPopupClick',
              blockId: value.modal_block,
              blockMachineName: value.block_machine_name,
              referringPage: value.modal_page,

              // Properties forwarded to GA after a registration completes
              eventCategory: trackingName,
              eventLabel: value.modal_page,
              eventAction: 'enroll', // This will be modified by the back end
              eventValue: '' // This will be populated by the back end
            });
          });
          // Once we have a modal turned on, we are all done
          return false;
        }

      });


    }
    catch (error) {
      // Log the error to the console, but protect browsers that do not support it by wrapping in try/catch
      try {
        console.log(error);
      }
      catch (e) {
      }
    }

  });
}(jQuery));