var RCPAR = (function ($) {
  // locally scoped Object
  var self = {};
  // A function that returns true if the user is on a phone or tablet--or false if not.
  self.isMobileBrowser = function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
      return true;
    }
    else {
      return false;
    }
  };
  return self;
})(jQuery);


// A module/anonymous closure for rcpar modals object.
(function ($, Drupal) {

  // locally scoped Object
  var self = {};

  // get and return modal data
  self.getModalData = function () {
    if (Drupal.settings.rcpar_mods.rcpar_modals) {
      return Drupal.settings.rcpar_mods.rcpar_modals;
    }
    else {
      return false;
    }
  };

  // set modal data via api endpoint
  self.setModalData = function (machinename, token) {
    $.ajax({
      type: 'post',
      url: '/api/modals/set?jwt=' + token,
      data: {
        'machinename': machinename,
      },
      success: function (response) {
        //alert(response.message);
      },
      error: function () {
        alert("error");
      }
    });
  };


  // update modal data via api endpoint
  self.updateModalData = function (machinename, token, status) {
    $.ajax({
      type: 'post',
      url: '/api/modals/update?jwt=' + token,
      data: {
        'machinename': machinename,
        'status': status,
      },
      success: function (response) {
        //alert(response.message);
      },
      error: function () {
        alert("error");
      }
    });
  };

  // update modal timestamp via api endpoint
  self.updateModalTimestamp = function (machinename, token, timestamp) {
    $.ajax({
      type: 'post',
      url: '/api/modals/update/time?jwt=' + token,
      data: {
        'machinename': machinename,
        'timestamp': timestamp,
      },
      success: function (response) {
      },
      error: function () {
        alert("error");
      }
    });
  };


  // determine if user has seen specific modal(s)
  // accepts an array of modal machine names
  // returns bool
  self.userHasSeenModals = function (modal_machine_names) {
    var modalSeen = false;
    // Get the modals the user has already seen.
    var modal_data = RCPAR.modals.getModalData();
    // Determine if the user has asked to never see this popup again
    $.each(modal_data, function(index, value) {
      if ($.inArray(value['machine_name'], modal_machine_names) !== -1) {
        modalSeen = true;
      }
    });
    return modalSeen;
  };

  // Determine via Drupal settings if modal can be shown to user.
  self.modalCanBeShown = function () {
    if (Drupal.settings.rcpar_mods.modal_can_be_shown == "TRUE") {
      return true;
    }
    else {
      return false;
    }
  };
  ;

  // Determine is a modal is already visible.
  self.modalIsVisible = function () {
    if ($('.modal').length && $('.modal').is(':visible')) {
      return true;
    }
    else {
      return false;
    }
  };

  // Get the date a modal was seen
  self.getDateModalWasSeen = function (modal_machine_name) {
    // Get the modals the user has already seen.
    var modal_data = RCPAR.modals.getModalData();
    var dateSeen = '';
    // Determine if the user has asked to never see this popup again
    $.each(modal_data, function(index, value) {
      if (value['machine_name'] === modal_machine_name) {
        dateSeen = value['timestamp'];
      }
    });
    return dateSeen;
  };

  RCPAR.modals = self;
})(jQuery, Drupal);


// A module/anonymous closure for user token.
(function ($, Drupal) {
  // get and return jwt
  self.getJwt = function () {
    if (Drupal.settings.rcpar_mods.rcpar_jwt) {
      return Drupal.settings.rcpar_mods.rcpar_jwt;
    }
    else {
      return false;
    }
  };
  RCPAR.jwt = self;
})(jQuery, Drupal);

