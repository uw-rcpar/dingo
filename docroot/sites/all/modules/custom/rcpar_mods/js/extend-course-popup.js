
(function ($) {
	Drupal.behaviors.rcpar_mods_extend_course = {
		attach: function (context, settings) {

			function is_touch_device() {
      return 'ontouchstart' in window // works on most browsers 
        || 'onmsgesturechange' in window; // works on ie10
      }

      $(document).ready(function () {
        // only display modal for desktop
        // Check if cookie does not exist. 
        // It means we will display the popup only before creating the cookie
        var days = Drupal.settings.mods.days;
        var cookieName = 'ExtendPopup'+days+'=';
        var getCookie = document.cookie.indexOf(cookieName);
     		if (!is_touch_device() && getCookie == -1) {
          var content = Drupal.settings.mods.content;
          $("#container-form-popup").html(content);
          $("#form-popup-modal-button").trigger("click");
          // Create cookie 
          document.cookie = cookieName;
        }
      });
    }
  };
}(jQuery));
