/*
// We are overriding recaptcha.js to fix an iOS scrolling bug.
// The bug scrolls the window on mobile devices down the page after
// the recaptcha closes, losing the user's place in the page/form.
*/
(function ($, window, Drupal) {

  Drupal.behaviors.recaptcha = {
    attach: function (context) {
      $('.g-recaptcha', context).each(function () {
        if (typeof grecaptcha === 'undefined' || typeof grecaptcha.render === 'undefined') {
          return;
        }
        if ($(this).hasClass('recaptcha-processed')) {
          grecaptcha.reset();
        }
        else {
          grecaptcha.render(this, $(this).data());
          $(this).addClass('recaptcha-processed');
        }

      });
    }
  };

  // A function to scroll the window to it's proper place.
  var focusAfterRecaptcha = function (response) {
    $("html, body").animate({ scrollTop: $("fieldset.captcha").offset().top }, "slow");
  };

  // Recaptcha callback function - we will add a callback to our focusAfterRecaptcha function.
  window.drupalRecaptchaOnload = function () {
    $('.g-recaptcha').each(function () {
      // Add a callback to focusAfterRecaptcha for mobile
      if (RCPAR.isMobileBrowser()) {
        $(this).data().callback = focusAfterRecaptcha;
      }
      grecaptcha.render(this, $(this).data());
      $(this).addClass('recaptcha-processed');
    });
  }
})(jQuery, window, Drupal);
