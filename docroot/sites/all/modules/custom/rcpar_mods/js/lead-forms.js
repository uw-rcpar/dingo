(function ($) {

  Drupal.behaviors.rcpar_mods_lead_forms = {
    attach: function (context, settings) {

      // Admin marketing lead forms - adding or editing:
      // Show or hide the "steps" field on page load
      if ($('#edit-multistep input', context).length && $('#edit-multistep input:checked').val() == 1) {
        $('.steps-wrapper', context).show();
      }
      else {
        $('.steps-wrapper', context).hide();
      }
      // Show or hide the "steps" field on input click
      $('#edit-multistep input', context).click(function () {
        if ($('#edit-multistep input:checked').val() == 1) {
          $('.steps-wrapper', context).show();
        }
        else {
          $('.steps-wrapper', context).hide();
        }
      });

      // Admin marketing lead forms - adding or editing:
      // Show or hide the audience "type" field on page load
      // Admin marketing lead forms - adding or editing:
      if ($('#edit-conditional-fields input', context).length && $('#edit-conditional-fields input:checked').val() == 1) {
        $('.audience-wrapper').show();
      }
      else {
        $('.audience-wrapper').hide();
      }
      // Show or hide the "type" field on input click
      $('#edit-conditional-fields input', context).click(function () {
        if ($('#edit-conditional-fields input:checked').val() == 1) {
          $('.audience-wrapper').show();
        }
        else {
          $('.audience-wrapper').hide();
        }
      });
      $('.audience-wrapper select', context).each(function () {
        if ($(this).val() == 1) {
          $('.' + $(this).attr("id")).show();
        }
        else {
          $('.' + $(this).attr("id")).hide();
        }
      });
      // When audience dropdown is changed, show or hide the "type" for dependent fields
      $('.audience-wrapper select', context).change(function () {
        if ($(this).val() == 1) {
          $('.' + $(this).attr("id")).show();
          // force the steps dropdown to step two and disable it.
          $(this).closest('.form-type-radios').find('.steps-wrapper').find('select').val('2').prop('disabled', true);
        }
        else {
          $('.' + $(this).attr("id")).hide();
          // enable the steps dropdown
          $(this).closest('.form-type-radios').find('.steps-wrapper').find('select').prop('disabled', false);
          // clear values on any fields that were checked
          $('.' + $(this).attr("id") + ' input').prop('checked', false);
        }
      });
    }
  };

}(jQuery));