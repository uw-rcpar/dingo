(function ($) {
    Drupal.behaviors.rcpar_mods_reports = {
        attach: function (context, settings) {
            $(".collapser").click(function () {
                $header = $(this);
                $content = $header.next();
                $content.slideToggle(200, function () {
             });
                return false;
            });

            $("#edit-submit-csv").click(function () {
                var url = $(location).attr('pathname');
                if(Drupal.settings.rcpar_mods_csv_link){
                    url = Drupal.settings.rcpar_mods_csv_link;
                } else {
                    url += '/csv';
                }
                url += $(location).attr('search');
                window.open(url, '_blank');
                return false;
            });
        }
    };
}(jQuery));