jQuery(document).ready(function() {
  jQuery('.post-content img').each(function() {
    // Loop through each blog post image on the page

    // Get the offset of the image so that the Pin It button can be positioned over the top left corner of the image
    var imgTop = jQuery(this).offset().top;
    var imgLeft = jQuery(this).offset().left;

    // Get the image's URL so that it is possible to pin it to Pinterest
    var imgUrl = jQuery(this).attr('src');

    // Make a unique ID based on the image position so that the Pin It button can be shown/hidden on hover.
    var id = parseInt(imgTop) + parseInt(imgLeft);
    jQuery(this).data('pinit-id', id);

    // Create Pin It button
    var pinit = jQuery('<a>').attr('href', 'https://www.pinterest.com/pin/create/button/?media=' + imgUrl).attr('data-pin-do', 'buttonPin');
    var img = jQuery('<img>').attr('src', '//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png');

    pinit.append(img);

    // Create a wrapper for the Pin It button which will be positioned at a specific place on the page
    var wrapper = jQuery('<span>').attr('id', id).data('pinit-id', id).addClass('pinit-container').css('position','absolute').css('top',imgTop + 'px').css('left', imgLeft + 10 + 'px');

    // Append the button to the wrapper
    wrapper.append(pinit);

    // Show the button when hovering over an image and hide it when hovering out
    jQuery(this).add(wrapper).hover(function() {
      var pinitId = jQuery(this).data('pinit-id');
      jQuery('#' + pinitId + '.pinit-container').css('visibility','visible');
    }, function() {
      var pinitId = jQuery(this).data('pinit-id');
      jQuery('#' + pinitId + '.pinit-container').css('visibility','hidden');
    });

    // Append the button to the body so that it can be absolutely positioned
    jQuery('body').append(wrapper);
  });
});