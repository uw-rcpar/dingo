(function ($) {

  // A function to hide or show the processing modal created in ajax.
  Drupal.ajax.prototype.commands.hideOrShowProcessingModal = function(ajax, response, status) {
    if ($('#processing-modal').length > 0) {
      $('#processing-modal').modal(response.hideOrShow);
    }
  }

  Drupal.behaviors.rcpar_mods = {
    attach: function (context, settings) {

      // If we are on a page that uses google optimize
      if (Drupal.settings.rcpar_mods && Drupal.settings.rcpar_mods.optimize_pages == window.location.pathname.substring(1)
        // and the page has an add to cart form
        && $('.commerce-add-to-cart').length
        // and the page has a querystring parameter
        && window.location.search) {
        // we need to make sure the add to cart form action includes it
        // but only if it's not already there.
        if ($('.commerce-add-to-cart').prop('action').indexOf(window.location.pathname.substring(1)) < 0) {
          $('.commerce-add-to-cart').prop('action', window.location.pathname + window.location.search);
        }
      }

      // Change selectpicker for mobile devices.
      // This will enable the native scroll wheel for mobile.
      if (RCPAR.isMobileBrowser()) {

        $('.selectpicker', context).selectpicker('mobile');

        // If the select uses Drupal's ajax, it gets moved into a wrapping div
        // with an "input-group" css class when the ajax is triggered for Drupal's
        // ajax progress throbber. This html structure is incompatible with the
        // selectpicker css and messes up the mobile selectpicker. We need to
        // unwrap any selectpickers wrapped in that div.
        $.each($('.selectpicker', context), function () {
          if ($(this).closest('.input-group').length) {
            $(this).unwrap('.input-group');
          }
        });

      }

      // Trigger a modal with a "processing" message for free trial form submits.
      // The form embedded in a node needs a click function.
      $('.large-form .mktgd-free-trial-form-new .form-submit', context).click(function () {
        if ($('#processing-modal').length > 0) {
          $('#processing-modal', context).modal('show');
        }
      });
      // The form when in a popup needs a mousedown function (it's an ajax thing).
      $('.mktgd-free-trial-form-new .form-submit.ajax-processed', context).mousedown(function () {
        if ($('#processing-modal').length > 0) {
          $('#processing-modal').modal('show');
        }
      });

    }
  };

}(jQuery));