(function ($) {
    $(document).ready(function(){
        // Feedback tab
        $("#feedback-tab").on('click',function() {
            // Hide feedback tab
            $(this).toggle();
            // Show the feedback form, slide in from right
            $("#feedback-form").show();
            // Ensure the submit button is enabled
            $("#confirm_submit").removeAttr("disabled");

        });
        // Feedback close button
        $(".feedback-close").click(function () {
            // Hide the feedback form, slide out to the right
            $("#feedback-form").hide();
            // Reset the form field
            $('#edit-feedback').val("");
            // Show the feedback tab after form is gone
            $("#feedback-tab").toggle();
        });
        // Use off() to prevent multiple submissions due to multiple ajax forms in IPQ
        $("#feedback-form form").on('submit',function(event) {
            event.preventDefault();
            var $form = $(this);
            var feedback = $('#edit-feedback').val();
            // Empty field? Drop some validation science on the mofo
            if (feedback.length < 2) {
                $('#edit-feedback').attr("placeholder", "This field is required.").addClass("feedback-validate");
                // Turn the submit button back on
                $("#confirm_submit").prop('disabled',false);
                return false;
            }
            // Disable the submit button
            $("#confirm_submit").prop('disabled',true);
            if(confirm('Are you sure you want to submit this feedback?')) {
                request = $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serialize(),
                    success: function () {
                        // Hide the feedback form, slide out to the right
                        $("#feedback-form").hide();
                        // Show the feedback tab after form is gone
                        $("#feedback-tab").toggle();
                        // Reset the form field
                        $('#edit-feedback').val("");
                    }
                });
            }
            return false;
        });
    });
})(jQuery);