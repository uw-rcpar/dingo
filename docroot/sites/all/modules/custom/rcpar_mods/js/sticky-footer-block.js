(function ($) {

  $(document).ready(function () {

    // The sticky footer should only show after the user has scrolled past certain elements on the page.
    // Possible elements are webforms, if they exist on the page.
    // If there are no webforms, then it should show after the user has scrolled past the hero image.
    // If there is no hero image, then it should show after the user has scrolled past the header on the page.
    var scrolledPast = $('#main-content');
    if ($('.hero-image').length) {
      scrolledPast = $('.hero-image');
    }
    if ($('.region-content .webform-client-form').length) {
      // If the only webform on the page is the one in the sticky footer, don't use it to determine scrolledPast.
      if(!$('.region-content .webform-client-form').first().closest('.sticky-footer').length){
        scrolledPast = $('.region-content .webform-client-form').first();
      }
    }
    // For the home page, we want the sticky footer to appear after the slideshow banner is scrolled past.
    if ($('.helping-students-pass').length) {
      scrolledPast = $('.helping-students-pass');
    }
    // Get offset of element scrolled past.
    var offset = scrolledPast.offset();
    var top = offset.top;
    // The top offset plus the height of the element give us the bottom position of the scrolled past element.
    var bottom = top + scrolledPast.outerHeight();

    // When the user has scrolled to the footer of the page, the sticky footer needs to stack on top of the footer.
    // iOS has issues detecting when user has scrolled to the bottom of the page, so we'll just detect if the
    // footer element is in the viewport.
    footerIsInViewport = function() {
      if ($('#footer').length) {
        var elementTop = $('#footer').offset().top;
        var elementBottom = elementTop + $('#footer').outerHeight();

        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
      }
    };
    // For pages with modals on them, the sticky footer's z-index needs to change while the modal is open.
    $('.modal:not(#stickyModal)').on('shown.bs.modal', function () {
      $('.sticky-footer').css('z-index', '200');
    });
    $('.modal:not(#stickyModal)').on('hidden.bs.modal', function () {
      $('.sticky-footer').css('z-index', '450');
    });


    // Do stuff when user is scrolling the window.
    $(window).scroll(function(event) {


      // Show sticky footer when window has scrolled far enough past certain elements on the page.
      if ($('.sticky-footer').is(':hidden')) {
        if ($(window).scrollTop() >= bottom) {
          $('.sticky-footer').show();
          $('body').addClass('with-sticky-footer');
        }
      }
      // Show sticky footer when window is scrolled far enough back up.
      if ($('.sticky-footer').is(':visible')) {
        if ($(window).scrollTop() < bottom) {
          $('.sticky-footer').hide();
          $('body').removeClass('with-sticky-footer');
        }
      }
      // If user has scrolled to the footer of the page, add class that will make the sticky footer not sticky.
      if (footerIsInViewport()) {
        $('.sticky-footer').addClass('stacked');
      }
      // If the user has scroll back up, the sticky footer needs to be sticky again.
      if (!footerIsInViewport()) {
        $('.sticky-footer').removeClass('stacked');
      }
    });




  });

}(jQuery));
