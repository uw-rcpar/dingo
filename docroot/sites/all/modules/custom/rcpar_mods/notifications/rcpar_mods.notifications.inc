<?php

use RCPAR\Entities\Notifications\Broadcast;
use RCPAR\Entities\Notifications\Channel;

/**
 * Notifications admin landing page. Provides a list of links to the individual settings pages.
 * @return string
 */
function rcpar_mods_notifications() {
  $output = '';

  // Get all the channels
  foreach (Channel::getAllChannels() as $channel) {
    $output .= '<div class="notifications-channel-list" style="padding-left: 20px;padding-right: 20px; border: 1px solid #eee; margin-bottom: 40px;">';
    $output .= '<h2 style="margin-top:30px; margin-bottom: 10px; font-weight: bold">' . $channel->getName() . ' - ' . $channel->getDescription() . '</h2>';
    if ($channel->getMachineName() == Channel::PRIVATE_CHANNEL) {
      $output .= '<div style="margin-top:10px; margin-bottom: 10px;">Broadcasts assigned to the Private channel are not sent automatically. They are triggered by specific events for individual users.</div>';
    }


    // ACTIVE BROADCASTS IN CHANNEL
    $output .= '<h3 style="margin-top:30px; margin-bottom: 10px; font-weight: normal; text-transform: uppercase; font-size: 14px;">Active Broadcasts</h3>';
    $header = array('Broadcast', 'Summary', 'Type', 'Expiration');
    $rows = array();
    $broadcasts = $channel->getActiveBroadcasts();
    if ($broadcasts) {
      foreach ($broadcasts as $broadcast) {
        $rows[] = array(
          'data' => array(
            array(
              'data' => '<a href="/node/' . $broadcast->getNid() . '/edit">' . $broadcast->getTitle() . '</a></div>',
              'style' => 'width: 10%; vertical-align: top;'
            ),
            array('data' => $broadcast->getSummary(), 'style' => 'width: 70%; vertical-align: top;'),
            array('data' => $broadcast->getType(), 'style' => 'width: 5%; vertical-align: top;'),
            array(
              'data' => format_date($broadcast->getExpire(), "m/d/Y"),
              'style' => 'width: 15%; vertical-align: top;'
            ),
          ),
        );
      }
      $output .= theme('table', array('header' => $header, 'rows' => $rows));
    }
    else {
      $output .= '<div style="border-bottom: 1px solid #ddd; padding-bottom: 10px;">No active broadcasts.</div>';
    }

    // INACTIVE BROADCASTS IN CHANNEL
    $output .= '<h3 style="margin-top:30px; margin-bottom: 10px; font-weight: normal; text-transform: uppercase; font-size: 14px;">Inactive Broadcasts</h3>';
    $inactive_broadcasts = $channel->getInactiveBroadcasts();
    $rows = array();
    if ($inactive_broadcasts) {
      foreach ($inactive_broadcasts as $broadcast) {
        $rows[] = array(
          'data' => array(
            array(
              'data' => '<a href="/node/' . $broadcast->getNid() . '/edit">' . $broadcast->getTitle() . '</a></div>',
              'style' => 'width: 10%; vertical-align: top;'
            ),
            array('data' => $broadcast->getSummary(), 'style' => 'width: 70%; vertical-align: top;'),
            array('data' => $broadcast->getType(), 'style' => 'width: 5%; vertical-align: top;'),
            array(
              'data' => format_date($broadcast->getExpire(), "m/d/Y"),
              'style' => 'width: 15%; vertical-align: top;'
            ),
          ),
        );
      }
      $output .= theme('table', array('header' => $header, 'rows' => $rows));
    }
    else {
      $output .= '<div style="border-bottom: 1px solid #ddd; padding-bottom: 10px;">No inactive broadcasts.</div>';
    }


    // RECENT BROADCASTS IN CHANNEL SENT
    $output .= '<h3 style="margin-top:30px; margin-bottom: 10px; font-weight: normal; text-transform: uppercase; font-size: 14px;">Recently Sent Notifications</h3>';

    if($notifs = $channel->getRecentNotifications()) {
      $header = array('Broadcast', 'Type', 'Message', 'Expires', 'User', 'Is read', 'Read on', 'Created');
      $rows = array();
      foreach ($notifs as $n) {
        // Get the broadcast associated with this notification
        $broadcast_link = 'None';
        if($b = $n->getBroadcast()) {
          $broadcast_link = l($b->getTitle(), "node/{$b->getId()}/edit");
        }
        $rows[] = array(
          $broadcast_link,
          $n->getType(),
          $n->getMessage(),
          $n->getExpire('m/d/Y'),
          $n->getUser()->getEmail(),
          $n->getIsRead() ? 'Yes' : 'No',
          $n->getReadOn('m/d/Y'),
          $n->getCreated('m/d/Y')
        );
      }
      $output .= theme('table', array('header' => $header, 'rows' => $rows));
    }
    else {
      $output .= '<div>No notifications in this channel yet.</div>';
    }


    $output .= '</div> <!-- end notifications-channel-list -->';
  }

  return $output;
}

/**
 * Implements hook_form_FORM_ID_form_alter
 */
function rcpar_mods_form_notifications_template_node_form_alter(&$form, &$form_state, $form_id) {
  // Create an option list of all channels.
  $c = new Channel();
  $channels = $c->getAllChannels();
  $options = array();
  foreach ($channels as $channel) {
    $options[$channel->getId()] = $channel->getName();
  }

  // If we are on the node edit page, get the previously selected channels
  // and add them as a default value.
  $default_value = array();
  if (!empty($form['nid']['#value'])) {
    $b = new Broadcast($form['#node']);
    $existing_channels = $b->getChannelIds();
    $default_value = array_pop($existing_channels);
  }

  // Add channels to the form as checkboxes.
  $form['field_channel'] = array(
    '#title' => t('Channels'),
    '#type' => 'radios',
    '#description' => t('Select channels for this broadcast.'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $default_value,
  );

  // Add a custom submit handler that will record the channel in the database.
  $form['actions']['submit']['#submit'][] = 'rcpar_mods_notifications_form_submit';
}

/**
 * Submit handler to add record database for channels when notification nodes are created.
 * @param $form_id
 * @param $form_state
 * @throws Exception
 */
function rcpar_mods_notifications_form_submit($form_id, &$form_state) {
  // Get all channels that were selected in the node.
  $channels = array($form_state['values']['field_channel']);
  // Get broadcast.
  $b = new Broadcast($form_state['nid']);
  // Check for existing channels that are already assigned to this broadcast.
  $existing_channels = $b->getChannelIds();
  // Find any channels that need removing.
  $channels_to_remove = array_merge(array_diff($channels, $existing_channels), array_diff($existing_channels, $channels));
  if (!empty($channels_to_remove)) {
    foreach ($channels_to_remove as $remove_id) {
      $b->deleteChannel($remove_id);
    }
  }
  // Add channels if they're not already assigned to the broadcast.
  foreach ($channels as $channel => $channel_id) {
    if (!in_array($channel_id, $existing_channels)) {
      $b->addChannel($channel_id);
    }
  }
}
