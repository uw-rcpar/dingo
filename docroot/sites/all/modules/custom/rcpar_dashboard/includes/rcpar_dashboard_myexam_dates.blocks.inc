<?php
  
 /**
 * Dashboard: My Exam Dates Block Content
 */
function rcpar_dashboard_get_myexam_dates() {
  drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/exam_dates.js');
  drupal_add_css(drupal_get_path('module', 'rcpar_dashboard') . '/css/exam_dates.css');
  
  $output = '';
  $next_exam_days = 0;
  $output = theme("myexam_dates", 
              array(
              "next_exam" => $next_exam_days, 
              "entitlements" => array(),
              )
            );

  return $output;
}

/**
 * preprocess to set values for My Exam Date Block
 */
function rcpar_dashboard_preprocess_myexam_dates(&$vars) {
  $next_date = rcpar_dashboard_get_next_exam_date();
  $vars["next_exam"] = $next_date;
  $form = drupal_get_form('rcpar_dashboard_myexam_dates_form');
  $vars["date_form"]= render($form);
}

function rcpar_dashboard_myexam_dates_form() {
  global $user;
  $form = array();
  $nodes = user_entitlements_get_entitlements($user);   
  $user_exam_dates = user_entitlements_get_entitlements_skus($nodes);           
  $user_exam_dates_info = rcpar_dashboard_get_user_exam_dates();    
  $courses = rcpar_dashboard_entitlements_options();

  // Get the next exam date closest to today.
  $next = array();
  krsort($user_exam_dates_info);
  foreach($user_exam_dates_info AS $d => $sku) {
    if (isset($sku['sku']) && $d >= time()) {
      $next = $sku;
    }
  }
  $user_profile = entity_metadata_wrapper('user', $user->uid);

  foreach($courses AS $cr) {
    $u_course_filed = "field_" . strtolower($cr). "_exam_date";
    $u_course = $user_profile->{$u_course_filed}->value();
        
    //if (isset($user_exam_dates[$cr])) {
      if($u_course && is_numeric($u_course)){
        $date = new DateTime($u_course);
        $desc = $date->format('m/d/y');
        $rawdate = $u_course;
      }else{
        $desc = (isset($user_exam_dates_info[$cr]['formatted'])) ? $user_exam_dates_info[$cr]['formatted'] : '---';
        $rawdate = (isset($user_exam_dates_info[$cr]['raw'])) ? $user_exam_dates_info[$cr]['raw'] : '00/00/0000' ;
      }

      $action = (isset($user_exam_dates_info[$cr]['raw'])) ? 'edit' : 'add' ;

      // Set class so we can hide all others and only display next date coming up.
      $upcoming = '';
      if (isset($next['sku']) && $next['sku'] == $cr) {
        $upcoming = ' upcoming';
      }

      $form[strtolower($cr) . '_exam_date'] = array(
        '#title' => t($cr),
        '#type' => 'date_popup',
        '#default_value' => $rawdate . ' 00:00:00',
        '#date_format' => 'm/d/y',
        '#description' => $desc,
        '#ajax' => array(
          'callback' => 'rcpar_dashboard_set_myexam_date_ajax',
          'wrapper' => $cr.'-description', 
          'method' => 'replace',
          ),
        '#prefix' => '<div class="form-item-wrapper' . $upcoming . '"><div class="edit" cr="' . strtolower($cr) . '">' . $action . '</div><div id="'.$cr.'-description">',
        '#suffix' => '</div></div>',
      );
     //}
  }    
  return $form;
}

function rcpar_dashboard_set_myexam_date_ajax($form, &$form_state) {
  $commands = array();

  // Save values submitted here.
  $dates_exams = array();
  $proceed = rcpar_dashboard_set_user_exam_dates($form_state['values']);

  foreach ($form_state['values'] AS $name => $val) {
    if (strstr($name, '_exam_date')) {
      if ($name == $form_state['triggering_element']['#name']) {
        $value_date = !empty($val) ? date('m/d/y', strtotime($val)) : '---';
        $commands[] = ajax_command_invoke(NULL, "resetExamDateField", array(str_ireplace('_', '-', $name)));
        // Validate if rcpar_dashboard_tour.js has been add to the site.
        $scripts = drupal_add_js();
        $path =  drupal_get_path('module', 'rcpar_dashboard') . '/js/rcpar_dashboard_tour.js';
        if(isset($scripts[$path])) {
          // invoke if tour has been added.
          $commands[] = ajax_command_invoke(NULL, "refreshTour");
        }
        $commands[] = ajax_command_html('.form-item-' . str_ireplace('_', '-', $name) . ' .help-block', $value_date);
        $commands[] = ajax_command_html('.notice-date-exams', t(strtoupper(strstr($name, '_', true)) . ' Exam Dates have been successfully updated.'));
      }       
      $dates_exams[$name]['raw'] = $val;
    }
  }
  $next_date = rcpar_dashboard_get_next_exam_date($dates_exams);        
  $commands[] = ajax_command_html('.next-date', $next_date);

  return array('#type' => 'ajax', '#commands' => $commands);
}

function rcpar_dashboard_set_user_exam_dates($values) {
  $result = array();
  if (user_is_logged_in()) {
    $date = new DateTime();
    $date = $date->getTimestamp();
    global $user;
    $user_profile = entity_metadata_wrapper('user', $user->uid);
    $hub_trigger = 0;
    if (module_exists('hub_workflow')){ 
      $hub_trigger = 1;
    }
    foreach ($values AS $name => $val) {
      $fld = 'field_' . $name;
      if (strstr($fld, '_exam_date')) {
        $timestamp_val = new DateTime($val);
        $timestamp_val = $timestamp_val->getTimestamp();
        $sku = str_replace('_exam_date', '', $name);
        if ($hub_trigger) {
          $property = $sku == "aud" ? 'exam_date' : 'exam_date_' . $sku;
          $param = array($property => $val);
          hub_workflow_set_field_date($user->uid, $param);
        }
        $user_profile->{$fld}->set("$val");
        $user_profile->save();
        if ($date < $timestamp_val) {
          $result[] = $name;
        }
      }
    }
    cache_clear_all("u{$user->uid}_rcpar_dashboard_exam_dates", 'cache', TRUE); 
  }
  return $result;
}

/**
 * Returns a list of exams with respective dates info
 * @return array $user_exam_dates
 */
function rcpar_dashboard_get_user_exam_dates($uid = null) {  
  
  if (is_null($uid)) {
    global $user;
  } else {
    $user = user_load($uid);    
  }
  $user_exam_dates = &drupal_static(__FUNCTION__);
  if (!isset($user_exam_dates) || $user_exam_dates['uid'] != $user->uid) {
    if ($cache = cache_get("u{$user->uid}_rcpar_dashboard_exam_dates")) {
        $user_exam_dates = $cache->data;
      }else {    
          $user_profile = entity_metadata_wrapper('user', $user->uid);
          $courses = rcpar_dashboard_entitlements_options();
          foreach($courses AS $cr) {
            $fld = 'field_' . strtolower($cr) . '_exam_date';
            $date = $user_profile->{$fld}->value();
            if (!empty($date)) {
              $key = strtotime($date);
              $user_exam_dates[$key]['sku'] = $cr;
              $user_exam_dates[$cr]['raw'] = $date;
              $user_exam_dates[$cr]['formatted'] = ($date != '---') ? date('m/d/y', strtotime($date)) : $date ;              
            }
        }
        $user_exam_dates['uid'] = $user->uid;
        cache_set("u{$user->uid}_rcpar_dashboard_exam_dates", $user_exam_dates, 'cache');
      }    
  } 
  return $user_exam_dates;
}

function rcpar_dashboard_date_popup_process_alter(&$element, &$form_state, $context) {
  unset($element['date']['#description']);
  unset($element['date']['#title']);
}

function rcpar_dashboard_get_next_exam_date($current_dates = array()) {
  $next_date = 0;
  $dates = array();
  $user_exam_dates = !empty($current_dates) ? $current_dates : rcpar_dashboard_get_user_exam_dates();
  if(!empty($user_exam_dates)){      
    foreach($user_exam_dates AS $date) {
      if (isset($date['raw'])) {
        $dates[] = $date['raw'];
      }
    }  
    if (!empty($dates)) {
      sort($dates);
      $exam_date = $dates[0];

      // Find the date that is greater than today.
      foreach ($dates AS $date) {
        if (time() <= strtotime($date)) {
          $exam_date = $date;
          $next_date = _date_difference($exam_date, date('Y-m-d'));
          break;
        }
        else {
          $next_date = 0;
        }
      }
    }
  }
  return $next_date;
}

function _date_difference($date_1 , $date_2 , $differenceFormat='%a') {
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
        
    if(is_object($datetime1) && is_object($datetime2)){
      $interval = date_diff($datetime1, $datetime2);      
      return $interval->format($differenceFormat);
    }        
    return 0;    
}
