<?php

function rcpar_dashboard_statistics_footer() {
    global $user;
    $custom_class = "";
    drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/radial_elements.js');

// Check if AICPA should be active
    $var = variable_get("aicpa_records", array());
    if (in_array($user->uid, $var)) {
        $custom_class = "active";        
    }
    
    $vars = array(
        "uid" => $user->uid,
        "exams" => rcpar_dashboard_get_cpa_exams(),
        "custom_class" => $custom_class,
    );
    return theme('exam_review_footer', $vars);
}

function rcpar_dashboard_cram_statistics_footer() {
    global $user;
    $custom_class = "";
    
    $vars = array(
        "uid" => $user->uid,
        "exams" => rcpar_dashboard_get_cram_exams(),
        "custom_class" =>  $custom_class 
    );
    return theme('cram_exam_review_footer', $vars);
}

/*
 * Process radial grafifcs for CPA exams
 */

function rcpar_dashboard_statistics_content($exams= array()) {
    global $user;
    // RESOURCES
    libraries_load('d3');
    drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/radial_elements.js');
    drupal_add_css(drupal_get_path('module', 'rcpar_dashboard') . '/css/radial_elements.css');
    
    if(empty($exams)){
        $exams = rcpar_dashboard_get_cpa_exams();
    }
    
    // We also need to collect the user exam versions information to know for which
    // sections we should display the exam version selection UI
    $exam_version_info = array();
    foreach($exams as $sku => $info){
      $exam_version_info[$sku] = user_entitlements_get_exam_version_for_course($sku, $user);
    }
    $vars = array(
        "uid" => $user->uid,
        "exams" => $exams,
        "exams_versions" => $exam_version_info,
        "average" => number_format(rcpar_dashboard_get_cpa_exams(1), 2),
        // These are not necessarily expired, just eligible to be extended, which means they display a little differently
        // in exam_review_content.tpl.php
        "expired_unlimited_exams" => rcpar_dashboard_unlimited_activation_skus(),
    );

    return theme('exam_review_content', $vars);
}

/**
 * Use this function to get the exam version modals and initialize them
 * @return string
 * - Returns themed exam selection modals required html
 */
function rcpar_dashboard_exam_version_selection_modals() {
  // if exam versioning is toggled on
  if (exam_version_is_versioning_on()) {
    global $user;
    drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/exam_version_selection.js');
    drupal_add_css(drupal_get_path('module', 'rcpar_dashboard') . '/css/exam_version_selection.css');

    // We need to know if the user have some progress for each of the sections and send it to the js widget
    $sections_to_check = array('AUD', 'BEC', 'FAR', 'REG');
    $sections_info = array();
    foreach ($sections_to_check as $section) {
      if (isset($user->user_entitlements['products'][strtoupper($section)])) {
        $have_data = FALSE;
        $version = '2016';
        $ent = $user->user_entitlements['products'][strtoupper($section)];

        $version = $ent['exam_version'] ? $ent['exam_version'] : exam_version_get_default_version() ;

        $selected_textbook = $ent['selected_textbook'];
        $sections_info[$section] = array(
          'have_data'            => $have_data,
          'current_exam_version' => $version,
          'selected_textbook'    => $selected_textbook,
        );
      }
    }

    $info = array('previous_data_info' => $sections_info);
    $params = drupal_get_query_parameters();
    if (isset($params['force_sel'])) {
      $info['force_selection'] = $params['force_sel'];
      $info['redirect_after_select'] = $params['destination'];
    }


    drupal_add_js(array('rcpar_dashboard_ev' => $info), 'setting');

    return theme('exam_version_modals');
  }
}

/*
 * Process radial grafifcs for CRAM exams
 */

function rcpar_dashboard_cram_statistics_content() {
    global $user;    
    // RESOURCES
    libraries_load('d3');
    drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/radial_elements.js');
    drupal_add_css(drupal_get_path('module', 'rcpar_dashboard') . '/css/radial_elements.css');

    $vars = array(
        "uid" => $user->uid,
        "exams" => rcpar_dashboard_get_cram_exams(),
        "average" => number_format(rcpar_dashboard_get_cram_exams(1), 2),
    );

    return theme('cram_exam_review_content', $vars);
}

/**
 * 
 * @return <int> number of courses that user has on its object
 */
function rcpar_dashboard_courses_number() {
    global $user;
    $nodes = user_entitlements_get_entitlements($user);
    $standard_skus = user_entitlements_get_entitlements_skus($nodes);
    $total_courses = 0;
    if (count(array_intersect(rcpar_dashboard_entitlements_options(), $standard_skus))) {
        $total_courses++;
    }
    if (count(array_intersect(rcpar_dashboard_entitlements_crams_options(), $standard_skus))) {
        $total_courses++;
    }

    return $total_courses;
}

/**
 * Gets a custom class for footer links
 * @param String $case
 * @return string
 */
function rcpar_dashboard_footer_links_get_custom_classes($case){
  $result = "";  
  
  switch ($case) {
    case "aicpa":
      // For: dashboard/my-courses/AICPA-released-questions
      $nid = 6572;
      break;
    case "course_textbook":
      // For: /dashboard/my-courses/course-textbook-updates
      $nid = 6571;
      break;
    case "course_breakdowns":
      // For: /dashboard/my-courses/course-breakdowns
      $nid = 6530;
      break;
  }  
  if (node_mark($nid, REQUEST_TIME) > 0) {    
    $last_viewed = node_last_viewed($nid);
    $node = node_load($nid);
    $updated_date = $node->changed;
    if($updated_date > $last_viewed){
      $result = "active";
    }    
  }  

  return $result;
}


/**
 * A function to determine whether we should show the NPS form modal to the user.
 * @param \RCPAR\User $u
 * @return bool
 */
function rcpar_dashboard_nps_modal_should_show(\RCPAR\User $u) {

  $show_modal = FALSE;

  /*
   * Here is the logic:
   * First, we only want to run this code if a modal has been shown to the user in the last hour.
   * If so, we don't want to hassle them with another one right away, so we won't display the NPS modal.
   * Then check the db to see if the modal has ever been shown to the user.
   * If it hasn't, check user entitlements for any regular course that is older than 4 months.
   * If user only has a cram course, then verify they have watched more than 25% of the course.
   * If either of these are true, show the modal, and log the date and user info in the database.
   *
   * If an entry for the user already exists in the database, then check to be sure they have
   * valid entitlements. It doesn't matter what kind of course (regular or cram) they have,
   * we will show the modal every four months from the the last date it was shown.
   *
  */

  if (!\RCPAR\Modeling\ModalModel::modalCanBeShown($u->getUid())) {
    return FALSE;
  }

  // Check if the user has ever seen the nps form modal by looking up database record.
  $query = db_query("select * from {rcpar_nps_log} where uid = :uid order by date desc", array(':uid' => $u->getUid()));
  $result = $query->fetchObject();

  // If no rows exist in the rcpar_nps_log table, user has never seen the nps modal.
  // Check user entitlements to see if they've had them longer than 4 months.
  // If user only has cram entitlements, check to see if they've completed more than 25% of the course.
  if (!$result) {
    // First check for regular course entitlements.
    $e = $u->getValidEntitlements()->filterByOnlineCourse()->filterByPaid();
    if (!empty($e->getEntitlements())) {
      $start_date = $e->getEarliestCreatedDate();
      if ($start_date <= strtotime('-4 months')) {
        // It has been 4 months or more since the user has seen the nps form modal.
        // Show the modal.
        $show_modal = TRUE;
      }
    }
    // If user has no regular course entitlements, check for cram entitlements.
    else {
      $e = $u->getValidEntitlements()->filterByCramCourse()->filterByPaid();
      if (!empty($e->getEntitlements())) {
        // Loop through each cram entitlement and determine if the user has completed more than 25%
        // of the video for any one cram course.
        foreach ($e->getEntitlements() as $index => $ent) {
          $percentage_viewed = \RCPAR\Course::getProgressPercentageForUser($u, $e);
          if (!empty($percentage_viewed) && $percentage_viewed[0]['percentage'] >= 25) {
            // User has viewed more than 25% of a cram course.
            // Show the modal.
            $show_modal = TRUE;
            break;
          }
        }
      }
    }
  }
  else {
    // If user has seen the modal before, check if it's been 4 months since the last time they saw the modal.
    $saved_date = $result->date; // date of last submission
    if ($saved_date <= strtotime('-4 months')) {
      // Check if user has any active entitlements and if they do, show the modal.
      $e = $u->getValidEntitlements()->filterByOnlineAndCramCourse()->filterByPaid();
      if (!empty($e->getEntitlements())) {
        $show_modal = TRUE;
      }
    }
  }

  return $show_modal;
}

/**
 * A function that returns the user's purchaser type. Used for NPS form modal tracking.
 * @param \RCPAR\User $u
 * @return string
 */
function rcpar_dashboard_get_purchaser_type(\RCPAR\User $u) {

  // Get user's valid online and cram course entitlements.
  $e = $u->getValidEntitlements()->filterByOnlineAndCramCourse();

  // Determine the user's purchaser type. Possible purchaser types are:
  // single - has purchased a single regular course
  // single-cram - has purchased a single cram course
  // combo - has purchased a combination of courses but not a bundle
  // elite - has purchased the elite bundle
  // premier - has purchased the premier bundle
  // select - has purchased the select bundle

  $purchaser_type = 'single'; // We'll use single as the default
  if ($e->count() > 1) {
    $purchaser_type = 'combo'; // Change to combo if more than one valid entitlement
  }
  else {
    if ($e->count() == 1 && $e->filterByCramCourse()->isNotEmpty()) {
      $purchaser_type = 'single-cram'; // If only one valid cram entitlement
    }
  }

  // Change the purchaser type if the user has purchased a course bundle.
  // Loop through entitlements and look for a bundle.
  foreach ($e->getEntitlements() as $ent) {
    switch ($ent->getBundledProduct()) {
      case 'FULL-DIS': $purchaser_type = 'select'; break 2;
      case 'FULL-PREM-DIS': $purchaser_type = 'premier'; break 2;
      case 'FULL-ELITE-DIS': $purchaser_type = 'elite'; break 2;
    }
  }

  return $purchaser_type;
}


/**
 * A function that returns a comma separated list of the user's purchased skus. Used for NPS form modal tracking.
 * @param \RCPAR\User $u
 * @return string
 */
function rcpar_dashboard_get_skus(\RCPAR\User $u) {
  // Get user's valid online and cram course entitlements.
  $e = $u->getValidEntitlements()->filterByOnlineAndCramCourse();
  // Determine what skus the user has purchased.
  $skus = '';
  if ($e->getSkus()) {
    $skus = implode(", ", $e->getSkus());
  }
  return $skus;
}

/**
 * Use this function to display NPS (Net Promoter Score) modal on dashboard
 * @return string - Theme
 */
function rcpar_dashboard_nps_display_modal($purchaser_type, $skus, \RCPAR\User $u) {

  // WE ARE GOING TO SHOW THE MODAL!
  // Each time the modal is displayed, we create a log in the table to log the date, even if the user doesn't answer the form.
  // First we save the initial data, and later we'll update it with the form submission.
  $fields = array(
    'uid'            => $u->getUid(),
    'date'           => REQUEST_TIME,
    'skus'           => $skus,
    'purchaser_type' => $purchaser_type,
  );

  // Save data into custom table and get back the id of the inserted row.
  db_insert('rcpar_nps_log')->fields($fields)->execute();
  $inserted_id = Database::getConnection()->lastInsertId();

  // Add javascript to trigger the modal and css to style it.
  drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/rcpar_dashboard_nps_modal.js');
  drupal_add_css(drupal_get_path('module', 'rcpar_dashboard') . '/css/rcpar_dashboard_nps_modal.css');

  // Get the NPS form, passing the inserted row id. The row id will be used to update
  // the record when the form is submitted.
  $form = drupal_get_form('rcpar_dashboard_nps_form', $inserted_id);
  // Create our modal and render it.
  $m = new RCPARModal('rcpar-dashboard-nps','rcpar-dashboard-nps-modal',array('data-backdrop' => 'true', 'role' => 'dialog'));
  $m->setHeaderContent('<h3 class="modal-title"> We\'d love to hear your feedback!<button type="button" class="close finish-break" data-dismiss="modal" aria-hidden="true">&times;</button></h3>')
    ->setBodyContent(drupal_render($form));
  return $m->render();

}


/**
 * Display a form for NPS comment and score submission 
 * @param type $form
 * @param type $form_state
 * @param type $inserted_id
 * @return form
 */
function rcpar_dashboard_nps_form($form, &$form_state, $inserted_id){
  $form['title']= array(
    '#type' => 'item',
    '#title' => t('How likely are you to recommend Roger CPA Review to a friend or colleague?'),
  );
  $form['options']= array(
    '#type' => 'container',
    '#attributes' => array('class' => array('options-wrap','clearfix'), 'id' => array("selected-option-field")),
  );
  // Display radios for non-phone users
  $form['options']['score'] = array(
    '#type' => 'radios',
    '#options' => array(
      '0' => t('0'),
      '1' => t('1'),
      '2' => t('2'),
      '3' => t('3'),
      '4' => t('4'),
      '5' => t('5'),
      '6' => t('6'),
      '7' => t('7'),
      '8' => t('8'),
      '9' => t('9'),
      '10' => t('10')
    ),
  );
  // Hide radios for phone users and display a select list
  $form['options']['selected_option'] = array(
    '#type' => 'select',
    '#options' => array(
      '0' => t('Choose one'),
      '1' => t('1 -  Not at all Likely'),
      '2' => t('2'),
      '3' => t('3'),
      '4' => t('4'),
      '5' => t('5'),
      '6' => t('6'),
      '7' => t('7'),
      '8' => t('8'),
      '9' => t('9'),
      '10' => t('10 - Extremely Likely')
    ),
    '#attributes' => array('class' => array('nps-select')),
  );
  $form['nps_id'] = array(
    '#type' => 'hidden',
    '#value' => $inserted_id,
  );
  $form['tags'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('tags','clearfix')),
  );
  $form['tags']['left']= array(
    '#type' => 'item',
    '#title' => t('Not at all likely'),
  ); 
  $form['tags']['right']= array(
    '#type' => 'item',
    '#title' => t('Extremely likely'),
  );
  $form['comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Any additional comments?'),
    '#attributes' => array('maxlength' => array(300)),
    '#description' => "<span class='count'>0</span>/300"
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#ajax' => array(
      'callback' => 'rcpar_dashboard_nps_submit',
      'wrapper' => 'rcpar-dashboard-nps-form',
    ),
  );
  return $form;
}

/**
 * Custom Ajax submit for updating db row of NPS score
 * @param type $form
 * @param type $form_state
 * @return string of thanks message if succeed 
 */
function rcpar_dashboard_nps_submit($form, $form_state){ 
  $nps_id = $form_state['values']['nps_id'];
  // Non-phone users were shown radio options
  if ($form_state['values']['score'] != '') {
    $score = $form_state['values']['score'];
  }
  // Phone users were shown a select list
  else {
    $score = $form_state['values']['selected_option'];
  }
  $result = db_update('rcpar_nps_log')
    ->fields(
      array(
        'comment' => $form_state['values']['comment'],
        'score' => $score,
      )
    )
    ->condition ('id', $nps_id)
    ->execute();
  if($result){
    return 'Thank you!';
  }
}

/**
 * Check for expired unlimited access entitlements. If found we return
 * the sku and progress meter data
 * @return array
 */
function rcpar_dashboard_unlimited_activation_skus() {
  // Get entitlements that are eligible for unlimited access extension
  $u = new \RCPAR\User($GLOBALS['user']);
  $ents = rcpar_dashboard_get_eligible_unlimited_ext_ents($u);

  // Get the progress percentages for each entitlement
  foreach ((array)\RCPAR\Course::getProgressPercentageForUser($u, $ents) as $pctdata) {
    $unlimited_activation_skus[$pctdata['sku']] = $pctdata['percentage'];
  }

  // If there is no progress default to 0
  foreach ($ents as $ent) {
    if (!isset($unlimited_activation_skus[$ent->getProductSku()])) {
      $unlimited_activation_skus[$ent->getProductSku()] = 0;
    }
  }

  return $unlimited_activation_skus;
}
