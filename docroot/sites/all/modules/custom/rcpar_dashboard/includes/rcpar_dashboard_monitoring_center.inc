<?php

function rcpar_dashboard_monitoring_center() {
    return theme('rcpar_dashboard_monitoring_center');  
}

function rcpar_monitoring_page($uid = NULL) {
  drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/rcpar_monitoring.js');
  return theme('html_tag', array(
      'element' => array(
          '#tag' => 'h2',
          '#value' => drupal_get_title(),
      ),
  ));
}

function rcpar_dashboard_button_view_details($form, $form_state) {
  $form['view_details'] = array(
      '#type' => 'button',  
      '#disabled' => TRUE,
      '#attributes' => array(
          'style' => 'display:none',
      ),      
      '#ajax' => array(
          'callback' => 'rcpar_dashboard_button_view_details_submit',
          'method' => 'replace',
          'effect' => 'fade',
      ),
  );  
  return $form;
}

function rcpar_dashboard_button_exam_data($form, $form_state, $uid) {

    $form['exam_data_details'] = array(
      '#type' => 'button',
      '#value' => t('exam data'),
      '#attributes' => array(
          'class' => array('btn btn-primary btn-lg exam-data-source-' . $uid),
      ),
      '#ajax' => array(
          'callback' => 'rcpar_dashboard_button_exam_data_display',
          'method' => 'replace',
          'effect' => 'fade',
      ),
  );

  $form['view_details_uid_'] = array(
      '#type' => 'hidden',
      '#value' => $uid
  );

  return $form;
}

function rcpar_dashboard_button_view_details_submit($form, $form_state) {    
  $commands[] = ajax_command_html('#container-of-values', theme('rcpar_dashboard_courses_cpa_modal', array('uid' => $form_state['values']['view_details_uid'])));
  $commands[] = ajax_command_invoke("#execute-modal-after-ajax", 'click');
  return array('#type' => 'ajax', '#commands' => $commands);  
}


function rcpar_dashboard_get_details($uid = NULL) {  
  $form = drupal_get_form('rcpar_dashboard_button_view_details');
  $form_render = render($form);    
  return $form_render;
}

function rcpar_dashboard_get_ipq_details($uid = NULL, $group_id = NULL, $case = "overview") {  
  switch ($case) {
    case 'overview':
      $form = drupal_get_form('ipq_button_overview_details', $uid, $group_id);
      break;
    case 'score':
      $form = drupal_get_form('ipq_button_score_details', $uid , $group_id);
      break;    

  }
  $form_render = render($form);    
  return $form_render;
}

function rcpar_dashboard_exam_data($uid = NULL) {  
  $form = drupal_get_form('rcpar_dashboard_button_exam_data', $uid);
  $form_render = render($form);    
  return $form_render;
}

function rcpar_dashboard_button_exam_data_display($form, $form_state) {   
  $uid = $form_state['values']['view_details_uid_'];
  $user = user_load($uid);
  $name = $user->name;
  $commands[] = ajax_command_html('#container-of-values', theme('rcpar_dashboard_courses_cpa_exam_data', array('uid' => $uid, 'name' => $name)));
  $commands[] = ajax_command_invoke("#execute-modal-after-ajax", 'click');
  return array('#type' => 'ajax', '#commands' => $commands);  
}


function rcpar_dashboard_button_exam_data_form($form, $form_state, $uid){
  
  $exist_reg = rcpar_dashboard_monitoring_data_exists($uid);
  $user_info = rcpar_dashboard_monitoring_user_data($uid);
  
  $form['current_employee'] = array(
      '#type' => 'checkbox',
      '#title' => t('Current Employee'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['current_employee'] : 0,
  );
  $form['date_aud'] = array(
      '#type' => 'textfield',
      '#title' => t('Date scheduled for AUD'),
      '#default_value' => isset($user_info['aud']) ? $user_info['aud'] : "",
      '#disabled' => TRUE,
  );
  $form['date_bec'] = array(
      '#type' => 'textfield',
      '#title' => t('Date scheduled for BEC'),
      '#default_value' => isset($user_info['bec']) ? $user_info['bec'] : "",
      '#disabled' => TRUE,
  );
  $form['date_far'] = array(
      '#type' => 'textfield',
      '#title' => t('Date scheduled for FAR'),
      '#default_value' => isset($user_info['far']) ? $user_info['far'] : "",
      '#disabled' => TRUE,
  );  
  $form['date_reg'] = array(
      '#type' => 'textfield',
      '#title' => t('Date scheduled for REG'),
      '#default_value' => isset($user_info['reg']) ? $user_info['reg'] : "",
      '#disabled' => TRUE,
  );
  $form['office'] = array(
      '#type' => 'textfield',
      '#title' => t('Office'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['office'] : 0,
  );  
  $form['date_aud_passed'] = array(
      '#type' => 'textfield',
      '#title' => t('Date AUD exam passed'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['date_aud_passed'] : "",
  );
  $form['date_bec_passed'] = array(
      '#type' => 'textfield',
      '#title' => t('Date BEC exam passed'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['date_bec_passed'] : "",
  );
  $form['date_far_passed'] = array(
      '#type' => 'textfield',
      '#title' => t('Date FAR exam passed'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['date_far_passed'] : "",
  );  
  $form['date_reg_passed'] = array(
      '#type' => 'textfield',
      '#title' => t('Date REG exam passed'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['date_reg_passed'] : "",
  );
  $form['aud_score'] = array(
      '#type' => 'textfield',
      '#title' => t('AUD score'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['aud_score'] : 0,
  );
  $form['bec_score'] = array(
      '#type' => 'textfield',
      '#title' => t('BEC score'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['bec_score'] : 0,
  );
  $form['far_score'] = array(
      '#type' => 'textfield',
      '#title' => t('FAR score'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['far_score'] : 0,
  );  
  $form['reg_score'] = array(
      '#type' => 'textfield',
      '#title' => t('REG score'),
      '#default_value' => is_array($exist_reg) ? $exist_reg['reg_score'] : 0,
  );  
  $form['uid'] = array(
      '#type' => 'hidden',
      '#value' => $uid
  );  
   $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),            
       '#ajax' => array(
          'callback' => 'rcpar_dashboard_button_exam_data_form_submit',
          'method' => 'replace',
          'effect' => 'fade',
      ),
  );   
  return $form;
}

function rcpar_dashboard_button_exam_data_form_submit ($form, $form_state){ 
  $record = array(
      'uid' => $form_state['values']['uid'],
      'current_employee' => $form_state['values']['current_employee'],
      'date_aud' => $form_state['values']['date_aud'],
      'date_bec' => $form_state['values']['date_bec'],
      'date_far' => $form_state['values']['date_far'],
      'date_reg' => $form_state['values']['date_reg'],  
      'office' => $form_state['values']['office'],
      'date_aud_passed' => $form_state['values']['date_aud_passed'],
      'date_bec_passed' => $form_state['values']['date_bec_passed'],
      'date_far_passed' => $form_state['values']['date_far_passed'],
      'date_reg_passed' => $form_state['values']['date_reg_passed'],
      'aud_score' => $form_state['values']['aud_score'],
      'bec_score' => $form_state['values']['bec_score'],
      'far_score' => $form_state['values']['far_score'],
      'reg_score' => $form_state['values']['reg_score']        
    );
      
  $exist_reg = rcpar_dashboard_monitoring_data_exists($form_state['values']['uid']);
  
  if(is_array($exist_reg)){        
    $result = db_update('monitoring_exam_data')    
            ->fields($record)->execute();    
    $tag = "updated";
  }else{     
    $result = db_insert('monitoring_exam_data')    
            ->fields($record)->execute();
    $tag = "saved";
  }
  
  drupal_set_message("successfully $tag exam data for user: ".$form_state['values']['uid']); 
  $commands = array();
  $commands[] = ajax_command_html("#monitoring-alert", theme('status_messages'));

  print ajax_render($commands);
  exit;
}

function rcpar_dashboard_monitoring_data_exists($uid){    
  return db_select('monitoring_exam_data', 'm')
    ->fields('m')
    ->condition('uid', $uid ,'=')
    ->execute()
    ->fetchAssoc();
}

function rcpar_dashboard_monitoring_user_data($uid) {
  $result = array();
  $user = user_load($uid);
  try {
    $user_wrapper = entity_metadata_wrapper('user', $user);
    $result = array(
        'aud' => $user_wrapper->field_aud_exam_date->value(),
        'bec' => $user_wrapper->field_bec_exam_date->value(),
        'reg' => $user_wrapper->field_reg_exam_date->value(),
        'far' => $user_wrapper->field_far_exam_date->value()
    );
  } catch (EntityMetadataWrapperException $exc) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
  }
  return $result;
}


/**
 * Get the available list of chapters for partner node  
 * @param int $partner_node_id
 * @return array
 */
function rcpar_dashboard_monitoring_get_partner_access_material($partner_node_id) {
  $partner_sections = $result = array();  
  //get exam version based on configured date  
  $exam_versions = exam_versions_years();  
  $default_version = exam_version_get_default_version();
  
  $exam_version = $exam_versions[$default_version];  
  
  $partner_node = node_load($partner_node_id);
  if ($partner_node) {
    $partner_wrapper = entity_metadata_wrapper('node', $partner_node);
    $partner_helper = new RCPARPartner($partner_node);
    $sections = $partner_helper->getCourseSections();

    $partner_restrictions = isset($partner_node->field_flag_restrict[LANGUAGE_NONE]) ?
            $partner_node->field_flag_restrict[LANGUAGE_NONE] : array();

    if (!empty($partner_restrictions)) {
      foreach ($partner_restrictions as $restriction_info) {
        $partner_sections[] = $restriction_info['chapter'];
      }
    }

    $query = db_select('course_stats', 'cs');
    $query->fields('cs');
    $query->condition('type', 'CHAPTER');
    $query->condition('course_type', 'Online Course');
    $query->condition('year', $exam_version);
    if (!empty($partner_sections)) {
      $query->condition('nid', $partner_sections, "IN");
    }
    $course_stats = $query->execute()->fetchAll();

    if ($course_stats) {
      foreach ($course_stats as $course_stats_value) {
        if (in_array($course_stats_value->section, $sections))
          $result[$course_stats_value->section][] = $course_stats_value->weight + 1;
      }

      foreach (array_keys($result) as $sku) {
        sort($result[$sku]);
      }
    }

    ksort($result);

    if (count($result) > 1) {
      $_result = $result;
      $title = array_slice($_result, 0, 1);
      $result = array(
          'fieldset_title' => $title,
          'fieldset_content' => $result,
      );
    }
  }

  return $result;
}

/**
 * Get the array of restrictions of a partner
 * @param int $partner_nid
 * @return array of restrictions
 */
function rcpar_dashboard_monitoring_get_topics_restriction($partner_nid){
  $_topics_restriction = NULL;
  $partner_node = node_load($partner_nid);    
  $partner_restrictions = isset($partner_node->field_flag_restrict[LANGUAGE_NONE]) ?
        $partner_node->field_flag_restrict[LANGUAGE_NONE] : array();    
       
  if (!empty($partner_restrictions)) {
    foreach ($partner_restrictions as $restriction_info) {
      if ($restriction_info['topic']) {
        $_topics_restriction[] = $restriction_info['topic'];
      }
    }
  }
  
  return $_topics_restriction;
}

function rcpar_dashboard_monitoring_tab_ajax($expired, $partner_nid , $js = NULL){
  ctools_include('ajax');
  if ($js) {   
    $groups_data = rcpar_partners_import_get_groups_info_with_user_progress($partner_nid, $expired);
    if (!empty($groups_data)) {
     $partner_groups[$partner_nid] = array(
       'groups' => $groups_data,
      ); 
    }
    $vars = array(
      'partner_groups' => reset($partner_groups),
      'first_partner_nid' => $partner_nid
    );
    $display = theme('rcpar_dashboard_monitoring_tab_content', $vars);
    $commands[] = ajax_command_replace(".tab-content .tab-pane", $display);  
    $response = array('#type' => 'ajax', '#commands' => $commands);
    ajax_deliver($response);      
  }else{
    print "ajax not working";
  }
}

function rcpar_dashboard_monitoring_student_courses(){
  
}


/**
 * 
 * @param type $nids list of all entity ids to load from the field
 * @return list of all shortnames that comes on the entity ids
 */
function rcpar_partners_get_partner_shortnames($nids = array()) {
  $shortnames = array();
  if (count($nids)) {
    $query = db_select("field_data_field_abbreviated_name", "f");
    $query->fields("f", array('field_abbreviated_name_value', 'entity_id'));
    $query->condition('entity_id', $nids);
    $query->condition('bundle', 'partners');
    $query_result = $query->execute()->fetchAll();
    if ($query_result) {
      foreach ($query_result as $value) {
        $shortnames[$value->entity_id] = $value->field_abbreviated_name_value;
      }
    }
  }
  return $shortnames;
}

/**
 * 
 * @param type $partner_nid the id of the node partner
 * @return the user owner of that partner node
 */
function rcpar_partners_get_partner_owner($partner_nid) {  
  $query = db_select("field_data_field_user_account", "f");
  $query->fields("f", array('field_user_account_target_id', 'entity_id'));
  $query->condition('entity_id', $partner_nid);
  $query->condition('bundle', 'partners');
  $query_result = $query->execute()->fetchCol();
  return isset($query_result[0]) ? $query_result[0] : "";
}

/**
 * 
 * @param type $pid partner id
 * @param type $uid user that owns the statistics
 * @param type $section aud bec far or reg
 * @return type
 */
function rpcar_partners_render_statistics($pid, $uid, $section) {  
  return theme('rcpar_dashboard_courses_cpa_modal', array('uid' => $uid,'section' => strtoupper($section), 'pid' => $pid));
}

function rpcar_partners_monitoring_callback($page_callback_result) {
    global $variables;

    // we don't want to add the css and the js when using the modal to display the content
    $no_js = isset($_GET['no-js']) && $_GET['no-js'] == 1 ? true : false;
    $html = '<html><head><title></title>';
    if (!$no_js) {
        $html .= drupal_get_css() . drupal_get_js();
    }
    $html .= '</head><body class="jquery-ajax-load">'.theme_status_messages($variables)  . $page_callback_result . '</body></html>';
    print $html;

    // Perform end-of-request tasks.
    drupal_page_footer();
}
