<?php

function rcpar_dasboard_tour_config($form, &$form_state) {  
  //step 1
  $form['step_1'] = array(
    '#type' => 'fieldset',
    '#title' => t('STEP 1'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['step_1']['rcpar_tour_title_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('rcpar_tour_title_1',''),
    '#size' => 21,
    '#maxlength' => 255,
    '#description' => t('Title to display on tour step 1'),
    '#required' => TRUE,
  );
  $form['step_1']['rcpar_tour_content_1'] = array(
    '#type' => 'textarea',
    '#title' => t('Content'),
    '#default_value' => variable_get('rcpar_tour_content_1',''),
    '#description' => t('Content to display on tour step 1'),
    '#required' => TRUE,
  );

  //step 2
  $form['step_2'] = array(
    '#type' => 'fieldset',
    '#title' => t('STEP 2'),    
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['step_2']['rcpar_tour_title_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('rcpar_tour_title_2',''),
    '#size' => 21,
    '#maxlength' => 255,
    '#description' => t('Title to display on tour step 2'),
    '#required' => TRUE,
  );
  $form['step_2']['rcpar_tour_content_2'] = array(
    '#type' => 'textarea',
    '#title' => t('Content'),
    '#default_value' => variable_get('rcpar_tour_content_2',''),
    '#description' => t('Content to display on tour step 2'),
    '#required' => TRUE,
  );

  //step 3
  $form['step_3'] = array(
    '#type' => 'fieldset',
    '#title' => t('STEP 3'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['step_3']['rcpar_tour_title_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('rcpar_tour_title_3',''),
    '#size' => 21,
    '#maxlength' => 255,
    '#description' => t('Title to display on tour step 3'),
    '#required' => TRUE,
  );
  $form['step_3']['rcpar_tour_content_3'] = array(
    '#type' => 'textarea',
    '#title' => t('Content'),
    '#default_value' => variable_get('rcpar_tour_content_3',''),
    '#description' => t('Content to display on tour step 3'),
    '#required' => TRUE,
  );

  //step 4
  $form['step_4'] = array(
    '#type' => 'fieldset',
    '#title' => t('STEP 4'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['step_4']['rcpar_tour_title_4'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('rcpar_tour_title_4',''),
    '#size' => 21,
    '#maxlength' => 255,
    '#description' => t('Title to display on tour step 4'),
    '#required' => TRUE,
  );
  $form['step_4']['rcpar_tour_content_4'] = array(
    '#type' => 'textarea',
    '#title' => t('Content'),
    '#default_value' => variable_get('rcpar_tour_content_4',''),
    '#description' => t('Content to display on tour step 4'),
    '#required' => TRUE,
  );

  //step 4
  $form['step_5'] = array(
    '#type' => 'fieldset',
    '#title' => t('STEP 5'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['step_5']['rcpar_tour_title_5'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('rcpar_tour_title_5',''),
    '#size' => 21,
    '#maxlength' => 255,
    '#description' => t('Title to display on tour step 5'),
    '#required' => TRUE,
  );
  $form['step_5']['rcpar_tour_content_5'] = array(
    '#type' => 'textarea',
    '#title' => t('Content'),
    '#default_value' => variable_get('rcpar_tour_content_5',''),
    '#description' => t('Content to display on tour step 5'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
