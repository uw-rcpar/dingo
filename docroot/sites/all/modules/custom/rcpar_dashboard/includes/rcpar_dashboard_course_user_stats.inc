<?php
 
function rcpar_dashboard_video_duration_in_milliseconds($video_duration) {
	sscanf($video_duration, "%d:%d", $minutes, $seconds);
	$time_seconds = ($minutes * 60 + $seconds) * 1000;
	return $time_seconds;
}

function rcpar_partners_get_section_ids() {
  $sections = array();
  $query = db_select("course_stats", "s");
  $query->fields("s", array('nid', 'section'));
  $query->condition('type', 'SECTION');  
  $query_result = $query->execute()->fetchAll();
  if ($query_result) {
    foreach ($query_result as $value) {
      $sections[$value->section] = $value->nid;
    }
  }

  return $sections;
}
