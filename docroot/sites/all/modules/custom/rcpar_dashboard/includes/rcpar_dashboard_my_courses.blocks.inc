<?php

/*
 * Multiple Graphic for CPA exams
 */

function rcpar_dashboard_courses_multiple_graphic() {
    global $user;
    $start_date = "";
    $dates = $vars= array();

    libraries_load('d3');
    drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/radial_elements.js');
    drupal_add_css(drupal_get_path('module', 'rcpar_dashboard') . '/css/radial_elements.css');

    $user_exam_dates_info = rcpar_dashboard_get_user_exam_dates();
    if (!empty($user_exam_dates_info)) {
    foreach ($user_exam_dates_info as $exams_dates) {
      $dates[$exams_dates['formatted']] = strtotime($exams_dates['raw']);
    }
    $start_date = array_search(min($dates), $dates);

    $exams = rcpar_dashboard_get_cpa_exams();
    arsort($exams);

    $vars = array(
        "exams" => $exams,
        "average" => number_format(rcpar_dashboard_get_cpa_exams(1), 2),
        "start_date" => $start_date,
    );
  }

  return theme('rcpar_dashboard_courses_muliple_graphic', $vars);
}

/*
 * CPA exams Single info blocks
 */

function rcpar_dashboard_courses_single_description() {
    global $user;
  $exams_info = array();

  libraries_load('d3');
  drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/radial_elements.js');
  drupal_add_css(drupal_get_path('module', 'rcpar_dashboard') . '/css/radial_elements.css');

  $nodes = user_entitlements_get_entitlements($user);
  $exams_object = rcpar_dashboard_get_entitlements_ids($nodes);
  $exams = rcpar_dashboard_get_cpa_exams();

  if (!empty($exams)) {
    foreach ($exams as $sku => $value) {
      $expire_date = "";
      $nid = $tid = 0;
      if (isset($exams_object[$sku])) {
        $exams_info[$sku] = rcpar_dashboard_get_courses_info($exams_object[$sku]);
      }
    }
  }
  $vars = array(
      "uid" => $user->uid,
      "exams" => $exams,
      "exams_info" => $exams_info,
  );
  return theme('rcpar_dashboard_courses_single_desc', $vars);
}

/**
 * Get extra information about a course 
 * @param int $nid
 */
function rcpar_dashboard_get_courses_info($nid, $cram_type_tid = 0) {
    $tid = 0;
    $node = node_load($nid);
    $exams_info = array();
    try {
      $node_wrapper = entity_metadata_wrapper('node', $node);
      $expire_date = $node_wrapper->field_expiration_date->value();
      $unlimited_access = $node_wrapper->field_unlimited_access->value();
      if (!empty($node->field_course_section)) {
        $tid = $node->field_course_section[LANGUAGE_NONE][0]['tid'];               
        $course = rcpar_dashboard_get_course_by_term($tid , $cram_type_tid);
        if (!empty($course['node'])) {
          $nid = key($course['node']);
        }
      }
      $exams_info['nid'] = $nid;
      $exams_info['tid'] = $tid;
      // Change expire date message for entitlements with Unlimited Access
      if ($unlimited_access == 1){
        $exams_info['expire_date'] = 'Unlimited Access';
      }
      else {
        $exams_info['expire_date'] = 'Expires ' . date("m/j/Y", $expire_date);
      }

    } catch (EntityMetadataWrapperException $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
    return $exams_info;
  }

  /*
 * Form Customer Review for "My Courses" page
 */

function rcpar_dashboard_customer_review_form() {
  module_load_include('inc', 'node', 'node.pages');
  global $user;
  $user_wrapper = entity_metadata_wrapper('user', $user);
  try {
    if(user_entitlements_user_has_paid_entitlements($user)){
      $name = isset($user_wrapper->field_first_name) &&  $user_wrapper->field_first_name->value() != "" ? $user_wrapper->field_first_name->value() . " " . $user_wrapper->field_last_name->value() : $user->name;
      $node_form = new stdClass;
      $node_form->type = 'testimonials';    
      $node_form->uid = $user->uid;
      $node_form->status = 1;
      $node_form->language = LANGUAGE_NONE;
      $node_form->title = $name;
      $user_college =  RCPAR\User::getInstance($user->uid)->getCollege()->getName();
      $user_employer = $user_wrapper->field_firm->value();
      if (isset($user_college)) {
        $node_form->field_school_name['und'][0]['value'] = $user_college;
      }
      if (isset($user_employer)) {
        $node_form->field_employer['und'][0]['value'] = $user_employer;
      }
      $form = drupal_get_form('testimonials_node_form', $node_form, 'dashboard_node');
      return $form;
    }
  } catch (EntityMetadataWrapperException $exc) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
  }
}

/*
 * Submit function for Customer Review 
 */

function rcpar_dashboard_customer_review_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'dashboard';
}

/**
 * Displays a progress bar for Video History
 * @param  $topic_id
 */
function rcpar_dashboard_get_progress_bar($vh_id = 0) {
  $percent = 0;
  if($vh_id){
    $percent = rcpar_dashboard_get_percentage_by_last_position($vh_id);
    $last_seen = rcpar_dashboard_last_seen_by_last_position($vh_id);
  }    
      
  $vars = array(
        "percent" => $percent,
    ); 
  
  $progress['bar'] = theme('rcpar_dashboard_progress_bar', $vars);
  $progress['last_seen'] = isset($last_seen) ? $last_seen : 0;
  return $progress;
}

/**
 * Returns a list of history videos related with a topic 
 * @param int $topic_id
 * @return type
 */
function rcpar_dashboard_get_counter_video_history_per_topic($topic_id, $uid = 0) {        
  if (!$uid) {
    global $user;
  }else{
    $user = user_load($uid);
  }
  $array_result = array();

  $topic = node_load($topic_id);
  $topic_wrapper = entity_metadata_wrapper('node', $topic);
  $section = $topic_wrapper->field_course_section->value();
  $section_id = $section->tid;

  $nodes = user_entitlements_get_entitlements($user);
  
  if (!empty($nodes)) {
    $nodes_ids = array_keys($nodes);
    foreach ($nodes_ids as $key => $nid) {
      if (is_int($nid)) {
        $entitlement = node_load($nid);
        $entitlement_wrapper = entity_metadata_wrapper('node', $entitlement);
        if ($entitlement_wrapper->field_course_section->value()) {
          $entitlement_section = $entitlement_wrapper->field_course_section->value();
          $entitlement_section_id = $entitlement_section->tid;
          if ($entitlement_section_id == $section_id) {
            $query = new EntityFieldQuery();
            $query->entityCondition('entity_type', 'video_history');
            $query->entityCondition('bundle', 'video_history');
            $query->propertyCondition('uid', $user->uid);
            $query->propertyOrderBy("created", "DESC");
            $query->fieldCondition('field_topic_reference', 'target_id', $topic_id);
            $query->fieldCondition('field_entitlement_product', 'target_id', $nid);
            $query->addMetaData('account', user_load(1));
            $array_result = $query->execute();            
            if(!empty($array_result)){
              return $array_result;
            }
            
          }
        }
      }
    }
  }
  return $array_result;
}

/**
 * Gets the course node associated with a course section and type.
 *
 * @param int $term
 * - Term ID from the course_sections vocab
 * @param int $type
 * - Term ID from the course_type vocab
 * @return array
 * - An array of arrays keyed by node id (direct return from EntityFieldQuery::execute())
 */
function rcpar_dashboard_get_course_by_term($term , $type = 1464) {
    $type = $type != 0 ? $type : 1464;
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', 'rcpa_course');
    $query->propertyCondition('status', 1);
    $query->fieldCondition('field_course_section', 'tid', $term);
    $query->fieldCondition('field_course_type_ref', 'tid', $type);
    $query->addMetaData('account', user_load(1)); // Run the query as user 1.
    $array_result = $query->execute();
    return $array_result;
}

/**
 * Returns the container chapter for an specific topic
 * @param int $topic_id
 * @return type
 */
function rcpar_dashboard_get_chapter_by_topic($topic_id) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', 'rcpa_chapter');
    $query->propertyCondition('status', 1);
    $query->fieldCondition('field_topic_reference', 'target_id', $topic_id);
    $query->addMetaData('account', user_load(1)); // Run the query as user 1.
    $array_result = $query->execute();
    return $array_result;
}

/**
 * Get chapters counter for a matched course
 * @param int $type
 * @param int $section
 * @return int
 */
function rcpar_dashboard_get_chapters_counter_by_section($type, $section){
  $result = 1;
  $course = rcpar_dashboard_get_course_by_term($section ,$type);
  if($course['node']){
    $course_nid = key($course['node']);
    $course = node_load($course_nid);
    try {
      //Get exam version
      global $user;
      $section_name = taxonomy_term_load($section);
      $exam_version = user_entitlements_get_exam_version_for_course($section_name->name, $user);

      //Get chapter list
      $exam_version = taxonomy_get_term_by_name($exam_version, 'exam_version');
      $exam_version_tid = key($exam_version);
      $course_wrapper = entity_metadata_wrapper('node', $course);
      $chapter_list = _courseware_get_chapters($course_wrapper, $exam_version_tid);
      $result = count($chapter_list);
    } catch (EntityMetadataWrapperException $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));      
    }
  }
  return $result;
}


/**
 * Displays the counter of video views in courses topics
 */
function rcpar_dashboard_get_counter_topics_per_chapter($chapter_id, $ajax_case = NULL, $uid = NULL, $version, $topics_chapter,$partner_id=NULL) {
  if (!$uid) {
    global $user;
    $uid = $user->uid;
  } else {
    $user = user_load($uid);
  }

  $nodes = user_entitlements_get_entitlements($user,$partner_id);
  $nodes_ids = is_array($nodes) ? array_keys($nodes) : array();
  
  if(!count($nodes_ids)){
    return 0;
  }
  // transform version text to term id
  $version_obj = taxonomy_get_term_by_name($version, 'exam_version');
  $version_obj = reset($version_obj);
  $version_tid = $version_obj->tid;
  
  $entitlements_ids = implode(",", array_map('intval', $nodes_ids));
  $topics_chapter_list = implode (",", $topics_chapter);

  if ($chapter_info = db_query("SELECT count(DISTINCT(us.topic_reference)) as chapter_counter, cs.total_count FROM eck_video_history us
        INNER JOIN  course_stats cs on cs.nid = us.chapter_reference
        and cs.year=:year
        and us.uid=:uid
        and us.chapter_reference=:chapter_id and (us.entitlement_product IN ({$entitlements_ids})) AND (us.topic_reference IN ({$topics_chapter_list}))
        ", array(':uid' => $uid, ":chapter_id" => $chapter_id, ':year' =>$version_tid))->fetchObject()) {

    if ($ajax_case) {
      if (is_null($chapter_info->total_count)) {
        $total_count = db_select('course_stats', 'cs')->fields('cs', array('total_count'))->condition("nid", $chapter_id)->condition("type", "CHAPTER")->condition("year", $version_tid)->execute()->fetchField();
      } else {
        $total_count = $chapter_info->total_count;
      }
      return "{$chapter_info->chapter_counter} / {$total_count} viewed";
    }

    return $chapter_info->chapter_counter;
  }

  return 0;
}

/**
 * Displays viewed videos per chapter
 * @param array $chapter_collection
 * @return json $result
 */
function rcpar_dashboard_get_counter_topics_per_chapter_ajax($chapter_collection, $uid = 0, $data_version,$partner_id=NULL){
  $result = array();
  
  if (!empty($chapter_collection) && !empty($data_version)) {
    if(!$uid){
      global $user;
      $uid = $user->uid;
    }
    // get version data
    $data = explode('-', $data_version);
    $section = $data[0];
    $version = $data[1];
    $version_obj = taxonomy_get_term_by_name($version, 'exam_version');
    $version_obj = reset($version_obj);
    $version_tid = $version_obj->tid;

    // Cram sections are labeled "[section]-CRAM" in the course_stats table
    // In order to get the proper section name we need to query the course_stats table
    // Chapter ids are unique to cram and regular course types
    $first_chapter = reset(explode(',', $chapter_collection));
    $first_chapter = explode('-', $first_chapter);
    $first_chapter_nid = $first_chapter[1];
    $section = db_select('course_stats', 'c')
      ->fields('c',array('section'))
      ->condition('year',$version_tid, '=')
      ->condition('nid',$first_chapter_nid, '=')
      ->execute()
      ->fetchField();

    // Get the topics
    $topics_chapter = db_query("SELECT nid FROM course_stats WHERE year=:year AND section=:section AND type=:type", array(':year' => $version_tid, ':section' => $section, ':type' => "TOPIC"))->fetchAll(PDO::FETCH_COLUMN, 0);

    $chapter_collection = explode(',', $chapter_collection);
    foreach ($chapter_collection as $item) {
      $nid = explode('-', $item);
      $nid = $nid[1];
      // get topics in chapter
      $chaper = node_load($nid);
      $chaper_wrapper = entity_metadata_wrapper('node', $chaper);
      $tag = theme('html_tag', array(
        'element' => array(
          '#tag' => 'p',
          '#attributes' => array(
            'class' => 'videos-viewed'),
          '#value' => rcpar_dashboard_get_counter_topics_per_chapter($nid, TRUE, $uid, $version, $topics_chapter, $partner_id),
        ),
      ));
      $result[$item] = $tag;
    }    
  }
  
  return drupal_json_output($result);
}

/**
 * Get statistic block in Course node page
 */
function rcpar_dashboard_courses_progress_match_case() {    
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
    if($node->type == "rcpa_course"){
                
        $node_wrapper = entity_metadata_wrapper('node', $node);
        $course_type = $node_wrapper->field_course_section->value();
        if($course_type){
            $course_type = $course_type->name;
        }
        
        $exams = rcpar_dashboard_get_cpa_exams();
        $exams = array($course_type."_c" => $exams[$course_type]);
                
        return rcpar_dashboard_statistics_content($exams);
    }
    
  }
}

/*
 * Display Audio courses  
 */

function rcpar_dashboard_audio_courses_display() {
  global $user;
  $result = &drupal_static(__FUNCTION__);
  if (!isset($result)) {
    if ($cache = cache_get("u{$user->uid}_rcpar_dashboard_audio_exams")) {
      $result = $cache->data;
    }
    elseif(isset($user->user_entitlements['products'])) {
      $skus = array();
      foreach($user->user_entitlements['products'] AS $prods) {  
        if (user_entitlements_is_audio_course($prods['product_id'])) {
          $skus[] = $prods;
        }
      }  
      if (isset($skus)) {
        foreach ($skus as $p_aud) {
          try {
            if (user_entitlements_is_product_expired($p_aud['expiration_date'])) {
              continue;
            }
            $product_wrapper = entity_metadata_wrapper('commerce_product', $p_aud['product_id']);

            $img = $product_wrapper->field_product_image->value();
            $desc = $product_wrapper->field_description->value();
            $section = $product_wrapper->field_course_section->value();
            $file = base64_encode($product_wrapper->field_audio_file_name_text->value());
            $file = !empty($file) ? 'audio-download/download/file/' . $file : "";

            $result[$p_aud['product_id']] = array(
              "img" => image_style_url('audio_thumbnail', $img[0]['uri']),
              "desc" => $desc,
              "section" => $section->name,
              "file" => $file,
              "expiration_date" => $p_aud['expiration_date']
            );
            cache_set("u{$user->uid}_rcpar_dashboard_audio_exams", $result, 'cache');
          } catch (EntityMetadataWrapperException $exc) {
            watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
          }
        }
      }
    }
  }
  $vars = array("courses" => $result);
  return theme('rcpar_dashboard_audio_courses', $vars);
}

/**
 * Collects info about courses 
 * @param Array $all_exams
 * @param Array $exams
 * @param Int $cram_tid
 * @param Int $nid
 * @param Object $js
 * @return Array
 */
function rcpar_dashboard_courses_fill_tabs($all_exams, $exams, $cram_tid = 0, $nid = 0, $js = NULL, $uid = NULL , $monitoring_case = 0) {
  if (!is_null($uid)) {
    $user = user_load($uid);
    if (!$user) {
      return 0;
    }
  }
  else {
    global $user;
  }

  $active_ones = $no_active_ones = $exams_info = $commands = $delayed_list = array();
  libraries_load('d3');
  drupal_add_js(drupal_get_path('module', 'rcpar_dashboard') . '/js/radial_elements.js');
  drupal_add_css(drupal_get_path('module', 'rcpar_dashboard') . '/css/radial_elements.css');
  ctools_include('ajax');

  $daemon = $nid ? 100 : 0;
  $nodes = user_entitlements_get_entitlements($user);

  $exams_object = rcpar_dashboard_get_entitlements_ids($nodes);

  if (isset($user->user_entitlements['delayed_products']) && !empty($user->user_entitlements['delayed_products'])) {
    $delayed_skus = $user->user_entitlements['delayed_products'];
  }
  else {
    $delayed_ones = user_entitlements_get_products_delayed($user);
    $delayed_skus = user_entitlements_get_entitlements_skus_and_id($delayed_ones);
  }

  foreach ($all_exams as $exam) {
    $daemon_monitoring = 0;
    $mc_access = rcpar_dashboard_get_individual_percentage($user->uid, $exam);
    if ($monitoring_case && $mc_access == "-.--") {
      $daemon_monitoring = 1;
    }

    if (in_array($exam, array_keys($exams)) && !$daemon_monitoring) {
      if (array_key_exists($exam, $delayed_skus)) {
        $delayed_list[$exam] = $delayed_skus[$exam];
      }
      elseif (isset($exams_object[$exam])) {
        $nid_reference = rcpar_dashboard_get_courses_info($exams_object[$exam], $cram_tid);
        if ($nid && $nid == $nid_reference['nid']) {
          $daemon = 0;
        }

        // Read exam version from the entitlement, fallback on the default version if not available.
        $exam_version = exam_version_get_default_version();
        if (isset($user->user_entitlements['products'][$exam]) && !empty($user->user_entitlements['products'][$exam]['exam_version']->tid)) {
          $exam_version = (int) $user->user_entitlements['products'][$exam]['exam_version']->name;
        }

        $active_ones[$exam] = array(
            "value" => $exams[$exam],
            "class" => $daemon == 0 ? "active" : "item",
            "exam_version" => $exam_version
          ) + rcpar_dashboard_get_courses_info($exams_object[$exam], $cram_tid);

        $daemon++;
      }
    }
    else {
      $no_active_ones[$exam] = $exam;
    }
  }

  $vars = array(
    "active_ones" => $active_ones,
    "no_active_ones" => $no_active_ones,
    "delayed_ones" => $delayed_list,
  );

  if ($monitoring_case) {
    $vars['student'] = $user->name;
    $vars['student_mail'] = $user->mail;
  }

  return $vars;
}

/**
 * Display tab navigation for Courseware
 * @param Int $nid
 * @param Object $js
 */
function rcpar_dashboard_coursesware_tabs($nid = 0, $js = NULL, $uid = NULL, $monitoring_case = FALSE) {
  $cpa_ent = rcpar_dashboard_entitlements_options();
  $cpa_exams = rcpar_dashboard_get_cpa_exams(0, $uid);
  $cram_ent = rcpar_dashboard_entitlements_crams_options();
  $cram_exams = rcpar_dashboard_get_cram_exams();
  $cram_tabs = '';
  
  if (!empty($cram_exams)) {
    $cram_vars = rcpar_dashboard_courses_fill_tabs($cram_ent, $cram_exams, 1466, $nid, $js);
  }
  if (!empty($cpa_exams)) {
    $cpa_vars = rcpar_dashboard_courses_fill_tabs($cpa_ent, $cpa_exams, 1464, $nid, $js, $uid , $monitoring_case);
  }

  if (!empty($cpa_exams) && !empty($cram_exams)) {
    $vars = array_merge_recursive($cpa_vars, $cram_vars);
    // Unset duplicate active tab
    if(isset($vars['active_ones'])){
      $daemon = 0 ;
      foreach ($vars['active_ones'] as $active_key => $active_ent) {
        if($active_ent['class'] == 'active' && $daemon){
          $vars['active_ones'][$active_key]['class'] = "item";
        }else{
          $daemon = 1 ;
        }
      }
    }
//$vars = $cpa_vars;
    if ($js && $nid) {
      $commands[] = ajax_command_html('#my-courses-tabs', theme('rcpar_dashboard_coursesware_tabs', $vars));
      print ajax_render($commands);
      exit;
    }
    if($monitoring_case){
      $vars["monitoring_case"] = 1;
    }    

    return theme('rcpar_dashboard_coursesware_tabs',  $vars);
  }
}

function rcpar_dashboard_set_dashboard_breadcrumbs($breadcrumbs=array()) {
  // Build Breadcrumbs
  $breadcrumb = drupal_get_breadcrumb();
  foreach($breadcrumbs AS $k=>$v) {
    $breadcrumb[] = l($k, $v);    
  }
  //$breadcrumb[] = l('Home', '/');
  //$breadcrumb[] = l('My Dashboard', 'dashboard');
  //$breadcrumb[] = l(drupal_get_title(), current_path());  
  // Set Breadcrumbs
  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Dsplay tabs for CPA courses info
 * @param Int $nid
 * @param Object $js
 */
function rcpar_dashboard_courses_cpa_tabs($nid = 0, $js = NULL, $uid = NULL, $monitoring_case = FALSE) {
  drupal_add_library('system', 'drupal.ajax');
  $all_exams = rcpar_dashboard_entitlements_options();
  $cpa_exams = rcpar_dashboard_get_cpa_exams(0, $uid);

  rcpar_dashboard_set_dashboard_breadcrumbs(array('Home'=>'<front>','My Dashboard'=>'dashboard'));

  if (!empty($cpa_exams)) {
    $vars = rcpar_dashboard_courses_fill_tabs($all_exams, $cpa_exams, 1464, $nid, $js, $uid , $monitoring_case);

    // We also need the exam version per course section
    foreach($vars['active_ones'] as $key => $ent_info){
      $vars['active_ones'][$key]['exam_version'] = user_entitlements_get_exam_version_for_course($key);
    }

    // This page will show the AUD course outline by default
    // so we need to check if the user already settled up the exam version for AUD
    // and if he didn't, redirect it to a section that he already set
    // or to the dashboard to require that the exam version for AUD to be set
    // if exam versioning is toggled on
    if (exam_version_is_versioning_on()) {
      if (!isset($vars['active_ones']['AUD']) || !user_entitlements_get_exam_version_for_course('AUD')) {
        foreach ($vars['active_ones'] as $sku => $value) {
          $exam_version = user_entitlements_get_exam_version_for_course($sku);
          if ($exam_version) {
            $exam_version = user_entitlements_get_exam_version_for_course($sku);
            if ($exam_version) {
              drupal_goto("dashboard/my-courses/" . $sku);
            }
          }
        }
        // No exam version set yet
        // we find a active entitlement and require user to set its exam version
        reset($vars['active_ones']);
        $first_sku = key($vars['active_ones']);
        rcpar_dashboard_force_ev_before_continue($first_sku);
      }
    }

    if ($js && $nid) {
      $commands[] = ajax_command_html('#my-courses-tabs', theme('rcpar_dashboard_courses_cpa_tabs', $vars));
      print ajax_render($commands);
      exit;
    }
    if($monitoring_case){
      return theme('rcpar_dashboard_monitoring_cpa_tabs', $vars); 
    }else{
      return theme('rcpar_dashboard_courses_cpa_tabs', $vars);
    }
  }else{
    drupal_set_message("You have no content to see here !");
    return "";
  }
}

/**
 * Dsplay tabs for CPA courses info with parmas
 * @param Int $nid
 * @param Object $js
 */
function rcpar_dashboard_courses_cpa_tabs_args($sku , $section = 0 , $sub_section = 0) {
  drupal_add_library('system', 'drupal.ajax');
  $all_exams = rcpar_dashboard_entitlements_options();
  $cpa_exams = rcpar_dashboard_get_cpa_exams();

  rcpar_dashboard_set_dashboard_breadcrumbs(array('My Dashboard'=>'dashboard'));
  if (!empty($cpa_exams)) {
    $collection = rcpar_dashboard_courses_fill_tabs($all_exams, $cpa_exams, 1464);
    $vars = rcpar_dashboard_get_courses_active_item ($collection, $sku);
    $vars['exp_unlimited'] = array();
    // Check for expired unlimited access entitlements
    $unlimited_activation_skus = rcpar_dashboard_unlimited_activation_skus();
    if (!empty($unlimited_activation_skus)) {
      foreach ($unlimited_activation_skus as $sku => $percentage) {
        $vars['exp_unlimited'][$sku]['value'] = $percentage;
      }
    }

    // We also need the exam version per course section
    // if exam versioning is toggled on
    if (exam_version_is_versioning_on()) {
      foreach ($vars['active_ones'] as $key => $ent_info) {
        $vars['active_ones'][$key]['exam_version'] = user_entitlements_get_exam_version_for_course($key);
      }
      $exam_version = user_entitlements_get_exam_version_for_course($sku);
      if (!$exam_version) {
        if (!$sku) {
          // On this case user is not trying to access to a specific section
          // so if we don't have the exam version for the default, we might want to redirect it to
          // a section for which he does have the exam version set
          foreach (array('AUD', 'BEC', 'REG', 'FAR') as $new_sec) {
            if (user_entitlements_get_exam_version_for_course($new_sec)) {
              drupal_goto("dashboard/my-courses/" . $new_sec);
            }
          }
          // If the user has not set any of their entitlements, we just redirect it to dashboard on the
          // rcpar_dashboard_force_ev_before_continue
        }
        rcpar_dashboard_force_ev_before_continue($sku);
      }
    }
    else {

    }
    if($section && $sub_section){
      $file = drupal_get_path('module', 'rcpar_dashboard') . '/js/rcpar_courseware_section.js';
      $options = array(
          'section' => $section,
          'sku' => $sku,
          'sub_section' => $sub_section);
      drupal_add_js(array('rcpar_courseware' => $options), array('type' => 'setting'));
      drupal_add_js($file);
    }
    return theme('rcpar_dashboard_courses_cpa_tabs', $vars);
  }else{
    drupal_set_message("You have no content to see here !");
    return "";
  }
}

/**
 * Dsplay tabs for CRAM courses info
 * @param Int $nid
 * @param Object $js
 */
function rcpar_dashboard_courses_cram_tabs($nid = 0, $js = NULL) {
  drupal_add_library('system', 'drupal.ajax');
  $all_exams = rcpar_dashboard_entitlements_crams_options();
  $cram_exams = rcpar_dashboard_get_cram_exams();

  rcpar_dashboard_set_dashboard_breadcrumbs(array('Home'=>'<front>','My Dashboard'=>'dashboard'));

  if (!empty($cram_exams)) {
    $vars = rcpar_dashboard_courses_fill_tabs($all_exams, $cram_exams, 1466, $nid, $js);
    if ($js && $nid) {
      $commands[] = ajax_command_html('#my-cram-courses-tabs', theme('rcpar_dashboard_courses_cram_tabs', $vars));
      print ajax_render($commands);
      exit;
    }
    return theme('rcpar_dashboard_courses_cram_tabs', $vars);
  }else{
    drupal_set_message("You have no content to see here !");
    return "";
  }
}

/**
 * Dsplay tabs for CRAM courses info with params
 * @param Int $nid
 * @param Object $js
 */
function rcpar_dashboard_courses_cram_tabs_args($sku , $section = 0 , $sub_section = 0) {
  drupal_add_library('system', 'drupal.ajax');
  $all_exams = rcpar_dashboard_entitlements_crams_options();
  $cram_exams = rcpar_dashboard_get_cram_exams();

  rcpar_dashboard_set_dashboard_breadcrumbs(array('Home'=>'<front>','My Dashboard'=>'dashboard'));

  if (!empty($cram_exams)) {       
    $collection = rcpar_dashboard_courses_fill_tabs($all_exams, $cram_exams, 1466);
    $vars = rcpar_dashboard_get_courses_active_item ($collection, $sku);  
    
    if($section && $sub_section){     
      $file = drupal_get_path('module', 'rcpar_dashboard') . '/js/rcpar_courseware_section.js';
      $options = array(
          'section' => $section,
          'sku' => $sku,
          'sub_section' => $sub_section);
      drupal_add_js(array('rcpar_courseware' => $options), array('type' => 'setting'));
      drupal_add_js($file);
    }
    return theme('rcpar_dashboard_courses_cram_tabs', $vars);
  }else{
    drupal_set_message("You have no content to see here !");
    return "";
  }
}


/**
 * Get a collection of courses with selected active item
 * @param array $collection
 * @param String $sku
 * @return array Collection of courses formatted as expected
 */
function rcpar_dashboard_get_courses_active_item($collection = array(), $sku){
  if(isset($collection['active_ones'])){    
    foreach ($collection['active_ones'] as $active_key => $active_ent) {
      $active_key_daemon = str_replace("-CRAM", "", $active_key);
      if($active_key_daemon == $sku){
        $collection['active_ones'][$active_key]['class'] = "active";
      }else{
        $collection['active_ones'][$active_key]['class'] = "item";
      }        
    }
  }  
  return $collection;
}

function rcpar_dashboard_mc_get_my_courses_view($nid, $exam_version){
  // if exam versioning is toggled on
  if (!exam_version_is_versioning_on()) {
    $exam_version = exam_version_get_default_version();
  }
  $result = views_embed_view("course_elements", "chapter_topics_block", $nid, $exam_version, $exam_version);
  return $result;
}

/**
 * 
 * @param type $nid course id to load from
 * @param type $get_raw_data if true return  raw data
 * @param type $exam_version year of version we should load
 * @return rendered version of tree chapter -> topics
 */
function rcpar_dashboard_get_my_courses_view($nid, $get_raw_data = FALSE, $exam_version) {

  // if exam versioning is toggled on
  if (!exam_version_is_versioning_on()) {
    $exam_version = exam_version_get_default_version();
  }
  $cache_file_path = variable_get('file_public_path', conf_path() . '/files') . "/rcpar_dashboard_view_cache_{$nid}_{$exam_version}.html";
  if (file_exists($cache_file_path) && !$get_raw_data) {
    $result = file_get_contents($cache_file_path, true);
  } else {
    if ($get_raw_data) {
      if ($cache = cache_get("rcpar_dashboard_view_cache_{$nid}_{$exam_version}_{$get_raw_data}")) {
        $result = $cache->data;
      } else {
        $result = views_get_view_result("course_elements", "chapter_topics_block", $nid, $exam_version, $exam_version);
        cache_set("rcpar_dashboard_view_cache_{$nid}_{$exam_version}", $result, 'cache');
      }
    } else {
      if ($cache = cache_get("rcpar_dashboard_view_cache_{$nid}_{$exam_version}")) {
        $result = $cache->data;
      } else {
        $result = views_embed_view("course_elements", "chapter_topics_block", $nid, $exam_version, $exam_version);
        cache_set("rcpar_dashboard_view_cache_{$nid}_{$exam_version}", $result, 'cache');
      }
    }
  }
  return $result;
}

function rcpar_dashboard_get_my_courses_notes_view($nid,$exam_version = 2016){  
  $result = views_embed_view("course_elements", "course_notes_block", $nid,$exam_version, $exam_version);
  return $result;
}

/**
 * Display notes tabs for CPA courses info
 * @param Int $nid
 * @param Object $js
 */
function rcpar_dashboard_notes_cpa_tabs($nid = 0, $js = NULL, $uid = NULL) {
  $all_exams = rcpar_dashboard_entitlements_options();
  $cpa_exams = rcpar_dashboard_get_cpa_exams(0, $uid);

  rcpar_dashboard_set_dashboard_breadcrumbs(array('Home'=>'<front>','My Dashboard'=>'dashboard', 'Course Outline'=>'dashboard/my-courses'));
  
  if (!empty($cpa_exams)) {
    $vars = rcpar_dashboard_courses_fill_tabs($all_exams, $cpa_exams, 1464, $nid, $js, $uid);
    $vars['notes_option']= 1;
    $vars['delayed_ones']= array();    
    if ($js && $nid) {
      $commands[] = ajax_command_html('#my-courses-tabs', theme('rcpar_dashboard_courses_cpa_tabs', $vars));
      print ajax_render($commands);
      exit;
    }
    return theme('rcpar_dashboard_courses_cpa_tabs', $vars);
  }
}

/**
 * Display notes tabs for CRAM courses info
 * @param Int $nid
 * @param Object $js
 */
function rcpar_dashboard_notes_cram_tabs($nid = 0, $js = NULL, $uid = NULL) {
  $all_exams = rcpar_dashboard_entitlements_crams_options();
  $cram_exams = rcpar_dashboard_get_cram_exams();

  rcpar_dashboard_set_dashboard_breadcrumbs(array('Home'=>'<front>','My Dashboard'=>'dashboard', 'CRAM Course Outline'=>'dashboard/my-cram-courses'));
  
  if (!empty($cram_exams)) {
    $vars = rcpar_dashboard_courses_fill_tabs($all_exams, $cram_exams, 1466, $nid, $js);
    $vars['notes_option']= 1;
    $vars['delayed_ones']= array(); 
    if ($js && $nid) {
      $commands[] = ajax_command_html('#my-cram-courses-tabs', theme('rcpar_dashboard_courses_cram_tabs', $vars));
      print ajax_render($commands);
      exit;
    }
    return theme('rcpar_dashboard_courses_cram_tabs', $vars);
  }
}

/**
 * Get topic notes related with a topic id
 * @param int $topic_id
 * @return array
 */
function rcpar_dashboard_get_notes_by_topic_id($topic_id, $user = NULL) {
  if (!isset($user)) {
    global $user;
  }

  if (isset($user->user_entitlements['products']) && !empty($user->user_entitlements['products'])) {
    foreach ($user->user_entitlements['products'] as $product_value) {
      $nodes_ids[] = $product_value['nid'];
    }
  } else {
    $nodes = user_entitlements_get_entitlements($user);
    $nodes_ids = is_array($nodes) ? array_keys($nodes) : array();
  }
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'topic_notes');
  $query->propertyCondition('status', 1);
  $query->fieldCondition('field_topic_reference', 'target_id', $topic_id);
  $query->fieldCondition('field_entitlement_product', 'target_id', $nodes_ids);
  $query->addMetaData('account', user_load(1)); // Run the query as user 1.
  $array_result = $query->execute();
  return $array_result;
}

/**
 * Custom filter for 'My Notes' view. Filter according an existing relation 
 * @param type $topic_id
 * @return boolean
 */
function rcpar_dashboard_validate_by_note_display($topic_id){
  // True by default, so none will be displayed at first instance
  $result = TRUE;
  $consultation = rcpar_dashboard_get_notes_by_topic_id($topic_id);  

  if(isset($consultation['node'])){
    $result = FALSE;
  }
  return $result;
}

/**
 * Gets all notes created by an user in html format
 */
function rcpar_dashboard_all_notes_html(){
  $data = views_embed_view("print_", "page");
  echo theme('rcpar_dashboard_html_printing',array('data' => $data));  
  exit;
}

/**
 * Gets all notes created by an user in html format
 * Filters by section term id
 */
function rcpar_dashboard_all_notes_html_section($term_id){
  $data = views_embed_view("print_", "page_1", $term_id);
  echo theme('rcpar_dashboard_html_printing',array('data' => $data));  
  exit;
}

/**
 * Gets all notes created by an user in PDF format
 * Filters by chapter id
 */
function rcpar_dashboard_all_notes_html_chapter($chapter_id){
  $data = views_embed_view("print_", "page_2", $chapter_id);
  echo theme('rcpar_dashboard_html_printing',array('data' => $data));  
  exit;
}

/**
 * Gets all notes created by an user in PDF format
 */
function rcpar_dashboard_all_notes_pdf (){
  $html = views_embed_view("print_", "page");  
  rcpar_dashboard_render_pdf_object($html);
}

/**
 * Gets all notes created by an user in PDF format
 * Filters by section term id
 */
function rcpar_dashboard_all_notes_pdf_section($term_id){
  $html = views_embed_view("print_", "page_1", $term_id);
  rcpar_dashboard_render_pdf_object($html);
}

/**
 * Gets all notes created by an user in PDF format
 * Filters by chapter id
 */
function rcpar_dashboard_all_notes_pdf_chapter($chapter_id){  
  $html = views_embed_view("print_", "page_2", $chapter_id);
  rcpar_dashboard_render_pdf_object($html);
}

/**
 * Creates and display a PDF file
 * @param String $data
 */
function rcpar_dashboard_render_pdf_object($data){    
  require_once("sites/all/libraries/dompdf/dompdf_config.inc.php");  
  $site = variable_get('site_name');
  $date = format_date(REQUEST_TIME, 'medium');
  $data = theme('rcpar_dashboard_html_printing',array('data' => $data));  
  $dompdf = new DOMPDF;
  $dompdf->load_html($data);
  $dompdf->render();
  $dompdf->stream($site . " - my notes - " . $date.".pdf");  
}

/**
 * Creates a file and display html code in file
 * @param String $data
 */
function rcpar_dashboard_render_html_object($data){
  $html_obj = new simple_html_dom();
  $html_obj->load($data);
  $file = file_unmanaged_save_data($html_obj);   
  file_transfer($file, array('Content-disposition', 'attachment; filename=test'));
}
