<?php

/**
 * Query over the whole tree structure:
 *  Course
 *    --Chapter
 *      --Topic > video
 * 
 * and create a batch that will be cache all the information 
 * in one table and several files to increase site persformance
 * 
 */
function rcpar_dashboard_course_stats_rebuilding () {
  $years = exam_versions_years();
  //Topic list view cache
  cache_clear_all("rcpar_dashboard_view_cache_", 'cache', TRUE);
  $operations = array();
  db_truncate("course_stats")->execute();
  $query = "SELECT 
          n.nid as course_id, 
          echapter.entity_id as chapter_id, 
          topic.entity_id as topic_id, 
          chapter.delta as weight, 
          ref.delta as chapter_weight,
          section.field_course_section_tid as section, 
          course_type.field_course_type_ref_tid AS c_type,
          CONVERT(SUBSTRING(field_video_duration_value,1,2), unsigned integer) * 60000 + CONVERT(SUBSTRING(field_video_duration_value,-2), UNSIGNED INTEGER) * 1000 as ttime 
          FROM node n
          INNER JOIN field_data_field_course_section section on section.entity_id=n.nid and section.bundle='rcpa_course'
          INNER JOIN field_data_field_course_type_ref course_type on course_type.entity_id=n.nid and section.bundle='rcpa_course'
                    #COURSE
          INNER JOIN field_data_field_chapter_per_exam_version eref on eref.entity_id=n.nid
          INNER JOIN field_data_field_exam_version_single_val eyear on eyear.entity_id=eref.field_chapter_per_exam_version_target_id and eyear.field_exam_version_single_val_tid = :year
          INNER JOIN field_data_field_topic_exam_chapter_ref ref on ref.entity_id=eref.field_chapter_per_exam_version_target_id and ref.entity_type='node_ref_by_exam_version'
                    #CHAPTER
          INNER JOIN field_data_field_topic_per_exam_version echapter on echapter.entity_id =ref.field_topic_exam_chapter_ref_target_id
          INNER JOIN field_data_field_exam_version_single_val ceyear on ceyear.entity_id=echapter.field_topic_per_exam_version_target_id and ceyear.field_exam_version_single_val_tid = :year 
          INNER JOIN field_data_field_topic_reference chapter on chapter.entity_id = echapter.field_topic_per_exam_version_target_id and chapter.entity_type='node_ref_by_exam_version'
                    #TOPIC
          INNER JOIN field_data_field_rcpa_video topic on topic.entity_id = chapter.field_topic_reference_target_id
          INNER JOIN field_data_field_video_duration duration on duration.entity_id = topic.field_rcpa_video_target_id
          where n.type='rcpa_course' 
          group by topic.entity_id
          order by n.nid, chapter_id";

  $operations = array();
  $total = 0;
  foreach ($years as $year => $year_tid) {   
    $result = db_query($query, array(':year' => $year_tid));
    $data = $topic_collection = array();
    foreach ($result as $record) { 
      // collect data from record
      $course_id = $record->course_id;
      $chapter_id = $record->chapter_id;
      $topic_id = $record->topic_id;
      $weight = $record->weight;
      $chapter_weight = $record->chapter_weight;
      $section = $record->section;
      $c_type = $record->c_type;
      $ttime = $record->ttime;
      
      // collect all topics weight and time for each chapter
      $topic_collection[$course_id][$chapter_id][$topic_id]['weight'] = $weight;
      $topic_collection[$course_id][$chapter_id][$topic_id]['ttime']= $ttime;

      // Create structure for each course and chapters set of topics
      $data[$year_tid][$course_id][$chapter_id]= array(
        'chapter_weight' => $chapter_weight,
        'topics' => $topic_collection[$course_id][$chapter_id],
        'section' => $section,
        'c_type' => $c_type,
        );
      
    }
    // Rows to insert counter
    $total_rows = count($result);
    $operations[] = array( 'rcpar_dashboard_rebuilding_batch' , array(
        'data' => $data, 
        'year_ref' => $year
        )
    );
  } 
  /// Batch 
  $batch = array(
    'operations' => $operations,
    'finished' => 'rcpar_dashboard_rebuilding_batch_finish',
    'title' => t('Processing Dashboard Cache'),
    'init_message' => t('Inicializing Processing Dashboard.'),
    'progress_message' => t("Processed @current out of @total years-versions."),
    'error_message' => t('Dashboard cache could not be created'),
    );

  batch_set($batch);
}

function rcpar_dashboard_get_all_sections() {
  $vocabulary = taxonomy_vocabulary_machine_name_load('course_sections');
  $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
  $sections = array();
  foreach ($terms as $tid => $term) {
    $sections[$tid] = $term->name;
  }

  return $sections;
}

function rcpar_dashboard_get_all_types() {
  $vocabulary = taxonomy_vocabulary_machine_name_load('course_type');
  $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
  $sections = array();
  foreach ($terms as $tid => $term) {
    $sections[$tid] = $term->name;
  }

  return $sections;
}

function rcpar_dashboard_rebuild_form($form, $form_state) {
  $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Start Rebuild Process'),
      '#submit' => array('rcpar_dashboard_course_stats_rebuilding'),
  );

  return $form;
}

/**
 * Form to show Exam Day Countdown statistics
 */
function rcpar_dashboard_exam_countdown_statistics_form() {
  $form = array();
  $table = '';
  $statistics_var = variable_get('exam_countdown_statistics', array());
  
  if(!empty($statistics_var)){
    $header = array('Section', 'Count', 'Percentage');
    $rows = array();
    $users = $statistics_var['users'];
    foreach ($statistics_var as $key => $value) {
      if($key != 'users'){
       $percent =  $value/($users/100);
       $rows[] = array($key, $value, number_format($percent, 0). "%"); 
      }
    }
    $table = theme('table', array('header' => $header, 'rows' => $rows));
    $form['table'] = array(
      '#type' => 'item',
      '#markup' => $table,
      '#description' => t("Total Active users: $users")
    );
  }
  
  $form['action_button'] = array(
    '#type' => 'submit',
    '#value' => 'Process report',
  );

  return $form;
}

/**
 * Form submit to show Exam Day Countdown statistics
 */
function rcpar_dashboard_exam_countdown_statistics_form_submit($form, $form_state){
  $skus = rcpar_dashboard_entitlements_options();
  $results_exams = $results_entitlements = array();

  // Users with exam dates
  foreach ($skus as $sku) {
    $field = strtolower("field_{$sku}_exam_date_value");
    $field_data = strtolower("field_data_field_{$sku}_exam_date");
    $query = "SELECT DISTINCT u.uid
            FROM users u
            INNER JOIN node n on u.uid=n.uid
            INNER JOIN {$field_data} dt on u.uid=dt.entity_id
            where n.type = :type and dt.{$field} != :avoid 
            and dt.{$field}  != :void
            and n.title = CONCAT(u.uid,' - {$sku} Entitlement Product')";
  
    $results_exams[$sku] = db_query($query, array(      
      ':avoid' => '---',
      ':type' => 'user_entitlement_product',
      ':void' => '' ))->rowCount();
  }
  
  // All users with entitlements
  $result_entitlements = db_query("SELECT u.uid FROM users u
                                   INNER JOIN node n on u.uid=n.uid
                                    where n.type= :type GROUP BY u.uid", array(            
                                      ':type' => 'user_entitlement_product'))->rowCount();
  
  $result = $results_exams + array('users' => $result_entitlements);
  variable_set('exam_countdown_statistics', $result);

}


/****JSON FORMS AND FUNCTIONS****/

/**
 * Builds a json list of course -> chapters -> topics -> videos format.
 */
function rcpar_dashboard_course_stats_rebuild_json($form, &$form_state, $assoc = TRUE) {
  if (!$form_state['values']['version']) {
    drupal_set_message("No version selected", 'error');
    return;
  }

  /** @var int $version - Exam version term ID */
  $version = $form_state['values']['version'];
  $exam_version_term = taxonomy_term_load($version);

  $json = array();

  $result = db_query("SELECT 
          n.title,
          topic_node.title as topic_title,
          chapter.entity_id as chapter_id, 
          topic.entity_id as topic_id, 
          chapter.delta as weight, 
          ref.delta as chapter_weight,
          n_video.title as video_title,
          n_video.nid as video_id,
          n_chapter.title as chapter_title,
          section.field_course_section_tid as section, 
          course_type.field_course_type_ref_tid AS c_type,field_video_duration_value
          FROM node n
          INNER JOIN field_data_field_course_section section on section.entity_id=n.nid and section.bundle='rcpa_course'
          INNER JOIN field_data_field_course_type_ref course_type on course_type.entity_id=n.nid and section.bundle='rcpa_course'
          #COURSE
          INNER JOIN field_data_field_chapter_per_exam_version eref on eref.entity_id=n.nid
          INNER JOIN field_data_field_exam_version_single_val eyear on eyear.entity_id=eref.field_chapter_per_exam_version_target_id and eyear.field_exam_version_single_val_tid = {$version}
          INNER JOIN field_data_field_topic_exam_chapter_ref ref on ref.entity_id=eref.field_chapter_per_exam_version_target_id and ref.entity_type='node_ref_by_exam_version'		  
          INNER JOIN node n_chapter on ref.field_topic_exam_chapter_ref_target_id=n_chapter.nid
          #CHAPTER
          INNER JOIN field_data_field_topic_per_exam_version echapter on echapter.entity_id =ref.field_topic_exam_chapter_ref_target_id
          INNER JOIN field_data_field_exam_version_single_val ceyear on ceyear.entity_id=echapter.field_topic_per_exam_version_target_id and ceyear.field_exam_version_single_val_tid =  {$version}
          INNER JOIN field_data_field_topic_reference chapter on chapter.entity_id = echapter.field_topic_per_exam_version_target_id and chapter.entity_type='node_ref_by_exam_version'
          #TOPIC
          INNER JOIN field_data_field_rcpa_video topic on topic.entity_id = chapter.field_topic_reference_target_id and section.bundle='rcpa_course'
          INNER JOIN node topic_node on topic.entity_id = topic_node.nid
          INNER JOIN field_data_field_video_duration duration on duration.entity_id = topic.field_rcpa_video_target_id          
          INNER JOIN node n_video on n_video.nid = topic.field_rcpa_video_target_id
          where n.type='rcpa_course' 
          group by topic.entity_id
          order by n.nid, weight,chapter_weight;");


  if(!$assoc) {
    $sectionKeys = $chapterKeys = $topicKeys = array();
  }

  $sectionTotalVidTime = array();

  foreach ($result as $record) {
    $isCram = strstr(strtolower($record->title), "cram");

    $section = $isCram ? substr($record->title, 0, 3) . "-CRAM" : substr($record->title, 0, 3);
    $section_file = $isCram ? substr($record->title, 0, 3) . "-Cram" : substr($record->title, 0, 3);

    // Get the section title (e.g. "Auditing and attestation")
    $section_term = taxonomy_term_load($record->section);
    $section_title = $section_term->description;

    // Formulate chapter and topic slug
    $chapter_weight = $record->chapter_weight;
    $chapter_slug = "$section-$chapter_weight";
    $two_digits_weight = $isCram ? sprintf("%02d", $record->weight) : sprintf("%02d", $record->weight + 1);
    $topic_slug = "$chapter_weight.$two_digits_weight";

    // Get video URLs
    module_load_include('inc', 'courseware', 'services/topic');
    $courseType = $isCram ? 'cram_course' : 'regular_course';
    $video_urls = _courseware_videos_urls($courseType, $section_term->name, 'low', 'mid', 'high', $topic_slug);

    // Get PDF URL
    $urls = _courseware_get_topic_pdf_urls($courseType, $section_term->name, $topic_slug);
    $pdfURL = $urls['url_' . $exam_version_term->name];

    // Associative array format
    if($assoc) {
      $sectionKey = $section;
      $chapterKey = $chapter_slug;
      $topicKey = $topic_slug;
    }
    // Numeric array format (easier to parse with JSON)
    else {
      // Initialize this portion of the keys array if needed
      if(empty($sectionKeys)) {
        $sectionKeys = array();
      }
      if(empty($chapterKeys[$section])) {
        $chapterKeys[$section] = array();
      }
      if(empty($topicKeys[$section][$chapter_slug])) {
        $topicKeys[$section][$chapter_slug] = array();
      }

      // Associate each section, chapter, and topic with a numeric array index
      if(array_search($section, $sectionKeys) === FALSE) {
        $sectionKeys[] = $section;
      }
      if(array_search($chapter_slug, $chapterKeys[$section]) === FALSE) {
        $chapterKeys[$section][] = $chapter_slug;
      }
      if(array_search($topic_slug, $topicKeys[$section][$chapter_slug]) === FALSE) {
        $topicKeys[$section][$chapter_slug][] = $topic_slug;
      }

      // Retrieve the numeric index
      $sectionKey = array_search($section, $sectionKeys);
      $chapterKey = array_search($chapter_slug, $chapterKeys[$section]);
      $topicKey = array_search($topic_slug, $topicKeys[$section][$chapter_slug]);
    }

    // Convert video duration to milliseconds
    list($vidMin, $vidSec) = explode(':', $record->field_video_duration_value);
    $durationSeconds = ($vidMin * 60) + $vidSec;
    $durationMs = $durationSeconds * 1000;

    // Get total video time for this section
    if(!isset($sectionTotalVidTime[$section])) {
      $sectionTotalVidTime[$section] = db_query("
        SELECT total_time FROM {course_stats} 
        WHERE section = :sec AND year = :year",
        array(':sec' => $section, ':year' => $version))->fetchField();
    }

    // Build JSON
    $json['sections'][$sectionKey]['title'] = $section_title;
    $json['sections'][$sectionKey]['section'] = $section_term->name;
    $json['sections'][$sectionKey]['type'] = $courseType;
    $json['sections'][$sectionKey]['total_time'] = $sectionTotalVidTime[$section];

    $json['sections'][$sectionKey]['chapters'][$chapterKey]['title'] = $record->chapter_title;
    $json['sections'][$sectionKey]['chapters'][$chapterKey]['slug'] = $chapter_slug;
    $json['sections'][$sectionKey]['chapters'][$chapterKey]['delta'] = $record->chapter_weight;
    $json['sections'][$sectionKey]['chapters'][$chapterKey]['id'] = $record->chapter_id;
    $json['sections'][$sectionKey]['chapters'][$chapterKey]['topics'][$topicKey] = array(
      'title' => $record->topic_title,
      'slug' => $topic_slug,
      'delta' => $record->weight,
      'video_file' => "{$section_file}-{$chapter_weight}.{$two_digits_weight}.mp4",
      'video_urls' => array(
        'low' => $video_urls[0],
        'mid' => $video_urls[1],
        'high' => $video_urls[2],
      ),
      'pdf_url' => $pdfURL,
      'video_id' => $record->video_id,
      'duration' => $durationMs,
      'id' => $record->topic_id,
    );
  }
  return $json;
}

/**
 * Create a form so admin can generate a json file for statistics
 */
function rcpar_dashboard_rebuild_form_json($form, $form_state) {
  $versions = exam_versions_years();
  $form['version'] = array(
    '#type' => 'select',
    '#options' => array_flip($versions),
    '#title' => t('Version to generate the json'),
  );
  $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Start Rebuild Process for json'),
      '#submit' => array('rcpar_dashboard_course_stats_rebuild_json'),
  );

  return $form;
}

/**
 * Custom batch process to build a table with info about Courses, Chapters and Topics
 * This also set the cache to avoid heavy loads on dashboard pages
 */ 
function rcpar_dashboard_rebuilding_batch($data, $year_ref, &$context) {
  $sections = rcpar_dashboard_get_all_sections();
  $course_types = rcpar_dashboard_get_all_types();
  // Year value comes form the data key
  $year_term_name = $year_ref; 
  foreach($data as $year => $course){
    // loop courses
    foreach($course as $course_id => $course_chapters){ 
      $section_time = 0;
      // loop chapters
      foreach($course_chapters as $chapter_id => $chapter_info){ 
        $chapter_time = 0 ;
        // get chapter info
        $chapter_topics = $chapter_info['topics'];
        $course_type = $chapter_info['c_type'];
        $section_id = $chapter_info['section'];
        $chapter_weight = $chapter_info['chapter_weight']; 
        // loops topics
        foreach($chapter_topics as $topic_id => $topic_info){
          $topic_weight = $topic_info['weight'];
          $ttime = $topic_info['ttime'];
          // Recalculate chapter time accordint topics time sume
          $chapter_time += $ttime;
          // Send to insert Topics
          $context['results']['insert'][] = array(
            'type' => 'TOPIC',
            'section' => $course_types[$course_type] == "Online Course" ? $sections[$section_id] : $sections[$section_id] . "-CRAM",
            'course_type' => $course_types[$course_type],
            'weight' => $topic_weight,
            'total_time' => $ttime,
            'total_count' => 1,
            'nid' => $topic_id,
            'year' => $year
          );
        }
        // Recalculate section time according chapters time sume
        $section_time += $chapter_time;
        // Send to insert Chapter
        $context['results']['insert'][] = array( 
          'type' => 'CHAPTER',
          'section' => $course_types[$course_type] == "Online Course" ? $sections[$section_id] : $sections[$section_id] . "-CRAM",
          'course_type' => $course_types[$course_type],
          'weight' => $chapter_weight,
          'total_time' => $chapter_time,
          'total_count' => count($chapter_topics),
          'nid' => $chapter_id,
          'year' => $year
        );
      }
      // Send to Insert section
      $context['results']['insert'][] = array(
          'type' => 'SECTION',
          'section' => $course_types[$course_type] == "Online Course" ? $sections[$section_id] : $sections[$section_id] . "-CRAM",
          'course_type' => $course_types[$course_type],
          'weight' => 0,
          'total_time' => $section_time,
          'total_count' => count($data[$year][$course_id]),
          'nid' => $course_id,
          'year' => $year
      );
      // Load course elements view with course 
      // Save it as html file to be used on dashboard since the view is heavy.
      $result_courses = rcpar_dashboard_embed_view("course_elements", "chapter_topics_block", $course_id, $year_term_name, $year_term_name);
      cache_set("rcpar_dashboard_view_cache_{$course_id}_{$year_term_name}", $result_courses, 'cache');
      $cache_file_path = variable_get('file_public_path', conf_path() . '/files') . "/rcpar_dashboard_view_cache_{$course_id}_{$year_term_name}.html";
   
      if (file_exists($cache_file_path)) {
        unlink($cache_file_path);
      }
      file_put_contents($cache_file_path, $result_courses . "<!--STATIC FILE  COURSE ID: {$course_id} YEAR: {$year} -->");
    }
  }
  // Status message under progress bar
  $context['message'] = "Processing $year_term_name courses version";
}


/**
 * 
 * @param type $success false if one of the operations fail
 * @param type $results has a insert array of values to create on the table stats
 * @param type $operations
 * 
 * 
 */
function rcpar_dashboard_rebuilding_batch_finish($success, $results, $operations) {  
  if ($success) {    
    $insert = db_insert('course_stats')->fields(array('type', 'section', 'weight', 'total_time', 'total_count', 'course_type', 'nid', 'year'));
    foreach ($results['insert'] as $key => $value) {
      $insert->values($value);
    }
    $insert->execute();
    drupal_set_message("Rebuilding Process Complete Succesfully");
  } else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(
            t('An error occurred while processing @operation with arguments : @args', array(
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
                    )
            ), 'error'
    );
  }
}


/**
 * this is a reference for rcpar_dashboard_embed_view but ignoring the cache on the
 * views_get_view function
 */
function rcpar_dashboard_embed_view($name, $display_id = 'default') {
  $args = func_get_args();
  array_shift($args); // remove $name
  if (count($args)) {
    array_shift($args); // remove $display_id
  }

  $view = views_get_view($name, TRUE);
  if (!$view || !$view->access($display_id)) {
    return;
  }

  return $view->preview($display_id, $args);
}
