<?php

/**
 * Custom handler display for video link 
 * Used in course_elements view . It requires specific data in the view
 *
 * @ingroup views_field_handlers
 */
class rcpar_dashboard_video_link extends views_handler_field_entity {

  function render($values) {

    if($values){
      // Get course section (AUD,BEC, FAR, REG)
      $course_section_field = $values->field_field_course_section;
      // transform course section tid to value
      $course_section_term = $course_section_field[0]['raw']['taxonomy_term']; 
      $course_section = $course_section_term->name;

      // Get course type (Online, Cram)
      $course_type_field = $values->field_field_course_type_ref;
      // get tid
      $course_type = $course_type_field[0]['raw']['tid']; 

      // get deltas (weights)
      $chapter_delta = (int)$values->eck_node_ref_by_exam_version_field_data_field_chapter_per_ex_1;
      $topic_delta= (int)$values->eck_node_ref_by_exam_version_field_data_field_topic_per_exam;
      
      $video_title = $values->node_field_data_field_topic_reference_title;
      $video_duration_ref = $values->field_field_video_duration;
      $video_duration = $video_duration_ref[0]['raw']['value'];
      
      // for url
      $study_txt = "study";      
      // transform topic delta if is not Cram
      if($course_type != 1466) {
        $topic_delta = $topic_delta + 1;
      } else {
      // attach crma tag
        $study_txt = $study_txt."-cram";
      }
      $delta_item = $chapter_delta.".".sprintf("%02s", $topic_delta);
      $chapter_indicator = $chapter_delta ? $chapter_delta : "Intro";

      // Build text for video link
      if(arg(2, drupal_get_path_alias()) == "my-notes"){
        $result =  "{$delta_item} {$video_title} ({$video_duration})";
      }
      else {
        $text = "{$delta_item} {$video_title} ({$video_duration}) <i class='icon glyphicon glyphicon-chevron-right' aria-hidden='true'></i>";
        $url = "{$study_txt}/{$course_section}/{$course_section}-{$chapter_indicator}/{$delta_item}";      
        $result = l($text, $url, array('html' => TRUE, ));
      }
      return $result;
    }
  }
}
