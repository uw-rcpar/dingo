<?php
//Template for Exam Review Footer
//Variables:
//$uid
//$exams
//$custom_class
?>

<?php if (!empty($exams)) { ?>

<div class="footer-content" id="cpa-footer-content">
   <!--My Notes link-->
    <a class="exam-review-footer-item notes-class" href="#">
    <img src="/sites/all/themes/sensation/images/notes.png">
    <p>My Notes</p>
   </a>
    <!--Course Breakdowns link-->       
    <a class="exam-review-footer-item breakdowns-class" href="#">
    <img src="/sites/all/themes/sensation/images/course_breakdown.png">
    <p>Course Breakdowns</p>
   </a>
    <!--AICPA link-->       
    <a class="exam-review-footer-item aicpa-class use-ajax <?php print $custom_class;?>" href="dashboard-aicp-flag/nojs">
    <img src="/sites/all/themes/sensation/images/AICPA.png">
    <p> AICPA Released Questions</p>
   </a>    
    <!-- Course Textbook Updates link-->       
    <a class="exam-review-footer-item ctul-class" href="#">
    <img src="/sites/all/themes/sensation/images/star.png">
    <p> Course Textbook Updates</p>
   </a>    
    
</div>

<?php } ?>

