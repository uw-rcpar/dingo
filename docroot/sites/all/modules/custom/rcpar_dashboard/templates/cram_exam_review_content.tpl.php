<?php
//Template for Exam Review Content
//Variables:
//$uid
//$exams
//$average
?>


<?php if (!empty($exams)) { ?>

  <div id="cram-exams-review-content dashboard">
    <div><h4>Course Progression </h4> <span class="courses-average-class"><?php print $average . " %" ?></span></div>
    <?php foreach ($exams as $key => $value) { ?>
      <div class="radial-element" id=<?php print $key ?>> <p class="element-value"> <?php print $value ?> </p> </div>         
    <?php } ?>  
  </div>

<?php } else { ?>
  <div id="cram-exams-review-content-empty">
    <div><h4>Add a related course: </h4> <span class="courses-type-empty-box"> Cram Course </span> <a>Add more link</a></div>
  </div>
<?php } ?>


