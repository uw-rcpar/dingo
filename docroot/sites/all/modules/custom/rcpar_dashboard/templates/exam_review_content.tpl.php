<?php
//Template for Exam Review Content
//Variables:
//$uid
//$exams
//$average
$all_exams = rcpar_dashboard_entitlements_options();
$exams = empty($exams) ? array() : $exams;
?>

  <div id="exams-review-content">
    <div class="launch-course-link"><a href="">Launch Course Overview <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></div>
    <?php foreach ($all_exams as $exam) { ?>    
      <?php if(in_array($exam, array_keys($exams))) : ?>
      <div class="radial-element" id="<?php print $exam ?>">
      <p class="element-value"> <?php print $exams[$exam] ?> </p> 
      </div>         
      <?php else:?>
      <div class="add-link">
      	<div class="radial-static-element" id="static-<?php print $exam ?>"></div>
	  	<div class="element-value"><?php print $exam ?></div>
         <div class="plus-sign"><a href="/<?php echo drupal_get_path_alias('node/395');?>" target="_blank" class="element-link">+</a></div>
         <div class="add-course"><a href="/<?php echo drupal_get_path_alias('node/395');?>" target="_blank" class="element-link"> +ADD COURSE</a></div>
	  </div>

      <?php endif?>
    <?php } ?>  
    <div class="clear-empty"></div>
  </div>

