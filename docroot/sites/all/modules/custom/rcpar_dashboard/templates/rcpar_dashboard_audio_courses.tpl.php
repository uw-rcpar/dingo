<?php
//Template for Audio Courses
//Variables:
//courses
?>

  <div class="audio-courses dashboard">
    <h2 class="block-title">Audio Lectures </h2>
    <div class="audio-courses-content">
	    <?php 
	    if (!empty($courses)) {
	    foreach ($courses as $key => $value) { 
	      $img = $value['img'];
	      $desc = $value['desc'];
	      $section = $value['section'];
	      $file = $value['file'] != "" ? l("Download",$value['file'],array('attributes' => array('target' => '_blank'))) : "";
              $expiration_date = $value['expiration_date'];
	      ?>
	      <div class="audio-course-item no-gutter" id=<?php print "audio-$section" ?>> 
	      	<div class="audio-img col-md-4"><img src=<?php print $img ?> ></div>
		  	<div class="col-md-8">
		  	  <h4 class="audio-title"> <?php print $section ?> <span class="audio-tag">Audio Lectures</span> </h4> 
		  	  <div class="audio-description"> <?php print $desc ?> </div> 
		  	  <div class="audio-link"><?php print $file ?></div>
		  	  <div class="audio-expires">Expires <?php print date('m/d/Y',$expiration_date) ?></div>
		  	</div>
		  	<div class="clear-empty"></div>
	      </div>         
	    <?php } }?> 
    </div> 
  </div>



