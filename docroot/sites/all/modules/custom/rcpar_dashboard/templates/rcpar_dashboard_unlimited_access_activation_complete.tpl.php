<div class="access-extended">
  <h2>Access Extended</h2>
  <p>Your Course Access has been extended for six months.</p>
  <div class="take-me-to-course-btn"><a href="/dashboard" class="btn btn-primary">Take me to my course</a></div>
  <div class="footnote">For additional assistance, please contact our <a href="/customer-care/course">Customer Care Department</a>. Course policies can be found <a href="/dashboard/support/course-policies-and-procedures">here</a>.</div>
</div>