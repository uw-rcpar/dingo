
<div id="exam-version-selection" class="modal fade exam-version-modal" data-backdrop="true" role="dialog">
  <div class="modal-content">

    <!-- STEP 1 -->
    <div id="step-exam-changes-info" class="exam-version-selection-step">
      <div class="modal-header">
        <h3 class="modal-title">Select course materials for <span class="section-name">AUD</span> <button type="button" class="close finish-break" data-dismiss="modal" aria-hidden="true">&times;</button></h3>
      </div><!-- /.modal-header -->
      <div class="modal-body">
        <div class="text">
          <p>Effective April 2017, the CPA Exam is undergoing significant changes.
            But rest assured - we've got you covered. Simply <strong>let us
            know which version of the <span class="section-name">AUD</span> Exam you're studying for</strong> and
            we'll customize your course materials accordingly.
          </p>
          <p>You’ll be able to upgrade from 2016 to 2017 course materials if you need to, but you won’t be able to downgrade from 2017 to 2016 materials.
          </p>
          <div>
            <input type="radio" name="exam-version" id="radio-2016" value="2016" />
              <label for="radio-2016">
                <strong>2016 Materials:</strong> I'm sitting for the <span class="section-name">AUD</span> Exam before April 2017.
                <div class="selected-textbook-info selected-textbook-2016">(You've been shipped the 2016 <span class="section-name">AUD</span> textbook.)</div>
              </label>
            <br>
            <input type="radio" name="exam-version" id="radio-2017" value="2017" />
            <label for="radio-2017">
              <strong>2017 Materials:</strong> I'm sitting for the <span class="section-name">AUD</span> Exam on or after April 1, 2017.
              <div class="selected-textbook-info selected-textbook-2017">(You've been shipped the 2017 <span class="section-name">AUD</span> textbook.)</div>
            </label>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col-sm-4 buttons-left">
          <button type="button" data-dismiss="modal" class="close-modal">Cancel</button>
        </div>
        <div class="col-sm-8 buttons-right">
          <div class="ok-button">
            <button type="button" data-dismiss="modal" class="btn next-step">
              Ok, take me to my course!
              <span class="glyphicon glyphicon-refresh spinning"></span>
            </button>
          </div>
          <div class="more-button">
            <button type="button" data-dismiss="modal" class="btn" id="wait-tell-me-more" >Wait, tell me more about this</button>
          </div>
        </div>
      </div>
    </div>

    <!-- STEP 2 -->
    <div id="step-confirmation-have-data" class="exam-version-selection-step">
      <div class="modal-header">
        <h3 class="modal-title"><img src="/sites/all/themes/bootstrap_rcpar/css/img/alert-warning-icon.svg" width="30">
          Are you sure you want to upgrade to 2017 <span class="section-name">AUD</span> <br>course materials?
          <button type="button" class="close finish-break" data-dismiss="modal" aria-hidden="true">&times;</button></h3>
      </div><!-- /.modal-header -->
      <div class="modal-body">
        <div class="text">
          <p>
            You have existing progress in the 2016 version of the <span class="section-name">AUD</span> course materials. On upgrade to 2017 course materials, most of your progress and saved data will carry over and areas you need to revisit will be brought to your attention.
          </p>
          <p>
            For areas that have undergone changes from the 2016 to the 2017 version of course materials, this action will clear the following data:
          </p>
          <ul>
            <li>Video watch history</li>
            <li>All 2016 e-textbook highlights will be cleared</li>
          </ul>
          <p>
            Once upgraded, you won’t be able to revert to 2016 course materials or retrieve lost data. If you need additional help or clarification please contact our customer support team at 877-764-4272.
          </p>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col-sm-2 buttons-left">
          <button type="button" data-dismiss="modal" class="close-modal">Cancel</button>
        </div>
        <div class="col-sm-10 buttons-right">
          <button type="button" data-dismiss="modal" class="btn next-step">
            Upgrade me to 2017 (I understand this will clear some of my 2016 history)
            <span class="glyphicon glyphicon-refresh spinning"></span>
          </button>
        </div>
      </div>
    </div>


    <!-- STEP 3 -->
    <div id="step-confirmation-no-data" class="exam-version-selection-step">
      <div class="modal-header">
        <h3 class="modal-title"> Are you sure you need 2017 <span class="section-name">AUD</span> materials?
          <button type="button" class="close finish-break" data-dismiss="modal" aria-hidden="true">&times;</button></h3>
      </div><!-- /.modal-header -->
      <div class="modal-body">
        <div class="text">
          <p>
            Once you set your <span class="section-name">AUD</span> course materials to the 2017 version, you won't be
            able to access 2016 course materials.
          </p>
          <p>
            If there’s a chance you’ll be taking the <span class="section-name">AUD</span> exam sooner than April 2017, your best bet is to wait until you know your exam date so that you don’t lock yourself into the 2017 course materials ahead of time.
          </p>
          <p class="hidden">
            If you need assistance, please contact Customer Support at 877-764-4272.
          </p>
        </div>
      </div>
      <div class="modal-footer">
        <div class="col-sm-5 buttons-left">
          <button type="button" data-dismiss="modal" class="close-modal">I'll wait and choose later</button>
        </div>
        <div class="col-sm-7 buttons-right">
          <button type="button" data-dismiss="modal" class="btn next-step">
            Give me the 2017 <span class="section-name">AUD</span> materials!
            <span class="glyphicon glyphicon-refresh spinning"></span>
          </button>
        </div>
      </div>
    </div>

  </div><!-- /.modal-content -->
</div><!-- /.modal -->
