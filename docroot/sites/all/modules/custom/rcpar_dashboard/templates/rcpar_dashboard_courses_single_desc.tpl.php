<?php
//Template for Exam Review Content
//Variables:
//$uid
//$exams
//$exams_info
$all_exams = rcpar_dashboard_entitlements_options();
?>


<?php if (!empty($exams)) { ?>
    <div id="courses-detail-block">
        <?php 
          foreach ($all_exams as $exam) { 
            if(in_array($exam, array_keys($exams))) {
              $key = $exam;
              $value = $exams[$exam];
          ?>
            <div class="course-single-element" id=<?php print $key . "single-detail" ?>>              
                <div class="radial-element" id=<?php print $key ?>> <p class="element-value"> <?php print $value ?> </p> </div>                 

                <div clas="expire-date-class">Expires <?php print $exams_info[$key]["expire_date"]  ?></div>             
                <a clas="launch-course-class" href="/node/<?php print $exams_info[$key]["nid"] ?>">Launch Course </a>
                <div class="you-left-video-class">
                    <h4>You left off at: </h4>
                    <?php
                    print views_embed_view("videos_record", "block_1", $uid,$exams_info[$key]["tid"]);
                    ?>
                </div>
                <div class="course-single-element-footer">

                    <a class="exam-review-footer-item my-notes" href="#">My Notes</a>
                    <a class="exam-review-footer-item course-breakdowns" href="#">Course<br />Breakdowns</a>
                    <a class="exam-review-footer-item aicpa-questions aicpa-class" id="aicpa-id" href="#">AICPA<br />Released<br />Questions</a>    
                    <a class="exam-review-footer-item course-textbook-updates" href="#">Course<br />Textbook<br />Updates</a> 

                </div>

            </div>                            
        <?php } else{?>  
          <a href="/<?php echo drupal_get_path_alias('node/12');?>" target="_blank">       
          <div class="radial-static-element" id=static-<?php print $exam ?>>
          <p class="element-value"><?php print $exam ?></p>
          <p>+</p>
          <p>+ADD COURSE</p>
      </div>
      </a>
      <?php } ?> 
        
    </div>
<?php } }?>


