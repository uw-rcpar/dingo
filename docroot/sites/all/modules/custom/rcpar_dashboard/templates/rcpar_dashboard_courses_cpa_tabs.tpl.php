<?php 
//Template for Courses tabs
//Variables:
//$active_ones
//$no_active_ones
?>

<div id="my-courses-tabs" role="tabpanel">  
<ul class="nav nav-tabs" role="tablist">  
 <?php 
 $i = 0;
 foreach ($active_ones as $a_key => $a_value) { 
   $i++; ?>  
  <li role="presentation" class= "<?php print $a_value["class"]?>" >
     <a class="use-ajax" href='/dashboard/courses-tab/<?php  print $a_value["nid"]?>/nojs' aria-controls="tab-<?php print $i; ?>" role="tab" data-toggle="tab">
       <div class="radial-element" id="<?php print $a_key ?>"> 
         <p class="element-value"> <?php print $a_value['value'] ?> </p> 
       </div>      
      <div class="expire-date-class"> Expires <?php print $a_value["expire_date"] ?> </div>    
    </a>
  </li>    
 <?php } foreach ($no_active_ones as $no_key => $no_value) { ?>
    <li role="presentation">
      <div class="add-link">                              
      <div class="radial-static-element" id="static-<?php print $no_key ?>"></div>
	 	 <div class="element-value"><?php print $no_key ?></div>
         <div class="plus-sign"><a  href="/node/<?php print drupal_get_path_alias('node/12'); ?>" target="_blank" class="element-link">+</a></div>
         <div class="add-course"><a  href="/node/<?php print drupal_get_path_alias('node/12'); ?>" target="_blank" class="element-link"> +ADD COURSE</a></div>      
      </div>      
  </li>    
<?php } ?>
</ul>  
  <div class="tab-content">
 <?php 
 $n = 0;
 foreach ($active_ones as $a_key => $a_value) { 
   $n++;?>  
    <?php if($a_value["class"] == "active"){ ?>        
      <div role="tabpanel" class="tab-pane fade<?php if($a_value["class"] == "active"){ ?> active in<?php } ?>" id="tab-<?php print $n; ?>">
        <?php                    
          print views_embed_view("course_elements", "chapter_topics_block", $a_value["nid"], $a_value["exam_version"], $a_value["exam_version"]);
        ?>
      </div>    
 <?php } ?>   
 <?php } ?>  
  </div>
</div>
