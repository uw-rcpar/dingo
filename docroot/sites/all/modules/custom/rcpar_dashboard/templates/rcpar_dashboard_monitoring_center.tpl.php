<?php if(empty($partner_groups)){ ?>
<div class='empty-monitoring-center'>
  <p> No content to display</p>
</div>
 <?php }else{ ?>
<div id="monitoring-tabs" class="tabs-wrapper" role="tabpanel">
  <div class="monitoring-filter">
    <ul>            
      <li>
        <a class="monitoring-filter-link all-groups-filter" href="/dashboard/monitoring-center"> Show all groups</a>
      </li>       
      <li>
        <a class="monitoring-filter-link all-groups-filter" href="/dashboard/monitoring-center?expired_groups=1"> Show only expired groups</a>
      </li>              
    </ul>
  </div>
  <ul class="nav nav-tabs" role="tablist">                     
    <?php      
    $i = 0;
    foreach ($partner_groups as $node_key => $node_groups) {  
      foreach ($node_groups['groups'] as $group_key => $group_info) { $i++; 
      if($i == 1){ ?>
        <li role="presentation" class="tab-item  group-tab-<?php print $i; ?> <?php print $group_key ?>-tab col-sm-3 active">
      <?php }else{ ?>
        <li role="presentation" class="tab-item  group-tab-<?php print $i; ?> <?php print $group_key ?>-tab col-sm-3">
      <?php } ?> 
        <a href="#tab-<?php print $i; ?>" aria-controls="tab-<?php print $i; ?>" role="tab" data-toggle="tab">
          <?php print key($group_info);?>
        </a>
      </li>    
    <?php } } ?>
  </ul>
      
  <div class="tab-content">      
    <?php 
    $n = 0; 
    foreach ($partner_groups as $node_key => $node_groups) {  
      foreach ($node_groups['groups'] as $group_key => $group_info) { $n++; 
      if($n == 1){ ?>             
        <div role="tabpanel" class="active in tab-pane fade tab-content-<?php print $group_key ?>" id="tab-<?php print $n; ?>">     
       <?php }else{ ?>
        <div role="tabpanel" class="tab-pane fade tab-content-<?php print $group_key ?>" id="tab-<?php print $n; ?>">  
       <?php } ?>                
        <!--Header Group Info-->
        <div class="header-group-info group-material" > 
          <label>CPA Exam Material</label>      
          <?php
          if(isset($node_groups['partner_sections']['fieldset_content'])){ ?>                     
          <fieldset class="panel panel-default partner-section-fieldset-class">            
            <a href="#partner-sections-fieldset" class="fieldset-legend collapsed" data-toggle="collapse"> 
              <?php print implode(' , ', $node_groups['partner_sections']['fieldset_title'])?> 
              <span class ='partner-section-fieldset-show-more'> Show more </span>
            </a>            
            <div id="partner-sections-fieldset" class="panel-collapse fade collapse" style="height: 0px;">
              <div class="panel-body"> <?php print implode(' , ', $node_groups['partner_sections']['fieldset_content'])?> </div>
            </div>
          </fieldset> 
          <?php }else{ ?>            
          <p><?php print implode(' , ', $node_groups['partner_sections'])?></p> 
          <?php }
          ?>                    
        </div>
        <div class="header-group-info group-dates"> 
          <div class="header-group-info group-start-date">
            <label>Access Start</label>          
            <p><?php print $group_info[key($group_info)]['start_date']?></p> 
          </div>
          <div class="header-group-info group-end-date">
            <label>Access End</label>
            <p><?php print $group_info[key($group_info)]['end_date']?></p>
          </div>                     
        </div>
        <!--Ends Header Group Info-->
        <!--Email List Group Info-->
        <div class="group-email-list">    
          <ul>                             
            <table>                    
              <tr>                      
                <th>Roster</th>                      
                <th>Lecture Ranges</th>
              </tr>                               
                <?php                                     
                foreach ($group_info as $group_info_value) {           
                  foreach ($group_info_value as $group_info_value_key => $group_info_value_email) {
                    if(is_numeric($group_info_value_key)){ ?>                                                
                      <tr>         
                        <td class="group-participant-email">
                          <?php print $participants_info[$group_info_value_email]['uname']; ?> 
                        </td>
                        <td class="group-participant-average">
                          <?php print $participants_info[$group_info_value_email]['average']; ?> 
                        </td>
                      </tr>               
                <?php                
                    }                                                  
                  }              
                }  ?> 
            </table>              
          </ul>
        </div>        
        <!--End Email List Group Info-->
      </div>	
    <?php } } ?> 
  </div>  

</div>
  
<?php } ?>