<?php
// If the old/original 'ipq' module is still enabled in this environment, we're still using the original monitoring
// center, and we want to load its original JS/CSS assets.
if(module_exists('ipq')) {
  drupal_add_js(drupal_get_path('module','ipq') . '/js/ipq-modal.js');
  drupal_add_css(drupal_get_path('module', 'ipq') . '/css/ipq.css');
}
else {
  // New monitoring center JS/CSS
  drupal_add_js(drupal_get_path('module','ipq_common') . '/js/ipq-modal.js');
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/ipq.css');
}

global $user;
          $y = 1;
          ?>
          <div role="tabpanel" class="active in tab-pane fade tab-content-<?php print $first_partner_nid ?>" id="tab-<?php print $y; ?>"> 
          <?php $i = 0;
          foreach ($partner_groups['groups'] as $group_key => $group_info) { $i++;  ?>
                <div class="panel-group" id="accordion-<?php print $y ?>" role="tablist" aria-multiselectable="true" >
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading<?php print $i; ?>">				                    
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-<?php print $y ?>" href="#collapse<?php print $i; ?>" aria-expanded="true" aria-controls="collapse<?php print $i; ?>"><?php print key($group_info); ?> </a></h4>				                
                        </div>
                        <div id="collapse<?php print $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php print $i; ?>">
                            <div class="panel-body">
                                <!--Header Group Info-->				                        
                                <div class="header-group-info group-material">				                            
                                    <label>CPA Exam Material</label> 				                            				                            
                                        <?php 
                                        $partner_sections = rcpar_dashboard_monitoring_get_partner_access_material($first_partner_nid);
                                        if (isset($partner_sections['fieldset_content'])) { ?>
                                      <fieldset class="panel panel-default partner-section-fieldset-class">
                                          <a href="#partner-sections-fieldset<?php print $i; ?>" class="fieldset-legend collapsed" data-toggle="collapse"><span class='partner-section-fieldset-show-more pull-right btn btn-default'>Show more</span></a>			
                                          <p>
                                          <?php
                                          $c = 0;
                                          $total = count($partner_sections['fieldset_title']);
                                          foreach ($partner_sections['fieldset_title'] as $ps_sku => $ps_rows) {
                                            $c++;
                                            print "<b> $ps_sku : </b>" . implode(', ', $ps_rows);
                                            print $c != $total ? ' | ' : '';
                                          }
                                          ?>
                                          </p>
                                          <div id="partner-sections-fieldset<?php print $i; ?>" class="panel-collapse fade collapse" style="height: 0px;">
                                              <div class="panel-body col-sm-9">
                                                  <p>
                                                  <?php
                                                  $c = 0;
                                                  $total = count($partner_sections['fieldset_content']);
                                                  foreach ($partner_sections['fieldset_content'] as $ps_sku => $ps_rows) {
                                                    $c++;
                                                    if ($ps_sku != '') {
                                                      print "<b> $ps_sku : </b>" . implode(', ', $ps_rows) ;
                                                    }
                                                    else {
                                                      print implode(', ', $ps_rows);
                                                    }
                                                    print $c != $total ? ' | ' : ' .';
                                                  }
                                                  ?>
                                                  </p>
                                              </div>
                                          </div>
                                      </fieldset>
                                        <?php }
                                        else { ?>
                                      <p><?php
                                    $c = 0;
                                    foreach ($partner_sections as $ps_sku => $ps_rows) {
                                      print "<b> $ps_sku : </b>" . implode(', ', $ps_rows) ;
                                      print $c == count($partner_sections) ? ' | ' : '';
                                      $c++;
                                    }
                                    ?></p>
          <?php } ?>
                                </div>

                                <div class="header-group-info group-dates">
                                    <div class="header-group-info group-start-date col-sm-3">
                                        <label>Access Start</label>

                                        <p><?php print $group_info[key($group_info)]['start_date'] ?></p>
                                    </div>

                                    <div class="header-group-info group-end-date col-sm-3">
                                        <label>Access End</label>

                                        <p><?php print $group_info[key($group_info)]['end_date'] ?></p>
                                    </div>
                                </div><!--Ends Header Group Info-->
                                <!--Email List Group Info-->
                                <div class="group-email-list">
                                    <div>
                                        <table class="table table-bordered">                                            
                                            <tr>                                                    
                                                <th>Roster</th>                                                    
                                                  <?php foreach (rcpar_dashboard_entitlements_options() as $sku) { ?>                                                                               
                                                      <th> <?php print $sku ?></th>
                                                  <?php } ?>
                                                </tr>
                                                <?php
                                                foreach ($group_info as $group_info_value) {
                                                  foreach ($group_info_value['students_info']  as $email => $student_info) { ?>
                                                      <tr>
                                                          <td class="group-participant-email"><?php print $email; ?></td>
                                                              <?php foreach (rcpar_dashboard_entitlements_options() as $sku) { ?>                                                                               
                                                            <td class="group-row-data"> <?php print isset($student_info['progress'][$sku]) ?
                                                                        $student_info['progress'][$sku] . '%' : '0%';
                                                                ?></td>                                             
                                                              <?php } 
                                                                $ipq_classes = array('modal-load', 'link-disabled');
                                                                $section = "";
                                                                if ($student_info['uid'] &&  $group_key){
                                                                  $ipq_classes = array('modal-load');
                                                                  $list = rcpar_partner_get_access_from_list($student_info['uid']);
                                                                  $section = array_pop($list[$user->uid]);                                                                  
                                                                }
                                                              ?>                                                                                            
                                                          <td class='group-row-options' style="display: none;" colspan="4"> 
                                                            <?php print l('LECTURE DETAILS', 'ajax/monitoring/center/tabs/'.$student_info['uid'].'/'.$section,  array('attributes' => array('class' => $ipq_classes )) ); ?>                                                         
                                                            <?php print l('IPQ SCORE', '/dashboard/testcenter/history/'.$student_info['uid'].'/'.$group_key.'/'.$section,  array('attributes' => array('class' => $ipq_classes )) ); ?>
                                                          <?php print l('IPQ OVERVIEW', '/dashboard/testcenter/overview/'.$student_info['uid'].'/'.$group_key.'/'.$section,  array('attributes' => array('class' => $ipq_classes )) ); ?> </td>                                                          
                                                      </tr>
                                                      <?php  
                                                  }
                                                }
                                                ?>
                                          
                                        </table>
                                    </div>    
                                </div>
                                <!--End Email List Group Info-->
                            </div> <!-- panel body -->
                        </div> <!-- panel-collapse body -->                                             
                    </div> <!-- panel-default body -->
                </div> <!-- panel group --> 
  <?php } ?>
          </div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
