<!-- Modal -->
<div class="modal fade" id="course-activation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Course Activation</h4>
      </div>
      <div class="modal-body">
        <p>
          Please confirm that you wish to activate your <?php print $course; ?> course.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>     
        <a class="use-ajax" href= <?php print "/activate-delayed-courses/".$course_id."/nojs"?>>
            <button type="button" class="btn btn-primary" >Activate Now </button>
        </a>
      </div>
    </div>
  </div>
</div>
