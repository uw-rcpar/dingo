

<?php if (!empty($exams)) { ?>

  <div id="multi-graph-content">
    <div><h4>Course Progression </h4> <span class="courses-average-class">Total Progress: <?php print $average . " %" ?></span></div>
    <?php foreach ($exams as $key => $value) { ?>
      <div class="radial-element multi-radial-element" id=<?php print $key."-multi" ?>> 
          <p class="element-value"> <?php print $value ?> </p> 
    <?php } ?>  
    <?php foreach ($exams as $key => $value) { ?>
      </div>         
    <?php } ?>  
      <div class="start-date-class"> Start date <?php print $start_date ?></div>
  </div>

<?php } ?>
