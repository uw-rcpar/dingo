<?php 
//Template for Courses tabs
//Variables:
//$active_ones
//$no_active_ones
?>

<div id="my-courses-tabs" class="tabs-wrapper" role="tabpanel">  
<h4 class="modal-title" id="myModalLabel">Lecture Details</h4>    
<ul class="nav nav-tabs" role="tablist">  
 <?php 
 $i = 0;
 foreach ($active_ones as $a_key => $a_value) {
 $i++; ?>  
  <li role="presentation" class="tab-item <?php print $a_value["class"] ?> tab-<?php print $i; ?> <?php print $a_key ?>-tab col-sm-3">
     <a href="#tab-<?php print $i; ?>" aria-controls="tab-<?php print $i; ?>" role="tab" data-toggle="tab">
     <div class="radial-wrapper">
       <div class="radial-element" id="<?php print $a_key ?>"> 
         <p class="element-value"> <?php print $a_value['value'] ?> </p> 
       </div>      
      <div class="expire-date-class"> Expires <?php print $a_value["expire_date"] ?> </div> 
      </div>   
    </a>
  </li>    
 <?php } ?> 
</ul>   
<div class='student-info'>
  <label>Student: </label> <p><?php print $student;?></p>
  <p> <?php print $student_mail;?> </p>
</div>
    <div class="tab-content">
 <?php 
 $n = 0;
 foreach ($active_ones as $a_key => $a_value) {
 $n++; ?>  
       
      <div role="tabpanel" class="tab-pane fade<?php if($a_value["class"] == "active"){ ?> active in<?php } ?> tab-content-<?php print $a_key ?>" id="tab-<?php print $n; ?>">
        <?php print rcpar_dashboard_mc_get_my_courses_view($a_value["nid"]); ?>
      </div>
	
 <?php } ?>  
    </div>
</div>

