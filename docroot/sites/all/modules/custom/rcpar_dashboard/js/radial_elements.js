(function ($) {
  Drupal.behaviors.rcpar_dashboard = {
    attach: function (context, settings) {
      /// Custom Accordion for My courses Tabs
      //  if (context == document) {
      function viewed_display(li) {
        var tab_id = li.find("a").attr("aria-controls");
        var chapter_collection = [];
        var version = 0;
        $.each($("#" + tab_id + " h3"), function () {
          var item = $(this).find(".viewed-wrapper");
          chapter_collection.push(item.attr('id'));
          version = item.attr('data-version');
        });

        var uid = 0;
        var pid = "";
        if ($('.view-course-elements').closest('.modal-body').length) {
          uid = document.getElementById("details-uid").value;
          pid = "/" + document.getElementById("details-pid").value;
        }
        $.ajax({
          url: Drupal.settings.basePath + "viewed-info/" + chapter_collection + "/" + uid + "/" + version + pid,
          success: function (data) {
            $.each(data, function (id) {
              $("#" + id).append(data[id]);
              $("#" + id).find('.chap-loading-tag').remove();
            });
          }
        });
      }

      //section to asign prefix in header rows
      $(".view-course-elements h3", context).each(function () {
        var item = $(this).find("p:first-child").text();
        var section_text = $.trim(item.split(":")[0]);
        var sku = $.trim(section_text.split("-")[0]);
        section_text = section_text.replace(" ", '-');
        $(this).addClass(section_text);

        //section to assign prefix in link items
        $(this).nextUntil("h3").each(function () {
          $(this).find("a:first-child").each(function () {
            var a_element = $(this);
            var sub_item = $(a_element).text();
            var sub_section_text = $.trim(sub_item.split(" ")[0]);
            sub_section_text = sub_section_text.replace(".", "-");
            sku = $.trim(sku.split(" ")[0]);
            $(this).addClass(sku + "-" + sub_section_text);
          });
        });
      });
      //ends here

      $('.view-course-elements .view-content > .views-row', context).toggle();
      $('.view-course-elements h3', context).click(function () {
        var header_item = $(this);
        var sku = header_item.attr('class');
        sku = $.trim(sku.split("-")[0]);
        var chapter = header_item.find(".viewed-wrapper").attr('id');
        chapter = $.trim(chapter.split("-")[1]);
        $(this).nextUntil("h3").toggle('clip', function () {
          if ($(this).hasClass("open")) {
            $(this).removeClass("open");
            header_item.removeClass("open");
          }
          else {
            $(this).addClass("open");
            header_item.addClass("open");
            $(this).attr("style", "display: block; overflow: visible;");
          }
        });

        if (!$(this).hasClass("already-attached")) {
          $(this).addClass("already-attached");
          var topics_collection = [];
          $(this).nextUntil("h3").each(function () {
            var item = $(this).find(".courses-wrapper");
            topics_collection.push(item.attr('id'));
          });
          var uid = 0;
          if ($('.view-course-elements').closest('.modal-body').length) {
            var uid = document.getElementById("details-uid").value;
          }

          if ($('body').hasClass("page-dashboard-my-cram-courses")) {
            sku = sku + '-CRAM';
          }

          $.ajax({
            url: Drupal.settings.basePath + "restricted-topics/" + topics_collection + "/" + sku + "/" + chapter + "/" + uid,
            success: function (data) {
              $.each(data, function (id) {
                var a_element = $("#" + id).parent().parent().prev().find('a');
                a_element.parent().parent().addClass("restricted-item").attr('data-toggle', 'popover').attr('title', 'Currently Unavailable').attr('data-content', "Upgrade your course for full lecture access.").attr('data-placement', 'bottom');
                var a_text = a_element.text();
                var classes = a_element.attr('class');
                $(a_element).replaceWith("<p class=" + classes + ">" + a_text + "</p>");
                $('[data-toggle="popover"]').popover().click(function () {
                  setTimeout(function () {
                    $('.restricted-item').popover('hide');
                  }, 6000);
                });
              });
            }
          });

          var url = Drupal.settings.basePath + "course-info/" + topics_collection + "/" + sku + "/" + uid;
          if (window.location.href.indexOf('dashboard/monitoring-center') != -1) {
            url += "/1"; // We need to indicate the server that we need to check the expired entitlements too
          }
          $.ajax({
            url: url,
            success: function (data) {
              $.each(data, function (id) {
                $("#" + id).append(data[id]['progress_notes_bookmark']);
                $("#" + id).next('.last-viewed-class').append(data[id]['last_viewed']);
                $("#" + id).find('.loading-tag').remove();
              });
            }
          });

        }

      });

      /// Load Viewed info according Chapter

      $.each($('.page-dashboard #my-courses-tabs li', context), function () {
        if ($(this).hasClass("active")) {
          viewed_display($(this));
          $(this).addClass("viewed-attached");
          return false;
        }
      });

      $('.page-dashboard #my-courses-tabs li', context).click(function () {
        if (!$(this).hasClass("viewed-attached")) {
          $(this).addClass("viewed-attached");
          viewed_display($(this));
        }
        $('.page-dashboard #my-courses-tabs li').removeClass("active-tab");
        $(this).addClass("active-tab");
      });

      /// Dashboard Alert Messages

      if ($(".aicpa-class").hasClass("active")) {
        $("#block-user-alert-user-alert").hide();
      }
      else {
        $("#block-user-alert-user-alert").show();
      }

      /// Radial Elements

      var elements = [];
      var counter = 0;
      var elements_size = 0;
      var font_size = 16;

      $.each($('.radial-element'), function () {
        var id = $(this).attr('id');
        elements[id] = $(this).find(".element-value").html();
        elements_size = elements_size + 1;

        if ($(this).closest("li").length > 0) {
          font_size = 12;
        }
      });


      for (var i in elements) {
        if (window.location.pathname != "/dashboard") {
          var diameter_val = 56;
          var is_multi_graph = i.search('multi');
          /// Radial Elements Multiple Graphic
          if (is_multi_graph !== parseInt(-1)) {
            label_val = label_val.replace("-multi", "");
            var daemon = diameter_val / elements_size;
            diameter_val = diameter_val - (daemon * counter * 14);
            counter = counter + 1;
          }
          if (diameter_val < 56) {
            diameter_val = 56;
          }
        }
        else {
          var diameter_val = 44;
          var is_multi_graph = i.search('multi');
          /// Radial Elements Multiple Graphic
          if (is_multi_graph !== parseInt(-1)) {
            label_val = label_val.replace("-multi", "");
            var daemon = diameter_val / elements_size;
            diameter_val = diameter_val - (daemon * counter * 14);
            counter = counter + 1;
          }
          if (diameter_val < 44) {
            diameter_val = 44;
          }
        }

        var label_val = i.replace("_c", "");
        if (diameter_val < 30) {
          diameter_val = 30;
        }

        var label_val = i.replace("_c", "");
        label_val = i.replace("-CRAM", "");
        if (i == 'TBS' || i == 'drs') {
          diameter_val = 70;
        }
        if (window.location.pathname != "/dashboard") {
          radialProgress(document.getElementById(i))
            .label(label_val)
            .onClick(function () {
            })
            .diameter(diameter_val)
            .fontSize(font_size)
            .value(elements[i])
            .render();
        }
        else {
          radialProgressDashboard(document.getElementById(i))
            .label(label_val)
            .onClick(function () {
              // TO DO SOMETHING
            })
            .diameter(diameter_val)
            .fontSize(font_size)
            .value(elements[i])
            .render();
        }
      }

      //On load edit atrr text in My courses tabs

      if (window.location.pathname == "/dashboard/monitoring-center") {
        $.each($('#my-courses-tabs li svg'), function () {
          var item = $(this).find(".labels .label");
          var value = parseInt(item.html());
          var x = 30;
          if (value >= 100) {
            x = 25;
          }
          else {
            if (value >= 10) {
              x = 27;
            }
          }

          item.attr("y", 32);
          item.attr("x", x);
        });
      }
      else {
        $.each($('#my-courses-tabs li svg'), function () {
          var item = $(this).find(".labels .label");
          var value = parseInt(item.html());
          var x = 20;
          if (value >= 100) {
            x = 15;
          }
          else {
            if (value >= 10) {
              x = 17;
            }
          }

          item.attr("y", 32);
          item.attr("x", x);
        });

      }


      //    }
    }

  };
}(jQuery));
