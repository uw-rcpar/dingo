var tour;
(function ($, Drupal, window, document, undefined) {
  $(document).ready(function () {
    // Identify the ipq element in a variable since it is used in multiple places
    var ipqElement = ".exam-ipq-link:first";
    var tour_values = jQuery.parseJSON(Drupal.settings.rcpar_dasboard_tour_values);
    // Instantiate the tour
    tour = new Tour({
      template: function (count,step) {
        //first step
        if(count === 0 ){
            return "<div class='popover tour'>" +
          "<div class='arrow'></div>" +
            "<h3 class='popover-title'></h3>" +
            "<button type='button' class='close' aria-label='Close' data-role='end'><span aria-hidden='true'>&times;</span></button>" +
            "<div class='popover-content'></div>" +
            "<div class='popover-navigation'>" +
              "<button class='btn btn-default next start' data-role='next'>Start the tour</button>" +
            "</div>" +
        "</div>";
        }
        //between steps
        return step.next !== -1 ? "<div class='popover tour'>" +
        "<div class='arrow'></div>" +
          "<h3 class='popover-title'></h3>" +
          "<button type='button' class='close' aria-label='Close' data-role='end'><span aria-hidden='true'>&times;</span></button>" +
          "<div class='popover-content'></div>" +
          "<div class='popover-navigation'>" +
            "<button class='btn btn-default prev' data-role='prev'>Prev</button>" +
            "<button class='btn btn-default next' data-role='next'>Next</button>" +
          "</div>" +
      "</div>" :
      //last step
      "<div class='popover tour'>" +
        "<div class='arrow'></div>" +
          "<h3 class='popover-title'></h3>" +
          "<button type='button' class='close' aria-label='Close' data-role='end'><span aria-hidden='true'>&times;</span></button>" +
          "<div class='popover-content'></div>" +
          "<div class='popover-navigation'>" +
            "<button class='btn btn-default prev' data-role='prev'>Prev</button>" +
            "<button class='btn btn-default next' data-role='end'>Close</button>" +
          "</div>" +
      "</div>"
    },
      backdrop: true,
      storage:  window.localStorage, //check resetTour function
      steps: [
        {
          title: tour_values.rcpar_tour_title_1,
          content: tour_values.rcpar_tour_content_1,
          orphan: true,
          prev: -1,
          next: 1
        },
        {
          element: "#block-rcpar-dashboard-rcpar-dashboard-myexam-dates",
          title: tour_values.rcpar_tour_title_2,
          content: tour_values.rcpar_tour_content_2,
          backdropPadding: 4
        },
        {
          element: "#block-rcpar-dashboard-rcpar-dashboard-exam-rev-content",
          title: tour_values.rcpar_tour_title_3,
          content: tour_values.rcpar_tour_content_3,
          backdropPadding: 2
        },
        {
          element: ipqElement,
          title: tour_values.rcpar_tour_title_4,
          content: tour_values.rcpar_tour_content_4,
          backdropPadding: {top: 10, right: -60, left: 10},
          placement: "bottom"
        },
        {
          element: "#showRightPush",
          title: tour_values.rcpar_tour_title_5,
          content: tour_values.rcpar_tour_content_5,
          placement: "left",
          // If the user tries to click the element, it will dismiss the tour. Having both open
          // messes up the page display
          reflex: true

        }
      ],
      onShown: function (tour) {
        // Get data on the current step
        var i = tour.getCurrentStep();
        var step = tour.getStep(i);

        // Adjust the position of the popup on the IPQ link
        if (step.element == ipqElement) {
          $('.tour-tour').css('left', '-=31');
        }
        if(i === 1){
          $('.tour-tour-1').prev().css('z-index','1100');
          $('.tour-tour-1').prev().css('background','inherit');
        }
      },
      onEnd: function (tour) {
        var steps = tour._options.steps.length - 1;
        var current_step = tour.getCurrentStep();
        if(current_step !== steps){
          tourConfirmationButton({
            messageText: "Are you sure you want to exit the feature tour before having finished it? The tour won't be displayed again.",
            headerText: "Exiting Feature Tour",
            yesButtonText: 'OK',
            noButtonText: 'Cancel  ',
          }).done(function (e) {
           if(e === false){
              tour.init();
              tour.start(true);
           }else{
            endTour();
           }
          });
        }else{
          endTour();
        }
      }
    });

    if(Drupal.settings.rcpar_dashboard_user_tour_reset === "TRUE"){
      // Reset the tour
      resetTour();
    }
    if(Drupal.settings.rcpar_dashboard_user_tour_ended === "0"){
      // Initialize the tour
      tour.init();

      // Start the tour
      tour.start();
    }
  }); // END - document.ready

  //confirmation button on tour
  function tourConfirmationButton(options) {
    var deferredObject = $.Deferred();
    var defaults = {
      modalSize: 'modal-sm', //modal-sm, modal-lg
      okButtonText: 'Ok',
      cancelButtonText: 'Cancel',
      yesButtonText: 'Yes',
      noButtonText: 'No',
      headerText: 'Attention',
      messageText: 'Message',
    }
    $.extend(defaults, options);

    var _show = function () {
      $('BODY').append(
          '<div id="tourModal" class="modal fade tour-tour">' +
          '<div class="modal-dialog" class="' + defaults.modalSize + '">' +
          '<div class="modal-content">' +
          '<div id="tourModal-header" class="modal-header popover-title alert-info">' +
          '<button id="close-button" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>' +
          '<h4 id="tourModal-title" class="modal-title">Modal title</h4>' +
          '</div>' +
          '<div id="tourModal-body" class="modal-body popover-content">' +
          '<div id="tourModal-message" ></div>' +
          '</div>' +
          '<div id="tourModal-footer" class="modal-footer">' +
          '</div>' +
          '</div>' +
          '</div>' +
          '</div>'
          );

      $('#tourModal-title').text(defaults.headerText);
      $('#tourModal-message').html(defaults.messageText);

      var keyb = "false", backd = "static";
      var calbackParam = "";
      //set up the buttons
      var btnhtml = '<button id="ezok-btn" class="btn btn-primary next">' + defaults.yesButtonText + '</button>';
      if (defaults.noButtonText && defaults.noButtonText.length > 0) {
        btnhtml += '<button id="ezclose-btn" class="btn btn-default next" style="width: 84px">' + defaults.noButtonText + '</button>';
      }
      $('#tourModal-footer').html(btnhtml).on('click', 'button', function (e) {
        if (e.target.id === 'ezok-btn') {
          calbackParam = true;
          $('#tourModal').modal('hide');
        } else if (e.target.id === 'ezclose-btn') {
          calbackParam = false;
          $('#tourModal').modal('hide');
        }
      });

      $('#tourModal').modal({
        show: false,
        backdrop: backd,
        keyboard: keyb
      }).on('hidden.bs.modal', function (e) {
        $('#tourModal').remove();
        deferredObject.resolve(calbackParam);
      }).on('shown.bs.modal', function (e) {
        if ($('#prompt').length > 0) {
          $('#prompt').focus();
        }
      }).modal('show');
    }

    _show();
    return deferredObject.promise();
  }

  // will show the user the local storage again
  // on local storage we will be saving two variables
  // tour_current_step integer
  // tour_end boolean
  function resetTour(){
    window.localStorage.removeItem('tour_current_step');
    window.localStorage.removeItem('tour_end');
  }

  //We set the global variable of the user so we never run this
  //tour again.
  function endTour() {
    // Get the user's token
    var jwt = RCPAR.jwt.getJwt();
    // Record modal tour as seen in database
    RCPAR.modals.setModalData('dashboard_tour', jwt);
  }

  $.fn.refreshTour = function (data){
    if(typeof tour != 'undefined'){
      tour.goTo(tour.getCurrentStep());
    }
  };

})(jQuery, Drupal, this, this.document); // END - Closure