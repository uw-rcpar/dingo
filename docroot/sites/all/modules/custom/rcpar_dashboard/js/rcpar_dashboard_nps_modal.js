(function ($) {
  $(document).ready(function () {
    // Show the NPS form modal.
    $('#rcpar-dashboard-nps').modal('show');

    // Get the user's token
    var jwt = RCPAR.jwt.getJwt();

    // Record modal as seen in modal table in database
    RCPAR.modals.setModalData('nps_modal', jwt);

    // Disable submit button until we have a choice selected
    $("#rcpar-dashboard-nps-form .form-submit").prop("disabled",true);

    // If a score is chosen we can enable the submit button
    $("#rcpar-dashboard-nps-form input:radio").change(function () {
      $("#rcpar-dashboard-nps-form .form-submit").prop("disabled", false);
    });

    // Display to user the number of characters they have typed (the textarea is limited to 300).
    $("#rcpar-dashboard-nps-form .form-textarea").on('keyup', function(e) {
      var chars = $.trim(this.value).length ? $.trim(this.value).length : 0
      $('#rcpar-dashboard-nps-form .help-block .count').text(chars);
    });
  });

}(jQuery));
