(function($) {

  $(document).ready(function () { 
    // toggle when edit or add clicked
    $(".edit").on("click", function (e) {
      cr = $(this).attr("cr");
      $("#edit-"+cr+"-exam-date").toggle();
    });
    // close field after update
    $.fn.resetExamDateField = function(data) {
      $("#edit-"+data).toggle();
    };
  });

}(jQuery));