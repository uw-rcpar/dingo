(function($) {
    Drupal.behaviors.rcpar_courseware = {
        attach: function(context, settings) {
            if (context == document) {
                var sku = settings.rcpar_courseware.sku;
                var section = settings.rcpar_courseware.section;
                section = section.replace("Intro", '0');
                var sub_section = settings.rcpar_courseware.sub_section;
                sub_section = sub_section.replace(".", "-");

                $(".tab-content-" + sku).ready(function() {
                    $(".tab-content-" + sku).find("." + section).trigger("click");
                    if (sub_section) {
                        $("." + sku + "-" + sub_section).addClass("active-topic");
                    }

                });

                $(window).bind("load", function() {
                    if (sub_section) {
                        var top = $("." + sku + "-" + sub_section).offset().top;
                        var val = top;

                        $('body').animate({
                            scrollTop: val - 100
                        }, 2000);
                    }
                });


            }
        }
    };
}(jQuery));