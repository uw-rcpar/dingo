(function ($) {

  Drupal.behaviors.rcpar_dashboard_exam_version_sel = {

    examVersionSelHandler: null,

    // The idea is to only have one of this objects instantiated at the same time
    // (singleton pattern)
    getExamVerSelHandler: function () {
      if (!this.examVersionSelHandler) {
        this.examVersionSelHandler = new ExamVerSelectionModalHandler(
          this, // Parent object
          '#exam-version-selection' // Element containing the modal HTML
        );
      }
      return this.examVersionSelHandler;
    },

    openExamVersionSelection: function(section) {
      var modalsHandler = this.getExamVerSelHandler();
      var cv = Drupal.settings.rcpar_dashboard_ev.previous_data_info[section].current_exam_version;
      var have2016Data = Drupal.settings.rcpar_dashboard_ev.previous_data_info[section].have_data;
      var selectedTextBook = Drupal.settings.rcpar_dashboard_ev.previous_data_info[section].selected_textbook;
      modalsHandler.setSection(section, cv, have2016Data, selectedTextBook);
      modalsHandler.showModals();
    },

    attach: function (context, settings) {
      var behavioursObj = this;
      $('.open-exam-version-modal', context).click(function () {
        var section = $(this).attr('data-section');
        behavioursObj.openExamVersionSelection(section);
        return false;
      });

      // We might have been redirected here after we tried to access to a section that require that we set the
      // exam version for a particular version.
      // If that's the case, we open the exam selection modal

      if (Drupal.settings.rcpar_dashboard_ev.force_selection){
        var section = Drupal.settings.rcpar_dashboard_ev.force_selection;
        behavioursObj.openExamVersionSelection(section);
        behavioursObj.getExamVerSelHandler().setDestination(Drupal.settings.rcpar_dashboard_ev.redirect_after_select);

        // We set the variable to null to avoid the dialog to be automatically opened more than once
        Drupal.settings.rcpar_dashboard_ev.force_selection = null;
      }
    }
  };

  var ExamVerSelectionModalHandler = function (parentObj, modalSelector) {
    this.current_step = null;
    this.modalSelector = modalSelector;
    this.redirectTo = null;

    // Default values
    this.section = 'AUD';
    this.currentVersion = '2016';
    this.has2016Data = true;
    this.selectedTextBook = '2016';

    this.stepList = ['exam-changes-info', 'confirmation-have-data', 'confirmation-no-data'];

    /// Class method declarations
    /**
     * This method is going to be executed at the end of the constructor code
     */
    this.init = function () {
      // We set the handlers to the UI events that we need to respond here:
      var that = this;
      // We just need to do this once, but init method can be called several times
      $(modalSelector).once(function () {
        $('#wait-tell-me-more', modalSelector).click(function () {
          // This button just opens a new tab with information
          window.open(Drupal.settings.basePath + 'cpa-exam/changes', '_blank');
          return false;
        });

        $('.next-step', modalSelector).click(function () {
          that.nextStep();
          return false;
        });
      });

      // Other stuff

      // If the user have some 2016 data for this section, we preselect the 2016 option on the
      // exam-changes-info state

      // destination after finish have to be set everytime the dialog is reseted
      this.redirectTo = null;

      if (this.currentVersion) {
        $('input[name="exam-version"]').val([this.currentVersion]);
      }

      $('.selected-textbook-info', this.modalSelector).hide();

      if (this.selectedTextBook) {
        $('.selected-textbook-' + this.selectedTextBook, this.modalSelector).show();
      }

      $('.spinning', modalSelector).hide();
      $('.section-name', modalSelector).html(this.section);
      $('.exam-version-selection-step', this.modalSelector).hide();
      this.showStep('exam-changes-info');
    };

    /**
     * Use this function to set a url to be redirected after the exam version is set
     * @param destination
     */
    this.setDestination = function(destination){
      this.redirectTo = destination;
    };

    /**
     * This function updates the modal settings and reset it's status to it's initial state
     * @param section
     * - Section that is going to be configured
     * @param currentVersion
     * - User's current exam version
     * @param has2016Data
     * - Indicates if the user currently have data (in case it's using 2016 exam version)
     * @param selectedTextBook
     * - Indicates which textbook version (2016/2017) the user had selected when checking out the order
     */
    this.setSection = function (section, currentVersion, has2016Data, selectedTextBook) {
      this.section = section;
      this.currentVersion = currentVersion;
      this.has2016Data = has2016Data;
      this.selectedTextBook = selectedTextBook;
      this.init();
    };

    /**
     * Handles all the logic required to start the exam mid point break (show dialog, pause timer, etc)
     */
    this.showModals = function () {
      // Before we show the modal, we reset it's status to the first step
      this.showStep('exam-changes-info');

      $(modalSelector).modal('show');
    };

    /**
     * Show a particular step of the Dialog Modals
     * @param stepName
     * - Step name (string, must be one of the values on this.stepList)
     */
    this.showStep = function (stepName) {
      this.current_step = stepName;

      // On the confirmation-have-data we need to have a special class on the modal container itself
      // (and only on that step)
      if (stepName == 'confirmation-have-data'){
        $(this.modalSelector).addClass('confirmation-have-data');
      }
      else {
        $(this.modalSelector).removeClass('confirmation-have-data');
      }

      $('.exam-version-selection-step', this.modalSelector).hide();
      $('#step-' + stepName, this.modalSelector).show();
    };

    /**
     * Use this function to react to the finished ajax call to exam version update
     */
    this.courseUpdated = function () {
      // After we update some information, we just redirect the user or reload the page
      if(this.redirectTo){
        window.location = Drupal.settings.basePath + this.redirectTo;
      }
      else {
        document.location=document.location;
      }
    };

    /**
     * Saves (using ajax) the information for the new version of the current section
     * @param newVersion
     */
    this.saveVersion = function (newVersion) {
      var that = this;
      // Add some visuals to let the users know that we are working
      $('button.next-step .spinning', modalSelector).show();

      // As we are going to send an ajax call to the server and as that ajax call
      // can't be canceled by the user, we disable the modal controls and prevent
      // it to be manually closed
      $('button', modalSelector).attr('disabled', 'disabled');
      $('input[type=radio]', modalSelector).attr('disabled', 'disabled');
      $(modalSelector).data('bs.modal').options.keyboard = false;
      $(modalSelector).data('bs.modal').options.backdrop = 'static';

      // Here we request the server to do the exam version change
      var changeApiUrl =
        Drupal.settings.basePath +
        'courseware-services/update-course-version/';
      var parameters = this.section + '/' + newVersion;

      $.ajax({
        url: changeApiUrl + parameters,
      }).done(function () {
        that.courseUpdated();
      });
    };

    /**
     * Advance the Mid Point Dialog to the next step
     */
    this.nextStep = function () {
      var next_step = null;
      switch (this.current_step) {
        case 'exam-changes-info':
          var userSelectedVersion = $('input[name="exam-version"]:checked').val();
          if (userSelectedVersion == '2016') {
            this.saveVersion(userSelectedVersion);
          }
          else {
            if (this.has2016Data) {
              next_step = 'confirmation-have-data';
            }
            else {
              next_step = 'confirmation-no-data';
            }
          }
          break;
        case 'confirmation-have-data':
        case 'confirmation-no-data':
          var userSelectedVersion = $('input[name="exam-version"]:checked').val();
          this.saveVersion(userSelectedVersion);
          break;
      }
      if (next_step) {
        $(modalSelector).addClass(next_step);
        this.showStep(next_step);
        return true;
      }
      else {
        // We where not able to advance to the next step (validation errors)
        return false;
      }
    };

    /////////// - End of Class methods declaration

    // Let's initialize the object
    this.init();
  }

}(jQuery));
