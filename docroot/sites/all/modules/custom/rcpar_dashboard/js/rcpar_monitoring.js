(function ($) {
    Drupal.behaviors.rcpar_monitoring = {
        attach: function (context, settings) {

        // Hover effect in monitoring center
			 $("body").on('mouseenter', '.group-wrapper', function() {
			    $('.group-wrapper-overlay').hide();
			    $('.' + this.id + '-overlay').show();
			}).on('mouseleave', '.group-wrapper-overlay', function() {
			    $('.group-wrapper-overlay').hide();
			});

        
            $('.page-dashboard-monitoring-center #myTabDrop1-contents a').click(function(){              
                $('.page-dashboard-monitoring-center #myTabDrop1-contents li').removeClass('active');
                $(this).parent().addClass('active');
            });
            
        }
    };
}(jQuery));