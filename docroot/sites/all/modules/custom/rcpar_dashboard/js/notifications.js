var RCPARNotifications = (function ($) {
  // locally scoped Object
  var self = {};

  // Local collection of notifications
  var notifications = [];

  // The outer wrapper of the notifications area
  var $wrapper;

  // The div containing the individual notifications
  var $notifsContainer;

  /**
   * Initialize the notifications markup structure
   */
  self.setup = function () {
    // Create the notifications container
    var containerHtml =
      '<div id="notifs-container">\n' +
      '  <div class="notifications-header row">\n' +
      '    <div id="notif-close-x">\n' +
      '      <img src="/sites/all/themes/bootstrap_rcpar/css/img/notif-close-x.svg" style="width: 21px; height: 21px;"/>\n' +
      '    </div>\n' +
      '    <div class="notif-heading">Notifications</div>\n' +
      '  </div>\n' +
      '  <div id="notifs">' +
      '    <div class="notif" style="padding-bottom: 70px"><div class="content-wrapper"><div class="title" style="padding-left: 70px">You don’t have any notifications at the moment.</div></div></div>' +
      '  </div>' +
      '</div>';
    $wrapper = $(containerHtml);
    $wrapper.appendTo('#cbp-spmenu-notif');

    $notifsContainer = $wrapper.find('#notifs');

    // Make the toggle icon visible
    $('#showNotifications').removeClass('hidden');
  };

  /**
   * Open or close the notification panel so that notifications are visible/hidden
   */
  self.openOrClosePanel = function () {
    $('body').toggleClass('cbp-spmenu-push-toleft');
    $('#cbp-spmenu-notif').toggleClass('cbp-spmenu-open');
    $('.overlay-container').toggle();
  };

  /**
   * Returns true if the notification panel is open
   * @returns bool
   */
  self.isPanelOpen = function() {
    return $('#cbp-spmenu-notif').hasClass('cbp-spmenu-open');
  };


  /**
   * Open or close the notification body
   */
  self.openOrCloseNotif = function (id) {
    self.getNotifById(id).getContainer().toggleClass('open');
  };

  /**
   * Returns true if any of the notifications in the current collection are unread
   * @returns {boolean}
   */
  self.hasUnread = function () {
    var hasUnread = false;
    $.each(notifications, function () {
      if (this.isRead == 0) {
        hasUnread = true;
      }
    });

    return hasUnread;
  };

  /**
   * Notification sub-class
   */
  function Notification() {
    this.id;
    this.isRead;
    this.isExpanded = false;
    this.date;
    this.dateFormatted;
    this.title;
    this.teaser; // todo - derive from body, maybe only if absent
    this.body;
    this.type;

    /**
     * Gets the jQuery wrapper for this notification
     * @returns {*|HTMLElement}
     */
    this.getContainer = function() {
      return $('#notification-' + this.id);
    }
  }

  /**
   * Get a notification container by ID
   * @param id
   * @returns {Notification}
   */
  self.getNotifById = function(id) {
    var ret;

    $.each(notifications, function () {
      if(id == this.id) {
        ret = this;
      }
    });
    return ret;
  };


  /**
   * Append a notification container into the panel
   * @param {Notification} notif
   */
  function renderNotification(notif) {
    var $n = $('<div class="notif" id="notification-' + notif.id + '"></div>');
    $n.attr('data-id', notif.id);
    $n.attr('data-isRead', notif.isRead);
    if (notif.isRead == 1) {
      $n.addClass('read');
    }
    var html = '<div class="date">' + notif.dateFormatted + '</div>';
    html = html + '<div class="row notif-content clearfix">';
    html = html + '<div class="expand unread"><i class="fa fa-circle"></i></div>';
    html = html + '<div class="expand closed"><i class="fa fa-angle-right"></i></div>';
    html = html + '<div class="expand open"><i class="fa fa-angle-down"></i></div>';
    html = html + '<div class="content-wrapper">';
    html = html + '<div class="title">' + notif.title + '</div>';
    // html = html + '<div class="teaser">' + notif.body.split(" ").splice(0,6).join(" ") + '...</div>';
    html = html + '<div class="teaser">' + notif.teaser + '</div>';
    html = html + '<div class="body">' + notif.body + '</div>';
    html = html + '</div><!-- / content-wrapper-->';
    html = html + '</div><!-- / row-->';
    $n.append(html);
    return $n;
  }

  /**
   * Query for notifications from the API, add them to the local collection, and render them.
   *
   * @param {string} token
   * - JWT API token
   * @param callback
   * - A function to execute after retrieval, whether successful or failed.
   */
  self.retrieveNotifications = function (token, callback) {
    // Query the API
    $.ajax({
      type: 'GET',
      url: '/api/v2/user/notifications/get/all?jwt=' + token,
      data: {},
      success: function (response) {

        try {
          if (response.code == 200) {
            // Don't render if there are no notifications retrieved
            if(response.notifications.length == 0) {
              return;
            }

            // Clear existing notifications
            notifications = [];

            // Create a collection of Notification objects from the retrieved data
            $.each(response.notifications, function () {
              var n = new Notification();
              n.id = this.id;
              n.body = this.message;
              n.teaser = this.teaser;
              n.title = this.title;
              n.type = this.type;
              n.date = this.created;
              n.dateFormatted = this.created_formatted;
              n.isRead = this.is_read;

              // Add to the object collection
              notifications.push(n);
            });

            self.renderNotifications();

            if(callback) {
              callback();
            }
          }
          // API error
          else {
            // console.log('API error, non 200 code');
          }
        }
        catch (e) {
          // console.log('Caught an error during API request.');
        }

      },
      error: function () {
        if(callback) {
          callback();
        }
      }
    });
  };

  /**
   * Mark a notification as read via the API
   * @param token
   * @param id
   * - Notification ID
   * @param callback
   */
  self.markRead = function (token, id, callback) {
    var notif = self.getNotifById(id);

    // If it's already been marked read, nothing to do
    if(notif.isRead == 1) {
      return;
    }

    // Update the notification object and its container
    notif.isRead = 1;
    notif.getContainer().addClass('read');

    // Query the API
    $.ajax({
      type: 'POST',
      url: '/api/v2/user/notifications/read?jwt=' + token,
      data: {id:id},
      success: function (response) {

        try {
          if (response.code == 200) {
            // Nothing really to do here - we assume success and don't do anything
            // to handle an error. The user might just have to click again later,
            // which is annoying, but no big deal.
            if(callback) {
              callback();
            }
          }
          // API error
          else {
            // console.log('API error, non 200 code');
          }
        }
        catch (e) {
          // console.log('Caught an error during API request.');
        }
      },
      error: function () {
        if(callback) {
          callback();
        }
      }
    });
  };

  /**
   * Render the current collection of Notification objects into the HTML page.
   */
  self.renderNotifications = function () {
    // Wipe existing display
    $notifsContainer.html('');

    // Sort notifications by date
    notifications = notifications.sort(function (a, b) {
      return b.date - a.date;
    });

    for(let i in notifications) {
      // Render notification and append to container
      var $render = renderNotification(notifications[i]);
      $notifsContainer.append($render);
    }

    /*// Iterate through all notifications
    $.each(notifications, function () {
      // Render notification and append to container
      var $render = renderNotification(this);
      $notifsContainer.append($render);
    });*/
  };

  return self;
})(jQuery);


(function ($, Drupal, window, document, undefined) {

  $(document).ready(function () {
    var jwtToken = RCPAR.jwt.getJwt();

    // Setup the container of notifications and retrieve notifications
    RCPARNotifications.setup();
    RCPARNotifications.retrieveNotifications(jwtToken, notifIconHandler);

    // Bind to notification icon's click event
    $('#showNotifications').click(RCPARNotifications.openOrClosePanel);

    // Bind to notification panel's "X" icon
    $('#notif-close-x').click(RCPARNotifications.openOrClosePanel);

    // Open Notification to read it.
    $(document).on("click", '.expand', function(event) {
      var notifId = $(this).closest('.notif').attr('data-id');
      RCPARNotifications.openOrCloseNotif(notifId);
      RCPARNotifications.markRead(jwtToken, notifId); // todo - could optimize by only sending if not already read
      notifIconHandler();
    });

    /**
     * Marks the notification toggler icon with or without a dot to indicate
     * whether there are unread messages
     */
    function notifIconHandler() {
      if(RCPARNotifications.hasUnread()) {
        $('.navbar-toggle-notif').addClass('active');
      }
      else {
        $('.navbar-toggle-notif').removeClass('active');
      }
    }

    // Poll for new notifications every x seconds (10 sec. by default)
    if(Drupal.settings.rcpar_notifications_poll_time > 0) {
      var notificationPoll = setInterval(function () {
        // Don't check for notifications while the panel is open, that might be disruptive.
        if(!RCPARNotifications.isPanelOpen()) {
          RCPARNotifications.retrieveNotifications(jwtToken, notifIconHandler);
        }
      }, Drupal.settings.rcpar_notifications_poll_time);
    }

  }); //END - document.ready
})(jQuery, Drupal, this, this.document); //END - Closure