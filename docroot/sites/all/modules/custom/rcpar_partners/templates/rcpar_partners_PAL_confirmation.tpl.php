<div class="email-logo"><a href="[site:login-url]"><img src="https://www.rogercpareview.com/sites/default/files/rcpar-logo_0.png" /></a></div>

<h2 style="border-top: 1px solid #ffa300;
    color: #28a9e0;
    font-size: 14px;
    font-weight: bold;
    letter-spacing: 2px;
    margin-top: 40px;
    padding-top: 30px;
    text-transform: uppercase;
     margin-bottom: 10px;">Enrolment Confirmation</h2>

<p style="color: #666; font-size: 16px;">Thank you for enrolling in a Roger CPA Review course!</p>

<?php print $product_info ?>

<p style="color: #666; font-size: 13px;">If you would like to request any changes, please contact our <a href="<?php global $base_url; print $base_url; ?>/contact">Customer Service</a> department as soon as possible.</p>

<p style="color: #666; font-size: 13px;"><b>Email address:</b> staff@rogercpareview.com</p>

<p style="color: #666; font-size: 13px;"><b>Confirmation Number:</b>
<?php print $order_id ?> </p>

<p style="color: #666; font-size: 13px;"><b>Partner:</b>
<?php print $partner ?> </p>



