/**
 * @file - Added in rcpar_partners_form_alter() only for Partner node form
 */

(function ($) {
  Drupal.behaviors.rcpar_partners = {

    // This function is a little hack that we are doing to the DOM
    // we want that the field-course-sections cbs appears under "Per Part Information" when
    // the billing type is not freetrial and under "Partner Information" otherwise
    positionCourseSection: function () {
      var $billingType = $("#edit-field-billing-type-und");
      var cbs = null;
      cbs = $('#edit-field-course-sections').detach();
      if ($billingType.val() == 'freetrial') {
        cbs.insertAfter("#edit-field-fixed-expiration-date");
        // Show the FULL option
        cbs.find('.form-item-field-course-sections-und-FULL').show();
      }
      else {
        cbs.insertAfter("#edit-field-per-part-pricing");
        // Hide the FULL option
        cbs.find('.form-item-field-course-sections-und-FULL').hide();
      }
      // On some cases, our manipulation makes the "Per Part Information" appears visible
      // even if it doesn't have anything on it
      if ($('.group-per-part-information .fieldset-wrapper').height() == 0) {
        $('.group-per-part-information').hide();
      }
    },

    attach: function (context) {
      var behaviors = this;

      // When 'billing type' changes, reposition course section
      $("#edit-field-billing-type-und").on('change', function() {
        behaviors.positionCourseSection();
      });

      // When a 'partner type' of 'normal' is chosen (option value is actually 'freetrial'), only restrict 'billing type'
      // options to 'free access' only.
      $("#edit-field-partner-type-und", context).on('change', function() {
        var $billingType = $("#edit-field-billing-type-und");
        // Check if "Normal" (2) is chosen
        if($(this).val() == '2') { // todo - I don't really like hardcoding the value here, maybe the backend should expose this
          // Start by setting all billing type options to be disabled
          $billingType.find('option').attr('disabled', 'disabled');

          // Now enable the option labled "Free Access" (value is actually 'free trial')
          $billingType.find('option[value="freetrial"]').removeAttr('disabled');

          // Make the selected option be "Free Access"
          $billingType.val('freetrial');
        }
        // If another value is chosen, enable all options
        else {
          $billingType.find('option').removeAttr('disabled');
        }
      });
      
     //field User-Selected Course, dependencie handling
     $('input[id^=edit-field-user-selected-course]').click(function () {       
        if ($(this).attr('checked')) {
          $('#edit-field-course-sections-und-full').attr('checked',false).attr('disabled',true).hide().next().hide();
        }
        else{
          $('#edit-field-course-sections-und-full').attr('checked',true).attr('disabled',false).show().next().show();
        }
      });

      // Hide Unlimited Access checkbox for all but Direct Bill and Credit Card billing types.
      $('.form-item-field-bundle-package-und-unlimitedaccess').hide();
      // Initial page load.
      if ($('#edit-field-billing-type-und').val() == 'creditcard' || $('#edit-field-billing-type-und').val() == 'directbill') {
        $('.form-item-field-bundle-package-und-unlimitedaccess').show();
      }
      // When billing type drop down is changed.
      $('#edit-field-billing-type-und').change(function() {
        $('.form-item-field-bundle-package-und-unlimitedaccess').hide();
        if ($('#edit-field-billing-type-und').val() == 'creditcard' || $('#edit-field-billing-type-und').val() == 'directbill') {
          $('.form-item-field-bundle-package-und-unlimitedaccess').show();
        }
      });

      // Field bundle package (What would you like to offer on your bundle package?) checkboxes:
      // Unlimited Access checkbox should only be enabled if certain checkboxes are checked.
      function validateCheckboxes() {
        // These are the IDs of the checkboxes that must be checked for Unlimited Access to be enabled.
        var checkboxesToValidate = [
          'edit-field-bundle-package-und-exam',
          'edit-field-bundle-package-und-cram',
          'edit-field-bundle-package-und-audio',
          'edit-field-bundle-package-und-mobile',
          'edit-field-bundle-package-und-flash',
        ];
        var totalCheckboxes = checkboxesToValidate.length;
        var countCheckboxes = 0;
        // Count how many of the necessary checkboxes are checked.
        $.each(checkboxesToValidate, function(index, value) {
          if ($('#' + value).is(':checked')) {
            countCheckboxes++;
          }
        });
        // If they are all checked, enable the Unlimited Access checkbox.
        if (totalCheckboxes == countCheckboxes) {
          $('#edit-field-bundle-package-und-unlimitedaccess').attr('enabled', true).attr('disabled', false);
        }
        // Otherwise make sure the Unlimited Access checkbox is unchecked and disable it.
        else {
          $('#edit-field-bundle-package-und-unlimitedaccess').prop('checked', false).attr('disabled', true).attr('enabled', false);
        }
      }
      // Validate checkboxes on page load.
      validateCheckboxes();
      // Validate checkboxes when one is changed.
      $('#edit-field-bundle-package-und .form-checkbox').change(function() {
        validateCheckboxes();
      });

    }
  };

  // Trigger the change event for 'partner type' and 'billing type' so that our partner/billing type behaviors initialize on page load
  $(document).ready(function(){
    $("#edit-field-partner-type-und").trigger('change');
    $("#edit-field-billing-type-und").trigger('change');
  })
})(jQuery);
