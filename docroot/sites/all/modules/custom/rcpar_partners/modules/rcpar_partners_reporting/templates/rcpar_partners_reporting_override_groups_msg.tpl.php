<div clas="partner-override-msg" style="border-radius: 4px; margin-top: 10px; margin-bottom: 10px; padding: 5px 15px; background-color: #fcf8e3; border-color: #faebcc; color: #c09853;">
    <h3>IMPORTANT:</h3>
    <h5>Course Expiration Overridden</h5>
    <p>The default expirations have been overridden by one or many group configurations. Please ensure each override is correct before proceeding. The users course will expire based on the group "end dates" that are set up if "override default expiration" is selected.</p>
</div>

