<!-- Modal -->
<div class="modal fade" id="email-deletion" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php print $msg_title?></h4>
      </div>
      <div class="modal-body">
        <p>
           <?php print $message; ?> 
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>     
        <a class="use-ajax" href= <?php print $action_link?>>
            <button type="button" class="btn btn-primary" >Proceed </button>
        </a>
      </div>
    </div>
  </div>
</div>