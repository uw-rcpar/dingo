<?php

function course_tree_restriction_clone_form($form, $form_state) {
    //Get versions
    $exam_versions = exam_versions_years();

    $options = array();
    foreach ($exam_versions as $year => $value) {
        $options[$value] = $year;
    }

    $new_version = exam_versions_get_latest_version();
    $previous_version = exam_versions_previous_version($new_version->name);

    $form['version_from'] = array(
        '#title' => 'From',
        '#type' => 'select',
        '#default_value' => $previous_version->tid,
        '#options' => $options,
    );

    $form['version_to'] = array(
        '#title' => t('To'),
        '#type' => 'select',
        '#default_value' => $new_version->tid,        
        '#options' => $options,
    );

    $form['actions']['clone'] = array(
        '#type' => 'submit',
        '#value' => t('Start Copy Process'),
        '#submit' => array('course_tree_clone_restrictions'),
    );

    return $form;
}

/**
 * Clean the new version partner restrictions and copy them  
 * from old version
 */
function course_tree_clone_restrictions($form, $form_state){
    $operations = array();
    $version_from = $form_state['values']['version_from'];
    $version_to = $form_state['values']['version_to'];

    //Clean rows with the new version
    db_delete('field_data_field_flag_restrict')
    ->condition('bundle', 'partners')
    ->condition('field_flag_restrict_exam_version', $version_to)
    ->execute();

    //Select partners with restrictions
    $query = db_select('node', 'n');
    $query->distinct();
    $query->join('field_data_field_flag_restrict', 'f', 'f.entity_id = n.nid');
    $query->fields('n', array('nid'));
    $query->condition('n.type', 'partners');
    $query->condition('f.field_flag_restrict_exam_version', $version_from);
    $partners = $query->execute()->fetchAll();

    foreach ($partners as $key => $record) {
        $operations[] = array('course_tree_clone_restrictions_batch' , array(
                $version_from, 
                $version_to, 
                $record->nid
            )
        );
    }

    // Batch 
    $batch = array(
        'operations' => $operations,
        'finished' => 'course_tree_clone_restrictions_finish',
        'title' => t('Cloning Partners Restrictions'),
        'init_message' => t('Inicializing Processing Dashboard.'),
        'progress_message' => t("Processed @current out of @total rows."),
        'error_message' => t('Error'),
        'file' => drupal_get_path('module', 'course_tree') . '/includes/course_tree_restrictions_clone.inc',
    );

    batch_set($batch);
}

/**
 * Custom batch process to copy the partner restrictions to the new version 
 */ 
function course_tree_clone_restrictions_batch($version_from, $version_to, $nid, &$context){
    $context['message'] = "Copying Restrictions of Partner : $nid";

    //Load node and restrictions
    $node = node_load($nid);
    $restrictions = $node->field_flag_restrict['und'];

    //Clone the old version restrictions to the new version
    foreach ($restrictions as $delta => $restriction) {
        $new_version_restriction = array(
            'course' => $restriction['course'],
            'chapter' => $restriction['chapter'],
            'topic' => $restriction['topic'],
            'exam_version' => $version_to
        );
        $node->field_flag_restrict['und'][] = $new_version_restriction;
    }

    node_save($node);
}

/**
 * 
 * @param type $success false if one of the operations fail
 * @param type $results has a insert array of values to create on the table stats
 * @param type $operations
 * 
 */
function course_tree_clone_restrictions_finish($success, $results, $operations) {  
    if ($success) {
        drupal_flush_all_caches();    
        drupal_set_message("Copy Process Complete Succesfully");
    } else {
        // An error occurred.
        // $operations contains the operations that remained unprocessed.
        $error_operation = reset($operations);
        drupal_set_message(
            t('An error occurred while processing @operation with arguments : @args', array(
                '@operation' => $error_operation[0],
                '@args' => print_r($error_operation[0], TRUE))
            ), 'error'
        );
    }
}
