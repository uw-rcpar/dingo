(function ($) {

  function courseTreeRecalculateValues(course_tree){

    window.loadedRecords = [];
    // We should have one of those per exam version term
    $('.course-tree', course_tree).each(function(index, el){
      var exam_version = $(el).attr('id').replace('tree-','');

      window.loadedRecords.push(exam_version + '#');
      // Now we need to get the list of selected courses

      $('li.level-0', el).each(function(course_index, course_li){
        var course_cb = $(course_li).children('input[type=checkbox]')
        if (course_cb.attr('checked')){
          window.loadedRecords.push($(course_cb).val());

          // Now we check for the chapters of the selected course
          $('li.level-1', course_li).each(function(chapter_index, chapter_li){
            var chapter_cb = $(chapter_li).children('input[type=checkbox]')
            if (chapter_cb.attr('checked')){
              window.loadedRecords.push($(chapter_cb).val());

              // Now the topics
              $('li.level-2', chapter_li).each(function(topic_index, topic_li){
                var topic_cb = $(topic_li).children('input[type=checkbox]')
                if (topic_cb.attr('checked')){
                  window.loadedRecords.push($(topic_cb).val());
                }
              });
            }
          });
        }
      });
    });
    $('input[type=hidden]', course_tree).attr('value', window.loadedRecords);
  }

  $(document).ready(function () {

    $('.course-tree a').click(function (e) {
      $(this).parent().parent().find("ul").toggle("slow");
      $(this).parent().parent().toggleClass("collapsed");
      e.preventDefault();
    });

    $(document).on('click', '.course-tree input[type=checkbox]', function (e) {
      var parent = $(this).attr('checked');
      if (typeof parent !== 'undefined') {
        $(this).parent().parent().prev().prev().attr('checked', 'checked').parent().parent().prev().prev().attr('checked', 'checked');
      }
      $(this).parent().find('input[type=checkbox]').each(function () {
        if (typeof parent === 'undefined') {
          $(this).removeAttr('checked');
        }
        else {
          $(this).attr('checked', 'checked');
        }
      });

      courseTreeRecalculateValues($(this).closest('.field-type-course-tree'));

    });
    // show and hide tree tabs
    $('.tree-tab').click(function (e) {
      // remove active class from tab
      $('.tree-tab').removeClass('tab-active');
      // hide the currently shown course tree
      $('.course-tree').hide();
      // add active class to tab clicked on
      $('#' + $(this).attr('id')).addClass('tab-active');
      // show correct course tree
      $('.' + $(this).attr('id')).show();
    });

  });

})(jQuery);