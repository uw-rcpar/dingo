<?php


/**
 *
 * @param <array> $data(
 * ** uid
 * ** entitlement_id
 * ** partner_id
 * ** sku
 * ** uid
 * )
 *
 */
function rcpar_partners_notifications_push($data){
    $data['sended'] = 0;
    $data['timestamp'] = REQUEST_TIME;
    drupal_write_record ("partners_emails_queue", $data);
}

/**
 *
* @param <array> $data(
 * ** uid
 * ** entitlement_id
 * ** partner_id
 * ** sku
 * ** uid
 * )
 * @return boolean true if it email was send it
 */
function rcpar_partners_notifications_sended($data){
  $return = db_select('partners_emails_queue', 'q')
  ->fields('q')
  ->condition('uid', $data['uid'])
  ->condition('entitlement_id', $data['entitlement_id'])
  ->condition('partner_id', $data['partner_id'])
  ->condition('sku', $data['sku'])
  ->condition('sended',1)
  ->countQuery()
  ->execute()
  ->fetchField();

  return $return;
}

function rcpar_partners_notifications_queued(){
  $return = db_select('partners_emails_queue', 'q')
  ->fields('q')
  ->condition('sended',0)
  ->execute();
  return $return;
}

function rcpar_partners_mark_notification_as_sended($item){
  $num_updated = db_update('partners_emails_queue')
    ->fields(array('sended' => 1))
    ->condition('id', $item->id, '=')
    ->execute();
}

function rcpar_partners_get_user_notification_for_entitlement($uid, $entitlement_id){
  $return = db_select('partners_emails_queue', 'q')
    ->fields('q')
    ->condition('uid', $uid, '=')
    ->condition('entitlement_id', $entitlement_id, '=')
    ->execute()
    ->fetchAssoc();
  return $return;
}

function rcpar_partners_send_notification($data){
  $params = array(
    "id" => $data->id,
    "uid" => $data->uid,
    "entitlement_id" => $data->entitlement_id,
    "sku" => $data->sku,
    "partner_id" => $data->partner_id,
    "percentage" => $data->percentage,
  );

  $partner = node_load($data->partner_id);
  try {

    $partner_wrapper = entity_metadata_wrapper("node", $partner);
    $u = $partner_wrapper->field_user_account->value();

    $to = $u->mail;
    if ($to) {
      $from = variable_get('site_mail', "");
      // Disabled for DIN-640 Replace Ultimate Cron with Acquia Solution
      // Cron job is sending broken emails
      return ;
      $result = drupal_mail('rcpar_partners', 'notification', $to, language_default(), $params, $from, true);
      if ($result['result'] != TRUE) {
        watchdog('rcpar_partners', t('There was a problem sending a notification email.'), array(), WATCHDOG_ERROR);
      }
      else {
        rcpar_partners_mark_notification_as_sended($data);
      }
    }
  }
  catch (EntityMetadataWrapperException $ex) {
    watchdog('rcpar_partners', t("There was a problem sending a notification email: " . $ex->getMessage()), array(), WATCHDOG_ERROR);
  }
}


function rcpar_partners_notifications_get_partner_object($partner_id){

}

/**
 * Implement hook_node_update()
 */
function rcpar_partners_entity_update($entity, $type)   {
  if ($type == "video_history"){
    rcpar_partners_create_notif_if_needed($entity);
  }
}

/**
 * Implements hook_entity_insert()
 */
function rcpar_partners_entity_insert($entity, $type)  {
  if ($type == "video_history"){
    rcpar_partners_create_notif_if_needed($entity);
  }
  // Prepaid credit orders need to have their payment processed immediately.
  // Commerce does not provide a post save hook so we'll get the generated
  // order id in the entity_insert hook and check to see if the order is a
  // prepaid credit order
  $params = drupal_get_query_parameters();
  if ($type == "commerce_order" && isset($entity->order_id) && isset($params['prepaid'])){
    // I would have thought that the $entity object would be exactly the same
    // as an order object but it is not, so we load up the order
    $order = commerce_order_load($entity->order_id);
    $o = new RCPARCommerceOrderEntity($order);
    // Check for prepaid credit order
    if ($o->isPrePaidCreditOrder()) {
      // Now, we need to create a payment
      $transaction = commerce_payment_transaction_new('rcpar_partners_payment_prepaid_credit', $order->order_id);
      $transaction->instance_id = 'rcpar_partners_payment_prepaid_credit|commerce_payment_rcpar_partners_payment_prepaid_credit';
      $transaction->currency_code = $o->getCurrencyCode();
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->amount = $o->getOrderTotal();
      commerce_payment_transaction_save($transaction);
    }
  }
}

function rcpar_partners_create_notif_if_needed($vh_entity)  {
  $video_history_wrapper = entity_metadata_wrapper('video_history', $vh_entity);

  $completed = $video_history_wrapper->percentage_completed->value();
  $prod = $video_history_wrapper->entitlement_product->value();

  $prod_wrapper = entity_metadata_wrapper('node', $prod);
  try{
    $partner_id = $prod_wrapper->field_partner_id->value();
    if ($partner_id){
      $partner = node_load($partner_id);
      $partner_wrapper = entity_metadata_wrapper('node', $partner);
      $notifications_enabled = $partner_wrapper->field_enable_notifications->value();
      $notif_threshold = $partner_wrapper->field_progress_notification->value();

      if ($notifications_enabled && $notif_threshold <= $completed){
        $notif = rcpar_partners_get_user_notification_for_entitlement($vh_entity->uid, $prod->nid);
        if (!$notif){
          $values = array(
            'uid' => $vh_entity->uid,
            'entitlement_id' => $prod->nid,
            'partner_id' => $partner_id,
            'sku' => $prod_wrapper->field_product_sku->value(),
            'percentage' => $notif_threshold
          );
          rcpar_partners_notifications_push($values);
        }
      }
    }
  } catch (EntityMetadataWrapperException $exc) {
    watchdog('rcpar_partners_notifications', "function rcpar_partners_create_notif_if_needed entity error: ".$exc->getMessage());
  }
}
