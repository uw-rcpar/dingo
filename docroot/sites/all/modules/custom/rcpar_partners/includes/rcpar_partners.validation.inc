<?php

/**
 *
 */
function rcpar_partners_get_emails($form , &$form_state, $search_email = null, $search_partner_nid = null) {
  if ($search_email != "validation_emails" && isset($search_email) && $search_email !== "null") {
    $result = db_query("SELECT * FROM rcpa_partners_emails WHERE email LIKE '%%$search_email%%'");
  } else if($search_email != "validation_emails" && isset($search_partner_nid) && $search_partner_nid != "null") {
    $result = db_query("SELECT * FROM rcpa_partners_emails WHERE partner_nid LIKE '%%$search_partner_nid%%'");  
  //} else if($search_email != "validation_emails" && isset($search_partner_nid) && $search_partner_nid != "null" && isset($search_email) && $search_email !== "null") {
    //$result = db_query("SELECT * FROM rcpa_partners_emails WHERE email LIKE '%%$search_email%%' AND partner_nid LIKE '%%$search_partner_nid%%'");  
  } else {
    $sql = "SELECT * FROM rcpa_partners_emails";
    $result = db_query($sql);
  }

  $header = array();
  $data = array();
  $header[] = array('data'=>t('ID'));
  $header[] = array('data'=>t('Partner ID'));
  $header[] = array('data'=>t('Email'));
  $header[] = array('data'=>t('Status'));
  $header[] = array('data'=>t('Count'));
  $header[] = array('data'=>t('Options'));

  $rows = array();
  while ($row = $result->fetchObject()) {
    $rows[] = array($row->id, l($row->partner_nid, 'node/'.$row->partner_nid.'/edit'), $row->email, $row->status, $row->count, '<a href="/admin/settings/rcpar/partners/validation_emails/' . $row->id. '/edit">edit</a>');
  }

  // Show message if not results
  if (empty($rows)) {
    drupal_set_message('No results returned, try refining your search.','warning');
  }

  $output = theme_table(array('header' => $header,
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'sticky' => false,
    'empty' => false,
    'colgroups' => array()));

  $form = array();

  $form['rcpar'] = array(
  '#type' => 'fieldset',
  '#title' => 'Search Validation Emails',
  '#collapsible' => true,
  );

  $form['rcpar']['search_email'] = array(
  '#type' => 'textfield',
  '#title' => t('Email Search'),
  '#default_value' => ($search_email != "null" ? $search_email : ''),
  );

  $form['rcpar']['search_partner_nid'] = array(
  '#type' => 'textfield',
  '#title' => t('Partner ID Search'),
  '#default_value' => ($search_partner_nid != "null" ? $search_partner_nid : ''),
  );


  $form['rcpar']['submit'] = array(
  '#type' => 'submit',
  '#value' => t('Search')
  );

  $form['rcpar']['reset'] = array(
  '#type' => 'submit',
  '#value' => t('Reset')
  );

  // Add New email form
  $newform = drupal_get_form('rcpar_partners_new_email_form');
  $form['new_email'] = rcpar_partners_new_email_form();

  $form['info'] = array(
  '#markup' => "<br><br>". $output
  );

  return $form;
}

function rcpar_partners_new_email_form() {

  // create a new email
  $form['new_rcpar'] = array(
  '#type' => 'fieldset',
  '#title' => 'Add a New Email',
  '#prefix' => '<div id="new-email">',
  '#suffix' => '</div>',
  '#collapsible' => true,
  '#collapsed' => true,
  );

  $form['new_rcpar']['new_partner_nid'] = array(
  '#type' => 'textfield',
  '#title' => t('Partner ID'),
  '#size' => 25
  );

  $form['new_rcpar']['new_email'] = array(
  '#type' => 'textfield',
  '#title' => t('Email'),
  '#size' => 25
  );

  $options = array('0'=>'0','1'=>'1');
  $form['new_rcpar']['new_status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => $options,
    '#default_value' => 1,
  );

  $form['new_rcpar']['actions'] = array(
    '#type' => 'submit',
    '#value' => t('Save Email'),
    '#submit' => array('rcpar_partners_new_email_form_submit'),
  );

  $form['new_rcpar']['actions']['#validate'] = array('rcpar_partners_new_email_validate');

  return $form;
}

function rcpar_partners_new_email_form_submit($form, &$form_state) {
  drupal_set_message(t('Need to add the submit handler for adding new emails.'), 'notice');
}

function rcpar_partners_new_email_validate($form, &$form_state) {
  //print_r($form_state['values']);
  form_set_error('new_partner_nid', 'Need to add Validation for adding a new email.');
  form_set_error('new_email', 'Need to add Validation for adding a new email.');
}

function rcpar_partners_get_emails_validate($form, &$form_state) {

  if ($form_state['values']['op'] == 'Search' && !empty($form_state['values']['search_email'])) {
    drupal_goto('admin/settings/rcpar/partners/validation_emails/' . $form_state['values']['search_email'] . '/null');
//  drupal_goto('admin/settings/rcpar/partners/validation_emails/' . $form_state['values']['search_email'] . '/' . $form_state['values']['search_partner_nid']);
  }
  if ($form_state['values']['op'] == 'Search' && !empty($form_state['values']['search_partner_nid'])) {
    drupal_goto('admin/settings/rcpar/partners/validation_emails/null/' . $form_state['values']['search_partner_nid']);
//  drupal_goto('admin/settings/rcpar/partners/validation_emails/' . $form_state['values']['search_email'] . '/' . $form_state['values']['search_partner_nid']);
  }
  if ($form_state['values']['op'] == 'Reset') {
    drupal_goto('admin/settings/rcpar/partners/validation_email_list');
  }


  if ($form_state['values']['op'] == 'Add Email') {
    if (empty($form_state['values']['email'])) {
      form_set_error('email', 'You must enter a valid Email.');
    }

    $select = db_select('rcpa_partners_emails', 'pe');
    $select->addField('pe', 'email');
    $select->condition('email',$form_state['values']['email']);
    $id = $select->execute()->fetchColumn();

    if ($id) {
      form_set_error('email', 'There is already an Email by this name.');
    }
  }
}

function rcpar_partners_get_emails_submit($form, &$form_state) {
  switch ($form_state['values']['op']) {
    case 'Search':
      echo "Searching<br>";
      break;

    case 'Add Email':

      if ($form_state['values']['email']) {
        $insert = db_insert('rcpa_partners_emails');
        $insert->fields(array('partner_nid' => $form_state['values']['partner_nid'], 
                              'email' => $form_state['values']['email'], 
                              'status' => $form_state['values']['status'],
                              'created' => REQUEST_TIME)
                        );
        $result = $insert->execute();
        drupal_set_message('New Validation Email Added: '. $form_state['values']['email']);
      }

      break;
  }
}

/**
 * Get the user access key data from central_reporting db
 *
 * @param unknown_type $account
 * @return unknown
 */
function rcpar_partners_get_email_data($id, $lookup_type='id') {
  // arguement not passed in.
  if (empty($id) || $id == 0) {
    return;
  }

  $result = db_query("SELECT * FROM rcpa_partners_emails
                        WHERE id = :id", array(':id' => $id));

  $results = $result->fetchObject();

  return $results;
}


/**
 * Email manager callback
 */
function rcpar_partners_email_edit($form , &$form_state, $id) {
  $email = rcpar_partners_get_email_data($id);

  if (!isset($email->id)) {
    drupal_not_found();
    exit;
  }

  $output = l(t('Return to Email'), 'admin/settings/rcpar/partners/validation_email_list', array('attributes'=>array('class'=>'email-cancel')));

  $form['cancel'] = array(
    '#markup' => $output
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $email->id
  );

  $form['partner_nid'] = array(
    '#type' => 'textfield',
    '#title' => 'Partner ID',
    '#default_value' => $email->partner_nid
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => 'Email',
    '#default_value' => $email->email
  );

  $options = array('0'=>'0','1'=>'1');
  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => $options,
    '#default_value' => $email->status,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update Email'
  );

  drupal_set_title('Edit Email: ('. $email->id .') '. $email->email);

  return $form;
}

function rcpar_partners_email_edit_validate($form, &$form_state) {
  if ($form_state['values']['id'] == '' || !is_numeric($form_state['values']['id'])) {
    form_set_error('', t('You must enter a valid Email ID.'));
  }

  if ($form_state['values']['email'] == '') {
    form_set_error('', t('You must enter a valid Email Title.'));
  }
}

function rcpar_partners_email_edit_submit($form, &$form_state) {

  if ($form_state['values']['id'] != '') {
    db_query("UPDATE rcpa_partners_emails SET partner_nid=:partner_nid, email=:email, status=:status 
                WHERE id = :id", array(':partner_nid'=>$form_state['values']['partner_nid'], ':email'=>$form_state['values']['email'], ':status'=>$form_state['values']['status'], ':id'=>$form_state['values']['id']));
    drupal_set_message(t('Email has been saved. ' . l(t('Return to Email'), 'admin/settings/rcpar/partners/validation_email_list', array('attributes'=>array('class'=>'email-cancel')))), 'notice');
  }

}