<?php
function rcpar_partners_settings() {    
  $form['rcpar_partners'] = array(
      '#type' => 'fieldset',
      '#title' => t('Partner Commerce Message Settings'),
      '#collapsible' => false,
      '#collapsed' => false,
  );
  
  $form['rcpar_partners']['directbill_msg'] = array(
      '#type' => 'textarea',
      '#title' => t('Direct Billing Message (Shipping Page)'),
      '#description' => t('Message to be displayed on Shipping page when a user comes through a partner that is setup for direct bill payments.'),
      '#default_value' => variable_get('directbill_msg', ''),
  );

  $form['rcpar_partners']['directbill_msg2'] = array(
      '#type' => 'textarea',
      '#title' => t('Direct Billing Message (Billing Page)'),
      '#description' => t('Message to be displayed on Billing page when a user comes through a partner that is setup for direct bill payments.'),
      '#default_value' => variable_get('directbill_msg2', ''),
  );

  $form['rcpar_partners']['directbill_freetrial_msg'] = array(
      '#type' => 'textarea',
      '#title' => t('Free Trial Message (Shipping Page)'),
      '#description' => t('Message to be displayed on Shipping page when a user comes through a partner that is setup for free trial.'),
      '#default_value' => variable_get('directbill_freetrial_msg', ''),
  );

  $form['rcpar_partners']['directbill_freetrial_msg2'] = array(
      '#type' => 'textarea',
      '#title' => t('Free Trial Message (Billing Page)'),
      '#description' => t('Message to be displayed on Billing page when a user comes through a partner that is setup for free trial.'),
      '#default_value' => variable_get('directbill_freetrial_msg2', ''),
  );
    
  return system_settings_form($form);
}
