<?php

function _glazed_builder_profile_form_els() {
  $profiles = glazed_builder_profile_load_all();
  $form = [];
  foreach ($profiles as $profile) {
    $form['profiles'][$profile->name]['#profile'] = $profile->name;
    $form['profiles'][$profile->name]['name'] = array('#markup' => check_plain($profile->name));
    $form['profiles'][$profile->name]['sidebar'] = array(
      '#markup' => $profile->sidebar,
    );
    $form['profiles'][$profile->name]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array('@title' => $profile->name)),
      '#title_display' => 'invisible',
      '#delta' => 10,
      '#default_value' => $profile->weight,
    );
    $roles = $profile->roles;
    array_walk($roles, function(&$value) {
      $role = user_role_load($value);
      if ($role) {
        $value = $role->name;
      }
      else {
        $value = t('Missing Role');
      }
    });
    $form['profiles'][$profile->name]['roles'] = array(
      '#markup' => implode(', ', $roles),
    );
    $form['profiles'][$profile->name]['edit'] = array(
      '#type' => 'link',
      '#title' => t('Edit profile'),
      '#href' => 'admin/config/content/glazed_builder/' . $profile->name,
    );
    if ($profile->type == 'Normal') {
      $form['profiles'][$profile->name]['delete'] = array(
        '#type' => 'link',
        '#title' => t('Delete profile'),
        '#href' => 'admin/config/content/glazed_builder/' . $profile->name . '/delete',
      );
    }
  }
  $form['profiles']['#tree'] = TRUE;
  return $form;
}

/**
 * Form submission handler for glazed_builder_form().
 */
function glazed_builder_form_submit($form, &$form_state) {
  // Update profile weights.
  if (isset($form_state['values']['profiles'])) {
    foreach ($form_state['values']['profiles'] as $profile_name => $data) {
      $profile = glazed_builder_profile_load($profile_name);
      $profile->weight = $data['weight'];
      $profile->is_new = FALSE;
      glazed_builder_profile_save($profile);
    }
  }
}

/**
 * Returns HTML for the settings form as a sortable list of profiles.
 */
function theme_glazed_builder_form($variables) {
  $form = $variables['form'];

  $rows = array();
  if (isset($form['profiles'])) {
    foreach (element_children($form['profiles']) as $key) {
      $profile = &$form['profiles'][$key];

      $row = array();
      $row[] = drupal_render($profile['name']);
      if (isset($profile['weight'])) {
        $profile['weight']['#attributes']['class'] = array('profile-weight');
        $row[] = drupal_render($profile['weight']);
      }
      $row[] = drupal_render($profile['roles']);
      $row[] = drupal_render($profile['sidebar']) ? 'Showed' : 'Hidden';
      $row[] = drupal_render($profile['edit']);
      $row[] = drupal_render($profile['delete']);
      $rows[] = array('data' => $row, 'class'  => array('draggable'));
    }
  }

  $header = array(t('Profile name'));
  if (isset($form['actions'])) {
    $header[] = t('Weight');
    $header[] = t('Roles');
    $header[] = t('Sidebar');
    drupal_add_tabledrag('glazed-builder-profiles', 'order', 'sibling', 'profile-weight');

  }
  $header[] = array('data' => t('Operations'), 'colspan' => '2');
  return theme(
    'table',
    array(
      'header' => $header,
      'rows' => $rows,
      'empty' => t('No profiles configured yet'),
      'attributes' => array('id' => 'glazed-builder-profiles'),
    )
  ) . t('A user will get the first Glazed Builder profile that has a matching user role.') . drupal_render_children($form);
}

/**
 * Form builder for Glazed Builder profile form.
 */
function glazed_builder_profile_form($form, &$form_state, $profile = NULL) {

  if (!$profile) {
    $profile = new stdClass();
    $profile->name = NULL;
    $profile->roles = [];
    $profile->sidebar = TRUE;
    $profile->elements = array_keys(glazed_builder_elements());
    $profile->blocks = array_keys(glazed_builder_blocks());
    $profile->views = array_keys(glazed_builder_views());
    $profile->ckeditor_buttons_inline = array(
      'Bold',
      'Italic',
      'RemoveFormat',
      'TextColor',
      'Format',
      'Styles',
      'FontSize',
      'JustifyLeft',
      'JustifyCenter',
      'JustifyRight',
      'JustifyBlock',
      'BulletedList',
      'Link',
      'Unlink',
      'Image',
      'Table',
      'Undo',
      'Redo',
    );
    $profile->ckeditor_buttons_modal = array(
      'Bold',
      'Italic',
      'Underline',
      'Strike',
      'Superscript',
      'Subscript',
      'RemoveFormat',
      'JustifyLeft',
      'JustifyCenter',
      'JustifyRight',
      'JustifyBlock',
      'BulletedList',
      'NumberedList',
      'Outdent',
      'Indent',
      'Blockquote',
      'CreateDiv',
      'Undo',
      'Redo',
      'PasteText',
      'PasteFromWord',
      'Link',
      'Unlink',
      'Image',
      'HorizontalRule',
      'SpecialChar',
      'Table',
      'Templates',
      'TextColor',
      'Source',
      'ShowBlocks',
      'Maximize',
      'Format',
      'Styles',
      'FontSize',
      'Scayt',
    );
  }

  $form['is_new'] = [
    '#type' => 'value',
    '#value' => (bool) !$profile->name,
  ];

  $form['name'] = [
    '#type' => 'machine_name',
    '#title' => t('Name'),
    '#default_value' => $profile->name,
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'glazed_builder_profile_load',
    ),
    '#disabled' => (bool) $profile->name,
  ];

  $form['sidebar'] = [
    '#type' => 'checkbox',
    '#title' => 'Show snippet sidebar',
    '#default_value' => $profile->sidebar,
  ];

  $form['roles_wrapper'] = [
    '#type' => 'fieldset',
    '#title' => t('Roles'),
    '#description' => t('If a user has one of the selected roles his Glazed Builder interface will be limited to the elements and buttons selected in this profile. New blocks and views are not included automatically.'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  ];
  $form['roles_wrapper']['roles'] = [
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#options' => array_map('check_plain', user_roles(TRUE)),
    '#default_value' => $profile->roles,
  ];

  $form['elements_wrapper'] = [
    '#type' => 'fieldset',
    '#title' => t('Elements'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  ];
  $form['elements_wrapper']['elements'] = [
    '#type' => 'checkboxes',
    '#title' => t('Elements'),
    '#options' => glazed_builder_elements(),
    '#default_value' => $profile->elements,
  ];

  $form['blocks_wrapper'] = [
    '#type' => 'fieldset',
    '#title' => t('Blocks'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  ];

  $default_value = array();
  foreach (glazed_builder_blocks() as $value => $label) {
    if (!in_array($value, $profile->blocks)) {
      $default_value[] = $value;
    }
  }
  $form['blocks_wrapper']['blocks'] = [
    '#type' => 'checkboxes',
    '#title' => t('Blocks'),
    '#options' => glazed_builder_blocks(),
    '#default_value' => $profile->blocks,
  ];

  $form['views_wrapper'] = [
    '#type' => 'fieldset',
    '#title' => t('Views'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  ];

  $default_value = array();
  foreach (glazed_builder_views() as $value => $label) {
    if (!in_array($value, $profile->views)) {
      $default_value[] = $value;
    }
  }
  $form['views_wrapper']['views'] = [
    '#type' => 'checkboxes',
    '#title' => t('Views'),
    '#options' => glazed_builder_views(),
    '#default_value' => $profile->views,
  ];

  foreach (array('inline', 'modal') as  $mode) {

    $key = 'ckeditor_buttons_' . $mode;
    $title = $mode == 'inline' ? t('CKEditor buttons (inline editing)') :  t('CKEditor buttons (modal editing)');

    $form[$key] = array(
      '#type' => 'fieldset',
      '#title' => $title,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      '#attributes' => array('class' => array('cke_ltr')),
    );

    // Generate the button list.
    foreach (glazed_builder_get_ckeditor_buttons() as $button => $title) {
      $form[$key][$button] = array(
        '#type' => 'checkbox',
        '#title' => check_plain($title),
        '#default_value' => in_array($button, $profile->{$key}),
        // Add a button icon near to the checkbox.
        '#field_suffix' => sprintf('<span class="cke_button_icon cke_button__%s_icon"></span>', strtolower($button)),
      );
    }

  }

  $module_path = drupal_get_path('module', 'glazed_builder');
  drupal_add_css($module_path . '/glazed_builder/vendor/ckeditor/skins/moono-lisa/editor.css');
  drupal_add_css($module_path . '/css/profile-form.css');

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  $form['#attached']['js'][] = drupal_get_path('module', 'glazed_builder') . '/js/profile-form.js';

  return $form;
}

/**
 * Form builder for Glazed Builder profile form.
 */
function glazed_builder_profile_form_submit($form, &$form_state) {
  $values = &$form_state['values'];

  $profile = new stdClass();
  $profile->is_new = $values['is_new'];
  $profile->name = $values['name'];
  $profile->sidebar = $values['sidebar'];

  $list_properties = array(
    'roles',
    'elements',
  );
  foreach ($list_properties as $property) {
    $profile->{$property} = array_values(array_filter($values[$property]));
  }

  $profile->roles = array_values(array_filter($values['roles']));
  $profile->elements = array_values(array_filter($values['elements']));
  $profile->blocks = array_values(array_filter($values['blocks']));
  $profile->views = array_values(array_filter($values['views']));

  $profile->ckeditor_buttons_inline = array_keys(array_filter($values['ckeditor_buttons_inline']));
  $profile->ckeditor_buttons_inline = array_keys(array_filter($values['ckeditor_buttons_inline']));
  $profile->ckeditor_buttons_modal = array_keys(array_filter($values['ckeditor_buttons_modal']));
  $profile->ckeditor_buttons_modal = array_keys(array_filter($values['ckeditor_buttons_modal']));

  $result = glazed_builder_profile_save($profile);
  if ($result == SAVED_NEW) {
    drupal_set_message(t('Profile %name has been created.', array('%name' => $profile->name)));
  }
  else {
    drupal_set_message(t('Profile %name has been updated.', array('%name' => $profile->name)));
  }
  $form_state['redirect'] = 'admin/config/content/glazed_builder';
}

/**
 * Delete editor profile confirmation form.
 */
function glazed_builder_profile_delete_form($form, &$form_state, $profile) {
  if ($profile->type != 'Normal') {
    drupal_not_found();
    drupal_exit();
  }
  $form_state['profile'] = $profile;
  return confirm_form(
    $form,
    t('Are you sure you want to remove the profile %name?', array('%name' => $profile->name)),
    'admin/config/content/glazed_builder',
    t('This action cannot be undone.'),
    t('Remove'),
    t('Cancel')
  );
}

/**
 * Submit callback for Wysiwyg profile delete form.
 *
 * @see wysiwyg_profile_delete_confirm()
 */
function glazed_builder_profile_delete_form_submit($form, &$form_state) {
  $profile = $form_state['profile'];
  glazed_builder_profile_delete($profile->name);
  drupal_set_message(t('Glazed Builder profile %name has been deleted.', array('%name' => $profile->name)));
  $form_state['redirect'] = 'admin/config/content/glazed_builder';
}
