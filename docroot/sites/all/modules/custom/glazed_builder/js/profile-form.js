(function ($) {

  /**
   * Shows checked and disabled checkboxes for inherited permissions.
   */
  Drupal.behaviors.glazedBuilderProfileForm = {
    attach: function () {

      var $rolesWrapper = $('#edit-roles');
      var $authRoleInput = $rolesWrapper.find('input[name="roles[2]"]');
      var $namedRoleInputs = $rolesWrapper.find('input').not($authRoleInput);

      $authRoleInput.change(function () {
        if (this.checked) {
          $namedRoleInputs.attr('checked', true);
          $namedRoleInputs.attr('disabled', true);
        }
        else {
          $namedRoleInputs.attr('disabled', false);
        }
      })

    }
  }

})(jQuery);
