(function ($) {
    Drupal.behaviors.rcparCommerceOrdersAffirm = {

        attach: function (context, settings) {
            // we need to wait until affirm adds the button to add our callback
            var func = function () {
                if ($("#affirm_error_back_button", $("#affirm_error_screen").contents()).length > 0) {
                    $("#affirm_error_back_button", $("#affirm_error_screen").contents()).click(function () {
                        window.location.href = Drupal.settings.commerce_affirm.CancelUrl;
                    });

                } else {
                    setTimeout(function () {
                        func();
                    }, 300);
                }
            }
            func();
        }
    };
}(jQuery));
