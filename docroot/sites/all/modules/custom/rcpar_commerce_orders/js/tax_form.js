(function ($) {
    Drupal.behaviors.rcparCommerceOrdersTax = {
        attach: function (context, settings) {
            $('.delete-tax-row').click(function(){
                return confirm("Are you sure you want to delete this element?");
            });
        }
    };
}(jQuery));
