<?php

/**
 * Implements hook_token_info_alter().
 */
function rcpar_commerce_orders_token_info_alter(&$data) {
    $data['tokens']['commerce-order']['partner-shipping-type-notice'] = array(
        'name' => t('Partner Shipping Type Notice'),
        'description' => t('A notice explaining if we need to do something especial to ship this order.'),
    );
    $data['tokens']['commerce-order']['custom-order-display'] = array(
        'name' => t('Custom Order Display'),
        'description' => t('Custom order display according total order.'),
    );
}

/**
 * implements a hook_token_info
 */
function rcpar_commerce_orders_token_info(){
  $info = array();
  // Tokens for commerce emials
  $info['tokens']['commerce-order']['commerce-order-partner-session-info'] = array(
        'name' => t("Commerce email token - PHP session info"),
        'description' => t("Display session with partner info ."),
    );
    return $info; 
}

/**
 * Implements hook_tokens().
 */
function rcpar_commerce_orders_tokens($type, $tokens, array $data = array(), array $options = array()) {
    $url_options = array('absolute' => TRUE);

    if (isset($options['language'])) {
        $url_options['language'] = $options['language'];
        $language_code = $options['language']->language;
    }
    else {
        $language_code = NULL;
    }

    $sanitize = !empty($options['sanitize']);

    $replacements = array();

    if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
        $order = $data['commerce-order'];

        foreach ($tokens as $name => $original) {
            switch ($name) {
                case 'partner-shipping-type-notice':
                    $message = '';
                    $shipping_type = '';
                    $field_shipping_flow = false;
                    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
                    try{
                        //Current possible values: 'standar', 'pre-approval', 'no-shipping'
                        if (isset($order_wrapper->field_partner_profile->field_shipping_flow)){
                            $field_shipping_flow = $order_wrapper->field_partner_profile->field_shipping_flow;
                            $val = $field_shipping_flow->value();
                        }
                        if ($field_shipping_flow && !empty($val)){
                            $shipping_type = $val;
                        }
                    } catch(EntityMetadataWrapperException $exc) {
                        watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
                    }

                    if ($shipping_type == 'no-shipping'){
                        $message = t('NO SHIPPING REQUIRED<br>');
                    } else if ($shipping_type == 'pre-approval'){
                        $message = t('SHIPPING REQUIRES APPROVAL<br>');
                    }
                    $replacements[$original] = $message;
                break;
                
                case 'commerce-order-partner-session-info':
                // Custom token to display partner info in commerce emails
                global $user;
                $value = "";
                $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

                if($partner = $order_wrapper->field_partner_profile->value()){ 
                    $partner_wrapper = entity_metadata_wrapper('node', $partner);
                    $partner_info = $partner_wrapper->field_abbreviated_name->value() ? $partner_wrapper->field_abbreviated_name->value() : $partner->id;
                    $value = "<p style='color: #666; font-size: 13px;'><b>Partner</b>: {$partner_info} </p>";
                }
                $replacements[$original] = $value;
                break;
				
                case 'custom-order-display':
                  $order_items = rcpar_commerce_orders_display_total_order($order);
                  $replacements[$original] = $order_items;
                  break;
            }
        }
    }
    return $replacements;
}

