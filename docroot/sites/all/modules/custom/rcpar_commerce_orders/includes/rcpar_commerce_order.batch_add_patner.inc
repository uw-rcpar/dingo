<?php

/**
 * Form to start executing the process of cleaning orphan's on the 
 * tree structure Orders -> Partner.
 */
function rcpar_commerce_order_add_partner_form($form, $form_state) {
  $form['actions']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Clean Up orders without Partner'),
    '#submit' => array('rcpar_commerce_order_add_partner_batch'),
  );

  return $form;
}

/**
 * Operations of the batch to start cleaning each orphan on 
 * the tree structure Orders -> Partner.
 */
function rcpar_commerce_order_add_partner_batch() {
  $operations = array();

  // Get the Orders to be process.
  $result = db_query("
    SELECT o.field_order_id_value, p.`field_partner_id_value`
    FROM field_data_field_order_id as o
    INNER JOIN node as n
    ON o.entity_id = n.nid 
    INNER JOIN field_data_field_partner_id as p
    ON p.entity_id = n.nid
    where n.type = 'user_entitlement_product' and o.entity_id = n.nid and p.entity_id = n.nid and p.field_partner_id_value <> 0 and n.created >= '1545609600' and 
    o.field_order_id_value not in (select partner.entity_id from field_data_field_partner_profile as partner where partner.entity_id = o.field_order_id_value)
    group by o.field_order_id_value, p.`field_partner_id_value`", array()
  );

  foreach ($result as $order) {
    $operations[] = array('rcpar_commerce_order_add_partner_process', array($order));
  }
  // Batch
  $batch = array(
    'operations' => $operations,
    'finished' => 'rcpar_commerce_order_add_partner_finished_batch',
    'title' => t('Cleaning up Orders without Partners'),
    'init_message' => t('Inicializing Cleaning Up process.'),
    'progress_message' => t("Processed @current out of @total Chapters."),
    'error_message' => t('Error'),
    'file' => drupal_get_path('module', 'rcpar_commerce_orders') . '/includes/rcpar_commerce_order.batch_add_patner.inc',
  );
  batch_set($batch);
}

/**
 * Last message when the batch finish it's procesing.
 */
function rcpar_commerce_order_add_partner_finished_batch() {
  drupal_set_message("All Chapters have been processed");
}

/**
 * Add the partner profile to the orders without the relationship.
 * @param $order_data
 *  - object with the partner and order id of the order with problem.
 */
function rcpar_commerce_order_add_partner_process($order_data) {
  $order = commerce_order_load($order_data->field_order_id_value);
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // Add the Partner relationship in the order.
  $order_wrapper->field_partner_profile->set(node_load($order_data->field_partner_id_value));
  $order_wrapper->save();
  drupal_set_message("The Partner $order_data->field_partner_id_value has been added to the Order #$order_data->field_order_id_value has been ");
}
