<?php

/**
*  hook_views_data_alter()
*/

function rcpar_commerce_orders_views_data_alter(&$data){
    $data['commerce_order']['hostname'] = array(
        'title' => t('Orderer IP'),
        'help' => t('The IP address of the order creator'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => FALSE,
        )
    );
}
