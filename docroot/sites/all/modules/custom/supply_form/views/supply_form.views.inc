<?php

function supply_form_views_data() {
  $data = array();
  $data['supply_form'] = array(
      'table' => array(
          'group' => t('Supply Form Group'),
          'title' => t('Supply Form'),
          'help' => t('All the submission for Supply form'),
          'entity type' => 'supply_form',
          'base' => array(
              'field' => 'sid',
              'title' => t('SID'),
              'help' => t('Submittions for supply form'),
          ),
      ),
      'sid' => array(
          'title' => t('SID'),
          'help' => t('Submittion ID'),
          'field' => array(
              'handler' => 'views_handler_field',
          ),
          'sort' => array(
              'handler' => 'views_handler_sort',
          ),
      ),
      'created' => array(
          'title' => t('Created Timestamp'),
          'help' => t('The date that the submission was made'),
          'field' => array(
              'handler' => 'views_handler_field',
          ), 'sort' => array(
              'handler' => 'views_handler_sort',
          ),
      ),
      'shipped' => array(
          'title' => t('Shipped'),
          'help' => t('Flag that indicates whether the supply elements where shipped or not'),
          'field' => array(
              'handler' => 'views_handler_field',
          ), 'sort' => array(
              'handler' => 'views_handler_sort',
          ),
      ),
      'data' => array(
          'title' => t('Data'),
          'help' => t('All the information submitted on the supply form'),
          'field' => array(
              'handler' => 'views_handler_field',
          ), 'sort' => array(
              'handler' => 'views_handler_sort',
          ),
      ),
      'uid' => array(
          'title' => t('User ID'),
          'help' => t('The user that submits the supply form'),
          'field' => array(
              'handler' => 'views_handler_field_user',
          ), 'sort' => array(
              'handler' => 'views_handler_sort',
          ),
          'filter' => array(
              'handler' => 'views_handler_filter_user_current',
          ),
      ),
  );



  return $data;
}
