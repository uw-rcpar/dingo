<?php
extract($variables)

/*
 * Variables:
 * $email
 * $sid = submission id
 */
?>

<div class="supply-email-container">
    <div class='order-data'>
        Supply Request <?php echo $sid?>:
        You have a NEW Supply Request that has been submitted.
        Please review the details here: <a href="/admin/settings/rcpar/supply-form">Supply Form</a>
    </div>
</div>