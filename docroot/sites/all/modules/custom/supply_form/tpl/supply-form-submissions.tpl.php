<?php extract($variables) ?>
<?php //echo '<pre>';print_r($data);exit; ?>

<a id="supply-form-show-deatails" style="background-image: linear-gradient(-90deg, #ffffff 0px, #f9f9f9 100%); border-radius: 5px; cursor: pointer; padding: 5px; background-color: #ffffff; border: 1px solid #0071b3; width: 70px; text-align: center; font-size: 11px; display: inline-block;">Show</a>
<div class="main-submissions-container" style="display: none">

<?php 

foreach ($data as $box_id => $box) : ?>

  <?php $box_title = explode("-", $box_id) ?>
  <h2>
    <?php 
    $label = count($box_title) > 1 ? implode(" ", $box_title) : "General"; 
    echo strtoupper($label);
    ?>
  </h2>

  <?php 
    foreach ($box as $title => $sub_data) :
        if (strpos($title, 'folder-') !== false) {
          $data = explode('-', $title);
          $folder_counter = $data[1];
        }
  
        if (!is_array($sub_data)) {
          if (!is_null($sub_data)) {
            //  Special Values              
            if (strpos($title, 'include with order') !== false) {
              $sub_data = $sub_data ? 'Yes' : 'No';
            }
            if (strpos($title, 'due date') !== false && isset($box['date approved by']['date'])) {
              $sub_data_text = supply_form_basic_due_date_options($box['due date']);
              $sub_data = $sub_data_text .' '.$box['date approved by']['date'];
            }
          ?>
      <div style="background-color: #f5f4f4; padding: 5px 20px 5px 20px;">
        <div style="width: 35%; float: left;"><b><?php echo ucwords(str_replace("-", " ", $title)) ?>:</b></div>
        <div style="width: 65%; float: left;"><?php echo $sub_data ?></div>
        <div style="clear:both;"></div>
      </div>

  <?php 
      }
    } elseif(!empty($sub_data) && !isset ($sub_data['date'])) {
      if(array_filter($sub_data)){      
  ?>

<!--   Flyer Data comes out here: -->
      <div class="flyer-folder">
        <div class="values-content">
        <?php 
          $records = '';
          $header = "<h2>" . strtoupper(str_replace("-", " ", $title)) . "</h2>";
          foreach ($sub_data as $title2 => $value) :
            if (!is_null($value)) {
              $show = TRUE;
              $folder_type = '';
              // If array rebuild it            
              if (is_array($value)) {
                $value = theme('file_link',array('file' => (object)$value)); 
              }                            
              //  Special Values        
              if ($title2 == 'folder type') {                
                $folder_type = $value;
                $value = supply_form_basic_folder_type_options($value);
              }
              if (strpos($title2, 'folder type selected') !== false) {  
                $value = supply_form_folder_type_options($value);
                $show = !$folder_type ? FALSE : TRUE;
              }
              if (strpos($title2, 'back side of firm specific') !== false) {                
                $value = supply_form_folder_specific_flyer_options($value);
                 $show = !$folder_type ? TRUE : FALSE;
              }  
              if (strpos($title2, 'attached file') !== false) {                
                $value = $value ? $value : 'No';
              }                
              if(strpos($title2, 'attached front side of firm') !== false){
                $value = $value ? $value : 'No';
                $show = !$folder_type ? TRUE : FALSE;
              }
              // Special render for supply notes 
              if(strpos($title2, 'other supplies') !== false){ 
                $notes = preg_split("/\r\n|\n|\r/", $value);
                $value = "";
                foreach ($notes as $notes_item) {
                  $value .= "<p>{$notes_item}</p>";
                }
              }
              //
              if($show && !is_null($value)){     
                $records .= '<div style="background-color: #f5f4f4; padding: 5px 20px 5px 20px;">
                <div style="width: 35%; float: left;"><b>' . ucwords($title2) . ':</b></div>
                <div style="width: 65%; float: left;">' . $value . '</div>
                <div style="clear:both;"></div>
                </div>'; 
              }
            } else {
              $header = '';
            }
          endforeach; 
          
          if ($records) {
            echo $header . $records;
          }
        ?>          
          </div>
      </div>
<!--   END Flyer Data comes out here: -->

    <?php 
          }
          }
    ?>
  <?php endforeach; ?>
<?php endforeach; ?>
<a href="#" class="print-submittion-supply">Print</a>
</div>
