<?php extract($variables) ;
//dpm($variables['flyers']);
/*
 * Variables:
 * $box_key
 * $box_by
 * $ship_to
 * $purpose
 * $order_approved_by
 * $flyers = array('flyer name', 'quantity', 'attached file', 'notes instructions')
 * $folders = array('folder type', 'folder quantity', 'folder type selected', 'folder type specific folder' , 'notes instructions');
 */

?>
<div class ="box_<?php print $box_key?>">

    <label for="box-dest">Date :</label><p class='box-dest'><?php print $box_by?></p>
    <?php if(isset($ship_to)){ ?>
      <label for="box-order-desc">Ship this box to:</label><p class='box-order-desc'><?php print $ship_to?></p>
    <?php } ?>
    <label for="box-purpose">Purpose / Event:</label><p class='box-purpose'><?php print $purpose?></p>
    <label for="box_approved">Order approved by:</label><p class='box_approved'><?php print $order_approved_by?></p>
    
        <div class ='flyers-section'>
            <h3>Flyers</h3>
            <?php foreach ($flyers as $fy_key => $fy_value) { ?>
            <div class='flyer flyer_<?php print $fy_key?>'>
                <label for="flyer-name">Flyer Name:</label><p class='flyer-name'><?php print $fy_value['flyer name']?></p>
                <label for="flyer-quantity">Flyer Quantity:</label><p class='flyer-quantity'><?php print $fy_value['quantity']?></p>
                <label for="flyer-file">Flyer File:</label><p class='flyer-file'><?php print $fy_value['attached file']->filename ?></p>
                <label for="flyer-notes">Flyer Notes:</label><p class='flyer-notes'><?php print $fy_value['notes instructions']?></p>
            </div>
            <?php } ?>
        </div>    
        <div class ='folders-section'>
            <h3>Folders</h3>
            <?php foreach ($folders as $fd_key => $fd_value) { ?>
            <div class='folder flyer_<?php print $fd_key?>'>
                <label for="folder-type">Folder type:</label><p class='folder-type'><?php print $fd_value['folder type']?></p>
                <label for="folder-quantity">Folder Quantity:</label><p class='folder-quantity'><?php print $fd_value['folder quantity']?></p>
                <label for="folder-type-selected">Folder Type Selected:</label><p class='folder-type-selected'><?php print $fd_value['folder type selected']?></p>
                <label for="folder-type-specific">Folder Type Specific:</label><p class='folder-type-specific'><?php print $fd_value['folder type specific folder']?></p>
                <label for="folder-notes">Folder Notes:</label><p class='folder-notes'><?php print $fd_value['notes instructions']?></p>
            </div>
            <?php } ?>
        </div>    

</div>