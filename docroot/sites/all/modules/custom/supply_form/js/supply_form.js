(function ($) {
  Drupal.behaviors.supply_form = {
    attach: function (context, settings) {      
      $('#edit-box-1').addClass('selected').removeClass('hidden-box');
      $('a#supply-form-show-deatails').click(function() {        
        if($(this).next().is(':visible')==false){
          $(this).html("Hide");
        }else{
          $(this).html("Show");
        }
        $(this).next().toggle("slow");
      });
      $('.add-box-button').click(function() {
        var complete = false;
        var last = null;
        $('.box-to-add').each(function(){
          $(this).removeClass('selected').addClass('not-selected');          
          if($(this).hasClass('hidden-box') && !complete){
            $(this).removeClass('hidden-box').addClass('selected').removeClass('not-selected');            
            $(this).find("*").prop('disabled',false);
            $(this).find("*").removeClass('disabled');
            $(this).find(".order-check-box").attr('checked','checked').attr("value","1");
            complete = true;
          }            
          last = $(this);
        });
        
        if(!complete){          
          last.addClass('selected').removeClass('not-selected');            
        }
      });
      
      $('.button-box').click(function(e) {
        e.preventDefault();
        $('.box-to-add').each(function(){
          $(this).removeClass('selected').addClass('not-selected');          
        });
        $(this).parent().parent().addClass('selected').removeClass('not-selected');        
      });

      $('.print-submittion-supply').click(function () {
        var divContents = $(this).parent().html();
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html><head><title>Supply Form</title><style>.print-submittion-supply { display: none; }</style>');
        printWindow.document.write('</head><body>');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();

      });
      if(jQuery().datepicker) {
        // warn user if ship by date chosen is less than 10 days from current date
        $("#edit-date-approved-by-box-1-datepicker-popup-0").datepicker({

          dateFormat: "yy-mm-dd",

          onSelect: function (dateText) {

            // today's date plus 10 days
            var shipping_limit = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 10).getTime();

            // selected ship by date
            var selected = new Date(dateText).getTime();

            // if selected date is less that ten days from now, launch modal
            if (selected < shipping_limit) {
              $('#shipping-warning').modal('show');
            }

          }
        });
      }

    }
  };
  

}(jQuery));