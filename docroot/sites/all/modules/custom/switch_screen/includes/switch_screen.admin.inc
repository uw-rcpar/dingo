<?php
/**
 * @file
 * Defines switch screen admin config settings
 */
function switch_screen_config() {
  $form = array();
  
  $form['ss_config'] = array(
  '#type' => 'fieldset',
  '#title' => t('Switch Screen Configuration'),
  '#collapsible' => true,
  '#collapsed' => false,
  );
  
  $form['ss_config']['ss_theme'] = array(
  '#type' => 'textfield',
  '#title' => t('Switch Theme Default'),        
  '#size' => 60,
  '#maxlength' => 128,     
  '#default_value' => variable_get('ss_theme', ''),
  );
  
  $form['ss_config']['ss_urls'] = array(
  '#type' => 'textarea',
  '#title' => t('Page URLS that will use Switch Screen theme:'),
  '#description' => t('Enter each URL on a new line.<br>Examples:<br> student/help-center/*<br>*/help-center/*'),
  '#default_value' => variable_get('ss_urls', ''),
  );
  
  return system_settings_form($form);
}