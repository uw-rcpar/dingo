<?php

use RCPAR\Course;
use RCPAR\User;


/**
 * Implements hook_token_info_alter
 */
function rcpar_salesforce_token_info_alter(&$data) {

  $data['tokens']['commerce-order']['lead-status-all'] = array(
    'name'        => 'Lead Status All',
    'description' => 'Lead Status All',
  );
  $data['tokens']['commerce-order']['salesforce-account-id'] = array(
    'name'        => 'Salesforce account (partner associated account) id',
    'description' => 'Id of the partner associated account entity on salesforce.',
  );

  $data['tokens']['commerce-order']['salesforce-opportunity-id'] = array(
    'name'        => 'Salesforce opportunity (partner) id',
    'description' => 'Id of the partner entity (Account entity) on salesforce.',
  );

  $data['tokens']['commerce-product']['salesforce-id'] = array(
    'name'        => 'Salesforce mapping id',
    'description' => 'Id of the product on salesforce price book.',
  );

  $data['tokens']['commerce-line-item']['pricebook-entry-id'] = array(
    'name'        => 'Salesforce pricebook id',
    'description' => 'Id of the product on salesforce price book.',
  );

  $data['tokens']['commerce-line-item']['product-price-raw'] = array(
    'name'        => 'Product unit price, without formatting',
    'description' => 'Product unit price, without formatting and with no accounts applied (eg 100.00).',
  );

  $data['tokens']['user']['salesforce-account-id'] = array(
    'name'        => 'Salesforce account (partner associated account) id',
    'description' => 'Id of the partner associated account entity on salesforce.',
  );
  $data['tokens']['user']['salesforce-opportunity-id'] = array(
    'name'        => 'Salesforce opportunity (partner) id',
    'description' => 'Id of the partner entity (Account entity) on salesforce.',
  );
  $data['tokens']['user']['field-aud-exam-date_as-date'] = array(
    'name'        => 'AUD exam date expressed as date',
    'description' => 'AUD exam date expressed as date instead of regular textfield.',
  );
  $data['tokens']['user']['field-bec-exam-date_as-date'] = array(
    'name'        => 'BEC exam date expressed as date',
    'description' => 'BEC exam date expressed as date instead of regular textfield.',
  );
  $data['tokens']['user']['field-far-exam-date_as-date'] = array(
    'name'        => 'FAR exam date expressed as date',
    'description' => 'FAR exam date expressed as date instead of regular textfield.',
  );
  $data['tokens']['user']['field-reg-exam-date_as-date'] = array(
    'name'        => 'REG exam date expressed as date',
    'description' => 'REG exam date expressed as date instead of regular textfield.',
  );
  $data['tokens']['user']['timezone'] = array(
    'name'        => 'User timezone',
    'description' => 'User timezone',
  );
  $data['tokens']['user']['drupal-created'] = array(
    'name'        => 'Drupal Created',
    'description' => 'Date and time user was created in Drupal',
  );
  $data['tokens']['user']['logged-days'] = array(
    'name'        => 'Logged days',
    'description' => 'User Logged days',
  );
  $data['tokens']['user']['aud-progress'] = array(
    'name'        => 'AUD Progress percent',
    'description' => 'AUD Progress percent',
  );
  $data['tokens']['user']['bec-progress'] = array(
    'name'        => 'BEC Progress percent',
    'description' => 'BEC Progress percent',
  );
  $data['tokens']['user']['reg-progress'] = array(
    'name'        => 'REG Progress percent',
    'description' => 'REG Progress percent',
  );
  $data['tokens']['user']['far-progress'] = array(
    'name'        => 'FAR Progress percent',
    'description' => 'FAR Progress percent',
  );
  $data['tokens']['user']['field_college_state_list_label'] = array(
    'name'        => 'User - College state label',
    'description' => 'User - College state label ',
  );
  
  $data['tokens']['node']['field_college_state_list_label'] = array(
    'name'        => 'Node - College state label',
    'description' => 'Node - College state label',
  );
  
  $data['tokens']['node']['array-field-join:?:?'] = array(
    'name'        => 'Implode array of values into a string',
    'description' => 'Implode array of field values (first param. is field name) using the specified glue (second param.).',
  );

  $data['tokens']['node']['sf-opportunity-type-id'] = array(
    'name'        => 'Opportunity type SF ID',
    'description' => 'Get the opportunity type ID for the selected partner type (only valid on partners nodes)',
  );

  $data['tokens']['node']['field_billing_type-key'] = array(
    'name'        => 'Billing Type key value',
    'description' => 'Prints the key value selected for the field field_billing_type',
  );
  $data['tokens']['node']['cloudamp__data__c'] = array(
    'name'        => 'cloudamp__data__c',
    'description' => 'cloudamp__data__c',
  );

  $data['tokens']['commerce-payment-transaction']['amount-for-salesforce'] = array(
    'name'        => 'Amount (formatted for Salesforce)',
    'description' => 'The amount for this transaction (formatted to be send to Salesforce).',
  );
  $data['tokens']['commerce-product']['q-pool-for-salesforce'] = array(
    'name'        => 'Question Pool',
    'description' => 'Question pool in format (<cp_pool_id>) <q_pool_title> (for ACT Prods only.)',
  );
  $data['tokens']['commerce-product']['act-sample-quizzes'] = array(
    'name'        => 'ACT Sample Quizzes',
    'description' => 'Quizzes in format (<cp_quiz_id>) <cp_quiz_title> (for ACT Sample Prods only.)',
  );
  $data['tokens']['commerce-product']['weight-for-salesforce'] = array(
    'name'        => 'Weight (formatted for Salesforce)',
    'description' => 'Weight (formatted for Salesforce)',
  );
  $data['tokens']['commerce-product']['dimensions-for-salesforce'] = array(
    'name'        => 'Dimensions (formatted for Salesforce)',
    'description' => 'Dimensions (formatted for Salesforce)',
  );
  $data['tokens']['commerce-product']['type-for-salesforce'] = array(
    'name'        => 'Product type (formatted for Salesforce)',
    'description' => 'Product type (formatted for Salesforce)',
  );
  $data['tokens']['commerce-product']['record-type-id-for-salesforce'] = array(
    'name'        => 'RecordTypeId for Salesforce',
    'description' => 'RecordTypeId for Salesforce',
  );
  $data['tokens']['address-field']['address-line-join'] = array(
    'name'        => 'Address lines concatenated',
    'description' => 'Address Line 1 and Address Line 2 on the same field',
  );
}


/**
 * implements hook_tokens
 */
function rcpar_salesforce_tokens($type, $tokens, array $data = array(), array $options = array()) {
  if (isset($options['language'])) {
    $language_code = $options['language']->language;
  }
  else {
    $language_code = LANGUAGE_NONE;
  }
  $sanitize = !empty($options['sanitize']);
  $replacements = array();

  if ($type == 'commerce-product' && !empty($data['commerce-product'])) {
    $product = $data['commerce-product'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'salesforce-id':
          $entity_wrapper = entity_metadata_wrapper('commerce_product', $product);
          $sf_mapping_object = $entity_wrapper->salesforce_mapping_object->value();
          $replacements[$original] = $sf_mapping_object->salesforce_id;
          break;

        case 'q-pool-for-salesforce':
          $ap_class = new ActProduct($product);
          $my_pool = $ap_class->getQuestionPool();
          if (!empty($my_pool)) {
            // TODO this is probably going to change, once it's more defined, put function in ActSProduct class
            $my_replacement = '(' . $my_pool->id . ') ' . $my_pool->title;
          }
          else {
            $my_replacement = '';
          }
          $replacements[$original] = $my_replacement;
          break;

        case 'act-sample-quizzes':
          if ($product->type == 'act_sample_materials') {
            $asp_class = new ActSampleProduct($product);
            $quizzes = $asp_class->getQuizzes();
            // TODO this is probably going to change, once it's more defined, put function in ActSampleProduct class
            $quiz_str = '';
            foreach ($quizzes AS $quiz) {
              $quiz_str .= '(' . $quiz->id . ') ' . $quiz->title . '';
            }
            // Strip off the last return.
            $my_replacement = substr($quiz_str, 0, -1);
            $replacements[$original] = $my_replacement;
          }
          break;

        case 'weight-for-salesforce':
          $entity_wrapper = entity_metadata_wrapper('commerce_product', $product);
          $my_replacement = '';
          $vals_arr = $entity_wrapper->field_weight->value();
          // Note that we are dealing w/ a string.
          $my_replacement .= $vals_arr['weight'];
          $my_replacement = rcpar_salesforce_number_format_helper($my_replacement);
          $my_replacement .= ' ' . $vals_arr['unit'];
          $replacements[$original] = $my_replacement;
          break;

        case 'dimensions-for-salesforce':
          $entity_wrapper = entity_metadata_wrapper('commerce_product', $product);
          $my_replacement = '';
          $vals_arr = $entity_wrapper->field_dimensions->value();
          $my_replacement .= rcpar_salesforce_number_format_helper($vals_arr['length']);
          $my_replacement .= ' x ';
          $my_replacement .= rcpar_salesforce_number_format_helper($vals_arr['width']);
          $my_replacement .= ' x ';
          $my_replacement .= rcpar_salesforce_number_format_helper($vals_arr['height']);
          $my_replacement .= ' ' . $vals_arr['unit'];
          $replacements[$original] = $my_replacement;
          break;

        case 'type-for-salesforce';
          // This is used to set the sf Product Family.
          // TODO: Update sf Product Family picklist w hook_commerce_product_type_insert, hook_commerce_product_type_delete & poss hook_commerce_product_type_update.
          $prod_type_map = rcpar_salesforce_get_prod_type_map();
          if (array_key_exists($product->type, $prod_type_map)) {
            $my_replacement = $prod_type_map[$product->type]['name'];
          }
          else {
            $my_replacement = '--None--';
          }
          $replacements[$original] = $my_replacement;
          break;

        case 'record-type-id-for-salesforce':
          $prod_type_map = rcpar_salesforce_get_prod_type_map();
          if (array_key_exists($product->type, $prod_type_map)) {
            $my_replacement = $prod_type_map[$product->type]['recordTypeId'];
          }
          else {
            $my_replacement = $prod_type_map['Master']['recordTypeId'];
          }
          $replacements[$original] = $my_replacement;
          break;

      }
    }
  }
  else {
    if ($type == 'commerce-line-item' && !empty($data['commerce-line-item'])) {
      $item_line = $data['commerce-line-item'];
      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'pricebook-entry-id':
            $entity_wrapper = entity_metadata_wrapper('commerce_line_item', $item_line);
            if ($item_line->type == 'product') {
              if (isset($entity_wrapper->commerce_product->field_sf_price_book_id)) {
                $product_pb_id = $entity_wrapper->commerce_product->field_sf_price_book_id->value();
                $replacements[$original] = $product_pb_id;
              }
            }
            else {
              if ($item_line->type == 'avatax') {
                $replacements[$original] = variable_get('salesforce_avatax_pricebook_id', '01u41000001X5vr');
              }
              else {
                if ($item_line->type == 'shipping') {
                  $replacements[$original] = variable_get('salesforce_shipping_pricebook_id', '01u41000003F4m0');
                }
              }
            }
            break;
          case 'product-price-raw':
            $entity_wrapper = entity_metadata_wrapper('commerce_line_item', $item_line);
            if ($item_line->type == 'product') {
              $price = $entity_wrapper->commerce_unit_price->value();
              foreach ($price['data']['components'] as $component) {
                if ($component['name'] == 'base_price') {
                  $amount = $component['price']['amount'];
                }
              }
              $replacements[$original] = $amount / 100;
            }
            break;
        }
      }
    }
    else {
      if ($type == 'entity' && !empty($data['entity'])) {
        if ($data['entity_type'] == 'node') {
          $node = $data['entity'];
          foreach ($tokens as $name => $original) {
            if (strpos($name, 'array-field-join') !== FALSE) {
              $replacement = '';
              $token_parts = explode(':', $name);
              $field_name = $token_parts[1];
              $separator = $token_parts[2];
              $entity_wrapper = entity_metadata_wrapper('node', $node);
              if (isset($entity_wrapper->{$field_name})) {
                $values = $entity_wrapper->{$field_name}->value();
                $list = $entity_wrapper->{$field_name}->optionsList();
                foreach ($values as $val) {
                  if ($replacement != '') {
                    $replacement .= $separator;
                  }
                  $replacement .= $list[$val];
                }
                $replacements[$original] = $replacement;
              }
            }
            elseif ($name == 'sf-opportunity-type-id'){
              $partnerObj = new RCPARPartner($node);
              if ($partnerObj->isPartnerTypeUniversity()) {
                // TODO: create a dynamic updatable table for partner object type ids
                // (Although, this values seems to be the same across sandboxes).
                $type_id = '01241000001IPCnAAO';
              }
              else {
                // "Firm" for all other types
                $type_id = '01241000001IPDRAA4';
              }
              $replacements[$original] = $type_id;
            }
            elseif ($name == 'field_billing_type-key') {
              $value = '';
              $node_wrapper = entity_metadata_wrapper('node', $node);
              if (isset($node_wrapper->field_billing_type)){
                $value = $node_wrapper->field_billing_type->value();
              }
              $replacements[$original] = $value;
            }
            elseif ($name == "field_college_state_list_label") {
              $allowed = list_allowed_values( field_info_field('field_college_state_list'), null, 'node', $node );
              $entity_wrapper = entity_metadata_wrapper('node', $node);
              if ($state_code = $entity_wrapper->field_college_state_list->value()){
                $label = $allowed[$state_code];
                $replacements[$original] = $label;
              }
            }
            elseif ($name == "cloudamp__data__c") {
              $replacements[$original] = $node->cloudamp__data__c;
            }
          }
        }
        else {
          if ($data['entity_type'] == 'user') {
            $user = $data['entity'];
            foreach ($tokens as $name => $original) {
              switch ($name) {
                case 'salesforce-opportunity-id':
                case 'salesforce-account-id':
                  // We need to get the partner that was used to create this user (in case it exists)
                  // If we find at least one product with an associated partner
                  // we assume that this user was created using that partner
                  $partner_id = NULL;
                  if (isset($user->uid)) { // If the user is just being created, there may be no uid (I think...not certain)
                    $u = new User($user->uid);
                    $partneredEnts = $u->getValidEntitlements()->filterByPartnered();
                    if ($partneredEnts->isNotEmpty()) {
                      $partner_id = $partneredEnts->getFirst()->getPartnerId();
                    }
                  }
          
                  // We might be creating the user so it doesn't have entitlements yet
                  // On that case we would need to check the current order to see if it's associated with a partner
                  if (!$partner_id && $user->is_new){
                    $partner_helper = new RCPARPartner();
                    if ($partner_helper->isLoaded() ){
                      $partner_id = $partner_helper->getNid();
                    }
                  }

                  // Here the logic is different for each of the tokens
                  if ($name == 'salesforce-opportunity-id'){
                    $sf_partner_id = '';
                    if ($partner_id){
                      $partner_node = node_load($partner_id);
                      $partner_wrapper =  entity_metadata_wrapper('node', $partner_node);
                      if(isset($partner_wrapper->salesforce_mapping_object)){
                        $sf_obj_info = $partner_wrapper->salesforce_mapping_object->value();
                        $sf_partner_id = $sf_obj_info->salesforce_id;
                      }
                    }
                    $replacements[$original] = $sf_partner_id;
                  }
                  // salesforce-account-id
                  else {
                    // Try to read partnerSfId from $user->data
                    if (!empty($user->data['partnerSfId'])){
                      $sf_account_id = $user->data['partnerSfId'] ;
                    }else {
                      $sf_account_id = variable_get('salesforce_default_account_id');
                      if ($partner_id) {
                        $partner_node = node_load($partner_id);
                        $partnerObj = new RCPARPartner($partner_node);
                        $sf_account_id = $partnerObj->getSalesForceAssociatedAccId();
                      }
                    }
                    $replacements[$original] = $sf_account_id;
                  }
                break;
                case 'field-aud-exam-date_as-date':
                case 'field-far-exam-date_as-date':
                case 'field-bec-exam-date_as-date':
                case 'field-reg-exam-date_as-date':
                  $name = str_replace('_as-date', '', $name);
                  $name = str_replace('-', '_', $name);
                  $entity_wrapper = entity_metadata_wrapper('user', $user);
                  $v = $entity_wrapper->{$name}->value();
                  // Since I can't find a way to send a null value to a date field on salesforce
                  // we just send the date corresponding to the timestamp 0
                  // when the user didn't entered a date on this field
                  $date = new DateTime();
                  $date->setTimestamp(0);
                  if ($v && $v != '---') {
                    try {
                      $date = new DateTime($v);
                    }
                      // User is technically allowed to put things that are not dates on the field
                      // on this token types and for those cases, we just want to replace the value with null
                    catch (Exception $e) {
                      // We don't do nothing here, we just want to catch the exception
                    }
                  }
                  $replacement = $date->format('Y-m-d');
                  $replacements[$original] = $replacement;
                  break;

                case 'timezone':
                  $replacements[$original] = property_exists($user, 'timezone') ? $user->timezone : '';
                  break;

                case 'drupal-created':
                  // Salesforce expects an ISO 8601 date format like '2004-02-12T15:19:21+00:00'
                  $replacements[$original] = gmdate('c', $user->created);
                  break;
                  
                case 'logged-days':
                  if(isset($user->data['drupal_logged_days'])){
                    $days_counter = $user->data['drupal_logged_days']['counter'];
                  }else{
                    $days_counter = null ;
                  }
                  $replacements[$original] = $days_counter ;
                  break;
                  
                case 'aud-progress':
                case 'far-progress':
                case 'bec-progress':
                case 'reg-progress':
                  // Determine the section based on token name
                  $section = strtoupper(explode('-',$name)[0]);
                  
                  // Get the video progress for this section
                  $u = new User($user);
                  $ents = $u->getValidEntitlements()->filterByOnlineCourse()->filterBySectionName($section);
                  $progress = Course::getProgressPercentageForUserSku($u, $ents, $section);
                  
                  if (!is_null($progress)) {
                    $replacements[$original] = $progress;
                  }
                  break;
                  
                case 'field_college_state_list_label':
                  $allowed = list_allowed_values( field_info_field('field_college_state_list'), null, 'user', $user );
                  $entity_wrapper = entity_metadata_wrapper('user', $user);
                  if ($state_code = $entity_wrapper->field_college_state_list->value()){
                    $label = $allowed[$state_code];
                    $replacements[$original] = $label;
                  }
                  break;
                
              }
            }
          }
        }
      }
      else {
        if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
          _rcpar_salesforce_tokens_commerce_orders($tokens, $data, $replacements);
        }
        else {
          if ($type == 'commerce-payment-transaction' && !empty($data['commerce-payment-transaction'])) {
            foreach ($tokens as $name => $original) {
              if ($name == 'amount-for-salesforce') {
                $transaction = $data['commerce-payment-transaction'];
                $replacements[$original] = $transaction->amount / 100;
              }
            }
          }else{
            // @TODO Review the current formatting style too much levels in deep
            // If we're generating tokens for an address field, extract the address data
            if ($type == 'address-field' && !empty($data['address-field'][$language_code]) && is_array($data['address-field'][$language_code])) {
              $address = reset($data['address-field'][$language_code]);
              foreach ($tokens as $name => $original) {
                switch ($name) {
                  case 'address-line-join':
                   $replacement = '';
                   $replacement .= $sanitize ? check_plain($address['thoroughfare']) : $address['thoroughfare'];
                   $replacement .= "\n";
                   $replacement .= $sanitize ? check_plain($address['premise']) : $address['premise'] ;
                   $replacements[$original] = $replacement ;
                   break;
                }
              }
            }
          }
        }
      }
    }
  }
  return $replacements;
}


/**
 * @param $tokens
 * @param $data
 * @param $replacements
 */
function _rcpar_salesforce_tokens_commerce_orders($tokens, array $data = array(), &$replacements) {
  
  foreach ($tokens as $name => $original) {
    switch ($name) {
      case 'salesforce-opportunity-id' :
        $order = $data['commerce-order'];
        $entity_wrapper = entity_metadata_wrapper('commerce_order', $order);
        if ($entity_wrapper->field_partner_profile->value()
          &&
          isset($entity_wrapper->field_partner_profile->salesforce_mapping_object)
          &&
          $entity_wrapper->field_partner_profile->salesforce_mapping_object->value()
        ) {
          $sf_obj_info = $entity_wrapper->field_partner_profile->salesforce_mapping_object->value();
          $sf_partner_id = $sf_obj_info->salesforce_id;
        }
        else {
          $sf_partner_id = '';
        }
        $replacements[$original] = $sf_partner_id;
        break;
      
      case 'salesforce-account-id' :
        $order = $data['commerce-order'];
        $entity_wrapper = entity_metadata_wrapper('commerce_order', $order);
        if ($entity_wrapper->field_partner_profile->value()) {
          $partner = $entity_wrapper->field_partner_profile->value();
          $partnerObj = new RCPARPartner($partner);
          $sf_account_id = $partnerObj->getSalesForceAssociatedAccId();
        }
        elseif ($entity_wrapper->field_uw_sf_id->value()) {
          $sf_account_id = $entity_wrapper->field_uw_sf_id->value();
        }
        else {
          // If sf_account_id still empty, set the default id from drupal variable
          $sf_account_id = variable_get('salesforce_default_account_id');
        }
        $replacements[$original] = $sf_account_id;
        break;
      
      case 'lead-status-all' :
        $replacements[$original] = rcpar_salesforce_get_lead_status_partner($data['commerce-order']);
        break;
    }
  }
}


/**
 * Get specific lead status type according the partner in an order
 * Code extracted from deprecated hubspot_get_hs_lead_status_partner() function
 * @param  object $order
 * @return String $hs_lead_status id of lead status*
 */
function rcpar_salesforce_get_lead_status_partner ($order){
  $lead_status = FALSE;
  try  {
    $o = new RCPARCommerceOrder($order);
    if ($partner = $o->getSavedPartnerObject()) {
      $p = new RCPARPartner($partner);
      if (!empty($results['commerce_payment_transaction'])) {
        $transactions = entity_load('commerce_payment_transaction', array_keys($results['commerce_payment_transaction']));
        foreach ($transactions as $transaction) {
          $payment_method = $transaction->payment_method;
        }
      }
      // Concatenate partner_type , payment_method  and billing_type to determine Lead Status case
      $partner_type = $p->getPartnerType();
      $payment_method = $o->getCommercePaymentTransactionName();
      $billing_type = $p->getBillingType();
      $partner_case_str = "{$partner_type}-{$payment_method}-{$billing_type}";

      /**
       * Partner type options:
       *   1 : Firm/Partner
       *   2 : Normal
       *   3 : University
       *   4 : Publisher
       * Billing type options:
       *   creditcard : Credit Card/Affirm
       *   directbill : Direct Bill
       *   freetrial : Free Access
       */
     
      switch ($partner_case_str) {
        //  1 : Firm/Partner
        case firm_partner.'-rcpar_partners_payment_freetrial-freetrial':
        case firm_partner.'-rcpar_partners_payment-freetrial':
        case firm_partner.'-enroll_flow_uworld_payment-freetrial':
          $lead_status = 'FSL Lead';
          break;
        case firm_partner.'-rcpar_partners_payment-directbill':
          $lead_status = 'FDB Lead';
          break;
        case firm_partner.'-authnet_aim-creditcard':
        case firm_partner.'-rcpar_partners_payment-creditcard':
          $lead_status = 'FCC Lead';
          break;
        case firm_partner.'-rcpar_partners_payment_affirm':
        case firm_partner.'-affirm-creditcard':
          $lead_status = 'FFIN Lead';
          break;
        // 2 : Normal
        case free_trial.'-authnet_aim-freetrial':
        case free_trial.'-rcpar_partners_payment_freetrial-freetrial':
        case free_trial.'-authnet_aim-freetrial':
        case free_trial.'-enroll_flow_uworld_payment-freetrial':
          $lead_status = 'FreeTrial Lead';
          break;
        // 3 : University
        case campus_sample_course.'-rcpar_partners_payment_freetrial-freetrial':
        case campus_sample_course.'-rcpar_partners_payment-freetrial':
          $lead_status = 'ASL Lead';
          if($p->isActPartner()){
            $lead_status = 'ACT Lead';
          }
          break;
        case campus_sample_course.'-rcpar_partners_payment-directbill':
          $lead_status = 'ASLDB Lead';
          if($p->isActPartner()){
            $lead_status = 'ACTDB Lead';
          }
          break;
        case campus_sample_course.'-authnet_aim-creditcard':
        case campus_sample_course.'-rcpar_partners_payment-creditcard':
          $lead_status = 'ASLCC Lead';
          if($p->isActPartner()){
            $lead_status = 'ACTCC Lead';
          }
          break;
        case campus_sample_course.'-rcpar_partners_payment_affirm':
        case campus_sample_course.'-affirm-creditcard':
          $lead_status = 'ASLFIN Lead';
          if($p->isActPartner()){
            $lead_status = 'ACTFIN Lead';
          }
          break;
        // 4 : Publisher
        case publisher.'-rcpar_partners_payment_freetrial-freetrial':
        case publisher.'-rcpar_partners_payment_freetrial':
          $lead_status = 'PAL Lead';
          break;
        case publisher.'-rcpar_partners_payment-directbill':
          $lead_status = 'PDB Lead';
          break;
        case publisher.'-authnet_aim-creditcard':
        case publisher.'-rcpar_partners_payment-creditcard':
          $lead_status = 'PCC Lead';
          break;
        case publisher.'-rcpar_partners_payment_affirm':
        case publisher.'-affirm-creditcard':
          $lead_status = 'PFIN Lead';
          break;
        default:
          $lead_status = 'Organic';
          break;
      }
    }else{
      $lead_status = 'Organic';
    }
    return $lead_status;
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
  }
}