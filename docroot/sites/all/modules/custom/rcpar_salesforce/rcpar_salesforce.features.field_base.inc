<?php
/**
 * @file
 * rcpar_salesforce.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function rcpar_salesforce_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_account_tier'.
  $field_bases['field_account_tier'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_account_tier',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'A' => 'A',
        'B' => 'B',
        'C' => 'C',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_account_type'.
  $field_bases['field_account_type'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_account_type',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        '01241000001IQcd' => 'Alliances',
        '01241000001IPCd' => 'Competitor',
        '01241000001IPCe' => 'Firm',
        '01241000001IQcn' => 'Government',
        '01241000001IQcx' => 'Organization',
        '01241000001IQcs' => 'Private',
        '01241000001IQd2' => 'Publisher Partner',
        '01241000001IQci' => 'State Society',
        '01241000001IPCg' => 'University',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_partner_account'.
  $field_bases['field_partner_account'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_partner_account',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'title',
          'type' => 'property',
        ),
        'target_bundles' => array(
          'sf_account' => 'sf_account',
        ),
      ),
      'target_type' => 'sf_account',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_sf_price_book_id'.
  $field_bases['field_sf_price_book_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_sf_price_book_id',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
