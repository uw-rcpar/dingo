<?php

/**
 * Implements hook_drush_command().
 */
function rcpar_salesforce_drush_command() {

  $items['rcpar-salesforce-remove-orphan-il'] = array(
    'description' => 'Removes orphaned order item lines from Salesforce.',
  );

  $items['rcpar-salesforce-resync-order'] = array(
    'description' => 'Re push order information to Salesforce.',
    'arguments' => array(
      'order_id' => 'Drupal Id of the order to resync.',
    ),
    'options' => array(
      'clear_order_mapping_first' => 'Clear the order and related entities before re syncing them.',
    ),
  );
  
  $items['rcpar-salesforce-resync-entitlement-dates'] = array(
    'description' => 'Re sync course dates based on user entitlements',
    'options' => array(
      'user_ids' => 'User Ids', // Comma separated list of user ids
    ),
  );

  return $items;
}

/**
 * Callback for the rcpar-salesforce-remove-orphan-il command.
 */
function drush_rcpar_salesforce_remove_orphan_il() {
  $sfapi = salesforce_get_api();

  // Not authorized, we need to bail this time around.
  if (!$sfapi->isAuthorized()) {
    return;
  }

  $deleted_rows = 0;
  // Get the orphaned item lines id
  $result = db_query("
    SELECT * from salesforce_mapping_object sfmo 
      WHERE entity_type = 'commerce_line_item'
    AND    
    NOT EXISTS(SELECT * FROM commerce_line_item cli WHERE cli.line_item_id = sfmo.entity_id);
  ");
  foreach ($result as $record) {
    $mapping = salesforce_mapping_object_load_by_drupal($record->entity_type, $record->entity_id);
    try {
      $sfapi->objectDelete('OrderItem', $record->salesforce_id);
    }
    catch(SalesforceException $e) {
      watchdog_exception('rcpar_salesforce_drush', $e);
      continue;
    }
    $mapping->delete();
    $deleted_rows++;
  }
}

/**
 * Callback for the rcpar-salesforce-resync-order command.
 */
function drush_rcpar_salesforce_resync_order($order_id, $clear_order_mapping_first = FALSE) {
  try {
    $order = commerce_order_load($order_id);
    if ($clear_order_mapping_first){
      rcpar_salesforce_clear_order_mapping_info($order);
    }
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    _rcpar_salesforce_add_disc_to_il($order_wrapper);
    rcpar_salesforce_force_order_push($order_id);
    foreach ($order_wrapper ->commerce_line_items as $line_item_wrapper) {
      if($line_item_wrapper->type->value() == 'avatax') {
        _rcpar_salesforce_cleanup_taxes($line_item_wrapper);
      }
    }
    _rcpar_salesforce_cleanup_order($order_id);
  }
  catch (EntityMetadataWrapperException $e) {
    drush_log('Exception catched: ' . $e->getMessage(), 'error');
  }
}




/**
 * Callback for the rcpar-salesforce-resync-order command.
 */
function drush_rcpar_salesforce_resync_entitlement_dates() {
  try {
    if ($user_ids_csv = drush_get_option('user_ids','')){
        $user_ids = explode(",", $user_ids_csv);
        foreach ($user_ids as $uid) {
          drupal_set_message("Fixing user $uid ");
          if ($u = new RCPAR\User($uid)) {
            $entitlements = $u->getAllEntitlements()->filterByOnlineCourse();
            foreach ($entitlements as $ent) {
              drupal_set_message("Fixing user $uid / entitlement " . $ent->getId());
              user_entitlements_save_salesforce_dates($ent);   
            }
            drupal_set_message("Fixed user $uid OK");
          }else{
            drupal_set_message("User $uid not found");
          }
        }
    }
  }
  catch (EntityMetadataWrapperException $e) {
    drush_log('Exception catched: ' . $e->getMessage(), 'error');
  }
}