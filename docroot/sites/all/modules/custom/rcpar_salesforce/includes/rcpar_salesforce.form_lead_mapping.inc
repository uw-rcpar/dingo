<?php
return [
  'firstname' => 'FirstName',
  'lastname' => 'LastName',
  'email' => 'Email',
  'phone' => 'Phone',
  'audience_type' => 'Audience_Type__c',
  'graduation_date' => 'Graduation_Date__c',
  'study_date' => 'Study_for_the_CPA_Exam_Date__c',
  'page_url' => 'Page_URL__c',
  'uid' => 'Drupal_UID__c',
  'campus' => 'Campus__c',
  'campus_state' => 'Campus_State__c',
  'firm' => 'Employer__c',
  'jobtitle' => 'Title',
  'what_subject_do_you_teach' => 'What_Course_s_do_you_teach__c',
  'what_are_you_interested_in_learning_more_about' => 'Interested_in__c',
  'comments' => 'Comments__c',
  'state' => 'State',
  'country' => 'Country',
  'bdr_event' => 'BDR_Event__c',
  'cloudamp__data__c' => 'cloudamp__data__c',
  'college_state' => 'College_State__c',
  'university_college' => 'University_College__c', //Existing school on the list "College Name Selection" @deprecated
  'college_name' => 'College_name__c', //If school is not listed
  'university_account' => 'university_account__c',
  'partner_id' => 'Account__c'
];