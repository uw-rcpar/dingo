<?php

use RCPAR\Course;
use RCPAR\User;

/**
 * Push course progress percent to salesforce
 * @param int $uid
 * @param bool $background
 */
function rcpar_salesforce_push_course_percentages($uid) {
  if (!$uid) {
    return;
  }
  $sfapi = salesforce_get_api();
  if (!$sfapi->isAuthorized()) {
    die("not auth");
   // Not authorized
   return;
  }
  
  if ($mapping_object = salesforce_mapping_object_load_by_drupal('user', $uid)){
    // Get all video proress for standard online courses for this user (AUD, BEC, FAR, REG)
    $u = new User($uid);
    $ents = $u->getValidEntitlements()->filterByOnlineCourse();
    $progress = Course::getProgressPercentageForUser($u, $ents);
    
    // Update SF for sections where we have data
    foreach ((array)$progress as $data) {
      $salesforceFieldName = sprintf("%s_Completion_Percentage__c", strtoupper($data['sku']));
      $updates[$salesforceFieldName] = $data['percentage'];
    }
    $sfapi->objectUpdate("Contact", $mapping_object->salesforce_id, $updates);
  }
}



/**
 * Helper function that calculates the closest floor value in array
 * Null if  $search is the lowest
 *
 * @param integer $search
 * @param array $array
 *
 * @return integer
 */
function _rcpar_salesforce_closest_floor($search, $array) {
  rsort($array);
  foreach ($array as $flag_value) {
    if ($search >= $flag_value) {
      return $flag_value;
    }
  }
  return null ;
}

/**
 * Removes existing entries for course_progress_queue to avoid duplicate operations on queue system
 * Compares items by serialized object
 * This is not supported by queue API interface, so requires this customization
 * @param $item
 */
function rcpar_salesforce_course_progress_queue_clean_duplicates($item) {
  $serial = serialize($item);
  db_query("DELETE FROM {queue} WHERE name = 'rcpar_salesforce_course_progress' AND data = :value ", [":value" => $serial] );
}