<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Defines the available question pools in the system.
 *
 * @return array
 * - An array of loaded cp_question_pool entities keyed by some unique id
 */
function hook_question_pool() {
  $question_pools = array();

  // Load an entity from the DB
  $question_pools['arbitrary_identifier'] = entity_load_single('cp_question_group', $id);

  // Build an entity programmatically
  $entity_type = 'cp_question_group';
  $entity = entity_create($entity_type, array('type' => 'cp_question_pool'));
  $wrapper = entity_metadata_wrapper($entity_type, $entity);
  // Question node IDs
  $wrapper->field_cp_question = array(
    0 => 3020953,
    1 => 3021008,
    2 => 3021025,
    3 => 3023968,
    4 => 3025253,
    5 => 3025784,
    6 => 3025842,
  );
  $wrapper->title = 'My title';
  $wrapper->uid = 1; // Author's user id
  $wrapper->created = REQUEST_TIME;
  $wrapper->changed = REQUEST_TIME;
  $question_pools['my-title'] = $wrapper->value();

  return $question_pools;
}