<?php
/**
 * @file
 * rcpar_cp.features.inc
 */

/**
 * Implements hook_eck_entity_type_info().
 */
function rcpar_cp_eck_entity_type_info() {
  $items = array(
    'cp_question_group' => array(
      'name' => 'cp_question_group',
      'label' => 'Curation Platform Question Grouping',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
  );
  return $items;
}
