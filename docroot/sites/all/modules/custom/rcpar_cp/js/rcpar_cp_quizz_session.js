( function ($) {

  var allowPageUnload = false;

  window.onbeforeunload = function (evt) {
    // Note that the message on the exit dialog is not supported on every browser
    if (!allowPageUnload) {
      return "Leaving this page will end the test.\nAre you sure you want to continue?";
    }
  }

  Drupal.behaviors.rcpar_cp_quizz_session = {
    attach: function (context, settings) {

      $('a, button', context).click(function(e){
        var allowed_exit_btn_ids = ['edit-next', 'edit-back', 'submit-test-link'];

        // We need to know if the user is trying to close the countdown timer
        var timeup_warning_closing = $(this).hasClass('dismiss') && $(this).parents('#time-limit-reached').length > 0;

        // If the user is leaving using the provided navigation or is exiting after the countdown timer reached zero
        // we allow them to leave without showing them the warning
        if( allowed_exit_btn_ids.indexOf($(this).attr('id')) != -1 || $(this).hasClass('pager-link') || timeup_warning_closing) {
          allowPageUnload = true;
        }
        else {
          allowPageUnload = false;
        }
      });
    }
  }

}(jQuery));