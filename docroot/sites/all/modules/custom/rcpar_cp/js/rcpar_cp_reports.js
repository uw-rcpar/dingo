/**
 * Created by Reed on 11/22/16.
 */
( function ($) {
  Drupal.behaviors.rcpar_cp_reports = {
    attach: function (context, settings) {

      /**
       * Form labels on Reports page need active class
       *
       */
      // results type radios
      // add active-label class to first radio if it is checked
      if($('.form-item-results-type .form-item-results-type:nth-child(1) input').is(':checked')) {
        $('.form-item-results-type .form-item-results-type:nth-child(1) label').addClass('active-label');
      }

      $('#rcpar-cp-author-quiz-results-form .form-item-results-type label.option').once().on('click', function () {
        // if the label clicked on is not the active label,
        // toggle the sidebar grouping radios and the results table
        if (!$(this).hasClass('active-label')) {
          $('.form-item-quiz-grouping.form-type-radios, #results-table').toggle();
        }
        // remove active-label class from whichever radio has it
        // and add it to the one that is clicked
        $('.form-item-results-type label.option').removeClass('active-label');
        $(this).addClass('active-label');
      });

      // quiz grouping radios
      // add active-label class to first radio if it is checked
      if($('.form-item-quiz-grouping:nth-child(1) input').is(':checked')) {
        $('.form-item-quiz-grouping:nth-child(1) label').addClass('active-label');
      }
      // when clicked, remove active-label class from whichever radio has it
      // and add it to the one that is clicked
      $('.form-item-quiz-grouping label.option').on('click', function () {
        $('.form-item-quiz-grouping label.option').removeClass('active-label');
        $(this).addClass('active-label');
      });
      // END form labels on Reports page need active class


      // show more / show less of section list on Lecture Progress reports page
      $('body').on('click', '#partner-section-show-more', function () {
        $('.all-sections').height('auto');
        $('.show-more').hide();
        $('.show-less').show();
        return false;
      });
      $('body').on('click', '#partner-section-show-less', function () {
        $('.all-sections').height('20px');
        $('.show-more').show();
        $('.show-less').hide();
        return false;
      });
      // END show more / show less of section list on Lecture Progress reports page
    }
  }

}(jQuery));
