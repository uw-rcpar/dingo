/**
 * Created by Susan on 01/11/17.
 */
( function ($) {
  Drupal.behaviors.rcpar_cp_timer = {
    attach: function (context, settings) {

      // change the Elapsed timer classes to use the Countdown timer classes
      // so that we don't have to override the form--question_display.tpl.php file
      if ($('.digital-timer .time-elapsed', context).length) {
        $('.digital-timer .time-elapsed').addClass('time-remaining').html('Time Remaining');
        $('.digital-timer .hours-elapsed-container').addClass('hours-remaining-container');
        $('.digital-timer .hours-elapsed').addClass('hours-remaining');
        $('.digital-timer .minutes-elapsed-container').addClass('minutes-remaining-container');
        $('.digital-timer .minutes-elapsed').addClass('minutes-remaining');
        $('.digital-timer .seconds-elapsed-container').addClass('seconds-remaining-container');
        $('.digital-timer .seconds-elapsed').addClass('seconds-remaining');
      }

      var redirect = Drupal.settings.rcpar_cp_timer;

      // add body class when time limit reached modal is open
      // so we can change the css for the modal backdrop
      $('#time-limit-reached').on('show.bs.modal', function () {
        $('body').addClass('time-limit-modal-open');
        // change the text inside the modal to say quiz rather than exam
        $('#time-limit-reached .modal-body').html('You have run out of time to complete the quiz.');

        // override the dismiss redirection
        $('#time-limit-reached .dismiss').click(function() {
          var redirect_url = Drupal.settings.rcpar_cp_timer;
          window.location.pathname = redirect_url;
        });
      });
      // remove the body class when modal is closed
      $('#time-limit-reached').on('hide.bs.modal', function () {
        $('body').removeClass('time-limit-modal-open');
      });

    }
  }

}(jQuery));
