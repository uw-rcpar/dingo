(function ($, Drupal, window, document, undefined) {
  $(document).ready(function () {
    // Change to the product page selected in the dropdown
    $('.act-dropdown select').change(function (e) {
      window.location.href = $(this).val();
    });
  }); //END - document.ready
})(jQuery, Drupal, this, this.document); //END - Closure
