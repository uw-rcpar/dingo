/**
 * Created by Reed on 1/10/17.
 */
( function ($) {
  Drupal.behaviors.rcpar_cp_q_preview = {
    attach: function (context, settings) {
      if($("#cp-preview-question").length >  0 ){
        $(".page-act-quiz-take .digi-timer", context).hide();
        $(".page-act-quiz-take .submit-test-link", context).hide();
      }
      $('a.rcpar-cp-view-solution', context).once('cbQuestionPreview', function () {
        var viewSolutionButE = $('a.rcpar-cp-view-solution'); // the view solution button element
        // For non-MCQ ("mpq") questions
        if (!viewSolutionButE.hasClass('is-mpq')) {
          // add non-mpq click listener for VIEW SOLUTION button supplied in rcpar_cp_question_preview()
          viewSolutionButE.on('click', function () {
            cpShowQpreviewSolution();
            return false;
          });
        }
      });

      /**
       * called by click listener for VIEW SOLUTION button
       */
      function cpShowQpreviewSolution() {

        // the jQuery object holding the view solution modal
        var ipqViewSolE = $('#ipq-view-solution', '#ipq-container');
        ipqViewSolE.modal('show');

        ipqViewSolE.draggable({
          handle: '.modal-header'
        });

        // modify the template that ipq provides, NOTE: we only need/want to modify the template once
        ipqViewSolE.once('addViewSolutionBg', function () {
          $('#ipq-view-solution .modal-footer', context).html('<button id="cp-q-view-solution-close" type="button" class="close-btn btn btn-success">Close</button>');
          $('h3', '#ipq-view-solution').remove(); // remove all header msgs and add our own below

          $('.modal-header', '#ipq-view-solution').append('<h3 id="termsLabel">VIEW SOLUTION</h3>');

          // the next line would add our default backdrop to MPQ questions but not TBS, so for consistency it's not added to either
          // ipqViewSolE.append('<div class="modal-backdrop" style="background-color: #000;opacity: .5 !important;z-index: -10000;"></div>');

          $('#cp-q-view-solution-close').on('mousedown', function () {
            ipqViewSolE.modal('hide');
          });

        });
      }

    }

  }

}(jQuery));
