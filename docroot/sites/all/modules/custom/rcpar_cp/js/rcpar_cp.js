/**
 * Remove Drupal's default sending of 'ajax_html_ids[]' in ajax requests.
 *
 * This block of code is a fairly major hack to Drupal's ajax API, just for the
 * quiz creation part of curation platform. This form seems to easily exceed
 * PHP's max_input_vars limit (even after it was increased to 2500) when all
 * of the questions from all parts/sections/topics are shown. The primary
 * reason
 * for this is the sending of all of the ajax_html_ids[]. These values are
 * useless to us and are problematic to Drupal's ajax framework at best (for
 * example, see "drupal_html_id() considered harmful; remove ajax_html_ids to
 * use GET (not POST) AJAX requests" - https://www.drupal.org/node/1305882).
 *
 * The solution here is to store the original beforeSerialize method and take
 * it over with our own. We call the original method, then remove the
 * ajax_html_ids in our own method. Note: I tried to do this with the available
 * 'beforeSubmit' method that seems to be made to be overridden, but it appears
 * to be too late in the chain to alter data to be sent.
 * -jd
 */
try {
  if (Drupal.ajax != undefined) {
    Drupal.ajax.prototype.cpBeforeSerialize = function (element, options) {
      // Call the original beforeSerialize method
      Drupal.ajax.prototype.originalBeforeSerialize(element, options);

      // Remove ajax_html_ids[] from the data about to be sent.
      options.data['ajax_html_ids[]'] = null;
    };

    // Store the original beforeSerialize method and then replace it with our own.
    Drupal.ajax.prototype.originalBeforeSerialize = Drupal.ajax.prototype.beforeSerialize;
    Drupal.ajax.prototype.beforeSerialize = Drupal.ajax.prototype.cpBeforeSerialize;
  }
}
catch(e) {}

/**
 * Created by Reed on 11/22/16.
 */
( function ($) {
  Drupal.behaviors.rcpar_cp = {
    attach: function (context, settings) {
      // close any open datepickers
      $('#form-content-container', context).once('closeDatePicker', function () {
        $('div.ui-datepicker').remove();
      });
      // draggable handle hover:
      // we change the border color on sibling div
      // it acts as a bg for the drag handle
      $(".tabledrag-handle", context).once('tdHoverFunc', function () {
        // NOTE: if this doesn't use .once, it will get called multiple times,
        // as many times as there are tabledrag-handles
        $(this).hover(
          function () {
            $(this).next('.rcpar-cp-qx').addClass('drag-hover');
          }, function () {
            $(this).next('.rcpar-cp-qx').removeClass('drag-hover');
          });
      });
      if ($('#questions-selected-table', context).length) {
        // draggable handle focus
        // adding a class to apply css when being dragged
        $('body.rcpar-cp-quiz-form').once('tableDragProtoClassAdd', function () {
          // adding .once to the body keeps the prototype from being adjusted more than once (which is all that's needed)
          Drupal.tableDrag.prototype.onDrag = function () {
            $(this.rowObject.element).find('.rcpar-cp-qx').addClass('moving');
          }
          Drupal.tableDrag.prototype.onDrop = function () {
            $(this.rowObject.element).find('.rcpar-cp-qx').removeClass('moving');
            renumberPreviewQuestions();
          }
        });
      }

      /**
       * Functionality to add ordering numbers to selected questions.
       */
      // Add a div to the tabledrag handles to hold the questions' order labels.
      // Use .once so that multiple labels won't be added.
      $('#questions-selected-table', context).once('setUpQuestionOrder', function () {
        // Use setTimeout so that Drupal's tabledrag js functions will finish drawing the table so there's handles to work on.
        setTimeout(function () {
          $('.tabledrag-handle .handle').before('<div class="rcparCpQorder"></div>');
          // Renumber the questions' order
          renumberPreviewQuestions();
        }, 1);
      });

      /**
       * Renumbers the questions in the quiz preview column.
       * Called by custom Drupal.tableDrag.prototype.onDrop.
       */
      function renumberPreviewQuestions() {
        $('.rcparCpQorder').each(function (index, value) {
          // set the left padding to more if we're dealing with 1 digit, and add the label
          $(this).css('paddingLeft', ((index > 8) ? '2px' : '6px')).html(index + 1);
        });
      }

      $('.rename-quiz', context).once('renameQuiz', function () {
        // this will be called multiple times if .once is not used
        $(this).on('click', function () {
          $('#current-name-container').hide();
          var newNameContE = $('#new-name-container');
          newNameContE.show();
          $('input[id^=edit-new-name]', newNameContE).select();
          // submit on return/enter
          newNameContE.on('keypress', function (e) {
            if (e.which == 13) {
              $('button', newNameContE).mousedown();
              return false;
            }
          });
          return false;
        });
      });

      // element validation error for an empty new name (from _cp_quiz_timer_active_e_validate)
      if ($('#name-container .messages.error', context).length) {
        // expose the new name field & submit button
        $('.rename-quiz').click();
        // select the newname field
        $('input[id^=edit-new-name]', '#new-name-container').select();
      }

      if ($('.rcpar-cp-qx', context).length) {
        // add a mousedown handler to save the question nid to send with ajax call
        $('.rcpar-cp-qx button', context).on('mousedown', function () {
          var q_no = $(this).parent().parent().attr('id').substr(11);
          setQuestionNo(q_no);
        });

        // reset filters on the questions
        filterCbChangeFunc();
      }

      // add a mousedown handler to save visible question nids to send with ajax call
      $('button.batch-link', context).on('mousedown', function (e) {
        e.stopImmediatePropagation(); // this keeps it from firing twice

        $('input[name="question_selected"]', '#question-available-wrapper').val('');
        // if ($(this).hasClass('batch-link-bank-add')){
        var mySelector1 = (($(this).hasClass('batch-link-bank-add')) ? 'div.cp-qx-add' : 'div.cp-qx-remove');
        var mySelector2 = (($(this).hasClass('batch-link-selected-remove')) ? '#questions-selected-container' : '#questions-fieldset-wrapper');

        $(mySelector1, mySelector2).not('.rcpar_cp_hide').each(function () {
          var qSelE = $('input[name="question_selected"]', '#question-available-wrapper');
          var origVal = qSelE.val();

          qSelE.val(origVal += ($(this).attr('id').substr(11) + ','));
        });
        // js_initiated replaces Drupal's default button mousedown event, set in rcpar_cp_form_rcpar_cp_quiz_form_alter
        // to assure that all visible question nids have been set before the callback is triggered
        $(this).trigger('js_initiated');
      });


      /**
       * Functionality for filtering questions by type
       */
      $('#cp-quiz-filter-q-but', context).once('cbQuizFilterBut', function () {

        // show or hide filter checkboxes on clicking function
        $('#cp-quiz-filter-q-but').on('click', function () {
          var filterCbE = $('#cp-quiz-filter-checkboxes');
          if (filterCbE.hasClass('rcpar_cp_hide')) {
            filterCbE.removeClass('rcpar_cp_hide');

            // add a mousedown function for hiding checkboxes when clicking outside of the checkboxes div
            $(document).on('mouseup.filterOFF touchend.filterOFF', function (e) { // touchend is for mobile functionality
              var filterCbE = $('#cp-quiz-filter-checkboxes');

              if ($('#cp-quiz-filter-q-but').is(e.target)) { // button is handled by it's own handler, just remove doc mouseup
                $(document).off('mouseup.filterOFF touchend.filterOFF');
              }
              else if (!filterCbE.is(e.target) // not our div
                && filterCbE.has(e.target).length === 0) { // ... nor a descendant of our div
                filterCbE.addClass('rcpar_cp_hide');
                $(document).off('mouseup.filterOFF touchend.filterOFF');
              }
            });

          }
          else {
            filterCbE.addClass('rcpar_cp_hide');
          }


        }); // END show or hide filter checkboxes on clicking function


        //  add a change event for filter checkboxes
        $('input[type=checkbox]', '#cp-quiz-filter-checkboxes').on('change', function (e) {
          e.stopImmediatePropagation(); // this keeps it from firing twice
          filterCbChangeFunc();
        }); // end change event for filter checkboxes

        // add closebox functionality
        $('a.cp-psudo-menu-close').click(function (e) {
          e.preventDefault();
          $(document).off('mouseup.filterOFF touchend.filterOFF');
          $(this).parent().addClass('rcpar_cp_hide');
        });


      });

      /**
       * Change function for filter checkboxes
       * also called when available question views are changed
       */
      function filterCbChangeFunc() {
        // if no boxes are checked then we want to show all so check all boxes first
        if ($('input[type=checkbox]:checked', '#cp-quiz-filter-checkboxes').length == 0) {
          $('.rcpar-cp-qx.rcpar_cp_hide', '#questions-fieldset-wrapper').removeClass('rcpar_cp_hide');
          // mark filter button NOT in use
          $('#cp-quiz-filter-q-but').removeClass('cp-qx-filter-active');
        }
        else {
          // show checked question types
          $('input[type=checkbox]:checked', '#cp-quiz-filter-checkboxes').each(function () {
            var myClassSelector = '.cp-qx-type-' + $(this).val();
            $('div.rcpar_cp_hide' + myClassSelector, '#questions-fieldset-wrapper').removeClass('rcpar_cp_hide');
          });
          // hide unchecked question types
          $('input[type=checkbox]', '#cp-quiz-filter-checkboxes').not(':checked').each(function () {
            var myClassSelector = '.cp-qx-type-' + $(this).val();
            $('div' + myClassSelector, '#questions-fieldset-wrapper').addClass('rcpar_cp_hide');
          });
          // mark filter button in use
          $('#cp-quiz-filter-q-but').addClass('cp-qx-filter-active');
        }
        if ($('div.rcpar-cp-qx', '#questions-fieldset-wrapper').not('.rcpar_cp_hide').length == 0) {
          $('#avail-batch-container').addClass('rcpar_cp_hide');
        }
        else {
          $('#avail-batch-container').removeClass('rcpar_cp_hide');
        }
        // set the count of currently visible questions in the Question Bank
        $('.rcparCpAvailQcount').html($("#questions-fieldset-wrapper .rcpar-cp-qx:visible").length);
      }

      // end Functionality for filtering questions by type

      /**
       * Functionality for sorting questions
       */
      $('#cp-quiz-sort-q-but', context).once('cbQuizSortBut', function () {

        // show or hide radios on clicking function
        $('#cp-quiz-sort-q-but').on('click', function () {
          var sortRadiosE = $('#cp-quiz-sort-radios');
          if (sortRadiosE.hasClass('rcpar_cp_hide')) {
            sortRadiosE.removeClass('rcpar_cp_hide');

            // add a mousedown function for hiding checkboxes when clicking outside of the checkboxes div
            $(document).on('mouseup.sortOFF touchend.sortOFF', function (e) { // touchend is for mobile functionality
              var sortRadiosE = $('#cp-quiz-sort-radios');

              if ($('#cp-quiz-sort-q-but').is(e.target)) { // button is handled by it's own handler, just remove doc mouseup
                $(document).off('mouseup.sortOFF touchend.sortOFF');
              }
              else if (!sortRadiosE.is(e.target) // not our div
                && sortRadiosE.has(e.target).length === 0) { // ... nor a descendant of our div
                sortRadiosE.addClass('rcpar_cp_hide');
                $(document).off('mouseup.sortOFF touchend.sortOFF');
              }
            });

          }
          else {
            sortRadiosE.addClass('rcpar_cp_hide');
          }


        }); // END show or hide radios on clicking function

      });

      /**
       * functionality for search questions
       */

      $('.cp_quiz_qx_search_tb', context).once('cbQuizSearch', function () {
        // sets a function that is called by ajax search return to set the focus to the end of the search textbox
        $(document).delegate('.cp_quiz_qx_search_tb', 'focusAtEnd', function (ev, data) {
          $(this).focus();
          var thisVal = $(this).val();
          $(this).val('').val(thisVal);
        });

        var typingTimer; // an timer for determining when to trigger the search ajax
        var doneTypingInterval = 1000; // number of microseconds after keystrokes before ajax is triggered

        // this replaces the default textfield blur event, set in rcpar_cp_form_rcpar_cp_quiz_form_alter
        // in the search textfield #ajax definition, this causes the ajax to be fired if there is a value in the search
        // field and the user pauses for doneTypingInterval/1000 seconds
        // adding a delay sending an ajax command for the search callback
        $('.cp_quiz_qx_search_tb').on('keyup', function (e) {
          clearTimeout(typingTimer);
          if ($(this).val) {
            var trigid = $(this);
            typingTimer = setTimeout(function () {
              trigid.trigger('search_delay');
            }, doneTypingInterval);
          }
        });
      });

      /**
       * The form #states wasn't working to enable/disable the Quiz timer
       * fields when the page timer_active field was inactive when the page is
       * accessed. This appears to be because bootstrap-select needs
       * .selectpicker('refresh') after the disabled prop is changed. This set
       * of functions fixes that.
       */
      $('input[id^="edit-timer-active"]', context).once('quizTimerStatesHack', function () {
        // NOTE: all selectors use the [id^= ...] type selector because on validation errors, ids are augmented with '-<some number>'

        // add a change func for the timer_active field & init it
        $('input[id^="edit-timer-active"]', '#form-content-container').on("change", function () {
          var isDisabled = true;
          if (this.checked) {
            isDisabled = false;
          }
          $('select[id^=edit-timer-length-]', '#form-content-container').prop('disabled', isDisabled).selectpicker('refresh');
        }).change();
      });

      /**
       * This is a similar hack to the one just above to enable/disable the
       * show_results_date field depending on whether the show_results field is
       * set to quiz_specific_date. This seems to be necessary because #states
       * doesn't recognize 'input[name=show_results]:checked'
       */
      $('#cp-show-results-container', context).once('showResultsStatesHack', function () {
        // NOTE: some selectors use the [id^= ...] type selector because on validation errors, ids are augmented with '-<some number>'

        // add a change func for the edit-show-results field & init it
        $('input:radio[name=show_results]', '#cp-show-results-container').on("change", function (e) {
          e.stopImmediatePropagation(); // this keeps it from firing multiple times
          var isDisabled = true;
          $("input[name='show_results']:checked", '#cp-show-results-container').val();
          if ($('input[name=show_results]:checked', '#cp-show-results-container').val() == 'quiz_specific_date') {
            isDisabled = false;
          }

          // Disable all inputs and selects within. Also trigger a change event so that bootstrap select can respond
          $('input, select', '#cp-show-results-container .form-item-show-results-date')
            .prop('disabled', isDisabled)
            .trigger('change');
          try {
            $('#cp-show-results-container .selectpicker').selectpicker('refresh');
          }
          catch(e) {}

          // add/remove required label depending on if it's enabled
          var reqLabel = ' <span class="form-required" title="This field is required.">*</span>';
          $('label', 'div.form-item-show-results-date').each(function () {
            var myHtml = $(this).html();
            if (isDisabled) {
              $(this).html(myHtml.replace(reqLabel, ''));
              $('.form-item-show-results-date').addClass('disabled-wrapper');
            }
            else {
              $(this).html(myHtml + reqLabel);
              $('.form-item-show-results-date').removeClass('disabled-wrapper');
            }
          });
        });
        // call the change only on the checked input so that it's only called once when the page is reached via the continue button
        $('input:radio[name=show_results]:checked', '#cp-show-results-container').change();
      });

      /**
       * called by submit of question being added or removed so as to be
       * included in ajax
       * @param q_no integer nid of the question being selected
       */
      function setQuestionNo(q_no) {
        $('input[name="question_selected"]', '#question-available-wrapper').val(q_no);
      }

      /**
       * Custom event to switch to the questions page & show an error message
       * when the quiz is submitted with no questions Called by
       * ajax_command_invoke('#rcpar-cp-quiz-form', 'trigger',
       * array('noQuestionsError')) in rcpar_cp_save_quiz_submit() when the
       * form is attempted to be scheduled with no questions from step 3.
       */
      $('#rcpar-cp-quiz-form', context).once('noQuestionsErrorInit', function () { // load the function only once
        // the name of the backend ajax_command_invoke being triggered is noQuestionsError
        $('#rcpar-cp-quiz-form').on('noQuestionsError', function () {
          // Trigger the back button.
          $('#edit-back').trigger('mousedown');

          // Holds the setInterval so that we can clear it when it's finished.
          var showDrupalMsgInterval;
          // Holds the number of times the interval is called. We'll check it to avoid an infinite loop.
          var showDrupalMsgIntervalCount = 0;

          // Set a repeating function that checks if step 2 has been drawn.
          showDrupalMsgInterval = setInterval(function () {
            // Execute as soon as we're on step 2, don't wait more than 2 seconds.
            if ($('.cp-form-submit.step-2').length > 0 || showDrupalMsgIntervalCount >= 200) {
              clearInterval(showDrupalMsgInterval); // Stops this interval from repeating
              // Insert our pseudo drupal message (rcparCpFormSubmitErrHtml was set in the backend) at the top of the screen.
              $('#rcpar-cp-quiz-form').before(Drupal.settings.rcpar_cp.rcparCpFormSubmitErrHtml);
              // Clear rcparCpFormSubmitErrHtml from our settings (not strictly needed.)
              delete Drupal.settings.rcpar_cp.rcparCpFormSubmitErrHtml;
            }
            // Keep track of how many times the interval function has been called.
            showDrupalMsgIntervalCount++;
          }, 10); // 10 indicates that the function will be called 100 times (10/1000) a second.
        });
      });

      /**
       * Add functionality for disabling filters & hiding selects when searching for questions by id.
       */
      $('#cp-quiz-filter-q-but', context).once('isSearchQid', function () { // load the function only once
        // This works because the filter buttons only show if available questions are present.
        if ($('.is_search_qid', context).length) {
          $('#edit-question-selects-container').hide();
          $('#cp-quiz-filter-q-but, #cp-quiz-sort-q-but').addClass('disabled');
        }
        else {
          $('#edit-question-selects-container').show();
          $('#cp-quiz-filter-q-but, #cp-quiz-sort-q-but').removeClass('disabled');
        }
      });

      /**
       * Add functionality for Quiz preview modal info
       */
      if ($('#step_2_container', context).length) {
        // This is set by the continue/back buttons on the top of the page
        // and tells us if the professor is on the "add questions" page of quiz setup.
        if (Drupal.settings.rcpar_cp.is_navigation) {

          // Determine if the user has asked to never see this popup again
          var showPreviewPopup = true;
          var modals_to_check = ['act_professor_quiz_preview'];
          if (RCPAR.modals.userHasSeenModals(modals_to_check)) {
            showPreviewPopup = false;
          }

          if (showPreviewPopup) {
            $('#cp-quiz-preview-info-modal').modal('toggle');
          }
          $('#cp-quiz-preview-info-modal').on('hidden.bs.modal', function () {
            // Log the modal in the database when the don't show checkbox is checked.
            if ($('div.dont_show_checkbox input')[0].checked) {
              // Get the user's token
              var jwt = RCPAR.jwt.getJwt();
              RCPAR.modals.setModalData('act_professor_quiz_preview', jwt);
            }
          });
          Drupal.settings.rcpar_cp.is_navigation = 0;
        }
      }
    }
  };
  Drupal.ajax.prototype.commands.redirectAfterCpSubmit = function (ajax, response) {
    // this is added to redirect the form after a successful ajax submit
    $(location).attr('href', response.path);
  };
}(jQuery));
