(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.rcparCpPreviewBind = {
    attach: function (context, settings) {
      /**
       * Question preview links - show in an iframe
       */
      $('.rcpar-cp-qx-view', context).click(function (e) {
        e.preventDefault();

        // There can be a delay when previewing a new question as the browser is loading the new source.
        // To prevent this, we wipe out the old iframe and insert a new one.
        $('#cp-preview-question iframe').remove();
        $('#cp-preview-question .modal-body').html('<iframe id="cp-preview-question-iframe">Loading...</iframe>');

        // Iframe src will be the question preview link's href
        $('#cp-preview-question iframe').attr('src', $(this).attr('href'));
        
        // After the iframe loads, add a class to its body tag to format the preview correctly.
        $('#cp-preview-question iframe').load( function() {
          var body = document.getElementById('cp-preview-question-iframe').contentDocument.getElementsByTagName('body')[0];
          $(body).addClass('within-iframe');
        });
      });
    }
  };
})(jQuery, Drupal, this, this.document); //END - Closure