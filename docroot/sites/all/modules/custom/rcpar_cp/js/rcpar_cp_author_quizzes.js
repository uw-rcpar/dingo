/**
 * Created by Reed on 2/8/17.
 */
( function ($) {
  // this just adds a command to redirect from an ajax response
  // TODO: consider putting this in a generic location for inclusion in many places
  Drupal.ajax.prototype.commands.redirectFromBackEnd = function (ajax, response) {
    $(location).attr('href', response.path);
  };
}(jQuery));
