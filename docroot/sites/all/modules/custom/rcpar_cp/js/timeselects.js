(function ($, Drupal, window, document, undefined) {
  $(document).ready(function () {
    Drupal.behaviors.timeselects = {
      attach: function (context, settings) {
        // When changed, programmatically inject the value from our time selects into the hidden Drupal timepicker field
        $('.timeselects select', context).change(function(e){
          // Find the parent wrapper for our time selects
          var $ts = $(this).closest('.timeselects');

          // Build a full value like "02:30PM" from the current values in the time selects
          var timeValFromSelects = $ts.find('select.hour').val() + ':' + $ts.find('select.minute').val() + $ts.find('select.ampm').val();

          // Set Drupal's time textfield to the value we built
          var form = $(this).closest('.form-type-date-popup').find("input[name*='[time]']").val(timeValFromSelects);
        });

        // On initial load, trigger the change event on our time select in case the time field is empty.
        // This will prevent the scenario where the select lists appear to show a time since they have
        // options that are selected by default, but nothing has been put into the hidden textfield
        $('.timeselects select', context).trigger('change');
      }
    };
  }); //END - document.ready
})(jQuery, Drupal, this, this.document); //END - Closure
