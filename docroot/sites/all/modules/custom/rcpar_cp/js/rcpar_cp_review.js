/**
 * Created by Reed on 1/20/17.
 */
( function ($) {
  Drupal.behaviors.rcpar_cp_review = {
    attach: function (context, settings) {
      // check if results button should be shown
      // !! is used to see if the object exists @see http://stackoverflow.com/a/2127324/2788785
      // the logic here is unless Drupal.settings.rcparCp.showResults exists & is set to true, remove the remove session button
      if (!(!!Drupal.settings.rcparCp && !!Drupal.settings.rcparCp.showResults && Drupal.settings.rcparCp.showResults)) {
        $('a.review-sess-btn', 'div.pull-right').remove();
      }
      var rcparCpUrl = '/act/student/quizzes'; // the current default
      // once again, !! is used to see if the object exists @see http://stackoverflow.com/a/2127324/2788785
      // the logic here is if Drupal.settings.rcparCp.rcparCpUrl exists set local var rcparCpUrl to Drupal.settings.rcparCp.rcparCpUrl, a little overkill..
      if (!!Drupal.settings.rcparCp && !!Drupal.settings.rcparCp.rcparCpUrl) {
        rcparCpUrl = Drupal.settings.rcparCp.rcparCpUrl;
      }
      // add a return to student quizzes button
      $('div.pull-right').append('<a id="student-quizzes-return" href="' + rcparCpUrl + '" class="btn btn-primary btn-sm review-sess-btn student-quizzes-return" role="button"> Done<span class="reader-instructions">, return to Quizzes Overview</span></a>');
    }

  }
}(jQuery));