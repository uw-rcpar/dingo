<?php

/**
 * This function will return TRUE if the current user has ANY curation platform access. This includes:
 * 1. ACT Assignable Materials entitlements (ASL student)
 * 2. ACT Creator access (ASL Professor)
 * 3. PAL Creator access (RCPAR admin)
 * 4. ACT Sample Materials entitlements (PAL user)
 *
 * This function exists because certain components of the ACT system are common to all user expereiences.
 * For example, taking a quiz - all consumers take quizzes, and creators have the ability to preview quizzes,
 * so this can be an access callback that is shared for all quiz taking callbacks.
 *
 * @return bool
 */
function rcpar_cp_access_any() {
  // Cache once per page load - return the cached value
  $hasAccess = &drupal_static(__FUNCTION__);
  if (isset($hasAccess)) {
    return $hasAccess;
  }
  // Default to no access
  $hasAccess = FALSE;

  // ASL student
  if (rcpar_cp_access_consumer()) {
    $hasAccess = TRUE;
  }

  // ASL professor or PAL administrator
  if(rcpar_cp_access_creator_any()) {
    $hasAccess = TRUE;
  }

  // PAL student
  if (rcpar_cp_access_sample_consumer()) {
    $hasAccess = TRUE;
  }

  return $hasAccess;
}

/**
 * Determines if the current user should have access to the consumer/student portions of CP.
 * We check this two ways:
 * 1. A user is part of an partner's email group and that partner has CP turned on
 * 2. A user should have access if any of their active entitlements come from a partner that has ACT turned on.
 * - @todo We don't have a specific entitlement given to users that JUST have CP access and no other course entitlements
 * (this is basically the publisher access scenario). Entitlements for ACT may be coming.
 *
 * @return bool
 */
function rcpar_cp_access_consumer() {
  // Cache once per page load - return the cached value
  $hasAccess = &drupal_static(__FUNCTION__);
  if (isset($hasAccess)) {
    return $hasAccess;
  }
  // Default to no access
  $hasAccess = FALSE;

  // Check the user's entitlements for 'ACT Assignable Materials' products
  $usersActAssignableProducts = rcpar_cp_user_get_assignable_entitlement_skus();
  if (sizeof($usersActAssignableProducts) > 0) {
    $hasAccess = TRUE;
  }

  return $hasAccess;
}

/**
 * Returns TRUE if the user has access to create EITHER ACT Sample Materials OR ACT Assignable Materials
 * @return bool
 */
function rcpar_cp_access_creator_any() {
  if(rcpar_cp_access_creator()) {
    return TRUE;
  }

  if(rcpar_cp_access_sample_creator()) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Determines whether the current user has access to the creator/instructor portion of curation platform.
 * This is TRUE if they are listed as the admin user of any partner that also has ACT access checked.
 * @return bool
 */
function rcpar_cp_access_creator() {
  // Cache once per page load - return the cached value
  $hasAccess = &drupal_static(__FUNCTION__);
  if (isset($hasAccess)) {
    return $hasAccess;
  }
  // Default to no access
  $hasAccess = FALSE;

  // Give access to administrators
  if (user_access('administer site configuration')) {
    $hasAccess = TRUE;
    return $hasAccess;
  }

  // Get partners that this user is an admin of with ACT turned on
  $cpUser = new CPUser();
  $partners = $cpUser->getCreatorsACTEnabledPartners();
  if (!empty($partners)) {
    $hasAccess = TRUE;
    return $hasAccess;
  }

  return $hasAccess;
}

/**
 * Returns TRUE if the user has any entitlements for Act Sample Materials
 * @param ActSampleProduct $asp
 * - When provided, checks access for a specific product. When NULL, checks if the user has
 * access to ANY ACT sample product.
 * @return bool
 */
function rcpar_cp_access_sample_consumer(ActSampleProduct $asp = NULL) {
  $act_skus = rcpar_cp_user_get_sample_entitlement_skus();

  // When a product SKU is passed in, check that the user has an entitlement
  // for that product specifically.
  if (!is_null($asp)) {
    // Make sure the product can be loaded.
    if(!$asp->isLoaded()) {
      return FALSE;
    }

    // Check that the user has an entitlement for this product
    return array_search($asp->getSku(), $act_skus) !== FALSE;
  }

  // Otherwise, we just check whether the user has access to any products at all
  return !empty($act_skus);
}
