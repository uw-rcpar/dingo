<?php

/**
 * Utility function to dynamically get the node form ids of all active IPQ
 * question types. (Called by rcpar_cp_form_alter())
 *
 * @return array
 *  - of node form ids of all active IPQ question types
 */
function rcpar_cp_ipq_usage_data_get_ipq_q_type_form_ids() {
  $ipq_q_type_form_ids = &drupal_static(__FUNCTION__);
  if (!isset($ipq_q_type_form_ids)) {
    $ipq_q_type_form_ids = db_query("SELECT type FROM {ipq_question_types} WHERE is_active = 1")->fetchCol();
    foreach ($ipq_q_type_form_ids AS $key => &$val) {
      $val .= '_node_form';
    }
  }
  return $ipq_q_type_form_ids;
}

/**
 * Creates HTML markup to show where and how often a question is used at the top
 * of it's node edit form.
 *
 * @param object $node
 * - A question node
 * @return array
 * - Drupal renderable array
 */
function rcpar_cp_ipq_usage_data_get_usage($node) {
  $qid = $node->nid;

  // In this function mysql will do the heavy work because it's faster than entity queries.


  //Check the IPQ Question Pool
  $result = db_query("SELECT ipq_questions_id FROM ipq_question_pool WHERE ipq_questions_id = :ipq_questions_id",
    array( ':ipq_questions_id' => $qid))->fetchAll();
  $is_in_ipq_question_pool = ($result) ? 'Yes' : 'No';

  // Get count of ACT sample quizzes this question is in questions in.
  $act_pub_quiz_count = db_query("
                SELECT count(g.id) AS act_pub_quiz_count
                FROM {field_data_field_cp_question} q
                INNER JOIN {eck_cp_question_group} g ON g.id = q.entity_id
                INNER JOIN {field_data_field_quizzes_ref} qr ON qr.field_quizzes_ref_target_id = g.id
                WHERE q.field_cp_question_target_id = :qid
                AND g.type = 'cp_quiz'
                AND qr.bundle = 'act_sample_materials'",
    array(':qid' => $qid))->fetchField();

  // Get the ACT_ASL question groups the question belongs to.
  $act_asl_q_pools = db_query("SELECT GROUP_CONCAT(DISTINCT cp.title ORDER BY cp.title SEPARATOR '\", \"')
                    FROM field_data_field_cp_question q
                    INNER JOIN eck_cp_question_group g ON g.id = q.entity_id
                    INNER JOIN field_data_field_question_pool_ref qp ON qp.field_question_pool_ref_target_id = g.id
                    INNER JOIN commerce_product cp ON cp.product_id = qp.entity_id
                    WHERE q.field_cp_question_target_id = :qid
                    AND qp.bundle = 'act_assignable_materials'
                    GROUP BY g.id",
    array(':qid' => $qid))->fetchField();
  $act_asl_q_pools = empty($act_asl_q_pools) ? '(none)' : '"' . $act_asl_q_pools . '"';

  // This query is very similar to first one above.
  // Save it in case we have other types of quizzes in the future
  // $query = db_query("SELECT COUNT(g.id)
  //                  FROM {field_data_field_cp_question} q
  //                  INNER JOIN {eck_cp_question_group} g ON g.id = q.entity_id
  //                  LEFT JOIN {field_data_field_quizzes_ref} qr ON qr.field_quizzes_ref_target_id = g.id
  //                  WHERE q.field_cp_question_target_id = :qid
  //                  AND g.type = 'cp_quiz'
  //                  AND (qr.bundle IS NULL OR qr.bundle != 'act_sample_materials')",
  //  array(':qid' => $qid));

  // Use this instead it's a faster query because it has no LEFT JOINs

  // Get the total quizzes the question is used in, we'll subtract the count of
  // sample quizzes from it to get the ACT ASL quiz count.
  $act_total_quiz_count = db_query("SELECT COUNT(g.id)
                    FROM {field_data_field_cp_question} q
                    INNER JOIN {eck_cp_question_group} g ON g.id = q.entity_id
                    WHERE q.field_cp_question_target_id = :qid
                    AND g.type = 'cp_quiz'",
    array(':qid' => $qid))->fetchField();

  $act_assignable_quiz_count = $act_total_quiz_count - $act_pub_quiz_count;
  $act_assignable_quizzes = t('@act_als_cnt usages.', array('@act_als_cnt' => $act_assignable_quiz_count));

  // List question relationships. Note that "type" will be a past-tense verb. Currently the only type is "replicated".
  $related = array();
  $result = db_query("SELECT r.ipq_question_id, r.type FROM ipq_question_relationships r WHERE ipq_related_question_id = :nid", [':nid' => $qid]);
  foreach ($result as $item) {
    // E.g. "Replicated from xxxxx"
    $related[] = ucfirst($item->type) . ' from ' . l($item->ipq_question_id, 'node/' . $item->ipq_question_id . '/edit');
  }
  $result = db_query("SELECT r.ipq_related_question_id, r.type FROM ipq_question_relationships r WHERE ipq_question_id = :nid", [':nid' => $qid]);
  foreach ($result as $item) {
    // E.g. "Replicated to xxxxx"
    $related[] = ucfirst($item->type) . ' to ' . l($item->ipq_related_question_id, 'node/' . $item->ipq_related_question_id . '/edit');
  }
  if(empty($related)) {
    $related[] = '<em>No relationships found.</em>';
  }

  // Make a preview link for each version of the exam that this question is tagged with. It would be nice if we could
  // have just simply queried this info from the question pool, but admins may not have published this question yet,
  // which means there would be no data there.
  $preview_links = array();
  try {
    $question_wrapper = entity_metadata_wrapper('node', $node);
    $question_versions = $question_wrapper->field_section_per_exam_version->value();
    foreach ($question_versions as $question_version) {
      $question_version_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $question_version);
      $term_version = $question_version_wrapper->field_exam_version_single_val->value();

      // Add a preview link to our array
      $preview_url = url("ipq/quiz/setup/preview/$qid", ['query' => ['exam_version' => $term_version->name]]);
      $preview_links[] = strtr('<a href=":url" target="_blank" class="button">:version</a>',
        [':url' => $preview_url, ':version' => $term_version->name]
      );
    }
  }
  catch (EntityMetadataWrapperException $e) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . ' ERROR: ' . $e->getMessage());
  }
  if(empty($preview_links)) {
    $preview_links[] = '<em>Cannot preview until exam version information has been added to this question</em>';
  }


  // @see rcpar_cp/css/cp,css for styling.
  $render = array(
    'question_bank_info' => array(
      '#title' => 'Question Bank Information',
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      'content' => array(
        '#markup' => '
          <div class="ipq-form-locations" xmlns="http://www.w3.org/1999/html">
            <p><strong>IPQ Question Pool:</strong> ' . $is_in_ipq_question_pool . '</p>
            <p><strong>ACT Sample Product Quizzes:</strong> Used in ' . $act_pub_quiz_count . ' quiz(zes)</p>
            <p><strong>ACT Assignable Question Pool:</strong> Used in: ' . $act_asl_q_pools . '</p>
            <p><strong>ACT Assignable Quizzes:</strong> ' . $act_assignable_quizzes . '</p>
            <p><strong>Question Relationships:</strong><br>' . implode('<br>', $related) . '</p>
          </div>'
      ),
    ),
    // Preview in quiz links per version
    'preview_tools' => array(
      '#title' => 'Preview in Quiz',
      '#description' => 'Creates a one-question quiz that simulates the user\'s experience of answering and reviewing this question.',
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      'content' => array('#markup' => implode('&nbsp;&nbsp;', $preview_links)),
    ),

    // This file holds styling for the markup.
    '#attached' => array(
      'css' => array(
        drupal_get_path('module', 'rcpar_cp') . '/css/cp-ipq-usage.css',
      ),
    ),
  );

  return $render;
}