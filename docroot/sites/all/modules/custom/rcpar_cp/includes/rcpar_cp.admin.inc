<?php

/**
 * Settings form for ACT professors.
 */
function rcpar_cp_profdemo_settings() {
  $form = array();

  // Get all assignable products
  $assignable_products = rcpar_cp_get_assignable_products();

  // Checkboxes to select products
  $form['act_prof_sample_skus'] = array(
    '#title'         => 'Professor Sample SKUs',
    '#description'   => 'The following question banks will be available to professors to use ' .
      'when creating unassigned sample quizzes. The ability to create sample quizzes is offered ' .
      'before the professor has any student rosters available, which would normally determine the' .
      ' questions they have access to.',
    '#type'          => 'checkboxes',
    '#required'      => TRUE,
    '#options'       => array_combine(array_keys($assignable_products), array_keys($assignable_products)),
    '#default_value' => variable_get('act_prof_sample_skus', RCPAR_CP_PROF_SAMPLE_SKUS),
  );

  return system_settings_form($form);
}

/**
 * Temporary no-frills form for saving generated question pools
 * @param $form
 * @param $form_state
 * @return renderable array
 */
function rcpar_cp_q_pool_entity_form($form, &$form_state) {
  $all_pools = rcpar_cp_get_question_pools();

  // will hold the titles of saved pools to compare to generated
  $saved_titles = array();
  $saved_exists = FALSE;
  foreach (array('saved', 'generated') AS $my_type) {
    $form[$my_type . '-pools-container'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => $my_type . '-pools-container',
      ),
      '#prefix' => '<p><strong>************* ' . strtoupper($my_type) . ' POOLS *************</strong></p>',
    );
  }
  foreach ($all_pools AS $pool_key => $pool) {
    $my_markup = '';
    if ($pool_key !== $pool->title) {
      $my_type = 'saved';
      $my_label = 'Delete ' . $pool->id . '-' . $pool->title;
      $my_action = 'Saved';
      $saved_exists = TRUE;
      $saved_titles[] = $pool->title;
    }
    else {
      $my_type = 'generated';
      $my_label = 'Save ' . $pool_key;
      $my_action = 'Save';
      if (in_array($pool_key, $saved_titles)) {
        $my_markup = t('<span><strong>A saved version of the generated pool "' . $pool->title . '" already exists.</strong></span>');
      }
    }
    $form[$my_type . '-pools-container'][$pool_key] = array(
      '#type' => 'submit',
      '#value' => $my_label,
      '#submit' => array('rcpar_cp_q_pool_entity_form_submit'),
      '#prefix' => $my_markup,
      '#suffix' => '<br>'
    );

    if($my_type == 'saved') {
      $form[$my_type . '-pools-container'][$pool_key]['#suffix'] = l('Edit', "admin/structure/entity-type/cp_question_group/cp_question_pool/{$pool->id}/edit") . '<br>';
    }


    if (!empty($my_markup)) {
      $form[$my_type . '-pools-container'][$pool_key]['#disabled'] = TRUE;
    }
  }
  if (!$saved_exists) {
    $form['saved-pools-container']['#suffix'] = '<p>(There are no saved question pools yet.)</p>';
  }
  return $form;
}

/**
 * Submit callback for temporary no-frills form for saving generated question pools
 * @param $form
 * @param $form_state
 */
function rcpar_cp_q_pool_entity_form_submit($form, &$form_state) {
  $entity_type = 'cp_question_group';
  if (isset($_POST['op'])) {
    $my_arr = explode(' ', $_POST['op']);
    if ($my_arr[0] == 'Save') {
      $my_pools = rcpar_cp_get_question_pools(array($my_arr[1]));
      $my_pool = $my_pools[$my_arr[1]];

      // machine name has to be explicitly set, wrappers won't work
      $my_pool->field_cp_pool_machine_name = array(
        LANGUAGE_NONE => array(
          0 => array(
            'value' => $my_pool->title,
          ),
        ),
      );

      entity_save($entity_type, $my_pool);
    }
    else {
      $my_pool_id_arr = explode('-', $my_arr[1]);
      $my_pool_id = $my_pool_id_arr[0];
      entity_delete($entity_type, $my_pool_id);
    }
  }
}