<?php

/**
 * Class RCPARCPPalQuizzesForm
 *
 * Controls form functionality for rcpar_cp_author_pal_quizzes_form
 *
 * The following is an exact copy of Pedro's comment for Class RCPARCPQuizzesForm, it applies equally here.
 *
 * Note that we are extending RCPARCPResultsForm even as that class contains lots of functions and features
 * that we are not using
 * probably a more elegant way to resolve this would have been to move the functionality that
 * this class and RCPARCPResultsForm uses to a super class and extend both classes from it
 * but after a discussion we decided that this approach was quicker and as both forms works
 * we are just going to leave it in this way (see also RCPARCPGroupsForm class which is on the same situation)
 */
class RCPARCPPalQuizzesForm extends RCPARCPResultsForm {

  public $sampleProdOptions; // The options available in the sample products select list

  public function __construct($form, &$form_state) {
    $this->form = $form;
    $this->form_state =& $form_state;

    $this->setSampleProdOptions();
    $this->setSelectedGroup(); // Depends on getSampleProdOptions()
    $this->setselectedGroupPid(); //Depends on setSelectedGroup()
  }

  /**
   * Returns the constructed form array.
   * @return array
   */
  public function getForm() {
    $form = $this->form;
    $form_state = $this->form_state;
    // Select sample product
    $form['sample_prod_select'] = array(
      '#type' => 'select',
      '#options' => $this->getSampleProdOptions(),
      '#default_value' => $this->getSelectedGroup(),
      '#prefix' => '<div id="group-select-wrapper">',
      '#suffix' => '</div>',
      '#ajax' => array(
        'callback' => 'rcpar_cp_author_quiz_palquizzes_form_select_ajax',
      ),
    );


    // container for create a quiz button & sample product selector
    $form['top'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'create-pub-quiz-button-container',
        'class' => array('create-a-quiz')
      ),
      'content' => array(
        '#markup' => l('Create a quiz', RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/creator/' . $this->getSelectedGroup() .'/create/nojs', array('attributes' => array('class' => array('create-a-quiz-btn btn btn-success use-ajax'))))

      ),
    );

    $form['palquizzes_table']['#prefix'] = '<div id="results-table" class="quiz-results-table">';
    $form['palquizzes_table']['#suffix'] = '</div>';
    return $form;
  }


  public function setSelectedGroup() {
    $form_state =& $this->form_state;

    // Look for a selected group from the form post
    if (!empty($form_state['values']['sample_prod_select'])) {
      $selected_group = $form_state['values']['sample_prod_select'];
    }
    else {
      // No post, default to the first available value
      $sample_prod_select_options = $this->getSampleProdOptions();
      reset($sample_prod_select_options);
      $selected_group = key($sample_prod_select_options);
    }

    $this->selectedGroup = $selected_group;
  }

  /**
   * returns the options for ACT sample products select
   * @return mixed
   */
  public function getSampleProdOptions() {
    return $this->sampleProdOptions;
  }

  /**
   * sets the options for ACT sample products select
   */
  public function setSampleProdOptions() {
    $this->sampleProdOptions = rcpar_cp_get_sample_skus(TRUE);
  }

  /**
   * Returns a subsection of the form showing quizzes of the selected sample product
   * @return array
   */
  public function getPalQuizzesOnCurrentGroup() {
    $act_prod_class = new ActSampleProduct($this->getSelectedGroup());
    $quizzes = $act_prod_class->getQuizzes();
    if (!empty($quizzes)) {
      // Build a table of status icons and quiz edit links
      $vars = array('rows' => array());
      $my_icon = rcpar_cp_get_icon('status-launched');
      foreach ($quizzes as $delta => $quiz) {
        $changed = $quiz->changed;
        $changedDate = date_create_from_format('U', $changed);
        // for lack of any better timezone, we'll set them all to Eastern
        $dtz = new DateTimeZone('America/New_York');
        date_timezone_set($changedDate, $dtz);
        $date_str = $changedDate->format('n/j/Y g:i a T');

        $text = '<div class="quiz-title quizzes">' . $quiz->title . '</div>';
        // TODO: once publisher quiz editing is available, replace above line w/ the following:
        //$text = '<div class="quiz-title">' . l($quiz->title, RCPAR_CP_URL . '/quiz/' . $quiz-> . '/edit') . '</div>';
        $text .= ' <strong>Saved</strong> ';
        $text .= $date_str . '</div>';

        // build the rows array
        $vars['rows'][] = array($my_icon, $text);
      }

      // The results need to use pagination.
      $per_page = 10; // 10 rows per page
      // Initialize the pager.
      $current_page = pager_default_initialize(count($vars['rows']), $per_page);
      // Split rows into page sized chunks.
      $chunks = array_chunk($vars['rows'], $per_page, TRUE);

      // Render table and pager.
      $render['quizzes']['content']['#markup'] = theme('table', array('header' => '', 'rows' => $chunks[$current_page])) . theme('pager', array('quantity',count($vars['rows'])));

    }
    else {
      $result['quizzes']['content']['#markup'] = '<div class="no-results">No quizzes have been assigned to the selected ACT Sample Product.</div>';
    }
    return $result;
  }


}

/**
 * Class RCPARCPQuizzesForm
 *
 * Controls form functionality for rcpar_cp_author_quizes_form
 *
 * Note that we are extending RCPARCPResultsForm even as that class contains lots of functions and features
 * that we are not using
 * probably a more elegant way to resolve this would have been to move the functionality that
 * this class and RCPARCPResultsForm uses to a super class and extend both classes from it
 * but after a discussion we decided that this approach was quicker and as both forms works
 * we are just going to leave it in this way (see also RCPARCPGroupsForm class which is on the same situation)
 */

class RCPARCPQuizzesForm extends RCPARCPResultsForm {
  public function __construct($form, &$form_state) {
    $this->form = $form;
    $this->form_state =& $form_state;

    $this->setEmailGroupOptions();
    $this->setSelectedGroup(); // Depends on getEmailGroupOptions()
    $this->setselectedGroupPid(); //Depends on setSelectedGroup()
  }

  /**
   * Returns the constructed form array.
   * @return array
   */
  public function getForm() {
    $form = $this->form;

    // Select list form email group
    $form['email_group_select'] = array(
      '#type'          => 'select',
      '#options'       => $this->getEmailGroupOptions(),
      '#default_value' => $this->getSelectedGroup(),
      '#prefix'        => '<div id="group-select-wrapper">',
      '#suffix'        => '</div>',
      '#ajax'          => $this->getResultsAjaxOptions()
    );

    // Add in a faux group for unassigned quizzes
    $form['email_group_select']['#options']['Sample Quizzes'][0] = 'Unassigned';

    // 'Create a Quiz' div and button
    $form['top'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('create-a-quiz')),
      'content' => array(
        '#markup' => l('Create a quiz', RCPAR_CP_URL . '/quiz/create', array('attributes' => array('class' => array('create-a-quiz-btn btn btn-success'))))
      ),
    );

    // Here we get the quizzes list
    $table = $this->getQuizzesOnCurrentGroup();
    if (!empty($table)) {
      $form['result_table'] = $table;
    }

    $form['result_table']['#prefix'] = '<div id="results-table" class="quiz-results-table">';
    $form['result_table']['#suffix'] = '</div>';
    return $form;
  }

  /**
   * Returns an array of options for FAPI's #ajax property.
   * @return array
   */
  public function getResultsAjaxOptions() {
    return array(
      'callback' => 'rcpar_cp_author_quizzes_form_ajax',
      'wrapper'  => 'results-table',
      'method'   => 'replace',
      'effect'   => 'fade',
    );
  }

  /**
   * Returns a subsection of the form when results are shown by roster.
   * @return array
   */
  public function getQuizzesOnCurrentGroup() {
    $selected_group = $this->getSelectedGroup();

    // Initialize render structure for quiz area
    $result['quizzes'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('quizzes')),
      'content' => array(),
    );

    // Query for all quizzes by owner and email group
    $quizzes = rcpar_cp_get_quizzes_authored_by_uid(null, $selected_group);
    if(!empty($quizzes)) {
      // Build a table of status icons and quiz edit links
      $vars = array();
      foreach ($quizzes as $id => $quiz) {
        $cpQuizObj = new CPQuiz($quiz);

        // In certain unusual circumstances, $gid may be NULL before any email group has been set. This may cause
        // PAL quizzes authored by this user to be present in the result set, which throws errors when trying to
        // work with the date object. This is not a concern for normal end users, but is annoying for an admin.
        if($cpQuizObj->getQuizType() == 'act_pal') {
          continue;
        }

        $gid = $cpQuizObj->getStudentGroup();
        $qState = $cpQuizObj->getQuizAdminState();

        // TEMPORARILY put unassigned quizzes in the 'unassigned' qstate. We will restore this
        if($gid == "0") {
          $qState = 'unassigned';
        }

        /** Determine title and button links based on quiz state **/
        switch ($qState) {
          // Launched and closed states use "reports" for title and button links
          case 'launched':
          case 'closed':
          default:
            $title_link = l($quiz->title, RCPAR_CP_URL . '/reports', array('query' => array('gid' => $gid)));
            $button_link = l(t('View results'), RCPAR_CP_URL . '/reports',
              array(
                'query'      => array('gid' => $gid),
                'attributes' => array('class' => 'student-quiz-link'),
              )
            );
            break;

          // Draft & future scheduled quizzes use "edit" links in their title & button links
          // We also make a fake status called 'unassigned' to put unassigned quizzes in this category
          case 'scheduled':
          case 'draft':
          case 'unassigned':
            $title_link = l($quiz->title, RCPAR_CP_URL . '/quizzes/edit/ajax/' . $quiz->id, array('attributes' => array('class' => 'use-ajax')));
            $button_link = l(t('Edit'), RCPAR_CP_URL . '/quizzes/edit/ajax/' . $quiz->id, array(
              'attributes' => array(
                'class' => array(
                  'use-ajax',
                  'student-quiz-link'
                )
              )
            ));
            break;
        }
        // Restore quiz state for unassigned quizzes
        if($gid == "0") {
          $qState = $cpQuizObj->getQuizAdminState();
        }


        // All quizzes have a copy link available in the dropdown menu of the actions button

        // $copy_link is used by quizzes in 'launched' state
        $copy_link = l(t('Copy'), RCPAR_CP_URL . '/quizzes/copy/ajax/' . $quiz->id, array(
          'attributes' => array('class' => array('use-ajax','is-middle')))
        );

        // $copy_one_dropdown_link is used by all other quiz states. It is identical except classes: 'is-middle' removed
        $copy_one_dropdown_link = l(t('Copy'), RCPAR_CP_URL . '/quizzes/copy/ajax/' . $quiz->id, array(
          'attributes' => array('class' => array('use-ajax')))
        );

        // $preview_link is used by quizzes in all states.
        $preview_link = l(t('Preview'), RCPAR_CP_URL . '/quiz/' . $quiz->id . '/setup/preview', array(
          'attributes' => array('class' => array('is-middle')),
        ));
        // Special preview mode for administrators - supports testing and QA for IPQ by allowing us to build a quiz with
        // specific questions
        if (user_access('administer site configuration')) {
          $preview_in_ipq_link = l(t('Preview in IPQ'), RCPAR_CP_URL . '/quiz/' . $quiz->id . '/setup/preview', array(
            'attributes' => array('class' => array('is-middle')),
            'query' => array('ipq' => 1),
          ));
        }


        // NOTE: $delete_link is used by all quizzes, and is added to the utility function getQuizActionButton()

        if ($qState == 'closed') {
          // closed state quizzes have only title & button links to view their results
          $date = $cpQuizObj->getCloseDate();
          $date_str = $date->format('n/j/Y g:i a T');
          $text = '<div class="quiz-title">' . $title_link . '</div>';
          $text .= ' <strong>Closed</strong> ';
          $text .= $date_str . '</div>';
          $button = $this->getQuizActionButton($quiz->id, $button_link, array($copy_one_dropdown_link, $preview_link, $preview_in_ipq_link));
        }
        else {
          // NOTE: all states except closed quizzes have links to edit them

          if ($qState == 'launched') {
            // launched state title & button links default to viewing results
            // but the button will have a edit link in a drop-down
            $edit_link = l('Edit', RCPAR_CP_URL . '/quizzes/edit/ajax/' . $quiz->id, array('attributes' => array('class' => 'use-ajax')));

            $text = '<div class="quiz-title">' . $title_link . '</div>';

            $date = $cpQuizObj->getStartDate();
            $date_str = $date->format('n/j/Y g:i a T');
            $text .= ' <strong>Launched</strong> ';
            $text .= $date_str . '</div>';

            // The launched state button has a edit link in a drop-down.
            $button = $this->getQuizActionButton($quiz->id, $button_link, array($edit_link, $copy_link, $preview_link, $preview_in_ipq_link));
          }
          else {
            $text = '<div class="quiz-title">' . $title_link . '</div>';
            $button = $this->getQuizActionButton($quiz->id, $button_link, array($copy_one_dropdown_link, $preview_link, $preview_in_ipq_link));

            if ($qState == 'draft') { // we change the text following the title link
              $date = $cpQuizObj->getChangedDate();
              $date_str = $date->format('n/j/Y g:i a T');
              $text .= ' <strong>Draft</strong> saved ';
              $text .= $date_str . '</div>';
            }
            else { // only scheduled status left
              if ($qState == 'scheduled') { // we change the text following the title link
                $date = $cpQuizObj->getStartDate();
                $date_str = $date->format('n/j/Y g:i a T');
                $text .= ' <strong>Scheduled</strong> to launch ';
                $text .= $date_str . '</div>';
              }
            }
          }
        }
        // build the rows array
        $rows[] = array(
          'quiz_status' => $qState,
          'text' => $text,
          'date' => $date,
          'button' => $button,
        );
      }
      // quizzes need to display grouped by their status
      $scheduled = array(); // scheduled to launch at a future date
      $launched = array(); // currently active
      $draft = array(); // still a draft
      $closed = array(); // past their end date
      // loop through rows and build individual arrays based on their status so we can sort them
      foreach ($rows as $row) {
        if ($row['quiz_status'] == 'scheduled') {
          $scheduled[] = $row;
        }
        if ($row['quiz_status'] == 'launched') {
          $launched[] = $row;
        }
        if ($row['quiz_status'] == 'draft') {
          $draft[] = $row;
        }
        if ($row['quiz_status'] == 'closed') {
          $closed[] = $row;
        }
      }
      // sort each array - all should sort with oldest date first except for scheduled quizzes
      rcpar_cp_sort_quizzes($scheduled, 'asc');
      rcpar_cp_sort_quizzes($launched, 'desc');
      rcpar_cp_sort_quizzes($draft, 'desc');
      rcpar_cp_sort_quizzes($closed, 'desc');
      // merge all the arrays into a single array
      $grouped_rows = array_merge($scheduled, $launched, $draft, $closed);
      // loop through array and build a table of status icons and links
      foreach($grouped_rows as $row){
        $vars['rows'][] = array(
          rcpar_cp_get_icon('status-' . $row['quiz_status']),
          $row['text'],
          $row['button']
        );
      }
      // Render table
      $result['quizzes']['content']['#markup'] = theme('table', $vars) . theme('pager');
    }
    else {
      $result['quizzes']['content']['#markup'] = '<div class="no-results">No quizzes have been assigned to the selected group.</div>';
    }
    return $result;
  }

  /**
   * Utility for returning rendered html for the action button listed in creator quizzes list
   * NOTE: link params must have proper classes for html to be rendered properly
   * @see getQuizzesOnCurrentGroup for examples
   *
   * @param integer $quiz_is used to generate the delete link
   * @param string $button_link html rendered via drupal l() function
   * @param string $dropdown_link html rendered via drupal l() function
   * @param string $dropdown_2_link html rendered via drupal l() function
   * @return string html for the action button
   */
  protected function getQuizActionButton($quiz_id, $button_link, $dropdown_links = array()) {

    // This link is added as the last action dropdown link for all of the action buttonsl
    $delete_link = l(t('Delete'), RCPAR_CP_URL . '/quizzes/delete/ajax/' . $quiz_id, array(
      'attributes' => array(
        'class' => array(
          'use-ajax',
          'is-last'
        )
      )
    ));


    if (sizeof($dropdown_links) == 1) {
      $dropdown_link = $dropdown_links[0];
      // render code for one dropdown link
      $html = '
<div class="btn-group">
  ' . $button_link . '
  <button type="button" class="btn dropdown-toggle btn-dropdown-toggle-no-marg" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li>' . $dropdown_link . '</li>
    <li class="divider"></li>
    <li>' . $delete_link . '</li>
  </ul>
</div>
';
    }
    else {
      // render code for 2 dropdown links
      $html = '
<div class="btn-group">
  ' . $button_link . '
  <button type="button" class="btn dropdown-toggle btn-dropdown-toggle-no-marg" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu" role="menu">';
      foreach ($dropdown_links as $dropdown_link) {
        if(!empty($dropdown_link)) {
          $html .= '<li>' . $dropdown_link . '</li>
                  <li class="divider"></li>';
        }
      }
      // Always add delete link last
      $html .= '<li>' . $delete_link . '</li>';
      $html .= '
  </ul>
</div>
';
    }
    return $html;
  }
}

/**
 * Page callback for /RCPAR_CP_URL/quizzes
 * Shows all of the quizzes authored by the currently logged in user.
 */
function rcpar_cp_author_my_quizzes() {
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");

  // small js to add redirect function redirectFromBackEnd
  drupal_add_js("$path_to_module/js/rcpar_cp_author_quizzes.js");

  return drupal_get_form('rcpar_cp_author_quizes_form');
}

/**
 * Drupal form builder for rcpar_cp_author_quizes_form
 */
function rcpar_cp_author_quizes_form($form, &$form_state) {
  $f = new RCPARCPQuizzesForm($form, $form_state);
  // after a quiz is created/edited, we set the list email group to be shown to that of the quiz being saved
  if (isset($_GET['gid'])) {
    $form_state['values']['email_group_select'] = $_GET['gid'];
    $f->setSelectedGroup();
  }
  return $f->getForm();
}

/**
 * Form ajax callback for rcpar_cp_author_quiz_results_form
 * Returns the results area of the form when options are changed via ajax.
 */
function rcpar_cp_author_quizzes_form_ajax($form, $form_state) {
  return $form['result_table'];
}

/**
 * Load quizzes authored by the specified user.
 *
 * @param null|int $uid
 * - The user ID whose quizzes we are loading. Defaults to the current user if not provided
 * @param null|int $gid
 * - A students email group ID which might be used to filter the questions.
 *   Defaults to the all groups for which the user has added quizzes if not provided
 * @return array
 * - Array of loaded cp_quiz entities indexed by id
 */
function rcpar_cp_get_quizzes_authored_by_uid($uid = null, $gid = null) {
  // If no uid is supplied, use the current user
  $uid = is_null($uid) ? $GLOBALS['user']->uid : $uid;

  $query = new EntityFieldQuery();

  // Query for quizzes authored by the specified user
  $result = $query->entityCondition('entity_type', 'cp_question_group')
    ->entityCondition('bundle', 'cp_quiz')
    ->propertyCondition('uid', $uid, '=')
    ->pager(10);

  // Optionally, we might have to filter by student group
  if(!is_null($gid)) {
    $query->fieldCondition('field_cp_student_group', 'value', array($gid), 'IN');
  }
  $result = $query->execute();

  // If we found quizzes, return an array of loaded quiz entities indexed by id
  if (!empty($result['cp_question_group'])) {
    return entity_load('cp_question_group', array_keys($result['cp_question_group']));
  }

  // No quizzes found, return empty array
  return array();
}


/**
 * Page callback for RCPAR_CP_URL/RCPAR_CP_PUB_URL/creator/overview
 * Shows all of the publisher quizzes
 */
function rcpar_cp_palcreator_quizzes_page() {
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");

  // small js to add redirect function redirectFromBackEnd
  drupal_add_js("$path_to_module/js/rcpar_cp_author_quizzes.js");

  // get create but & act sample prod select form
  $top_form = drupal_get_form('rcpar_cp_author_pal_quizzes_form');
  $top_form_render = drupal_render($top_form);

  // get act sample form for reordering quizzes
  $prod_id = $top_form['sample_prod_select']['#default_value'];
  $prod_form_render = rcpar_cp_get_act_sample_prod_form_render($prod_id);
  return $top_form_render . $prod_form_render;
}

/**
 * Drupal form builder for rcpar_cp_author_pal_quizzes_form
 */
function rcpar_cp_author_pal_quizzes_form($form, &$form_state) {
  $f = new RCPARCPPalQuizzesForm($form, $form_state);
  // after a quiz is created/edited, we set select_id to be shown to that of the quiz being saved
  if (isset($_GET['select_id'])) {
    $form_state['values']['sample_prod_select'] = $_GET['select_id'];
    $f->setSelectedGroup();
  }
  return $f->getForm();
}

/**
 * Ajax callback for rcpar_cp_author_pal_quizzes_form Create a quiz button
 *
 * @param $form
 * @param $form_state
 */
function rcpar_cp_author_pal_quizzes_form_ajax_cb($act_sample_prod_sku) {
  // Redirect to create the publisher quiz
  $commands = array();
  $commands[] = array(
    // Note: this command is added to rcpar_cp_author_quizzes.js file, identical to redirectAfterCpSubmit in rcpar_cp.js
    'command' => 'redirectFromBackEnd',
    'path' => url(RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/creator/' . $act_sample_prod_sku .'/create'),
  );
  print ajax_render($commands);
  drupal_exit();
}

/**
 * Ajax callback for rcpar_cp_author_pal_quizzes_form ACT sample product select
 * @param $form
 * @param $form_state
 * @return mixed
 */
function rcpar_cp_author_quiz_palquizzes_form_select_ajax($form, &$form_state) {
  $f = new RCPARCPPalQuizzesForm($form, $form_state);
  $f->setSelectedGroup();
  $commands = array();
  // replace the create a quiz button (to update it's act sample prod id)
  $button_render = drupal_render($form['top']);
  $commands[] = ajax_command_replace('#create-pub-quiz-button-container', $button_render);

  //  replace the quizzes table
  $prod_form_render = rcpar_cp_get_act_sample_prod_form_render($f->getSelectedGroup());
  $commands[] = ajax_command_replace('#pub-quiz-list-act-sample-prod-quiz-form', $prod_form_render);

  // remove any drupal messages (from quiz reordering)
  $commands[] = ajax_command_invoke('.messages.alert', 'remove');

  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Get the act sample product form formatted for use with the publisher quiz creator overview page
 *
 * @param integer $prod_id the product ID of a sample_act_material product
 * @return string renderable HTML for the commerce product form with cancel link removed,
 */
function rcpar_cp_get_act_sample_prod_form_render($prod_id) {
  // get image & title
  $aspc = new ActSampleProduct($prod_id);
  $prod_title = $aspc->getTitle();
  $prod_image = $aspc->getImage();
  
  // if there are no quizzes yet, don't bother rendering the form
  $prod_quizzes = $aspc->getQuizzes();
  $no_quizzes = TRUE;
  if (!empty($prod_quizzes)) {
    // this is needed only because if a quiz is deleted using the EntityToolKit UI, it will leave an empty array in the product's quiz ref field
    foreach ($prod_quizzes AS $prod_quiz) {
      if (!empty($prod_quiz)) {
        $no_quizzes = FALSE;
        break;
      }
    }
  }
  if ($no_quizzes) {
    $prod_form_render = '<p class="act-sample-prod-no-quizzes-msg">There are no quizzes associated with this ACT Sample Material product yet.</p>';
  }
  else {
    $product = commerce_product_load($prod_id);
    module_load_include('inc', 'commerce_product', 'includes/commerce_product_ui.products');
    $prod_form = commerce_product_ui_product_form_wrapper($product);
    // Make the label more specific.
    $prod_form['title']['#title'] = 'ACT Sample Product Title';
    $prod_form['#action'] = url(RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/creator/overview', array('query' => array('select_id' => $prod_id)));
    // lose the cancel button, add a link to main config page
    $prod_form['actions']['submit']['#suffix'] = '<p class="act-sample-prod-submit-suffix">' . l('Open product configuration page', url('admin/commerce/products/' . $prod_id . '/edit')) . '</p>';
    $prod_form_render = drupal_render($prod_form);
  }

  $render_start = '<div id="pub-quiz-list-act-sample-prod-quiz-form" class="quiz-results-table">';
  $render_start .= '<p class="act-sample-prod-img-title">';

  // Render a product image if we have one
  if (!empty($prod_image)) {
    $render_start .= '<img class="act-sample-prod-img" src="' . image_style_url('thumbnail', $prod_image['uri']) . '"/>';
  }

  $render_start .= '<span class="act-sample-prod-title">' . $prod_title . '</span>';
  $render_start .= '</p>';
  return $render_start . $prod_form_render . '</div>';
}

/**
 * Ajax callback for copy quiz action, creates a draft status clone of the quiz
 * NOTE: currently for ACT ASL quizzed only. Cloneing publisher quizzes would need some refactoring.
 *
 * @param integer $quiz_id the id of the quiz
 */
function rcpar_cp_copy_quiz_ajax_cb($quiz_id) {
  $entity_type = 'cp_question_group';
  $orig_entity = entity_load_single($entity_type, $quiz_id);

  $copy_entity = clone $orig_entity;

  // make it a new quiz
  $copy_entity->id = NULL;
  // set creator to the current user
  $copy_entity->uid = $GLOBALS['user']->uid;
  // set new times to now
  $copy_entity->created = REQUEST_TIME;
  $copy_entity->changed = REQUEST_TIME;
  // rename title
  $copy_entity->title = t('Copy of !title', array('!title' => $copy_entity->title));

  // set the status to draft (wrapper really wouldn't be easier)
  $copy_entity->field_cp_quiz_status = array(
    LANGUAGE_NONE => array(
      0 =>
        array(
          'value' => '0',
        ),
    ),
  );
  // get the email group list for redirecting afterwards
  // NOTE: this will have to change if this feature is added to publisher quizzes
  $field_cp_student_group = $copy_entity->field_cp_student_group[LANGUAGE_NONE][0]['value'];

  entity_save($entity_type, $copy_entity);

  drupal_set_message(t('Your quiz has been saved'));

  // Redirect to the creator quizzes page to redraw it & show the new quiz
  $commands = array();
  $commands[] = array(
    // Note: this command is added to rcpar_cp_author_quizzes.js file, identical to redirectAfterCpSubmit in rcpar_cp.js
    'command' => 'redirectFromBackEnd',
    'path' => url(RCPAR_CP_URL . '/quizzes', array('query' => array('gid' => $field_cp_student_group))),
  );
  print ajax_render($commands);
  drupal_exit();
}

/**
 * Ajax callback for delete quiz action, displays confirmation modal to confirm quiz & record deletion
 *
 * @param int $quiz_id
 * - Entity ID of a quiz
 */
function rcpar_cp_delete_quiz_ajax_cb($quiz_id) {
  // Get a quiz class to get the quiz group id used for the destination when confirmation is confirmed
  // and in checking that current user has edit quiz access.
  $q = new CPQuiz($quiz_id);

  // Check that the current user has edit access to the quiz we are deleting.
  $profUser = new CPUser();
  if (!$profUser->hasEditAccessForQuiz($q)) {
    drupal_access_denied();
  }


  // needed for to display results in modal
  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  $abortLink = l(
    'Never mind, abort!',
    '#',
    array(
      'attributes' => array(
        'class' => array(
          'cpQuizWarnButton',
          'abortEdit',
          'ctools-close-modal',
          'btn'
        )
      )
    )
  );

  $confirmLink = l(
    'Yes, delete this quiz.',
    RCPAR_CP_URL . "/quizzes/delete/confirm/ajax/$quiz_id",
    array(
      'attributes' => array(
        'class' => array(
          'cpQuizWarnButton',
          'doEdit',
          'btn',
          'btn-success',
          'btn-primary'
        )
      ),
      'query' => array('destination' => RCPAR_CP_URL . '/quizzes?gid=' . $q->getStudentGroup()),
    )
  );

  // create the html for the modal
  $my_html = strtr('
    <div class="cpQuizEditWarn">
      <h2 class="heading">Confirm Deletion</h2>
      <p>Are you sure you want to delete this quiz? If the quiz is active, students will no longer be able to take it, 
      and any related student reporting data will be permanently removed.</p>
      <p>
      <div class="cpQuizWarnButtons">!abortlink &nbsp; !confirmlink</div>
    </div>',
    array(
      '!abortlink' => $abortLink,
      '!confirmlink' => $confirmLink,
    )
  );

  // throw the warning into a modal
  ctools_modal_render('', $my_html);
  drupal_exit();
}

/**
 * Ajax callback for quiz deletion called by rcpar_cp_delete_quiz_ajax_cb when delete action is confirmed.
 *
 * @param integer $quiz_id
 *  - the quiz id
 */
function rcpar_cp_delete_quiz_confirmed_ajax_cb($quiz_id) {
  // Get a quiz class to get the email group id this quiz belongs to and check the current user
  // has access to edit the quiz we are deleting.
  $q_class = new CPQuiz($quiz_id);

  // Check that the current user has edit access to the quiz we are deleting.
  $profUser = new CPUser();
  if (!$profUser->hasEditAccessForQuiz($q_class)) {
    drupal_access_denied();
  }

  $gid = $q_class->getStudentGroup();

  // Find all active user ids in this email group.
  $query = db_query("SELECT u.uid
                      FROM {rcpa_partners_emails} rpe
                      INNER JOIN {users} u ON u.mail = rpe.email
                      WHERE gid = :gid",
    array(':gid' => $gid));

  $gid_uids = $query->fetchCol();

  // Delete the quiz sessions for each of the active users of this email group (only if they've taken the quiz.)
  foreach ($gid_uids AS $gid_uid) {
    $user_class = new CPUser($gid_uid);
    if (sizeof($user_class->getSessionDataForQuiz($quiz_id)) > 0) {
      $user_class->deleteSessionDataForQuiz($quiz_id);
    }
  }

  // Set a message before deleting the quiz.
  drupal_set_message(rcpar_cp_get_icon('message-tick') . t('The quiz %quiz has been deleted.', array(
      '%quiz' => $q_class->getTitle()
    ))
  );

  // Delete the quiz itself.
  $entity_type = 'cp_question_group';
  entity_delete($entity_type, $quiz_id);

  // Redirect to our previously set destination.
  drupal_goto();

}
