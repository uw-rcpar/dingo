<?php

/**
 * Page callback for /RCPAR_CP_URL/groups
 * @return array|mixed
 */
function rcpar_cp_author_groups_page() {
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");

  return drupal_get_form('rcpar_cp_author_groups_form');
}

/**
 * Drupal form builder for rcpar_cp_author_groups_form
 */
function rcpar_cp_author_groups_form($form, &$form_state) {
  $f = new RCPARCPGroupsForm($form, $form_state);
  return $f->getForm();
}

/**
 * Form ajax callback for rcpar_cp_author_quiz_results_form
 * Returns the results area of the form when options are changed via ajax.
 */
//function rcpar_cp_author_quiz_results_form_ajax($form, $form_state) {
function rcpar_cp_author_groups_form_ajax($form, $form_state) {
  return $form['result_table'];
}


/**
 * Class RCPARCPGroupsForm
 *
 * Controls form functionality for rcpar_cp_author_groups_form
 *
 * Note that we are extending RCPARCPResultsForm even as that class contains lots of functions and features
 * that we are not using
 * probably a more elegant way to resolve this would have been to move the functionality that
 * this class and RCPARCPResultsForm uses to a super class and extend both classes from it
 * but after a discussion we decided that this approach was quicker and as both forms works
 * we are just going to leave it in this way (see also RCPARCPQuizzesForm class which is on the same situation)
 */
class RCPARCPGroupsForm extends RCPARCPResultsForm {
  public function __construct($form, &$form_state) {
    $this->form = $form;
    $this->form_state =& $form_state;

    $this->setEmailGroupOptions();
    $this->setSelectedGroup(); // Depends on getEmailGroupOptions()
    $this->setselectedGroupPid(); //Depends on setSelectedGroup()
  }

  /**
   * Returns the constructed form array.
   * @return array
   */
  public function getForm() {
    $form = $this->form;

    // Select list form email group
    $form['email_group_select'] = array(
      '#type'          => 'select',
      '#options'       => $this->getEmailGroupOptions(),
      '#default_value' => $this->getSelectedGroup(),
      '#prefix'        => '<div id="group-select-wrapper">',
      '#suffix'        => '</div>',
      '#ajax'          => $this->getResultsAjaxOptions()
    );

    $table = $this->getUsersOnCurrentGroup();
    if (!empty($table)) {
      $form['result_table'] = $table;
    }

    $form['result_table']['#prefix'] = '<div id="results-table" class="quiz-results-table">';
    $form['result_table']['#suffix'] = '</div>';

    return $form;
  }

  /**
   * Returns an array of options for FAPI's #ajax property.
   * @return array
   */
  public function getResultsAjaxOptions() {
    return array(
      'callback' => 'rcpar_cp_author_groups_form_ajax',
      'wrapper'  => 'results-table',
      'method'   => 'replace',
      'effect'   => 'fade',
    );
  }

  /**
   * Returns a subsection of the form when results are shown by roster.
   * @return array
   */
  public function getUsersOnCurrentGroup() {
    $selected_group = $this->getSelectedGroup();

    $header = array('Student Name', 'Email', 'Username', 'Last Login', 'Account Created');

    $rows = array();
    $email_group_uids = rcpar_partners_get_email_group_users($selected_group);

    // Create the rows of individual students
    foreach ($email_group_uids AS $email => $uid) {
      $row = array();

      if (!empty($uid)) {
        $cpUser = new CPUser($uid);
        $not_reg_user = FALSE;
      }
      else {
        $not_reg_user = TRUE;
      }

      // Note that account might be null (if $not_reg_user == true)
      $account = ((isset($cpUser)) ? $cpUser->getDrupalUser() : NULL);

      // First column: First and last name (if available, otherwise username)
      // if not registered yet show email & a notice (same one lecture progress uses)
      $row[] = (($not_reg_user) ? $email . '<div class="student-not-registered">Unregistered user</div>' : $cpUser->getDisplayName());
      $row[] = (($not_reg_user) ? '--' : '<a href="mailto:'. $email . '">' . $email . '</a>');
      $row[] = (($not_reg_user) ? '--' : $account->name);
      // If the user completed the registration but didn't logged in again after that
      // $account->login will be NULL
      $row[] = (($not_reg_user || !$account->login) ? '--' : format_date($account->login, 'custom', 'm/d/y, g:i a'));
      $row[] = (($not_reg_user) ? '--' : format_date($account->created, 'custom', 'm/d/y, g:i a'));

      $rows[] = $row;
    }

    // Return a renderable table
    return array(
      '#theme'      => 'table',
      '#header'     => $header,
      '#rows'       => $rows,
      '#attributes' => array(
        'class' => array('quiz-results'),
      ),
    );
  }

}