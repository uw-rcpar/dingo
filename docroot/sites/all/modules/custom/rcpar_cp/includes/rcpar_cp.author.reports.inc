<?php

/**
 * Page callback for /RCPAR_CP_URL/reports
 * @return array|mixed
 */
function rcpar_cp_author_reports_page() {
  // Add CSS & JS
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");
  drupal_add_js($path_to_module . '/js/rcpar_cp_reports.js');
  drupal_add_js($path_to_module . '/js/rcpar_cp_preview_bind.js');

  return drupal_get_form('rcpar_cp_author_quiz_results_form');
}

/**
 * Drupal form builder for rcpar_cp_author_quiz_results_form
 */
function rcpar_cp_author_quiz_results_form($form, &$form_state) {
  if (isset($_GET['gid'])) {
    $form_state['values']['email_group_select'] = $_GET['gid'];
  }
  $f = new RCPARCPResultsForm($form, $form_state);
  return $f->getForm();
}

/**
 * Form ajax callback for rcpar_cp_author_quiz_results_form
 * Returns the results area of the form when options are changed via ajax.
 */
function rcpar_cp_author_quiz_results_form_ajax($form, $form_state) {
  return $form['result_table'];
}

/**
 * Class RCPARCPResultsForm
 *
 * Controls form functionality for rcpar_cp_author_quiz_results_form
 *
 * This exists in class form for organization and flexibility.
 * After the constructor is called, various other options can be manipulated using the class methods or public
 * properties before producing a form array if needed.
 */
class RCPARCPResultsForm {
  public $form;
  public $form_state;

  public $emailGroupOptions; // The options available in the email group select list
  public $selectedGroup;     // The currently selected email group
  public $quizGroupingType;  // 'roster' or 'content'
  public $resultsType;       // 'quiz' or 'lecture'
  public $quizzes;           // Array of cp_quiz entities (or empty array)
  public $selectedGroupPid;


  public function __construct($form, &$form_state) {
    $this->form = $form;
    $this->form_state =& $form_state;

    $this->setResultsType();
    $this->setQuizGroupingType();
    $this->setEmailGroupOptions();
    $this->setSelectedGroup(); // Depends on getEmailGroupOptions()
    $this->setselectedGroupPid(); //Depends on setSelectedGroup()
    $this->setQuizzes();  // Depends on setSelectedGroup()
  }

  /**
   * Returns the constructed form array.
   * @return array
   */
  public function getForm() {
    $form = $this->form;

    // Results type radio options (Quiz results or Lecture progress)
    $form['results_type'] = array(
      '#type'          => 'radios',
      '#title'         => 'Results type',
      '#options'       => array(
        'quiz'    => 'Quiz results',
        'lecture' => 'Lecture progress',
      ),
      '#default_value' => $this->getResultsType(),
      '#ajax' => $this->getResultsAjaxOptions()
    );

    // Grouping: 'By roster' or 'By content'
    $form['quiz_grouping'] = array(
      '#type'          => 'radios',
      '#title'         => 'Grouping',
      '#options'       => array(
        'roster'  => 'By roster',
        'content' => 'By content',
      ),
      '#default_value' => $this->getQuizGroupingType(),
      '#ajax'          => $this->getResultsAjaxOptions(),
    );

    // Select list form email group
    $form['email_group_select'] = array(
      '#type'          => 'select',
      '#title'         => t('Course'),
      '#options'       => $this->getEmailGroupOptions(),
      '#default_value' => $this->getSelectedGroup(),
      '#prefix'        => '<div id="results-form-wrapper">',
      '#suffix'        => '</div>',
      '#ajax'          => $this->getResultsAjaxOptions()
    );

    /****** Quiz Results ******/
    if ($this->getResultsType() == 'quiz') {
      // Start out with a default empty value
      // No quizzes for this group. We re-use 'result_table' element so that it can be returned by our ajax form callback.
      $form['result_table'] = array(
        '#markup' => '<div class="no-results">No quizzes have been assigned to the selected group.</div>',
      );

      // Results by roster
      if ($this->getQuizGroupingType() == 'roster') {
        // Generate a table of student scores for each quiz
        $table = $this->getResultsByRoster();
        if (!empty($table)) {
          $form['result_table'] = $table;
          // We are providing a CSV version of the results by roster page
          // we need the link inside the result_table div for it to be updated when users change the current group
          $form['result_table']['#suffix'] =
            l('Export to CSV', RCPAR_CP_URL . '/reports/roster/csv/' . $form['email_group_select']['#default_value'],
              array('attributes' => array('target' => '_blank')));
        }
      }
      // Results by content
      else {
        // Generate a table of student scores for each quiz
        $table = $this->getResultsByContent();
        if (!empty($table)) {
          $form['result_table'] = $table;
        }
      }

      // give the wrapping div a different class if results are lecture or quiz
      $form['result_table']['#prefix'] ='<div id="results-table" class="quiz-results-table">';
      $form['result_table']['#suffix'] .='</div>';
    }
    /****** Lecture Progress ******/
    else {
      $form['result_table'] = array(
        // give the wrapping div a different class if results are lecture or quiz
        '#prefix' => '<div id="results-table" class="lecture-results-table">',
        '#markup' => $this->getLectureProgress(),
        '#suffix' => '</div>',
      );
    }

    return $form;
  }

  /**
   * @return string
   *  - Rendered html to display group info
   * for lecture progress by mail.
   */
  public function getLectureProgress() {
    return theme('lecture_progress_cp', array(
      'group_id' => $this->getSelectedGroup(),
      'partner_nid' => $this->getselectedGroupPid()
      )
    );

  }

  /**
   * Returns all available email groups that the current user is an admin of.
   * @return array
   * - An array of email group titles indexed by gid
   */
  public function getEmailGroupOptions() {
    return $this->emailGroupOptions;
  }
  public function setEmailGroupOptions() {
    $cpUser = new CPUser();
    $this->emailGroupOptions = $cpUser->getCreatorsACTEnabledEmailGroupsFormOptions();
  }

  /**
   * Returns the gid of the selected email group. Defaults to the first available if no user input.
   * @return int
   */
  public function getSelectedGroup() {
    return $this->selectedGroup;
  }
  public function setSelectedGroup() {
    $form_state =& $this->form_state;

    // Look for a selected group from the form post
    if (isset($form_state['values']['email_group_select']) && !is_null($form_state['values']['email_group_select'])) {
      $selected_group = $form_state['values']['email_group_select'];
      $_SESSION['act_selected_group'] = $selected_group;
    }
    // No post, see if we remember the last selected group
    elseif(!is_null($_SESSION['act_selected_group'])) {
      $selected_group = $_SESSION['act_selected_group'];
    }
    // Default to the first available value
    else {
      // Get a raw (ungrouped) list of available email groups
      $cpUser = new CPUser();
      $email_group_select_options = $cpUser->getCreatorsACTEnabledEmailGroups();

      // Use the first one
      reset($email_group_select_options);
      $selected_group = key($email_group_select_options);
    }

    $this->selectedGroup = $selected_group;
  }

  /**
   * Determines if we show quiz results or lecture progress.
   * @return string
   * - 'quiz' or 'lecture'
   */
  public function getResultsType() {
    return $this->resultsType;
  }
  public function setResultsType() {
    $form_state =& $this->form_state;
    $this->resultsType = !empty($form_state['values']['results_type']) ? $form_state['values']['results_type'] : 'quiz';
  }

  /**
   * Determines how results are grouped.
   * @return string
   * - 'roster' or 'content'
   */
  public function getQuizGroupingType() {
    return $this->quizGroupingType;
  }
  public function setQuizGroupingType() {
    $form_state =& $this->form_state;
    $this->quizGroupingType = !empty($form_state['values']['quiz_grouping']) ? $form_state['values']['quiz_grouping'] : 'roster';
  }

  /**
   * Returns a subsection of the form when results are shown by roster.
   * @param bool $raw_formatting
   * - When TRUE, we are producing output for a CSV
   * @return array
   */
  public function getResultsByRoster($raw_formatting = FALSE) {
    $selected_group = $this->getSelectedGroup();
    $quizzes = rcpar_cp_get_quizzes_in_email_group($selected_group, TRUE);

    if (!empty($quizzes)) {
      // Build the table header
      $header = array('Student Name');
      foreach ($quizzes as $quiz) {
        $header[] = $quiz->title;
      };

      $rows = array();
      $email_group_uids = rcpar_partners_get_email_group_users($selected_group);

      // FOR TESTING ONLY: Add admin user to the list so it's easier to see a score.
      // $email_group_uids[] = 1;

      // Calculate an average for each quiz on a whole based on all student averages
      $quiz_scores = array();
      $quiz_attempts = array();

      foreach ($email_group_uids AS $email => $uid) {
        if (!empty($uid)) {
          $cpUser = new CPUser($uid);
          foreach ($quizzes as $quiz_id => $quiz_entity) {
            $quiz_data = $cpUser->getLatestSessionDataForQuiz($quiz_id);
            if (!empty($quiz_data)) {
              foreach ($quiz_data as $data) {
                if ($data->is_correct) {
                  $quiz_scores[$quiz_id]++;
                }
                $quiz_attempts[$quiz_id]++;
              }
            }
          }
        }
      }

      // Add the initial row of total averages
      if ($raw_formatting){
        $row = array('Average');
      }
      else {
        $row = array('<strong>Average</strong>');
      }

      foreach ($quizzes as $quiz_id => $quiz_entity) {
        if(isset($quiz_attempts[$quiz_id])) {
          $val = round(($quiz_scores[$quiz_id] / $quiz_attempts[$quiz_id]) * 100, 1) . '%';
        }
        // No attempts for this quiz, indicate never taken
        else {
          if ($raw_formatting){
            $val = '';
          }
          else {
            $val = '--';
          }
        }

        if ($raw_formatting){
          $row[] = $val;
        }
        else {
          $row[] = "<strong>$val</strong>";
        }

      }
      $rows[] = $row;

      // Create the rows of individual student scores
      foreach ($email_group_uids AS $email => $uid) {
        $row = array();

        if (!empty($uid)) {
          $cpUser = new CPUser($uid);
          $not_reg_user = FALSE;
        }
        else {
          $not_reg_user = TRUE;
        }
        // First column: First and last name (if available, otherwise username)
        // if not registered yet show email & a notice (same one lecture progress uses)
        if ($raw_formatting){
          $row[] = (($not_reg_user) ? $email . ' (Unregistered user)' : $cpUser->getDisplayName());
        }
        else {
          $row[] = (($not_reg_user) ? $email . '<div class="student-not-registered">Unregistered user</div>' : $cpUser->getDisplayName());
        }


        // The next columns will be the quizzes
        foreach ($quizzes as $quiz_id => $quiz_entity) {
          $quiz_data = (($not_reg_user) ? NULL : $cpUser->getLatestSessionDataForQuiz($quiz_id));

          if (!empty($quiz_data)) {
            $score = 0;
            $max_score = 0;

            // Add up each correct question and each question attempt.
            // Note: We could probably do the percentage calculation with a mysql query and simplify code, however there
            // are a huge number of records in ipq_saved_session_data, so this may not be more efficient.
            // Haven't tried. -jd
            foreach ($quiz_data as $data) {
              // By questions correct:
              if($data->is_correct) {
                $score++;
              }
              $max_score++;

              // By questions score:
              // NOTE: TBS question 3077487 gave me a score of 5 of 12 (but is_correct false and percent correct 0) before I
              // had even tried to answer the question. It seems there is some buggy data in the score column, however
              // percent_correct seems to be accurate.
              //if($data->is_correct) {
              //  $score += $data->score;
              //}
              //$max_score += $data->max_score;
            }

            // Calculate percentage and round to a single decmial point
            $perc = round(($score / $max_score) * 100, 1) . '%';

            if ($raw_formatting) {
              $row[] = $perc;
            }
            else { // (For screen-view of the report only)
              //Generate a link for the instructor to reset the score for this user's assignment
              $reset_score_link = l(
                'Reset Score',
                RCPAR_CP_URL . '/quizzes/reset/nojs/' . $cpUser->getDrupalUser()->uid . '/' . $quiz_id,
                array('attributes' => array('class' => array('use-ajax')))
              );
              $row[] = $perc . '<br>' . $reset_score_link;
            }
          }
          else {
            // Indicate that this user has never taken this quiz:
            if ($raw_formatting) {
              $row[] = '';
            }
            else {
              $row[] = '--';
            }
          }
        }
        $rows[] = $row;
      }

      // Return a renderable table
      return array(
        '#theme'      => 'table',
        '#header'     => $header,
        '#rows'       => $rows,
        '#sticky'     => FALSE,
        '#attributes' => array(
          'class' => array('quiz-results'),
        ),
      );
    }

    // No quizzes, return empty array
    return array();
  }

  /**
   * Returns the quiz entity id of the currently selected quiz. If no user input, defaults to the first available.
   * If no quizzes are available for the group, will return 0.
   * @return int
   */
  public function getSelectedQuiz() {
    $form_state =& $this->form_state;
    $quizzes = $this->getQuizzesForSelectedGroup();

    // We can only have a selected quiz if any quizzes are available for this group
    if(!empty($quizzes)) {
      // Get the value from form_state if user has supplied input
      if (!empty($form_state['values']['quiz_select'])) {
        $selected_quiz_id = $form_state['values']['quiz_select'];

        // We need to make sure that the selected quiz id is actually in the options list.
        // When the user changes student groups, the last selected quiz id is still remembered,
        // so we need to default to the first value in the new group
        if(!isset($quizzes[$selected_quiz_id])) {
          reset($quizzes);
          $selected_quiz_id = key($quizzes);
        }
      }
      // Otherwise, get the first selected value
      else {
        reset($quizzes);
        $selected_quiz_id = key($quizzes);
      }
    }
    else {
      $selected_quiz_id = 0;
    }

    return $selected_quiz_id;
  }

  /**
   * Returns an array of options for FAPI's #ajax property that are shared among form sections.
   * @return array
   */
  public  function getResultsAjaxOptions() {
    return array(
      'callback' => 'rcpar_cp_author_quiz_results_form_ajax',
      'wrapper'  => 'results-table',
      'method'   => 'replace',
      'effect'   => 'fade',
    );
  }

  /**
   * Gets the quizzes for the currently selected email group.
   * @return array
   * - An array of loaded cp_quiz entities index by entity id, or an empty array if none are available.
   */
  public function getQuizzesForSelectedGroup() {
    return $this->quizzes;
  }

  /**
   * Sets the quizzes param for the class to those that are published and belong to the selected email group
   */
  public function setQuizzes() {
    $selected_group = $this->getSelectedGroup();
    $this->quizzes = rcpar_cp_get_quizzes_in_email_group($selected_group, TRUE);
  }


  /**
   * Returns a subsection of the form when results are shown by content.
   * @param bool $raw_formatting
   * - if TRUE then format result for csv report
   * @return array
   */
  public function getResultsByContent($raw_formatting = FALSE) {
    $form = array();

    // Build an options list of quizzes
    $quiz_options = array();
    foreach ($this->getQuizzesForSelectedGroup() as $qid => $quiz) {
      $quiz_options[$qid] = $quiz->title;
    }

    // Select list form email group
    if(!empty($quiz_options)) {
      $form['quiz_select'] = array(
        '#type'          => 'select',
        '#title'         => t('Quiz'),
        '#options'       => $quiz_options,
        '#default_value' => $this->getSelectedQuiz(),
        '#ajax'          => $this->getResultsAjaxOptions(),
      );
    }


    // Load the selected quiz
    $quiz = entity_load_single('cp_question_group', $this->getSelectedQuiz());

    // Iterate through each question reference and grab its topic
    $questions_by_topic = array();
    $topic_data = array();
    $chapter_data = array();
    $topic_labels = array();
    try {
      $w = entity_metadata_wrapper('cp_question_group', $quiz);
      foreach ($w->field_cp_question->getIterator() as $node_wrapper) {
        // We need to create one entry on ipq_question_pool per exam version year
        foreach ($node_wrapper->field_section_per_exam_version as $ev_info) {
          $exam_version = $ev_info->field_exam_version_single_val->name->value(); // name will produce '2016' or '2017', etc.

          // Only handle default version exam. @todo - Needs to be future proofed.
          if ($exam_version != exam_version_get_default_version()) {
            continue;
          }

          $section_id = $ev_info->field_ipq_course_section->value()->tid;
          $chapter_id = $ev_info->field_ipq_chapter->value()->nid;

          // Get chapter data for this course
          $course = ipq_common_get_online_course_by_section_tid($section_id);
          if (!isset($chapter_data[$course->nid])) {
            $chapter_data[$course->nid] = ipq_common_get_chapters_by_course_nid($course->nid, FALSE, $exam_version);
          }

          // Get topic data for this chapter
          if (!isset($topic_data[$chapter_id])) {
            $topic_data[$chapter_id] = ipq_common_get_topics_by_chapter_nid($chapter_id, $exam_version);
          }

          // Find the topic in our topic data
          // We need to iterate through the question's topics to find the last topic this question has that is in the
          // chapter's topics (currently only 2017 topics)
          // Sometimes the questions are erroneously tagged w/ topics that aren't in the chapters 2017 topics
          // this will assure that those bad topics aren't used
          $my_topic_id = NULL;

          foreach ($ev_info->field_ipq_topic->getIterator() AS $q_topic_wrapper) {
            foreach ($topic_data[$chapter_id] as $my_topic_delta => $my_topicNode) {
              if ($my_topicNode->nid == $q_topic_wrapper->nid->value()) {
                $my_topic_id = $my_topicNode->nid;
                $selected_topic_delta = $my_topic_delta;
                $selected_topicNode = $my_topicNode;
                // Note: no break here because the quiz creator also assigns questions to their the last (2017) topic
              }
            }
          }

          // Store topic label
          $topic_labels[$my_topic_id] = strtr('@section @chapterDelta.@topicDelta: @topicTitle', array(
            '@section' => $ev_info->field_ipq_course_section->name->value(),
            '@chapterDelta' => $chapter_data[$course->nid][$chapter_id]['delta'],
            '@topicDelta' => $selected_topic_delta + 1,
            '@topicTitle' => $selected_topicNode->title,
          ));
          $questions_by_topic[$my_topic_id][] = $node_wrapper;
        }
      }
    }
    catch (EntityMetadataWrapperException $e) {
      $form['empty'] = array('#markup' => '<div class="no-results">No quizzes have been assigned to the selected group.</div>');
      return $form;
    }

    if ($raw_formatting) { // preparing report for CSV output
      $rows = array();
      // Set up header for CSV report
      $row = array('Quiz: ' . $quiz_options[$this->getSelectedQuiz()]);
      $rows[] = $row;

      // Set up column headers for the rest of the report
      $row = array(
        'Quiz topic / question',
        'Average score',
        'Question ID',
      );
      $rows[] = $row;

    }
    else { // default screen-view output
      // Build the table header
      $form['results']['header'] = array(
        '#markup' => '<div class="row-wrapper header row"><div class="col-sm-10 column-left">Quiz topic / question</div><div class="col-sm-2 column-right">Average score</div></div>'
      );
    }
    // Iterate over every question in this quiz
    foreach ($questions_by_topic as $topic_id => $questions) {
      // Reset questions markup
      if ($raw_formatting) {
        // Questions are shown categorized by topic with the avg score for the questions in this topic. We'll add this after gathering each topic's questions data.

        // There will be one or more question per topic, hence "rows". They are shown under the topic. We'll add the question id to this row as well.
        $topic_question_rows = array();
      }
      else {
        $questionAveragesHTML = '';
      }

      // We track the number of question attempts and answers correct per topic so we can generate a topic average as well
      // as tell the difference between 0% and no answers.
      $num_question_attempts_in_topic = 0;
      $num_questions_correct_in_topic = 0;

      // Generate averages for each question and then add up topic average
      foreach ($questions as $q_node_wrapper) {

        // Get the average score for this question in this quiz
        $question_session_data = rcpar_cp_session_data_for_question_in_quiz($q_node_wrapper->getIdentifier(), $this->getSelectedQuiz());
        if (!empty($question_session_data)) {
          $num_correct = 0;
          foreach ($question_session_data as $question_session_datum) {
            $num_question_attempts_in_topic++;
            if ($question_session_datum->is_correct) {
              $num_correct++;
              $num_questions_correct_in_topic++;
            }
          }
          // Calculate the percentage: number correct / number of question attempts
          $question_avg = round(($num_correct / sizeof($question_session_data)) * 100, 1);
        }
        // No data for this question, indicate a blank instead of 0
        else {
          $question_avg = '--';
        }

        // If we have a percentage, add the percentage sign.
        $question_avg = is_numeric($question_avg) ? $question_avg . '%' : $question_avg;

        // Build a row for the individual question and its score average
        $question_label = truncate_utf8($q_node_wrapper->label(), 120, TRUE, TRUE, 1);
        if ($raw_formatting) { // formatting for CSV output
          $topic_question_rows[] = array(
            $question_label,
            $question_avg,
            $q_node_wrapper->getIdentifier(),
          );
        }
        else { // default screen view output
          $question_label_preview_link = '<a data-toggle="modal" data-target="#cp-preview-question" target="_blank" class="rcpar-cp-qx-view" href="/' . RCPAR_CP_URL . '/question/preview/' . $q_node_wrapper->nid->value() . '">' . $question_label . '</a>';
          $questionAveragesHTML .= '<div class="row-wrapper row">';
          $questionAveragesHTML .= '  <div class="col-sm-10 column-left"><div class="question-text">' . $question_label_preview_link . '</div></div>';
          $questionAveragesHTML .= '  <div class="col-sm-2 column-right">' . $question_avg . '</div>';
          $questionAveragesHTML .= '</div>';
        }
      }

      // Generate average for all questions in this topic
      if($num_question_attempts_in_topic > 0) {
        $topic_avg = round(($num_questions_correct_in_topic / $num_question_attempts_in_topic) * 100, 1) . '%';
      }
      else {
        $topic_avg = '--';
      }
      if ($raw_formatting) { // format this topic's question(s) data for CSV output
        // Add topic avg score
        $rows[] = array(
          $topic_labels[$topic_id],
          $topic_avg,
          '(n/a)'
        );
        // Add topic's question data
        foreach ($topic_question_rows AS $topic_question_row) {
          $rows[] = $topic_question_row;
        }

      }
      else { // default, screen view output
        // #title contains the topic and its averages
        $form['results']['rows'][] = array(
          '#type' => 'fieldset',
          '#title' => '<div class="col-sm-10 column-left"><div class="topic-name">' . $topic_labels[$topic_id] . '</div></div><div class="col-sm-2 column-right">' . $topic_avg . '</div><div class="clear-empty"></div>',
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#pre_render' => array('rcpar_cp_process_fieldset'),
          '#attributes' => array('class' => array('row-wrapper row')),
          // contents contains all questions in the topic and their averages.
          'contents' => array('#markup' => $questionAveragesHTML),
        );

      }
    }
    if ($raw_formatting) {
      // Return a renderable table for CSV output
      return array(
        '#theme' => 'table',
        '#rows' => $rows,
      );

    }
    else { // add the Export to CSV link to the form & return the form
      $form['footer'] = array(
        '#suffix' => l('Export to CSV', RCPAR_CP_URL . '/reports/content/csv/' . $this->getSelectedGroup() . '/' . $this->getSelectedQuiz(),
          array('attributes' => array('target' => '_blank'))),
      );
      return $form;
    }
  }
  
  /**
   * @return the partner id based on the current selected group
   * or false if no results or error
   */
  public function getselectedGroupPid() {
      return $this->selectedGroupPid;
}

/**
   * Set the partner for the current selected group property
   */
  public function setselectedGroupPid() {
        $this->selectedGroupPid = 
            db_select('rcpar_partners_emails_group', 'gg')
            ->fields('gg', array('partner_nid'))
            ->condition('gg.id', $this->getSelectedGroup())
            ->execute()->fetchField();
    }

}

/**
 * Load quizzes assigned to a particular email group ordered by creation date (ascending).
 *
 * @param int $gid
 * - ID from rcpar_partners_emails_group
 * @param bool $not_drafts
 * - (default FALSE) if TRUE show only published quizzes
 * @return array
 * - Array of loaded cp_quiz entities indexed by id
 */
function rcpar_cp_get_quizzes_in_email_group($gid, $not_drafts = FALSE) {
  $query = new EntityFieldQuery();

  // Query for quizzes authored by the specified user
  $query->entityCondition('entity_type', 'cp_question_group')
    ->entityCondition('bundle', 'cp_quiz')
    ->fieldCondition('field_cp_student_group', 'value', $gid);

  if ($not_drafts) {
    $query->fieldCondition('field_cp_quiz_status', 'value', TRUE);
  }

  $query->propertyOrderBy('created', 'ASC');

  $result = $query->execute();

  // If we found quizzes, return an array of loaded quiz entities indexed by id
  if (!empty($result['cp_question_group'])) {
    return entity_load('cp_question_group', array_keys($result['cp_question_group']));
  }

  // No quizzes found, return empty array
  return array();
}

/**
 * For a given user and quiz, returns ALL session data (ipq_saved_session_data) that the user has generated for that
 * quiz.
 *
 * @param $uid
 * - User id of the user
 * @param $quiz_id
 * - Id of the cp_quiz entity
 * @return array
 * - Returns an array of stdclass objects. Each object contains all of the fields from the ipq_saved_session_data table.
 */
function rcpar_cp_get_student_session_data_for_quiz($uid, $quiz_id) {
  return db_query("SELECT i.ipq_saved_sessions_id, i.updated_on, i.is_correct FROM {ipq_saved_session_data} i, {rcpar_cp_ipq_map} map 
      WHERE i.uid = :uid
      AND map.quiz_id = :quizid
      AND i.ipq_saved_sessions_id = map.ipq_saved_sessions_id
      ", array(':uid' => $uid, ':quizid' => $quiz_id))->fetchAllAssoc('id');
}

/**
 * When fieldset elements are rendered outside of FAPI, they don't get collapsible stuff attached to them,
 * so we add a process function with a snippet stolen from form_process_fieldset()
 */
function rcpar_cp_process_fieldset(&$element) {
  // Collapsible fieldsets
  if (!empty($element['#collapsible'])) {
    $element['#attached']['library'][] = array('system', 'drupal.collapse');
    $element['#attributes']['class'][] = 'collapsible';
    if (!empty($element['#collapsed'])) {
      $element['#attributes']['class'][] = 'collapsed';
    }
  }

  return $element;
}

/**
 * For a given question and quiz, returns ALL session data (ipq_saved_session_data) that has been generated for that
 * question.
 *
 * @param $qnid
 * - Node id of the question
 * @param $quiz_id
 * - Id of the cp_quiz entity
 * @return array
 * - Returns an array of stdclass objects. Each object contains all of the fields from the ipq_saved_session_data table.
 */
function rcpar_cp_session_data_for_question_in_quiz($qnid, $quiz_id) {
  return db_query("SELECT i.ipq_saved_sessions_id, i.updated_on, i.is_correct FROM {ipq_saved_session_data} i, {rcpar_cp_ipq_map} map 
      WHERE i.ipq_question_id= :qid
      AND map.quiz_id = :quizid
      AND i.ipq_saved_sessions_id = map.ipq_saved_sessions_id
      ", array(':qid' => $qnid, ':quizid' => $quiz_id))->fetchAllAssoc('id');
}

/**
 * Page callback for RCPAR_CP_URL . '/reports/roster/csv/%', it returns a csv file equivalent to /RCPAR_CP_URL/reports page (when grouped by roster)
 * NOTE: rcpar_cp_author_reports_by_roster_csv & rcpar_cp_author_reports_by_roster_csv are very similar
 * @param $gid
 * - Email group id
 * @return string
 * - Returns a cvs file
 */
function rcpar_cp_author_reports_by_roster_csv($gid) {
  $cpUser = new CPUser();
  $my_student_groups = $cpUser->getCreatorsACTEnabledEmailGroups();
  if (!in_array($gid, array_keys($my_student_groups))) {
    drupal_access_denied();
  }

  // We need to get the group name
  // to do that, we load the group object from the db
  $query = db_select('rcpar_partners_emails_group', 'rpeg');
  $query->fields('rpeg');
  $query->condition('id', $gid);
  $group = $query->execute()->fetchObject();

  $csv_filename = t('ACT - Quiz results by roster - @group_name', array('@group_name' => $group->group_name));

  // We want to reuse getResultsByRoster method of RCPARCPResultsForm class
  // so we simulate a form with an already submitted value on email_group_select field
  $form = array();
  $form_state = array('values' => array('email_group_select' => $gid));
  $f = new RCPARCPResultsForm($form, $form_state);

  $table = $f->getResultsByRoster(TRUE);

  $h = $table['#header'];
  $csv_res = $table['#rows'];

  // Run the quiz through a sanitizing function to ensure it's good for a filename.
  $csv_filename_safe = drupal_clean_css_identifier($csv_filename);

  // We serve the page as a CSV content type page
  drupal_add_http_header('Content-type', 'application/csv; charset=ISO-8859-1');
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $csv_filename_safe . '.csv"');

  $fp = fopen('php://output', 'w');
  fputcsv($fp, $h);
  foreach ($csv_res as $line) {
    fputcsv($fp, $line);
  }
  fclose($fp);
  drupal_exit();
}


/**
 * Page callback for RCPAR_CP_URL . '/reports/content/csv/%/%', it returns a csv file equivalent to /RCPAR_CP_URL/reports page (when grouped by content)
 * NOTE: rcpar_cp_author_reports_by_roster_csv & rcpar_cp_author_reports_by_roster_csv are very similar
 * @param $gid
 * - Email group id
 * @param $quiz_id
 * - Quiz id
 * @return string
 * - Returns a cvs file
 */
function rcpar_cp_author_reports_by_content_csv($gid, $quiz_id) {
  $cpUser = new CPUser();
  $my_student_groups = $cpUser->getCreatorsACTEnabledEmailGroups();
  if (!in_array($gid, array_keys($my_student_groups))) {
    drupal_access_denied();
  }

  // We want to reuse getResultsByContent method of RCPARCPResultsForm class
  // so we simulate a form with an already submitted value on email_group_select field
  $form = array();
  $form_state = array(
    'values' => array(
      'email_group_select' => $gid,
      'quiz_select' => $quiz_id,
    ),
  );
  $f = new RCPARCPResultsForm($form, $form_state);

  $table = $f->getResultsByContent(TRUE);

  $h = $table['#header'];
  $csv_res = $table['#rows'];

  // We need to get the group name
  // to do that, we load the group object from the db
  // (This will show a shorter name than that of the form class select options.)
  $query = db_select('rcpar_partners_emails_group', 'rpeg');
  $query->fields('rpeg');
  $query->condition('id', $gid);
  $group = $query->execute()->fetchObject();

  // Get the name of the quiz from the form class
  $quiz_name = $f->getQuizzesForSelectedGroup()[$quiz_id]->title;

  $csv_filename = t('ACT - @quiz_name by content - @group_name',
    array(
      '@quiz_name' => $quiz_name,
      '@group_name' => $group->group_name
    )
  );

  // Since professors can name their quizzes, we'll run the quiz through a sanitizing function.
  $csv_filename_safe = drupal_clean_css_identifier($csv_filename);

  // We serve the page as a CSV content type page
  drupal_add_http_header('Content-type', 'application/csv; charset=ISO-8859-1');
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $csv_filename_safe . '.csv"');

  $fp = fopen('php://output', 'w');
  fputcsv($fp, $h);
  foreach ($csv_res as $line) {
    fputcsv($fp, $line);
  }
  fclose($fp);
  drupal_exit();
}

/**
 * Ajax page callback for RCPAR_CP_URL/quizzes/reset/ajax/%user/%
 * Provides a modal confirmation for the professor to erase all score history
 * for a particular user's specific quiz.
 *
 * @param stdClass $user
 * - Loaded Drupal user object
 * @param int $quiz_id
 * - Entity ID of a quiz
 */
function rcpar_cp_reset_quiz_ajax_cb($user, $quiz_id) {
  $cpUser = new CPUser($user);
  $q = new CPQuiz($quiz_id);

  // needed for to display results in modal
  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();

  $abortLink = l(
    'Never mind, abort!',
    '#',
    array('attributes' => array('class' => array('cpQuizWarnButton', 'abortEdit', 'ctools-close-modal', 'btn')))
  );

  $confirmLink = l(
    'Yes, do it.',
    RCPAR_CP_URL . "/quizzes/reset/confirm/{$user->uid}/$quiz_id",
    array(
      'attributes' => array('class' => array('cpQuizWarnButton', 'doEdit', 'btn', 'btn-success', 'btn-primary')),
      'query'      => array('destination' => RCPAR_CP_URL .'/reports?gid=' . $q->getStudentGroup()),
    )
  );

  // create the html for the modal
  $my_html = strtr('
    <div class="cpQuizEditWarn" id="delete-score-history-modal">
      <div class="modal-header">
        <h2 class="heading">Are you sure you want to reset this student\'s score for this quiz?</h2>
      </div>
      <div class="modal-body">
        <p>This action cannot be undone. Once the quiz is reset for this user, all past score history will be removed,
        and it will be as if the student had never taken it.</p>
        <p>
        <strong>Student:</strong> !student<br>
        <strong>Quiz:</strong> !quizname</p>
        <div class="cpQuizWarnButtons text-center">!abortlink &nbsp; !confirmlink</div>
      </div>
    </div>',
    array(
      '!abortlink'   => $abortLink,
      '!confirmlink' => $confirmLink,
      '!student'     => $cpUser->getDisplayName(),
      '!quizname'    => $q->getTitle()
    )
  );

  // throw the warning into a modal
  ctools_modal_render('', $my_html);
  drupal_exit();
}

/**
 * Page callback for RCPAR_CP_URL/quizzes/reset/confirm/%user/%
 * Given a user and a quiz id, removes all IPQ data for sessions taken for that quiz.
 *
 * @param stdClass $user
 * - Loaded Drupal user object
 * @param int $quiz_id
 * - Entity ID of a quiz
 */
function rcpar_cp_reset_quiz($user, $quiz_id) {
  $cpUser = new CPUser($user);
  $q = new CPQuiz($quiz_id);

  // Check that the current user has edit access to the quiz who's score history we are resetting.
  $profUser = new CPUser();
  if(!$profUser->hasEditAccessForQuiz($q)) {
    drupal_access_denied();
  }

  // Delete all of this user's IPQ session data for this quiz and set a status message
  $cpUser->deleteSessionDataForQuiz($quiz_id);
  drupal_set_message(rcpar_cp_get_icon('message-tick') . t('Reset %student\'s score for %quiz', array(
      '%student' => $cpUser->getDisplayName(),
      '%quiz'    => $q->getTitle()
    ))
  );

  // Redirect to our previously set destination.
  drupal_goto();
}