<?php

/**
 * Page callback for /RCPAR_CP_URL/RCPAR_CP_PUB_URL/content/%sku
 * Shows all of the quizzes for a given question pool that the consumer(aka student) take
 */
function rcpar_cp_palconsumer_quizzes_page(ActSampleProduct $asp) {
  // Ensure the user has access to this product
  $users_act_skus = rcpar_cp_user_get_sample_entitlement_skus();
  if (array_search($asp->getSku(), $users_act_skus) === FALSE) {
    drupal_access_denied();
  }

  // Add CSS
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");
  drupal_add_js("$path_to_module/js/rcpar_cp_palconsumer.js");

  // Initialize renderable output
  $render = array();

  // Initialize render structure for quiz area
  $render['quizzes'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('quizzes')),
    'content' => array(),
  );

  $cpUser = new CPUser($GLOBALS['user']);

  // Query for all quizzes associated with this product
  $quizzes = $asp->getQuizzes();
  if (!empty($quizzes)) {

    // Build a table of status icons and quiz edit links
    $vars = array();
    $rows = array();
    foreach ($quizzes as $quiz_ref) {
      $quiz_id = $quiz_ref->id;
      $q = new CPQuiz($quiz_id);
      $q->setACTSampleProduct($asp);

      // Reset text vars
      $action = $icon = $status_label = $title = $date = '';

      // Determine the quiz status for the student.
      try {
        // Quiz taking URL
        $setupUrl = $q->getUrlQuizSetup();

        // If the student has any session data for this quiz, show as 'taken'
        $quiz_data = $cpUser->getSessionDataForQuiz($quiz_id);
        if (!empty($quiz_data)) {
          $icon = 'status-launched';
          $status_label = 'Completed';

          // Show date as when the student updated the last question in the quiz session data
          $last_answer = end($quiz_data);
          $date = new DateTime('', $q->getTimezone());
          $date->setTimestamp($last_answer->updated_on);

          // Results are always available, provide links
          $title = l($q->getTitle(), $setupUrl, array('attributes' => array('title' => $q->getTitle())));
          $action = l('Retake', $setupUrl, array('attributes' => array('class' => 'student-quiz-link', 'title' => 'Retake ' . $q->getTitle())));
        }
        // The quiz hasn't been taken - show as 'not taken'
        else {
          $icon = 'status-scheduled';
          $status_label = 'Not taken';
          $date = '';
          $title = l($q->getTitle(), $setupUrl, array('attributes' => array('title' => $q->getTitle())));
          $action = l('Take Quiz', $setupUrl, array('attributes' => array('class' => 'student-quiz-link', 'title' => $q->getTitle())));
        }
      }
      catch (EntityMetadataWrapperException $e) {
        // Unknown data problem, continue to next row.
        continue;
      }

      $rows[] = array(
        'title' => $title,
        'icon' => $icon,
        'status_label' => $status_label,
        'date' => $date,
        'action' => $action,
        'quiz_id' => $quiz_id,
      );
    }

    foreach ($rows as $row) {
      /** Build row output **/
      // NOTE: Since not all user's will have their timezone correctly set,
      // we'll always display dates in the timezone set at the time of creating the quiz
      $text = '<div class="quiz-title">' . $row['title'] . '</div>';
      $text .= '<div class="quiz-status"><span class="' . drupal_html_class($row['status_label']) . '">' . $row['status_label'] . ' </span> ';

      // If a date is present, it indicates the time that the quiz was last taken
      if (!empty($row['date'])) {
        $text .= $row['date']->format('n/j/Y g:i a T');
      }
      $vars['rows'][] = array(
        rcpar_cp_get_icon($row['icon']),
        $text,
        $row['action']
      );
    }
    // add a table summary for screen readers
    $vars['attributes']['summary'] = t('Your quizzes');

    // The results need to use pagination.
    $per_page = 10; // 10 rows per page
    // Initialize the pager.
    $current_page = pager_default_initialize(count($vars['rows']), $per_page);
    // Split rows into page sized chunks.
    $chunks = array_chunk($vars['rows'], $per_page, TRUE);

    // Render table and pager.
    $render['quizzes']['content']['#markup'] = theme('table', array('header' => '', 'rows' => $chunks[$current_page])) . theme('pager', array('quantity',count($vars['rows'])));

  }
  else {
    // No quizzes exist
    $render['quizzes']['#attributes']['class'][] = 'empty';
    $render['quizzes']['content']['#markup'] = '<div class="no-results">No quizzes have been created yet.</div>';
  }

  return $render;
}

/**
 * Page callback for /RCPAR_CP_URL/RCPAR_CP_PUB_URL/content/%sku/history
 *
 * Shows all of the quizzes for a given ACT Sample Materials product that the
 * consumer(aka student) has taken
 *
 * @param \ActSampleProduct $asp
 *  - ActSampleProduct object
 *
 * @return array
 * - Renderable array of the student's quizzes page or history page
 */
function rcpar_cp_palconsumer_quizzes_history_page(ActSampleProduct $asp) {
  // Ensure the user has access to this product
  $users_act_skus = rcpar_cp_user_get_sample_entitlement_skus();
  if (array_search($asp->getSku(), $users_act_skus) === FALSE) {
    drupal_access_denied();
  }

  // Add CSS
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");
  drupal_add_js("$path_to_module/js/rcpar_cp_palconsumer.js");

  // Initialize renderable output
  $render = array();

  // Initialize render structure for quiz area
  $render['quizzes'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('quizzes')),
    'content' => array(),
  );

  $cpUser = new CPUser($GLOBALS['user']);

  // Query for all quizzes associated with this product
  $quizzes = $asp->getQuizzes();

  if (!empty($quizzes)) {

    // Add a class to our container for css differentiation.
    $render['quizzes']['#attributes']['class'][] = 'pal-score-histories';

    // For score histories, we'll use an ipc_common function to gather all
    // histories of quizzes. These will possibly include non-ACT quizzes so
    // we'll have to filter the results to be shown below.
    module_load_include("inc", "ipq_common", "includes/ipq_common_history");
    // NOTE: Its query is altered by rcpar_cp_query_ipq_history_get_data_alter().
    // This alter will remove the arbitrary section that we pass in.
    $all_ipq_histories = ipq_history_get_data($GLOBALS['user']->uid, 'AUD');

    // Will hold quiz_ids keyed by session_ids of pal_quizzes, used to filter
    // $all_quiz_histories to only pal_quizzes & relate them to the quiz.
    $pal_quiz_sessions = array();

    // This will be used for Score History pages to store the main props needed
    // for each history row.
    $pal_quiz_props = array();

    // This will be used for Score History pages (applies to PAL quizzes only)
    // for counting how many times each quiz has been taken. (keyed on quiz_id)
    $quiz_counts = array();

    // Used to constuct PAL history rows.
    $pal_history_row_data = array();

    // Variable for renaming history quiz sessions.
    $session_label_start = t(' - Session ');

    // Build a table of quiz results & links to retake & review
    $vars = array();
    $rows = array();
    foreach ($quizzes as $quiz_ref) {
      $quiz_id = $quiz_ref->id;
      $q = new CPQuiz($quiz_id);
      $q->setACTSampleProduct($asp);

      // Reset text vars
      $review = $action = $icon = $status_label = $date = '';

      // Determine the quiz status for the student.
      try {
        // Quiz taking URL
        $setupUrl = $q->getUrlQuizSetup();

        // If the student has any session data for this quiz, show as 'taken'
        $quiz_data = $cpUser->getSessionDataForQuiz($quiz_id);
        if (!empty($quiz_data)) {
          $status_label = 'Completed';

          // Show date as when the student updated the last question in the quiz session data
          $last_answer = end($quiz_data);
          $date = new DateTime('', $q->getTimezone());
          $date->setTimestamp($last_answer->updated_on);

          // Provide link for retaking
          $action = l('Retake', $setupUrl, array(
            'query' =>
              array(
                'returnto' => $q->getACTSampleProduct()
                  ->getConsumerHistoryUrl()
              ),
            'attributes' =>
              array('class' => 'student-quiz-link', 'title' => 'Retake ' . $q->getTitle()),
          ));

          // Save the data needed for this quiz for building rows later
          $pal_quiz_props[$quiz_id] = array(
            'title' => $q->getTitle(),
            'status_label' => $status_label,
            'action' => $action,
            'quiz_id' => $quiz_id,
          );

          // Save the quiz_id keyed on unique PAL quiz session_id for filtering $all_ipq_histories and finding its related quiz.
          foreach ($quiz_data as $quiz_datum) {
            $pal_quiz_sessions[$quiz_datum->ipq_saved_sessions_id] = $quiz_id;
          }
        }
      }
      catch (EntityMetadataWrapperException $e) {
        // Unknown data problem, continue to next row.
        continue;
      }
    }
    // This foreach will save data needed for the history rows into $pal_history_row_data.
    foreach ($all_ipq_histories AS $quiz_history) {
      // Only build rows for quiz histories whose sessions are in our stored $pal_quiz_sessions.
      if (array_key_exists($quiz_history['session_id'], $pal_quiz_sessions)) {
        // Start building our row data with the props we saved for this quiz_id.

        // Get this session's quiz_id.
        $my_quiz_id = $pal_quiz_sessions[$quiz_history['session_id']];
        $my_history_row_data = $pal_quiz_props[$my_quiz_id];

        // Add correct % timestamp, & session_id info.
        $my_history_row_data['total_percent'] = $quiz_history['total_percent'];
        $my_history_row_data['timestamp'] = $quiz_history['timestamp'];
        $my_history_row_data['session_id'] = $quiz_history['session_id']; // used for review button

        $pal_history_row_data[] = $my_history_row_data;

        // Keep track of the total sessions for this quiz.
        if (!array_key_exists($my_quiz_id, $quiz_counts)) {
          $quiz_counts[$my_quiz_id] = 0;
        }
        $quiz_counts[$my_quiz_id]++;
      }
    }

    // We need to cycle though again to customize the title with the session name
    // from $quiz_counts. This foreach will handle all of this row's customization.
    foreach ($pal_history_row_data AS $pal_history_row_datum) {
      // Set this session's completed date.
      $date = new DateTime('');
      $date->setTimestamp($pal_history_row_datum['timestamp']);
      $pal_history_row_datum['date'] = $date;

      // Setup its score.
      $pal_history_row_datum['score'] = '
<div class="score-history">
    <div class="clearfix text-left score-text">
        <div>
            ' . $pal_history_row_datum['total_percent'] . '% Correct
        </div>
        <div class="question-score-bar">
            <div style="width:' . $pal_history_row_datum['total_percent'] . '%" class="correct"></div>
        </div>
    </div>
</div>';


      // Add the number of this session to the title.
      $my_quiz_id = $pal_history_row_datum['quiz_id'];
      $pal_history_row_datum['title'] .= $session_label_start . $quiz_counts[$my_quiz_id];

      // Setup its review link.
      $reviewUrl = RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/content/' . arg(3) . '/quiz/' . $pal_history_row_datum['quiz_id'] . '/review/' . $pal_history_row_datum['session_id'];
      $pal_history_row_datum['review'] = l(
        'View Results',
        $reviewUrl,
        array(
          'query' => array(
            'returnto' => $q->getACTSampleProduct()
              ->getConsumerHistoryUrl()
          ),
          'attributes' =>
            array('class' => 'student-quiz-link','title' => 'View Results of ' . $pal_history_row_datum['title'],)
        )
      );
      $rows[] = $pal_history_row_datum;
      // Decrease the $quiz_counts for this quiz to be ready for the next of this quiz' session title.
      $quiz_counts[$my_quiz_id]--;
    }

    foreach ($rows as $row) {
      /** Build row output **/
      // NOTE: Since not all user's will have their timezone correctly set,
      // we'll always display dates in the timezone set at the time of creating the quiz
      $text = '<div class="quiz-title">' . $row['title'] . '</div>';
      $text .= '<div class="quiz-status"><span class="' . drupal_html_class($row['status_label']) . '">' . $row['status_label'] . ' </span> ';

      // If a date is present, it indicates the time that the quiz was last taken
      if (!empty($row['date'])) {
        $text .= $row['date']->format('n/j/Y g:i a T');
      }
      $var_row = array(
        $text,
        $row['score'],
        $row['review'] . $row['action'],
      );
      $vars['rows'][] = $var_row;
    }

    // add a table summary for screen readers
    $vars['attributes']['summary'] = t('Your completed quizzes by session');


    // The results need to use pagination.
    $per_page = 10; // 10 rows per page
    // Initialize the pager.
    $current_page = pager_default_initialize(count($vars['rows']), $per_page);
    // Split rows into page sized chunks.
    $chunks = array_chunk($vars['rows'], $per_page, TRUE);

    // Render table and pager.
    $render['quizzes']['content']['#markup'] = theme('table', array('header' => '', 'rows' => $chunks[$current_page])) . theme('pager', array('quantity',count($vars['rows'])));

    // Score history page: a student has quizzes but hasn't taken any yet.
    if (count($rows) == 0) {
      $render['quizzes']['#attributes']['class'][] = 'empty';
      $render['quizzes']['content']['#markup'] = '<div class="no-results">No quiz results to display yet. Once you\'ve taken a quiz, your results will show up here.</div>';
    }

  }
  return $render;
}

/**
 * Renders the block content for the block 'rcpar_cp_sample_questions'
 *
 * @return array
 * - Renderable array
 */
function rcpar_cp_sample_block_content() {
  // add css file for this block
  drupal_add_css(drupal_get_path('module', 'rcpar_cp') . '/css/my-quizzes-block.css');
  $render = array();

  $render['#attached']['css'] = array(
    drupal_get_path('module', 'rcpar_cp') . '/css/act-sample-questions-block.css',
  );

  // Launch link
  $link_ops = array(
    'attributes' => array(
      'class' => array('launch')
    ),
    'html' => true,
  );
  $render['link'] = array('#markup' => l('Launch Accounting Classroom Trainer <i class="fa fa-chevron-right"></i>', RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL  . '/content/overview', $link_ops));

  // Build a table of sample question links
  $skus = rcpar_cp_user_get_sample_entitlement_skus();
  $vars = array('rows' => array());
  foreach ($skus as $sku) {
    $asp = new ActSampleProduct($sku);

    // Product title (with link) and author name
    $link_and_author = l($asp->getTitle(), $asp->getConsumerUrl());
    $link_and_author .= '<br>' . $asp->getAuthorName();

    // Publisher logo image with link (or blank string if no image is present)
    $img = $asp->getImageStyled('field_act_image', 'act_publisher_logo');
    $logo_image = !empty($img) ? l(render($img), $asp->getConsumerUrl(), array('html' => TRUE)) : '';

    // Textbook image with link (or blank string if no image is present)
    $img = $asp->getImageStyled('field_textbook_image', 'act_textbook_image');
    $textbook_image = !empty($img) ? l(render($img), $asp->getConsumerUrl(), array('html' => TRUE)) : '';

    // Build table row
    if(!empty($logo_image) && empty($textbook_image)) {
      $vars['rows'][] = array(
        array('data' => $logo_image, 'class' => array('logo-column'), 'colspan' => 1),
        array('data' => $link_and_author, 'class' => array('link-author-column'), 'colspan' => 2),
      );
    }
    elseif(empty($logo_image) && !empty($textbook_image)) {
      $vars['rows'][] = array(
        array('data' => $textbook_image, 'class' => array('textbook-image-column'), 'colspan' => 1),
        array('data' => $link_and_author, 'class' => array('link-author-column'), 'colspan' => 2),
      );
    }
    elseif(empty($logo_image) && empty($textbook_image)) {
      $vars['rows'][] = array(
        array('data' => $link_and_author, 'class' => array('link-author-column'), 'colspan' => 3),
      );
    }
    else {
      $vars['rows'][] = array(
        array('data' => $logo_image, 'class' => array('logo-column')),
        array('data' => $textbook_image, 'class' => array('textbook-image-column')),
        array('data' => $link_and_author, 'class' => array('link-author-column')),
      );
    }
  }

  // Render table
  $render['content'] = array('#markup' => theme('table', $vars));

  // This block wouldn't appear if the user had no products, but we'll handle an empty scenario just in case.
  if(empty($vars['rows'])) {
    $render['content'] = array('#markup' => '<div class="no-quizzes">No sample questions available.</div>');
  }

  return $render;
}

/**
 * Implements hook_query_TAG_alter().
 * to alter the query in ipq_history_get_data
 * to be used by rcpar_cp_palconsumer_quizzes_page for Score Histories page callback
 */
function rcpar_cp_query_ipq_history_get_data_alter(QueryAlterableInterface &$query) {
  if (arg(0) == RCPAR_CP_URL && arg(1) == RCPAR_CP_PUB_URL) {

    // remove the condition requiring matching uid: $query->condition('ss.section_id', <whatever, defaults to 1452>, '=');
    // @see https://www.drupal.org/docs/7/api/database-api/dynamic-queries/query-alteration-tagging hook_query_alter() section at bottom
    // @see https://www.drupal.org/node/2095031
    $where =& $query->conditions();
    foreach ($where AS $q_cond_key => $q_cond_val) {
      if ($q_cond_val['field'] == 'ss.section_id' || $q_cond_val['field'] == 'ss.entitlement_id') {
        unset($where[$q_cond_key]);
      }
    }
    // The original query has a pager for 10.
    // The limit has no relevance here since this is being called to get all the
    // user's quizzes, then filtered to only PAL quiz results,
    // Reset the limit to include all.
    $limit =& $query->limit();
    $limit->limit(1000);
  }
}
