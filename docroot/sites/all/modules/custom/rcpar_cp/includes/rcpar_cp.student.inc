<?php

/**
 * Page callback for /RCPAR_CP_URL/student/quizzes
 * Shows all of the quizzes that the consumer(aka student) can act upon
 * (e.g. take quiz, view results)
 */
function rcpar_cp_student_my_quizzes_page() {
  // Add CSS
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");

  // Initialize renderable output
  $render = array();

  // Initialize render structure for quiz area
  $render['quizzes'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('quizzes')),
    'content' => array(),
  );

  $cpUser = new CPUser($GLOBALS['user']);

  // Query for all quizzes by owner
  $quizzes = $cpUser->getConsumableQuizzes();
  if(!empty($quizzes)) {
    // Build a table of status icons and quiz edit links
    $vars = array();
    $rows = array();
    foreach ($quizzes as $quiz_id => $quiz) {
      $q = new CPQuiz($quiz);

      // Reset text vars
      $action = $icon = $status_label = $title = '';

      // Determine the quiz status for the student.
      try {
        // If the quiz has not started yet, we don't show it
        if($q->notYetStarted()) {
          // In development, it can be helpful to see quizzes that have not yet
          // started, so I'm preserving the code snippet below.
          continue;

          $title = $quiz->title;
          $icon = 'status-pending';
          $status_label = 'Not yet available';
          $date = $q->getStartDate(); // Show date as when the quiz opens
          $action = '';
        }
        // The quiz has either started, resume or finished.
        // For the student, it may be:
        // - Completed (has been taken)
        // - Due (has not been taken, but can be)
        // - Closed (has not been taken, no longer can be)
        else {
          // If the student has any session data for this quiz, it's completed.
          $quiz_data = $cpUser->getSessionDataForQuiz($quiz_id);
          if(!empty($quiz_data)) {
            if($cpUser->isQuizResumable($quiz_id)) {
              $title = l($quiz->title, $q->getUrlQuizSetup());
              $icon = 'status-scheduled';
              $status_label = 'Due';
              $date = $q->getCloseDate(); // Show date as when the quiz closes
              $action = l('Resume Quiz', $q->getUrlQuizSetup(), array('attributes' => array('class' => 'student-quiz-link')));
            }
            else {
              $title = $quiz->title;
              $icon = 'status-launched';
              $status_label = 'Completed';
              $action = '';

              // Show date as when the student updated the last question in the quiz session data
              $last_answer = end($quiz_data);
              $date = new DateTime('', $q->getTimezone());
              $date->setTimestamp($last_answer->updated_on);

              // If the results are available, show a 'View results' links
              if($cpUser->quizResultsAvailableToConsumer($quiz_id)) {
                $action = l('View Results', RCPAR_CP_URL . "/quiz/$quiz_id/results", array('attributes' => array('class' => 'student-quiz-link')));
                $title = l($quiz->title, RCPAR_CP_URL . "/quiz/$quiz_id/results");
              }
            }
          }
          // The quiz hasn't been taken - see if it's 'due' or 'closed'
          else {
            // If the quiz is currently open, we'll show the status as being 'Due'
            if($q->isOpen()) {
              $title = l($quiz->title, $q->getUrlQuizSetup());
              $icon = 'status-scheduled';
              $status_label = 'Due';
              $date = $q->getCloseDate(); // Show date as when the quiz closes
              $action = l('Take Quiz', $q->getUrlQuizSetup(), array('attributes' => array('class' => 'student-quiz-link disable-on-click')));
            }
            // Otherwise, it's closed.
            else {
              $title = $quiz->title;
              $icon = 'status-closed';
              $status_label = 'Closed';
              $date = $q->getCloseDate(); // Show date as when the quiz closed
              $action = '';
            }
          }
        }
      }
      catch (EntityMetadataWrapperException $e) {
        // Unknown data problem, continue to next row.
        continue;
      }

      $rows[] = array(
        'title' => $title,
        'icon' => $icon,
        'status_label' => $status_label,
        'date' => $date,
        'action' => $action,
        'quiz_id' => $quiz_id,
      );
    }

    // On this page, we want to show the Due quizzes first and then the Closed and Completed
    // also, Due quizzes and Closed and Completed quizzes "blocks"
    // should be sorted by their date in ascending order (note that the date can be the closing date
    // or the date when the user answered his last question on the quiz)

    // here we create the two "blocks"
    $due = array();
    $closed_or_completed = array();
    foreach ($rows as $row) {
      if ($row['status_label'] == 'Completed' || $row['status_label'] == 'Closed') {
        $closed_or_completed[] = $row;
      }
      else {
        $due[] = $row;
      }
    }
    // sort the "blocks"
    rcpar_cp_sort_quizzes($due, 'asc');
    rcpar_cp_sort_quizzes($closed_or_completed, 'desc');
    // merge the arrays
    $rows = array_merge($due, $closed_or_completed);

    foreach($rows as $row){
      /** Build row output **/
      // NOTE: Since not all user's will have their timezone correctly set,
      // we'll always display dates in the timezone set at the time of creating the quiz
      $text = '<div class="quiz-title">'. $row['title'] . '</div>';
      $text .= '<div class="quiz-status"><span>' . $row['status_label'] . ' </span> ' . $row['date']->format('n/j/Y g:i a T');

      // For development, it can be helpful to see the start and end date, so preserving this
      // $text .= '<div class="quiz-date">Start '. $q->getStartDate()->format('n/j/Y g:i a T');
      // $text .= '<div class="quiz-date">End '. $q->getCloseDate()->format('n/j/Y g:i a T');

      $vars['rows'][] = array(rcpar_cp_get_icon($row['icon']), $text, $row['action']);
    }


    // The results need to use pagination.
    $per_page = 10; // 10 rows per page
    // Initialize the pager.
    $current_page = pager_default_initialize(count($vars['rows']), $per_page);
    // Split rows into page sized chunks.
    $chunks = array_chunk($vars['rows'], $per_page, TRUE);

    // Render table and pager.
    $render['quizzes']['content']['#markup'] = theme('table', array('rows' => $chunks[$current_page])) . theme('pager', array('quantity',count($vars['rows'])));

  }
  else {
    // No quizzes exist
    $render['quizzes']['#attributes']['class'][] = 'empty';
    $render['quizzes']['content']['#markup'] = '<div class="no-results">You don\'t have any assigned quizzes at the moment.</div>';
  }

  return $render;
}

/**
 * Renders the block content for the block 'rcpar_cp_student_my_quizzes'
 * This blocks displays a list of quizzes that are open and have not yet been taken by the current user.
 *
 * @return array
 * - Renderable array
 */
function rcpar_cp_student_my_quizzes_block_content() {
  // add css file for this block
  drupal_add_css(drupal_get_path('module', 'rcpar_cp') . '/css/my-quizzes-block.css');
  $render = array();

  // Launch link
  $link_ops = array(
    'attributes' => array(
      'class' => array('launch')
    ),
    'html' => true,
  );
  $render['link'] = array('#markup' => l('Launch Accounting Classroom Trainer <i class="fa fa-chevron-right"></i>', RCPAR_CP_URL . '/student/quizzes', $link_ops));

  // Query for all quizzes by owner
  $cpUser = new CPUser($GLOBALS['user']);
  $quizzes = $cpUser->getConsumableQuizzes();
  $vars = array('rows' => array());
  if(!empty($quizzes)) {
    // Build a table of status icons and quiz edit links
    foreach ($quizzes as $quiz_id => $quiz) {
      $q = new CPQuiz($quiz);

      if(!$q->isClosed() && $cpUser->isQuizResumable($quiz_id)) {
         $title = l($quiz->title, $q->getUrlQuizSetup());
         $icon = 'status-scheduled';
         $status_label = 'Due';
         $date = $q->getCloseDate(); // Show date as when the quiz closes
         $action = l('Resume Quiz', $q->getUrlQuizSetup(), array('attributes' => array('class' => 'student-quiz-link btn btn-success')));

         /** Build row output **/
         // NOTE: Since not all user's will have their timezone correctly set,
         // we'll always display dates in the timezone set at the time of creating the quiz
         $text = '<div class="quiz-title">'. $title . '</div>';
         $text .= '<div class="quiz-status"><span>' . $status_label . ' </span> ' . $date->format('n/j/Y g:i a T');
         $vars['rows'][] = array(rcpar_cp_get_icon($icon), $text, $action);
      }
      // We only show open quizzes that the user hasn't taken
      elseif($q->isOpen() && !$cpUser->quizHasBeenFinished($quiz_id)) {
        // This code stanza is basically stolen from
        $title = l($quiz->title, $q->getUrlQuizSetup());
        $icon = 'status-scheduled';
        $status_label = 'Due';
        $date = $q->getCloseDate(); // Show date as when the quiz closes
        $action = l('Take Quiz', $q->getUrlQuizSetup(), array('attributes' => array('class' => 'student-quiz-link disable-on-click btn btn-success')));

        /** Build row output **/
        // NOTE: Since not all user's will have their timezone correctly set,
        // we'll always display dates in the timezone set at the time of creating the quiz
        $text = '<div class="quiz-title">'. $title . '</div>';
        $text .= '<div class="quiz-status"><span>' . $status_label . ' </span> ' . $date->format('n/j/Y g:i a T');
        $vars['rows'][] = array(rcpar_cp_get_icon($icon), $text, $action);
      }
    }


    // The results need to use pagination.
    $per_page = 10; // 10 rows per page
    // Initialize the pager.
    $current_page = pager_default_initialize(count($vars['rows']), $per_page);
    // Split rows into page sized chunks.
    $chunks = array_chunk($vars['rows'], $per_page, TRUE);

    // Render table and pager.
    $render['content']['#markup'] = theme('table', array('header' => '', 'rows' => $chunks[$current_page])) . theme('pager', array('quantity',count($vars['rows'])));

  }

  // There may be consumable quizzes that have closed or are already taken, so we need to look at the table row count
  // to determine whether to show the empty message
  if(empty($vars['rows'])) {
    $render['content'] = array('#markup' => '<div class="no-quizzes">You don\'t have any assigned quizzes at the moment.</div>');
  }

  return $render;
}

/**
 * Page callback for RCPAR_CP_URL/quiz/%/setup and RCPAR_CP_URL/RCPAR_CP_PUB_URL/content/quiz/%/setup
 * Loads quiz questions into an IPQ session and redirects to /ipq/quiz/take
 * @param $quiz_id
 * - Numeric ID of a cp_question_group entity
 * @param string|null $take_type
 * - May be NULL or set to 'preview' when the quiz is being previewed by a professor
 */
function rcpar_cp_quiz_setup($quiz_id, ActSampleProduct $asp = NULL, $take_type = NULL) {
  $question_node_types = array();
  $question_node_ids = array();

  $q = new CPQuiz($quiz_id);
  if(!empty($asp)) {
    $q->setACTSampleProduct($asp);
  }
  $cpUser = new CPUser();

  /** Validations for taking the quiz: **/
  // If the user is previewing a quiz, we will only disallow it if they are not the author
  if($take_type == 'preview') {
    if($GLOBALS['user']->uid != $q->getAuthorUid()) {
      drupal_set_message("You don't have access to this quiz.", 'error');
      drupal_goto(RCPAR_CP_URL . '/quizzes');
    }
     // User has access to preview:
    else {
      // Let's force the session to be recreated to not bring all data when professor does previews.
      $cpUser->deleteSessionDataForQuiz($quiz_id);  
    }
  }
  // ASL quiz
  elseif ($q->getQuizType() == 'act_asl') {
    // Already taken
    if($cpUser->quizHasBeenFinished($quiz_id)) {
      drupal_set_message('You have already taken this quiz.', 'error');
      drupal_goto(RCPAR_CP_URL . '/student/quizzes');
    }

    // Not open
    if(!$q->isOpen()) {
      drupal_set_message('This quiz is not available for you to take.', 'error');
      drupal_goto(RCPAR_CP_URL . '/student/quizzes');
    }

    // User is not the target audience for this quiz (quiz email group does not match the user's groups)
    if(!in_array($q->getStudentGroup(), $cpUser->getEmailGroupIds())) {
      drupal_set_message("You don't have access to this quiz.", 'error');
      drupal_goto(RCPAR_CP_URL . '/student/quizzes');
    }
  }
  // PAL quiz
  else {
    // Access is already checked for PAL quizzes by the menu system. As long as the
    // user has access to the product, they can take the quiz.
  }

  // Load the quiz entity - @todo: leverage CPQuiz class, which did not exist when this code was written
  $quiz = entity_load_single('cp_question_group', $quiz_id);
  $w = entity_metadata_wrapper('cp_question_group', $quiz);

  // Iterate through each question reference. @todo - This should leverage CPQuiz class
  try {
    foreach ($w->field_cp_question as $nref) {
      // Increment the counter for this question type
      $question_node_types[$nref->getBundle()] += 1;
      $question_node_ids[] = $nref->getIdentifier();
    }

    // No questions in the quiz: This is an edge case that probably will never be seen by the end user.
    if(empty($question_node_ids)) {
      drupal_set_message('There are no questions included in this quiz, it\'s not ready to take yet. Please contact an administrator if this error is unexpected.', 'error');
      if($take_type == 'preview') {
        drupal_goto(RCPAR_CP_URL . '/quizzes');
      }
      else {
        drupal_goto(RCPAR_CP_URL . '/student/quizzes');
      }
    }
  }
  catch (EntityMetadataWrapperException $e) {
    // Unlikely error scenario - something weird has happened with the node reference data.
    drupal_set_message('Sorry, we encountered an error while trying to set up this quiz. Please contact an administrator.', 'error');
    drupal_goto(RCPAR_CP_URL . '/student/quizzes');
  }

  // Get question types IDs from ipq_question_types table in order to formulate a quizlet name.
  $query_params = array(':question_node_types' => array_keys($question_node_types));
  $ipq_question_types_ids = db_query("SELECT id FROM {ipq_question_types} WHERE type IN (:question_node_types)", $query_params)->fetchCol();
  $quizlet_name = 'quizlet-group-' . implode('-', $ipq_question_types_ids);

  $question_list = array($quizlet_name => $question_node_ids);
  // now we need to get the tooltips and titles info
  $question_tooltips = array();
  $question_titles = array();
  foreach($question_list as $index => $ql){
    $tooltips = _ipq_common_get_tooltip_info($ql);
    $titles = _ipq_common_get_question_titles($ql);
    $question_tooltips[$index] = $tooltips;
    $question_titles[$index] = $titles;
  }

  // check if timer is enabled for quiz and get timer settings
  // todo - This should be a method on CPQuiz
  $custom_timer = FALSE;
  $timer_hours = '';
  $timer_minutes = '';
  try {
    if ($w->field_cp_quiz_timer_active->value()==1) {
      $custom_timer = TRUE;
      // if hours or minutes are set to zero, the countdowntimer.js script will throw an error
      // so we'll only populate the variables if they are greater than zero
      if ($w->field_cp_quiz_timer_length_hours->value() > '0') {
        $timer_hours = $w->field_cp_quiz_timer_length_hours->value();
      }
      if ($w->field_cp_quiz_timer_length_mins->value() > '0') {
        $timer_minutes = $w->field_cp_quiz_timer_length_mins->value();
      }
    }
  }
  catch (EntityMetadataWrapperException $e) {
      // todo - better handle exception
  }

  // If the user has never taken this quiz before...
  //or the user is an pal user
  if(!$cpUser->sessionExistsForQuiz($quiz_id) || $q->getQuizType() == 'act_pal') {

    // Build IPQ session data
    $session_data = array(
      'is_new'           => TRUE,
      'uid'              => $GLOBALS['user']->uid,
      'name'             => $quiz->title . ' - ' . date('M j, Y - g:iA'),
      'session_id'       => $GLOBALS['user']->sid,
      'session_type'     => 'quiz',
      'average_time'     => '0',
      'section_id'       => 0, // Section term id
      'percent_complete' => '0',
      'percent_correct'  => '0',
      'created_on'       => $_SERVER['REQUEST_TIME'],
      'updated_on'       => $_SERVER['REQUEST_TIME'],
      'session_config'   => array(
        'quiz_id'               => $quiz_id,
        'exam_version'          => exam_version_get_default_version(),
        'only_no_seen'          => FALSE,
        'only_incorrect'        => FALSE,
        'only_skipped'          => FALSE,
        'question_types'        => $question_node_types,
        'chapters'              => array(), //todo ?
        'hide_timer'            => FALSE,
        'custom_timer'          => $custom_timer,
        'timer_hours'           => $timer_hours,
        'timer_minutes'         => $timer_minutes,
        'score_as_you_go'       => FALSE,
        'view_solutions'        => FALSE,
        'explain_answers'       => FALSE,
        'question_list'         => $question_list,
        'current_question_type' => $quizlet_name,
        'current_question_id'   => $question_node_ids[0],
        'question_tooltips'     => $question_tooltips,
        'question_titles'       => $question_titles,
        'preview'               => $take_type == 'preview', // Indicates this session is just the user previewing the quiz.
      ),
      // We mark the quiz as 'finished' if it has a timer because the user cannot resume it once they have started it
      'finished'         => $q->isTimerActive() ? 1 : 0,
    );

    // Save the quiz config into the database
    module_load_include('inc', 'ipq_common', 'includes/ipq_session');
    $ipq_session = ipq_save_session($session_data);

    // Save a link of this quiz id to the session created
    $record = array('quiz_id' => $quiz_id, 'ipq_saved_sessions_id' => $ipq_session['id']);
    drupal_write_record('rcpar_cp_ipq_map', $record);

    // Put the quiz's IPQ data into the session and redirect to the quiz take page
    ipq_quiz_build_session($ipq_session['id']);
  }
  // User has a session for this quiz already: resume.
  else {
    module_load_include('inc', 'ipq_common', 'includes/ipq_session');
    $session_data = end($cpUser->getLatestSessionDataForQuiz($quiz_id));
    ipq_quiz_build_session($session_data->ipq_saved_sessions_id);
  }

  // If this is a previewed quiz, we want to store the session ID in a queue for deletion later.
  // We don't want these dummy results to pollute our IPQ data.
  if($take_type == 'preview') {
    /** @var DrupalQueueInterface $queue */
    $queue = DrupalQueue::get(RCPAR_CP_PREVIEW_QUEUE);
    $queue->createItem(array(
        'ipq_saved_sessions_id' => $ipq_session['id'],
        'quiz_id'               => $quiz_id,
        'uid'                   => $GLOBALS['user']->uid,
        'timestamp'             => mktime(),
      )
    );

    // Preview in IPQ
    if (!empty($_GET['ipq'])) {
      drupal_goto('ipq/quiz/take');
    }
  }

  drupal_goto($q->getUrlQuizTake());
}

/**
 * Page callback for RCPAR_CP_URL/quiz/take
 * Takes the quiz that is currently loaded in the session
 * @param $quiz_id
 */
function rcpar_cp_quiz_take() {
  // Add our module css
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");

  drupal_add_js($path_to_module . '/js/rcpar_cp_quizz_session.js');

  // Quiz ID is available here: $_SESSION['ipq_session']['session_config']['quiz_id'], but we are going to instantiate
  // the CPQuiz option with the session so we can access session related methods on the object
  $q = new CPQuiz($_SESSION['ipq_session']['id'], TRUE);

  // if the quiz requires a timer
  if ($_SESSION['ipq_session']['session_config']['custom_timer']) {
    // load countdown timer js
    drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/countdowntimer.js', array('group' => JS_DEFAULT, 'type' => 'file'));
    drupal_add_js($path_to_module . '/js/rcpar_cp_timer.js', array('group' => JS_DEFAULT, 'type' => 'file', 'weight' => '100'));
    // we need to add ipq_sessiontimer settings
    drupal_add_js(array('ipq_sessiontimer' => 'session_average_per_person', '0'), 'setting');
    // override the ipq countdown time settings
    drupal_add_js(array('examCountdownTime' => rcpar_cp_getCountdownTime()), 'setting');
    // pass the URL constant to rcpar_cp_timer.js. If previewing a quiz, we will go back to the professor's quiz listing,
    // otherwise we go to the student's.
    $timeout_url = $q->getQuizFinishedUrl();
    drupal_add_js(array('rcpar_cp_timer' => $timeout_url, 'redirect_URL'), 'setting');
  }
  else {
    // Timer CSS
    drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/timer.css');
    // otherwise load the elapsed timer js
    drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/elapsedtimer.js', array('group' => JS_DEFAULT, 'type' => 'file'));
  }



  // IPQ overrides
  drupal_add_css("$path_to_module/css/ipq_overrides.css");

  module_load_include('inc', 'ipq_common', 'includes/ipq.quiz.display');
  $form = ipq_common_quiz_display('take');
  
  // add ipq css and js by calling this function, 
  // this code was moved below the form load due to PROD-205.
  rcpar_cp_add_ipq_css_js();
  
  // Show the quiz name instead of course section title
  $form['navigation_info']['#value']['section_title'] = $q->getTitle();
  return $form;
}


function rcpar_cp_getCountdownTime() {

  // create an array
  $timeobj = array();
  $timeobj['hours'] = $_SESSION['ipq_session']['session_config']['timer_hours'];
  $timeobj['minutes'] = $_SESSION['ipq_session']['session_config']['timer_minutes'];
  $timeobj['seconds'] = '0';

  return $timeobj;
}



/**
 * Page callback for RCPAR_CP_URL/quiz/results
 */
function rcpar_cp_quiz_results() {
  // Add CSS & JS
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");

  rcpar_cp_add_ipq_css_js();

  // IPQ overrides
  drupal_add_css("$path_to_module/css/ipq_overrides.css");

  module_load_include('inc', 'ipq_common', 'includes/ipq.quiz.results');
  return ipq_quiz_results();
}

/**
 * Page callback for /quiz/#/results
 * & RCPAR_CP_URL/RCPAR_CP_PUB_URL/content/quiz/%/review/% (PAL quiz Score Histories View Results button)
 * shows quiz results for a specific quiz
 * @param integer $quiz_id
 *  - id of the cp_question_group_cp_quiz entity
 * @param null|integer $session_id
 *  - not null indicates we're reviewing a PAL quiz session
 *
 * @return string
 *  - the rendering of the first page of the review
 */
function rcpar_cp_specific_quiz_results($quiz_id, $session_id = NULL) {
  $cpUser = new CPUser();

  // Session ID is supplied when reviewing PAL Score Histories. This will return

  // Used later to make an adjustment to the URL to return to Score Histories page when needed.
  $is_history = (!empty($session_id));

  if (empty($session_id)) { // Not reviewing from PAL Score history pages.
    $session_id = $cpUser->getMostRecentSessionId($quiz_id);
  }

  $q = new CPQuiz($session_id, TRUE);

  // Sanity check for existence of results
  if(empty($session_id)) {
    drupal_set_message('This quiz has not been taken yet, so results cannot be displayed.');
    drupal_goto(RCPAR_CP_URL . '/student/quizzes');
  }

  // ASL users: verify that the user has access to the results.
  if ($q->getQuizType() == 'act_asl') {
    if(!$cpUser->quizResultsAvailableToConsumer($quiz_id)) {
      drupal_set_message('You don\'t have access to view the results for this quiz yet. Please check with your instructor if you feel there is an error.');
      drupal_goto(RCPAR_CP_URL . '/student/quizzes');
    }

    // the review button needs to be removed if the quiz results type is set to
    // "Just the overall percentage score".
    $results_type = $q->getResultsType();

    // Since this button is set in ipq_quiz_results.tpl.php there's no easy way to
    // edit it out other then using js. rcpar_cp_review.js will read this setting &
    // remove the button based on it's setting
    // todo: menu access for RCPAR_CP_URL/quiz/results need to check $_SESSION['ipq_session'] & block student access if quiz has 'summary' results type
    drupal_add_js(array(
      'rcparCp' => array(
        'showResults' => ($results_type == 'detailed'),
        'rcparCpUrl' => url($q->getQuizFinishedUrl()),
      ),
    ), 'setting');
  }
  // PAL quiz
  else {
    // Get an adjustment to the url for returning to history if needed.
    $history_url_adjust = '';
    if ($is_history) {
      $history_url_adjust = '/history';
    }
    drupal_add_js(array(
      'rcparCp' => array(
        'showResults' => 1, // Results always available
        'rcparCpUrl' => url(RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/content/' .
          $q->getACTSampleProduct()->getSku() . $history_url_adjust),
      ),
      // Done URL
    ), 'setting');
  }

  // load the quiz session into $_SESSION['ipq_session']
  module_load_include('inc', 'ipq_common', 'includes/ipq_session');
  ipq_quiz_build_session($session_id);

  // The initial quiz results screen was getting "Quizzes - Overview" as the page title
  // Not sure why but need to ensure the page title is correct
  drupal_set_title("Quiz Results");

  // now that the session is loaded, we can get the results of this quiz
  $review_render = rcpar_cp_quiz_results();
  drupal_add_js(drupal_get_path('module', 'rcpar_cp') . '/js/rcpar_cp_review.js');
  return $review_render;
}

/**
 * Page callback for /RCPAR_CP_URL/review
 * This is the IPQ equivalent of ipq_user_session_review()
 * @see ipq_user_session_review()
 */
function rcpar_cp_user_session_review() {
  // Add CSS & JS
  $path_to_module = drupal_get_path('module', 'rcpar_cp');
  drupal_add_css("$path_to_module/css/cp.css");
  rcpar_cp_add_ipq_css_js();
  // IPQ overrides
  drupal_add_css("$path_to_module/css/ipq_overrides.css");

  // @todo - do we need to do anything else fancy to get the ipq session?
  $ipq_session = &$_SESSION['ipq_session'];

  module_load_include('inc', 'ipq_common', 'includes/ipq_common_review');
  $data = ipq_user_session_review_get_form_data($ipq_session);
  $form = drupal_get_form('ipq_user_session_review_form', $data);
  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter()
 * Add a submit handler to ipq_user_session_review_form to help us override redirects.
 */
function rcpar_cp_form_ipq_user_session_review_form_alter(&$form, &$form_state) {
  if (arg(0) == RCPAR_CP_URL) {
    $form['#submit'][] = 'rcpar_cp_user_session_review_form_submit';
  }
}

/**
 * On exiting a CP quiz review session, redirect to the user to RCPAR_CP_URL/student/quizzes
 */
function rcpar_cp_user_session_review_form_submit($form, &$form_state) {
  if ($form_state['triggering_element']['#name'] == 'exit_review') {
    // ipq_user_session_review_form_submit handles pagination by forcing the last
    //  question clickto submit the form. Check if the submit was sent by a question click
    if (isset($form_state['values']['force_question']) && $form_state['values']['force_question'] != '') {
      // it was sent by a question click. The next bit of code it the same approach
      // that ipq_user_session_review_form_submit uses to handle question clicks
      $session = $form_state['build_info']['args'][0]['session'];

      if (isset($session['is_temporary'])) {
        $ipq_session = &$_SESSION['ipq_preview_temp_session'];
      }
      else {
        $ipq_session = &$_SESSION['ipq_session'];
      }
      $next_question = $form_state['values']['force_question'];
      update_session_current_question($ipq_session, $next_question);
    }
    else { // it wasn't a question click so redirect to the consumer's home page
      // Get quiz from the session
      // Quiz ID is available here: $_SESSION['ipq_session']['session_config']['quiz_id'], but we are going to instantiate
      // the CPQuiz option with the session so we can access session related methods on the object
      $q = new CPQuiz($_SESSION['ipq_session']['id'], TRUE);

      // Sanity check
      if(!$q->isLoaded()) {
        $form_state['redirect'] = 'dashboard';
      }

      // ASL quiz:
      if($q->getQuizType() == 'act_asl') {
        $form_state['redirect'] = $q->getQuizFinishedUrl();
      }
      // PAL quiz
      else {
        // Get an adjustment to the url for returning to history if needed.
        $history_url_adjust = '';
        if (drupal_match_path(current_path(), '*/history/quiz/review')) {
          $history_url_adjust = '/history';
        }
        $form_state['redirect'] = RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/content/' .
          $q->getACTSampleProduct()->getSku() . $history_url_adjust;
      }
    }
  }
}

/**
 * Adds CSS and Javascript required to take an IPQ quiz.
 * This code is largely ripped off from ipq_common_preprocess_page() which cannot be leveraged since it relies on
 * URL paths following the ipq pattern. A refactor to avoid duplication appears cumbersome, but possible.
 */
function rcpar_cp_add_ipq_css_js() {
  // Global ipq css/js (formerly in ipq_preprocess_page)()
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/ipq.css');
  drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/ipq-global.js');


  // Load js/css for all quiz take and exam take pages
  drupal_add_js(drupal_get_path('module','ipq_common') . '/js/ipq-session-display.js');
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/jquery-ui.css', array('group' => CSS_DEFAULT, 'type' => 'file'));
  drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/jquery-ui.min.js', array('group' => JS_DEFAULT, 'type' => 'file'));

  //add calculator css
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/calculator.css', array('group' => CSS_DEFAULT, 'type' => 'file'));

  // Cookie jquery plugin -- TODO: I think this is not in use anymore, this is probably safe to delete - Dominique
  drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/js.cookie.js', array('group' => JS_DEFAULT, 'type' => 'file'));

  // Timer CSS
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/timer.css');

  // Spell-checker
  drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/spellchecker.js');
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/spellchecker.css');

  // Browser/platform detection
  drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/bowser/bowser.js');

  if(isset($_SESSION['ipq_session']['id'])) {
    drupal_add_js(array('ipqSessionId' => $_SESSION['ipq_session']['id']), 'setting');
  }
  ipq_common_load_handsontable_js_css();


  // Load css/js for all review pages
  $match_paths = array(
    RCPAR_CP_URL . '/*quiz/review',
  );
  if (drupal_match_path(current_path(), implode("\n", $match_paths))) {
    // Load handsontable js and css, but only if not on a deleted question.
    if (isset($_SESSION['ipq_session']['session_config']['current_question_id']) && node_load($_SESSION['ipq_session']['session_config']['current_question_id'])) {
      ipq_common_load_handsontable_js_css();
    }

    // For draggable View Solution modals
    drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/jquery-ui.css', array('group' => CSS_DEFAULT, 'type' => 'file'));
    drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/jquery-ui.min.js', array('group' => JS_DEFAULT, 'type' => 'file'));

    // For pager navigation
    drupal_add_js(drupal_get_path('module', 'ipq_common') . '/js/ipq-session-review.js', array('group' => JS_DEFAULT, 'type' => 'file'));
  }
}
