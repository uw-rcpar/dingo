<?php

/**
 * Gets the question nids for asl-act quizzes
 * @return array of question-nid arrays keyed by lower-case section (course) names
 */
function rcpar_cp_act_asl_questions($keep_types = FALSE) {
  // these reflect the questions as defined in excel ss 'CP-182 - ACT Question Pool.xlsx', downloaded from CP-182 2/21/2017
  // LESS questions discovered as in the wrong section or not having 2017 chapter topics
  $aud_mcq = array(
    3023173,
    3024333,
    3023386,
    3023180,
    3025299,
    3023185,
    3025232,
    3023183,
    3024742,
    3023136,
    3022742,
    3023654,
    3025003,
    3023219,
    3024813,
    3023176,
    3023712,
    3024466,
    3023900,
    3025637,
    3023761,
    3025235,
    3023256,
    3024845,
    3024879,
    3023239,
    3023227,
    3023225,
    3023407,
    3023211,
    3023217,
    3023232,
    3023347,
    3023065,
    3023308,
    3022627,
    3023286,
    3023296,
    3023265,
    3023278,
    3023251,
    3023280,
    3023272,
    3023302,
    3023277,
    3023304,
    3023070,
    3022571,
    3024920,
    3023259,
    3025234,
    3020764,
    3025378,
    3023505,
    3023437,
    3023309,
    3023306,
    3023500,
    3023775,
    3024520,
    3023305,
    3023288,
    3023275,
    3023950,
    3023270,
    3023974,
    3024052,
    3020757,
    3024622,
    3023914,
    3022747,
    3025101,
    3023145,
    3022718,
    3024615,
    3022802,
    3024997,
    3022702,
    3020767,
    3023630,
    3022632,
    3025023,
    3023222,
    3020829,
    3023209,
    3025321,
    3024970,
    3023249,
    3024794,
    3020794,
    3023238,
    3023254,
    3024359,
    3023307,
    3023268,
    3023261,
    3023610,
    3023374,
    3023292,
    3023257,
    3023267,
    3023263,
    3023293,
    3023295,
    3023313,
    3023371,
    3023464,
    3023363,
    3023321,
    3023368,
    3023284,
    3023301,
    4296846,
    3025267,
    3024525,
    3023294,
    3023285,
    3023367,
    3023291,
    3024215,
    3023287,
    3023484,
    3023316,
    3023361,
    3024439,
    3024647,
    3023477,
    3023303,
    3023373,
    3024557,
    3023300,
    3023310,
    3023289,
    3023697,
    3024855,
    3023290,
    3023494,
    3023573,
    3023314,
    3023283,
    3023385,
    3023382,
    3024369,
    3024777,
    3025074,
    3024422,
    3020731,
    3023760,
    3023451,
    3023481,
    3023471,
    3023446,
    3023487,
    3023455,
    3023479,
    3023468,
    3023444,
    3023547,
  );

  $aud_tbs = array(
    3008628,
    3031897,
    3441171,
    3027111,
    3071467,
    4055931,
    3002502,
    3442856,
    3004455,
    3028726,
    3222796,
    3085270,
    3085270,
    3442856,
    3152611,
    3166356,
    3222796,
    3010418,
    3002161,
    3354666,
  );

  $far_mcq = array(
    3023425,
    3023689,
    3023467,
    3023376,
    3025123,
    3023404,
    3023845,
    3025313,
    3023311,
    3023401,
    3022574,
    3022636,
    4296996,
    3022721,
    3025239,
    3023383,
    3023409,
    3023419,
    3023492,
    3023448,
    3023399,
    3023459,
    3025091,
    3022787,
    3023377,
    3023426,
    3022745,
    3025004,
    3023489,
    3023447,
    3024014,
    3022223,
    3023457,
    3023454,
    3023402,
    3023410,
    3023414,
    3023319,
    3023422,
    3023418,
    3023159,
    3023161,
    3023495,
    3023221,
    3023203,
    3023507,
    3023522,
    3023224,
    3023521,
    3023515,
    3023517,
    3023491,
    3023506,
    3023475,
    3023502,
    3023485,
    3023470,
    3023450,
    3023472,
    3023503,
    3023165,
    3023197,
    3022276,
    3023527,
    3023533,
    3023510,
    3023218,
    3023512,
    3023528,
    3023585,
    3023244,
    3023452,
    3024725,
    3023508,
    3023511,
    3022180,
    3023516,
    3023532,
    3025100,
    3025310,
    3023335,
    3023346,
    3023530,
    3022647,
    3023212,
    3023458,
    3023519,
    3023524,
    3023523,
    3023474,
    3023240,
    3023525,
    3023531,
    3023264,
    3023514,
    3023202,
    3023526,
    3023513,
    3023260,
    3023544,
    3023332,
    3023332,
    3023262,
    3023262,
    3023258,
    3023271,
    3023333,
    3023537,
    3023250,
    3023548,
    3023555,
    3023326,
    3023534,
    3023563,
    3023246,
    3023327,
    3023266,
    3023556,
    3023252,
    3023553,
    3023536,
    3023559,
    3023535,
    3023248,
    3023328,
    3023550,
    3023539,
    3023255,
    3023255,
    3025560,
    3023324,
    3023330,
    3023561,
    3023540,
    3023551,
    3023562,
    3023538,
    3023325,
    3023322,
    3025061,
    3023242,
    3023343,
    3023351,
    3023625,
    3023273,
    3023279,
    3023567,
    3023320,
    3020840,
    3023337,
    3023338,
    3023713,
    3023565,
    3023356,
    3023359,
    3023357,
    3020851,
    3025279,
    3023564,
    3023340,
    3023348,
    3023334,
    3023358,
    3023353,
    3023317,
    3023331,
    3023339,
    3023344,
    3023354,
    3022659,
    3024328,
    3020830,
    3020754,
    3023913,
    3024876,
    3024291,
    3024780,
    3020808,
    3023825,
    3022657,
    3022693,
    3024760,
    3020776,
    3025066,
    3023755,
    3022930,
    3022390,
    3024554,
    3020779,
    3023614,
    3022741,
    3022770,
    3024461,
    3022803,
    3020701,
    3023546,
    3022785,
    3020724,
    3024452,
    3024522,
    3022728,
    3025114,
    3020828,
    3022826,
    3024455,
    3022661,
    3022662,
    3022698,
    3024091,
    3020726,
    4300981,
    4301021,
  );

  $far_tbs = array(
    3210516,
    3076118,
    3077269,
    3254546,
    3262891,
    3267616,
    3267736,
    3271476,
    3272226,
    3286471,
    3301051,
    3172546,
    3318331,
    3348666,
    3081657,
    3333281,
    3318241,
    3335696,
    3347671,
    3017797,
    3079367,
    3339386,
    3081675,
    3015616,
  );

  $reg_mcq = array(
    3025821,
    3025824,
    3023095,
    3024053,
    3023355,
    3025029,
    3020760,
    3023119,
    3023089,
    3023685,
    3024037,
    3023103,
    3024670,
    3024159,
    3025827,
    3025822,
    3025823,
    3020655,
    3025826,
    3025825,
    3025828,
    3021889,
    3025290,
    3024733,
    3023387,
    3023013,
    3023750,
    3025092,
    4301291,
    3024930,
    3023106,
    3022980,
    3023107,
    3022337,
    3025379,
    3023590,
    3025085,
    3024247,
    3024523,
    3023126,
    3024586,
    3023102,
    3022956,
    3024112,
    3023012,
    3025116,
    3023975,
    3023975,
    3025278,
    3024503,
    3023093,
    3022949,
    3023111,
    3023109,
    3024163,
    3024999,
    3023473,
    3022997,
    3023117,
    3022969,
    3023071,
    3023113,
    3023034,
    3023115,
    3023132,
    3025105,
    3024617,
    3023110,
    3023099,
    3023101,
    3024367,
    3024772,
    3022923,
    3023751,
    3025072,
    3024239,
    3024906,
    3020702,
    3022899,
    3025077,
    3024680,
    3022961,
    3025133,
    3025018,
    3023733,
    3025063,
    3024293,
    3024748,
    3020756,
    3023698,
    3024708,
    3023908,
    3025308,
    3023919,
    3024886,
    3023397,
    3024790,
    3023091,
    3025470,
    3023994,
  );

  $reg_tbs = array(
    3373051,
    3178386,
    3053643,
    3036096,
    3092971,
    3032438,
    3036343,
    3053593,
    3188651,
  );

  $bec_mcq = array(
    3023778,
    3020676,
    3020674,
    3020672,
    3022599,
    3022464,
    3022373,
    3025205,
    3023582,
    3021717,
    3022809,
    3024962,
    3023118,
    3022082,
    3022292,
    3021058,
    3024805,
    3023349,
    3020681,
    3888346,
    3020677,
    3022284,
    3022015,
    3020656,
    3025853,
    3022159,
    3022823,
    3023036,
    3022045,
    3022118,
    3022360,
    3022215,
    3022136,
    3022151,
    3022158,
    3022147,
    3022699,
    3021721,
    3025732,
    3021938,
    3025284,
    3021708,
    3022226,
    3884126,
    3022182,
    3025831,
    3024304,
    3022534,
    3021067,
    3022196,
  );

  $bec_tbs = array(
    4036721,
  );

  $q_arr = array();
  if (!$keep_types) {
    $q_arr['aud'] = array_merge($aud_mcq, $aud_tbs);
    $q_arr['bec'] = array_merge($bec_mcq, $bec_tbs);
    $q_arr['far'] = array_merge($far_mcq, $far_tbs);
    $q_arr['reg'] = array_merge($reg_mcq, $reg_tbs);
  }
  else {
    foreach (array('_mcq', '_tbs') AS $q_type) {
      foreach (array('aud', 'bec', 'far', 'reg') AS $sec) {
        $my_label = $sec . $q_type;
        $q_arr[$my_label] = $$my_label;
      }
    }
  }
  return $q_arr;
}

/**
 * utility function used to proof questions from the spreadsheet in CP-182
 *
 * @param $pool_name the name of the generated q-pool ie aud_mcq, aud_tbs etc...
 * @return arrays telling which questions have the wrong section (AUD, BEC, etc...) and which are not found in 2017 chapter topics
 *
 * NOTE: the above arrays for generating q_pools has been fixed, all erronius questions removed.
 *
 * TODO: remove this function once we no longer use generated q pools
 */
function rcpar_cp_topic_pool_lu($pool_name) {
  $all_qs = rcpar_cp_act_asl_questions(TRUE);
  $my_qs = $all_qs[$pool_name];
  $q_vals = rcpar_cp_get_qvals_query_result($my_qs);

  $sections = array(
    1452 => 'AUD',
    1455 => 'BEC',
    1453 => 'FAR',
    1454 => 'REG',
  );

  $my_section = array_search(strtoupper(substr($pool_name, 0, 3)), $sections);

  $wrong_section = $chapter_topics = array();
  foreach ($q_vals AS $nid => $q_val) {
//    if ($nid == 3024204 || $nid == 3024730){
//      $break = TRUE;
//    }
    if ($q_val['section'] != $my_section) {
      $wrong_section[$sections[$q_val['section']]][] = $nid;
    }
    else {
      if (!array_key_exists($q_val['chapter'], $chapter_topics)) {
        $chapter_topics[$q_val['chapter']] = array();
      }
      if (!array_key_exists($q_val['topic'], $chapter_topics[$q_val['chapter']])) {
        $chapter_topics[$q_val['chapter']][$q_val['topic']] = array();
      }
      $chapter_topics[$q_val['chapter']][$q_val['topic']][] = $nid;
    }
  }
  $all_generated_chapter_topics = array();
  $default_version = exam_version_get_default_version();
  foreach ($chapter_topics AS $chap => $topics) {
    $my_chapter_topics = ipq_common_get_topics_by_chapter_nid($chap, $default_version);
    $my_generated_chapter_topics = array();
    foreach ($my_chapter_topics AS $my_chapter_topic) {
      $my_generated_chapter_topics[] = $my_chapter_topic->nid;
    }
    $all_generated_chapter_topics[$chap] = $my_generated_chapter_topics;
  }
  $not_found_chapter_topics = array();
  foreach ($chapter_topics AS $chap => $topics) {
    $not_found_topics = array_values(array_diff(array_keys($topics), $all_generated_chapter_topics[$chap]));
    if (!empty($not_found_topics)) {
      foreach ($not_found_topics AS $not_found_topic) {
        if (!array_key_exists($chap, $not_found_chapter_topics)) {
          $not_found_chapter_topics[$chap] = array();
        }
        $not_found_chapter_topics[$chap][$not_found_topic] = $chapter_topics[$chap][$not_found_topic];
      }
    }
  }
  return array(
    'wrong_section' => $wrong_section,
    'not_found' => $not_found_chapter_topics,
  );
}