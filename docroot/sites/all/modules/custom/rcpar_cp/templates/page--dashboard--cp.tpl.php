<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>

<button tabindex="0" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#course-activation" id="execute-modal-activation-after-ajax" style="display:none">
</button>
<div id="activation-container-of-values"></div>

<!-- **************************** -->
<!-- NAVIGATION / HEADER -->
<!-- **************************** -->
<?php if (!empty($page['hello_bar'])): ?>
  <div class="hello-bar container">
    <div class="row">
      <div class="col-sm-12">
        <?php print render($page['hello_bar']); ?>
      </div>
    </div>
  </div><!-- / .hello-bar -->
<?php endif; ?>
<header id="navbar" role="banner" class="<?php //print $navbar_classes; ?> no-gutter">
  <div class="container">
    <div class="row upper-header no-gutter">
      <div class="col-md-4 first">
        <?php if ($logo): ?>
          <a class="logo navbar-btn pull-left hidden-xs" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Roger CPA Review - Click to go to home page'); ?>"/>
          </a>
          <a class="logo navbar-btn pull-left hidden-sm hidden-md hidden-lg phone-logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
            <img src="/sites/default/files/phone-logo.png" alt="<?php print t('Roger CPA Review - Click to go to home page'); ?>"/>
          </a>
        <?php endif; ?>

        <?php if (!empty($site_name)): ?>
          <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
        <?php endif; ?>
      </div><!-- /.col-md-4 .first -->
      <div class="col-md-8 second">
        <button type="button" class="navbar-toggle-2" id="showRightPush">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <!-- **************************** -->
        <!-- HAMBURGER MENU -->
        <!-- **************************** -->
        <nav id="cbp-spmenu-s2" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
          <div id="dashboard-close-x" class="dashboard-close-button"><span class="reader-instructions">Close Dashboard menu</span></div>
          <?php if (!empty($page['lower_navigation'])): ?>
            <div class="navigation-nav">
              <?php print render($page['lower_navigation']); ?>
            </div>
          <?php endif; ?>
          <?php if (!empty($page['header'])): ?>
            <?php print render($page['header']); ?>
          <?php endif; ?>
        </nav>
        <div class="overlay-container"></div>
        <!-- **************************** -->
        <!-- END HAMBURGER MENU -->
        <!-- **************************** -->

        <?php if (!empty($page['navigation'])): ?>
          <div class="navigation-nav">
            <?php print render($page['navigation']); ?>
          </div>
        <?php endif; ?>
      </div><!-- /.col-md-8 .second -->
    </div><!-- /.row .upper-header .no-gutter -->
  </div><!-- /.container -->
</header><!-- /#navbar -->
<!-- **************************** -->
<!-- END NAVIGATION / HEADER -->
<!-- **************************** -->

<!-- **************************** -->
<!-- MAIN CONTAINER  -->
<!-- **************************** -->
<div class="main-container container" role="main">

  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>
  </header> <!-- /#page-header -->

  <div class="row">
    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <div class="row">
        <div class="col-sm-8">
          <?php
          /****** welcome block ******/
          print rcpar_mods_render_block('block',99);
          ?>
          <div class="dashboard-welcome">
            <?php
            /****** welcome copy block ******/
            print rcpar_mods_render_block('block','act_dashboard_welcome_copy');
            ?>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="dashboard-video">
            <?php
            /****** video block ******/
            print rcpar_mods_render_block('block','act_dashboard_video_modal');
            ?></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <?php
          /****** courses block ******/
          // Show the courses block if the user has access to any of the four online course parts
          if (sizeof(array_intersect(array('AUD', 'FAR', 'BEC', 'REG'), array_keys($user->user_entitlements['products']))) > 0) {
            print rcpar_mods_render_block('rcpar_dashboard','rcpar_dashboard_exam_rev_content');
          }

          // Show the 'my quizzes' block if the user has ACT ASL access.
          if (rcpar_cp_access_consumer()) {
            /****** quizzes block ******/
            print rcpar_mods_render_block('rcpar_cp','rcpar_cp_student_my_quizzes');
          }
          ?>
          <?php
          // Show the sample questions block if the user has a PAL entitlement
          if (rcpar_cp_access_sample_consumer()) {
            /****** sample questions block ******/
            print rcpar_mods_render_block('rcpar_cp','rcpar_cp_sample_questions');
          }
          ?>
        </div>
      </div>
      <div class="row linked-img-row">
        <div class="col-sm-4">
          <?php
          /****** pass rate block ******/
          print rcpar_mods_render_block('block',97);
          ?>
          <?php
          /****** products and courses block ******/
          print rcpar_mods_render_block('block',128);
          ?>
        </div>
        <div class="col-sm-4">
          <?php
          /****** blog block ******/
          print rcpar_mods_render_block('block',98);
          ?>
          <?php
          /****** study planners block ******/
          print rcpar_mods_render_block('block',127);
          ?>
        </div>
        <div class="col-sm-4">
          <?php
          /****** learning center block ******/
          print rcpar_mods_render_block('block',96);
          ?>
        </div>
      </div>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>
<?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
  <!-- Footer -->
  <div id="footer">

    <!-- 960 Container -->
    <div class="container">
      <div class="row">
        <span role="navigation">
          <!-- 1/4 Columns -->
          <div class="col-md-3 col-sm-6 footer-first">
            <?php print render($page['footer_firstcolumn']); ?>
          </div>

          <!--  1/4 Columns -->
          <div class="col-md-3 col-sm-6 footer-second">
            <?php print render($page['footer_secondcolumn']); ?>
          </div>

          <!-- 1/4 Columns -->
          <div class="col-md-3 col-sm-6 footer-third">
            <?php print render($page['footer_thirdcolumn']); ?>
            <div class="clearfix"></div>
          </div>
        </span>
        <!-- 1/4 Columns -->
        <div class="col-md-3 col-sm-6 footer-fourth">
          <?php print render($page['footer_fourthcolumn']); ?>
        </div>
        <div class="clear-empty"></div>
      </div><!-- End Row -->
    </div><!-- End 960 Container -->
  </div>
<?php endif; ?>

<footer class="footer" role="contentinfo">
  <div class="container">
    <?php print render($page['footer']); ?>
    <div class="clear-empty"></div>
  </div>
</footer>