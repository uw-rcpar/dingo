<?php

global $user;
extract($variables);
$group_info_value = end($group_data);
$rcpar_dashboard_entitlements_options = rcpar_dashboard_entitlements_options();
ksort($rcpar_dashboard_entitlements_options);
?>
<div role="tabpanel" class="active in tab-pane fade tab-content-<?php print $partner_nid ?>" id="tab-main">
  <div class="panel panel-default">
    <div class="panel-body">
      <!--Header Group Info-->
      <div class="header-group-info group-material">
        <label>CPA Exam Material</label>
        <?php
        $partner_sections = rcpar_dashboard_monitoring_get_partner_access_material($partner_nid);
        if (isset($partner_sections['fieldset_content'])) {
          ?>

          <div class="section-list">
            <?php
            $all_ps_rows = array();
            $all_ps = '';
            $ps_rows_string = '';
            $ps_rows_divider = '';
            $c = 0;
            $total = count($partner_sections['fieldset_content']);
            foreach ($partner_sections['fieldset_content'] as $ps_sku => $ps_rows) {
              $c++;
              if ($ps_sku != '') {
                $ps_rows_string = "<strong> $ps_sku: </strong>" . implode(', ', $ps_rows);
              }
              else {
                $ps_rows_string = implode(', ', $ps_rows);
              }

              $c != $total ? $ps_rows_divider = ' | ' : $ps_rows_divider = '. ';
              $all_ps_rows[] = $ps_rows_string . $ps_rows_divider;
            }
            $all_ps = implode('', $all_ps_rows);
            ?>
            <div class="sections-wrapper">
              <div class="partial-sections"><?php echo mb_strimwidth($all_ps, 0, 205, ''); ?></div>
              <div class="all-sections"><?php print $all_ps; ?></div>
            </div>

            <button id="partner-section-show-more" class="partner-section-show-hide show-more btn btn-default">Show more</button>
            <button id="partner-section-show-less" class="partner-section-show-hide show-less btn btn-default">Show less</button>

          </div>


        <?php }
        else { ?>

          <div class="section-list">
            <div class="sections-wrapper">
              <?php
              $c = 0;
              foreach ($partner_sections as $ps_sku => $ps_rows) {
                print "<b> $ps_sku: </b>" . implode(', ', $ps_rows);
                print $c == count($partner_sections) ? ' | ' : '';
                $c++;
              }
              ?>
            </div>
          </div>
        <?php } ?>
      </div>

      <div class="header-group-info group-dates col-sm-12">
        <div class="header-group-info group-start-date col-sm-3">
          <label>Access Start</label> <?php print $group_info_value['start_date'] ?>
        </div>

        <div class="header-group-info group-end-date col-sm-3">
          <label>Access End</label> <?php print $group_info_value['end_date'] ?>
        </div>
        <div style="clear: both;"></div>
      </div><!--Ends Header Group Info-->
      <!--Email List Group Info-->
      <div class="group-email-list col-sm-12">
        <div>
          <div class="row">
            <div class="col-sm-12">
              <div class="roster-heading col-sm-4">Roster</div>
              <?php foreach ($rcpar_dashboard_entitlements_options as $sku) { ?>
                <div class="section-heading section-heading-<?php print $sku ?> col-sm-2"> <?php print $sku ?></div>
              <?php } ?>
            </div>
          </div>
          <?php
          $i = 0;
          $email_count = count($students_info);

          foreach ($students_info as $email => $student_info) {
            $i++;
            $ipq_classes = array('modal-load', 'link-disabled');
            $section = t("no entitlemts");
            $sections = array();
            $PID = isset($student_info['pid']) ? $student_info['pid'] : 0;
            if (isset($student_info['user_obj'])) {
              $student_user = $student_info['user_obj'];
              $cp_u = new CPUser($student_user);
              $student_display_name = $cp_u->getDisplayName();
            }
            else {
              $student_user = null;
              $student_display_name = $email;
            }
            if ($student_info['uid'] && $group_id) {
              $list = $student_info['entitlements_by_partner'][$PID];
              if (count($list) > 0) {
                $ipq_classes = array('modal-load');
                $sections = array_flip($list);
                $section = array_pop($list);
              }
            }
            ?>
            <div class="groups-wrapper row<?php if ($email_count == $i) { ?> last-row<?php } ?>">
              <div class="group-wrapper" id="group-wrapper-<?php print $i; ?>-cp">
                <div class="monitoring-center-groups row">
                  <div class="group-row col-sm-12">
                      <div class="group-participant-email col-sm-4<?php if (!$student_user) : ?> unregistered-user<?php endif ?>">
                        <?php print $student_display_name; ?>
                        <?php if (!$student_user) : ?>
                        <div class="student-not-registered">Unregistered user</div>
                      <?php endif ?>
                    </div>
                    <?php
                    foreach ($rcpar_dashboard_entitlements_options as $sku) {
                      $percentage = '-';
                      if (isset($student_info['progress'][$sku]) && isset($sections[$sku])) {
                        $percentage = $student_info['progress'][$sku] . '%';
                      }
                      ?>
                      <div class="group-row-data group-row-data-<?php print $sku; ?> col-sm-2">
                        <?php
                        if ($section != "no entitlemts") {
                          print l($percentage, "ajax/monitoring/center/tabs/{$PID}/" . $student_info['uid'] . '/' . $sku, array('attributes' => array('class' => 'modal-load ipq-lecture-link')));
                        }
                        else {
                          print '-';
                        }
                        ?>
                      </div>
                    <?php } ?>
                    <div style="clear: both;"></div>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
      <!--End Email List Group Info-->
    </div> <!-- panel body -->
  </div> <!-- panel-default body -->
  <div id="monitoring-tabs" class="tabs-wrapper" role="tabpanel">
    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">X</span></button>
          </div>
          <div class="modal-body">
            <p></p>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>

</div>
