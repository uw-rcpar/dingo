<?php
$modal_id = 'cp-quiz-preview-info-modal';
$aria_label = 'Close Quiz Preview Information Popup';
$header_h3_id = 'quizPreviewInfoLabel';

// To make the content of the popup CMS editible via /admin/structure/block/manage/block/216/configure
// or by going to /admin/structure/block/manage/block and use configure for block called "Homework Help Center instructions"
// Bid of block called "Homework Help Center instructions"
//$bid = 216;
$delta = 'cp_quiz_preview_info_modal';
$render_array_key = 'boxes_' . $delta;
$block = block_load('boxes', $delta);
$block_render_array = _block_render_blocks([$block]);
$header_h3_title = $block_render_array[$render_array_key]->title;
$modal_body = $block_render_array[$render_array_key]->content['#markup'];
$button_txt = 'Ok, got it!';
$modal_footer = '<button type="button" data-dismiss="modal" class="btn btn-success pull-right">' . $button_txt . '</button>';

// Add the don't show again checkbox
$modal_footer .= '
<div class="dont_show_checkbox">
  <label><input type="checkbox" value="">Don\'t show this again</label>
</div>
';

$m = new RCPARModal($modal_id, ['rcpar-mod-typical-modal', $modal_id], [
  'data-backdrop'   => 'true',
  'role'            => 'dialog',
  'aria-labelledby' => $header_h3_id,
  'tabindex'        => '-1',
]);

// Create the modal as usual.
$m->setHeaderContent('<button type="button" class="close" data-dismiss="modal"  aria-label="' . $aria_label . '">x</button><h3 id="' . $header_h3_id . '" class="modal-title">' . $header_h3_title . '</h3>')
  ->setBodyContent($modal_body)
  ->setFooterContent($modal_footer);
print $m->render();
?>