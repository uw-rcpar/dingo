<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['lower_navigation']: Items for the lower navigation region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<div class="overlay-container"></div>

<?php
$is_pal_creator = (arg(1) == RCPAR_CP_PUB_URL && arg(2) == 'creator'); // used for adapting template for publisher quiz creator list

// Identify when the user is viewing as a PAL consumer
$is_pal_consumer = (arg(1) == RCPAR_CP_PUB_URL && arg(2) == 'content');

// Identify any PAL context
$is_pal = (arg(1) == RCPAR_CP_PUB_URL);


// Logo URL
// ACT PAL
if($is_pal_creator) {
  $logo_url = url(RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/creator/overview');
}
elseif($is_pal_consumer) {
  $logo_url = url(RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/content/overview');
}
// ACT ASL
else {
  // Professor (note: it is possible for this user to also have student access)
  $is_asl_creator = rcpar_cp_access_creator();

  if($is_asl_creator) {
    $logo_url = url(RCPAR_CP_URL . '/quizzes');
  }
  // Student
  else {
    $logo_url = url(RCPAR_CP_URL . '/student/quizzes');
  }
}

?>
<header id="navbar" role="banner" class="<?php print $navbar_classes; ?> no-gutter header-element">
  <div class="row logo-row cp-nav">
    <div class="cp-nav" role="navigation">
      <div class="cp-logos">
        <div class="cp-logo"><a href="<?php print url('dashboard'); ?>" tabindex="1" role="button" id="cp-logo"><img src="/<?php print drupal_get_path('module', 'rcpar_cp'); ?>/css/img/cp-roger-logo.svg" height="28" alt="Roger CPA Review" /><span class="reader-instructions">Student Dashboard</span></a></div>
        <div class="cp-logo"><a href="<?php print $logo_url; ?>"><img src="/<?php print drupal_get_path('module', 'rcpar_cp'); ?>/css/img/act-logo.svg"  height="28" alt="Accounting Classroom Trainer"/></a>
        </div>
        <?php
        // ACT Sample Materials publisher logo
        if (rcpar_cp_is_act_sample_page()) {
          /** @var ActSampleProduct $asp */
          $asp = menu_get_object('rcpar_cp_act_product', 3);
          $image = $asp->getImageRendered();
          if (!empty($image)) {
            // add alt text to the image array
            $image['0']['#item']['alt'] = $asp->getAuthorName();
            print '<div class="publisher-logo"><div class="divider"></div> <div class="in-partnership">In partnership with</div> ' . render($image) . '<div class="clear-empty"></div></div>';
          }
        }
        ?>
        <button tabindex="0" id="showRightPush" class="navbar-toggle-2" type="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <div id="close-x" class="close-button hidden-xs hidden-sm hidden-md hidden-lg"></div>
        </button>

        <!-- THIS IS THE SIDE MENU -->
        <div id="cbp-spmenu-s2" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
          <div id="dashboard-close-x" class="dashboard-close-button" role="button"></div>
          <?php if (!empty($page['lower_navigation'])): ?>
            <div class="navigation-nav">
              <?php print render($page['lower_navigation']); ?>
            </div><!-- /navigation-nav -->
          <?php endif; ?>
          <?php if (!empty($page['header'])): ?>
            <?php print render($page['header']); ?>
          <?php endif; ?>
          <!-- END OF SIDE MENU -->
        </div><!-- /nav -->
        <ul class="menu cp-menu" role="menubar">
          <?php
          if($is_pal) {
            // publisher quiz list for creators
            if ($is_pal_creator) {
              print('<li class="active">' . l('Admin Quizzes', RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/creator/overview') . '</li>');
            }

            // ACT Sample Materials
            if ($is_pal_consumer) {
              if (rcpar_cp_is_act_sample_page()) {
                /** @var ActSampleProduct $asp */
                $asp = menu_get_object('rcpar_cp_act_product', 3);
                // This holds whether we are accessing from the Score History page.
                $is_history = arg(4) == 'history';
                ?>
                <li class="overview<?php print !$is_history ? ' active' : ''; ?>" role="menuitem"><?php print l('Overview', $asp->getConsumerUrl()); ?></li>
                <li class="score-history<?php print $is_history ? ' active' : ''; ?>" role="menuitem"><?php print l('Score History', $asp->getConsumerUrl() . '/history') ?></li>
                <?php
                // Build a drop-down list of the user's ACT Sample products
                $options = array();
                foreach (rcpar_cp_user_get_sample_entitlement_skus() as $sku) {
                  $a = new ActSampleProduct($sku);
                  // The select url will depend on whether we're accessing from the history page or main page/
                  $my_url = (($is_history) ? $a->getConsumerHistoryUrl() : $a->getConsumerUrl());
                  $options[$my_url] = $a->getTitle();
                }
                $variables = array(
                  'element' => array(
                    '#options'    => $options,
                    '#attributes' => array(
                      'name'  => 'act-sample-products-select',
                      'title' => 'Select a Product',
                      'id'    => 'products-select',
                    )
                  )
                );
                print '<li class="act-dropdown"><form><label for="products-select">Select a product</label>';
                print '<div class="reader-instructions">Screen reader users: The next element on the page provides a listing of options relating to your textbook-based access. If multiple options are available, choosing an option other than the one that is currently active will reload this page with quizzes pertinent to the relevant textbook.</div>';
                print theme('select', $variables);
                print '</form></li>';
              }
              else {
                // @todo
                // Currently there doesn't seem to be a case where the user is not viewing a sample page but
                // still needs to see the navigation - but this will need to be changed when the user is
                // taking a quiz or viewing quiz results but still needs to see the ACT consumer navigation
                // context.
              }
            }
          }
          // ASL version of ACT:
          else {
            // Student/consumer version of the menu:
            if (rcpar_cp_access_consumer()) { ?>
              <li class="<?php print arg(2) == 'quizzes' ? 'active' : ''; ?>" role="menuitem">
                <?php
                // Edge case - user is student AND instructor, differentiate between the two 'Quizzes' links
                if ($is_asl_creator) {
                  print(l('Assigned Quizzes', RCPAR_CP_URL . '/student/quizzes'));
                }
                // Default - student just has student access
                else {
                  print(l('Quizzes', RCPAR_CP_URL . '/student/quizzes'));
                }
                ?>
              </li>
            <?php }
            // Instructor version:
            if ($is_asl_creator) { ?>
              <li class="<?php print arg(1) == 'quizzes' | arg(1) == 'quiz' ? 'active' : ''; ?>" role="menuitem"><?php print(l('Quizzes', RCPAR_CP_URL . '/quizzes')); ?></li>
              <li class="<?php print arg(1) == 'reports' ? 'active' : ''; ?>" role="menuitem"><?php print(l('Reports', RCPAR_CP_URL . '/reports')); ?></li>
              <li class="<?php print arg(1) == 'groups' ? 'active' : ''; ?>" role="menuitem"><?php print(l('Groups', RCPAR_CP_URL . '/groups')); ?></li>
              <li class="" role="menuitem"><?php print(l('ACT Help Center', 'customer-care/act', array('attributes' => array('target' => '_blank')))); ?></li>
            <?php }
          } ?>
        </ul>
      </div>


    </div>
  </div>
</header>

<div class="main-container container cp-container" role="main">


  <?php print $messages; ?>

  <div class="row">

    <?php if (!empty($page['sidebar_first'])) { ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php } ?>

    <?php
    if (is_array($tabs) && !empty($tabs)) {
      render($tabs);
    }
    elseif (is_string($tabs)) {
      print $tabs;
    } ?>

    <?php
    // create section class if sidebars exist
    if (!empty($page['sidebar_first'])) {
      $section_class = 'with-sidebar-first col-sm-8';
    }
    if (!empty($page['sidebar_second'])) {
      $section_class = 'with-sidebar-second col-sm-8';
    }
    if (!empty($page['sidebar_first']) && !empty($page['sidebar_second'])) {
      $section_class = 'with-both-sidebars col-sm-4';
    }
    ?>

    <section class="<?php print $section_class;?>" id="first-section">
      <?php if (!empty($page['highlighted'])) { ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php } ?>

      <a id="main-content"></a>
      <?php
      if (!empty($page['help'])) {
        print render($page['help']);
      } ?>

      <?php if (!empty($action_links)) { ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php } ?>

      <?php print render($page['content']); ?>

    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class=">col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>

<!-- Modal for question preview iframe -->
<div id="cp-preview-question" class="modal" data-remote="false">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <h3 id="termsLabel" class="modal-title">Preview Question</h3>
        <button type="button" data-dismiss="modal" class="btn btn-success pull-right">Close</button>
      </div><!-- /.modal-header -->

      <div class="modal-body">
        <iframe id="cp-preview-question-iframe">Loading...</iframe>
      </div>

    </div><!-- /.modal-content -->
  </div>
</div><!-- /.modal -->