<?php
/**
 * @file
 * act_sample_materials.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function act_sample_materials_commerce_product_default_types() {
  $items = array(
    'act_sample_materials' => array(
      'type' => 'act_sample_materials',
      'name' => 'ACT Sample Materials',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_image_default_styles().
 */
function act_sample_materials_image_default_styles() {
  $styles = array();

  // Exported image style: act_publisher_logo.
  $styles['act_publisher_logo'] = array(
    'label' => 'ACT publisher logo',
    'effects' => array(
      23 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 96,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
      26 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 30,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: act_textbook_image.
  $styles['act_textbook_image'] = array(
    'label' => 'ACT textbook image',
    'effects' => array(
      24 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 80,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
      25 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 62,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
