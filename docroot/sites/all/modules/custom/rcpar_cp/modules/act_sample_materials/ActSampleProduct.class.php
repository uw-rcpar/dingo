<?php

class ActSampleProduct extends ActProduct {
  public function getAuthorName() {
    return $this->getFieldValue('field_act_author_name');
  }

  public function getImage() {
    return $this->getFieldValue('field_act_image');
  }

  /**
   * Returns a renderable array that will produce an image.
   * If there is no image, the array will be empty.
   * @return array
   */
  public function getImageRendered() {
    return field_view_field('commerce_product', $this->product, 'field_act_image', 'default');
  }

  /**
   * Get a commerce product image with image style applied
   * @param $field_name
   * Field name should be the machine name of the image field, ie, 'field_act_image'
   * @param $style_name
   * Style name should be the machine name of the image style, ie, 'act_publisher_logo'
   * @return array
   * Returns a renderable array containing a commerce product image with image style applied.
   * When there is no image in the field, the array will be empty.
   */
  public function getImageStyled($field_name, $style_name) {
    $image = field_get_items('commerce_product', $this->product, $field_name);

    // If there is no image, just return a blank string
    if(empty($image)) {
      return array();
    }
    // add alt text to image, including publisher's and textbook name
    if ($this->isLoaded) {
      $textbook_name = $this->getTitle();
      $publisher_name = $this->getAuthorName();
    }
    if ($field_name == 'field_act_image') {
      $image[0]['alt'] = $publisher_name . " logo";
    }
    if ($field_name == 'field_textbook_image') {
      $image[0]['alt'] = $textbook_name . " textbook";
    }
    // Generate renderable array for the image style
    $output = field_view_value('commerce_product', $this->product, $field_name, $image[0], array(
      'type' => 'image',
      'settings' => array(
        'image_style' => $style_name,
      ),
    ));
    return $output;
  }

  /**
   * Get quizzed referenced by the sample product
   * @return array of quiz entities, keyed on the weight they should be shown in this product's list of quizzes
   */
  public function getQuizzes() {
    return $this->getFieldValue('field_quizzes_ref');
  }

  /**
   * Adds a quiz to the end of a Sample Product quiz reference field (if it's not already referenced)
   * @param $quiz_id Quiz Entity id
   * @return bool|void
   */
  public function setQuiz($quiz_id){
    $my_quizzes = $this->getQuizzes();
    foreach ($my_quizzes AS $my_quiz){
      if ($quiz_id == $my_quiz->id){
        return;
      }
    }
    if ($wrapper = $this->wrapper) {
      try {
        $wrapper->field_quizzes_ref[] = $quiz_id;
        $wrapper->save();
      }
      catch (EntityMetadataWrapperException $e) {
        watchdog(__CLASS__, 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      }
    }
  }

  /**
   * Get's the URL to this product's content page from a consumer perspective
   * @return string
   */
  public function getConsumerUrl() {
    return url(RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/content/' . $this->getSku());
  }

  /**
   * Get's Score History URL to this product's content page from a consumer perspective
   * @return string
   */
  public function getConsumerHistoryUrl() {
    return url($this->getConsumerUrl() . '/history');
  }
}