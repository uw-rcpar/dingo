<?php
/**
 * @file
 * act_assignable_materials.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function act_assignable_materials_commerce_product_default_types() {
  $items = array(
    'act_assignable_materials' => array(
      'type' => 'act_assignable_materials',
      'name' => 'ACT Assignable Materials',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}
