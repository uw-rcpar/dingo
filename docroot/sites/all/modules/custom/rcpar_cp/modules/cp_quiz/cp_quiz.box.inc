<?php
/**
 * @file
 * cp_quiz.box.inc
 */

/**
 * Implements hook_default_box().
 */
function cp_quiz_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'cp_quiz_preview_info_modal';
  $box->plugin_key = 'simple';
  $box->title = 'Previewing your quiz';
  $box->description = 'Quiz preview info modal content';
  $box->options = array(
    'body' => array(
      'value' => '<p>You can preview this quiz after having scheduled it for launch. Choose the quiz preview operation from your quiz listing to experience the quiz from the students perspective. Quiz preview data will not be stored in the reports view.</p>
<div><img class="modal_image_center" src="/sites/default/files/DIN-1453-02.png" alt="Preview illustration" /> </div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['cp_quiz_preview_info_modal'] = $box;

  return $export;
}
