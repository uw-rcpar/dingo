<?php
/**
 * @file
 * cp_quiz.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function cp_quiz_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'cp_question_group-cp_quiz-field_cp_dates'.
  $field_instances['cp_question_group-cp_quiz-field_cp_dates'] = array(
    'bundle' => 'cp_quiz',
    'deleted' => 0,
    'description' => 'The start & end dates and timezone defining when the quiz is available. NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_dates',
    'label' => 'CP Quiz Dates',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - g:i:sa',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'cp_question_group-cp_quiz-field_cp_question'.
  $field_instances['cp_question_group-cp_quiz-field_cp_question'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_question',
    'label' => 'CP Question',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'cp_question_group-cp_quiz-field_cp_quiz_pools'.
  $field_instances['cp_question_group-cp_quiz-field_cp_quiz_pools'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Question Pools that make up this quiz.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_quiz_pools',
    'label' => 'CP Quiz Pools',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'cp_question_group-cp_quiz-field_cp_quiz_results_type'.
  $field_instances['cp_question_group-cp_quiz-field_cp_quiz_results_type'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => array(
      0 => array(
        'value' => 'detailed',
      ),
    ),
    'deleted' => 0,
    'description' => 'Students will only see results for their own performance, not for the class as a whole. NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_quiz_results_type',
    'label' => 'CP Quiz Results Type',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'cp_question_group-cp_quiz-field_cp_quiz_show_results'.
  $field_instances['cp_question_group-cp_quiz-field_cp_quiz_show_results'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => array(
      0 => array(
        'value' => 'student_finished',
      ),
    ),
    'deleted' => 0,
    'description' => 'Students will only see results for their own performance, not for the class as a whole. NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_quiz_show_results',
    'label' => 'CP Quiz Show Results',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 8,
    ),
  );

  // Exported field_instance:
  // 'cp_question_group-cp_quiz-field_cp_quiz_show_results_date'.
  $field_instances['cp_question_group-cp_quiz-field_cp_quiz_show_results_date'] = array(
    'bundle' => 'cp_quiz',
    'deleted' => 0,
    'description' => 'NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 13,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_quiz_show_results_date',
    'label' => 'CP Quiz Show Results Date',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - g:i:sa',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'cp_question_group-cp_quiz-field_cp_quiz_status'.
  $field_instances['cp_question_group-cp_quiz-field_cp_quiz_status'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'Default value of Draft (0) will be set until the Schedule quiz button is selected on last step of create/edit quiz form.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 16,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_quiz_status',
    'label' => 'CP Quiz Status',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 13,
    ),
  );

  // Exported field_instance:
  // 'cp_question_group-cp_quiz-field_cp_quiz_timer_active'.
  $field_instances['cp_question_group-cp_quiz-field_cp_quiz_timer_active'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'If enabled, the student\'s quiz session will conclude when the timer ends. NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_quiz_timer_active',
    'label' => 'CP Quiz Timer Active',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_onoff',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'cp_question_group-cp_quiz-field_cp_quiz_timer_length_hours'.
  $field_instances['cp_question_group-cp_quiz-field_cp_quiz_timer_length_hours'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_quiz_timer_length_hours',
    'label' => 'CP Quiz Timer Length Hours',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 9,
    ),
  );

  // Exported field_instance:
  // 'cp_question_group-cp_quiz-field_cp_quiz_timer_length_mins'.
  $field_instances['cp_question_group-cp_quiz-field_cp_quiz_timer_length_mins'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 14,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_quiz_timer_length_mins',
    'label' => 'CP Quiz Timer Length Minutes',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 11,
    ),
  );

  // Exported field_instance:
  // 'cp_question_group-cp_quiz-field_cp_student_group'.
  $field_instances['cp_question_group-cp_quiz-field_cp_student_group'] = array(
    'bundle' => 'cp_quiz',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Student group does not apply to Publisher quizzes.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_student_group',
    'label' => 'CP Student Group',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('CP Question');
  t('CP Quiz Dates');
  t('CP Quiz Pools');
  t('CP Quiz Results Type');
  t('CP Quiz Show Results');
  t('CP Quiz Show Results Date');
  t('CP Quiz Status');
  t('CP Quiz Timer Active');
  t('CP Quiz Timer Length Hours');
  t('CP Quiz Timer Length Minutes');
  t('CP Student Group');
  t('Default value of Draft (0) will be set until the Schedule quiz button is selected on last step of create/edit quiz form.');
  t('If enabled, the student\'s quiz session will conclude when the timer ends. NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.');
  t('NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.');
  t('Question Pools that make up this quiz.');
  t('Student group does not apply to Publisher quizzes.');
  t('Students will only see results for their own performance, not for the class as a whole. NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.');
  t('The start & end dates and timezone defining when the quiz is available. NOTE: label and settings in the edit field screen are similar to what are shown on the create/edit quiz form, but edits here will not change the form, changes must me made programmatically.');

  return $field_instances;
}
