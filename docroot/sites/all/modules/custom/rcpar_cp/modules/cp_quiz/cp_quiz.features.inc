<?php
/**
 * @file
 * cp_quiz.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cp_quiz_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function cp_quiz_eck_bundle_info() {
  $items = array(
    'cp_question_group_cp_quiz' => array(
      'machine_name' => 'cp_question_group_cp_quiz',
      'entity_type' => 'cp_question_group',
      'name' => 'cp_quiz',
      'label' => 'Curation Platform Quiz',
      'config' => array(),
    ),
  );
  return $items;
}
