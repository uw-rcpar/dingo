<?php
/**
 * @file
 * cp_question_pool.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function cp_question_pool_eck_bundle_info() {
  $items = array(
    'cp_question_group_cp_question_pool' => array(
      'machine_name' => 'cp_question_group_cp_question_pool',
      'entity_type' => 'cp_question_group',
      'name' => 'cp_question_pool',
      'label' => 'Curation Platform Question Pool',
      'config' => array(
        'managed_properties' => array(
          'title' => 0,
          'uid' => 0,
          'created' => 0,
          'changed' => 0,
        ),
      ),
    ),
  );
  return $items;
}
