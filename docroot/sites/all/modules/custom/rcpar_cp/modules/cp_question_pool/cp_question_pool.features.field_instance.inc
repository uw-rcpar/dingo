<?php
/**
 * @file
 * cp_question_pool.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function cp_question_pool_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'cp_question_group-cp_question_pool-field_cp_pool_machine_name'.
  $field_instances['cp_question_group-cp_question_pool-field_cp_pool_machine_name'] = array(
    'bundle' => 'cp_question_pool',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The machine name is set when creating an question pool. It may not be edited after it is created.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'machine_name',
        'settings' => array(),
        'type' => 'machine_name_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_pool_machine_name',
    'label' => 'CP Pool Machine Name',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'machine_name',
      'settings' => array(
        'size' => 128,
      ),
      'type' => 'machine_name_default',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'cp_question_group-cp_question_pool-field_cp_question'.
  $field_instances['cp_question_group-cp_question_pool-field_cp_question'] = array(
    'bundle' => 'cp_question_pool',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'cp_question_group',
    'field_name' => 'field_cp_question',
    'label' => 'Questions',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('CP Pool Machine Name');
  t('Questions');
  t('The machine name is set when creating an question pool. It may not be edited after it is created.');

  return $field_instances;
}
