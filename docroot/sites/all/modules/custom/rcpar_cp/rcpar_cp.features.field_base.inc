<?php
/**
 * @file
 * rcpar_cp.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function rcpar_cp_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_cp_question'.
  $field_bases['field_cp_question'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_cp_question',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'ipq_drs_question' => 'ipq_drs_question',
          'ipq_mcq_question' => 'ipq_mcq_question',
          'ipq_research_question' => 'ipq_research_question',
          'ipq_tbs_form_question' => 'ipq_tbs_form_question',
          'ipq_tbs_journal_question' => 'ipq_tbs_journal_question',
          'ipq_tbs_question' => 'ipq_tbs_question',
          'ipq_wc_question' => 'ipq_wc_question',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
