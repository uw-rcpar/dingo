<?php

class ActProduct {
  protected $isLoaded = FALSE;
  protected $product;
  protected $wrapper;

  /**
   * ActSampleProduct constructor.
   * @param $id_or_sku_or_product
   * - Product ID, SKU, or already loaded product object
   */
  function __construct($id_or_sku_or_product) {
    // If an int is supplied, load by product id
    if (is_numeric($id_or_sku_or_product)) {
      $product = commerce_product_load($id_or_sku_or_product);
    }
    // For string, load by SKU
    elseif(is_string($id_or_sku_or_product)) {
      $product = commerce_product_load_by_sku($id_or_sku_or_product);
    }
    // Assume we were given a loaded product entity
    else {
      $product = $id_or_sku_or_product;
    }

    if (is_object($product) && !empty($product->product_id)) {
      $this->product = $product;
      $wrapper = entity_metadata_wrapper('commerce_product', $product);
      $this->wrapper = $wrapper;
      $this->isLoaded = TRUE;
    }
  }

  public function isLoaded() {
    return $this->isLoaded;
  }

  /**
   * Returns the commerce product ID
   * @return int
   */
  public function getId() {
    return $this->wrapper->getIdentifier();
  }

  public function getTitle() {
    return $this->product->title;
  }

  public function getSku() {
    return $this->product->sku;
  }

  /**
   * Helper function to get the value of a field on the entity from the wrapper.
   * This function allows us to avoid the boilerplate of checking for the wrapper, supplying a fallback default,
   * and handling EntityMetadataWrapperException exceptions.
   *
   * @param string $fieldname
   * - A field name on the node
   * @param bool $default
   * - Default value to return if there is any problem getting the value (an EntityMetadataWrapperException for example)
   * @return mixed
   * - Will return whatever value the field stores, which could be just about anything.
   */
  protected function getFieldValue($fieldname, $default = FALSE) {
    if ($wrapper = $this->wrapper) {
      try {
        return $wrapper->{$fieldname}->value();
      }
      catch (EntityMetadataWrapperException $e) {
        watchdog(__CLASS__, 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      }
    }
    return $default;
  }

  /**
   * Get question pools for this product
   * @return array
   * - A numeric array of loaded question pool entities
   */
  public function getQuestionPool() {
    return $this->getFieldValue('field_question_pool_ref', array());
  }
}