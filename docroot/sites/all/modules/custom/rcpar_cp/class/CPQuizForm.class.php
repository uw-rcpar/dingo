<?php

/**
 * Class CPQuizForm extends CPQuiz to add form functionality
 */
class CPQuizForm extends CPQuiz {
  protected $form;
  protected $form_state;

  /** @var RCPARPartner $partner */
  protected $partner;

  protected $actProducts;

  protected $questionPools = NULL;

  /**
   * CPQuizForm constructor.
   * @param array $form @see drupal FAPI
   * @param array $form_state @see drupal FAPI
   * @param empty str (or NULL)|integer $quiz_id empty when creating a new quiz, otherwise the quiz_id
   */
  function __construct(&$form, &$form_state, $quiz_id = NULL) {
    $this->form =& $form;
    $this->form_state =& $form_state;

    if (empty($quiz_id)) { // creating a new quiz
      $entity_type = 'cp_question_group';
      // NOTE: here $quiz_id is a little misleading, it's actually an entity but this way I don't have to type parent::__construct twice (of course this note took even more energy)
      $quiz_id = entity_create('cp_question_group', array('type' => 'cp_quiz'));
    }
    parent::__construct($quiz_id);
  }


  /**
   * Gets an array of values the form expects keyed on the field name, and
   * "selected_q_nos" which holds the values for $form_state['selected_q_nos']
   * @param string $act_type 'asl': original CP quiz type, 'pal': publisher quiz type
   *
   * @return array like:
   * i.e. (for an 'asl' act type)
   *  array (
   *  'quiz_group_select' => '2221',
   *  'quiz_name' => 'Result Date Quiz',
   *  'results_type' => 'detailed',
   *  'timer_active' => true,
   *  'timer_length_hours' => '2',
   *  'timer_length_minutes' => '3',
   *  'show_results' => 'quiz_specific_date',
   *  'start_date_time' => array (
   *    'date' => '01/09/2017',
   *    'time' => '01:11',
   *  ),
   *  'end_date_time' => array (
   *    'date' => '02/28/2017',
   *    'time' => '02:22',
   *  ),
   *  'quiz_date_timezone' => 'Pacific/Honolulu',
   *  'field_cp_quiz_show_results_date' => array (
   *     'date' => '03/03/2017',
   *     'time' => '13:33',
   *   ),
   *  'selected_q_nos' => array (
   *     '3020649' => 0,
   *     '3020918' => 1,
   *     '3020923' => 2,
   *   ),
   *  )
   */
  public function getFormFieldVals($act_type = 'asl') {
    $form_vals = array();

    // most of the form values can simply be mapped from field values
    $simple_form_entity_mappings = $this->getSimpleFieldMappings($act_type);

    foreach ($simple_form_entity_mappings AS $form_field => $entity_field) {
      $val = $this->getFieldValue($entity_field);
      $form_vals[$form_field] = ((empty($val)) ? '' : $val);
    }

    // we only need to do this for asl quizzes, publisher quizzes don't save date fields
    if ($act_type == 'asl') {
      // time fields need a little more work

      // NOTE: @see function _rcpar_convert_form_date_to_mysql_w_blanks &
      // function rcpar_cp_date_popup_process_alter for how blanks are handled

      // get start_date_time & 'end_date_time'

      // this value will be like:
      // array (
      //   'value' => '2017-01-09 01:11:00',
      //   'value2' => '2017-02-28 02:22:00',
      //   'timezone' => 'Pacific/Honolulu',
      //   'offset' => NULL,
      //   'offset2' => NULL,
      //   'timezone_db' => 'Pacific/Honolulu',
      //   'date_type' => 'datetime',
      // );
      $field_cp_dates_val = $this->getFieldValue('field_cp_dates');

      if (!empty($field_cp_dates_val)) {
        // NOTE: This is sometimes empty when saving a new quiz as a draft.
        //
        // Otherwise, this will always be set because empty field values are converted
        // @see function _rcpar_convert_form_date_to_mysql_w_blanks &
        // function rcpar_cp_date_popup_process_alter for how blanks are handled
        $dtz = new DateTimeZone($field_cp_dates_val['timezone']);

        $start_datetime = new DateTime($field_cp_dates_val['value'], $dtz);
        $form_vals['start_date_time'] = date_format($start_datetime, 'Y-m-d H:i:s');

        $end_datetime = new DateTime($field_cp_dates_val['value2'], $dtz);
        $form_vals['end_date_time'] = date_format($end_datetime, 'Y-m-d H:i:s');

        // set the timezone, it expects an array like this
        $form_vals['quiz_date_timezone'] = array(
          'timezone' => $field_cp_dates_val['timezone'],
        );

        // 'field_cp_quiz_show_results_date' returns a unix timestamp like '1488547980',
        $field_cp_quiz_show_results_date_val = $this->getFieldValue('field_cp_quiz_show_results_date');

        if ($field_cp_quiz_show_results_date_val) {
          // Since field_cp_quiz_show_results_date is stored in seconds since the Unix Epoch format
          // before setting the field values we need to:
          // 1) Create the date
          // 2) set it's timezone
          $field_cp_quiz_show_results_date = date_create_from_format('U', $field_cp_quiz_show_results_date_val);
          date_timezone_set($field_cp_quiz_show_results_date, $dtz);

          $form_vals['show_results_date'] = date_format($field_cp_quiz_show_results_date, 'Y-m-d H:i:s');
        }
        else { // field_cp_quiz_show_results_date isn't set
          $form_vals['show_results_date'] = '';
        }
      }
    }

    // return the questions & their order in a format the form expects:
    // $q_nid => #weight
    $selected_q_nos = array();

    if ($this->quiz && property_exists($this->quiz, 'field_cp_question')) {
      // simpler not to use a wrapper for this

      // $quiz->field_cp_question looks like:
      // array (
      //   'und' =>
      //     array (
      //       0 =>
      //         array (
      //           'target_id' => '3020649',
      //         ),
      //       1 =>
      //         array (
      //           'target_id' => '3020918',
      //         ),
      //       2 =>
      //         array (
      //           'target_id' => '3020923',
      //         ),
      //     ),
      // )
      foreach ($this->quiz->field_cp_question[LANGUAGE_NONE] AS $delta => $val_arr) {
        $selected_q_nos[$val_arr['target_id']] = $delta;
      }
    }
    $form_vals['selected_q_nos'] = $selected_q_nos;

    return $form_vals;
  }

  /**
   * Get an array of field to entity mappings simple enough to use a foreach loop
   * @param string $act_type 'asl': original CP quiz type, 'pal': publisher quiz type
   *
   * @return array of form_field_key => entity_field_name
   */
  public function getSimpleFieldMappings($act_type = 'asl') {
    $form_simple_field_mappings = array(
      'quiz_group_select' => 'field_cp_student_group',
      'quiz_name' => 'title',
      'results_type' => 'field_cp_quiz_results_type',
      'timer_active' => 'field_cp_quiz_timer_active',
      'timer_length_hours' => 'field_cp_quiz_timer_length_hours',
      'timer_length_minutes' => 'field_cp_quiz_timer_length_mins',
      'show_results' => 'field_cp_quiz_show_results',
      'question_pool_ids' => 'field_cp_quiz_pools',
    );
    if ($act_type == 'pal') {
      // publisher quizzes don't have the following fields
      $unset_keys = array(
        'quiz_group_select',
        'results_type',
        'timer_active',
        'timer_length_hours',
        'timer_length_minutes',
        'show_results',
      );
      foreach ($unset_keys AS $unset_key) {
        unset($form_simple_field_mappings[$unset_key]);
      }
    }
    return $form_simple_field_mappings;
  }

  /**
   * Save the form values into the entity, set an error message if there is one
   *
   * @param array $entity_field_vals array of form values keyed on entity_field_names to save in the entity
   * @return bool|int the quiz_id (FALSE would indicate an error occurred)
   */
  public function saveFormVals($entity_field_vals) {
    /**
     * some field settings may have been deleted since the last time this was saved,
     * we'll need to set those entity values to NULL
     */
    // get all of the props this quiz currently has set
    $all_quiz_props = array_keys(get_object_vars($this->quiz));

    // these props we don't want to set to NULL
    // include title because even if it's unset somehow, we never want to leave it null
    $ignore_props = array('created', 'id', 'type', 'title', 'rdf_mapping');

    //Publisher quizzes will also ignore the following fields because they will never be set.
    if ($this->getQuizType() == 'act_pal') {
      $ignore_props = array_merge($ignore_props, array(
        'field_cp_dates',
        'field_cp_quiz_show_results_date',
        'field_cp_quiz_timer_length_hours',
        'field_cp_quiz_timer_length_mins'
      ));
    }

    // the props we are going to set
    $props_to_be_set = array_keys($entity_field_vals);

    // this gives us the props we'll need to unset
    $props_to_unset = array_diff($all_quiz_props, $ignore_props, $props_to_be_set);

    foreach ($props_to_unset AS $fieldname) {
      $success = $this->setFieldValue($fieldname, NULL, FALSE);

      if (!success) { // shouldn't happen but just in case
        $msg = t("Could not UNSET the quiz's :fieldname",
          array(
            ':fieldname' => $fieldname,
          )
        );
        drupal_set_message($msg, 'error');
      }
    }

    /**
     * Set values for props currently set
     */
    // get the name of the last key of values to be set
    end($entity_field_vals);
    $last_key = key($entity_field_vals);

    foreach ($entity_field_vals AS $fieldname => $value) {
      // don't save the entity until the last field is set
      $do_save = ($fieldname == $last_key);
      $success = $this->setFieldValue($fieldname, $value, $do_save);

      if (!success) { // shouldn't happen but just in case
        $msg = t("Could not set the quiz's :fieldname to :value",
          array(
            ':fieldname' => $fieldname,
            ':value' => $value
          )
        );
        drupal_set_message($msg, 'error');
      }
    }
    return $success;
  }

  /**
   * Return the partner id that owns an email group
   * @param integer $gid the email group's identifier
   * return integer the partner that owns the email group
   * TODO: Refactor this to use procedural equivalent function in rcpar_partners scheduled in next merge next from master
   */
  public function getEmailGroupPartner($gid) {
    $query = db_query("SELECT partner_nid
                      FROM {rcpar_partners_emails_group}
                      WHERE id = :gid",
      array(':gid' => $gid));

    $part_id = $query->fetchField();
    return $part_id;
  }

  /**
   * Generate options for the chapters select, with an option to add them to $form_state['saved_query_results']
   *
   * @param array $chapter_options_arg
   *  - chapter_ids needing options in the form of:
   *    section_id => array(chapter_id => chapter_id (duped)) to limit the chapters & options to only those listed in each section
   *
   * @param bool $do_add_to_form_state
   *  - whether to add to $form_state['saved_query_results']
   *
   * @return array
   *  - array of chapter_id => option label (often this is ignored)
   */
  public function setChapterOptions($chapter_options_arg, $do_add_to_form_state = TRUE) {
    $form_state =& $this->form_state;

    // If there is no question pool, we will not add the  '_all' => t('All Sections') option.
    $do_include_all_sections = $this->getQuestionPools() !== array();

    // This will be what we return .
    $return_chapter_options = array();

    $all_selected_section_chapters_options = array();
    foreach ($chapter_options_arg AS $selected_section => &$chapters_options) {
      $all_selected_section_chapters_options[$selected_section] = array();
      $course = ipq_common_get_online_course_by_section_tid($selected_section);
      $default_version = exam_version_get_default_version();
      $chapters = ipq_common_get_chapters_by_course_nid($course->nid, TRUE, $default_version);
      foreach ($chapters as $key => $val) {
        $all_selected_section_chapters_options[$selected_section][$key] = $form_state['saved_query_results']['section_options'][$selected_section] . ' ' . $val['delta'] . ': ' . $val['title'];
      }
      // Add only options for chapters included in each section
      foreach ($chapters_options AS $my_option_key => &$my_option_val) {
        if (!in_array($my_option_key, array('_none', '_all'))) {
          // Users with question err access need to see blank chapter selects so they can access the questions that create them.
          if ((!rcpar_cp_question_error_access()) && empty(trim($all_selected_section_chapters_options[$selected_section][$my_option_key]))) {
            // Typical users will just have the chapter select option removed.
            unset($chapters_options[$my_option_key]);
          }
          else {
            $my_option_val = $all_selected_section_chapters_options[$selected_section][$my_option_key];
          }
        }
      }
      // Sort this section's chapter_options.
      natsort($chapters_options);

      if (count($chapters_options) > 1) {
        $first_options = array(
          '_none' => t('- Select a Section -'),
          '_all' => t('All Sections')
        );
        if (!$do_include_all_sections) { // i.e. for quizzes w/o question_pools
          unset($first_options['_all']);
        }
        $chapters_options = $first_options + $chapters_options;
      }

      if ($do_add_to_form_state) {
        $form_state['saved_query_results']['chapters_options'][$selected_section] = $chapters_options;
      }
      $return_chapter_options[$selected_section] = $chapters_options;
    }
    return $return_chapter_options;
  }

  /**
   * Generate options for the topics keyed by the chapter_ids they belong to, with an option add to $form_state['saved_query_results']
   * @param $chapter_topics_arg
   *  - arrays of topic_id => topic_id in an array keyed by chapter_id
   *
   * @param bool $do_add_to_form_state
   *  - whether to add to $form_state['saved_query_results'] (this is used when formatting selected questions when editing quizzes that have no q_pools
   * TODO: Consider setting this automatically to false when (!empty($chapters_options_arg))
   *
   * @param null|array $chapters_options_arg
   *  - when $do_add_to_form_state is FALSE, we need to supply the $chapters_options_arg to properly format the topic
   *
   * @return array
   *  - return the generated $chapter_topics array with labels for the values of the topic_ids (often this is ignored)
   */
  public function setChapterTopics($chapter_topics_arg, $do_add_to_form_state = TRUE, $chapters_options_arg = NULL) {
    if ($do_add_to_form_state) {
      $form_state =& $this->form_state;
      $chapters_options_source = $form_state['saved_query_results']['chapters_options'];
    }
    else {
      $chapters_options_source = $chapters_options_arg;
    }

    $all_section_opts = $this->getAllSectionOpts();

    // This will be what we return if $do_add_to_form_state == FALSE.
    $return_chapter_topics = array();

    $topic_opts = array();
    $topic_opts_to_add = array(
      '_all' => t('All Topics'),
    );

    // This is close to how ipq.common.module ipq_common_form_alter() creates the topic options.
    // selected section has to be reset- a little different that the other getting topics routine in this function
    // TODO: SIMPLIFY - this is more complicated than it needs to be. The code was copied from when I was using more than one section.
    // TODO: If you wanted to simplify, you could pass the section in with the argument, then set selected_section from it.
    $selected_section = NULL;
    $all_chapter_topic_vals = array();
    $default_version = exam_version_get_default_version();
    foreach ($chapter_topics_arg AS $selected_chapter_id => &$chapter_topic_vals) {
      // build up topic_opts for this chapter
      $topics = ipq_common_get_topics_by_chapter_nid($selected_chapter_id, $default_version);
      foreach ($topics as $delta => $t) {
        // See if we need to reset $selected_section
        foreach ($chapters_options_source AS $my_selected_section => $my_selected_section_chapters_options) {
          if (array_key_exists($selected_chapter_id, $my_selected_section_chapters_options)) {
            if ($my_selected_section != $selected_section) {
              $selected_section = $my_selected_section;
              // This takes a while so only do it when needed
              $course = ipq_common_get_online_course_by_section_tid($selected_section);
              $chapters = ipq_common_get_chapters_by_course_nid($course->nid, FALSE, $default_version);
            }
            break;
          }
        }
        // produces a label like "AUD 4.10: Financial Statement Accounts: Inventories"
        $topic_opts[$t->nid] = $all_section_opts[$selected_section] . ' ' . sprintf("%s.%02s: %s", $chapters[$selected_chapter_id]['delta'], $delta + 1, $t->title);
      }

      // add names for our topic options
      foreach ($chapter_topic_vals AS $chapter_topic_id => &$chapter_topic_val) {
        // SAVE THIS it could be used to eliminate empty topic selects. (search q_nids_needing_corrections for how it's done now)
        // if (empty($topic_opts[$chapter_topic_id])){
        //   unset($chapter_topic_vals[$chapter_topic_id]);
        // }
        // else {
        //   $chapter_topic_val = $topic_opts[$chapter_topic_id];
        // }

        $chapter_topic_val = $topic_opts[$chapter_topic_id];
      }
      natsort($chapter_topic_vals);
      if ($do_add_to_form_state) {
        $form_state['saved_query_results']['chapters_topics'][$selected_chapter_id] = $topic_opts_to_add + $chapter_topic_vals;
      }
      else {
        $return_chapter_topics[$selected_chapter_id] = $topic_opts_to_add + $chapter_topic_vals;
      }
      $all_chapter_topic_vals += $chapter_topic_vals;
    }
    natsort($chapter_topics_arg);
    // Add the chapter_topics to $form_state['saved_query_results']['chapters_topics']['_all'] in sorted order
    // and to $form_state['saved_query_results']['chapters_options']['_all']in order of part & section id.
    if ($do_add_to_form_state) {
      if (!array_key_exists('_all', $form_state['saved_query_results']['chapters_topics'])) {
        $form_state['saved_query_results']['chapters_topics']['_all'] = array();
      }
      foreach ($form_state['saved_query_results']['section_options'] AS $my_section_id => $my_section_chapter_options) {
        foreach ($chapter_topics_arg AS $selected_chapter_id => $chapter_topic_vals) {
          $form_state['saved_query_results']['chapters_topics']['_all'] += $chapter_topic_vals;
        }
      }
    }
    return $return_chapter_topics;
  }

  /**
   * Gets question values for a section, optionally limited to those with ids in $qnids_arg.
   *
   * @param integer $section_tid_arg
   *  - the tid of the section to limit the qvals to
   *
   * @param array $qnids_arg
   *  - if not empty, the ids of questions to limit the returned q_vals
   *
   * @return array
   *  - of arrays containing 'ipq_questions_id', 'chapter_id', 'topic_id', keyed on the 'ipq_questions_id'
   */
  public function getSectionQnids($section_tid_arg, $qnids_arg = array()) {
    // Using ORDER BY and GROUP BY here so that only the first chapter/topic of each question is retrieved
    // to follow the current functionality of building & reporting for quizzes.
    // Same strategy is used in rcpar_cp_get_qvals_query_result & rcpar_cp_quiz_form.

    /**
     * I apologize for this dynamic method of building the query string. I realize it's a bit
     * awkward to read. In support of this strategy, it assures that the return array always
     * formatted identically.
     */
    $default_version = exam_version_get_default_version();
    $query_str = "SELECT q.ipq_questions_id, q.chapter_id, q.topic_id
                  FROM {ipq_question_pool} q
                  INNER JOIN {taxonomy_term_data} t ON t.tid = q.exam_version_id
                  WHERE q.section_id = :sec_id";

    if (!empty($qnids_arg)) {
      $query_str .= "
                  AND q.ipq_questions_id IN (:qnids_arr)";
    }

    $query_str .= "
                  AND t.name = '$default_version'
                  GROUP BY q.ipq_questions_id
                  ORDER BY q.ipq_questions_id, q.section_id, q.chapter_id, q.topic_id";


    $query = db_query($query_str,
      array(':sec_id' => $section_tid_arg, ':qnids_arr' => $qnids_arg));

    $return_q_nids = $query->fetchAllAssoc('ipq_questions_id', PDO::FETCH_ASSOC);
    return $return_q_nids;
  }

  /**
   * Sets the $form_state saved_queries chapters_options and chapters_topics options and chapters_topics_q_nids for one section's quiz questions.
   *
   * Has an option to return the results instead of saving them to form state (which will also save changes to chapters_topics_q_nids.)
   * This is used for building selected questions when editing existing quizzes, (especially those without q_pools.)
   *
   * NOTE: needs to be repeated for each section.
   * @param array $needed_q_nids
   *  - array of arrays containing 'ipq_questions_id', 'chapter_id', 'topic_id', keyed on the 'ipq_questions_id'
   * @param integer $section_tid_arg
   *  - the tid of the section
   * @param bool $do_save_to_form_state
   *  - if false, return an array of results instead of saving to form_state
   */
  public function setChapterTopicOptionsFromQnids($needed_q_nids, $section_tid_arg, $do_save_to_form_state = TRUE) {
    $form_state =& $this->form_state;

    $return_results = array();

    // We will use this to set the chapter options
    $my_chapter_opts = array();

    // We'll use this to build the topic options
    $my_chapter_topics_arg = array();

    // This will be an array to add to the $form_state['chapters_topics_q_nids]
    $my_chapters_topics_q_nids = array();

    // Find only the chapters & topics needed for questions in these q_nids.
    foreach ($needed_q_nids AS $needed_q_nid) {
      // Add the necessary new chapter_ids.
      if (!array_key_exists($needed_q_nid['chapter_id'], $my_chapter_opts)) {
        $my_chapter_opts[$needed_q_nid['chapter_id']] = $needed_q_nid['chapter_id'];
        $my_chapter_topics_arg[$needed_q_nid['chapter_id']] = array();
        $my_chapters_topics_q_nids[$needed_q_nid['chapter_id']] = array('_all' => array());
      }

      // Assure the chapter_topics_arg is is not duped by adding the key & value as the topic id.
      $my_chapter_topics_arg[$needed_q_nid['chapter_id']][$needed_q_nid['topic_id']] = $needed_q_nid['topic_id'];

      // Add new topic_ids to the chapter_topics_q_nids.
      if (!array_key_exists($needed_q_nid['topic_id'], $my_chapters_topics_q_nids[$needed_q_nid['chapter_id']])) {
        $my_chapters_topics_q_nids[$needed_q_nid['chapter_id']][$needed_q_nid['topic_id']] = array();
      }
      // Add the q_nid to the correct chapter_topic.
      $my_chapters_topics_q_nids[$needed_q_nid['chapter_id']][$needed_q_nid['topic_id']][] = $needed_q_nid['ipq_questions_id'];
    }


    // Set this section's chapter options.
    $my_chapter_opts_arg = array($section_tid_arg => $my_chapter_opts);
    $return_results['chapters_options'] = $this->setChapterOptions($my_chapter_opts_arg, $do_save_to_form_state);

    // Set the chapter topics for all chapters in this section.
    if ($do_save_to_form_state) {
      $return_results['chapters_topics'] = $this->setChapterTopics($my_chapter_topics_arg, $do_save_to_form_state);
    }
    else {
      // When we are not saving the results to $form_state, we need to supply the chapters_topics so the topics will be properly formatted.
      $return_results['chapters_topics'] = $this->setChapterTopics($my_chapter_topics_arg, $do_save_to_form_state, $return_results['chapters_options']);
    }

    if ($do_save_to_form_state) {
      // Cycle through each chapters' topics in order and add their q_nids to the '_all' saved query.
      foreach (array_keys($form_state['saved_query_results']['chapters_topics']) AS $my_chapter_id) {
        foreach (array_keys($form_state['saved_query_results']['chapters_topics'][$my_chapter_id]) AS $topic_id) {
          if ($topic_id != '_all') {
            if(is_array($my_chapters_topics_q_nids[$my_chapter_id]['_all']) && is_array($my_chapters_topics_q_nids[$my_chapter_id][$topic_id])) {
              $my_chapters_topics_q_nids[$my_chapter_id]['_all'] = array_merge($my_chapters_topics_q_nids[$my_chapter_id]['_all'], $my_chapters_topics_q_nids[$my_chapter_id][$topic_id]);
            }
          }
        }
      }
      // Add the chapters_topics_q_nids to the form_state.
      $form_state['saved_query_results']['chapters_topics_q_nids'] += $my_chapters_topics_q_nids;
    }

    return $return_results;
  }

  /**
   * Gets array of section_tid => section name in natsort order
   * @return array like:
   * array (
   *   1452 => 'AUD',
   *   1455 => 'BEC',
   *   1453 => 'FAR',
   *   1454 => 'REG',
   * )
   */
  public function getAllSectionOpts() {
    $all_section_opts = array();
    // options for course_section select are all possible values of course minus 'FULL'
    $voc = taxonomy_vocabulary_machine_name_load('course_sections');
    $tree = taxonomy_get_tree($voc->vid);
    foreach ($tree as $term) {
      if ($term->name != 'FULL') {
        $all_section_opts[$term->tid] = $term->name;
      }
    }
    natsort($all_section_opts);
    return $all_section_opts;
  }

  /**
   * Based on the question pools for the products that this quiz is being generated from, we build a list of allowed
   * question ids that can be used when creating the quiz. If one or more products does not have a question pool
   * associated, all available questions are allowed.
   *
   * @return array
   * - Returns a numeric array of question node ids keyed by node id
   */
  public function getAllowedQuestionIds() {
    $question_pools = $this->getQuestionPools();

    // For empty pools, we query for ALL available question ids in the ipq_question_pool table
    if($question_pools === array()) {
      return db_query("SELECT ipq_questions_id FROM {ipq_question_pool} WHERE exam_version_id = :tid",
        array(':tid' => exam_version_get_default_version_tid())
      )->fetchCol();
    }
    // Build a list of allowed questions by iterating through ever assignable products question pool
    // that this partner gives to new enrollees
    else {
      $allowed_question_ids = array();
      $loaded_pools = rcpar_cp_get_question_pools($question_pools, TRUE);
      foreach ($loaded_pools as $pool) {
        // Iterate through each question and add it to our allowed quesiton list
        if(!empty($pool->field_cp_question)) {
          foreach ($pool->field_cp_question[LANGUAGE_NONE] as $item) {
            $allowed_question_ids[$item['target_id']] = $item['target_id'];
          }
        }
      }
    }

    return $allowed_question_ids;
  }

  /**
   * @return RCPARPartner
   */
  public function getPartner() {
    return $this->partner;
  }

  /**
   * Sets the partner of the ACT_ASL quiz being built. (There are no related partners of PAL quizzes.)
   * @param RCPARPartner|NULL $partner
   *  - if NULL will assume that the form_field quiz_group_select has already been set
   *    and set it from there (happens only for asl quizzes)
   */
  public function setPartner($partner = NULL) {
    if(!is_null($partner)) {
      $this->partner = $partner;
      return;
    }
    if (empty($this->form_state['act_sample_prod_id'])) {
      // It is a act quiz, determine the partner that owns by the email group
      $partner_id = rcpar_partners_get_owning_partner_of_email_group($this->getStudentGroup());
      $this->partner = new RCPARPartner($partner_id);
    }

  }

  /**
   * Sets Act Product for a quiz form
   * NOTE: For als quizzes, this always calls set $this->setPartner first
   */
  public function setActProducts() {
    // Special case: Unassigned quiz. If email group is 0, this is an unassigned quiz.
    if($this->getStudentGroup() == "0") {
      // Load this include so we can see default constant
      module_load_include('inc', 'rcpar_cp', 'includes/rcpar.admin');
      $this->actProducts = array();

      // Build an array of products
      foreach(array_filter(variable_get('act_prof_sample_skus', RCPAR_CP_PROF_SAMPLE_SKUS)) as $sku) {
        $this->actProducts[$sku] = new ActAssignableProduct($sku);
      }

      return;
    }

    if (empty($this->form_state['act_sample_prod_id'])) {
      // it's an asl quiz
      $this->setPartner();
      $this->actProducts = $this->getPartner()->getActAssignableProducts();
    }
    else {
      // pal quizzes will only get an array of the ActSampleProduct built from the one this quiz is built from.
      $my_act_product = new ActSampleProduct($this->form_state['act_sample_prod_id']);
      $this->actProducts = array($my_act_product);
    }
    // Now, set the question pools
  }

  /**
   * @return array
   *
   */
  public function getActProducts() {
    return $this->actProducts;
  }

  /**
   * @return array|NULL
   * - When the array is empty, one or more products used to build the quiz have no question pool, so the available
   * question list will be unbounded. When NULL, the pools have not been initialized yet. Otherwise a numeric array of
   * question pool machine names is returned
   */
  public function getQuestionPools() {
    return $this->questionPools;
  }

  /**
   * Sets the question pools associated with a quiz. This is called so far only from
   * there.
   *
   * Note $this->setActProducts() must be called first.
   *
   * @param array|NULL $questionPools (these may be set for asl type quizzes)
   * - When NULL, question pools will be determined by the associated ACT products.
   *
   * NOTE: $this->setActProducts() must be called first, this is a routine called
   * so far only the first time the quiz form class is created when editing a quiz.
   */
  public function setQuestionPools($questionPools = NULL) {
    if(!is_null($questionPools)) {
      $this->questionPools = $questionPools;
      $this->form_state['question_pool_ids'] = $this->questionPools;
      return;
    }

    // Iterate through each assignable product
    /** @var ActProduct $partner_assignable_product */
    // NOTE: $partner_assignable_product could be either an ActAssignableProduct or ActSampleProduct
    foreach ($this->getActProducts() as $partner_assignable_product) {
      $product_pool = $partner_assignable_product->getQuestionPool();

      // For empty product pools, we query for ALL available question ids in the ipq_question_pool table
      if(empty($product_pool)) {
        $this->questionPools = array();
        $this->form_state['question_pool_ids'] = $this->questionPools;
        return;
      }
      else {
        $this->questionPools[] = $product_pool->field_cp_pool_machine_name[LANGUAGE_NONE][0]['value'];
      }
    }

    $this->form_state['question_pool_ids'] = $this->questionPools;
  }

  /**
   * Get this quiz's student group
   * We override the base method so that we can get the group from the saved value in step 1 of
   * the form in case it has not been saved with the entity yet.
   * @return int|string
   *  - The id of the student group. Note that the id may be "0" in the case of an unassigned
   * quiz, but Drupal will return the value typecast as a string.
   */
  public function getStudentGroup() {
    // Give preference to $_POST, $form_state['input'], for $form_state['storage'] for the
    // email group ID first.
    $gid = _rcpar_cp_get_current_form_field_val($this->form_state, 'quiz_group_select');

    // If nothing is found, call the parent method which will attempt to load the field from
    // the saved entity.
    if(is_null($gid)) {
      $gid = parent::getStudentGroup();
    }

    return $gid;
  }

  /**
   * Used to explicitly set the email group id of an asl quiz. (Needed when saving
   * a new quiz as a draft the first time.)
   * @param integer $gid
   *  - the email group id
   */
  public function setAslEmailGroup($gid) {
    $success = $this->setFieldValue('field_cp_student_group', $gid);
  }

}
