<?php

/**
 * Class CPUser
 * Handles functionality relating to a CP user as well as CP user's quiz. For example, in
 * quizResultsAvailableToConsumer(), we are mainly dealing with quiz properties, but it is
 * also necessary to know who the student is, since results are not available unless the
 * student has taken the quiz.
 *
 * This differs from CPQuiz class in that CPQuiz will never reference a specific user.
 */
class CPUser {
  private $user;

  function __construct($uid_or_user = NULL) {
    // If no uid is supplied, use the current user
    if(is_null($uid_or_user)) {
      $user = $GLOBALS['user'];
    }
    else {
      if(is_numeric($uid_or_user)) {
        $user = user_load($uid_or_user);
      }
      else {
        $user = $uid_or_user;
      }
    }

    $this->user = $user;
  }

  /**
   * Load quizzes that are eligible to be taken by the user. This means:
   * - Quiz was created in the student's group
   * - Current time is between quiz open and close dates
   * - Does not consider if student has taken the quiz or not
   *
   * @return array
   * - Array of loaded cp_quiz entities indexed by id
   */
  public function getConsumableQuizzes() {
    // Get all the groups that the student is a part of. Users must have an active entitlement
    // from a partner to be considered in the group.
    $gids = $this->getEmailGroupIds();

    // No groups, return an empty array
    if(empty($gids)) {
      return array();
    }

    // Get all the quizzes assigned to these groups
    $query = new EntityFieldQuery();

    // Query for non-draft quizzes in the email groups this user belongs to
    $result = $query->entityCondition('entity_type', 'cp_question_group')
      ->entityCondition('bundle', 'cp_quiz')
      ->fieldCondition('field_cp_student_group', 'value', $gids, 'IN')
      ->fieldCondition('field_cp_quiz_status', 'value', TRUE, '=') // Published quiz (not draft)
      ->fieldOrderBy('field_cp_dates', 'value', 'ASC') // Order by quiz start date, ascending
      ->execute();

    // If we found quizzes, return an array of loaded quiz entities indexed by id
    if (!empty($result['cp_question_group'])) {
      return entity_load('cp_question_group', array_keys($result['cp_question_group']));
    }

    // No quizzes found, return empty array
    return array();
  }

  /**
   * Gets all partner IDs from which the user has received an active "ACT Assignable Materials"
   * entitlement.
   * @return array
   * - Numeric array of partner node ids keyed by node id
   */
  public function getActAssignablePartners() {
    $partner_nids = array();
    $actSkus = rcpar_cp_user_get_assignable_entitlement_skus();
    $user_entitlements = user_entitlements_get_entitlements($this->user);

    foreach (element_children($user_entitlements) as $nid) {
      $entitlement_wrapper = entity_metadata_wrapper('node', $user_entitlements[$nid]['#node']);
      try {
        // Get entitlement SKU
        $sku = $entitlement_wrapper->field_product_sku->value();

        // Ignore non-ACT skus
        if (!in_array($sku, $actSkus)) {
          continue;
        }

        // We have an ACT sku, store its partner id
        $partner_nids[$entitlement_wrapper->field_partner_id->value()] = $entitlement_wrapper->field_partner_id->value();
      }
      catch (EntityMetadataWrapperException $e) {}
    }

    return $partner_nids;
  }

  /**
   * Returns an array of group ids from rcpar_partners_emails_group that this user is a part of,
   * as long as they also have an active "ACT Assignable Materials" entitlement that comes from
   * that partner.
   *
   * @return array
   * - Numeric array of group ids.
   */
  public function getEmailGroupIds() {
    return db_query("SELECT gid FROM {rcpa_partners_emails} WHERE email = :email AND partner_nid IN(:partnernids)",
      array(':email' => $this->user->mail, ':partnernids' => $this->getActAssignablePartners()))
      ->fetchCol();
  }

  /**
   * For a given user and quiz, returns ALL session data (ipq_saved_session_data) that the user has generated for that
   * quiz.
   *
   * @param $quiz_id
   * - Id of the cp_quiz entity
   * @return array
   * - Returns an array of stdclass objects. Each object contains all of the fields from the ipq_saved_session_data table.
   */
  function getSessionDataForQuiz($quiz_id) {
    return db_query("SELECT i.id, i.ipq_saved_sessions_id, i.updated_on, i.is_correct FROM {ipq_saved_session_data} i, {rcpar_cp_ipq_map} map
      WHERE i.uid = :uid
      AND map.quiz_id = :quizid
      AND i.ipq_saved_sessions_id = map.ipq_saved_sessions_id
      ", array(':uid' => $this->user->uid, ':quizid' => $quiz_id))->fetchAllAssoc('id');
  }

  /**
   * Returns TRUE if the user has created an IPQ session for the given quiz ever before.
   *
   * @param int $quiz_id
   * - Id of the cp_quiz entity
   * @return bool
   */
  function sessionExistsForQuiz($quiz_id) {
    $count = db_query("SELECT count(*) 
      FROM rcpar_cp_ipq_map map, ipq_saved_session_data i
      WHERE i.uid = :uid
      AND map.quiz_id = :quizid
      AND i.ipq_saved_sessions_id = map.ipq_saved_sessions_id
      LIMIT 1
    ", array(':uid' => $this->user->uid, ':quizid' => $quiz_id))->fetchField();

    return $count > 0;
  }

  /**
   * For a given user and quiz, returns only the last session's  data (ipq_saved_session_data) that the user has
   * generated for the given quiz.
   *
   * @param int $quiz_id
   * - Id of the cp_quiz entity
   * @return array
   * - Returns an array of stdclass objects. Each object contains all of the fields from the ipq_saved_session_data table.
   * There will only be ONE object in the array.
   */
  function getLatestSessionDataForQuiz($quiz_id) {
    $id = $this->getMostRecentSessionId($quiz_id);

    return db_query("SELECT i.id, i.ipq_saved_sessions_id, i.updated_on, i.is_correct FROM {ipq_saved_session_data} i, {rcpar_cp_ipq_map} map
      WHERE i.uid = :uid
      AND map.quiz_id = :quizid
      AND i.ipq_saved_sessions_id = map.ipq_saved_sessions_id
      AND i.ipq_saved_sessions_id = :id
      ", array(':uid' => $this->user->uid, ':quizid' => $quiz_id, ':id' => $id))->fetchAllAssoc('id');
  }

  /**
   * Delete all related IPQ session data that this user has for the given quiz.
   * @param int $quiz_id
   * - Quiz entity id
   * @param int $sessionId
   * - The ID of an IPQ session. When specified, this will limit deletion to just this
   * specific session.
   */
  function deleteSessionDataForQuiz($quiz_id, $sessionId = NULL) {
    if(is_null($sessionId)) {
      // Query for all session IDs for this quiz, by this user.
      $sesIdsResult = db_query("SELECT sess.id
      FROM {ipq_saved_sessions} sess, {rcpar_cp_ipq_map} map
      WHERE map.quiz_id = :quizid
      AND map.ipq_saved_sessions_id = sess.id
      AND sess.uid = :uid", array(':quizid' => $quiz_id, ':uid' => $this->getDrupalUser()->uid));
    }
    else {
      // Put a single row with the session ID into our result array
      $row = new stdClass();
      $row->id = $sessionId;
      $sesIdsResult = array($row);
    }

    // Delete all rows matching this session id from the relevant IPQ tables
    foreach ($sesIdsResult as $row) {
      db_query("DELETE FROM {ipq_session_overview} WHERE ipq_saved_sessions_id = :id", array(':id' => $row->id));
      db_query("DELETE FROM {ipq_saved_session_data} WHERE ipq_saved_sessions_id = :id", array(':id' => $row->id));
      db_query("DELETE FROM {rcpar_cp_ipq_map} WHERE ipq_saved_sessions_id = :id", array(':id' => $row->id));
      db_query("DELETE FROM {ipq_saved_sessions} WHERE id = :id", array(':id' => $row->id));
    }
  }

  /**
   * For a given quiz, returns TRUE if the user has finished that quiz.
   *
   * @param $quiz_id
   * - Id of the cp_quiz entity
   * @return bool
   */
  function quizHasBeenFinished($quiz_id) {
    $hasData = db_query("SELECT finished FROM {ipq_saved_sessions} i, {rcpar_cp_ipq_map} map
      WHERE i.uid = :uid
      AND map.quiz_id = :quizid
      AND i.id = map.ipq_saved_sessions_id
      LIMIT 1
      ", array(':uid' => $this->user->uid, ':quizid' => $quiz_id))->fetchAllAssoc('finished');

    foreach ($hasData as $key => $value) {
      return $value->finished == 1;
    }
  }

  /**
   * For a given quiz, returns TRUE if quiz is resumable
   *
   * @param $quiz_id
   * - Id of the cp_quiz entity
   * @return bool
   */
  function isQuizResumable($quiz_id) {
    $q = new CPQuiz($quiz_id);

    // A quiz can't be resumable if it's not open
    if(!$q->isOpen()) {
      return FALSE;
    }

    // Check if the quiz has the timer option checked.
    $timerActive = $q->isTimerActive();

    // Check if the user has started the quiz
    $isStarted = sizeof($this->getSessionDataForQuiz($quiz_id)) > 0;

    // Check if the quiz is finished.
    $isFinished = $this->quizHasBeenFinished($quiz_id);

    return $isStarted && !$isFinished && !$timerActive;
  }

  /**
   * Set the finished flag of the session
   *
   * @param $session_id
   * - Id of the session

   */
  function setFinishedFlag($session_id) {
    $update = db_update('ipq_saved_sessions')
      ->fields(array(
        'finished' => 1
      ))
      ->condition('id', $session_id)
      ->execute();
  }

  /**
   * Returns TRUE if the consumer has taken the given quiz AND the quiz configuration allows them to see the results.
   *
   * @param $quiz_id
   * @return bool
   */
  function quizResultsAvailableToConsumer($quiz_id) {
    $q = new CPQuiz($quiz_id);
    if($q->isLoaded()) {

      // If the consumer has not taken this quiz, they can't see results.
      if(sizeof($this->getSessionDataForQuiz($quiz_id)) == 0) {
        return FALSE;
      }

      try {
        switch($q->getResultsDisclosureType()) {
          // As soon as they finish the quiz.
          case 'student_finished' :
            // If the student has any quiz data for this quiz, they can see results.
            // @todo - Need to ensure that the quiz is finished so the user can't peak at the results while taking it.
            // This may not actually be possible though.
            return TRUE;
            break;

          // When the quiz reaches its close date.
          case 'quiz_close_date':
            if(REQUEST_TIME > $q->getCloseDate()->getTimestamp()) {
              return TRUE;
            }
            break;

          // Beginning on this date:
          case 'quiz_specific_date':
            if(REQUEST_TIME > $q->getResultsAvailableDate()->getTimestamp()) {
              // It's after the date specified to reveal results, so we can show them.
              return TRUE;
            }

            break;
        }
      }
      catch (EntityMetadataWrapperException $e) {
        // In the event of an exception, we'll end up returning FALSE in the end.
      }
    }

    return FALSE;
  }

  /**
   * @return string
   * - Returns a concatenation of first and last name if available, otherwise defaults to username.
   */
  public function getDisplayName() {
    $account = $this->user;
    try {
      // Try to get first and last
      $u_wrapper = entity_metadata_wrapper('user', $account);
      $display_name = $u_wrapper->field_first_name->value() . ' ' . $u_wrapper->field_last_name->value();
    }
    catch (EntityMetadataWrapperException $exc) {
      // Default to username
      $display_name = $account->name;
    }
    if (empty(trim($display_name))) { // First & Last name are blank, default to username
      $display_name = $account->name;
    }
    return $display_name;
  }

  /**
   * Given a quiz_id, get the most recent ipq session ID for that quiz
   * @param $quiz_id
   * @return int|bool
   * - Returns the ID if any session has been created, otherwise returns FALSE
   */
  function getMostRecentSessionId($quiz_id) {
    // find this user's session id for this quiz
    // todo: consider saving the uid to the rcpar_cp_ipq_map (& a unique index on ipq_saved_sessions_id+uid)
    // NOTE: this will need to change if we allow students to retake the same quiz
    $query = db_query("SELECT rcim.ipq_saved_sessions_id
                    FROM {rcpar_cp_ipq_map} rcim
                    INNER JOIN {ipq_saved_sessions} iss ON iss.id = rcim.ipq_saved_sessions_id
                    WHERE rcim.quiz_id = :qid
                    AND iss.uid = :uid
                    ORDER BY rcim.ipq_saved_sessions_id DESC
                    LIMIT 1",
      array(':qid' => $quiz_id, ':uid' => $this->user->uid)
    );
    return $query->fetchField();
  }

  /**
   * Get all published partners that have 'ACT' enabled that this user is an admin of.
   * Note: This is very similar to rcpar_partners_get_partner_admins_all() with the
   * exception of the check for ACT checkbox.
   * @return array
   * - Numeric array of partner node ids.
   */
  public function getCreatorsACTEnabledPartners() {
    $uid = $this->user->uid;

    $partner_nids = array();

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'partners')
      ->propertyCondition('status', 1)
      ->fieldCondition('field_user_account', 'target_id', check_plain($uid), '=')
      ->fieldCondition('field_rcpar_cp_ic_access', 'value', 1, '=');
    $result = $query->execute();

    if (isset($result['node'])) {
      $partner_nids = array_keys($result['node']);
    }
    return $partner_nids;
  }

  /**
   * Get all of this creator's email groups. Groups come from ACT enabled partners.
   *
   * @param bool $all_group_info
   * - When TRUE, all fields of the email group will be returned. When FALSE,
   * only the group names will be present in the array.
   *
   * @return array
   * - Returns an array group names keyed by numeric group id.
   */
  public function getCreatorsACTEnabledEmailGroups($all_group_info = FALSE) {
    $email_group_select_options = array();

    foreach ($this->getCreatorsACTEnabledPartners() as $partner_id) {
      $p = new RCPARPartner($partner_id);
      $partner_groups = $p->getEmailGroups();

      foreach ($partner_groups as $group) {
        if($all_group_info) {
          $email_group_select_options[$group['id']] = $group;
        }
        else {
          $email_group_select_options[$group['id']] = $group['group_name'];
        }
      }
    }

    return $email_group_select_options;
  }

  /**
   * Intended to be used as a FAPI #options array with optgroups.
   * Get all of this creator's email groups grouped by partner, with a separate grouping for inactive
   * groups.
   *
   * @return array
   * - Returns an array (keyed by partner name) of arrays which are group names keyed by numeric group id.
   * The first level of the array is sorted by partner name alphabetically.
   */
  public function getCreatorsACTEnabledEmailGroupsFormOptions() {
    $email_group_select_options = array();

    // Organize the groups by partner, separating active and inactive groups
    foreach ($this->getCreatorsACTEnabledEmailGroups(TRUE) as $group) {
      $p = new RCPARPartner($group['partner_nid']);

      // Active Group
      if($group['create_date'] <= REQUEST_TIME && $group['end_date'] >= REQUEST_TIME) {
        $email_group_select_options[$p->getNameToDisplay()][$group['id']] = $group['group_name'];
      }
      // Inactive group (may be expired OR not yet started
      else {
        $email_group_select_options[$p->getNameToDisplay() . ' (Inactive)'][$group['id']] = $group['group_name'];
      }
    }

    // Sort by partner name (case insensitive). To do this, we create a sorting array of all lowercase values from
    // the group keys, which is where the partner names are. SORT_FLAG_CASE seemed like it should've done this,
    // but it doesn't.
    array_multisort(array_map('strtolower', array_keys($email_group_select_options)), SORT_ASC, SORT_NATURAL, $email_group_select_options);

    return $email_group_select_options;
  }

  /**
   * @return stdClass
   * - Drupal user object
   */
  public function getDrupalUser() {
    return $this->user;
  }

  /**
   * Determine if the user has the ability to edit the given quiz
   * @param CPQuiz $q
   *
   * @return bool
   */
  public function hasEditAccessForQuiz(CPQuiz $q) {
    // ASL quizzes are assigned to a particular email group
    if ($q->getQuizType() == 'act_asl') {

      // Check if this instructor has access to the student group
      $email_group_id = $q->getStudentGroup();

      // Unassigned quiz - access is granted if the user authored this quiz
      if($email_group_id == "0") {
        return $q->getAuthorUid() == $GLOBALS['user']->uid;
      }
      // Assigned quiz
      else {
        $my_student_groups = $this->getCreatorsACTEnabledEmailGroups();
        if (array_key_exists($email_group_id, $my_student_groups)) {
          return TRUE;
        }
      }

    }
    // It's a publisher quiz, access is determined by user permissions
    else {
      return rcpar_cp_access_sample_creator($this->getDrupalUser());
    }

    return FALSE;
  }
}
