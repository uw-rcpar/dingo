<?php

/**
 * Class CPQuiz
 * Wraps properties and functionality of the cp_quiz (a bundle of the cp_question_group entity type)
 */
class CPQuiz {
  private $isLoaded = FALSE;
  protected $quiz;
  private $wrapper;
  private $startDate;
  private $closeDate;

  /** @var  ActSampleProduct $actSampleProduct */
  protected $actSampleProduct;

  public $ipq_session_id = NULL;

  /**
   * CPQuiz constructor.
   * @param $quiz_id_or_entity_or_ipq_session_id
   * - Either a quiz id, a loaded quiz entity, or when $load_from_session is TRUE, may be an IPQ session id
   * @param bool $load_from_session
   * - When TRUE, we expect an IPQ session ID
   */
  function __construct($quiz_id_or_entity_or_ipq_session_id, $load_from_session = FALSE) {
    if($load_from_session) {
      $this->ipq_session_id = $quiz_id_or_entity_or_ipq_session_id;
      $quiz_id_or_entity_or_ipq_session_id = $this->getQuizIdFromIPQSessionID($this->ipq_session_id);

      // If we couldn't get a quiz ID, bail out
      if(empty($quiz_id_or_entity_or_ipq_session_id)) {
        return;
      }
    }

    // If an ID is supplied, load the entity
    if (is_numeric($quiz_id_or_entity_or_ipq_session_id)) {
      $quiz = entity_load_single('cp_question_group', $quiz_id_or_entity_or_ipq_session_id);
    }
    // Otherwise assume we've been provided a loaded entity
    else {
      $quiz = $quiz_id_or_entity_or_ipq_session_id;
    }

    if (is_object($quiz)) {
      $this->quiz = $quiz;
      $wrapper = entity_metadata_wrapper('cp_question_group', $quiz);
      $this->wrapper = $wrapper;

      $this->isLoaded = TRUE;

      // Set quiz dates
      $quiz_date = $this->getFieldValue('field_cp_dates');
      if (!empty($quiz_date)) { // they haven't been set yet
        $dtz = new DateTimeZone($quiz_date['timezone']);
        // NOTE: $dtz is required, if not used the timezone of the server is assumed
        $this->startDate = new DateTime($quiz_date['value'], $dtz);
        $this->closeDate = new DateTime($quiz_date['value2'], $dtz);
      }
    }
  }

  /**
   * Returns the entity ID of the loaded quiz.
   * @return int|bool
   * - Will return false if an ID cannot be retrieved.
   */
  function getId() {
    try {
      return $this->wrapper->getIdentifier();
    }
    catch (EntityMetadataWrapperException $e) {
      // Unexpected error condition
    }

    return FALSE;
  }

  /**
   * Gets the title of the quiz
   */
  public function getTitle() {
    try {
      return $this->quiz->title;
    }
    catch (EntityMetadataWrapperException $e) {
      // Unexpected error condition
    }
    return '';
  }

  /**
   * Gets the uid of the quiz author
   */
  public function getAuthorUid() {
    try {
      return $this->quiz->uid;
    }
    catch (EntityMetadataWrapperException $e) {
      // Unexpected error condition
    }
    return 0;
  }

  /**
   * Gets a quiz_id given a corresponding IPQ session ID
   * @param $session_id
   * - A sesion ID from
   * @return mixed
   */
  function getQuizIdFromIPQSessionID($session_id) {
    return db_query("
      SELECT quiz_id 
      FROM {rcpar_cp_ipq_map} map 
      WHERE map.ipq_saved_sessions_id = :sessionid
      LIMIT 1", array(':sessionid' => $session_id))->fetchField();
  }

  /**
   * returns whether a quiz has IPQ sessions (used to warn instructor when attempting to edit)
   * @return boolean
   */
  public function hasIPQSessions() {
    // gets an array of one object with one prop (quiz_id) of the first session for this quiz
    // it'll be an empty array if there are no sessions
    $first_sess = db_query("SELECT map.quiz_id
                            FROM {rcpar_cp_ipq_map} map 
                            WHERE map.quiz_id = :quizid
                            LIMIT 1",
      array(':quizid' => $this->getId()))->fetchAll();

    return count($first_sess);
  }

  /**
   * Get's questions attached to this quiz
   */
  public function getQuestions() {
    $this->getFieldValue('field_cp_question', array());
  }

  /**
   * @return DateTime
   */
  public function getStartDate() {
    return $this->startDate;
  }

  /**
   * @return DateTime
   */
  public function getCloseDate() {
    return $this->closeDate;
  }

  /**
   * @return DateTimeZone
   */
  public function getTimezone() {
    if (!empty($this->startDate)) {
      return $this->startDate->getTimezone();
    }
    else {
      return NULL;
    }
  }

  /**
   * Returns TRUE when the quiz is published (not draft) & current time is after the start date and before the close date.
   * @return bool
   */
  public function isOpen() {
    if (
      $this->isPublished() &&
      REQUEST_TIME >= $this->getStartDate()->getTimestamp() &&
      REQUEST_TIME < $this->getCloseDate()->getTimestamp()
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns TRUE when the quiz is published (not draft) & is start time for the quiz is still in the future.
   * @return bool
   */
  public function notYetStarted() {
    $result = (
      $this->isPublished() &&
      REQUEST_TIME <= $this->getStartDate()->getTimestamp()
    );
    return $result;
  }

  /**
   * Returns TRUE when the quiz close date is in the past
   * @return bool
   */
  public function isClosed() {
    return REQUEST_TIME >= $this->getCloseDate()->getTimestamp();
  }

  public function isLoaded() {
    return $this->isLoaded;
  }

  /**
   * Returns whether the quiz is published, if not it's a draft
   * @return boolean 1: published, 2: draft
   */
  public function isPublished() {
    return $this->getFieldValue('field_cp_quiz_status');
  }

  /**
   * Return TRUE if timer option is checked
   */
  public function isTimerActive() {
    return $this->getFieldValue('field_cp_quiz_timer_active');
  }

  /**
   * @return string
   * - As soon as the student has finished the quiz:
   *  'student_finished'
   * - When the quiz reaches its close date:
   *  'quiz_close_date'
   * - Beginning on this date:
   *  'quiz_specific_date'
   */
  public function getResultsDisclosureType() {
    $w = $this->wrapper;
    try {
      return $w->field_cp_quiz_show_results->value();
    }
    catch (EntityMetadataWrapperException $e) {}
    // Unexpected error condition - default to:
    return 'student_finished';
  }

  /**
   * @return DateTime
   */
  public function getResultsAvailableDate() {
    $w = $this->wrapper;
    try {
      // For some inexplicable reason, $w->field_cp_quiz_show_results_date->value(); is returning a timestamp instead of
      // an array with a string time and timezone (like field_cp_dates does). Maybe because an end date isn't collected?
      // So we read the raw data
      $node = $w->value();
      if(!empty($node->field_cp_quiz_show_results_date)) {
        // NOTE: $this->getTimezone is required, quiz setup always sets
        // field_cp_quiz_show_results_date using the same timezone as the start & end dates of the quiz
        // if not used the timezone of the server is assumed
        $date_string = $node->field_cp_quiz_show_results_date['und'][0]['value'];
        return new DateTime($date_string, $this->getTimezone());
      }
    }
    catch (Exception $e) {}
    // Unexpected error condition - default to now:
    return new DateTime();
  }

  /**
   * Get the quiz results type
   * @return string 'summary' or 'detailed'
   */
  public function getResultsType() {
    $w = $this->wrapper;
    try {
      return $w->field_cp_quiz_results_type->value();
    }
    catch (EntityMetadataWrapperException $e) {
      watchdog(
        'rcpar_cp', 'CPQuiz.class could not get results type for quiz ' . $quiz_id, NULL, WATCHDOG_ERROR
      );
      // default to the most conservative setting
      return 'summary';
    }
  }

  /**
   * Helper function to get the value of a field on the entity from the wrapper.
   * This function allows us to avoid the boilerplate of checking for the wrapper, supplying a fallback default,
   * and handling EntityMetadataWrapperException exceptions.
   *
   * @param string $fieldname
   * - A field name on the node
   * @param bool $default
   * - Default value to return if there is any problem getting the value (an EntityMetadataWrapperException for example)
   * @return mixed
   * - Will return whatever value the field stores, which could be just about anything.
   */
  protected function getFieldValue($fieldname, $default = FALSE) {
    if ($wrapper = $this->wrapper) {
      try {
        return $wrapper->{$fieldname}->value();
      }
      catch (EntityMetadataWrapperException $e) {
        watchdog(__CLASS__, 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      }
    }
    return $default;
  }

  /**
   * Helper function to set the value of a field on the entity from the wrapper.
   * This function allows us to avoid the boilerplate of checking for the wrapper
   * and handling EntityMetadataWrapperException exceptions.
   *
   * @param string $fieldname
   * - A field name on the entity
   * @param mixed $value
   * - value to set the entity field to
   * @param boolean $do_save
   * - if FALSE don't save the entity (to save a little time when updating multiple fields)
   *
   * @return boolean|integer
   * - if successful the entity_id (or TRUE if $do_save == FALSE)
   * - FALSE if there is any problem setting the value (an EntityMetadataWrapperException for example)
   */
  protected function setFieldValue($fieldname, $value, $do_save = TRUE) {
    $success = FALSE; // assume failure
    if ($wrapper = $this->wrapper) {
      try {
        $wrapper->{$fieldname} = $value;
        $success = TRUE;
        if ($do_save) {
          $wrapper->save();
          $success = $wrapper->id->value();
        }
        if ($fieldname == 'field_cp_dates') {
          // If we are changing the quiz dates, we need to update our class values
          $dtz = new DateTimeZone($value['timezone']);
          // NOTE: $dtz is required, if not used the timezone of the server is assumed
          $this->startDate = new DateTime($value['value'], $dtz);
          $this->closeDate = new DateTime($value['value2'], $dtz);
        }
      }
      catch (EntityMetadataWrapperException $e) {
        watchdog(__CLASS__, 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      }
    }
    return $success;
  }


  /**
   * Get this quiz's student group
   * @return integer the id of the student group
   */
  public function getStudentGroup() {
    return $this->getFieldValue('field_cp_student_group');
  }

  /**
   * Get the quiz state from the admin users perspective (not from the quiz consumer perspective)
   * @return string indicating quiz state ('launched', 'scheduled', 'closed' or 'draft')
   */
  public function getQuizAdminState() {
    if ($this->getQuizType() == 'act_pal') {
      return 'launched';
    }
    if (!$this->isPublished()) {
      return 'draft';
    }
    if ($this->isOpen()) {
      $state = 'launched';
    }
    else {
      if ($this->notYetStarted()) {
        $state = 'scheduled';
      }
      else {
        $state = 'closed';
      }
    }
    return $state;
  }

  /**
   * Get the value of changed property as a DateTime
   * return DateTime
   */
  public function getChangedDate() {
    $changed = $this->getFieldValue('changed');
    $changedDate = date_create_from_format('U', $changed);
    $dtz = $this->getTimezone();
    if (!empty($dtz)) { // actually should always be set...
      date_timezone_set($changedDate, $dtz);
    }
    return $changedDate;
  }

  /**
   * Returns 'act_pal' if no student field group is set, otherwise 'act_asl'
   * @return string
   * //TODO: DON'T TRUST THIS, SHOULD BE SET SPECIFICALLY AT THE BEGINNING OF THE FORM BUILDER
   */
  public function getQuizType() {
    if (is_null($this->getFieldValue('field_cp_student_group'))) {
      return 'act_pal';
    }
    else {
      return 'act_asl';
    }
  }

  public function setACTSampleProduct(ActSampleProduct $asp) {
    $this->actSampleProduct = $asp;
  }

  /**
   * Get the ACT Sample Material Product this quiz is related to (Publisher quizzes only)
   *
   * @return bool|ActSampleProduct
   * - FALSE if not a publisher quiz or product is not found, otherwise a loaded instance of ActSampleProduct
   */
  public function getACTSampleProduct() {
    // If an act sample product has been set explicitly already, return it so that we don't have to query.
    if(!empty($this->actSampleProduct)) {
      return $this->actSampleProduct;
    }

    if (!$this->getQuizType() == 'act_pal') {
      return FALSE;
    }
    $query = new EntityFieldQuery();

    // This is a query for ACT sample products that reference this quiz
    $result = $query->entityCondition('entity_type', 'commerce_product')
      ->entityCondition('bundle', 'act_sample_materials')
      ->fieldCondition('field_quizzes_ref', 'target_id', array($this->getId()), 'IN')// Question mpool matches
      // Run the query as user 1
      ->addMetaData('account', user_load(1))
      ->execute();

    // If we found the product, load an ActSampleProduct and return it.
    // NOTE: Currently publisher quizzes are referenced by one ACT Sample Product.
    // It's possible that in the future, this quiz could be assigned to more than
    // one product, this would only return the first one.
    if (!empty($result['commerce_product'])) {
      reset($result['commerce_product']);
      $this->actSampleProduct = new ActSampleProduct(key($result['commerce_product']));
      return $this->actSampleProduct;
    }

    return FALSE;
  }

  /**
   * Returns a URL to the quiz setup page, which allows a user to take the quiz.
   * This URL is not prefixed with a leading slash, it is ready to be passed into l() or url()
   * @return string
   */
  public function getUrlQuizSetup() {
    // ASL quiz
    if ($this->getQuizType() == 'act_asl') {
      return RCPAR_CP_URL . '/quiz/' . $this->getId() . '/setup';
    }
    // PAL quiz
    else {
      return RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/content/' . $this->getACTSampleProduct()->getSku() . '/quiz/' . $this->getId() . '/setup';
    }
  }

  /**
   * Returns a URL to the quiz take page, which allows a user to take quiz in the active session.
   * This URL is not prefixed with a leading slash, it is ready to be passed into l() or url()
   * @return string
   */
  public function getUrlQuizTake() {
    // ASL quiz
    if ($this->getQuizType() == 'act_asl') {
      return RCPAR_CP_URL . '/quiz/take';
    }
    // PAL quiz
    else {
      return RCPAR_CP_URL . '/' . RCPAR_CP_PUB_URL . '/content/' . $this->getACTSampleProduct()->getSku() . '/quiz/take';
    }
  }

  /**
   * Used to assign an id when saving a draft version of a new quiz (in the CPQuizFormClass)
   * @return integer the id quiz, (0 if there is an error)
   */
  public function quizSave() {
    $quiz_id = 0; // assume failure
    if ($wrapper = $this->wrapper) {
      try {
        $wrapper->save();
        $quiz_id = $wrapper->id->value();
      }
      catch (EntityMetadataWrapperException $e) {
        watchdog(__CLASS__, 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      }
    }
    return $quiz_id;
  }

  /**
   * If this quiz object was instantiated from an IPQ session ID, we can get info about that session
   * @return array
   * - An IPQ session array. The array will be empty if there is no specific session loaded.
   */
  public function getIpqSession() {
    $ipq_session = array();
    if(!is_null($this->ipq_session_id)) {
      // The following code is from ipq_quiz_build_session()
      $ipq_session = db_query("select session_config from {ipq_saved_sessions} s WHERE id = :session_id ", array(":session_id" => $this->ipq_session_id))->fetchAssoc();
      $ipq_session['session_config'] = drupal_json_decode($ipq_session['session_config']);
    }

    return $ipq_session;
  }

  /**
   * If we have a specific IPQ session attached to this quiz object, determines if that session was
   * a quiz preview.
   * @return bool
   */
  public function isPreviewSession() {
    $ipq_session = $this->getIpqSession();

    return !empty($ipq_session['session_config']['preview']);
  }

  /**
   * Returns the URL for the page that the user should be taken to when they are:
   * 1. Done taking the quiz and no results are available
   * 2. Click 'Done' on the quiz results page
   * 3. Click 'Exit' after completing a review.
   *
   * This URL is different depending on the context that the quiz was taken in.
   * Most of the time, the quiz will go to the students quiz listing page, but if the
   * quiz was a preview by a professor, it should go to the professor's page.
   * If for some reason we don't know the context, the default would be the student's page.
   *
   * @return string
   * - A Drupal URL path
   */
  public function getQuizFinishedUrl() {
    return $this->isPreviewSession() ? RCPAR_CP_URL . '/quizzes' : RCPAR_CP_URL . '/student/quizzes';
  }

}