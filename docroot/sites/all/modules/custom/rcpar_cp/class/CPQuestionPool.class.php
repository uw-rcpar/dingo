<?php

class CPQuestionPool {
  private $isLoaded = FALSE;
  protected $pool;
  private $wrapper;

  function __construct($id_or_entity) {
    // If an ID is supplied, load the entity
    if (is_numeric($id_or_entity)) {
      $pool = entity_load_single('cp_question_group', $id_or_entity);
    }
    // Otherwise assume we've been provided a loaded entity
    else {
      $pool = $id_or_entity;
    }

    if (is_object($pool)) {
      $this->pool = $pool;
      $wrapper = entity_metadata_wrapper('cp_question_group', $pool);
      $this->wrapper = $wrapper;
      $this->isLoaded = TRUE;
    }
  }

  public function isLoaded() {
    return $this->isLoaded;
  }

  public function getId() {
    return $this->wrapper->getIdentifier();
  }

  /**
   * Get all the quizzes associated with this question pool
   * @return array
   * - An array of loaded cp_question_group (cp_quiz type) entities
   * @deprecated
   * - Quizzes will no longer be associated with question pools
   */
  function getQuizzes() {
    $query = new EntityFieldQuery();

    // Query for non-draft quizzes in the email groups this user belongs to
    $result = $query->entityCondition('entity_type', 'cp_question_group')
      ->entityCondition('bundle', 'cp_quiz') // Type: cp_quiz
      ->fieldCondition('field_cp_quiz_pools', 'target_id', array($this->getId()), 'IN') // Question mpool matches
      ->fieldCondition('field_cp_quiz_status', 'value', TRUE, '=') // Published quiz (not draft)
      ->execute();

    // If we found quizzes, return an array of loaded quiz entities indexed by id
    if (!empty($result['cp_question_group'])) {
      return entity_load('cp_question_group', array_keys($result['cp_question_group']));
    }

    // No quizzes found, return empty array
    return array();
  }

}