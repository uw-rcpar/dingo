<?php

/**
 * Class RCPARCommerceBundle
 * Helper functions to calculate values within course_package nodes that are functions of their contents.
 * For example, the price of a course package is the sum of all of the items it contains.
 *
 * IMPORTANT NOTE: we are handling two kind of bundles, the regular product bundles, associated to
 * a commerce display node and our custom partner bundles, associated to a particular partner node
 */
class RCPARCommerceBundle {
  // Internal flag to represent when the bundle has been successfuly loaded or not
  private $isLoaded;

  // Title of the bundle
  private $title;

  // Price of the bundle
  private $price;

  // Price of package discounts (will be a positive number)
  private $discounts;

  // List of products that are contained in the package
  private $reference_products;

  // Indicates if this is a bundle associated to a display node ("standard") or to a partner ("partner")
  private $bundle_type;

  // A node object associated with this bundle - a node of type 'partners'.
  private $associated_node;

  // The product ID/SKU of the actual bundle product (FULL, FULL-PREM, FULL-ELITE, in the case of partners it is always 'partner_package')
  private $bundle_product_id;
  private $bundle_sku;

  // Define bundle types
  const BUNDLE_TYPE_PARTNER = 'partner';
  const BUNDLE_TYPE_COURSE_PACKAGE = 'standard'; // Note: the word 'standard' is a legacy term before there was a difference between course package and general
  const BUNDLE_TYPE_GENERAL = 'general';

  /**
   * RCPARCommerceBundle constructor.
   *
   * Calculates the total price and total amount of discounts for a package and stores the information
   *
   * @param mixed $param
   * - May be a variety of identifiers for the entity that's associated with the bundle:
   * a.) (int|string) Product id (will be loaded). In the case of partners, 'partner_package' is used as the product id
   * and the partner will be loaded.
   * b.) (stdClass) Partner node object
   * c.) (stdClass) Product display node object
   * d.) (string) A product SKU
   */
  public function __construct($param) {
    try {
      // If 'partner_package' is passed in, we'll load the partner that's currently in the session
      if (is_string($param) && $param == 'partner_package') {
        $p = new RCPARPartner();
        $param = $p->getNode();
      }

      // For a numeric param, load a product by product id
      if (is_numeric($param)) {
        $product = commerce_product_load($param);
        if ($product !== FALSE) {
          $param = $product->sku;
        }
      }

      // Node of type 'partners'
      if (is_object($param) && $param->type == 'partners') {
        $this->bundle_type = self::BUNDLE_TYPE_PARTNER;
        $this->bundle_product_id = 'partner_package';
        $this->setPartner($param);
      }
      // @deprecated - Node of type 'product_display' - this indicates a course package, but this method of identifying the course package
      // is considered deprecated now.
      elseif (is_object($param) && $param->type == 'product_display') {
        $this->bundle_type = self::BUNDLE_TYPE_COURSE_PACKAGE;
        $this->setBundle(self::getCoursePackageSkuFromNid($param));
      }
      // A product SKU
      else {
        if (in_array($param, enroll_flow_get_course_package_skus())) {
          $this->bundle_type = self::BUNDLE_TYPE_COURSE_PACKAGE;
        }
        else {
          $this->bundle_type = self::BUNDLE_TYPE_GENERAL;
        }
        $this->setBundle($param);
      }
    }
    // Bundle is not valid
    catch (Exception $e) {
      //muting noise on the logs, this message is not necessary since is a way
      //to catch errors as it is on if statements.
      //watchdog('rcpar_commerce_bundle', $e->getMessage() . ' See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      return;
    }

    // If we made it here without throwing any exceptions, the bundle should have been successfully loaded
    $this->isLoaded = TRUE;
  }

  public function isLoaded() {
    return $this->isLoaded;
  }

  /**
   * @param $associated_node
   * @throws Exception
   */
  protected function setPartner($associated_node) {
    $partner_helper = new RCPARPartner($associated_node);

    if(!$partner_helper->isLoaded()) {
      throw new Exception('Invalid partner');
    }

    $products = $partner_helper->getProductsInfo();
    $reference_products = $products['bundle_parts'];
    $discount_aggregate = 0;

    // We need to add in the original retail prices of the references products in this package so they can be displayed
    // in the add to cart confirmation, shopping cart, and 'view package contents'
    foreach ($reference_products as $product_info) {
      // Get a product wrapper
      $product_entity = commerce_product_load($product_info['product_id']);
      $product_wrapper = entity_metadata_wrapper('commerce_product', $product_entity);

      // Load the retail product price and store it with our product info
      $price = $product_wrapper->commerce_price->value(); // $price['amount'] == 59500 (is actually 595.00), $price['currency_code'] == 'USD'
      $product_info['price'] = commerce_currency_format($price['amount'], $price['currency_code']);

      // If this is a discount, register the discount amount
      if ($price['amount'] < 0) {
        $discount_aggregate += ($price['amount'] * -1);
      }
    }

    // Store these properties in our object
    $this->associated_node = $associated_node; // Node id of the partner
    $this->title = $partner_helper->getNameToDisplay() . ' Custom Package';
    $this->price = commerce_currency_decimal_to_amount($partner_helper->getBundlePrice(), 'USD');
    // The partner discount is applied later, separately from other discounts that may be part of the bundle contents
    // so we need to add it in.
    $this->discounts = $discount_aggregate + $partner_helper->getPartnerDiscount();
    $this->reference_products = $reference_products;
  }


  /**
   * @todo - Document
   * @param $sku
   * @throws Exception
   */
  protected function setBundle($sku) {
    $price_aggregate = 0;
    $discount_aggregate = 0;
    $reference_products = array();

    // Load the bundle product and store its ID and SKU
    $bundle_product = commerce_product_load_by_sku($sku);
    if($bundle_product === FALSE) {
      throw new Exception('Invalid product');
    }
    $this->bundle_product_id = $bundle_product->product_id;
    $this->bundle_sku = $bundle_product->sku;

    // Get bundle info and determine if this is in fact a configured bundle
    $bundles = rcpar_commerce_bundle_get_bundles();
    if(!isset($bundles[$this->bundle_product_id])) {
      // todo - watchdog, etc
      throw new Exception('Bundle configuration not found');
    }

    // Add up prices and discounts - iterate through this bundle products
    foreach ($bundles[$this->bundle_product_id] as $bundled_item_id) {
      $product_entity = $product = commerce_product_load($bundled_item_id);
      $product_wrapper = entity_metadata_wrapper('commerce_product', $product_entity);

      // Load the product price and add it up
      $price = $product_wrapper->commerce_price->value(); // $price['amount'] == 59500 (is actually 595.00), $price['currency_code'] == 'USD'
      $price_aggregate += $price['amount'];
      // If this is a discount, register the discount amount
      if ($price['amount'] < 0) {
        $discount_aggregate += ($price['amount'] * -1);
      }

      // collect products info
      $reference_products[$product_entity->product_id] = array(
        'title'      => $product_entity->title,
        'price'      => commerce_currency_format($price['amount'], $price['currency_code']),
        'sku'        => $product_entity->sku,
        'product_id' => $product_wrapper->product_id->value(),
      );
    }

    // Store these properties in our object
    $this->title = $bundle_product->title;
    $this->price = $price_aggregate;
    $this->discounts = $discount_aggregate;
    $this->reference_products = $reference_products;
  }

  /**
   * Gets the node id for the node associated with this package (either product display or partner)
   */
  public function getAssociatedNode() {
    return $this->associated_node;
  }

  /**
   * Get the price of a course package.
   *
   * @return int
   * - Returns an UNFORMATTED commerce price, which means that for a price of $595.00 USD, 59500 will be returned, because
   * this is just how commerce stores the price. Format with commerce_currency_format()
   */
  public function getPrice() {
    return $this->price;
  }

  /**
   * Returns the name of the bundle as it should be displayed to the user
   * @return string
   *
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Returns the ID of the bundle product
   * @return int
   */
  public function getBundleProductId() {
    return $this->bundle_product_id;
  }

  /**
   * Returns the SKU of the bundle product
   * @return string
   */
  public function getBundleSku() {
    return $this->bundle_sku;
  }

  /**
   * Returns the price of the package if the package discount weren't applied
   *
   * @return int
   * - Returns an UNFORMATTED commerce price, which means that for a price of $595.00 USD, 59500 will be returned, because
   * this is just how commerce stores the price. Format with commerce_currency_format()
   */
  public function getValue() {
    return $this->getPrice() + $this->getDiscounts();
  }

  /**
   * Get the total amount of package discounts (as a positive integer)
   * The logic for calculating this for standard packages vs partner packages is different.
   *
   * @return int
   * - UNFROMATTED commerce price - use commerce_currency_format() to display.
   */
  public function getDiscounts() {
    // For partners, we show the discount as being the difference between the retail/consumer price and the partner price
    if($this->bundle_type == self::BUNDLE_TYPE_PARTNER) {
      $p = new RCPARPartner($this->getAssociatedNode());
      return $p->getRetailPrice() - $this->getPrice();
    }
    return $this->discounts;
  }

  /**
   * Get product information about the products in this bundle
   *
   * @return array
   * - An array of product information keyed by product id
   * $reference_products[$product_entity->product_id] = array(
   *   'title'      => $product_entity->title,
   *   'price'      => commerce_currency_format($price['amount'], $price['currency_code']),
   *   'sku'        => $product_entity->sku,
   *   'product_id' => $product_wrapper->product_id->value(),
   * );
   */
  public function getProducts() {
    return $this->reference_products;
  }

  /**
   * Returns TRUE if this bundle is associated with a partner
   * @return bool
   */
  public function isPartner() {
    return $this->bundle_type == self::BUNDLE_TYPE_PARTNER;
  }

  /**
   * Return the type of bundle that this is.
   * Possible values:
   * Partner package: BUNDLE_TYPE_PARTNER
   * Course package: BUNDLE_TYPE_COURSE_PACKAGE
   * General (flashcard bundle, etc): BUNDLE_TYPE_GENERAL
   * @return string
   */
  public function getBundleType() {
    return $this->bundle_type;
  }

  /**
   * Adds all of the products in this bundle to the current user's shopping cart when the bundle is a partner package.
   * This method is used internally only, the public addToCart() method will determine if it should be used.
   * @return bool
   * - Returns TRUE if no errors were encountered while adding products.
   */
  protected function addToCartPartner() {
    $partner_helper = new RCPARPartner($this->getAssociatedNode());

    // If the user selected the partner_package option, we need to collect the list of products to add to the cart
    $partner_name = $partner_helper->getNameToDisplay();
    $partner_products = $partner_helper->getProductsInfo();
    $list = variable_get('config_skus_cpa_options', array('AUD', 'FAR', 'BEC', 'REG'));
    $suffix = '-BK-2019';
    // Build line items for each product and add them to the cart
    $error = false;
    $line_item_data = array(
      'associated_node_id' => $partner_helper->getNid(), // Identifies the partner id that its part of
      'partner'            => TRUE, // Indicates that parent package is a partner - this has become redundant if we look at package or product_id
      'package'            => 'partner_package', // Identifies this product as being part of a package
      'product_id'         => 'partner_package', // Not a real product id, but this is used in the add to cart form to represent partner product id
      'package_name'       => t("@name Custom Package", array('@name' => ucfirst($partner_name)))
    );
    foreach ($partner_products['bundle_parts'] as $prod_info) {
      $product = commerce_product_load($prod_info['product_id']);

      // 1 and 0 are the default values for quantity and order_id
      $line_item = commerce_product_line_item_new($product, 1, 0, $line_item_data);

      // Add to current user's cart: if the user is not logged in ($user->uid: 0) Drupal Commerce manages the $_SESSION
      $line_item_added = commerce_cart_product_add($GLOBALS['user']->uid, $line_item);

      if (!$line_item_added) {
        $error = TRUE;
      }
      else{
        if (!$partner_helper->isBillingFreeAccess() && in_array($product->sku, $list)) {
          if (variable_get('enable_uworld_configurations', FALSE)) {
            $product = commerce_product_load_by_sku("{$product->sku}{$suffix}");

            // 1 and 0 are the default values for quantity and order_id
            $line_item = commerce_product_line_item_new($product, 1, 0, $line_item_data);

            // Add to current user's cart: if the user is not logged in ($user->uid: 0) Drupal Commerce manages the $_SESSION
            $line_item_added = commerce_cart_product_add($GLOBALS['user']->uid, $line_item);
            
          }
        }        
      }
    }

    // Add the partner discount - note that here we are just adding the discount, the actual discount price will be
    // calculated in rcpar_partners_commerce_cart_line_item_refresh hook
    $partner_discount_sku = "PARTNER-DIS";
    $product = commerce_product_load_by_sku($partner_discount_sku);
    $item = commerce_product_line_item_new($product, 1, 0, $line_item_data);
    commerce_line_item_save($item);
    commerce_cart_product_add($GLOBALS['user']->uid, $item);

    return !$error;
  }

  /**
   * Adds all of the products in this bundle to the current user's shopping cart
   * @return bool
   * - Returns TRUE if no errors were encountered while adding products.
   */
  public function addToCart() {
    // PARTNER PACKAGE
    if($this->isPartner()) {
      return $this->addToCartPartner();
    }

    // NON-PARTNER PACKAGE
    $course_package_node_info = rcpar_commerce_bundle_get_sku_mapping();

    // The product is one of our custom bundles, we add the corresponding packages and return
    // note that on this case the default code of commerce_cart_add_to_cart_form_submit won't be executed
    $line_item_data = array(
      // @todo - associated_node_id is deprecated for course packages, but I left it in to cover one legacy use case in
      // rcpar_commerce_bundle_commerce_coupon_granted_amount_alter (code that has not been merged as of this writing)
      // Leaving the associated node id should prevent the coupon code from breaking when this is launched until it can
      // be refactored.
      'associated_node_id' => $course_package_node_info[$this->getBundleSku()]['nid'],
      'package'            => $this->getBundleSku(),
      'package_name'       => $this->getTitle(),
      'product_id'         => $this->getBundleProductId(),
    );
    foreach($this->getProducts() as $pid => $prod_info){
      try {
        $product = commerce_product_load($pid);
        $item = commerce_product_line_item_new($product, 1, 0, $line_item_data);
        commerce_line_item_save($item);
        $line_item_added = commerce_cart_product_add($GLOBALS['user']->uid, $item);

        if (!$line_item_added) {
          return false;
        }
      }
      catch(Exception $exc) {
        watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
        return false;
      }
    }

    return true;
  }

  /**
   * Add the bundle to the given order.
   *
   * @param EntityMetadataWrapper $order_wrapper
   * @return bool
   * - True on success, false otherwise.
   */
  public function addToOrder($order_wrapper) {
    // The product is one of our custom bundles, we add the corresponding packages and return
    // note that on this case the default code of commerce_cart_add_to_cart_form_submit won't be executed
    $line_item_data = $this->getLineItemMetaData();
    foreach ($this->getProducts() as $pid => $prod_info) {
      try {
        $product = commerce_product_load($pid);
        $line_item = commerce_product_line_item_new($product, 1, $order_wrapper->getIdentifier(), $line_item_data);
        commerce_line_item_save($line_item);

        // Save the incoming line item now so we get its ID.
        commerce_line_item_save($line_item);

        // Add it to the order's line item reference value.
        $order_wrapper->commerce_line_items[] = $line_item;
      }
      catch (Exception $exc) {
        watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Removes all of the products in this bundle to the current user's shopping cart
   */
  public function removeFromCart() {
    $order = commerce_cart_order_load($GLOBALS['user']->uid);
    // todo - probably should absorb this procedural function into this method.
    rcpar_checkout_remove_bundle_from_cart($order, $this->getBundleProductId());
  }

  /**
   * Checks products provided in the constructor.
   * Returns true when there is a single product ID that one of the three primary course packages "Select", "Premier",
   * or "Elite" as defined by rcpar_commerce_bundle_get_sku_mapping()
   * @return bool
   */
  public static function isProductIdCoursePackage($product_id) {
    $package_skus = enroll_flow_get_course_package_skus();

    // $course_products is an array of commerce_product objects keyed by product_id
    $course_products = commerce_product_load_multiple(array(), array('sku' => array($package_skus)));

    // If the product ID we've been provided matches the product id of a course package, return TRUE
    if (key_exists($product_id, $course_products)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns true if the product id is a partner package, course package, or generic bundle
   * @param $product_id
   * @return bool
   */
  public static function isProductIdBundle($product_id) {
    if($product_id == 'partner_package') {
      return TRUE;
    }

    $bundles = rcpar_commerce_bundle_get_bundles();
    return isset($bundles[$product_id]);
  }

  /**
   * Returns true if the product id is a bundle but is NOT a partner package or course package
   * @param $product_id
   * @return bool
   */
  public static function isProductGenericBundle($product_id) {
    // If it's a partner package, it's not generic
    if($product_id == 'partner_package') {
      return FALSE;
    }

    // If it's a course package, it's not genric
    if(RCPARCommerceBundle::isProductIdCoursePackage($product_id)) {
      return FALSE;
    }

    // As long as this product is in the bundle list, it must be a generic bundle since other bundle types have been
    // excluded
    return RCPARCommerceBundle::isProductIdBundle($product_id);
  }

  /**
   * @deprecated
   * If the given node id is a course package, returns its associated SKU. Otherwise, returns FALSE
   * @param $nid
   * - Node id of a course package product display
   * @return int|bool
   */
  public static function getCoursePackageSkuFromNid($nid) {
    foreach (rcpar_commerce_bundle_get_sku_mapping() as $info) {
      if($info['nid'] == $nid) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get the bundle line item meta data
   * @return array
   */
  public function getLineItemMetaData() {

    $line_item_data = array(
      'package' => $this->getBundleSku(),
      'package_name' => $this->getTitle(),
      'product_id' => $this->getBundleProductId(),
    );

    return $line_item_data;

  }
}
