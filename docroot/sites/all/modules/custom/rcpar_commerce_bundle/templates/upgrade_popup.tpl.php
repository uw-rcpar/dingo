<?php global $user?>
<div id="user-upgrade-now" class="modal form-completion-solution form-type-<?php print $form_type; ?>" data-backdrop="false">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="termsLabel" class="modal-title">
        Upgrade your Free Trial  Course
      </h3>
    </div><!-- /.modal-header -->
    <div class="modal-body">
      <span>
        Please confirm that you want to disable your free trial and upgrade to a paid course. Any existing progress will be reset.
      </span>
    </div>
    <div class="modal-footer">
      <button type="button" class="return-not-upgrade btn btn-default">NO</button>
      <button type="button" class="return-yes-upgrade btn btn-default">YES! UPGRADE ME.</button>
      <input type="hidden" value="<?php echo $user->uid?>" id="getuid" />
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal -->