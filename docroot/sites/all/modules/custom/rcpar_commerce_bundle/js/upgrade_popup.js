(function ($) {
  Drupal.behaviors.rcpar_upgrade_popup = {
    attach: function (context, settings) {
      $('.btn-add-to-cart', context).click(function(){
        if(!$('#user-upgrade-now').is(':visible')){
          $('#user-upgrade-now').modal('show');
        return false;
        }
      });
      $('.return-yes-upgrade', context).on('click', function(){
        var uid = $('#getuid').val();
        $.post( Drupal.settings.basePath + "delete-entitlements-freetrial/"+uid+"/nojs", function() {
          $('.btn-add-to-cart').trigger("click");
          $('#user-upgrade-now').modal('hide'); 
        });  
      });
      $('.return-not-upgrade', context).on('click', function(){
        $('#user-upgrade-now').modal('hide');
      });
    },
  };
}(jQuery));
