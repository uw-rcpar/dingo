<?php

/**
 * @file
 * Field handler to present a button to remove a line item or a bundle of line
 * items
 *
 * Important note: this is based on commerce_bundle_handler_field_edit_delete  handler
 */


class rcpar_cart_grouping_handler_field_edit_delete extends commerce_line_item_handler_field_edit_delete {

  /**
   * Returns the form which replaces the placeholder from render().
   */
  function views_form(&$form, &$form_state) {
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );

    // At this point, the query has already been run, so we can access the results
    // in order to get the base key value (for example, nid for nodes).
    foreach ($this->view->result as $row_id => $row) {

      // Extract field data containing entity information.
      $alias = $this->aliases['line_item_id'];
      $data =& $row->_field_data[$alias];
      $form_data =& $form[$this->options['id']][$row_id];

      // We are going to hide the remove button for the elements our custom package elements
      // except for the control row
      // (Only the control row of a bundle gets a 'Remove' button)
      if (isset($data['entity']->data['package'])) {
        // Only the control row of a bundle gets a 'Remove' button.
        $form_data['#line_item_id'] = $data['entity']->line_item_id;
        continue;
      }

      $form[$this->options['id']][$row_id] = array(
        '#type' => 'submit',
        '#name' => 'delete-line-item-' . $row_id,
        '#attributes' => array('class' => array('delete-line-item')),
        '#submit' => array_merge($form['#submit'], array('commerce_line_item_line_item_views_delete_form_submit')),
      );

      if ($data['entity_type'] == 'bundle_control') {
        $form_data['#value'] = t('Remove Bundle');
        continue;
      }

      $line_item_id = $this->get_value($row);
      $form_data['#value'] = t('Delete');
      $form_data['#line_item_id'] = $line_item_id;
    }
  }

  /**
   * Shopping cart submit handler - Handles the removal of a bundle or regular product from the cart view
   */
  function views_form_submit($form, &$form_state) {
    $order = commerce_order_load($form_state['order']->order_id);
    $field_name = $this->options['id'];

    $alias = $this->aliases['line_item_id'];

    foreach (element_children($form[$field_name]) as $row_id) {
      // Check for the removal of an item.
      if ($form_state['triggering_element']['#name'] == 'delete-line-item-' . $row_id) {
        $row_data = $this->view->result[$row_id]->_field_data[$alias];
        if ($row_data['entity_type'] == 'bundle_control') { // It's a bundle control row.

          // We need to get the package that this row represents
          $custom_package_name = NULL;
          if ($row_data['entity']->rcpar_bundle_config_id) {
            $custom_package_name = $row_data['entity']->rcpar_bundle_config_id;

            // Instantiate the bundle class and remove the whole bundle from the cart
            $bundle = new RCPARCommerceBundle($custom_package_name);
            if ($bundle->isLoaded()) {
              $bundle->removeFromCart();
            }

            // The stanza below should not really be needed, but the code will scan through all the rows in the view (e.g.
            // products in the cart) and remove them from the cart if they match the bundle of the control row that is
            // being removed. The bundle should have already been removed from the cart, but doing this could help us
            // cover some edge cases, such as if a bundle configuration was removed from the system and a user still had
            // the bundle in their cart, or the products in the bundle have been changed by an admin.
            foreach ($this->view->result as $row_index => $data) {
              if ($data->_field_data[$alias]['entity_type'] != 'bundle_control') {
                // If we are removing one of our RCPAR custom packages, we can see if they are from the same package comparing using data['package']
                if (isset($data->_field_data[$alias]['entity']->data['package']) && $data->_field_data[$alias]['entity']->data['package'] == $custom_package_name) {
                  $line_item_id = $form[$field_name][$row_index]['#line_item_id'];
                  commerce_cart_order_product_line_item_delete($order, $line_item_id);
                }
              }
            }
          }

          // Once we've found and operated on the triggering element, we can break the loop and be done
          break;
        }
        // A regular, non-bundled product is being removed from the cart
        else {
          $line_item_id = $form[$field_name][$row_id]['#line_item_id'];
          commerce_cart_order_product_line_item_delete($order, $line_item_id);
        }
      }
    }
  }
}
