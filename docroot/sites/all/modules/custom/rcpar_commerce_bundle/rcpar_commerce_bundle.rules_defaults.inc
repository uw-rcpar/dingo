<?php

/**
 * @file
 * Default rule configurations for rcpar_commerce_bundle
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rcpar_commerce_bundle_default_rules_configuration() {
  $configs = array();
  $rule = '{ "rules_commerce_couponprodref_validate_refererenced_products_01" : {
      "LABEL" : "Coupon Validation: Check the referenced products v 1",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules", "commerce_coupon", "rcpar_commerce_bundle" ],
      "ON" : { "commerce_coupon_validate" : [] },
      "IF" : [
        { "NOT php_eval" : { "code" : "return rcpar_commerce_bundle_rules_commerce_couponprodref_validate_refererenced_products($coupon, $commerce_order);" } }
      ],
      "DO" : [ { "commerce_coupon_action_is_invalid_coupon" : [] } ]
    }
  }';
  $configs['rules_commerce_couponprodref_validate_refererenced_products_01'] = rules_import($rule);
  return $configs;
}

