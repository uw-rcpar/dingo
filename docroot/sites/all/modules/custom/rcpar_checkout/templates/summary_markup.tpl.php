<div class="item-list-wrapper">

  <div class="item-count left">Items (<?php print $count; ?>)</div>
  <div class="item-count-value right"><?php print $subtotal; ?></div>

  <?php if ($display_promotions) { ?>
    <div class="total-promotions left">Total Promotions:</div>
    <div class="total-promotions-value right">- <?php print $promotions; ?></div>
  <?php } ?>

  <div class="subtotal-with-promotion left">Subtotal:</div>
  <?php if ($display_promotions) { ?>
    <div class="subtotal-with-promotion right"><?php print $subtotal_with_promotions; ?></div>
  <?php }
  else { ?>
    <div class="subtotal-without-promotion right"><?php print $subtotal; ?></div>
  <?php } ?>

  <div class="shipping left">Shipping:</div>
  <div class="shipping-value right"><?php print $shipping; ?></div>

  <div class="taxes left">Taxes:</div>
  <div class="taxes-value right"><?php print $tax; ?></div>

  <div class="order-total-wrapper">
    <div class="order-total left">Order total:</div>
    <div class="order-total-value right"><?php print $grandtotal; ?></div>
  </div>
  <?php
  if ($is_direct_bill) {
    print '<div class="direct-bill-note">NOTE: Your organization will be billed directly for this order.</div>';
  }
  if ($is_prepaid) {
    print '<div class="direct-bill-note">NOTE: Your organization has prepaid for this order.</div>';
  }
  ?>
  <div class="clear-empty"></div>

</div>