<div id="form-popup-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content messages commerce-add-to-cart-confirmation">
      <div class="modal-header message-inner">
        <button type="button" class="close commerce-add-to-cart-confirmation-close" data-dismiss="modal">&times;</button>
        <div class="added-product-title clearfix">
          <div class="cart-added">
            Product Details
            <span class="cart-modify">You can modify your order in <a href='/cart'>your cart</a>. </span>
          </div>
          <div class="cart-added-header"><span class="pull-left">PRODUCT</span><?php if (!$hide_prices) { ?><span class="pull-right">PRICE</span><?php } ?></div>
        </div>
      </div>
      <div class="modal-body message-inner view-content">
          <div class="line-item-heading">
            <table class="table-striped table-heading">
              <tbody>
              <tr class="odd views-row-first bundle-title">
                <td class="views-field-line-item-title"><?php print $bundle_title ?></td>
                <td class="views-field-commerce-total">
                  <?php
                  if (!$hide_prices) {
                    print $bundle_price;
                    if ($bundle_discount) {
                      print $bundle_discount;
                    }
                  } ?></td>
              </tr>
              </tbody>
            </table>
          </div>
          <div class="line-item-contents">
            <table class="table-striped">
              <tbody>
                <?php if(!empty($products)){
                foreach ($products as $product_data) { ?>
                <tr class="bundle-item">
                    <td class="views-field-line-item-title"><?php print $product_data['title'] ?> </td>
                    <td class="views-field-commerce-total"><?php print $product_data['price'] ?> </td>
                </tr>
                <?php } } ?>
              </tbody>
          </table>
          </div>
      </div>
    </div>

  </div>
</div>



