/* 
 * This is the same as commerce_add_to_cart_confirmation.js except except it adds
 * a .once wrapping to the code
 * to fix that multiple backgrounds were being added, due to calls from
 * rcpar_checkout_view_package_contents
 *
 * it replaces the commerce_add_to_cart_confirmation.js in rcpar_checkout_js_alter
 */
(function ($) {
  Drupal.behaviors.commerce_add_to_cart_confirmation_overlay = {
    attach: function (context, settings) {
      if ($('.commerce-add-to-cart-confirmation').length > 0) {
        // this .once wrapping is the only added code to the commerce_add_to_cart_confirmation.js version
        $('.commerce-add-to-cart-confirmation', context).once('rcparCheckoutCommerceAddToCartConfOverlay', function () {

          // Add the background overlay.
          $('body').append("<div class=\"commerce_add_to_cart_confirmation_overlay\"></div>");

          // Enable the close link.
          $('.commerce-add-to-cart-confirmation-close').live('click touchend', function (e) {
            e.preventDefault();
            $('.commerce-add-to-cart-confirmation').remove();
            $('.commerce_add_to_cart_confirmation_overlay').remove();
            $('.modal-backdrop').remove();
          });

        });
      }
    }
  }
})(jQuery);

