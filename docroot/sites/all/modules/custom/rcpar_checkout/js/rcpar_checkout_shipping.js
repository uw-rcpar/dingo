(function ($) {
  Drupal.behaviors.rcpar_checkout_shipping = {
    attach: function (context, settings) {
      // Re-enable the 'continue' button if there are shipping methods available for the user to choose
      if($('.form-item-commerce-shipping-shipping-service').length) {
        $("#pane-contents-shipping .checkout-continue").removeAttr("disabled");
      }

      // Disable the shipping pane's continue button when the shipping service is changed to prevent the user from
      // continuing while the recalculation is still loading.
      $(".form-item-commerce-shipping-shipping-service input", context).change(function () {
        $("#pane-contents-shipping .checkout-continue").attr("disabled", "disabled");
      });

      // Disable the shipping pane's continue button when 'recalculate shipping' is clicked to prevent the user from
      // continuing while the form is reloading. $.fn.commerceCheckShippingRecalculation also triggers the click event
      // so our code in enroll_flow.js that causes shipping to recalculate when
      $(".commerce_shipping button", context).click(function () {
        $("#pane-contents-shipping .checkout-continue").attr("disabled", "disabled");
      });
    }
  };
}(jQuery));
