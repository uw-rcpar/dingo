(function ($, Drupal, window, document, undefined) {
  $(document).ready(function () {
    // Hide the terms and conditions checkbox
    $('#terms-and-conditions').hide();

    // 2020 materials modal
    if($('#materials-popup-modal').length > 0) {
        $('#materials-popup-modal').modal();
    }

    // Disable place order button until validation passes
    $('#place-order-button button').attr('disabled', 'disabled');
    $('#place-order-wrapper').hide();

    // Bind to place order button click event - place the actual order by triggering the drupal ajax
    $('#place-order-button button').click(function(e){
      // We add some styling to the place order button to indicate that the order is being processed
      placeOrderBtnLoading(true);
      $('#pane-wrapper-checkout').find('.checkout-continue').trigger('mousedown');
    });

    // For directbill, hide the 'enter your payment information...' message
    if(Drupal.settings.rcparCheckout.billingType == 'directbill' || Drupal.settings.rcparCheckout.billingType == 'prepaid') {
      $('#place-order-message').hide();
    }

    // If the user has already entered data in all visible panes, we may be able to show the place order button
    validatePlaceOrder();

    // Bind to custom event 'paneScroll'
    $(document).delegate('body', 'paneScroll', function(e, data) {
      // If the pag_id is checkout, it means that the user has just closed the
      // shipping pane and gone to either the payment pane or checkout summary
      if (data.page_id == "checkout") {
        // Check to see if the user wants to use the same address for both
        // shipping and billing
        if ($('.commerce-customer-profile-copy input').prop('checked') == true) {
          // Clone the card details div before cloning the shipping data into
          // the billing summary, then prepend the card data into the billing
          // summary.
          var cardSummary = $('.card-summary').clone();
          $('.pseudo-summary').html($('#pane-contents-shipping').html());
          $('.pseudo-summary').prepend(cardSummary);
        }
      }

      // Scroll the screen to the top of the recently loaded section
      if($("#pane-wrapper-" + data.page_id).length){
        $('html, body').animate({scrollTop: $("#pane-wrapper-" + data.page_id).offset().top - 20}, 750);
      }

    });

    // Order Summary needs to scroll with window.
    //
    // Wrap cart summary wrapper and order summary wrapper
    // in a parent div because they both need to scroll as one.
    $('#cart-summary-wrapper,#order-summary-wrapper').wrapAll('<div id="cart-order-wrapper"></div>');

    // get the top position of the order cart summary wrapper div
    var order_summary_top = $('#cart-summary-wrapper').offset().top;

    // get the height of the order summary
    var order_summary_height = $('#cart-summary-wrapper').outerHeight() + $('#order-summary-wrapper').outerHeight();

      $(window).scroll(function() {
        // This only needs to happen if the window is more than 1023px wide,
        // because otherwise the order summary just stacks vertically on the page
        // and doesn't need to move.
        if ($(window).width() > 1023) {
          // Distance from top of footer to top of document
          footertotop = ($('#footer').position().top);
          // Distance user has scrolled from top, adjusted for height of order summary div
          // plus 40px top position and 20px space from footer (60)
          scrolltop = $(document).scrollTop()+order_summary_height+60;
          // Difference between the two
          difference = scrolltop-footertotop;
          // If user has scrolled further than footer,
          // pull sidebar up using a negative margin
          if (scrolltop > footertotop) {
            $('#cart-order-wrapper').css('margin-top',  0-difference);
          }
          else  {
            $('#cart-order-wrapper').css('margin-top', 0);
          }

          // If the window scrolls below the top position of the cart order wrapper div,
          // we need to add a class to reposition it
          if ($(window).scrollTop() >= order_summary_top) {
            $('#cart-order-wrapper').addClass('fixed-cart-order-wrapper');
          }
          else {
            // otherwise, remove the class
            $('#cart-order-wrapper').removeClass('fixed-cart-order-wrapper');
          }
        }
      });
  }); //END - document.ready

  /**
   * Adds (and remove) some styling to the place order button to indicate that the order is being processed
   * @param loading
   * - True to add the styling, false to remove it
   */
  var placeOrderBtnLoading = function(loading) {
    if(loading){
      $('#place-order-button button').append(' <i class="glyphicon glyphicon-refresh glyphicon-spin"></i>');
      $('#place-order-button button').attr('disabled', 'disabled');
      $('#place-order-button button').addClass('loading');
    }
    else {
      $('#place-order-button button .glyphicon-spin').remove();
      $('#place-order-button button').removeAttr('disabled');
      $('#place-order-button button').removeClass('loading');
    }
  };

  /**
   * Get's a wrapper to the last pane (from the user perspective, there might be other hidden panes)
   * @returns {*}
   */
  var getLastPaneWrapper = function(){
    // When on directbill mode, the last pane the user see might be the "Employment Information" pane
    // or the "Shipping" pane, depending on if the partner have some custom employment fields configured

    // note that we still have the "Payment" pane on the page, but it's hidden and it's manipulated
    // automatically by javascript
    if (Drupal.settings.rcparCheckout.billingType == 'directbill' || Drupal.settings.rcparCheckout.billingType == 'prepaid') {
      if ($('#pane-wrapper-employment_info').length > 0 ) {
        $paneWrapper = $('#pane-wrapper-employment_info');
      }
      else {
        // If #pane-wrapper-employment_info is not present, we should use the Shipping step as the last step^M
        $paneWrapper = $('#pane-wrapper-shipping');
      }
    }
    else {
      $paneWrapper = $('#pane-wrapper-checkout');
    }
    return $paneWrapper;
  }


  /**
   * Show a loading state for the given pane
   */
  var paneLoading = function(source) {
    var $paneWrapper = $(source).closest('.pane-wrapper');
    $paneWrapper.addClass('loading');
    $paneWrapper.find('.loader').remove();
    $paneWrapper.prepend('<div class="loader"></div>');
  };

  /**
   * Use this function to trigger custom events
   * @param event_name
   * - Custom event name
   */
  var triggerCustomEvent = function(event_name) {
    // Sometimes, we want to react to some events that we detect on the Behaviour attach method
    // but we want to execute our code after all the UI changes has been processed
    // to be able to do that we execute the triggering of the custom event
    // on a timeout handler to enque it at the end of the event queue
    // note that we are using a timeout interval of 0 milliseconds, so the event will be
    // triggered as soon as possible
    setTimeout(function runOnNextEvtLoopRun() {
      $('body').trigger(event_name);
    }, 0);
  };

  /**
   * Determine whether the final place order button should be clickable
   * @returns {boolean}
   */
  var validatePlaceOrder = function() {
    var $paneWrapper = getLastPaneWrapper();

    // There are some cases when this function is invoked while the order is being processed
    // on those cases, we just want to do nothing
    if($('#place-order-button button').hasClass('loading')){
      return;
    }

    // Hide and disable the place order button by default, we will show and re-enable based on criteria below
    $('#place-order-button button').attr('disabled', 'disabled');
    $('#place-order-wrapper').hide();

    // The conditions to determine if we should enable the place order options to the user
    // are different when we are on a direct bill, since we have the checkout pane hidden from the user
    // view and we manipulate it programmatically

    var should_enable_options = false;
    if(Drupal.settings.rcparCheckout.billingType == 'directbill' || Drupal.settings.rcparCheckout.billingType == 'prepaid') {
      // No (visible) panel should actively be edited
      if ($('.pane-wrapper.active').not('#pane-wrapper-checkout').length == 0 && $paneWrapper.hasClass('summary') && !$paneWrapper.hasClass('loading')) {
        should_enable_options = true
      }
    }
    else {
      // No panel should actively be edited and the last pane data is valid so the pseudo-summary is being shown
      if($('.pane-wrapper.active').length == 0 && $paneWrapper.find('.pseudo-summary:visible').length) {
        should_enable_options = true;
      }
    }

    if(should_enable_options) {
      // display the place order div
      $('#place-order-wrapper').show();

      // Show terms and conditions checkbox
      $('#terms-and-conditions').show();

      // Terms and conditions must be checked
      if ($('#terms-and-conditions-checkbox').prop('checked')) {
        // Enable the place order button!
        $('#place-order-button button').removeAttr('disabled');
      }
    }
  };


  Drupal.behaviors.rcparCheckout = {
    attach: function (context, settings) {
      // On some places, it might be handy to be notified when the shipping options are reloaded
      // so, we are going to detect that situation and generate a custom event
      // Note the indexOf usage, that is because sometimes the id comes as commerce-checkout-form-shipping
      // and others as commerce-checkout-form-shipping--2 (see drupal_html_id function)
      if($(context).attr('id') && $(context).attr('id').indexOf('commerce-checkout-form-shipping') == 0 || $(context).attr('id') == 'pane-wrapper-shipping') {
        triggerCustomEvent('rcpar-shipping-reloaded');
      }

      // Need to detect the reloading of the employment-info (mostly to detect when the user uploaded a file)
      // Note the indexOf usage (see above)
      if($(context).attr('id') && $(context).attr('id').indexOf('commerce-checkout-form-employment-info') == 0 ) {
        // This piece of code is executed before the new html is loaded into the DOM
        // so we execute as a timeout handler to make it run after this event is finished
        triggerCustomEvent('rcpar-form-employment-info-reloaded');
      }

      // We also need to know when the last pane was reloaded
      $paneWrapper = getLastPaneWrapper();
      if($(context).attr('id') == $paneWrapper.attr('id')) {
        triggerCustomEvent('rcpar-last-pane-loaded');
      }

      // And when the checkout pane was reloaded too
      if($(context).attr('id') == 'pane-wrapper-checkout') {
        triggerCustomEvent('rcpar-checkout-pane-loaded');
      }

      // Need to detect when the billing profile is turned on/off
      if ($(context).attr('id') == 'customer-profile-billing-ajax-wrapper') {
        // This piece of code is executed before the new html is loaded into the DOM
        // so we execute as a timeout handler to make it run after this event is finished
        triggerCustomEvent('rcpar-billing-profile-reloaded');
      }

      // Fire a generic handler for when attach behaviors is happening
      triggerCustomEvent('Drupal-attach');

      // We listen for the reload of the checkout pane
      // because if we submitted the order and something failed trying to place it
      // that pane will be reloaded and we need to re enable the place order btn and
      // remove some styling if that happens
      $('body').once('attach-events', function () {
        $(this).on('rcpar-checkout-pane-loaded', function () {
          // Remove the loading state from the place order button
          placeOrderBtnLoading(false);
          // Let's check if we can still show the place order button
          validatePlaceOrder();
        });
      });

      // Add a loader when a pane is submitted. Drupal's ajax framework is using mousedown and prevent's 'click' from triggering
      // so we need to bind to mousedown
      $('.pane-contents .form-submit', context).mousedown(function (e) {
        // Don't bind to the coupon form
        if ($(this).closest('.commerce_coupon').length) {
          return;
        }

        // Don't bind to the shipping recalculator
        if ($(this).closest('.commerce_shipping').length) {
          return;
        }

        // Don't bind to file upload form
        if ($(this).closest('.file-widget').length) {
          return;
        }

        paneLoading(this);
      });

      // Add a loader element class to a pane wrapper when it's edit or submit is clicked
      $('.pane-edit a', context).click(function (e) {
        paneLoading(this);
      });

      // Payment pane validation. For directbill, specific methods are overridden below to alter the behavior.
      PaymentValidation.init(context);


      // Validate the place order button when the t&c checkbox is changed, when a pane is submitted, when a pane is
      // edited, and when a pane is refreshed
      $('#terms-and-conditions-checkbox').change(validatePlaceOrder);
      $('.pane-contents .form-submit', context).mousedown(validatePlaceOrder);
      $('.pane-edit a', context).click(validatePlaceOrder);
      // Bind to custom event 'paneScroll' - this indicates a pane has just been refreshed
      $(document).delegate('body', 'paneScroll', function(e, data) {
        validatePlaceOrder();
      });

      // Using ajax instead of default page reload behaviour for the remove coupon link
      // note that we can't just use
      $('#commerce-checkout-coupon-ajax-wrapper .views-field-remove-from-order a', context).click(function(){
        var that = this;
        var url = $(this).attr('href');
        $(this).addClass('ajax-loading');
        $.ajax({
          url: url,
          complete: function (jqxhr, txt_status) {
            if (txt_status == 'success'){
              // we also need to update the order summary section
              // for that we call rcpar_checkout/get_order_summary
              // and also we send it the current form_build_id for it to update it
              var form_build_id = $('#commerce-checkout-form-checkout input[name=form_build_id]').val()
              $( "#order-summary-wrapper" ).load( "/rcpar_checkout/get_order_summary?form_build_id="+form_build_id, function() {
                // if everything is good, we remove the coupon from the coupon list
                $(that).closest('tr').hide();
              });
            }
          }
        });
        // return false to prevent browser to follow the link
        return false;
      });

      // For directbill, we need to hide the payment step
      // note that we are still "using" it, by manually triggering events on its buttons
      if(Drupal.settings.rcparCheckout.billingType == 'directbill' || Drupal.settings.rcparCheckout.billingType == 'prepaid') {
        // We need to hide the payment pane if it comes inside "context" variable

        var $paymentPaneWrapper = $('#pane-wrapper-checkout', context);
        // But we also need to do the same when it's the root element of 'context'
        if ($paymentPaneWrapper.length == 0){
          if($(context).attr('id') == 'pane-wrapper-checkout'){
            $paymentPaneWrapper = $(context);
          }
        }

        if ($paymentPaneWrapper.length){
          $paymentPaneWrapper.hide();
        }
      }

      // add an event listener for the Employee Verification upload field to automatically
      // press the upload button when the file is selected
      // NOTE: the '[id^="...' selector means the id begins with, used because on
      // subsequent ajax returns, Drupal add something like '--2' to the id
      if ($('input[id^="edit-commerce-fieldgroup-pane-group-custom-fields-field-employment-verification-und-0-upload"]', context).length) {
        $('input[id^="edit-commerce-fieldgroup-pane-group-custom-fields-field-employment-verification-und-0-upload"]', context).once('singlePageCheckoutEmpVerAutoUpload', function () {
          $('input[id^="edit-commerce-fieldgroup-pane-group-custom-fields-field-employment-verification-und-0-upload"]').change(function () {
            $('button[id^="edit-commerce-fieldgroup-pane-group-custom-fields-field-employment-verification-und-0-upload-button"]').mousedown();
          });
        });
      }

    }
  };


  // Payment pane validation
  var PaymentValidation = (function () {
    // locally scoped Object
    var self = {};

    // The wrapping div for the payment pane
    var $paneWrapper;

    // Track whether the user has begun the review step. We use this info to prevent the payment pane from being reloaded
    // which would cause info they've entered but not submitted to the server yet to be lost.
    self.reviewInitiated = false;

    /**
     * Bind to events and modify
     */
    self.init = function (context) {
      // Grab the last step pane wrapper (from user perspective)

      // When on directbill mode, the last pane the user see might be the "Employment Information" pane
      // or the "Shipping" pane, depending on if the partner have some custom employment fields configured

      // note that we still have the "Payment" pane on the page, but it's hidden and it's manipulated
      // automatically by javascript
      $paneWrapper = getLastPaneWrapper();
      self.$paneWrapper = $paneWrapper;

      // Initialize payment section by removing any buttons we created and extra classes we've added
      $paneWrapper.removeClass('affirm');
      $paneWrapper.find('.checkout-continue-affirm').remove();
      $paneWrapper.find('.checkout-continue-pseudo').remove();
      $paneWrapper.find('.place-order-message').remove();

      // If we can't find the checkout pane wrapper in the passed in context, abort
      if(!$paneWrapper.length) {
        return;
      }

      // Bind events for validation and workflow based on the selected payment type
      switch(self.detectPaymentType()) {
        case 'cc':
          // Attach jquery.creditCardValidator plugin to the credit card textfield
          self.creditCardValidation();

        case 'other':
          // Hide the real 'place order' button and inject a fake button that takes over to create the 'final review' step
          self.makePseudoLastStep();

          break;

        case 'affirm':
          // Add an 'affirm' class to help us with theming content in this pane when Affirm is selected
          $paneWrapper.addClass('affirm');

          // Hide the final review "Continue" button and make a new button to bind a click event to
          $paneWrapper.find('.checkout-continue-pseudo').hide();
          $paneWrapper.find('.place-order-message').remove();
          $paneWrapper.find('.checkout-continue')
            .hide()
            .after('<button class="checkout-continue-affirm btn btn-default btn-primary">Checkout with Affirm</button>');

          // On click,  trigger the real submit button for Drupal's
          $paneWrapper.find('.checkout-continue-affirm').click(function(e){
            e.preventDefault();

            // Trigger the submit on the real order form via Drupal's attached ajax handler. Drupal's response will
            // include inline javascript that redirects the user to the remote payment page
            $('#pane-wrapper-checkout').find('.checkout-continue').trigger('mousedown');
          });
          break;
      }
    };

    /**
     * Determine the currently selected payment type
     * We'll set self.paymentType as well as return a string value
     * @return string
     *  - 'cc', 'affirm', or 'other'
     */
    self.detectPaymentType = function() {
      var $paymentType = $('.form-item-commerce-payment-payment-method').find('input:checked');
      if($paymentType.val() == 'authnet_aim|commerce_payment_authnet_aim') {
        // credit card
        self.paymentType = 'cc';
      }
      else if($paymentType.val() == 'affirm|commerce_payment_affirm') {
        // Affirm
        self.paymentType = 'affirm';
      }
      else {
        // Direct bill or Free trial
        // Note: No use case for this variable setting right now, as the server side will indicate a Direct Bill
        self.paymentType = 'other';
      }

      return self.paymentType;
    };

    /**
     * Hide the real submit order button and inject a fake button that takes over the 'place order' button so we can add
     * a final review step before actually placing the order
     */
    self.makePseudoLastStep = function() {
      // Hide the place order button and make a facsimile
      $paneWrapper
        .find('.checkout-continue')
        .hide()
        .after('<div class="place-order-message">You\'ll still have a chance to review and edit your order before it\'s final.</div><button disabled="disabled" class="checkout-continue-pseudo btn btn-default btn-primary">Continue</button>');

      // Event binding: Enable the "Continue" button credit card # is valid and required fields are not empty
      self.validateLastStep();

      // Bind to our fake button's click event
      $paneWrapper.find('.checkout-continue-pseudo').click(function(e){
        e.preventDefault();


        if ($paneWrapper.attr('id') == 'pane-wrapper-checkout'){
          // Build and show the pane summary
          self.buildPaneSummary();

          // Show the Review info
          self.activateReviewStep();

          // Determine whether the final place order button should be clickable
          validatePlaceOrder();
        }
        else {
          // The difference between this case and the regular flow, is that
          // here we are submitting the last pane, and then we wait for
          // the submission to be finished before trying to enable the place order button
          $paneWrapper.find('.checkout-continue').trigger('mousedown');

          $('body').on('rcpar-last-pane-loaded', function(){
            if ($paneWrapper.find('.messages.error.alert').length == 0){

              // On this case, as we are actually submitting the last pane
              // we do not need to build the pane summary

              // Show the Review info
              self.activateReviewStep();

              // Determine whether the final place order button should be clickable
              validatePlaceOrder();
            }
          });
        }
      });
    };

    /**
     * Enable the review order "Continue" button last step fields are all valid
     * eg: button credit card # is valid and required fields are not empty
     * todo - validate that expiration is in the future?
     */
    self.validateLastStep = function() {
      // Bind to changes for any form input or select in the payment pane
      var handler = function(e) {
        var valid = false;

        // Return true for any element that is empty
        var removeFilledInputs = function(index) {
          if($(this).attr('type') == 'checkbox') {
            return !$(this).prop('checked');
          }
          else {
            if($(this).val() == '' || $(this).val() == '0') {
              return true
            }
          }
        };

        // The Directbill last step  its the "EMPLOYMENT INFORMATION" pane
        // while for the rest of payment types it's the "PAYMENT" pane
        // so we need to check for different fields when validating it
        if (Drupal.settings.rcparCheckout.billingType == 'directbill' || Drupal.settings.rcparCheckout.billingType == 'prepaid'){

          // For Directbill orders, we need to distinguish two cases
          // the case when the last step is the shipping step
          // of the case for when the last step is the employment information step

          if ($paneWrapper.attr('id') == 'pane-wrapper-shipping'){
            // last step is the shipping pane

            // In some cases, there may be no shippable products, so there is no shipping options possible. For
            // this scenario, we'll consider the pane valid and let the server ensure that required fields are filled
            // out.
            if($paneWrapper.find('.commerce_shipping').length == 0) {
              valid = true;
            }
            else {
              // There are shippable products in the order.
              // here, we only care that the user have selected the shipping option
              valid = $paneWrapper.find('.commerce_shipping input[checked=checked]').length > 0 ;
            }
          }
          else {
            // Last step is the employment information pane

            // We are going to create an array of inputs to validate the
            // required textboxes, selects and checkboxes of the pane
            // note that most of those fields can be enabled and disabled from the
            // admin UI for the partner, so some of the fields might not be present in the pane form
            var $validators = $();

            // Start Date
            if ($('.field-name-field-employment-start-date').length) {
              $validators = $validators.add('.field-name-field-employment-start-date input');
            }

            // Office location
            if ($('#custom_office_location_wrapper').length) {
              $validators = $validators.add('#custom_office_location_wrapper select');
            }

            // Custom fields
            for (var i = 1; i <= 3; i++) {
              if ($('.field-name-field-custom-field-value-' + i).length) {
                $validators = $validators.add('.field-name-field-custom-field-value-' + i + ' input');
              }
            }

            // Custom terms and conditions
            if ($('#custom_terms_wrapper').length) {
              $validators = $validators.add('#custom_terms_wrapper input')
            }

            // we also need to validate the that the user uploaded a file on the "Employment Verification" field
            // but the check for this have a special logic so we do it here instead of adding a validator on the array
            var hasEmplVerif =
              // Employment verification is also an optional field
              $('.field-name-field-employment-verification').length == 0
              ||
                // User selected a file, but haven't clicked on "upload" yet
              $('.field-name-field-employment-verification input[type=file]').val() != ''
              ||
                // User selected a file and already clicked on "upload"
              $('.field-name-field-employment-verification span.file').length;

            // If we have any empty inputs in our collection, or the user didn't uploaded
            // a file for "Employment Verification", keep the review button disabled
            if($validators.filter(removeFilledInputs).length == 0 && hasEmplVerif) {
              valid = true;
            }
            else {
              valid = false;
            }
          }
        }
        else {
          var cardValid, hasCVV;

          // Check if the credit card form is present
          if($('.form-item-commerce-payment-payment-details-credit-card-number').length) {
            // The user can proceed if they've entered a valid credit card number and have any value in the CVV code box
            cardValid = $('.form-item-commerce-payment-payment-details-credit-card-number').hasClass('valid');
            hasCVV = $('.form-item-commerce-payment-payment-details-credit-card-code input').val() != '';
          }
          // If the credit card form isn't present and the order mode is not direct bill, this may be a 'comp' order
          // (customer service rep places an order with a coupon code to zero out the order and then manually removes
          // the shipping cost via the admin screen). The order will actually use the direct bill payment method, but
          // we need the payment pane visible so they can enter the comp promo code).
          else {
            cardValid = true;
            hasCVV = true;
          }


          var validBillingProf = true;
          var billingProfCB = $paneWrapper.find('.form-item-customer-profile-billing-commerce-customer-profile-copy input[type=checkbox]');
          if (billingProfCB.attr('checked') != 'checked') {
            // If the user unchecked the "My Billing information is the same as my Shipping information."
            // Then we need to validate the billing profile form

            // We construct an array with the required fields
            var $validators = $('.form-type-select select, input.required', $paneWrapper.find('.customer_profile_billing'));
            // If all required fields are filled, we consider that the billing profile is valid
            validBillingProf = $validators.filter(removeFilledInputs).length == 0;
          }

          valid = cardValid && hasCVV && validBillingProf;
        }

        if(valid) {
          self.enableReviewButton();
        }
        // The button should stay disabled
        else {
          self.disableReviewButton();
        }
      };
      // First, unbind to any elements we have already bound to, to prevent duplicate event bindings
      $paneWrapper.find('form input, form select').unbind('keyup change', handler);
      $paneWrapper.find('form input, form select').on('keyup change', handler);

      // Trigger validation for any ajax reload - this allows us to react in the following scenarios, plus any we didn't think of:
      // - Shipping options reloaded (directbill with no 'employer info step')
      // - Employment info form reload (user uploaded a file on Employment verif. field)
      // - Billing profile is toggled on/off
      // - Promo code added/removed
      // - Any other ajax request on the page. Many will not be relevant to this validation, but it doesn't hurt.
      $('body').once('Drupal-attach', function () {
        $(this).on('Drupal-attach', handler);
      });

    };

    /**
     * Get our pseudo-continue button and enable it
     */
    self.enableReviewButton  = function() {$paneWrapper.find('.checkout-continue-pseudo').removeAttr('disabled');};

    /**
     * Get our pseudo-continue button and disable it
     */
    self.disableReviewButton  = function() {
      $paneWrapper.find('.checkout-continue-pseudo').attr('disabled', 'disabled');
    }

    /**
     * Review step: move the terms and conditions and place order button to left hand pane
     */
    self.activateReviewStep = function() {
      self.reviewInitiated = true;
      // putting back in empty div.info-review code (once held the "RED HAND OF GOD")
      // to simplify keeping backend code for not erasing credit card info once it's been entered
      // (the backend looks for this ID to identify that the user has progressed to the point of editing payment info)
      $('#checkout-steps-wrapper .info-review').remove();
      $('#checkout-steps-wrapper').prepend('<div id="info-review" class="info-review"></div>');
      // Show terms and conditions checkbox
      $('#terms-and-conditions').show();

    };

    /**
     * Grab values from the payment pane and make a pseudo-summary view. I call it "pseudo" because it does not come
     * from the server like the other summaries, but is created with JS from values currently in the form.
     * Also create an edit button that can toggle the contents on and off
     */
    self.buildPaneSummary = function() {
      // Remove any existing summary content for a fresh start and hide the original form
      $paneWrapper.find('.pseudo-summary').remove();
      $paneWrapper.find('.pane-contents form').hide();

      // Set up a wrapper for the summary content
      $paneWrapper.find('.pane-contents').append('<div class="field pseudo-summary"></div>');
      var $pseudoSummary = $paneWrapper.find('.pseudo-summary');
      self.alterSummaryContent($pseudoSummary);

      // Make an edit button
      $paneWrapper.find('.pane-edit').remove();
      $paneWrapper.find('.pane-title').after('<div class="pane-edit"><a class="checkout-edit-pane" href="javascript:;">Edit</a></div>');

      // Toggle form/summary when edit is clicked
      $paneWrapper.find('.pane-edit a').click(function(e){
        e.preventDefault();

        // Toggle classes and content
        $paneWrapper.toggleClass('collapsed')
          .toggleClass('active')
          .toggleClass('summary')
          .toggleClass('form');
        $paneWrapper.find('.pane-contents form').toggle();
        $paneWrapper.find('.pane-contents .pseudo-summary').toggle();

        // Remove the edit button, it will be created again when the pane is resubmitted
        $paneWrapper.find('.pane-edit').remove();

        validatePlaceOrder();
      });

      // Update classes
      $paneWrapper.addClass('collapsed')
        .addClass('summary')
        .removeClass('active')
        .removeClass('form');
    };

    /**
     * Generate and inject the HTML that goes in the pseudo-summary wrapper
     *
     * @param $pseudoSummary
     * - Wrapper that will contain the summary content
     */
    self.alterSummaryContent = function($pseudoSummary) {
      // Show the card type and last four of the credit card number, if present
      if($('.form-item-commerce-payment-payment-details-credit-card-number').length) {
        // Only show the last 4 digits of the card number
        var cardNum = $('.form-item-commerce-payment-payment-details-credit-card-number input').val();
        cardNum = 'XXXX-XXXX-XXXX-' + cardNum.substring(cardNum.length - 4, cardNum.length);
        var cardSummary = $('.form-item-commerce-payment-payment-details-credit-card-type select').val().toUpperCase() + ': ' + cardNum;
        $pseudoSummary.append('<div class="card-summary">' + cardSummary + '</div>');
      }

      // If user enters a billing address that is not the same as the shipping
      // address, we need to build the billing address summary in order to
      // display it on the summary page.
      if ($('.commerce-customer-profile-copy input').prop('checked') == false) {
        var separate_billing = $('<div>').addClass('separate_billing').html($('#pane-contents-shipping').html());
        var billing_form_class = '.customer_profile_billing';
        separate_billing.find('.first-name').text(
          $(billing_form_class).find('.first-name').val()
        );
        separate_billing.find('.last-name').text(
          $(billing_form_class).find('.last-name').val()
        );
        separate_billing.find('.thoroughfare').text(
          $(billing_form_class).find('.thoroughfare').val()
        );
        separate_billing.find('.premise').text(
          $(billing_form_class).find('.premise').val()
        );
        separate_billing.find('.state').text(
          $(billing_form_class).find('.state').val()
        );
        separate_billing.find('.postal-code').text(
          $(billing_form_class).find('.postal-code').val()
        );
        separate_billing.find('.locality').text(
          $(billing_form_class).find('.locality').val()
        );
        separate_billing.find('.country').text(
          $(billing_form_class).find('.country option:selected').text()
        );
        separate_billing.find('.field-name-field-phone .field-item').text(
          $('.form-item-customer-profile-billing-field-phone-und-0-value input').val()
        );
        $pseudoSummary.append(separate_billing.html());
      }
      else {
        // In this case, the user's billing and shipping addresses are the same,
        // so all we need to do is copy the shipping address summary and display
        // it as the billing address summary.
        var pseudoSummaryContent = $('<div>').html($('#pane-contents-shipping').html());
        $pseudoSummary.append(pseudoSummaryContent.html());
      }
    };

    /**
     * Attach our jquery.creditCardValidator plugin to the credit card number textfield.
     * It validates credit card input by adding and removing the 'valid' class as well as classes corresponding to the
     * type of credit card
     */
    self.creditCardValidation = function() {
      // Credit card validation
      try {
        $('.form-item-commerce-payment-payment-details-credit-card-number input', $paneWrapper).once('validateCC', function(){
          $(this).validateCreditCard(function(result) {
            var $parentDiv = $(this).closest('.form-item-commerce-payment-payment-details-credit-card-number');

            // Hide the credit card type dropdown, we'll set this for the user
            var $cardTypeWrapper = $('.form-item-commerce-payment-payment-details-credit-card-type');
            $cardTypeWrapper.hide();

            // Remove existing card type classes, if we can identify the card type, we'll re-add the class.
            $parentDiv.removeClass('visa')
              .removeClass('mastercard')
              .removeClass('amex')
              .removeClass('discover');

            // Set the 'card type' dropdown value
            var card_type = result.card_type == null ? '' : result.card_type.name;
            if(card_type) {
              // Set the credit card type value in our select list
              // Serendipitously, the validator plugin uses matching values as the dropdown values: 'visa', 'mastercard', 'amex', 'discover'
              $cardTypeWrapper.find('select').val(card_type);

              // Add a corresponding card-type class to our parent wrapper
              $parentDiv.addClass(card_type);
            }

            // Add a valid class
            var is_valid = result.valid && result.length_valid && result.luhn_valid;
            $parentDiv.removeClass('valid');
            if(is_valid) {
              $parentDiv.addClass('valid');
            }
          }, {accept: ['visa', 'mastercard', 'amex', 'discover']});
        });
      }
      catch(e) {  }
    };

    return self;
  })();

})(jQuery, Drupal, this, this.document); //END - Closure
