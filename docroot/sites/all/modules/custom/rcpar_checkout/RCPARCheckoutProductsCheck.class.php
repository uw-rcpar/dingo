<?php

/**
 * Class RCPARCheckoutCrossCheck
 * This class is used to determine if a list of products is purchasable based on a users existing active entitlements
 * and/or what they have in their shopping cart.
 */
class RCPARCheckoutProductsCheck {
  private $user_entitlements = array();
  private $product_ids_to_check = array();
  private $products_already_entitled = array();
  private $products_not_addable = array();
  private $product_ids_unexpanded = array();


  /**
   * RCPARCheckoutCrossCheck constructor.
   * @param $user
   * @param array $product_ids_to_check
   */
  function __construct($user, $product_ids_to_check) {
    $this->user = $user;

    // Get active and delayed entitlements and store in a normalize format with some easy to access product information
    $this->setUserEntitlements($user);

    // Store the unaltered product id(s) for future reference
    $this->product_ids_unexpanded = $product_ids_to_check;

    // If we've been given any bundles, expand the list of product ids to include the bundle contents. This will happen
    // for both course packages as well as accessory bundles (like cram, audio lectures, etc)
    $this->expandProductIds($product_ids_to_check);

    // Stores an array of products that the user already has entitlements for, as well as individual error messaging
    // with the products name and it's status (still active, delayed, etc)
    $this->determineProductsAlreadyEntitled();

    // Stores an array of products that the user cannot add to their cart, either because they already have entitlements
    // for the products, or because a duplicate product or package containing duplicate products is already in the cart
    $this->determineProductsNotAddableToCart();
  }

  /**
   * Expand bundles into the multiple products they contain.
   * If there is a bundled product in the product IDs we're checking, that bundle may actually consist of multiple
   * products - for example, course packages or audio lecture bundles, etc. In order to determine if the user can
   * purchase the bundle, we need to ensure that they don't have entitlements for the products it contains.
   * @param array $product_ids_to_check
   */
  private function expandProductIds($product_ids_to_check) {
    foreach ($product_ids_to_check as $product_id) {
      $rcb = new RCPARCommerceBundle($product_id);
      if($rcb->isLoaded()) {
        // Merge the associated bundle products into our list to check.
        $product_ids_to_check = array_unique(array_merge(array_keys($rcb->getProducts()), $product_ids_to_check));
      }
    }
    $this->product_ids_to_check = $product_ids_to_check;
  }

  /**
   * Get active and delayed entitlements and store in a normalize format with some easy to access product information
   * @param $user
   * - Normal Drupal user object
   */
  private function setUserEntitlements($user) {
    // we are going to use some active entitlements as well as some delayed ones
    // for the checkings
    // but we get them in different formats
    // so we are going to put them all in the $user_entitlements using
    // the same custom format
    $user_entitlements = array();

    // Collect this user's active entitlements.
    // (a entitlement is associated with a product, if we have a active entitlement
    // for a particular product, means that the user can't repurchase it)
    foreach (user_entitlements_get_entitlements($user) as $entitlement) {
      // user_entitlements_get_entitlements() returns at least one non-array element that we should ignore.
      if(!is_array($entitlement)) {
        continue;
      }
      $user_entitlements[$entitlement['field_product_id']['#items'][0]['value']] = array( // Key by product_id
        'prod_id'                => $entitlement['field_product_id']['#items'][0]['value'],
        'prod_sku'               => $entitlement['field_product_sku']['#items'][0]['value'],
        'entitlement_bundle'     => $entitlement['field_bundled_product']['#items'][0]['value'],
        'entitlement_expiration' => $entitlement['field_expiration_date']['#items'][0]['value'],
        'product_name'           => $entitlement['field_product_title']['#items'][0]['value'],
        'delayed'                => $entitlement['field_activation_delayed']['#items'][0]['value'],
        'course_section'         => isset($entitlement['field_activation_delayed']['#items'][0]['value']) ? 
                                      $entitlement['field_course_section']['#items'][0]['taxonomy_term']: NULL,
      );
    }

    // Add delayed products to our list of the user's entitlements.
    // We need the delayed products to prevent a user from repurchasing something that they haven't activated yet.
    // FWIW an example is a cram course when bought w/ the elite package (since
    // you wouldn't want to start that until you get closer to the exam date.)
    // Regular courses are not in delayed status
    $delayed_product_ids = user_entitlements_get_products_delayed($user);
    // to avoid getting an invalid arg err when $delayed_product_ids is NULL,
    // we'll check that it's an array (or object) BTW we're expecting an array,
    // but just to be sure...
    if (is_array($delayed_product_ids) || is_object($delayed_product_ids)) {
      foreach ((array) $delayed_product_ids as $delayed_prod_id) {
        $entitlement_node = node_load($delayed_prod_id['nid']);
        try {
          $entitlement_wrapper = entity_metadata_wrapper('node', $entitlement_node);
          $user_entitlements[$entitlement_wrapper->field_product_id->value()] = array( // Key by product_id
            'prod_id' => $entitlement_wrapper->field_product_id->value(),
            'prod_sku' => $entitlement_wrapper->field_product_sku->value(),
            'entitlement_bundle' => $entitlement_wrapper->field_bundled_product->value(),
            'entitlement_expiration' => $entitlement_wrapper->field_expiration_date->value(),
            'product_name' => $entitlement_wrapper->field_product_title->value(),
            'delayed' => $entitlement_wrapper->field_activation_delayed->value(),            
            'course_section' => isset($entitlement_wrapper->field_course_section) ? $entitlement_wrapper->field_course_section->value() : NULL,
          );
        } catch (EntityMetadataWrapperException $exc) {
          watchdog(
            'rcpar_checkout',
            'See ' . __FUNCTION__ . '() ' . $exc->getTraceAsString(),
            NULL, WATCHDOG_ERROR
          );
        }
      }
    }

    $this->user_entitlements = $user_entitlements;
  }

  private function getUserEntitlements() {
    return $this->user_entitlements;
  }

  /**
   * Stores an array of products that the user already has entitlements for, as well as individual error messaging
   * with the products name and it's status (still active, delayed, etc)
   */
  private function determineProductsAlreadyEntitled() {
    $user_entitlements = $this->getUserEntitlements();
    $product_ids_to_check = $this->product_ids_to_check;
    $products_already_entitled = array();

    // Determine if user already has entitlements for the specified products
    $product_ids_already_entitled = array_intersect($product_ids_to_check, array_keys($user_entitlements));

    foreach ($product_ids_already_entitled as $prod_id) {
      // we get the entitlement info, to know if its a delayed product or a regular one
      $invalid_prod = $user_entitlements[$prod_id];
      if ($invalid_prod['delayed']) {
        $error_msg = t("Your existing \"%product_name\" product is delayed and can not be repurchased at this time.<br>", array('%product_name' => $invalid_prod['product_name']));
      }
      else {
        $error_msg = t("Your existing \"%product_name\" product has NOT expired and can not be repurchased at this time.<br>", array('%product_name' => $invalid_prod['product_name']));
      }

      // Store an error message
      $products_already_entitled[$prod_id] = array(
        'message' => $error_msg,
      );
    }

    $this->products_already_entitled = $products_already_entitled;
  }


  /**
   * Stores an array of products that the user cannot add to their cart, either because they already have entitlements
   * for the products, or because a duplicate product or package containing duplicate products is already in the cart
   */
  private function determineProductsNotAddableToCart() {
    $invalid_products = array();

    $order = commerce_cart_order_load($this->user->uid);
    if($order === FALSE) {
      // If there are no products in the cart, then no other product would be prevented from being added
      return;
    }
    $commerce_order = new RCPARCommerceOrder($order);

    // If we're checking a bundle, we won't compare it against other items in the cart in an attempt to prevent it from
    // being added, except in the case of partner products being in the cart.
    // For normal products, adding a course package will trigger other validation to remove existing products so that
    // the cart contents can be replaced.
    if($this->isCoursePackage()) {
      // Prevent course packages from being added to the cart when there are partner products already there.
      if($commerce_order->includesPartnerProduct()) {
        foreach ($this->product_ids_to_check as $pid) {
          $invalid_products[$pid] = array('message' => t('This product cannot be added because a partner product is already in your cart.'));
        }
      }
    }
    elseif($this->isPartnerPackage()) {
      // Partner packages are always addable unless there is already a partner package in the cart.
      if($commerce_order->includesPartnerBundle()) {
        foreach ($this->product_ids_to_check as $pid) {
          $invalid_products[$pid] = array('message' => t('This product cannot be added because a partner bundle is already in your cart.'));
        }
      }
    }
    else {
      // We'll need to compare against the user's shopping cart - load it.
      // See if the order includes a partner product
      $includesPartnerProduct = $commerce_order->includesPartnerProduct(); // Avoid calling this for each product to lower queries

      // See if we're on the partner page
      $p = new RCPARPartner();
      $isPartnerPageLoaded = $p->isPartnerPageLoaded();

      $products_by_category = $commerce_order->getProductsByCategory();

      // Load products so we have their SKU and title
      $products = commerce_product_load_multiple($this->product_ids_to_check);

      foreach ($products as $product) {
        // If there is a partner product or partner bundle in the cart, we won't allow any other products to be added.
        // However, we don't want to summarily prevent products from being added when the user is on their own partner
        // page, otherwise once they've added one partner product they would be able to add another from the partner.
        if ($includesPartnerProduct && !$isPartnerPageLoaded) {
          $invalid_products[$product->product_id] = array(
            // The presence of each item in the array when a partner item is present, along with the related message
            // here will probably never be used, but we want to be consistent with our data.
            'message' => t('"%product_name" cannot be added because a partner product is in your cart.', array('%product_name' => $product->title)),
          );
        }
        else {
          // The product the user is trying to add is in the cart as part of a bundle
          if (key_exists($product->sku, $products_by_category['bundles'])) {
            $invalid_products[$product->product_id] = array(
              'message' => t('"%product_name" is already in your cart as part of your bundle.', array('%product_name' => $product->title)),
            );
          }

          // The product the user is trying to add is in the cart individually (NOT part of a bundle)
          if (key_exists($product->sku, $products_by_category['individuals'])) {
            $invalid_products[$product->product_id] = array(
              'message' => t('"%product_name" is already in your cart.', array('%product_name' => $product->title)),
            );
          }
        }
      }
    }

    // Get any in cart products with unmet dependencies
    array_merge($this->getCartProductsWithUnmetDependcies(), $invalid_products);

    $this->products_not_addable = $invalid_products;
  }

  /**
   * @return array
   *  - an array of all dependencies in the format product_id => dependent_product_id
   */
  private function getInCartDependencies() {
    // dependencies are set in an array wherein the key requires the value i.e. array(product_id => required_product_id)
    // @todo build a config screen to set these variables
    $products_with_dependencies_in_cart = array(
      // full crams books full require full cram course
      376 => 56,
      // reg cram book requires reg cram
      371 => 57,
      // bec cram book requires bec cram
      361 => 58,
      // far cram book requires far cram
      366 => 59,
      // aud cram book requires aud cram
      356 => 60,
    );

    return $this->products_with_dependencies_in_cart = $products_with_dependencies_in_cart;
  }

  /**
   * Checks the product ids in the cart for unmet dependencies
   * @return array
   *  - Returns an array of product ids that have unmet dependencies and so are not eligible to be in the shopping cart
   */
  public function getCartProductsWithUnmetDependcies() {
    $invalid_cart_dependencies = array();
    $dependencies = $this->getInCartDependencies();
    foreach ($this->product_ids_to_check as $product_id) {
      if (array_key_exists($product_id, $dependencies) && !in_array($dependencies[$product_id], $this->product_ids_to_check)){
        $invalid_cart_dependencies[$product_id] = array('message' => t('This product cannot be added because the dependent product is not in your cart.'));;
      }
    }
    return $this->filtered_cart_dependencies = $invalid_cart_dependencies;
  }

  /**
   * Returns TRUE if the products checked are purchasable.
   * A product is considered purchasable if the user does not already have an entitlement for it, or in the case of a
   * bundle, any of the products it contains.
   * Products may be purchasable even if they can't be added to the cart because exclusion from the cart is based not
   * only on existing entitlements, but also what is already in the cart. Ability to purchase products is only based on
   * what active entitlements you already have.
   * @return bool
   */
  public function isAllProductsPurchasable() {
    return (empty($this->products_already_entitled)) 
      && (count($this->product_ids_unexpanded) == count($this->getPurchasableProductIds()));
  }


  /**
   * Returns TRUE if the products can all be added to the cart.
   * A product can be added to the cart if
   * a.) The user does not already have entitlements for it (or in the case of bundles, any of the products it contains)
   * b.) The exact product is not already in the cart. (in the case of bundles, a different bundle is allowed to replace
   * a bundle that is already in your cart).
   * @param $prod_id
   * - The product ID of the product that we are checking is addable to the cart. This ID MUST be present in the array
   * of IDs that the class was initialized with.
   * @return bool
   */
  public function isAddableToCart() {
    // In addition to products that may already in the cart, products are not addable if the user already has
    // entitlements for them.
    // the trial user can buy a package.
    if((empty($this->products_already_entitled) && empty($this->products_not_addable)) || ($this->is_addable_by_trial_users() && empty($this->products_not_addable))){
      return TRUE;
    }
    return FALSE;
  }


  /**
   * Return an appropriate error message for products that are not purchasable.
   * @return string
   */
  public function getNotPurchasableErrorMessage() {
    $message = '';

    if(!empty($this->products_already_entitled)) {
      // If we only have one message, we can display it
      if(sizeof($this->products_already_entitled) == 1) {
        $first = reset($this->products_already_entitled);
        return $first['message'];
      }

      // Return a message when there are multiple existing entitlements
      if(sizeof($this->products_already_entitled) > 1) {
        return t("You already have products in this bundle that have not expired and cannot be repurchased at this time.");
      }
    }

    return $message;
  }

  /**
   * Return an appropriate error message for products that cannot be added to the cart.
   * @return string
   */
  public function getAddToCartErrorMessage() {
    // If we have errors because the user already has existing entitlements, show them over other messages.
    $message = $this->getNotPurchasableErrorMessage();
    if(!empty($message)) {
      return $message;
    }

    // Otherwise, we have some error related to products already in your cart.
    if(!empty($this->products_not_addable)) {
      if($order = commerce_cart_order_load($this->user->uid)) {
        // If we have a partner product in the cart and we're trying to add a non-partner product, we can generalize our
        // error messaging.
        $p = new RCPARPartner();
        $commerce_order = new RCPARCommerceOrder($order);
        if(!$p->isPartnerPageLoaded()) { // We don't show this type of error on the partner page itself
          if ($commerce_order->includesPartnerBundle()) {
            return t('This product cannot be added because a partner bundle is in your cart.');
          }
          if ($commerce_order->includesPartnerProduct()) {
            return t('This product cannot be added because a partner product is in your cart.');
          }
        }

      }

      // If we only have one message, we can display it
      if(sizeof($this->products_not_addable) == 1) {
        $first = reset($this->products_not_addable);
        return $first['message'];
      }

      // If the product we were checking is a bundle.
      // This also covers the scenario where a partner bundle is in the cart and the same partner bundle is being checked
      if($this->isBundle()) {
        return t("This bundle or the products it contains are already in your cart.");
      }

      // If we're here, there must be multiple products in the cart that cannot be added.
      return t("One or more of these products are already in your cart.");
    }

    return $message;
  }

  /**
   * @return array
   * - A numeric array of product ids that the user already has entitlements for from the list provided in
   * the constructor.
   */
  public function getProductIdsAlreadyEntitled() {
    return array_keys($this->products_already_entitled);
  }

  public function getProductsNotAddableToCart(){
    return $this->products_not_addable;
  }

  public function getProductIdsNotPurchasable() {
    return array_diff($this->product_ids_to_check, $this->getPurchasableProductIds());
  }

  /**
   * Checks products provided in the constructor.
   * Returns true when there is a single product ID that one of the three primary course packages "Select", "Premier",
   * or "Elite" as defined by RCPARCommerceBundle::isProductIdCoursePackage
   * @return bool
   */
  private function isCoursePackage() {
    if (sizeof($this->product_ids_unexpanded) == 1) {
      return RCPARCommerceBundle::isProductIdCoursePackage(reset($this->product_ids_unexpanded));
    }
    return FALSE;
  }

  /**
   * Returns TRUE when we have a single partner package.
   * @return bool
   */
  private function isPartnerPackage() {
    if (sizeof($this->product_ids_unexpanded) == 1) {
      if(in_array('partner_package', $this->product_ids_unexpanded)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Checks products provided in the constructor.
   * If we had one product ID originally supplied and it expanded to more than one, we were given a bundle.
   * This is true for course packages (select, premier, elite), but is also true for other bundles like cram, audio
   * lectures, etc.
   * @return bool
   */
  private function isBundle() {
    if (sizeof($this->product_ids_unexpanded) == 1) {
      if (sizeof($this->product_ids_to_check) > 1) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Checks products provided in the constructor.
   * If we were given one product and that product does not contain additional products, we are just working with one
   * individual product.
   * @return bool
   */
  private function isIndividual() {
    if (sizeof($this->product_ids_unexpanded) == 1) {
      if (sizeof($this->product_ids_to_check) == 1) {
        return TRUE;
      }
    }
  }

  public function getPurchasableProductIds() {
    // We might not be able to add all products, so we need to check
    // that the user does not already have an entitlement for the one we are trying to add
    $purchasableProducts = array();
    $purchasableProducts = array_diff($this->product_ids_unexpanded, $this->getProductIdsAlreadyEntitled());
    if ($this->getCartProductsWithUnmetDependcies()) {
      $purchasableProducts = array_diff($purchasableProducts, array_keys($this->getCartProductsWithUnmetDependcies()));
    }
    $purchasableProducts = $this->removeActAssignableMaterials($purchasableProducts);
    return $purchasableProducts;

  }
  

  /**
   * This fuction will check if user already have 
   * the courses of the sections, 
   * if user has it then the assignable materials
   * should be removed from the array with the courses 
   * that will be added to the user.
   * @param $products_ids array with the courses that will be added to the user
   */
  private function removeActAssignableMaterials($products_ids) {
    
    foreach ($products_ids as $key => $product_id) {

      //Check if product is act_assignable_materials
      $results = db_query("SELECT sku 
        FROM {commerce_product} cp 
        WHERE cp.type = 'act_assignable_materials' 
        AND cp.product_id = :product_id", 
        array(":product_id" => $product_id))->fetchAll();

      if(count($results) > 0) {
        $act_product = reset($results);

        //Get the section's name from the name of the act_assignable_material
        //For example: ACT-STUDENT-AUD -> AUD
        $section = end(explode('-',$act_product->sku));
      
        //Check if user already has the course of this section
        foreach ($this->user_entitlements as $user_entitlement) {
          //If user already has the course of this section
          //Remove the act_assignable_materials
          if(isset($user_entitlement['course_section']) 
            && $user_entitlement['prod_sku'] == $section
            && $user_entitlement['entitlement_expiration'] > time()) {
            unset($products_ids[$key]);
          }
        }
      }
    }
    return $products_ids;
  }
  
  /**
   * The users with only free entitlements and 
   * trying to buy a package will be able to add.
   * @return bool
   *  - Returns TRUE if the package can be added to the cart.
   */
  public function is_addable_by_trial_users() {
    if($this->isCoursePackage() 
        && user_entitlements_user_is_free_trial($this->user->uid)){
      return true;
    }
    return false;
  }
}

