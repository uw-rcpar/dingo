<?php

/**
 * Class RCPARCommerceOrder
 * This class analyzes and organizes line items on orders to provide custom business logic for our
 * eCommerce platform.
 *
 * For a simple wrapper around commerce_order entities, see RCPARCommerceOrderEntity.
 */
class RCPARCommerceOrder extends RCPARCommerceOrderEntity
{

  protected $order_total;
  protected $order_amount_components;
  protected $order_products_list_by_category;
  public $order;

  // The product IDs of any/all bundles within the order
  protected $bundle_product_ids = array();

  // The product IDs of any bundles within the order that are course packages (should never be more than one)
  protected $course_package_product_ids = array();

  // The node IDs of any partners associated with the order (should never be more than one)
  protected $partner_ids;

  // Unformatted commerce price of total amount of discount line items on the order (positive number)
  protected $discounts = 0;

  // Convenience storage for the product ids of products which are not part of a bundle
  protected $nonbundled_product_ids;

  // Product IDs for all of the individual products in this order, whether they're in a bundle or not
  // This does not include the product IDs of the actual bundle products themselves.
  protected $all_product_ids = array();

  // The bundled product SKU as would be represented on user entitlements produced from
  // this order. E.g. "_none", "FULL_DIS", "FULL-PREM-DIS", etc. Defaults to "_none"
  protected $bundled_product_sku = '_none';

  /**
   * RCPARCommerceOrder constructor.
   * Get information of an order
   * @param $order
   * - A entity of type 'commerce_order'
   */
  public function __construct($order) {
    // Load the order and order wrapper
    parent::__construct($order);

    // Order failed to load
    if (!$this->order) {
      return;
    }

    // Get total components in order
    $order_total = $this->wrapper->commerce_order_total->value();
    $this->order_total = $order_total['amount'];

    // Store tax information on $this->order_amount_components
    foreach ($order_total['data']['components'] as $total_component) {
      $name = ($total_component['name'] == 'avatax') ? 'tax' : $total_component['name'];
      $this->order_amount_components[$name] = $total_component['price']['amount'];
    }

    // Categorize products and store data about what's on the order
    $this->setProductCategories($this->wrapper);
  }

  /**
   * Get the order ID
   * @return int
   * - The order ID, or 0 on failure.
   */
  public function getOrderId() {
    try {
      return $this->wrapper->order_id->value();
    }
    catch (EntityMetadataWrapperException $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
    return 0;
  }

  /**
   * Categorize products and store data about what's on the order
   * We'll also tally up related partner ids and package nodes.
   * @param $order_wrapper
   */
  private function setProductCategories($order_wrapper)
  {
    // Organize products based on those which are part of a bundle vs those which are individuals
    $order_products_list_by_category = array(
      'bundles' => array(),
      'individuals' => array(),
    );
    // If we encounter products that are part of a bundle, we'll store the bundle's product id
    $bundle_product_ids = array();

    // A list of discount SKUs that represent a bundle. We'll look for a matching one in the order
    $bundle_skus = array_map('trim', explode("\n", variable_get('ef_bundled_skus', '')));

    // If we encounter products that are part of a NOT part of a bundle, we'll store orders line_item_id keyed on
    // the product id
    $nonbundled_product_ids = array();
    // If we encounter products that are related to a partner, we'll store the partner ID
    $partner_ids = array();

    // Iterate over each line item and categorize
    foreach ($order_wrapper->commerce_line_items as $line_wrapper) {
      // If this line item has a price and it's negative, store it in our discounts.
      try {
        $price = $line_wrapper->commerce_unit_price->value();
        if ($price['amount'] < 0) {
          $this->discounts += abs($price['amount']);
        }
      } catch (Exception $e) {
      }

      // Only look at 'product' line items
      if (in_array($line_wrapper->getBundle(), array('product'))) {
        $product = $line_wrapper->commerce_product->value();
        $sku = $product->sku;
        $line_item = $line_wrapper->value();

        // Determine if this line item is part of a bundled package (partner package, course package, or generic bundle)
        $bundled = isset($line_item->data['package']);

        // Check for a matching bundled product SKU and store.
        if (in_array($sku, $bundle_skus)) {
          $this->bundled_product_sku = $sku;
        }

        // If this is a bundled or individual product related to a partner, store the partner ID
        if (!empty($line_item->data['partner'])) {
          $partner_ids[$line_item->data['associated_node_id']] = $line_item->data['associated_node_id'];
        }

        // Add to all product ID storage
        $this->all_product_ids[] = $line_item->commerce_product['und'][0]['product_id'];

        // Store the product in our bundles group and store the bundle's node id
        if ($bundled) {
          $order_products_list_by_category['bundles'][$sku] = $line_wrapper->line_item_id->value();
          // Store the bundle's product ID
          $bundle_product_ids[$line_item->data['product_id']] = $line_item->data['product_id'];

          // If this bundle is also a course package, store this info.
          if (RCPARCommerceBundle::isProductIdCoursePackage($line_item->data['product_id'])) {
            $this->course_package_product_ids[$line_item->data['product_id']] = $line_item->data['product_id'];
          }
        } // Store in our individual's group
        else {
          $order_products_list_by_category['individuals'][$sku] = $line_wrapper->line_item_id->value();
          $nonbundled_product_ids[$line_wrapper->commerce_product->product_id->value()] = $line_wrapper->line_item_id->value();
        }
      }
    }

    // Store properties into our object
    $this->order_products_list_by_category = $order_products_list_by_category;
    $this->bundle_product_ids = $bundle_product_ids;
    $this->partner_ids = $partner_ids;
    $this->nonbundled_product_ids = $nonbundled_product_ids;
  }

  /**
   * Get the counter of products in an order.
   * If there are products related with a bundle, It should wrap all related products in 1
   * @return int
   */
  public function getItemCount() {
    return count($this->getBundleProductIds()) + count($this->order_products_list_by_category['individuals']);
  }

  /**
   * Get total amount of all coupons in an order
   * @return int
   */
  public function getTotalPromotions() {
    // Get all coupons on the order
    $coupons = $this->getCoupons();

    // Add the value of all coupons together
    $total = 0;
    foreach ($coupons as $coupon_value) {
      $total += $coupon_value;
    }

    return $total;
  }

  /**
   * Get coupons in an order
   * @return array with coupon code and value
   */
  public function getCoupons() {
    $coupons = array();
    try {
      $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
      $order_coupons = $order_wrapper->commerce_coupon_order_reference->value();
      if (!empty($order_coupons)) {
        foreach ($order_coupons as $order_coupon) {
          $code = $order_coupon->commerce_coupon_code[LANGUAGE_NONE][0]['value'];
          // some coupons don't have an amount prop, it's been working as expected w/ null
          $my_amount = NULL; // set the default value
          if (!empty($order_coupon->commerce_coupon_fixed_amount[LANGUAGE_NONE][0]['amount'])) {
            // this is from the original code, which works as expected for fixed coupons & elite discounts
            $my_amount = $order_coupon->commerce_coupon_fixed_amount[LANGUAGE_NONE][0]['amount'];
          } else {
            // now run the amount through this so that the granted amount is calculated properly
            // for example for percentage discounts
            // gotten from: commerce_coupon/includes/views/handlers function commerce_coupon_handler_field_coupon_granted_amount
            // using this approach so as to cover any new coupon types introduced in the future
            $amount = '';
            drupal_alter('commerce_coupon_granted_amount', $amount, $order_coupon, $this->order);
            // this converts the string w/ '$' to a float
            $amount_float = floatval(preg_replace("/([^0-9\\.])/i", "", $amount));
            // we need to multiply by 100 because the code expected $order_coupon->commerce_coupon_fixed_amount[LANGUAGE_NONE][0]['amount'];
            $my_amount = $amount_float * 100;
          }
          $coupons[$code] = $my_amount;
        }
      }
    } catch (EntityMetadataWrapperException $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
    return $coupons;
  }

  /**
   *  Percentage coupon that has been flagged as 
   *  Disable Affiliate Attribution Tracking
   * @return true if checked on coupon
   */
  public function isFirmDiscount() {
    try {
      $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
      $order_coupons = $order_wrapper->commerce_coupon_order_reference->value();
      if (!empty($order_coupons)) {
        foreach ($order_coupons as $order_coupon) {
          if ("commerce_coupon_pct" == $order_coupon->type && isset($order_coupon->field_disable_affiliate_attribut[LANGUAGE_NONE][0]['value'])) {
            return (bool) $order_coupon->field_disable_affiliate_attribut[LANGUAGE_NONE][0]['value'];
          }
        }
      }
    }
    catch (EntityMetadataWrapperException $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
    return false;
  }

  /**
   * Get coupon in an order by its code.
   * @param $code
   * @return bool
   */
  function couponExistsByCode($code) {
    $coupons = $this->getCoupons();
    if (isset($coupons[$code])) {
      return TRUE;
    }
    return FALSE;
  }


  /**
   * Get the sub total amount of an order minus total promotions
   * @return int
   */
  public function getTotalAfterTotalPromotions() {
    return $this->getSubTotal() - $this->getTotalPromotions();
  }

  /**
   * Get the sub total amount of an order
   * @return int
   */
  public function getSubTotal() {
    return !isset($this->order_amount_components['base_price']) ? 0 :
      $this->order_amount_components['base_price'];
  }

  /**
   * Get the total amount of shipping in an order
   * @return int
   */
  public function getShippingCost() {
    // need to check all available shipping_commerce_price_component_types
    // returns an array keyed on <shipping_commerce_price_component_type_name> => <array of shipping_commerce_price_component_type_info>
    $commerce_shipping_commerce_price_component_types = commerce_shipping_commerce_price_component_type_info();
    //TODO consider getting these array keys as a separate function somewhere cashing 
    // it with &drupal_static(__FUNCTION__);
    foreach (array_keys($commerce_shipping_commerce_price_component_types) AS $type_name) {
      if (isset($this->order_amount_components[$type_name])) {
        return $this->order_amount_components[$type_name];
      }
    }
    // shipping isn't set yet
    return 0;
  }


  /**
   * Get the shipping method of the order
   * @return string with shipping method
   */
  public function getShippingMethodName() {
    $shipping_method = '';

    if ($this->order) {
      $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
      // First, be sure there are line_items
      if (!empty($order_wrapper->commerce_line_items)) {
        // loop through line items and find the one for shipping
        foreach ($order_wrapper->commerce_line_items as $order_line_item_wrapper) {
          if ($order_line_item_wrapper->type->value() == 'shipping') {
            // get the shipping method
            $shipping_method = $order_line_item_wrapper->line_item_label->value();
          }
        }
      }
    }
    return $shipping_method;
  }

  /**
   * Get the total amount of an order without the tax
   * @return int
   */
  public function getTotalBeforeTax() {
    return $this->getGrandTotal() - $this->getTax();
  }

  /**
   * Get the tax amount of an order if is set.
   * @return int
   */
  public function getTax() {
    $result = 0;
    // new way to get tax on Avatax 2.0
    if (isset($this->order_amount_components['avatax_sales_tax']) && $this->order_amount_components['avatax_sales_tax'] > 0) {
      $result = $this->order_amount_components['avatax_sales_tax'];
    } // Condiiton for orders prior to upgrading Avatax to 2.0
    elseif (isset($this->order_amount_components['tax']) && $this->order_amount_components['tax'] > 0) {
      $result = $this->order_amount_components['tax'];
    }
    return $result;
  }

  /**
   * Get the final total amount of an order.
   * @return int
   */
  public function getGrandTotal() {
    return $this->order_total;
  }

  /**
   * Get the total amount of discounts in the line items on this order. Represented as a positive number. This is an
   * unformatted commerce price.
   * @return int
   */
  public function getDiscounts() {
    return $this->discounts;
  }

  /**
   *  This fuction define if the SKU is into the cart
   * @todo - Currently no usages of this function. This is not a very good approach anyway because we only add the first
   * SKU of a bundle into $this->order_products_list in order to count it once, so we might not find a SKU we were
   * looking for if it were part of a bundle.
   * For a good example of how to write this, see: \RCPARCheckoutProductsCheck::determineProductsNotAddableToCart()
   *
   * @param $sku
   *  The id of product
   *
   * @return(boolean) True if SKU is into the cart
   */
  public function is_this_product_into_the_cart($sku) {
    //return in_array($sku, $this->order_products_list);
  }

  /**
   *  This fuction return the list of product by category
   *
   * @return(array) list of product by category
   * // Organize products based on those which are part of a bundle vs those which are individuals
   * $order_products_list_by_category = array(
   *   'bundles' => array(
   *     $sku => $line_item_id,
   *   ),
   *   'individuals' => array(
   *     $sku => $line_item_id,
   *   ),
   * );
   */
  public function getProductsByCategory() {
    return $this->order_products_list_by_category;
  }

  /**
   * Returns an associated array keyed by SKU of line item ids of all products on the order
   * @return array
   */
  public function getAllProductsBySKU() {
    $productsByCategory = $this->getProductsByCategory();
    return array_merge($productsByCategory['bundles'], $productsByCategory['individuals']);
  }

  /**
   * Returns an array of all product IDs in this order after expanding bundles (e.g., a
   * single bundled product ID like '66' would become all of the individual product ids
   * within that bundle).
   * @return int[]
   */
  public function getAllProductIdsExpanded() {
    return $this->all_product_ids;
  }

  /**
   * Detects if the order contains a partner bundle on it
   * @return bool
   * - Returns true if the order does include a partner bundle, false otherwise
   */
  public function includesPartnerBundle() {
    $includes_bundle = FALSE;
    if ($this->order) {
      $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
      try {
        foreach ($order_wrapper->commerce_line_items as $order_line_item_wrapper) {
          if ($order_line_item_wrapper->type->value() == 'product' && $order_line_item_wrapper->line_item_label->value() == 'PARTNER-DIS') {
            // If the order includes the PARTNER-DIS discount is because it includes a partner bundled
            $includes_bundle = TRUE;
          }
        }
      } catch (EntityMetadataWrapperException $exc) {
        watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
    }
    return $includes_bundle;
  }

  /**
   * Determine if we have any products on the order related to a partner - this may be custom partner bundles OR
   * products offered individually by a partner.
   * @return bool
   */
  public function includesPartnerProduct() {
    if (sizeof($this->partner_ids) > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine if the order contains Unlimited Access
   * @return bool
   */
  public function includesUnlimitedAccess() {
    if (in_array('FULL-UA', array_keys($this->getAllProductsBySKU()))) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine if we have any bundles on the order that are course packages.
   * @return bool
   */
  public function includesCoursePackage() {
    return sizeof($this->course_package_product_ids) > 0;
  }

  /**
   * Determine if we have any bundles on the order - these may be accessory bundles (e.g. flashcards), course packages,
   * or a partner package
   * @return bool
   */
  public function includesBundles() {
    return sizeof($this->getBundleProductIds()) > 0;
  }

  /**
   * Determine if we have any bundles on the order which are NOT course package or partner bundles
   * todo - it may be more efficient to store generic bundle ids when we are iterating through products in the constructor
   * @return bool
   */
  public function includesGenericBundle() {
    foreach ($this->getBundleProductIds() as $bundleProductId) {
      $b = new RCPARCommerceBundle($bundleProductId);
      if ($b->getBundleType() == RCPARCommerceBundle::BUNDLE_TYPE_GENERAL) {
        return TRUE;
      }
    }
  }

  /**
   * If there are any products in this order that were added as part of a bundle, we'll have stored the bundle's
   * product id.
   * This function returns an array of those product ids, keyed by the same product id.
   * NOTE: Bundle refers to partner packages, course packages, and generic/accessory bundles like flashcards, etc.
   * @return array
   */
  public function getBundleProductIds() {
    return $this->bundle_product_ids;
  }

  /**
   * If there is a course package in this order, returns the node id associated with it, otherwise returns false.
   * Theoretically, multiple course packages could be on an order but other code logic should prevent this, so we just
   * return the first ID we find.
   * @return bool|int
   */
  public function getCoursePackageProductId() {
    if ($this->includesCoursePackage()) {
      return reset($this->course_package_product_ids);
    }
    return FALSE;
  }


  /**
   * If there are any products in this order that are NOT part of a bundle, we'll store the line_item_id keyed by the product_id
   *
   * It's used for example in validating if a coupon's referenced products applies to this order
   *
   * NOTE: "Bundle" in this case refers to course packages and partner packages. It does not currently refer to bundled
   * accessory products like 'full' flashcards, etc.  Our terminology is a little overloaded.
   *
   * @return array of line_item_ids keyed by their product_id (empty array if no unbundled products in the order)
   */
  public function getNonBundledProductIds() {
    return $this->nonbundled_product_ids;
  }

  /**
   * Gets all product IDs in the order
   * @return array
   * - An array of product IDs that includes both bundled and non-bundled products.
   */
  public function getAllProductIds() {
    return array_merge(array_keys($this->getNonBundledProductIds()), array_keys($this->getBundleProductIds()));
  }

  /**
   * Provides shipping information
   * @return array
   */
  public function getShippingAddress() {
    $result = array();
    $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
    try {
      if ($commerce_customer_shipping = $order_wrapper->commerce_customer_shipping->value()) {
        if (isset($commerce_customer_shipping->commerce_customer_address) && is_array($commerce_customer_shipping->commerce_customer_address)) {
          $commerce_customer_address = $commerce_customer_shipping->commerce_customer_address[LANGUAGE_NONE][0];
          $result = array(
            'administrative_area' => $commerce_customer_address['administrative_area'],
            'locality' => $commerce_customer_address['locality'],
            'postal_code' => $commerce_customer_address['postal_code'],
          );
        }
      }

    } catch (EntityMetadataWrapperException $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
    return $result;
  }

  /**
   * Get coupon line item if exists
   * @return bool
   */
  public function getCouponLineItem($coupon_name) {
    return isset($this->order_amount_components[$coupon_name]);
  }

  /**
   * Get payment method name of an order
   * @return String $payment_method
   */
  public function getCommercePaymentTransactionName() {
    $payment_method = '';
    try {
      $order = $this->order;
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'commerce_payment_transaction')
        ->propertyCondition('order_id', $order->order_id);

      $results = $query->execute();
      if (!empty($results['commerce_payment_transaction'])) {
        foreach ($results['commerce_payment_transaction'] as $transaction) {
          $payment_method = $transaction->payment_method;
        }
      }

    } catch (EntityMetadataWrapperException $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
    return $payment_method;
  }

  /**
   * Removes one or more products from the order
   * @param array $product_ids
   * @return bool
   *  - Returns TRUE only if ALL products were removed from the cart and the order was saved.
   */
  public function removeProductsFromCart($product_ids) {
    // Assume success unless an exception is thrown or error is found.
    $success = TRUE;

    try {
      $product_ids = array_keys($product_ids);
      foreach ($this->wrapper->commerce_line_items as $delta => $line_item_wrapper) {
        try {
          // Get the product id
          $current_product_id = $line_item_wrapper->commerce_product->raw();
          $line_item = $line_item_wrapper->value();
          // Remove if we have a match
          if (in_array($current_product_id, $product_ids)) {
            if (!commerce_cart_order_product_line_item_delete($this->order, $line_item->line_item_id)) {
              $success = FALSE;
            }
          }
        }
        catch (EntityMetadataWrapperException $exc) {
          watchdog('rcpar_checkout', 'Failed to remove product from cart. ' . $exc->getTraceAsString(), NULL, WATCHDOG_ERROR);
        }
      }
    }
    catch (EntityMetadataWrapperException $exc) {
      watchdog('rcpar_checkout', 'Order data issue while removing products from cart. ' . $exc->getTraceAsString(), NULL, WATCHDOG_ERROR);
    }

    // Save the order regardless of the number of products we were able to remove. If there's a problem, better some
    // than none.
    if(commerce_order_save($this->order)) {
     $success = FALSE;
    }

    return $success;
  }

  /**
   * Return the SKU of the discount that identifies the bundle. This will be a
   * package discount, like FULL-DIS, FULL-PREM-DIS, etc. If we find FULL-PREM-DIS,
   * we'll identify all entitlements in the order has having originated from a Premier
   * Package purchase. Defaults to "_none", as this is the empty value for the
   * field_bundled_product on the entitlement node type.
   *
   * @return string
   */
  public function getBundledProductSku() {
    return $this->bundled_product_sku;
  }
}
