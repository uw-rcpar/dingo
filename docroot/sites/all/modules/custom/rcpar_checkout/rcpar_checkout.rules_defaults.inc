<?php
/**
 * @file
 * full_plus_coupon.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rcpar_checkout_default_rules_configuration() {
  $items = array();
    $items['commerce_coupon_fixed_amount_redeem_a_premiere_coupon'] = entity_import('rules_config', '{ "commerce_coupon_fixed_amount_redeem_a_premiere_coupon" : {
      "LABEL" : "Redeem a Premiere Coupon",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "6",
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules", "commerce_order", "commerce_coupon" ],
      "ON" : { "commerce_coupon_redeem" : [] },
      "IF" : [
        { "NOT php_eval" : { "code" : " return enroll_flow_rules_coupon_validation_msg(\u0022premiere_discount\u0022, $commerce_order, $coupon); " } },
        { "data_is" : { "data" : [ "coupon:type" ], "value" : "premier_discount_v2" } },
        { "entity_has_field" : { "entity" : [ "coupon" ], "field" : "commerce_coupon_fixed_amount" } },
        { "entity_has_field" : {
            "entity" : [ "commerce_order" ],
            "field" : "commerce_coupon_order_reference"
          }
        },
        { "NOT AND" : [
            { "data_is_empty" : { "data" : [ "coupon:commerce-coupon-fixed-amount" ] } }
          ]
        },
        { "data_is" : {
            "data" : [ "coupon:commerce-coupon-fixed-amount:amount" ],
            "op" : "\u003E",
            "value" : 0
          }
        },
        { "data_is" : { "data" : [ "coupon:is-active" ], "op" : "=", "value" : true } },
        { "NOT php_eval" : { "code" : "return enroll_flow_order_has_coupon($commerce_order);" } },
        { "php_eval" : { "code" : "return enroll_flow_rules_coupon_exclusion($commerce_order);" } },
        { "OR" : [
            { "commerce_order_contains_product" : {
                "commerce_order" : [ "commerce_order" ],
                "product_id" : "FULL-PREM-DIS",
                "operator" : "\u003E=",
                "value" : "1"
              }
            },
            { "commerce_order_contains_product" : {
                "commerce_order" : [ "commerce_order" ],
                "product_id" : "FULL-PREM-AUD-DIS",
                "operator" : "\u003E=",
                "value" : "1"
              }
            },
            { "commerce_order_contains_product" : {
                "commerce_order" : [ "commerce_order" ],
                "product_id" : "FULL-PREM-EFC-DIS",
                "operator" : "\u003E=",
                "value" : "1"
              }
            }
          ]
        }
      ],
      "DO" : [
        { "list_add" : {
            "list" : [ "commerce-order:commerce-coupon-order-reference" ],
            "item" : [ "coupon" ],
            "unique" : 1
          }
        },
        { "commerce_coupon_action_create_coupon_line_item" : {
            "USING" : {
              "commerce_coupon" : [ "coupon" ],
              "commerce_order" : [ "commerce-order" ],
              "amount" : [ "coupon:commerce-coupon-fixed-amount:amount" ],
              "component_name" : [ "coupon:price-component-name" ],
              "currency_code" : [ "coupon:commerce-coupon-fixed-amount:currency-code" ]
            },
            "PROVIDE" : { "commerce_coupon_line_item" : { "commerce_coupon_line_item" : "commerce coupon line item" } }
          }
        }
      ]
    }
  }');
  return $items;
}
