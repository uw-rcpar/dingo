<?php

/**
 * Get the $form_state with values array defined with changes made as defined in 
 * confluance (https://confluence.rogercpareview.com/pages/viewpage.action?pageId=4849673&focusedCommentId=4849760)
 * The block for this file is defined between 'get_' and '_form_state_vals' of function name
 * like: get_<module>_<delta>_form_state_vals
 * This is a common format used by rcpar_checkout_deploy for defining one block per file
 * These values are gotten by debugging the form on submittal in a current enrollment environment
 * 
 * @return array Standard Drupal $form_state array
 */
function rcpar_checkout_deploy_get_block_152_form_state_vals() {
  $form_state           = array();
  $form_state['values'] = array(
    'module' => 'block',
    'delta' => '152',
    'title' => '',
    'info' => 'Checkout Page Header',
    'body' =>
    array(
      'value' => '<div class="row first-row">
        <div class="col-sm-12 ">
            <div class="page-heading checkout-heading">
                <h1 class="page-title pull-left"><img src="/sites/all/themes/bootstrap_rcpar/css/img/icon_cart.svg" height="48" class="cart-icon-mobile" />Checkout </h1>
                <div class="cart-added-help pull-right">
                    <div class="cart-questions">Questions? <em>We love ‘em!</em></div>
                    <div class="divider"></div>
                    <div class="cart-info">
                      <div class="cart-here">Call Us:</div>
                      <div class="cart-call"><span>1-877.764.4274</span></div>
                      <div class="cart-chat"><a href="/contact">Or Start a Chat</a></div>
                    </div>
                </div>
                <div class="clear-empty"></div>
        </div>
      </div>
    </div>


<div class="item-list breadcrumb-list checkout-breadcrumb"><ul class="breadcrumb checkout-breadcrumb"><li><a href="/cart">&lt; Back to Cart</a></li>
</ul></div>',
      'format' => 'full_html',
    ),
    'css_class' => '',
    'regions' =>
    array(
      'bootstrap' => -1,
      'bootstrap_rcpar' => 'content',
    ),
    'visibility' => '1',
    'pages' => 'custom-checkout*',
    'roles' =>
    array(),
    'custom' => '0',
    'types' =>
    array(),
    'rcpar_visibility_ignore' => '1',
    'rcpar_visibility_level' => '1',
    'rcpar_visibility_specific' =>
    array(
      'AUD' => 0,
      'BEC' => 0,
      'REG' => 0,
      'FAR' => 0,
    ),
//    'visibility__active_tab' => 'edit-path',
//    'submit' => 'Save block',
//    'form_build_id' => 'form-xdm2Nd_bTZFGwhxvmpOtu4SRE_gw1T5tEEIc3AyXNjY',
//    'form_token' => 'hG-GHBwHJj8VpfVLKHWswoDp-rfml0uCFHGxdCbxyRY',
//    'form_id' => 'block_admin_configure',
//    'op' => 'Save block',
  );
  return $form_state;
}
