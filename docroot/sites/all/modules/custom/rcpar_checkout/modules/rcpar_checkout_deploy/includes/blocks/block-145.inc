<?php

/**
 * Get the $form_state with values array defined with changes made as defined in 
 * confluance (https://confluence.rogercpareview.com/pages/viewpage.action?pageId=4849673&focusedCommentId=4849760)
 * The block for this file is defined between 'get_' and '_form_state_vals' of function name
 * like: get_<module>_<delta>_form_state_vals
 * This is a common format used by rcpar_checkout_deploy for defining one block per file
 * These values are gotten by debugging the form on submittal in a current enrollment environment
 * 
 * @return array Standard Drupal $form_state array
 */
function rcpar_checkout_deploy_get_block_145_form_state_vals() {
  $form_state           = array();
  $form_state['values'] = array(
    'module' => 'block',
    'delta' => '145',
    'title' => '',
    'info' => 'Affirm - information text in payment options',
    'body' =>
    array(
      'value' => '<div class="commerce-affirm-info">
 
        <div class="row affirm-first-row">
        	<div class="affirm-copy copy-monthly col-md-12">
			<h2>How does Affirm work?</h2>
                         <p>Buy now with Affirm and pay over time. It’s simple financing that fits your life.</p>
		</div> 
        </div>
        <div class="row affirm-second-row">
        	<div class="affirm-icon icon-monthly"><img src="/sites/all/modules/custom/rcpar_checkout/img/icon_affirm-pmnts.svg" width="29"></div> 
			<div class="affirm-copy copy-monthly">
				<h3>You\'re in Control</h3>
				<p>Pay over time with Affirm and split your purchase into 3, 6, 0r 12 monthly payments. Rates from 10-30% APR.</p>
			</div> 
        </div>
        <div class="row affirm-third-row">
        	<div class="affirm-icon icon-flexible"><img src="/sites/all/modules/custom/rcpar_checkout/img/icon_affirm-cart.svg" width="31"></div> 
			<div class="affirm-copy copy-flexible">
				<h3>Simple Checkout</h3>
<ol>
<li>Select Affirm as your payment method</li>
<li>Click to submit your order</li>
<li>Complete Affirm\'s quick and easy application</li>
<li>Finalize your order with RogerCPAReview.com
<div class="affirm-footnote">All successful orders are processed immediately.</div></li>
</ol>
			</div> 
        </div>	
</div>',
      'format' => 'full_html',
    ),
    'css_class' => '',
    'regions' =>
    array(
      'bootstrap' => -1,
      'bootstrap_rcpar' => -1,
    ),
    'visibility' => '0',
    'pages' => '',
    'roles' =>
    array(),
    'custom' => '0',
    'types' =>
    array(),
    'rcpar_visibility_ignore' => '1',
    'rcpar_visibility_level' => '1',
    'rcpar_visibility_specific' =>
    array(
      'AUD' => 0,
      'BEC' => 0,
      'REG' => 0,
      'FAR' => 0,
    ),
//    'visibility__active_tab' => 'edit-path',
//    'submit' => 'Save block',
//    'form_build_id' => 'form-RqxQ_tOvq3GXEKiTOWX-g2qC35w_ao8CoaLxnq1o8Qg',
//    'form_token' => 'hG-GHBwHJj8VpfVLKHWswoDp-rfml0uCFHGxdCbxyRY',
//    'form_id' => 'block_admin_configure',
//    'op' => 'Save block',
  );
  return $form_state;
}
