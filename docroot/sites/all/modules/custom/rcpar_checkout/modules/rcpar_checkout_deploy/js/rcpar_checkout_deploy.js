(function ($) {
  Drupal.behaviors.RcparCheckoutDeploy = {
    attach: function (context, settings) {
      // add functionality for select all blocks/views checkboxes
      $('input[id^="edit-checkboxes-all-"]').change(function () {
        $('input', $(this).parent().parent()).prop('checked', $(this).prop('checked'));
      });
    }

  };


}(jQuery));

