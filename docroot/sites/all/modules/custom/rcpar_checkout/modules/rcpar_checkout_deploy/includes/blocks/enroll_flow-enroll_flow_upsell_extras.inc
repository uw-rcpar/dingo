<?php

/**
 * Get the $form_state with values array defined with changes made as defined in 
 * confluance (https://confluence.rogercpareview.com/pages/viewpage.action?pageId=4849673&focusedCommentId=4849760)
 * The block for this file is defined between 'get_' and '_form_state_vals' of function name
 * like: get_<module>_<delta>_form_state_vals
 * This is a common format used by rcpar_checkout_deploy for defining one block per file
 * These values are gotten by debugging the form on submittal in a current enrollment environment
 * 
 * @return array Standard Drupal $form_state array
 */
function rcpar_checkout_deploy_get_enroll_flow_enroll_flow_upsell_extras_form_state_vals() {
  $form_state           = array();
  $form_state['values'] = array(
    'module' => 'enroll_flow',
    'delta' => 'enroll_flow_upsell_extras',
    'title' => 'Popular Add-On Products',
    'css_class' => '',
    'regions' =>
    array(
      'bootstrap' => -1,
      'bootstrap_rcpar' => 'content',
    ),
    'visibility' => '1',
    'pages' => 'cart',
    'roles' =>
    array(),
    'custom' => '0',
    'types' =>
    array(),
    'rcpar_visibility_ignore' => '1',
    'rcpar_visibility_level' => '1',
    'rcpar_visibility_specific' =>
    array(
      'AUD' => 0,
      'BEC' => 0,
      'REG' => 0,
      'FAR' => 0,
    ),
//    'visibility__active_tab' => 'edit-path',
//  'submit' => 'Save block',
//  'form_build_id' => 'form-lkzxJGBH9i-KQ3BhLJzpoiY-g-Rcm6fueZ4DiOdC4Ho',
//  'form_token' => 'hG-GHBwHJj8VpfVLKHWswoDp-rfml0uCFHGxdCbxyRY',
//  'form_id' => 'block_admin_configure',
//  'op' => 'Save block',
  );
  return $form_state;
}
