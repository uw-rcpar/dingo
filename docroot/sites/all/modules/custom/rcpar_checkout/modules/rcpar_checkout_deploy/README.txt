NOTE: see OVERVIEW section which follows the change log
**************************************
* CHANGE LOG
**************************************
16-10-03_06-33
version = 7.x-1.0-alpha3
EN-242 
 - removed config file for block-153
 - add functionality for disabling blocks (not actually used in any of the 
config files yet.)

---------------------------------
16-09-29_17-51 changes block config fixes
version = 7.x-1.0-alpha2
EN-242

---------------------------------
16-09-26_13-54 changes (first version)
version = 7.x-1.0-alpha1

---------------------------------

**************************************
* OVERVIEW
**************************************

This module automates some of the changes needed to activate the enrollment 
project.

---------------------------------
VIEWS:
There one file for each view that needs to be altered, located in:
/includes/views

The view files are named with the pattern:
<view_machine_name>.inc

Each view file has one function named w/ the pattern:
rcpar_checkout_deploy_get_<view_machine_name>_view()

To create this function:
 - export the current view from the working enrollment environment
 - paste the export in the function
 - add "return $view;" as the last line in the function

IF THE VIEW IS TO BE DISABLED:
 - add a line directly after the first export line ("$view = new view();") that reads:
"$view->rcpar_checkout_deploy_disable = TRUE; // used to designate that this view should be disabled"

---------------------------------
BLOCKS:

There is one file for each block that needs to be altered/added, located in:
/includes/blocks

The block files are named with the pattern:
<module>-<delta>.inc

Each block file has one function named w/ the pattern:
rcpar_checkout_deploy_get_<module>_<delta>_form_state_vals()

NOTE: If you need to change a block that already has a file, you can change that 
blocks file here directly without going through the following procedure.

To create this function (how I did it anyway, other ways too I'm sure):
 - in an updated enrollment environment, debug the browser
 - put a breakpoint on the first line in: 
block_admin_configure_submit($form, &$form_state) block_admin.inc:467
 - create a watch variable for var_export($form_state['values'], true)
 - navigate to the config block form using:
/admin/structure/block/manage/<module>/<delta>/configure
if the confluance doc stipulates that the block is to be added, you'll have to 
find the block that was added by searching the block_custom table where the info
field matchs the Block description. This table's bid will be your <delta>. Then 
query the block table for that delta, there will likely be > 1 row, but they'll 
probably all have the same module, that's your <module>

 - submit the form, it should break on the breakpoint you set.
 - save the value of the watch variable "var_export($form_state['values'], true)"
The value of this variable needs to be massaged a bit:
   - the arrays of 'roles' & 'types' will show many (usually all) keys with values = '0'.
All keys with the value of 0 should be deleted from the array. (If all had values == 0, 
then just leave an empty array();
   - if there is a 'body' value, you can search & replace '\r\r' with '\r'. 
(since we're talking html, extra carridge returns don't really matter, you could
skip this step.)
   - for the commerce_checkout_progress/indication block, I had to grab the 
$form_state variable from a breakpoint @ the beginning of 
commerce_checkout_progress_form_block_admin_configure_submit, as that module put's 
this submit callback in the front of the queue. Note that if more blocks are 
added to be configured, similar checks must be made.

 - start your function w/:
$form_state = array();
$form_state['values'] = 

 - then paste the value of your saved watch variable and add ";"

 - add the following last line to the function:
return $form_state;

To set the block to be disabled, add the following to the $form_state['values'] array:
  'rcpar_checkout_deploy_disable' => TRUE, // setting this value will cause the block to be disabled in all themes
(add it as the first key in the config file so it will be easy to find.)

 NOTE: I commented out the last 6 lines from the pasted watch variable array like:
//  'visibility__active_tab' => 'edit-path',
//  'submit' => 'Save block',
//  'form_build_id' => 'form-t_OQ_jfB8Tb5CSskT4N6L_gBkY2yUAWbLnjC8iBWg1M',
//  'form_token' => 'hG-GHBwHJj8VpfVLKHWswoDp-rfml0uCFHGxdCbxyRY',
//  'form_id' => 'block_admin_configure',
//  'op' => 'Save block',

as they aren't needed, but I think the function will work with them uncommented
