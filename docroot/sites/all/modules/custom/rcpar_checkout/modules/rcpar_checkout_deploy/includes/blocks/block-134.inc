<?php

/**
 * Get the $form_state with values array defined with changes made as defined in 
 * confluance (https://confluence.rogercpareview.com/pages/viewpage.action?pageId=4849673&focusedCommentId=4849760)
 * The block for this file is defined between 'get_' and '_form_state_vals' of function name
 * like: get_<module>_<delta>_form_state_vals
 * This is a common format used by rcpar_checkout_deploy for defining one block per file
 * These values are gotten by debugging the form on submittal in a current enrollment environment
 * 
 * @return array Standard Drupal $form_state array
 */
function rcpar_checkout_deploy_get_block_134_form_state_vals() {
  $form_state           = array();
  $form_state['values'] = array (
  'module' => 'block',
  'delta' => '134',
  'title' => 'Popular Add-On Products',
  'info' => 'Cart messaging',
  'body' => 
  array (
    'value' => '    <div class="row first-row">
        <div class="col-sm-12 ">
            <div class="page-heading cart-heading">
                <div class="field-items cart-mobile">
                    <div class="field-item even" property="dc:title">
                        <div class="cart-title"><img src="/sites/all/themes/bootstrap_rcpar/css/img/icon_cart.svg" height="48" />Your Cart</div>
                    </div>
                </div>
                <div class="field-items">
                    <div class="field-item even" property="dc:title">
                        <h1 class="page-title">Thank you for choosing Roger CPA Review! </h1>
                    </div>
                </div>
                 <div class="field field-name-field-intro-text field-type-text-long field-label-hidden">
                <div class="field-items">
                    <div class="field-item even">
                        <p>Below you will find an itemized list of the course packages and/or products you’ve selected.</p>
                    </div>
                </div>
            </div>
            </div>           
        </div>
    </div>
',
    'format' => 'full_html',
  ),
  'css_class' => '',
  'regions' => 
  array (
    'bootstrap' => -1,
    'bootstrap_rcpar' => 'content',
  ),
  'visibility' => '1',
  'pages' => 'cart',
  'roles' => 
  array (),
  'custom' => '0',
  'types' => 
  array (),
  'rcpar_visibility_ignore' => '1',
  'rcpar_visibility_level' => '1',
  'rcpar_visibility_specific' => 
  array (
    'AUD' => 'AUD',
    'BEC' => 'BEC',
    'REG' => 'REG',
    'FAR' => 'FAR',
  ),
  'visibility__active_tab' => 'edit-path',
  'submit' => 'Save block',
  'form_build_id' => 'form-L-R1jeJrLK26iOGV48kwijYMYCtQpNGaue2MxN0ADHA',
  'form_token' => 'GKwFf4Pj_xo8GX1reFHBPhkYsomHU2Y9kuyOXKppq1A',
  'form_id' => 'block_admin_configure',
  'op' => 'Save block',
);
  return $form_state;
}
