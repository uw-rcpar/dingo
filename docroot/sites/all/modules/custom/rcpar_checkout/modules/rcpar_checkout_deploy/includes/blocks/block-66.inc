<?php

/**
 * Get the $form_state with values array defined with changes made as defined in 
 * confluance (https://confluence.rogercpareview.com/pages/viewpage.action?pageId=4849673&focusedCommentId=4849760)
 * The block for this file is defined between 'get_' and '_form_state_vals' of function name
 * like: get_<module>_<delta>_form_state_vals
 * This is a common format used by rcpar_checkout_deploy for defining one block per file
 * These values are gotten by debugging the form on submittal in a current enrollment environment
 * 
 * @return array Standard Drupal $form_state array
 */
function rcpar_checkout_deploy_get_block_66_form_state_vals() {
  $form_state           = array();
  $form_state['values'] = array(
    'module' => 'block', // used
    'delta' => '66', // used
    'title' => 'Create an Account', // used
    'info' => 'Create an Account - login page', // used block_custom_block_save
    'body' => // used block_custom_block_save
    array(
      'value' => '<div class="register-copy">I do not have an account and need to create one</div>

<div class="register-link-wrapper"><a class="content-btn-link">Create Account</a></div>',
      'format' => 'full_html', // used block_custom_block_save
    ),
    'css_class' => '',
    'regions' =>  // used
    array(
      'bootstrap' => 'sidebar_first',
      'bootstrap_rcpar' => 'content',
    ),
    'visibility' => '2', // used
    'pages' => '<?php return enroll_flow_display_login_block(); ?>', // used
    'roles' => // used
    array(
      1 => '1',
    ),
    'custom' => '0', // used
    'types' => array(),
    array(),
    'rcpar_visibility_ignore' => '1',
    'rcpar_visibility_level' => '1',
    'rcpar_visibility_specific' =>
    array(
      'AUD' => 0,
      'BEC' => 0,
      'REG' => 0,
      'FAR' => 0,
    ),
//  'visibility__active_tab' => 'edit-path',
//  'submit' => 'Save block',
//  'form_build_id' => 'form-t_OQ_jfB8Tb5CSskT4N6L_gBkY2yUAWbLnjC8iBWg1M',
//  'form_token' => 'hG-GHBwHJj8VpfVLKHWswoDp-rfml0uCFHGxdCbxyRY',
//  'form_id' => 'block_admin_configure',
//  'op' => 'Save block',
  );
  return $form_state;
}
