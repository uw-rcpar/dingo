<?php

/**
 * Get the $form_state with values array defined with changes made as defined in 
 * confluance (https://confluence.rogercpareview.com/pages/viewpage.action?pageId=4849673&focusedCommentId=4849760)
 * The block for this file is defined between 'get_' and '_form_state_vals' of function name
 * like: get_<module>_<delta>_form_state_vals
 * This is a common format used by rcpar_checkout_deploy for defining one block per file
 * These values are gotten by debugging the form on submittal in a current enrollment environment
 * 
 * @return array Standard Drupal $form_state array
 */
function rcpar_checkout_deploy_get_commerce_checkout_progress_indication_form_state_vals() {
  $form_state           = array();
  $form_state['values'] = array (
  'module' => 'commerce_checkout_progress',
  'delta' => 'indication',
  'title' => '',
  'css_class' => '',
  'regions' => 
  array (
    'bootstrap' => -1,
    'bootstrap_rcpar' => -1,
  ),
  'visibility' => 1,
  'pages' => 
  array (
    'cart' => 'cart',
    'login' => 'login',
    'shipping' => 'shipping',
    'review' => 'review',
    'checkout' => 'checkout',
    'payment' => 'payment',
    'complete' => 'complete',
  ),
  'roles' => 
  array (),
  'custom' => '0',
  'types' => 
  array (),
  'rcpar_visibility_ignore' => '1',
  'rcpar_visibility_level' => '1',
  'rcpar_visibility_specific' => 
  array (
    'AUD' => 'AUD',
    'BEC' => 'BEC',
    'REG' => 'REG',
    'FAR' => 'FAR',
  ),
  'visibility__active_tab' => 'edit-path',
  'submit' => 'Save block',
  'form_build_id' => 'form-4-5ieZITvW6E5kSGAE_KEj4CeYXJ4q6etr1q1bRqDeo',
  'form_token' => 'GKwFf4Pj_xo8GX1reFHBPhkYsomHU2Y9kuyOXKppq1A',
  'form_id' => 'block_admin_configure',
  'op' => 'Save block',
);
  return $form_state;
}
