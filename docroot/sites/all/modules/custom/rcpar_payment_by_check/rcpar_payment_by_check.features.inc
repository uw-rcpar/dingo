<?php
/**
 * @file
 * rcpar_payment_by_check.features.inc
 */

/**
 * Implements hook_commerce_coupon_default_types().
 */
function rcpar_payment_by_check_commerce_coupon_default_types() {
  $items = array(
    'pay_by_check_coupons' => array(
      'type' => 'pay_by_check_coupons',
      'label' => 'Pay by Check coupons',
      'weight' => 0,
      'data' => NULL,
      'status' => 1,
      'module' => NULL,
      'rdf_mapping' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function rcpar_payment_by_check_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: pay_by_check_coupons
  $schemaorg['commerce_coupon']['pay_by_check_coupons'] = array(
    'commerce_coupon_percent_amount' => array(
      'predicates' => array(),
    ),
    'field_commerce_couponprodref' => array(
      'predicates' => array(),
    ),
  );

  return $schemaorg;
}
