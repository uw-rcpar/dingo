<?php
/**
 * @file
 * rcpar_payment_by_check.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function rcpar_payment_by_check_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'commerce_coupon_percent_amount'.
  $field_bases['commerce_coupon_percent_amount'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'commerce_coupon',
    ),
    'field_name' => 'commerce_coupon_percent_amount',
    'indexes' => array(),
    'label' => 'Percentage Amount',
    'locked' => 0,
    'module' => 'number',
    'settings' => array(
      'decimal_separator' => '.',
      'precision' => 10,
      'scale' => 2,
    ),
    'translatable' => 0,
    'type' => 'number_decimal',
  );

  // Exported field_base: 'field_commerce_couponprodref'.
  $field_bases['field_commerce_couponprodref'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_commerce_couponprodref',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'rcpar_mods_single',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'cart_upsell' => 'cart_upsell',
          'core_products' => 'core_products',
          'course_extension' => 'course_extension',
          'merchandise' => 'merchandise',
          'product' => 'product',
          'upsell' => 'upsell',
        ),
      ),
      'handler_submit' => 'Change handler',
      'target_type' => 'commerce_product',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
