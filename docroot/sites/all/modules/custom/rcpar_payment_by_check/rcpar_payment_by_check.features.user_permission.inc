<?php
/**
 * @file
 * rcpar_payment_by_check.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rcpar_payment_by_check_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create commerce_coupon entities of bundle pay_by_check_coupons'.
  $permissions['create commerce_coupon entities of bundle pay_by_check_coupons'] = array(
    'name' => 'create commerce_coupon entities of bundle pay_by_check_coupons',
    'roles' => array(),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'edit any commerce_coupon entity of bundle pay_by_check_coupons'.
  $permissions['edit any commerce_coupon entity of bundle pay_by_check_coupons'] = array(
    'name' => 'edit any commerce_coupon entity of bundle pay_by_check_coupons',
    'roles' => array(),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'edit own commerce_coupon entities of bundle pay_by_check_coupons'.
  $permissions['edit own commerce_coupon entities of bundle pay_by_check_coupons'] = array(
    'name' => 'edit own commerce_coupon entities of bundle pay_by_check_coupons',
    'roles' => array(),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'view any commerce_coupon entity of bundle pay_by_check_coupons'.
  $permissions['view any commerce_coupon entity of bundle pay_by_check_coupons'] = array(
    'name' => 'view any commerce_coupon entity of bundle pay_by_check_coupons',
    'roles' => array(),
    'module' => 'commerce_coupon',
  );

  // Exported permission: 'view own commerce_coupon entities of bundle pay_by_check_coupons'.
  $permissions['view own commerce_coupon entities of bundle pay_by_check_coupons'] = array(
    'name' => 'view own commerce_coupon entities of bundle pay_by_check_coupons',
    'roles' => array(),
    'module' => 'commerce_coupon',
  );

  return $permissions;
}
