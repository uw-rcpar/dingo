<?php
/**
 * @file
 * rcpar_payment_by_check.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function rcpar_payment_by_check_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_create-name:admin/commerce/coupons/add/pay-by-check-coupons.
  $menu_links['management_create-name:admin/commerce/coupons/add/pay-by-check-coupons'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/coupons/add/pay-by-check-coupons',
    'router_path' => 'admin/commerce/coupons/add/pay-by-check-coupons',
    'link_title' => 'Create !name',
    'options' => array(
      'identifier' => 'management_create-name:admin/commerce/coupons/add/pay-by-check-coupons',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_create-coupon:admin/commerce/coupons/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Create !name');

  return $menu_links;
}
