<?php
/**
 * @file
 * rcpar_payment_by_check.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rcpar_payment_by_check_default_rules_configuration() {
  $items = array();
  $items['commerce_payment_rcpar_pay_by_check'] = entity_import('rules_config', '{ "commerce_payment_rcpar_pay_by_check" : {
      "LABEL" : "Pay by Check",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Commerce Payment" ],
      "REQUIRES" : [ "rules", "commerce_payment" ],
      "ON" : { "commerce_payment_methods" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "commerce-order:owner" ],
            "roles" : { "value" : { "3" : "3", "12" : "12" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "commerce_payment_enable_rcpar_pay_by_check" : {
            "commerce_order" : [ "commerce-order" ],
            "payment_method" : "rcpar_pay_by_check"
          }
        }
      ]
    }
  }');
  $items['rules_calculate_coupon_with_pay_by_check_option'] = entity_import('rules_config', '{ "rules_calculate_coupon_with_pay_by_check_option" : {
      "LABEL" : "Calculate coupon with Pay by Check option",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "php", "commerce_coupon" ],
      "ON" : { "commerce_coupon_redeem" : [] },
      "IF" : [
        { "data_is" : { "data" : [ "coupon:type" ], "value" : "pay_by_check_coupons" } },
        { "entity_has_field" : { "entity" : [ "coupon" ], "field" : "commerce_coupon_percent_amount" } },
        { "NOT AND" : [
            { "data_is_empty" : { "data" : [ "coupon:commerce-coupon-percent-amount" ] } }
          ]
        },
        { "entity_has_field" : {
            "entity" : [ "commerce_order" ],
            "field" : "commerce_coupon_order_reference"
          }
        },
        { "data_is" : {
            "data" : [ "coupon:commerce-coupon-percent-amount" ],
            "op" : "\\u003E",
            "value" : 1
          }
        },
        { "data_is" : { "data" : [ "coupon:is-active" ], "op" : "=", "value" : true } },
        { "NOT php_eval" : { "code" : "return enroll_flow_order_has_coupon($commerce_order);" } }
      ],
      "DO" : [
        { "list_add" : {
            "list" : [ "commerce-order:commerce-coupon-order-reference" ],
            "item" : [ "coupon" ],
            "unique" : 1
          }
        },
        { "commerce_coupon_action_create_coupon_line_item" : {
            "USING" : {
              "commerce_coupon" : [ "coupon" ],
              "commerce_order" : [ "commerce_order" ],
              "amount" : "0",
              "component_name" : [ "coupon:price-component-name" ],
              "currency_code" : "USD"
            },
            "PROVIDE" : { "commerce_coupon_line_item" : { "commerce_coupon_line_item" : "commerce coupon line item" } }
          }
        }
      ]
    }
  }');
  return $items;
}
