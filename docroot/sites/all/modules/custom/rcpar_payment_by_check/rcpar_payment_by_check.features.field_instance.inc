<?php
/**
 * @file
 * rcpar_payment_by_check.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function rcpar_payment_by_check_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'commerce_coupon-pay_by_check_coupons-commerce_coupon_code'.
  $field_instances['commerce_coupon-pay_by_check_coupons-commerce_coupon_code'] = array(
    'bundle' => 'pay_by_check_coupons',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_code',
    'label' => 'Coupon Code',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-pay_by_check_coupons-commerce_coupon_number_of_uses'.
  $field_instances['commerce_coupon-pay_by_check_coupons-commerce_coupon_number_of_uses'] = array(
    'bundle' => 'pay_by_check_coupons',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Number of times that coupon code can be used by any customer on the site, before it is set to inactive',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_number_of_uses',
    'label' => 'Maximum number of Uses',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-pay_by_check_coupons-commerce_coupon_percent_amount'.
  $field_instances['commerce_coupon-pay_by_check_coupons-commerce_coupon_percent_amount'] = array(
    'bundle' => 'pay_by_check_coupons',
    'default_value' => array(
      0 => array(
        'value' => 100,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 2,
          'thousand_separator' => '',
        ),
        'type' => 'number_decimal',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_percent_amount',
    'label' => 'Percentage Amount',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-pay_by_check_coupons-field_commerce_couponprodref'.
  $field_instances['commerce_coupon-pay_by_check_coupons-field_commerce_couponprodref'] = array(
    'bundle' => 'pay_by_check_coupons',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_commerce_couponprodref',
    'label' => 'Products',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'options_select',
      'weight' => 5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Coupon Code');
  t('Maximum number of Uses');
  t('Number of times that coupon code can be used by any customer on the site, before it is set to inactive');
  t('Percentage Amount');
  t('Products');

  return $field_instances;
}
