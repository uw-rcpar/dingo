<?php

/**
 * Create a form to process entitlements access flag
 */
function user_entitlements_ipq_access_form() {
  $form['submit'] = array('#type' => 'submit', '#value' => t('Process'));

  return $form;
}

/**
 * prepare the operations to run batch
 */
function user_entitlements_ipq_access_form_submit($form, &$form_data) {


  $query = db_select('node', 'n');
  $query->fields('n', array('nid'));
  $query->innerJoin('field_data_field_course_type_ref', 'ft', 'ft.entity_id = n.nid and ft.field_course_type_ref_tid = ' . COURSE_TYPE_ONLINE_COURSE);
  $query->condition('n.type', 'user_entitlement_product');

  $subquery = db_select('field_data_field_ipq_access', 'ipq');
  $subquery->fields('ipq', array('entity_id'));
  $subquery->condition('field_ipq_access_value', 1);
  $query->condition('n.nid', $subquery, 'NOT IN');
  $query->range(0, 10000);
  $result = $query->execute();
  while ($record = $result->fetchAssoc()) {
    $operations[] = array(
      'user_entitlements_ipq_access_operation',
      array(
        'nid' => $record['nid'],
      ),
    );
  }



  $batch = array(
    'operations' => $operations,
    'title' => t('Procesing IPQ Access Flags'),
    'finished' => 'user_entitlements_ipq_access_finished',
    'init_message' => t('Preparing to process entitlements'),
    'progress_message' => t('Processed @current out of @total entitlements.'),
    'error_message' => t('Error Importing Records'),
  );
  batch_set($batch);
}

/**
 * operation of the batch to put all "ipq access flag" to on.
 */
function user_entitlements_ipq_access_operation($nid, &$context) {
  try {
    //we save only one field to avoid saving the whole node
    //and make the process much faster.
    $node = node_load($nid);
    $node->field_ipq_access[LANGUAGE_NONE][0]['value'] = 1;
    field_attach_presave('node', $node);
    field_attach_update('node', $node);
    // Clear the static loading cache.
    entity_get_controller('node')->resetCache(array($node->nid));
    $context['message'] = "Processing entitlement with id {$nid}";
    $context['results'][] = $nid;
  } catch (Exception $ex) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $ex->getMessage());
  }
}

/**
 * last batch operation. we print the remaining items after finish the processing.
 */
function user_entitlements_ipq_access_finished($success, $results, $operations) {
  if ($success) {
    $query = db_select('node', 'n');
    $query->addExpression('count(nid)', 'total');
    $query->innerJoin('field_data_field_course_type_ref', 'ft', 'ft.entity_id = n.nid');
    $query->condition('n.type', 'user_entitlement_product');
    $query->condition('ft.field_course_type_ref_tid', COURSE_TYPE_ONLINE_COURSE);

    $subquery = db_select('field_data_field_ipq_access', 'ipq');
    $subquery->fields('ipq', array('entity_id'));
    $subquery->condition('field_ipq_access_value', 1);
    $query->condition('n.nid', $subquery, 'NOT IN');
    $pending = $query->execute()->fetchObject()->total;

    drupal_set_message(t('There were processed @count entitlements there is still pending @pending entitlments to process.', array('@count' => count($results), '@pending' => $pending)));
  } else {
    // An error occurred.    
    $error_operation = reset($operations);
    drupal_set_message(
        t('An error occurred while processing @operation with arguments : @args', array(
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
            )
        ), 'error'
    );
  }
}
