<?php

/**
 * @file
 * This file contains a batch process to populate the hhc_access field on
 * user entitlements in order to provide the correct access levels - read, write, none
 * to the Homework Help Center. After running once this file is only kept for example/archive purposes.
 */


/**
 * Create a form to process entitlements hhc access flag
 */
function user_entitlements_hhc_access_form() {
  $form['submit'] = array('#type' => 'submit', '#value' => t('Process'));

  return $form;
}

/**
 * Prepare the operations to run a batch
 */
function user_entitlements_hhc_access_form_submit($form, &$form_data) {
  // Boost memory for this function
  ini_set('memory_limit','1024M');
  // All we are interested in is active, unexpired entitlements
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'user_entitlement_product')
    ->fieldCondition('field_product_sku', 'value', array('AUD','BEC','FAR','REG'), 'IN')
    ->fieldCondition('field_expiration_date', 'value', REQUEST_TIME, '>')
    ->propertyCondition('status', 1);
  $result = $query->execute();
  $nids = array_keys($result['node']);
  // Process all our entitlement nodes
  foreach ($nids as $nid) {
    $operations[] = array(
      'user_entitlements_hhc_access_operation',
      array(
        'nid' => $nid,
      ),
    );
  }

  $batch = array(
    'operations' => $operations,
    'title' => t('Procesing HHC Access Flags'),
    'finished' => 'user_entitlements_hhc_access_finished',
    'init_message' => t('Preparing to process entitlements'),
    'progress_message' => t('Processed @current out of @total entitlements.'),
    'error_message' => t('Error Importing Records'),
  );
  batch_set($batch);
}

/**
 * Batch operation to set the correct hhc access permission.
 */
function user_entitlements_hhc_access_operation($nid, &$context) {
  try {
    // Boost memory for this function
    ini_set('memory_limit','1024M');
    // Load up the entitlement
    $node = node_load($nid);
    // Get our perm value - none, read or write
    $hcenter_perm = user_entitlement_get_hhc_access_perm($node);
    // We save only one field to avoid saving the whole node
    // and make the process much faster.
    $node->field_hhc_access[LANGUAGE_NONE][0]['value'] = $hcenter_perm;
    field_attach_presave('node', $node);
    field_attach_update('node', $node);
    // Clear the static loading cache.
    entity_get_controller('node')->resetCache(array($node->nid));
    $context['message'] = "Processing entitlement with id {$nid}";
    $context['results'][] = $nid;
  } catch (Exception $ex) {
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $ex->getMessage());
  }
}

/**
 * Wrap up the batch process.
 */
function user_entitlements_hhc_access_finished($success, $results, $operations) {
  // Boost memory for this function
  ini_set('memory_limit','1024M');
  if ($success) {
    drupal_set_message(t('@count entitlements processed.', array('@count' => count($results))));
  } else {
    // An error occurred.
    $error_operation = reset($operations);
    drupal_set_message(
      t('An error occurred while processing @operation with arguments : @args', array(
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        )
      ), 'error'
    );
  }
}

/**
 * Helper function to work out a particular entitlement's HHC permissions. Note that this function
 * borrows heavily from rcpa_hcenter_get_user_access()
 * @param obj $node
 */
function user_entitlement_get_hhc_access_perm($node) {
  // rules for homework center access are
  // WRITE:
  // - normal user - partner_nid == 0
  // - DB Enrollees - partner type: (any), partner billing type: Direct Bill (directbill)
  // - Firm/Univ Discount - partner type: (by def not Normal), partner billing type: Credit Card/Affirm (creditcard)
  // - FSL - partner type: Firm/Partner (1), partner billing type: Free Access (freetrial)
  // READ:
  // - Free Trial - partner type: Normal (1), partner billing type: Free Access (freetrial)
  // NONE:
  // - ASL - partner type: University (3), partner billing type: Free Access (freetrial)
  // assume the worst
  $hcenter_perm = '_none';
  $sku = $node->field_product_sku[LANGUAGE_NONE][0]['value'];
  // Get our online course skus
  $standard_online_couse_skus = rcpar_dashboard_entitlements_options();


  if (in_array($sku, $standard_online_couse_skus)) { // only need to check standard online courses
    if ($node->field_partner_id[LANGUAGE_NONE][0]['value'] == 0) { // no partner usual purchase
      $hcenter_perm = 'write';
      return $hcenter_perm;
    }
    // load the partner for this product
    $partner_class = new RCPARPartner($node->field_partner_id[LANGUAGE_NONE][0]['value']);
    // FSL users
    if ($partner_class->isFSL()) {
      $hcenter_perm = 'write';
    }
    else if ($partner_class->isFreeTrial()) {
      $hcenter_perm = 'read';
    }
    else {
      if (!$partner_class->isBillingFreeAccess()) {
        // not free so it's not ASL or FSL, could be DB Enrollee or Firm/Univ Discount
        $hcenter_perm = 'write';
      }
    }
  }
  return $hcenter_perm;

}



