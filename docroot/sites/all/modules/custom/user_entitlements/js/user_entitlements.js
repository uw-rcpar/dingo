(function ($) {
  Drupal.behaviors.user_entitlements = {
    attach: function (context, settings) {

    //bulk operations date field
      var fields = ["#views-form-user-entitlements-page #edit-bundle-user-entitlement-product-field-original-expiration-date-und-0-value",
        "#views-form-user-entitlements-page #edit-bundle-user-entitlement-product-field-expiration-date-und-0-value"];
      for (var i = 0; i < fields.length; i++) {
        myfield = $(fields[i]);
        if (myfield.length) {
          myfield.datepicker();
        }
      }
    }

  };
})(jQuery);