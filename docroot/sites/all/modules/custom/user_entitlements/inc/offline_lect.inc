<?php

function sso_login_offline_lect_course_info(){
  // License keys database:
  $info = array (
    // key generation date
    '2016-05-10' => array(
      'AUD' => array(
        "license_key"=>"OlpNTXiTcuWbESV16Ofum1",
      ),
      'BEC' => array(
        "license_key"=>'ABDMym4cUNVrEtRi6jtOq6',
      ),
      'FAR' => array(
        "license_key"=>"XGSRG58vjb9BJjYBlQvjRP",
      ),
      'REG' => array(
        "license_key"=>"Z6CeMXQ08gB4y7UJi5PVCt",
      ),
      'AUD-CRAM' => array(
        "license_key"=>"oauGnz7ooPmswUytQ6zPBY",
      ),
      'BEC-CRAM' => array(
        "license_key"=>'axLXs9O4MutRtjbJhHy5if',
      ),
      'FAR-CRAM' => array(
        "license_key"=>"sz8JpjgOicZem8L8K1r4V9",
      ),
      'REG-CRAM' => array(
        "license_key"=>"urQhd8WKmg69tTTsNaoUOf",
      )
    ),
  );
  return $info;
}

/**
 * Helper function to check if is a cram course or not
 * Mostly suffixes with "-CRAM" will match
 * @param $id
 * @return bool
 */
function _sso_login_is_cram_course($id){
  $cram_courses = array("AUD-CRAM","BEC-CRAM","FAR-CRAM","REG-CRAM");
  return  in_array($id, $cram_courses) ;
}

function sso_login_offline_lect(){


  //// Read parameters
  $request = (array) json_decode(file_get_contents('php://input'));
  $course = $request['course'];
  $username = $request['username']; // can be a real username or an email
  $password= $request['password'];

  $course_cram = $course;

  if ( _sso_login_is_cram_course($course) ) {
    // Cram courses needs "-SSD"  suffix
    $course = str_replace('CRAM','SSD', $course) ;
  }
  else{
    // Full courses needs to add "-OFF-LEC" suffix - Cram courses: "-CRAM" (expected input)
    $course = $course . '-OFF-LEC';
  }
  $success = false;
  $requested_entitlement_info = null;
  $msg = 'You do not have access to this section or there has been an error with activation. Please try again or if you feel this is in error please contact customer support.';
  $u = user_load_by_mail($username);
  if ($u){
    $username = $u->name;
  }

  $uid = user_authenticate($username, $password);
  if($uid){
    $u = user_load($uid);

    $courses_keys = sso_login_offline_lect_course_info();
    $courses_info = reset($courses_keys);
    $keys_generation_date = key($courses_keys);
    $entitlements = user_entitlements_get_entitlements($u);
    // lets search for the user entitlements inside the user entitlements list
    foreach ($entitlements as $entitlement) {
      try {
        if (is_array($entitlement) && isset($entitlement["#node"])) {
          $node_wrapper = entity_metadata_wrapper('node', $entitlement["#node"]);
          $sku = strtoupper($node_wrapper->field_product_sku->value());
          if ($sku == $course){
            if(count($node_wrapper->field_installs->value()) < $node_wrapper->field_allowed_installs->value()){
              $success = true;
              $msg = 'Success';
              $requested_entitlement_info = $courses_info[$course];
              // we also need the entitlement expiration date
              $requested_entitlement_info['expires_on'] =  strtotime('now +8 months');
              $requested_entitlement_info['license_key'] = $courses_info[$course_cram]['license_key'];

              $node_wrapper->field_installs[] = date('m/d/Y');
              $node_wrapper->save();
              break;
            } else {
              $msg = 'Oops! Our records indicate that you have reached the installation limit for this section of your Offline Lectures. For assistance or additional installations, please contact our Customer Care Team at staff@rogercpareview.com or by calling 877-764-4272.';
            }
          }
        }
      } catch (EntityMetadataWrapperException $exc) {
        watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
    }
  }

  // Send response
  $response = array(
    "success" => $success,
    "entitlement" => $requested_entitlement_info,
    "message" => $msg,
    "key_generation_date" => $keys_generation_date,
  );
  return drupal_json_output($response);
}
