<?php

/**
 * BATCH TO PROCESS ACTIVE STUDENTS FOR MOBILE OFFLINE ACCEESS
 */

/**
 * Drupal form previous to process the batch
 */
function user_entitlements_active_mobile_batch_form($form, $form_state) {
  $form['description'] = array(
    '#markup' => 'Allow existent users with active entitlements to have access to the mobile offline<br>'
  );

  $form['actions'] = array(
    '#type' => 'submit',
    '#value' => t('Process Entitlements'),
  );

  return $form;
}

/**
 * form submission for the batch
 */
function user_entitlements_active_mobile_batch_form_submit($form, $form_state) {
  $items_to_process = variable_get('items_to_process', 10000);
  $items_per_operations = variable_get('items_per_operations', 1000);

  //we query all the active entitlements that are not active to process them.
  //is important to make the validation to only not processed in case the batch failed
  //we can start processing the remaingin ones and not duplicate the effort
  $result = db_query("
        SELECT n.nid from node n
        INNER JOIN field_data_field_expiration_date fe on fe.entity_id=n.nid and fe.`field_expiration_date_value` > UNIX_TIMESTAMP()
        INNER JOIN field_data_field_bundled_product b on b.entity_id=n.nid and b.field_bundled_product_value in ('FULL-ELITE-DIS','FULL-PREM-DIS')
        INNER JOIN field_data_field_course_section s on s.entity_id=n.nid and s.field_course_section_tid in (1452,1455,1453,1454)
        INNER JOIN field_data_field_course_type_ref t on t.entity_id=n.nid and t.field_course_type_ref_tid in (1464,1466)
        WHERE n.nid not in (SELECT entity_id FROM field_data_field_mobile_offline_access where field_mobile_offline_access_value=1)
        LIMIT {$items_to_process};");

  $i = 0;
  $nodes = $operations = array();
  foreach ($result as $record) {
    //process on pieces 
    if ($i < $items_per_operations) {
      $nodes[$record->nid] = $record->nid;
      $i++;
      continue;
    }

    $operations[] = array(
      'user_entitlements_active_mobile_batch_operation',
      array(
        'nids' => $nodes,
      )
    );
    $i = 0;
    $nodes = array();
  }

  //process the last batch which might be lower than the items to process so 
  //we avoid those items to be lost
  if (count($nodes)) {
    $operations[] = array(
      'user_entitlements_active_mobile_batch_operation',
      array(
        'nids' => $nodes,
      )
    );
  }

  //if we have room on the items to process we fill them up with the delayed
  //activation entitlements, so we can start procesing those.
  $items_to_process = $items_to_process - count($operations);

  if ($items_to_process > 0) {

    //bring all delayed activations entitlements, no matter the date, but it has to belong to
    //the packages and be cram courses to be processed.
    $result = db_query("
        SELECT n.nid from node n
        INNER JOIN field_data_field_bundled_product b on b.entity_id=n.nid and b.field_bundled_product_value in ('FULL-ELITE-DIS','FULL-PREM-DIS')
        INNER JOIN field_data_field_course_section s on s.entity_id=n.nid and s.field_course_section_tid in (1452,1455,1453,1454)
        INNER JOIN field_data_field_course_type_ref t on t.entity_id=n.nid and t.field_course_type_ref_tid in (1466)
        INNER JOIN field_data_field_activation_delayed ad on ad.entity_id=n.nid and ad.field_activation_delayed_value = 1
        WHERE n.nid not in (SELECT entity_id FROM field_data_field_mobile_offline_access where field_mobile_offline_access_value=1)
        LIMIT {$items_to_process};");

    $i = 0;
    $nodes = array();
    foreach ($result as $record) {
      //process on pieces 
      if ($i < $items_per_operations) {
        $nodes[$record->nid] = $record->nid;
        $i++;
        continue;
      }

      $operations[] = array(
        'user_entitlements_active_mobile_batch_operation',
        array(
          'nids' => $nodes,
        )
      );
      $i = 0;
      $nodes = array();
    }

    //process the last batch which might be lower than the items to process so 
    //we avoid those items to be lost

    if (count($nodes)) {
      $operations[] = array(
        'user_entitlements_active_mobile_batch_operation',
        array(
          'nids' => $nodes,
        )
      );
    }
  }


  $batch = array(
    'operations' => $operations,
    'finished' => 'user_entitlements_active_mobile_batch_finished',
    'title' => t('Updating Entitlements'),
    'init_message' => t('Inicializing Update of entitlements.'),
    'progress_message' => t('Processed @current out of @total Entitlements.'),
    'error_message' => t('No entitlements were updated'),
    'file' => drupal_get_path('module', 'user_entitlements') . '/inc/user_entitlements.active.mobile.batch.inc',
  );
  batch_set($batch);
}

/**
 * Operation Callback
 * @param <array> $nids list of nodes to be processed 
 * @param type $context
 */
function user_entitlements_active_mobile_batch_operation($nids, &$context) {

  foreach ($nids as $nid) {
    $node = node_load($nid);
    $node->field_mobile_offline_access[LANGUAGE_NONE][0]['value'] = 1;
    field_attach_presave('node', $node);
    field_attach_update('node', $node);
  }
}

/**
 * Finished callback
 */
function user_entitlements_active_mobile_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message("Copy Process Complete Succesfully");
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(
        t('An error occurred while processing @operation with arguments : @args', array(
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE))
        ), 'error'
    );
  }
}
