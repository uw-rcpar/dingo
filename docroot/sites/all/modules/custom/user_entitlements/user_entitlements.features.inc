<?php
/**
 * @file
 * user_entitlements.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function user_entitlements_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function user_entitlements_node_info() {
  $items = array(
    'user_entitlement_product' => array(
      'name' => t('User Entitlement Product'),
      'base' => 'node_content',
      'description' => t('Product and related information about the status of the product: expiration, duration, interval, etc.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
