<?php
/**
 * @file
 * user_entitlements.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function user_entitlements_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_authorization|node|user_entitlement_product|form';
  $field_group->group_name = 'group_authorization';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'user_entitlement_product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Authorization',
    'weight' => '22',
    'children' => array(
      0 => 'field_allowed_installs',
      1 => 'field_installs',
      2 => 'field_serial_number',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-authorization field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_authorization|node|user_entitlement_product|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Authorization');

  return $field_groups;
}
