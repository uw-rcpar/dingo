<?php

/**
 * Drush command implementation
 *
 */ 

/**
 * Implements hook_drush_help().
 */
function streaml_blogger_importer_drush_help($command) {
  switch ($command) {
    case 'drush:streaml-importer':
      return dt('Run my command');
      break;
    case 'drush:consistency-check-blog':
      return dt('Run my command');
      break;
    case 'drush:consistency-correct-missing-entit':
      return dt('Run my command');
      break;
    case 'drush:consistency-get-rev-data':
      return dt('Run my command');
      break;      
  }
}

/**
 * Implementation of hook_drush_command().
 */
function streaml_blogger_importer_drush_command() {
  $items = array();
  // Name of the drush command.
  $items['streaml-importer'] = array(
    'description' => 'Run streamline importer',
    'arguments' => array(
      'slave' => 'Slave site URL, starts with http and ending with /.',
    ),    
    'callback' => 'streaml_blogger_importer_drush_streaml_importer',
    'aliases' => array('blgimp'),
  );
  $items['consistency-check-blog'] = array(
    'description' => 'Shows a report about the status of nodes overwriten after blog copy',
    'callback' => 'streaml_consistency_check_blog_drush_streaml_importer',
    'aliases' => array('ccblog'),
  );
  $items['consistency-correct-missing-entit'] = array(
    'description' => 'Restore missing entitlements',
    'callback' => 'streaml_consistency_correct_missing_entit_drush_streaml_importer',
    'aliases' => array('ccmissingent'),
  );
  $items['consistency-correct-missing-notes'] = array(
    'description' => 'Restore missing topic notes',
    'callback' => 'streaml_consistency_correct_missing_topic_notes_drush_streaml_importer',
    'aliases' => array('ccmissingnotes'),
  );
  $items['consistency-correct-missing-video-history'] = array(
    'description' => 'Restore missing topic notes',
    'callback' => 'streaml_consistency_correct_missing_video_history_drush_streaml_importer',
    'aliases' => array('ccmissingvideohistory'),
  );
  $items['consistency-get-rev-data'] = array(
    'description' => 'Print existing revision data for a particular',
    'arguments' => array(
      'vid' => 'Revision id',
      'nid' => 'Node id',
    ),     
    'callback' => 'streaml_consistency_get_rev_data_drush_streaml_importer',
    'aliases' => array('ccrevd'),
  );  
  return $items;
}

function streaml_blogger_importer_drush_streaml_importer($file = NULL) {
    global $entries;
    global $current_entry;
    global $current_property;
    global $base_url;
    global $user;

    //check if the argument was passed in and just print it out
    if (!isset($file)) {
        drush_print($file);
        drush_log('need to pass the xml as an argument', 'error');
        return;
    }
 
    global $base_url;
    // TODO: UGLY patch, remove it after correcly set base_url
    if ($base_url == "http://default"){
       $base_url = "http://ec2-75-101-143-139.compute-1.amazonaws.com";
    }

    //$file = "blog-04-06-2015.xml";
    //$file = "/var/www/html/streaml/rogercpar/blog-04-06-2015.xml";


    $tags_equivalency = array(
        "Accounting Industry" => array(
            "tid" => 1,
            "tags" => array(
                "accounting students","accountants","accounting industry",
                "accounting exam","accounting club","American Accounting Association","financial accounting",
            )
        ),
        "AICPA" => array(
            "tid" => 2,
            "tags" => array(
                "AICPA"
            )
        ),
        "Become a CPA" => array(
            "tid" => 3,
            "tags" => array(
                "this way to CPA","international cpa","become a cpa","becoming a cpa","how to apply for cpa exam","cpa exam application process","how to become a cpa","forensic accounting","reasons to be a cpa",
            )
        ),
        "CPA Exam Review" => array(
            "tid" => 4,
            "tags" => array(
                "CPA exam prep","CPA exam updates","cpa exam questions","CPA Exam Courses",
                "CPA exam guest blog","cpa exam","cpa exam review","free cpa reviews",
                "CPA exam FAQs","2011 cpa exam","CPA Exam Review in Japan",
                "cpa exam results","materials","US CPA Exam","exam changes","roger cpa review"
            )
        ),
        "CPA Licensure" => array(
            "tid" => 5,
            "tags" => array(
                "cpa career tips","cpa career","CPA licensure","2014 cpa licensure"
            )
        ),
        "Education" => array(
            "tid" => 6,
            "tags" => array(
                "study planners","student of the month",
                "college","students","careers","study environment",
                "scholarship","Final Exams","Tutoring Center","education","education requirements","cpa study material"
            )
        ),
        "Study Tips" => array(
            "tid" => 7,
            "tags" => array(
                "cpa exam study Tips","cpa study tips",
                "The Roger Method","cpa exam tips","cpa tips","cpa exam tip study tips",
                "cpa recruiting tips","study tips","cpa review study tips","cpa review course","free cpa review",
                "tips","how to prepare","cpa review study tips"
            )
        ),
        "What to Expect on The Day of the Exam" => array(
            "tid" => 8,
            "tags" => array(
                "cpa exam day","Pass the CPA Exam","sit for the CPA Exam","cpa review",
                "2011 Changes","CPA exam changes","What to Expect on The Day of the Exam",
            )
        ),
    );

    drush_log('Starting importing process', 'ok');

    _streaml_blogger_import_log("starting import process...");


    $entries = array();
    $current_entry = null;

    _streaml_blogger_import_log("parsing " . $file . " file");
    $xml_parser = xml_parser_create();
    xml_set_element_handler($xml_parser, "_streaml_imp_startElement", "_streaml_imp_endElement");
    xml_set_character_data_handler($xml_parser, "_streaml_imp_characterData");
    if (!($fp = fopen($file, "r"))) {
        die("could not open XML input");
    }

    while ($data = fread($fp, 4096)) {
        if (!xml_parse($xml_parser, $data, feof($fp))) {
            die(sprintf("XML error: %s at line %d",
                xml_error_string(xml_get_error_code($xml_parser)),
                xml_get_current_line_number($xml_parser)));
        }
    }
    xml_parser_free($xml_parser);

    $posts = array();
    $comments_count = 0;

    // first we create all the posts
    drush_log('parsing finished creating Drupal entities', 'ok');
    drush_log('creating posts', 'ok');
    _streaml_blogger_import_log("parsing finished creating Drupal entities");
    foreach($entries as $e){
        if( $e["CATEGORY"]["attrs"]["TERM"] == "http://schemas.google.com/blogger/2008/kind#post"){

            // there's a particular post without data (just one)
            // TODO: we must find out what we should do with it
            if (!isset($e["CONTENT"]["data"])){
                drush_log('POST ' . $e["CONTENT"]["data"] . " doesn't have a body", 'error');
                continue;
            }

            $posts[$e["ID"]["data"]] = $e;
            $creation_date = new DateTime($e["PUBLISHED"]["data"]);
            $last_update_date = new DateTime($e["UPDATED"]["data"]);
            $values = array(
              'type' => 'blog',
              'uid' => $user->uid,
              'created' => $creation_date->getTimestamp(),
              'updated' => $last_update_date->getTimestamp(),
              'status' => 1,
              'comment' => 2,
              'promote' => 0, 
            );
            $entity = entity_create('node', $values);
            $ewrapper = entity_metadata_wrapper('node', $entity);

            $img_url = $e["GD:IMAGE"]["attrs"]["SRC"];
            if (substr($img_url, 0, 2) == "//"){
                $img_url = "http:" . $img_url;
            }
            $file_temp = file_get_contents($img_url);
            if ($file_temp){
                $file_temp = file_save_data($file_temp, 'public://' . $filename, FILE_EXISTS_RENAME);
                //file_usage_add($file, "daptive_piter", "node", $user->uid);
                $entity->field_image['und'][0] = array(
                    'fid' => $file_temp->fid,
                    'filename' => $file_temp->filename,
                    'filemime' => $file_temp->filemime,
                    'uid' => 1,
                    'uri' => $file_temp->uri,
                    'status' => 1,
                );
            } else {
                _streaml_blogger_import_log("Coudln't load file (image field): ". $img_url);
            }
            // this field is optional
            if($e["MEDIA:THUMBNAIL"]["attrs"]["URL"]){
            $img_url = $e["GD:IMAGE"]["attrs"]["SRC"];
                if (substr($img_url, 0, 11) == "youtube.com"){
                    $img_url = "http://" . $img_url;
                }
                $file_temp = file_get_contents($e["MEDIA:THUMBNAIL"]["attrs"]["URL"]);
                if($file_temp){
                    $file_temp = file_save_data($file_temp, 'public://' . $filename, FILE_EXISTS_RENAME);
                    //file_usage_add($file, "daptive_piter", "node", $user->uid);
                    $entity->field_thumbnail['und'][0] = array(
                        'fid' => $file_temp->fid,
                        'filename' => $file_temp->filename,
                        'filemime' => $file_temp->filemime,
                        'uid' => 1,
                        'uri' => $file_temp->uri,
                        'status' => 1,
                    );
                } else {
                    _streaml_blogger_import_log("Coudln't load file (thumbnail field): ". $e["MEDIA:THUMBNAIL"]["attrs"]["URL"]);
                }
            }
            // this post has no title on the xml file for unknown reasons
            if (!isset($e["TITLE"]["data"])){
                if ($e["ID"]["data"] == 'tag:blogger.com,1999:blog-6030581480425687432.post-2167344362579030893'){
                    $title = 'CPA Exam Preparation Blog';
                } else {   
                    drush_log('post with no title', 'error');
                    return;
                }
            }else{
                $title = $e["TITLE"]["data"];
            }
            $ewrapper->title->set($title);

            $body_value = preg_replace('/[^(\x20-\x7F)]*/','', $e["CONTENT"]["data"]);

            // Using the wrapper, we do not have to worry about telling Drupal
            // what language we are using. The Entity API handles that for us.
            $ewrapper->body->set(array('value' => $body_value));
            _streaml_imp_download_post_images($entity);

            // we are not going to import the categories, they are not needed
            /*
            if (isset($e["TAGS"])){
                foreach ($e["TAGS"] as $t){
                    $tags_to_load = array();
                    foreach($tags_equivalency as $tag_name => $tag_props){
                        if (in_array($t["TERM"], $tag_props["tags"])){
                            if(!in_array($tag_props["tid"], $tags_to_load)){
                                $tags_to_load[] = $tag_props["tid"];
                            }
                        }
                    }
                    if (count($tags_to_load)>0){
                        $entity->field_categories['und'] = array();
                    }
                    foreach($tags_to_load as $tid){
                        $entity->field_categories['und'][] = array("tid" => $tid);
                    }
                }
            }
            */
            $ewrapper->save();
            $posts[$e["ID"]["data"]]["node"] = $entity;
            
            $original_url = "not available";
            foreach($e["LINK"] as $l){
                if($l["REL"] == 'alternate'){
                    $original_url = $l["HREF"];
                }
            }
            if ($original_url != "not available"){
                $parts = explode("/", $original_url);
                $old_title = $parts[count($parts) - 1];
                $old_title = str_replace(".html", "", $old_title);
                //$new_alias = "blog/2015/05/" . $old_title;
                $new_alias = "blog/" . $old_title;

                $alias = $new_alias . "&";
                $source = "node/".$entity->nid;
                $count = db_query("SELECT COUNT(*) c FROM {url_alias} a WHERE a.alias LIKE :alias AND source <> :source", array(":alias" => $alias, ":source" => $source))->fetchField();

                if ($count > 0){
                    $new_alias .= "-".$count;
                }

                $num_deleted = db_delete('url_alias')
                  ->condition('source', "node/".$entity->nid)
                  ->execute();

                $p = array(
                    "source" => "node/" . $entity->nid,
                    "alias" => $new_alias,
                );
                path_save($p);
            }
            $url_alias = drupal_get_path_alias("node/".$entity->nid);
            // wait 0.5 seconds to avoid "Deadlock found when trying to get lock;" errors
            usleep(300000);
            _streaml_blogger_import_log("Blog node created:");
            _streaml_blogger_import_log("    Original url: ". $original_url);
            _streaml_blogger_import_log("    Drupal url: ". $base_url . "/" .$url_alias);
            _streaml_blogger_import_log(" ");
        }
    }

    // first we need to get all the comments sorted by date
    // (even if we create them with the correct creation time, drupal seems to be 
    // sorting them by creation order inside drupal)
    $comments = array();
    foreach($entries as $e){
        if( $e["CATEGORY"]["attrs"]["TERM"] == "http://schemas.google.com/blogger/2008/kind#comment"){
            if(isset($e["THR:IN-REPLY-TO"])){
                $comments_count++;
                if (!isset($posts[$e["THR:IN-REPLY-TO"]["attrs"]["REF"]])){
                    // it's a comment created as another comment response
                    $comment_parent_id = $e["THR:IN-REPLY-TO"]["attrs"]["REF"];
                    while($entries[$comment_parent_id]["CATEGORY"]["attrs"]["TERM"] == "http://schemas.google.com/blogger/2008/kind#comment"){
                        $comment_parent_id = $entries[$comment_parent_id]["THR:IN-REPLY-TO"]["attrs"]["REF"];
                    }
                    $post_id = $comment_parent_id;
                } else {
                    // it's a comment created as a post response
                    $post_id = $e["THR:IN-REPLY-TO"]["attrs"]["REF"]; 
                }
                if (!isset($comments[$post_id])){
                    $comments[$post_id] = array();    
                }
                $comments[$post_id][] = $e;
            }
        }
    }

    drush_log('creating comments', 'ok');
    _streaml_blogger_import_log("importing comments...");

    // now, we create the comments
    foreach($comments as $post_id => $post_comments){
        // we need to sort the comments by creation date
        // because drupal seems to sort them by id when show them
        // (disregarding the post date)
        usort($post_comments, function($a, $b){
            if ($a["PUBLISHED"]["data"] < $b["PUBLISHED"]["data"]){
                return -1;
            } else if ($a["PUBLISHED"]["data"] == $b["PUBLISHED"]["data"]){
                return 0;
            } else {
                return 1;
            }
        });

        $post_nid = $posts[$post_id]["node"]->nid;
        foreach($post_comments as $c){
            if(!isset($c["TITLE"]["data"])){
                // there are some comments that doesn't have title
                // but we need it on drupal, so we use the autor name as the 
                // comment title for those cases
                $title = $c["NAME"]["data"];
            } else {
                $title = $c["TITLE"]["data"];
            }

            $creation_date = new DateTime($c["PUBLISHED"]["data"]);
            $last_update_date = new DateTime($c["UPDATED"]["data"]);
             $comment = (object) array(
                'nid' => $post_nid,
                'cid' => 0,
                'pid' => 0,
                'uid' => 0,
                'mail' => '',
                'name' => $c["NAME"]["data"],
                  'created' => $creation_date->getTimestamp(),
                  'updated' => $last_update_date->getTimestamp(), 
                'is_anonymous' => 0,
                'homepage' => '',
                'status' => COMMENT_PUBLISHED,
                'subject' => $title,
                'language' => LANGUAGE_NONE,
                'comment_body' => array(
                  LANGUAGE_NONE => array(
                    0 => array (
                      'value' => $c["CONTENT"]["data"],
                      'format' => 'filtered_html'
                    )
                  )
                ),
              );  
            comment_submit($comment);
            $comment->created =  $creation_date->getTimestamp();
            $comment->updated =  $last_update_date->getTimestamp();
            comment_save($comment);
        } 
    }
    _streaml_blogger_import_log("importing process finished"); 
}

function _streaml_imp_startElement($parser, $name, $attrs)
{
    global $entries;
    global $current_entry;
    global $current_property;

    if ($name == "ENTRY"){
        $current_property = null;
        $current_entry = array();
    }else if (is_array($current_entry) ){
        $current_property = $name;
        if ($name=="CATEGORY"){
            // categories can be multivalued, and can tell us the type of entry (post or comment)
            // or the categories of the post
            if ($attrs["SCHEME"] != 'http://schemas.google.com/g/2005#kind'){
                if (!isset($current_entry[$name])){
                    $current_entry["TAGS"] = array();
                }
                $current_entry["TAGS"][] = $attrs;
            } else {
                $current_entry[$name]["attrs"] = $attrs;
            }
        }else if($name == "LINK"){
            if (!isset($current_entry[$name])){
                $current_entry[$name] = array();
            }
            $current_entry[$name][] = $attrs;

        } else {
            $current_entry[$name]["attrs"] = $attrs;
        }
    } else {
        $current_property = null;
    }
}

function _streaml_imp_endElement($parser, $name)
{
    global $entries;
    global $current_entry;    
    if ($name == "ENTRY"){
        $entries[$current_entry["ID"]["data"]] = $current_entry;
        $current_entry = null;
    }
}

function _streaml_imp_characterData($parser, $data) 
{
    global $current_property;
    global $current_entry;
    if (isset($current_entry[$current_property]["data"])){
        $current_entry[$current_property]["data"] .= $data;
    }else{
        $current_entry[$current_property]["data"] = $data;    
    }
}

function _streaml_blogger_import_log($string){
    static $log_file = null;
    if (!$log_file){
        $log_file_path = file_directory_temp() . '/streamline_importer.txt';
        $log_file = fopen($log_file_path, 'w+');
        drush_log('a log file is created on ' . $log_file_path, 'ok');
    }
    fwrite($log_file,  date("r") ." :" . $string . "\n" );
    fflush($log_file);
}

function _streaml_imp_download_post_images($node){
  // Download images.
  global $base_url;
  $directory = 'blog-images';
  if (!empty($directory)) {
    // Search for blogspot image references (thumbnails and linked images).
    if (preg_match_all('/(href|src)="(http:\/\/.*?\.blogspot\.com\/(.*?))"/', $node->body[LANGUAGE_NONE][0]['value'], $matches)
      || preg_match_all('/(href|src)="(http:\/\/.*?\.blogger\.com\/(.*?))"/', $node->body[LANGUAGE_NONE][0]['value'], $matches)) {
      // Set the destination directory.
      $directory = file_default_scheme() . '://' . $directory . '/';
      $replace = array();
      // Loop through image matches.
      foreach ($matches[2] as $key => $url) {
        $filename = pathinfo($matches[3][$key], PATHINFO_BASENAME);
        $dir_name = pathinfo($matches[3][$key], PATHINFO_DIRNAME);
        $folder = basename($dir_name);
        $filepath = $directory . $folder . '/' . $filename;
        // Image was already downloaded.
        if (file_exists($filepath)) {
           $img_url = file_create_url($filepath);
           $img_url = substr($img_url, strlen($base_url));
           $replace[$url] = $img_url;
        }
        // Image hasn't been download.
        else {
          // Create directory for new file.
          $path = dirname($filepath);
          file_prepare_directory($path, FILE_CREATE_DIRECTORY);
          // Download the image.
          $response = drupal_http_request($url);
          // Image downloaded successfully.
          if ($response->code == 200) {
            // Save it.
            file_save_data($response->data, $filepath, FILE_EXISTS_REPLACE);

            $img_url = file_create_url($filepath);
            $img_url = file_create_url($filepath);
            $img_url = substr($img_url, strlen($base_url));
            $replace[$url] = $img_url;
          }
        }
      }
      // Replace image URLs in node body.
      $node->body[LANGUAGE_NONE][0]['value'] = str_replace(array_keys($replace), array_values($replace),$node->body[LANGUAGE_NONE][0]['value']);
    }
  }
}

function streaml_consistency_check_blog_drush_streaml_importer(){

  $report = array(
    "total_count" => 0,
    "new_nodes" => 0,
  );

  $revision_tables = array();
  $result = db_query("SHOW TABLES");
  foreach($result as $row){
    $table_name = reset($row);
    if (strpos($table_name, "field_revision")===0 && strpos($table_name, "field_revision_commerce") === false){
      $revision_tables[] = $table_name;
    }
  }
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'blog')
    ->execute();
  foreach($result["node"] as $r){
    $report["total_count"]++;

    $n = node_load($r->nid);
    $nid = $n->nid;

    $revs = node_revision_list($n);
    $first_rev = reset($revs);
    $revision_to_set = null;
    $revision_to_remove = null;
    $must_change_rev = false;

    foreach ($revs as $rev){
      if($rev->log == "Imported from dataset file: sites/default/files/data_export_import/nodes/20150508_165550_nodes_blog.dataset"){
        if ($rev == $first_rev){
          $must_change_rev = true;
        } 
        $revision_to_remove = $rev;
      } else if ($must_change_rev && !$revision_to_set){
        $revision_to_set = $rev;
      }
    }

    if ($revision_to_set){
      $node_type = null;
      $old_title = $revision_to_set->title;
      // we need to get the original node type
      foreach ($revision_tables as $table){
        $bundle = db_query("SELECT bundle FROM $table r where r.revision_id = :vid  AND r.entity_id = :nid ",
            array(
              ':vid' => $revision_to_set->vid,
              ':nid' => $nid
            )
          )->fetchField();
        if ($bundle){
          $node_type = $bundle;
        }
      }

    
      // let's collect the old values
      $revision_to_remove_values = _get_value_for_node_revision($revision_tables, $revision_to_remove->vid, $nid);

      $olds_res = db_query("SELECT title, nid FROM node n where n.title = :title",
        array(
          ':title' => $old_title,
        )
      );
      $current_title = null;
      foreach ($olds_res as $old_n) {
        $current_title = $old_n->title;
        $current_nid = $old_n->nid;
      }
      if ( !isset($report["types"][$node_type]) ){
        $report["types"][$node_type] = array(
            "nodes_with_equivalent" => array(),
            "nodes_without_equivalent" => array(),
            "count_whit_equivalent" => 0,
            "count_whitout_equivalent" => 0,
        );
      }
      if ($current_title){
        $report["types"][$node_type]["count_whit_equivalent"]++;
        $report["types"][$node_type]["nodes_with_equivalent"][] = array("nid" =>$nid, "title" => $old_title, "values_to_remove" => $revision_to_remove_values, "equivalent" => "node/".$current_nid);
      } else {
        $report["types"][$node_type]["count_whitout_equivalent"]++;
        $report["types"][$node_type]["nodes_without_equivalent"][] = array("nid" =>$nid, "title" => $old_title, "values_to_remove" => $revision_to_remove_values);
      }
    }
    if (!$revision_to_set){
        $report["new_nodes"]++;
    }
  }

  foreach($report["types"] as $type => $t){
    print "overwrited nodes of type: " . $type. "\n";

    print "WITH equivalent: \n";
    foreach ($t["nodes_with_equivalent"] as $n){
        print "     nid: ".$n["nid"]." title:'".$n["title"]."' current node:".$n["equivalent"]." \n";
        print "         values to be removed: \n";
        foreach($n["values_to_remove"] as $table_name => $values){
            print "             table_name: ".$table_name."\n";
            foreach ($values as $value){
                $l = "          ";
                foreach ($value as $col => $field_val){
                    $l .= $col.":". $field_val . " ";
                }
                $l .= "\n";
                print $l;
            }
        }
        print "\n";
    }
    print "WITHOUT equivalent: \n";
    foreach ($t["nodes_without_equivalent"] as $n){
        print "     nid: ".$n["nid"]." title:'".$n["title"]."' current node:".$n["equivalent"]." \n";
        print "         values to be removed: \n";
        foreach($n["values_to_remove"] as $table_name => $values){
            print "             table_name: ".$table_name."\n";
            foreach ($values as $value){
                $l = "          ";
                foreach ($value as $col => $field_val){
                    $l .= $col.":". $field_val . " ";
                }
                $l .= "\n";
                print $l;
            }
        }
    }
    print "\n";
  }
  print "##################################################################\n\n";
  foreach($report["types"] as $type => $t){
    print "type: " . $type. "\n";
    print "     nodes that doesn't have a equivalent new node:" . count($t["nodes_without_equivalent"]). "\n";
    print "     nodes that HAVE a equivalent new node:" . count($t["nodes_with_equivalent"]). "\n";
    print "\n";
  }
}

function streaml_consistency_correct_missing_entit_drush_streaml_importer(){
    _restore_missing_nodes("user_entitlement_product");
}

function streaml_consistency_correct_missing_topic_notes_drush_streaml_importer(){
    _restore_missing_nodes("topic_notes");
}

function streaml_consistency_correct_missing_video_history_drush_streaml_importer(){
    _restore_missing_nodes("video_history");
}

function streaml_consistency_get_rev_data_drush_streaml_importer($vid, $nid){
    $vals = _get_all_revision_traces($vid, $nid);
    print "values for vid: $vid - nid: $nid \n";
    foreach($vals as $table_name => $values){
        print "             table_name: ".$table_name."\n";
        foreach ($values as $value){
            $l = "          ";
            foreach ($value as $col => $field_val){
                $l .= $col.":". $field_val . " ";
            }
            $l .= "\n";
            print $l;
        }
    }
}
