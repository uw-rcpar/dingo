<?php

/**
 * Callback for 'rcpar-asl/%' menu item
 * @param $partner_id
 *  partner node id
 * @return int|string
 *  return ASL forms or MENU_NOT_FOUND when something is wrong with the partner
 */
function rcpar_asl_create_usr_and_login_forms($partner_id) {
  global $user;

  // we need to ensure that the partner_id correspond to a partner node
  $partner = new RCPARPartner($partner_id);
  if (!$partner->getWrapper()) {
    return MENU_NOT_FOUND;
  }

  // we collect some partner information here for later usage
  $page_header = '';
  $additional_instructions = '';
  try {
    $partner_wrapper = $partner->getWrapper();
    if ($partner_wrapper->field_asl_landing_page_header->value()) {
      $page_header = $partner_wrapper->field_asl_landing_page_header->value();
    }
    else {
      $page_header = $partner->getNameToDisplay();
    }

    $additional_instructions = $partner_wrapper->field_additional_instructions->value();
    if(isset($additional_instructions['safe_value'])){
      $additional_instructions = $additional_instructions['safe_value'];
    }
  }
  catch (EntityMetadataWrapperException $exc) {
    watchdog(
      'rcpar_asl',
      'See ' . __FUNCTION__ . '() ' . $exc->getTraceAsString(),
      NULL, WATCHDOG_ERROR
    );
  }

  // This feature is available for only partners with billing type "Free Access (freetrial)"
  if (!$partner->isBillingFreeAccess()) {
    return MENU_NOT_FOUND;
  }

  // if we reached here, the partner_id is valid
  // and that means the user should have access to the forms

  // now we construct the page
  $str = "<div class=\"row\"><div class=\"col-md-12\"><div class=\"page-heading\">        		
            <h1 class=\"page-title\">$page_header</h1>
          </div></div></div>";

  $str .= "<div class='asl-instructions'>$additional_instructions</div>";

  // Grab the partner node to pass into our form.
  $partner_node = $partner->getNode();

  $saml_login_enabled = variable_get('enable_uworld_configurations', FALSE);

  // "create account" form
  // note that we are sending the partner to the drupal_get_form to have access to it
  // later on the $form_state['build_info']['args'] variable
  $create_form = drupal_get_form('rcpar_asl_create_account_form', $partner_node);

  if (!$saml_login_enabled) {
    $str .= '<div class="asl-discount-create-account-form col-md-6">
          <div class="asl-discount-form-left">
           <h2 class="asl-form-title">Create New Account</h2>' .
      drupal_render($create_form)
      . '</div></div>';
  }
  // Remove h2 header if SSO enabled
  else {
    $str .= '<div class="asl-discount-create-account-form col-md-6">
          <div class="asl-discount-form-left">' .
      drupal_render($create_form)
      . '</div></div>';

  }

  // "already have a user" form
  $login_form = drupal_get_form('rcpar_asl_login_form', $partner_node);
  $titleVerbiage = user_is_logged_in() ? 'Enroll with current account' : 'Sign in to existing account';

  if (!$saml_login_enabled) {
    $str .= '<div class="asl-discount-login-form col-md-6">
        		<div class="asl-discount-form-right">
             <h2 class="asl-form-title">' . $titleVerbiage . '</h2>' .
      drupal_render($login_form)
      . '</div></div>';
  }
  // No need for additional h2 header for SSO link
  else {
    $str .= '<div class="asl-discount-login-form col-md-6">
        		<div class="asl-discount-form-right">' .
      drupal_render($login_form)
      . '</div></div>';
  }

  return $str;
}

/****************************************************
 * CREATE ACCOUNT FORM related functions
 ****************************************************/

/**
 * rcpar_asl_create_account form definition
 * @param $form
 * @param $form_state
 * @param $partner
 * @return mixed
 */
function rcpar_asl_create_account_form($form, &$form_state, $partner_node) {

  $v = isset($form_state['values']) ? $form_state['values'] : array();

  $partner = new RCPARPartner($partner_node);

  //if selected course on we load the fsl form items
  $isUserSelectedCourse = $partner->isUserSelectedCourse();
  if($isUserSelectedCourse){
    $form = array_merge($form , rcpar_fsl_create_account_form($partner_node));
  }

  // Attach PAL partner fields
  if($partner->isPartnerPAL() && $partner->isActPartner()){
    $form = array_merge($form , rcpar_pal_partner_fields($partner));
  }

  // Store partner in the form
  $form_state['partner'] = $partner;

  // Adding a class to the free trial form for marketing/tracking purposes.
  if ($partner_node->nid == RCPAR_ASL_FREE_TRIAL_PARTNER_ID) {
    $form['#attributes'] = array('class' => 'mktgd-free-trial-form-new');
  }

  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#default_value' => isset($v['first_name']) ? $v['first_name'] : '',
    '#required' => TRUE,
    '#weight' => 1
  );
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#default_value' => isset($v['last_name']) ? $v['last_name'] : '',
    '#required' => TRUE,
    '#weight' => 2
  );
  $form['email_new'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email Address'),
    '#default_value' => isset($v['email_new']) ? $v['email_new'] : '',
    '#description'   => t('Must be a valid e‐mail address. Your personal information will not be made public and will only be used if you wish to receive a new password or agree to receive certain news or notifications.'),
    '#required'      => TRUE,
    '#weight' => 3
  );
  // Attach phone field for Freetrial partner or FSL
  if ($partner->isFreeTrial() || $isUserSelectedCourse) {
    $form['phone_number'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Phone'),
      '#default_value' => isset($v['phone_number']) ? $v['phone_number'] : '',
      '#required'      => TRUE,
      '#size'          => 24,
      '#weight'        => 4,
    );
  }
  $form['pass_new'] = array(
    '#type'     => 'password_confirm',
    '#required' => TRUE,
    '#weight' => 5,
  );

  $form['#submit'][] = 'rcpar_asl_create_account_form_submit';
  $form['#validate'][] = 'rcpar_asl_create_account_form_validate';

  $form['submit_button'] = array(
    '#type'  => 'submit',
    '#value' => t('Create Account'),
    '#weight' => 11
  );
  return $form;
}

/**
 * rcpar_asl_create_account form validation function
 * @param $form
 * @param $form_state
 */
function rcpar_asl_create_account_form_validate($form, &$form_state) {
  $mail = $form_state['values']['email_new'];
  $mail_error = user_validate_mail($mail);
  if ($mail_error) {
    form_set_error('email_new', $mail_error);
  }

  // Determine if email validation is required
  $partner_class = new RCPARPartner($form_state['build_info']['args'][0]);
  if ($partner_class->useEmailValidation()) {
    // Email validation is required - determine if the email address is approved

    if(!$partner_class->isOnEmailValidationGrp($mail)){
      form_set_error('email_new', t('The email address you have entered is not present on the pre-approved list. Please <a href="@url">log out</a> and try registering again using your pre-approved email address.', array('@url' => url('user/logout'))));
    }
    else {
      $exp_date = $partner_class->getEmailValidationGrpExpDate($mail);
      if($exp_date && $exp_date < REQUEST_TIME){
        form_set_error('email_new', t('Partner has an invalid Expiration Date, please contact customer support.'));
      }
    }
  }
  else {
    // Determine if the partner expiration date is still valid
    if (!$partner_class->isEntitlementsExpDateValid()) {
      form_set_error('email', t('Partner has an invalid Expiration Date, please contact customer support.'));
    }
  }

  //Determine if password has a length less than 8 characters or greater than 64 characters
  if(strlen($form_state['values']['pass_new']) > 64 || strlen($form_state['values']['pass_new']) < 8) {
    form_set_error('pass_new', t('Password must has be equal or greater than 8 characters and less than 64 characters'));
  }

  // we need to check that the email is not used already
  $taken = (bool) db_select('users')->fields('users', array('uid'))
    ->condition('mail', db_like($mail), 'LIKE')
    ->range(0, 1)->execute()->fetchField();
  if ($taken) {
    form_set_error('email_new', t('The email address is already in use'));
  }
}

function rcpar_pal_partner_fields($partner){
  $prods = $partner->getACTproductsSkuTitles();
  $form['pal_product'] = array(
    '#type' => 'hidden',
    '#value' => implode(", ", $prods)
  );
  return $form;
}

/**
 * rcpar_asl_create_account form submit function
 * saves registration values, creates user w/ username = email, sends contact as
 * lead to hubspot, associates entitlements for the user
 *
 * @param $form
 * @param $form_state
 * @throws Exception
 */
function rcpar_asl_create_account_form_submit($form, &$form_state) {
  // as the user only inputs his email, and drupal force us to use a username when creating a new user
  // we are autogenerating one based on the email
  // so we will need check for the availability of that username

  // note that the discount_verification_generate_username_from_email function
  // belongs to the discount_verification module hence the dependency on .info file
  $username = discount_verification_generate_username_from_email($form_state['values']['email_new']);
  // Now, let's ensure that the autogenerated username is not in use
  // and in case it's in use, we are going to change it a little and try again
  $base_username = $username;
  $attempts = 0;
  do {
    $taken = (bool) db_select('users')->fields('users', array('uid'))
      ->condition('name', db_like($username), 'LIKE')
      ->range(0, 1)->execute()->fetchField();
    if ($taken){
      // The username is already in use, let's add some random numbers and try again
      $username = $base_username.rand(0, 1000);
      $attempts++;
    }
  } while ($taken && $attempts < 20);
  if ($taken) {
    // In the case we were unlucky enough to try this 20 times without success, we print and error and fail
    drupal_set_message(t('There was a problem creating the user.'), 'error');
    return;
  }

  // we need the partner to know which entitlements we need to create for the user
  $partner = $form_state['build_info']['args'][0];

  $fields = array(
    'name'   => $username,
    'mail'   => $form_state['values']['email_new'],
    'pass'   => $form_state['values']['pass_new'],
    'status' => 1,
    'init'   => 'email address',
    'roles'  => array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      4                        => 'student enrolled',
    ),

  );
  //the first parameter is left blank so a new user is created

  // Needed for [user:salesforce-account-id] token on rcar_salesfoce module to work
  $_SESSION['rcpar_partner'] = $partner;
  $account = user_save(NULL, $fields);
  $account->field_did_you_graduate_college = isset($form_state['values']['field_did_you_graduate_college']) ? $form_state['values']['field_did_you_graduate_college'] : array();
  $account->field_college_state_list = isset($form_state['values']['field_college_state_list']) ? $form_state['values']['field_college_state_list'] : array();
  $wrapper = entity_metadata_wrapper('user', $account);
  $wrapper->field_first_name->set($form_state['values']['first_name']);
  $wrapper->field_last_name->set($form_state['values']['last_name']);

  if (isset($form_state['values']['phone_number'])){
    $wrapper->field_phone_number->set($form_state['values']['phone_number']);
  }

  if(isset($form_state['values']['field_college_name'])){
    $wrapper->field_college_name->set($form_state['values']['field_college_name']);
  }

  $wrapper->save();

  // Somehow, when we create the user the role 'student not enrolled' (rid 8) is added to the account
  // and as we are adding entitlements to the user, that would't be correct
  // also, that role creates visualization issues when users access to the dashboard
  // Note that this function does not return the updated $account object, so we reload it to ensure
  // we have an updated account when invoking hooks
  user_multiple_role_edit(array($account->uid), 'remove_role', 8);
  $account = user_load($account->uid);

  // log in the user
  $form_data = array('uid' => $account->uid);
  // We save the the uid for other modules to be able to use it (in particular the sales funnel widget)
  $form_state['uid'] = $account->uid;
  user_login_submit(array(), $form_data);

  // add the corresponding entitlements for the newly logged in user
  rcpar_asl_add_entitlements($partner);

  $form_state['redirect'] = 'dashboard';

  // Allow other modules to react to the registration
  $type = 'register';
  module_invoke_all('free_access_registration', $type, $partner, $account);
}



/****************************************************
 * LOGIN FORM related functions
 ****************************************************/

/**
 * rcpar_asl_login form definition
 * @param $form
 * @param $form_state
 * @param $partner
 * @return mixed
 */
function rcpar_asl_login_form($form, &$form_state, $partner_node) {
  // we get the previously send values to autopopulate the default value
  // if they exists
  global $user;
  $v = isset($form_state['values']) ? $form_state['values'] : array();
  $default_mail = isset($v['email']) ? $v['email'] : '';

  $partner = new RCPARPartner($partner_node);

  //if selected course on we load the fsl form items
  $isUserSelectedCourse = $partner->isUserSelectedCourse();
  if($isUserSelectedCourse){
    $form = array_merge($form , rcpar_fsl_login_form($partner_node));
  }

  // Adding a class to the free trial form for marketing/tracking purposes.
  if ($partner_node->nid == RCPAR_ASL_FREE_TRIAL_PARTNER_ID) {
    $form['#attributes'] = array('class' => 'mktgd-free-trial-form-existing');
  }

  // Store partner in the form
  $form_state['partner'] = $partner;

  $form['email'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email Address'),
    '#default_value' => user_is_logged_in() ? $user->mail : $default_mail,
    '#required'      => TRUE,
    '#disabled' => user_is_logged_in()
  );
  $form['pass'] = array(
    '#type'     => user_is_logged_in() ? 'hidden' : 'password',
    '#title'    => t('Password'),
    '#required' => !user_is_logged_in(), // Not required when user is already logged in
    '#disabled' => user_is_logged_in()
  );

  // If the user is logged in, supply a default value for graduation date if they've already entered it
  $graduation_default = 'YYYY-MM-DD 00:00:00';
  if (user_is_logged_in()) {
    $full_user = user_load($user->uid);
    if(!empty($full_user->field_graduation_month_and_year)) {
      $graduation_default = $full_user->field_graduation_month_and_year[LANGUAGE_NONE][0]['value'];
    }
  }
  $form['graduation_date'] = array(
    '#type'          => 'date_popup',
    '#date_format'   => 'm/d/Y',
    '#default_value' => $graduation_default,
    '#title'         => t('Graduation Date'),
    '#required'      => TRUE,
  );

  $form['forgot_password'] = array(
    '#type'          => 'item',
    '#markup' => '<a href="/user/password">Forgot Password?</a>',
  );

  $form['#submit'][] = 'rcpar_asl_login_form_submit';

  $form['submit_button'] = array(
    '#type'  => 'submit',
    '#value' => user_is_logged_in() ? t('Enroll Now') : t('Login Now'),
  );
  return $form;
}

/**
 * rcpar_asl_login form validation function
 * @param $form
 * @param $form_state
 */
function rcpar_asl_login_form_validate($form, &$form_state) {
  global $user;

  // When the user is already logged in, we can assume we have a valid user
  // and enroll with the global $user object
  if (user_is_logged_in()) {
    $u = $user;
  }
  else {
    // Try to load an existing user and authenticate with the password supplied in the form
    $u = user_load_by_mail($form_state['values']['email']);
    if(!user_authenticate($u->name, $form_state['values']['pass'])) {
      form_set_error('email', t('Incorrect email or password'));
      return;
    }
  }

  // Determine if email validation is required for this partner
  $partner_class = new RCPARPartner($form_state['build_info']['args'][0]);
  if ($partner_class->useEmailValidation()) {
    // Email validation is required - determine if the email address is approved
    if (!$partner_class->isOnEmailValidationGrp($u->mail)) {
      form_set_error('email_new', t('The email address you have entered is not present on the pre-approved list. Please <a href="@url">log out</a> and try registering again using your pre-approved email address.', array('@url' => url('user/logout'))));
    }
    else {
      $exp_date = $partner_class->getEmailValidationGrpExpDate($u->mail);
      if ($exp_date && $exp_date < REQUEST_TIME) {
        form_set_error('email_new', t('Partner has an invalid Expiration Date, please contact customer support.'));
      }
    }
  }
  else {
    // Determine if the partner expiration date is still valid
    if (!$partner_class->isEntitlementsExpDateValid()) {
      form_set_error('email', t('Partner has an invalid Expiration Date, please contact customer support.'));
    }
  }

}

/**
 * Form submit handler for rcpar_asl_login_form
 * Logs the user in (if necessary) and grants them any entitlements from the partner that
 * they don't already have.
 */
function rcpar_asl_login_form_submit($form, &$form_state) {
  
  $partner = $form_state['build_info']['args'][0];
  $partner_class = new RCPARPartner($partner);
  // Counter intuitively, the user can be already logged in while submitting this form. If that's the case, we just
  // look at the global user object
  if (user_is_logged_in()) {
    $u = $GLOBALS['user'];
  }
  // Load the user from the form data and log them in
  else {
    $u = user_load_by_mail($form_state['values']['email']);
    if (!$u) {
      // if we couldn't find the user, we can't proceed
      drupal_set_message(t('Incorrect email or password'), 'error');
      return;
    }

    // Log in the user
    $form_data = array('uid' => $u->uid);
    user_login_submit(array(), $form_data);
  }

  // now, we save the profile data (graduation date)
  // Note that even for an existing/already logged in user, we will update this data if they are entering an ASL portal
  $wrapper = entity_metadata_wrapper('user', $u);
  if (isset($form_state['values']['graduation_date'])) {
    $wrapper->field_graduation_month_and_year->set(strtotime($form_state['values']['graduation_date']));
  }
  
  //assign user-enrolled role in case is not on the user due to the fact
  //we are importing this from the api
  $roles = $wrapper->roles->value();
  if (!in_array(4, $roles)) {
    $roles[] = 4;
    $wrapper->roles->set($roles);
  }

  $wrapper->save();
  
  //remove user-not-enrolled role
  user_multiple_role_edit(array($wrapper->getIdentifier()), 'remove_role', 8);
  // Determine if there are any entitlements granted by this partner that the user doesn't have
  $partner_products_ids = rcpar_asl_get_entitlements_list($partner);
  $c = new RCPARCheckoutProductsCheck($u, $partner_products_ids);
  
  $products_not_entitled = $c->getPurchasableProductIds();

  // There are at least some entitlements that the user can get from this partner, we proceed
  if (sizeof($products_not_entitled) > 0) {
    rcpar_asl_add_entitlements($partner);
    $form_state['redirect'] = 'dashboard';
  }
  // The user has existing entitlements for every single entitlement granted by this partner
  // Show a warning message
  else {
    if($partner_class->isPartnerTypeUniversity()) {
      drupal_set_message(t('Our records show that you already have access to one or more sections of the Roger CPA Review products.<br> 
      Please head to your ' . l('Dashboard', 'dashboard') . ' to see this access.<br>
      Should you have any trouble accessing your ACT materials in your account, contact our team at universitysupport@Rogercpareview.com 
      so that we may assist you.'), 'warning');
    } 
    else {
      drupal_set_message(t("It appears that you have already enrolled in this course. You can always log in to your course by visiting our homepage at " . $GLOBALS['base_url']), 'warning');
    }
    $form_state['redirect'] = current_path();
  }

  // Allow other modules to react to the registration
  $type = 'login';
  module_invoke_all('free_access_registration', $type, $partner, $u);

  
}
