<?php

/**
 *  This file is intend to separate all the fsl related function into one place
 * to avoid creating a new module due to reusable code.
 */

/**
 * create form fields to dynamically add it to registration forms
 * 
 * @param <object> $partner_node the node of type partner
 * @return <array> form items to be merged into other forms
 */
function rcpar_fsl_create_account_form($partner_node) {

  $form = rcpar_fsl_sections_field($partner_node);
  $partner = new RCPARPartner($partner_node);
  $partner_wrappter = $partner->getWrapper();
  $form_new = $form_state_new = array();

  /**
   * simple instance of an user to load a form dynamically on specific fields
   */
  global $user;
  $account = user_load($user->uid);

  // Office location - If 'show office location' is checked and there are office locations provided, build a custom
  // form element (select list) of the admin-supplied options in the partner object
  if (isset($partner_wrappter->field_show_office_location_check) && $partner_wrappter->field_show_office_location_check->value() &&
      isset($partner_wrappter->field_office_location_choices) && $partner_wrappter->field_office_location_choices->value()) {
    $choices = explode("\n", $partner_wrappter->field_office_location_choices->value());
    $form['custom_office_location'] = array(
      '#type' => 'container',
      '#weight' => 5,
      '#attributes' => array('id' => 'custom_office_location_wrapper'),
      'office_location' => array(
        '#type' => 'select',
        '#title' => t('Office Location '),
        '#options' => array_combine($choices, $choices),
        '#validated' => TRUE,
        '#required' => TRUE,        
      ),
    );
  }
  
  
  //terms and conditions configuration.
  if ($partner_wrappter->field_collect_firm_terms->value()) {
    // Get the terms and conditions text
    $terms_text = $partner_wrappter->field_terms_and_conditions->value();

    // Trim extraneous white space to prevent a link with only white space content appearing.
    if (isset($terms_text['value'])) {
      $terms_text['value'] = trim($terms_text['value']);
    }

    // If we have t&c text, make a link with bootstrap popover content
    if (!empty($terms_text['value'])) {
      $link = array(
        '#type' => 'html_tag',
        '#tag' => 'a',
        '#value' => 'Terms &amp; Conditions',
        '#attributes' => array(
          'class' => array('terms-toggle'),
          'role' => 'button',
          'tabindex' => '0',
          'data-html' => 'true',
          'data-toggle' => 'popover',
          'data-trigger' => 'focus',
          'data-content' => $terms_text['value'], // Automatically processed with htmlspecialchars()
        ),
      );
      $tAndCText = drupal_render($link);
    }
    // Otherwise, no link.
    else {
      $tAndCText = 'Terms &amp; Conditions';
    }

    // Build the container and select list
    $form['custom_terms'] = array(
      '#type' => 'container',
      '#attributes' => array('id' => 'custom_terms_wrapper'),
      '#weight' => 11,
      'checkbox' => array(
        '#type' => 'checkbox',
        '#title' => t('I agree to the %name !tclink', array('%name' => rcpar_partners_get_name(), '!tclink' => $tAndCText)),
        '#required' => TRUE,
      ),
    );

    $form['#group_children']['custom_terms'] = 'group_custom_fields';
  }
  
  
  //Hidden fields to load values
  $form['field_college_name'] = array(
    '#type' => 'hidden',
  );

  $form['#submit'][] = 'rcpar_fsl_create_account_form_submit';
  $form['#validate'][] = 'rcpar_fsl_create_account_form_validate';

  return $form;
}

/**
 * rcpar_fsl_create_account form submit function
 * save the fsl flag on the partner object to be used on the entitlements creation
 * 
 */
function rcpar_fsl_create_account_form_submit($form, &$form_state) {
  $partner = $form_state['build_info']['args'][0];

  //we set the section to evaluate on the submittion
  if (isset($form_state['values']['fsl_sections'])) {
    $partner->fsl_section = $form_state['values']['fsl_sections'];
  }
  
  //we set the office location to save on the order
  if (isset($form_state['values']['fsl_sections'])) {
    $partner->fsl_office_location = $form_state['values']['office_location'];
  }

  //we validate and remove null values that might exist for the entity fields
  $values = $form_state['values'];
  foreach ($values as $name => $value) {
    if (strpos($name, 'field') === 0) {
      if (@is_null($value[LANGUAGE_NONE][0]['value'])) {
        unset($form_state['values'][$name]);
      }
    }
  }
  $form_state['build_info']['args'][0] = $partner;
}

/***
 * Validates fields that creates the fsl create account form
 */
function rcpar_fsl_create_account_form_validate($form, &$form_state) {
  
  //validate office location
  if (array_key_exists('office_location', $form_state['values']) && empty($form_state['values']['office_location'])) {    
    form_set_error('office_location', t('Office location is required.'));
  }
  
}
/**
 * create form fields to dynamically add it to login forms
 * 
 * @param <object> $partner_node the node of type partner
 * @return <array> form items to be merged into other forms
 */
function rcpar_fsl_login_form($partner_node) {
  $form = rcpar_fsl_sections_field($partner_node);
  $form['#submit'][] = 'rcpar_fsl_login_form_submit';
  return $form;
}

/**
 * rcpar_fsl_login_form form submit function
 * save the fsl flag on the partner object to be used on the entitlements creation
 * 
 */
function rcpar_fsl_login_form_submit($form, &$form_state) {
  $partner = $form_state['build_info']['args'][0];

  //we set the section to evaluate on the submittion
  if (isset($form_state['values']['fsl_sections'])) {
    $partner->fsl_section = $form_state['values']['fsl_sections'];
  }

  $form_state['build_info']['args'][0] = $partner;
}

/**
 *  creates a form sections field based on the partner node values
 * 
 * @param <object> $partner_node the node of type partner
 */
function rcpar_fsl_sections_field($partner_node) {
  $form = array();
  $partner = new RCPARPartner($partner_node);
  $sections = $partner->getCourseSections();
  $form['fsl_sections'] = array(
    '#type' => 'select',
    '#options' => array_combine($sections, $sections),
    '#title' => t('Course Access'),
    '#required' => TRUE
  );

  return $form;
}

/**
 * 
 * @param <string> $section course section, AUD, BEC, FAR, REG
 * @param <array> $list list of products ids to match agains
 * @return <array> one item array matching the FLS unique section
 */
function rcpar_fsl_match_fsl_section($section, $list) {
  $product = commerce_product_load_by_sku($section);
  return is_object($product) && in_array($product->product_id, $list) ? array($product->product_id) : array();
}
