(function ($) {
  Drupal.behaviors.rcpar_asl = {
    attach: function (context, settings) {

      if($('#rcpar-asl-create-account-form .password-field').length || $('#user-profile-form .password-field').length) {

        $(document).on('keypress', '#rcpar-asl-create-account-form .password-field, #rcpar-asl-create-account-form .password-confirm', function(e){
            // Prevent that password length has more than 64 characters
            if($(this).val().length >= 64) e.preventDefault();
        });

        $(document).on('paste', '#rcpar-asl-create-account-form .password-field, #rcpar-asl-create-account-form .password-confirm', function(e) {
            var pasteData = e.originalEvent.clipboardData.getData('text');
            // Prevent that paste a text in password field with a length greater than 64 characters
            if(pasteData.length >= 64) e.preventDefault();
        });

        $(document).on('keypress', '#user-profile-form .password-field, #user-profile-form .password-confirm', function(e){
            // Prevent that password length has more than 64 characters
            if($(this).val().length >= 64) e.preventDefault();
        });

        $(document).on('paste', '#user-profile-form .password-field, #user-profile-form .password-confirm', function(e) {
            var pasteData = e.originalEvent.clipboardData.getData('text');
            // Prevent that paste a text in password field with a length greater than 64 characters
            if(pasteData.length >= 64) e.preventDefault();
        });

        /**
         * Evaluate the strength of a user's password.
         *
         * Returns the estimated strength and the relevant output message.
         */
        Drupal.evaluatePasswordStrength = function (password, translate) {
          password = $.trim(password);

          var weaknesses = 0, strength = 100, msg = [];

          // If there is a username edit box on the page, compare password to that, otherwise
          // use value from the database.
          var usernameBox = $('input.username');
          var username = (usernameBox.length > 0) ? usernameBox.val() : translate.username;

          // Lose 5 points for every character less than 6, plus a 30 point penalty.
          if (password.length < 8) {
            strength -= ((8 - password.length) * 5) + 30;
          }

          if (password.length > 64) {
            strength -= ((8 - password.length) * 5) + 30;
          }

          msg.push(translate.tooShort);
          msg.push(translate.tooLong);
          strength -= 1;

          // Apply penalty for each weakness (balanced against length penalty).
          switch (weaknesses) {
            case 1:
              strength -= 12.5;
              break;

            case 2:
              strength -= 25;
              break;

            case 3:
              strength -= 40;
              break;

            case 4:
              strength -= 40;
              break;
          }

          // Check if password is the same as the username.
          if (password !== '' && password.toLowerCase() === username.toLowerCase()) {
            msg.push(translate.sameAsUsername);
            // Passwords the same as username are always very weak.
            strength = 5;
          }

          // Based on the strength, work out what text should be shown by the password strength meter.
          if (strength < 60) {
            indicatorText = translate.weak;
          } else if (strength < 70) {
            indicatorText = translate.fair;
          } else if (strength < 80) {
            indicatorText = translate.good;
          } else if (strength <= 100) {
            indicatorText = translate.strong;
          }

          // Assemble the final message.
          msg = translate.hasWeaknesses + '<ul><li>' + msg.join('</li><li>') + '</li></ul>';
          return { strength: strength, message: msg, indicatorText: indicatorText };

        };
      }
    }
  };
}(jQuery));
