<a href="<?php echo $jump_url ?>" class="jump-link"><?php
    echo views_trim_text(array(
        'max_length' => 30,
        'word_boundary' => 'yes',
        'ellipsis' => 'yes',
        'html' => 'yes'
            ), $jump_text)
    ?></a>
<a href="<?php echo $jump_url?>" class="jump-to-link">Jump to last Section</a>