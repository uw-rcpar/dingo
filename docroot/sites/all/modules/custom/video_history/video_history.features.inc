<?php
/**
 * @file
 * video_history.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function video_history_eck_bundle_info() {
  $items = array(
    'video_history_video_history' => array(
      'machine_name' => 'video_history_video_history',
      'entity_type' => 'video_history',
      'name' => 'video_history',
      'label' => 'Video History',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function video_history_eck_entity_type_info() {
  $items = array(
    'video_history' => array(
      'name' => 'video_history',
      'label' => 'Video History',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'topic_reference' => array(
          'label' => 'topic_reference',
          'type' => 'integer',
          'behavior' => '',
        ),
        'rcpa_video' => array(
          'label' => 'rcpa_video',
          'type' => 'integer',
          'behavior' => '',
        ),
        'last_position' => array(
          'label' => 'last_position',
          'type' => 'integer',
          'behavior' => '',
        ),
        'total_duration' => array(
          'label' => 'total_duration',
          'type' => 'text',
          'behavior' => '',
        ),
        'percentage_completed' => array(
          'label' => 'percentage_completed',
          'type' => 'integer',
          'behavior' => '',
        ),
        'video_history_content' => array(
          'label' => 'video_history_content',
          'type' => 'text',
          'behavior' => '',
        ),
        'entitlement_product' => array(
          'label' => 'entitlement_product',
          'type' => 'integer',
          'behavior' => '',
        ),
        'section' => array(
          'label' => 'Section',
          'type' => 'text',
          'behavior' => '',
        ),
        'course_type' => array(
          'label' => 'Type',
          'type' => 'text',
          'behavior' => '',
        ),
        'chapter_reference' => array(
          'label' => 'Chapter Reference',
          'type' => 'text',
          'behavior' => '',
        ),
      ),
    ),
  );
  return $items;
}
