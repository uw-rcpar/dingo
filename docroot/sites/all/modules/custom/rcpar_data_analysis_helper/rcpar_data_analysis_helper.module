<?php

/***
 *
 * This module is just a helper module to allow us to process some data to use
 * it on reports and queries that are specific for data analysis works and is
 * very likely that it shouldn't be enabled on most of the environments
 *
 * TL;DR: Don't enable unless you actually need it.
 * NOTE: it can't be enabled on prod, see rcpar_data_analysis_helper_init
 *
 */

// Drush commands to run batch processes on the command line
include_once 'includes/rcpar_data_analysis_helper.drush.inc';

/**
 * hook_init implementation
 * We use it to prevent this module to be enabled on prod
 */
function rcpar_data_analysis_helper_init(){
  if (isset($_ENV['AH_SITE_ENVIRONMENT']) && $_ENV['AH_SITE_ENVIRONMENT'] =='prod') {
    // This module shouldn't be enabled on PROD!
    // Note that we are not checking if it's enabled because
    // it obviously is since this code is part of this module
    module_disable(array('rcpar_data_analysis_helper'));
    drupal_set_message(t('RCPAR data analisys helper module shouldn\'t be enabled on prod! Disabling it automatically.'));
  }
}

/**
 * Hook menu implementation
 * @return array
 */
function rcpar_data_analysis_helper_menu(){
  $items = array();

  $items['rcpar_data_analysis_helper/repopulate_product_revenue_table'] = array(
    'title' => 'Revenue Report Generator',
    'description' => 'Revenue report separated by product label',
    'page callback' => 'rcpar_data_analysis_helper_product_revenue_table_batch',
    'access arguments' => array('access administration menu'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['rcpar_data_analysis_helper/repopulate_product_revenue_legacy_table'] = array(
    'title' => 'Revenue Report Generator',
    'description' => 'Revenue report separated by product label (legacy tables)',
    'page callback' => 'rcpar_data_analysis_helper_product_revenue_legacy_table_batch',
    'access arguments' => array('access administration menu'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['rcpar_data_analysis_helper/revenue_batch_finished'] = array(
    'title' => 'Revenue Report Generator Finish Page',
    'description' => 'Revenue report results',
    'page callback' => 'rcpar_data_analysis_helper_revenue_results',
    'access arguments' => array('access administration menu'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * This function is strongly based on rcpar_salesforce_salesforce_push_params_alter
 * @param $order
 * @return array
 */
function rcpar_data_analysis_helper_process_order($order, $store_in_db = TRUE) {

  // This variable is an array of course bundle discount skus keyed to the bundle id they belong to
  // i.e. FULL-PREM-DIS is a member of the FULL-PREM bundle whose id is 66
  // See /admin/settings/rcpar/system_settings/enroll_flow/bundles for bundle id and sku listings
  $discounts_list = array(
    66 => 'FULL-PREM-DIS',
    12 => 'FULL-DIS',
    110 => 'FULL-ELITE-DIS',
    306 => 'FULL-PREM-AUD-DIS',
    316 => 'FULL-PREM-EFC-DIS',
  );
  $products_of_interests = array(
    'FAR', 'AUD', 'REG', 'BEC',
    'AUD-6MEXT','BEC-6MEXT','FAR-6MEXT','REG-6MEXT','FULL-6MEXT',
    'AUD-AUD','BEC-AUD','FAR-AUD','REG-AUD',
    'AUD-CRAM','BEC-CRAM','FAR-CRAM','REG-CRAM',
    'AUD-EFC','BEC-EFC','FAR-EFC','REG-EFC',
    'AUD-OFF-LEC','BEC-OFF-LEC','FAR-OFF-LEC','REG-OFF-LEC',
    'CRAM-CERT','AUD-SSD','BEC-SSD','FAR-SSD','REG-SSD',
  );

  $entity_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $packages_in_order = array();

  // First, let's check if the order have packages or is a freetrial order
  foreach ($entity_wrapper->commerce_line_items as $line_item_wrapper) {
    $line_item = $line_item_wrapper->value();
    $is_free_trial = false;
    if ($line_item->line_item_label == 'FREETRIAL-BUNDLE'){
      $is_free_trial = true;
    }
    if ($is_free_trial){
      // We want to skip this kind of orders
      return array();
    }
  }

  // Now, we calculate the sum of the discounts that are applied to this order

  // Let's get the discounts applied to this order
  // we can have 3 types of discounts:
  // - discount products (eg: FULL-DIS)
  // - fixed price coupons
  // - percentage coupons

  $accumulated_discount = 0;
  $coupon_discounts = 0;

  // This variable represents the sum of all the amounts of the products to
  // which we are going to apply the discount
  $discountable_amount = 0;

  foreach ($entity_wrapper->commerce_line_items as $line_item_wrapper) {
    $line_item = $line_item_wrapper->value();
    if (in_array($line_item->type, commerce_product_line_item_types())) {
      $line_price = $line_item_wrapper->commerce_unit_price->amount->value();

      // If the discount product is part of a package, we must not count it here (we are going to do that later)
      $must_skip = false;
      foreach($packages_in_order as $p){
        if (isset($p['products_to_ignore'][$line_item_wrapper->commerce_product->product_id->value()])) {
          $must_skip = true;
        }
      }
      if (in_array($line_item->line_item_label, $discounts_list)) {
        // This discount types are always part of a package
        $must_skip = true;
      }
      if ($must_skip){
        continue;
      }

      // discount products (EG: FULL-EFC-DIS)
      $line_amount = ($line_price / 100) * $line_item->quantity;
      if ($line_amount < 0) {
        // $line_amount is negative, but we want to sum all the discounts
        // and have it as a positive measure
        $accumulated_discount += -1 * $line_amount;
      }

      // As we are here, we also sum the prices of the products to which we should apply the discount
      $apply_discount = TRUE;
      try {
        if (!$line_item_wrapper->commerce_product->field_apply_discount_avatax->value()) {
          $apply_discount = FALSE;
        }
      }
      catch (EntityMetadataWrapperException $e) {
        // if the field is not defined, we do the default behaviour, that is to apply the discount
      }

      if ($line_item_wrapper->type->value() == 'commerce_coupon') {
        $item_line_total = $line_item_wrapper->commerce_total->value();
        $coupon_discounts += $item_line_total['amount'];
      }

      if ($apply_discount) {
        $item_line_total = $line_item_wrapper->commerce_total->value();
        foreach ($item_line_total['data']['components'] as $c){
          if ($c['name'] == 'base_price'){
            $discountable_amount += $c['price']['amount'];
            break;
          }
        }
      }
    }
    // Fixed price coupons/discounts only
    // Note that we need to avoid adding percentage coupons here because those are added below.
    elseif (in_array($line_item->type, array('commerce_coupon')) && strpos($line_item->line_item_label, 'commerce_coupon_pct') !== 0) {
      $accumulated_discount += -1 * ($line_item_wrapper->commerce_unit_price->amount->value() / 100) * $line_item->quantity;
    }
  }

  // percentage coupons/discounts only
  // those discounts appears only on the order commerce_order_total field
  $price_components = $entity_wrapper->commerce_order_total->data->value();
  foreach ($price_components['components'] as $component) {
    if (strpos($component['name'], 'commerce_coupon_pct_') === 0) {
      // (Note that as 'amount' is a commerce amount, we need to divide it by 100 to get the actual amount)
      $accumulated_discount += -1 * $component['price']['amount'] / 100;
    }
  }

  // Now we calculate the packages prices
  foreach ($entity_wrapper->commerce_line_items as $line_item_wrapper) {
    $line_item = $line_item_wrapper->value();

    foreach ($packages_in_order as $pid => &$p) {
      if (isset($line_item_wrapper->commerce_product)) {
        if (isset($p['products_to_ignore'][$line_item_wrapper->commerce_product->product_id->value()])) {
          $v = $line_item_wrapper->commerce_total->value();
          foreach ($v['data']['components'] as $c) {
            if ($c['name'] == 'base_price') {
              $packages_in_order[$pid]['accumulated_price'] += $c['price']['amount'];
            }
          }
        }
      }
    }
  }
  // And add the packages price to the amount that we can use to apply the discounts
  foreach($packages_in_order as $p){
    $discountable_amount += $p['accumulated_price'];
  }
  // We have $discountable_amount expressed as a commerce amount, we divide it by 100 to get the real amount
  $discountable_amount = $discountable_amount / 100;

  if (($accumulated_discount != 0 || $discountable_amount) && $discountable_amount != 0 ) {
    // note that $discount_amount is negative
    $percent_after_discount = 1 - ($accumulated_discount / $discountable_amount) ;
  } else {
    $percent_after_discount = 1; // no discount
  }

  if ($percent_after_discount < 0){
    $percent_after_discount = 0;
  }

  // This variable will contain the information of the records that we need to add to
  // rcpar_data_analysis_helper_item_line_revenue
  $results = array();

  // Now we calculate the discounted amount per product and set it in the corresponding item line
  foreach ($entity_wrapper->commerce_line_items as $line_item_wrapper) {
    $line_item = $line_item_wrapper->value();

    $ignore_product = false;
    foreach($packages_in_order as $pid => &$p) {
      if (isset($line_item_wrapper->commerce_product)){
        if (isset($p['products_to_ignore'][$line_item_wrapper->commerce_product->product_id->value()])) {
          $v = $line_item_wrapper->commerce_total->value();
          unset($packages_in_order[$pid]['products_to_ignore'][$line_item_wrapper->commerce_product->product_id->value()]);
          $ignore_product = true;
        }
      }
    }
    // We already considered this product when we added the line for the package to $results
    if ($ignore_product){
      continue;
    }
    // We already considered this when we added the line for the package to $results
    if (in_array($line_item->line_item_label, $discounts_list)) {
      continue;
    }

    if (in_array($line_item->line_item_label, $products_of_interests)) {
      $apply_discount = TRUE;
      try {
        if (!$line_item_wrapper->commerce_product->field_apply_discount_avatax->value()) {
          $apply_discount = FALSE;
        }
      }
      catch (EntityMetadataWrapperException $e) {
        // if the field is not defined, we do the default behaviour, that is to apply the discount
      }
      // As the product total includes the discounts, we need the base price to apply them ourselves
      $v = $line_item_wrapper->commerce_total->value();
      foreach ($v['data']['components'] as $c) {
        if ($c['name'] == 'base_price') {
          $product_price = $c['price']['amount'];
        }
      }
      if ($apply_discount) {
        $product_price = $product_price * $percent_after_discount;
      }
      $results[] = array(
        'line_item_id' => $line_item->line_item_id,
        'product_label' => $line_item->line_item_label,
        'quantity' => $line_item->quantity,
        'total' => $product_price
      );
    }
  }

  // Here we are adding the lines for the packages
  foreach ($packages_in_order as $pkg) {
    $results[] = array(
      'line_item_id' => $pkg['line_item_id'],
      'product_label' => $pkg['product_label'],
      'quantity' => $pkg['quantity'],
      'total' => $pkg['accumulated_price'] * $percent_after_discount
    );
  }
  // Now we create the records on rcpar_data_analysis_helper_item_line_revenue
  $values = array();
  foreach ($results as $r){
    $record = new stdClass();
    $record->order_id = $order->order_id;
    $record->line_item_id = $r['line_item_id'];
    $record->quantity = $r['quantity'];
    $record->revenue = $r['total'];
    if ($store_in_db) {
      drupal_write_record('rcpar_data_analysis_helper_item_line_revenue', $record);
    }
    $values[] = $record;
  }
  return $values;
}

/**
 * Use this function to match a product name, with all the possible similar product names on the legacy
 * tables
 */
function _rcpar_data_analysis_helper_compare_item_name($item_product_name, $product_name) {
  // On the legacy tables, some products have versions versions of them
  // eg, the 'Online Course - Full' package have this 'versions':
  //  Regular Online Course - Full
  //  Online Course - Full (Repeat Student)
  //  Online Course - Full (Past Student)
  //  and some others with the format of 'Online Course - Full'.$something
  //
  if (
    strpos($item_product_name, $product_name) === 0
    ||
    $item_product_name == 'Regular ' . $product_name
  ) {
    // As we use strpos($item_product_name, $product_name) we need to be carefull on not to include
    // products that are on the form 'Online Course - FARE, AUDIT' when we are looking for
    // 'Online Course - FARE'
    if (strpos($item_product_name, $product_name . ',') === 0) {
      return FALSE;
    }
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Process a legacy order
 * @param $order
 * @return array
 */
function rcpar_data_analysis_helper_process_legacy_order($order) {
  foreach($order['items'] as $order_item){
    // If the order item was canceled, we skip it
    if ($order_item['Canceled']){
      continue;
    }
    $item_desc = $order_item['MerchName'];
    // There are several versions of the 'Online Course - Full' package
    // eg:
    //  Online Course - Full (Repeat Student)
    //  Online Course - Full (Past Student)
    if (_rcpar_data_analysis_helper_compare_item_name($item_desc, 'Online Course - Full')){
      $item_desc = 'SELECT';
    }
    // No versions of 'Premier Online Course - FULL' no need to use _rcpar_data_analysis_helper_compare_item_name
    else if ($item_desc == 'Premier Online Course - FULL'){
      $item_desc = 'PREMIUM';
    }
    // Course parts
    else if (_rcpar_data_analysis_helper_compare_item_name($item_desc, 'Online Course - AUDIT')){
      $item_desc = 'AUD';
    }
    else if (_rcpar_data_analysis_helper_compare_item_name($item_desc, 'Online Course - BEC')){
      $item_desc = 'BEC';
    }
    else if (_rcpar_data_analysis_helper_compare_item_name($item_desc, 'Online Course - FARE')){
      $item_desc = 'FAR';
    }
    else if (_rcpar_data_analysis_helper_compare_item_name($item_desc, 'Online Course - REG')){
      $item_desc = 'REG';
    }
    // Extension
    // No need to use _rcpar_data_analysis_helper_compare_item_name for the following items either
    else if ($item_desc == '6-Month Extension Online Course - AUD'){
      $item_desc = 'AUD-6MEXT';
    }
    else if ($item_desc == '6-Month Extension Online Course - BEC'){
      $item_desc = 'BEC-6MEXT';
    }
    else if ($item_desc == '6-Month Extension Online Course - FAR'){
      $item_desc = 'FAR-6MEXT';
    }
    else if ($item_desc == '6-Month Extension Online Course - REG'){
      $item_desc = 'REG-6MEXT';
    }
    // Audio courses
    else if ($item_desc == 'Roger Audio Course - AUD'){
      $item_desc = 'AUD-AUD';
    }
    else if ($item_desc == 'Roger Audio Course - BEC'){
      $item_desc = 'BEC-AUD';
    }
    else if ($item_desc == 'Roger Audio Course - FAR'){
      $item_desc = 'FAR-AUD';
    }
    else if ($item_desc == 'Roger Audio Course - REG'){
      $item_desc = 'REG-AUD';
    }
    // CRAM courses
    else if ($item_desc == 'Cram Online Course - AUDIT'){
      $item_desc = 'AUD-CRAM';
    }
    else if ($item_desc == 'Cram Online Course - BEC'){
      $item_desc = 'BEC-CRAM';
    }
    else if ($item_desc == 'Cram Online Course - FARE'){
      $item_desc = 'FAR-CRAM';
    }
    else if ($item_desc == 'Cram Online Course - REG'){
      $item_desc = 'REG-CRAM';
    }
    // EFC
    else if ($item_desc == 'CPA Exam Flashcards - AUD'){
      $item_desc = 'AUD-EFC';
    }
    else if ($item_desc == 'CPA Exam Flashcards - BEC'){
      $item_desc = 'BEC-EFC';
    }
    else if ($item_desc == 'CPA Exam Flashcards - FAR'){
      $item_desc = 'FAR-FAR';
    }
    else if ($item_desc == 'CPA Exam Flashcards - REG'){
      $item_desc = 'REG-REG';
    }
    // Offline lectures
    else if ($item_desc == 'Offline Lectures - AUD'){
      $item_desc = 'AUD-OFF-LEC';
    }
    else if ($item_desc == 'Offline Lectures - BEC'){
      $item_desc = 'BEC-OFF-LEC';
    }
    else if ($item_desc == 'Offline Lectures - FAR'){
      $item_desc = 'FAR-OFF-LEC';
    }
    else if ($item_desc == 'Offline Lectures - REG'){
      $item_desc = 'REG-OFF-LEC';
    }
    $nid = db_insert('rcpar_data_analysis_helper_legacy_order_item_revenue')
      ->fields(array(
        'order_id'  => $order_item['OrderID'],
        'item_name' => $item_desc,
        'item_id'   => $order_item['OrderItemID'],
        'quantity'  => $order_item['OrderQty'],
        'revenue'   => $order_item['ItemPrice'] * 100,
      ))
      ->execute();
  }
}

/**
 * The batch callback.
 */
function rcpar_data_analysis_helper_product_revenue_table_batch($start = 0, $stop = 0, $start_date = NULL, $csv_file = NULL) {
  $batch = array(
    'operations' => array(),
    'finished' => 'rcpar_data_analysis_helper_batch_finished',
    'title' => t('Data Analysis Helper\'s Batch'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.')
  );

  // First, let's remove all the old values from the table so we can start from scratch (if necessary).
  if (!$csv_file) {
    db_truncate('rcpar_data_analysis_helper_item_line_revenue')->execute();
  }

  $q = 'SELECT DISTINCT order_id FROM {commerce_order} o ';
  if ($start_date) {
    $q .= ' WHERE o.created >= :start_date OR o.changed >= :start_date ';
    $query_results = db_query($q, array(':start_date' => $start_date));
  }
  else {
    $query_results = db_query($q);
  }

  if ($csv_file) {
    // Let's truncate the file.
    $fileh = fopen($csv_file, 'w');
    $val = array('order_id','line_item_id','quantity','revenue');
    fputcsv($fileh, $val);
    fclose($fileh);
  }

  $count = 0;
  while ($item = $query_results->fetchObject()) {
    $batch['operations'][] = array('rcpar_data_analysis_helper_batch_process', array($item->order_id, $csv_file));
    $count++;
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('rcpar_data_analysis_helper/revenue_batch_finished'); // The path to redirect to when done.
  }
}

/**
 * The batch callback.
 */
function rcpar_data_analysis_helper_product_revenue_legacy_table_batch() {
  $batch = array(
    'operations' => array(),
    'finished' => 'rcpar_data_analysis_helper_batch_finished',
    'title' => t('Data Analysis Helper\'s Batch (legacy tables)'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total batches.'),
    'error_message' => t('Something went wrong.')
  );

  // First, let's remove all the old values from the table so we can start from scratch
  db_truncate('rcpar_data_analysis_helper_legacy_order_item_revenue')->execute();

  $q = 'SELECT `﻿OrderID` as order_id FROM legacy_merchandiseorders o ';

  $query_results = db_query($q);
  $count = 0;
  $order_ids = array();
  while ($item = $query_results->fetchObject()) {
    $order_ids[] = $item->order_id;
    $count++;
    if ($count > 150){
      $batch['operations'][] = array('rcpar_data_analysis_helper_legacy_batch_process', array($order_ids));
      $order_ids = array();
      $count = 0;
    }
  }
  if (!empty($order_ids)){
    $batch['operations'][] = array('rcpar_data_analysis_helper_legacy_batch_process', array($order_ids));
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    // If running from the browser
    batch_process('rcpar_data_analysis_helper/revenue_batch_finished'); // The path to redirect to when done.
  }

}

/**
 * Batch operation process function
 * @param $order_id
 * @param $context
 */
function rcpar_data_analysis_helper_batch_process($order_id, $csv_file, &$context) {
  $order = commerce_order_load($order_id);
  $save_to_db = true;
  if ($csv_file) {
    $save_to_db = false;
  }
  $values = rcpar_data_analysis_helper_process_order($order, $save_to_db);
  if($csv_file) {
    $fp = fopen($csv_file, 'a');
    foreach($values as $line){
      $val = array($line->order_id, $line->line_item_id,
        $line->quantity, $line->revenue);
      fputcsv($fp, $val);
    }
    fclose($fp);
  }
}

/**
 * Batch operation process function
 * @param $order_id
 * @param $context
 */
function rcpar_data_analysis_helper_legacy_batch_process($order_ids, &$context) {
  // Here we are going to collect all relevant information about the order and then
  // we will call rcpar_data_analysis_helper_process_legacy_order for the actual processing


  foreach($order_ids as $order_id){
    $opts = array('fetch' => PDO::FETCH_ASSOC);
    $order = db_query("
      SELECT * FROM legacy_merchandiseorders o
          INNER JOIN legacy_merchandiseorderstotals o_total ON o.`﻿OrderID` = o_total.OrderID 
      WHERE 
       o.`﻿OrderID` = :order_id 
       ", array(':order_id' => $order_id), $opts);
    $order = $order->fetchAssoc();

    if ($order){
      $order['items'] = array();
      $items = db_query("
      SELECT * FROM legacy_merchandiseorderitems order_item
      INNER JOIN legacy_merchandise merch ON order_item.ItemID = merch.MerchID 
      WHERE order_item.OrderID = ".$order['﻿OrderID']."
       ", array(), $opts);
      foreach ($items as $item) {
        $order['items'][] = $item;
      }

      // Ok, we got all the order info that we need, let's process it
      rcpar_data_analysis_helper_process_legacy_order($order);
    }
    else {
      drupal_set_message("There was a problem processing order: ". $order_id, 'error');
    }
  }
}

/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param $success
 * @param $results
 * @param $operations
 */
function rcpar_data_analysis_helper_batch_finished($success, $results, $operations){
  if ($success) {
    drupal_set_message('Batch completed!');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
  drupal_set_message(l('Run again', 'rcpar_data_analysis_helper/repopulate_product_revenue_table'));
}

/**
 * Menu callback to be invoked when the batch is finished
 * @return string
 */
function rcpar_data_analysis_helper_revenue_results(){
  return 'Finished';
}

/**
 * The batch callback.
 */
function rcpar_data_analysis_helper_item_lines_with_revenue_batch($start_date, $csv_file) {
  $batch = array(
    'operations' => array(),
    'finished' => 'rcpar_data_analysis_helper_batch_finished',
    'title' => t('Data Analysis Helper\'s Batch'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.')
  );

  $q = 'SELECT DISTINCT order_id FROM {commerce_order} o';
  if ($start_date && $start_date != "NULL") {
    $q .= ' WHERE o.created >= :start_date OR o.changed >= :start_date';
    $query_results = db_query($q, array(':start_date' => $start_date));
  }
  else {
    $query_results = db_query($q);
  }

  if ($csv_file) {
    // Let's truncate the file.
    $fileh = fopen($csv_file, 'w');
    $val = array(
      'order_id',
      'line_item_id',
      'sku',
      'name',
      'quantity',
      'created_date',
      'modified_date',
      'taxable',
      'revenue'
    );
    fputcsv($fileh, $val);
    fclose($fileh);
  }

  $count = 0;
  while ($item = $query_results->fetchObject()) {
    $batch['operations'][] = array('rcpar_data_analysis_line_item_with_revenue_batch_process', array($item->order_id, $csv_file));
    $count++;
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('rcpar_data_analysis_helper/revenue_batch_finished'); // The path to redirect to when done.
  }
}

/**
 * Batch operation process function
 * @param $order_id
 * @param $context
 */
function rcpar_data_analysis_line_item_with_revenue_batch_process($order_id, $csv_file, &$context) {
  $order = commerce_order_load($order_id);
  $save_to_db = false;

  // SET time_zone = '-7:00';
  $result = db_query("
        SELECT
          o.order_id, cli.line_item_id AS line_item_id, prod.sku, prod.title AS name, cli.quantity, FROM_UNIXTIME(cli.created) AS created_date,
          FROM_UNIXTIME(cli.changed) AS modified_date, FLOOR(rcpar_tax.taxable) AS taxable, FLOOR(rcpar_tax.taxed) AS taxed, FLOOR(rcpar_tax.total) AS total, FLOOR(rcpar_tax.shipping_part) AS shipping_part, fd_total.commerce_total_amount AS revenue
        FROM commerce_order o
        INNER JOIN field_data_commerce_line_items fd_cli ON fd_cli.entity_id = o.order_id AND fd_cli.entity_type = 'commerce_order'
        INNER JOIN commerce_line_item cli ON fd_cli.commerce_line_items_line_item_id = cli.line_item_id
        INNER JOIN rcpar_commerce_reports_tax rcpar_tax ON rcpar_tax.order_line_id = cli.line_item_id

        LEFT JOIN field_data_commerce_product fd_prod ON cli.line_item_id = fd_prod.entity_id AND fd_prod.entity_type = 'commerce_line_item' AND fd_prod.deleted = '0'
        LEFT JOIN commerce_product prod ON fd_prod.commerce_product_product_id = prod.product_id

        LEFT JOIN field_data_commerce_total fd_total ON fd_total.entity_id = cli.line_item_id AND fd_total.entity_type = 'commerce_line_item'

        WHERE cli.type = 'product' AND o.order_id = :order_id
    ", array(":order_id" => $order_id));

  $item_lines_info = array();
  foreach ($result as $record) {
    $item_lines_info[$record->line_item_id] = array(
      'order_id' => $record->order_id,
      'line_item_id' => $record->line_item_id,
      'sku' => $record->sku,
      'name' => $record->name,
      'quantity' => $record->quantity,
      'created_date' => $record->created_date,
      'modified_date' => $record->modified_date,
      'taxable' => $record->taxable,
      'revenue' => $record->revenue,
    );
  }
  $values = rcpar_data_analysis_helper_process_order($order, $save_to_db);
  foreach($values as $line){
    // We are just interested in "product" lines.
    if (isset($item_lines_info[$line->line_item_id])) {
      $item_lines_info[$line->line_item_id]['revenue'] = $line->revenue;
    }
  }

  $fp = fopen($csv_file, 'a');
  foreach ($item_lines_info as $line) {
    $val = array(
      $line["order_id"],
      $line["line_item_id"],
      $line["sku"],
      $line["name"],
      $line["quantity"],
      $line["created_date"],
      $line["modified_date"],
      $line["taxable"],
      $line["revenue"],
    );

    fputcsv($fp, $val);
  }
  fclose($fp);
}
