<?php
/**
 * Create drush commands to run rcpar_data_analysis_helper_product_revenue_legacy_table_batch
 * and rcpar_data_analysis_helper_product_revenue_table_batch
 * @return array
 */
function rcpar_data_analysis_helper_drush_command(){
  $items = array();
  $items['revenue_report'] = array(
    'callback' => 'rcpar_data_analysis_helper_product_revenue_legacy_table_batch',
    'description' => dt('Revenue Report Generator'),
    'arguments' => array(
      'start' => "start",
      'stop' => "stop",
    ),
  );
  $items['product_revenue_report'] = array(
    'callback' => 'rcpar_data_analysis_helper_product_revenue_table_batch',
    'description' => dt('Revenue Report Generator'),
    'arguments' => array(
      'start' => "start",
      'stop' => "stop",
    ),
    'options' => array(
      'start_date' => 'Timestamp from when we need to start considering the orders.',
      'csv_file' => 'Full path to a csv file to be created, instead of saving results to db.',
    ),

  );
    $items['item_lines_with_revenue_report_csv'] = array(
      'callback' => 'rcpar_data_analysis_helper_item_lines_with_revenue_batch',
      'description' => dt('Item Lines With Revenue CSV Report'),
      'arguments' => array(
        'start_date' => 'Timestamp from when we need to start considering the orders (pass NULL to pull all data).',
        'csv_file' => 'Full path to a csv file to be created.',
      ),
    );
  return $items;
}