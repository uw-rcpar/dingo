<?php
/**
 * @file
 * rcpar_kb.apachesolr_search_defaults.inc
 */

/**
 * Implements hook_apachesolr_search_default_searchers().
 */
function rcpar_kb_apachesolr_search_default_searchers() {
  $export = array();

  $searcher = new stdClass();
  $searcher->disabled = FALSE; /* Edit this to true to make a default searcher disabled initially */
  $searcher->api_version = 3;
  $searcher->page_id = 'knowledge_base';
  $searcher->label = 'Knowledge Base';
  $searcher->description = '';
  $searcher->search_path = 'customer-care/%rcpar_kb/search';
  $searcher->page_title = 'Help Center';
  $searcher->env_id = 'acquia_search_server_1';
  $searcher->settings = array(
    'fq' => array(
      0 => 'bundle:knowledge_base_article',
    ),
    'apachesolr_search_custom_enable' => 1,
    'apachesolr_search_search_type' => 'custom',
    'apachesolr_search_per_page' => 20,
    'apachesolr_search_spellcheck' => 0,
    'apachesolr_search_search_box' => 0,
    'apachesolr_search_allow_user_input' => 0,
    'apachesolr_search_browse' => 'results',
  );
  $export['knowledge_base'] = $searcher;

  return $export;
}
