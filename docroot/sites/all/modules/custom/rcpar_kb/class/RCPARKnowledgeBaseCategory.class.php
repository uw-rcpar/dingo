<?php

/**
 * class RCPARKnowledgeBaseCategory extends RCPARKnowledgeBase to handle categories within knowledge bases.
 */
class RCPARKnowledgeBaseCategory extends RCPARKnowledgeBase {
  /**
   * @var bool
   */
  protected $isLoaded = FALSE;
  protected $entity;
  protected $wrapper;


  /**
   * RCPARBase constructor.
   * @param $id_or_name
   * - Term ID, term machine name, or already loaded term object.
   */
  function __construct($id_or_name) {
    // The URL should be formatted with dashes and not underscores,
    // but the term name uses underscores.
    // Do a replacement of underscores in string with dashes.
    $id_or_name = str_replace("-","_",$id_or_name);

    // If an int is supplied, load by id
    if (is_numeric($id_or_name)) {
      $term = taxonomy_term_load($id_or_name);
    }
    // For string, load by string (machine name)
    else {
      if (is_string($id_or_name)) {

        // Retrieve Vocabulary ID (VID).
        $vocab = taxonomy_vocabulary_machine_name_load('knowledge_base');

        // Get taxonomy term by machine name field value.
        if ($vocab) {
          $query = new EntityFieldQuery();
          $query->entityCondition('entity_type', 'taxonomy_term')
            ->fieldCondition('field_machine_name', 'value', $id_or_name, '=');
          $result = $query->execute();

          if (!empty($result['taxonomy_term'])) {
            // Set the internal pointer of an array to its first element
            reset($result['taxonomy_term']);
            // Get the key value for the first item
            $termID = key($result['taxonomy_term']);
            $term = taxonomy_term_load($termID);
          }
        }

      }
      // Assume we were given a loaded entity
      else {
        $term = $id_or_name;
      }
    }

    if (is_object($term)) {
      $this->entity = $term;
      $wrapper = entity_metadata_wrapper('taxonomy_term', $term);
      $this->wrapper = $wrapper;
      $this->isLoaded = TRUE;
    }
  }
}