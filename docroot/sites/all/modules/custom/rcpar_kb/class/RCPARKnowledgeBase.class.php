<?php

/**
 * Class RCPARKnowledgeBase
 * A class of Knowledge Base utility functions to be shared over different parts of the RCPAR site.
 */
class RCPARKnowledgeBase {
  /**
   * @var bool
   */
  protected $isLoaded = FALSE;
  protected $entity;
  protected $wrapper;

  /**
   * @var object $vocab
   * - Vocab as returned by taxonomy_vocabulary_machine_name_load()
   */
  public $vocab;

  /**
   * RCPARBase constructor.
   * @param $id_or_name
   * - Term ID, term machine name, or already loaded term object.
   */
  function __construct($id_or_name) {
    // The URL should be formatted with dashes and not underscores,
    // but the term name uses underscores.
    // Do a replacement of underscores in string with dashes.
    $id_or_name = str_replace("-","_",$id_or_name);

    // If an int is supplied, load by id
    if (is_numeric($id_or_name)) {
      $term = taxonomy_term_load($id_or_name);
    }
    // For string, load by string (machine name)
    else {
      if (is_string($id_or_name)) {

        // Retrieve Vocabulary ID (VID).
        $vocab = taxonomy_vocabulary_machine_name_load('knowledge_base');
        $this->vocab = $vocab;

        // Get taxonomy term by machine name field value.
        if ($vocab) {
          $query = new EntityFieldQuery();
          $query->entityCondition('entity_type', 'taxonomy_term')
            ->fieldCondition('field_machine_name', 'value', $id_or_name, '=');
          $result = $query->execute();

          if (!empty($result['taxonomy_term'])) {
            // Set the internal pointer of an array to its first element
            reset($result['taxonomy_term']);
            // Get the key value for the first item
            $termID = key($result['taxonomy_term']);
            $term = taxonomy_term_load($termID);
          }
        }

      }
      // Assume we were given a loaded entity
      else {
        $term = $id_or_name;
      }
    }

    if (is_object($term)) {
      $this->entity = $term;
      $wrapper = entity_metadata_wrapper('taxonomy_term', $term);
      $this->wrapper = $wrapper;
      $this->isLoaded = TRUE;
    }
  }

  /**
   * Return an array of RCPARKnowledgeBase objects keyed by machine name
   */
  static function getAllKbs() {
    // Get the knowledge base vocabulary.
    $vocabulary = taxonomy_vocabulary_machine_name_load('knowledge_base');

    // Instantiate a RCPARKnowledgeBase object and put it in our array to be returned.
    $objects = array();
    foreach (taxonomy_get_tree($vocabulary->vid, 0, 1) as $term) {
      $kb = new RCPARKnowledgeBase($term->tid);
      $objects[$kb->getTermMachineName()] = $kb;
    }
    return $objects;
  }

  /**
   * Return a node's Knowledge Base machine name.
   */
  static function getKbFromNode($node_or_nid) {
    // If an int is supplied, load the node.
    if (is_numeric($node_or_nid)) {
      $node = node_load($node_or_nid);
    }
    // Otherwise assume we were given a loaded entity.
    else {
      $node = $node_or_nid;
    }
    // Get the node's knowledge base terms.
    $terms = field_get_items('node', $node, 'field_knowledge_base');
    foreach ($terms as $term) {
      // Find the parent term for the node (parent term is the knowledge base).
      $parent_term_count = count(taxonomy_get_parents($term['tid']));
      if ($parent_term_count == 0) {
        $kb = new RCPARKnowledgeBase($term['tid']);
        return $kb->getTermMachineName();
      }
    }
  }

  /**
   * Returns the Term's name.
   * @return string
   */
  public function getName() {
    return $this->wrapper->value()->name;
  }

  /**
   * Helper function to get the value of a field on the entity from the wrapper.
   * This function allows us to avoid the boilerplate of checking for the wrapper, supplying a fallback default,
   * and handling EntityMetadataWrapperException exceptions.
   *
   * @param string $fieldname
   * - A field name on a taxonomy term
   * @param bool $default
   * - Default value to return if there is any problem getting the value (an EntityMetadataWrapperException for example)
   * @return mixed
   * - Will return whatever value the field stores, which could be just about anything.
   */
  protected function getFieldValue($fieldname, $default = FALSE) {
    if ($wrapper = $this->wrapper) {
      try {
        // There is a bug with the machine name module that causes this to return null.
        // See here: https://www.drupal.org/node/2813785
        return $wrapper->{$fieldname}->value();
      }
      catch (EntityMetadataWrapperException $e) {
        try {
          // So for now we'll load the term and return the machine name this way.
          $term_id = $wrapper->getIdentifier();
          $term = taxonomy_term_load($term_id);
          return $term->field_machine_name['und']['0']['value'];
        }
        catch (Exception $e) {
          watchdog(__CLASS__, 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
        }
        watchdog(__CLASS__, 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      }
    }
    return $default;
  }
  /**
   * @return bool
   */
  public
  function isLoaded() {
    return $this->isLoaded;
  }

  /**
   * Returns the Term ID
   * @return int
   */
  public
  function getTermId() {
    return $this->wrapper->tid->value();
  }
  /**
   * Returns the Term machine name.
   * @return string
   */
  public
  function getTermMachineName() {
    return $this->getFieldValue('field_machine_name');
  }
  /**
   * Returns the Term name as a url-friendly string (replacing underscores with dashes).
   * @return string
   */
  public
  function getUrlFriendlyTermName() {
    $term_machine_name = $this->getFieldValue('field_machine_name');
    return str_replace("_","-",$term_machine_name);;
  }


}