<?php
/**
 * @file
 * rcpar_kb.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rcpar_kb_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "apachesolr_search" && $api == "apachesolr_search_defaults") {
    return array("version" => "3");
  }
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "hotblocks" && $api == "hotblocks") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function rcpar_kb_eck_bundle_info() {
  $items = array(
    'knowledge_base_category_list_knowledge_base_category_list' => array(
      'machine_name' => 'knowledge_base_category_list_knowledge_base_category_list',
      'entity_type' => 'knowledge_base_category_list',
      'name' => 'knowledge_base_category_list',
      'label' => 'Knowledge Base Category List',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function rcpar_kb_eck_entity_type_info() {
  $items = array(
    'knowledge_base_category_list' => array(
      'name' => 'knowledge_base_category_list',
      'label' => 'Knowledge Base Category List',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'language' => array(
          'label' => 'Entity language',
          'type' => 'language',
          'behavior' => 'language',
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_image_default_styles().
 */
function rcpar_kb_image_default_styles() {
  $styles = array();

  // Exported image style: kb_category_icon.
  $styles['kb_category_icon'] = array(
    'label' => 'KB Category Icon',
    'effects' => array(
      32 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 67,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function rcpar_kb_node_info() {
  $items = array(
    'knowledge_base_article' => array(
      'name' => t('Knowledge Base Article'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
