<?php
/**
 * @file
 * rcpar_kb.features.hierarchical_select.inc
 */

/**
 * Implements hook_hierarchical_select_default_configs().
 */
function rcpar_kb_hierarchical_select_default_configs() {
$configs = array();
$config = array(
  'save_lineage' => '1',
  'enforce_deepest' => '0',
  'resizable' => '1',
  'level_labels' => array(
    'status' => 1,
    'labels' => array(
      '0' => 'Knowledge Base',
      '1' => 'Category',
    ),
  ),
  'dropbox' => array(
    'status' => 0,
    'title' => 'All selections',
    'limit' => '0',
    'reset_hs' => '1',
    'sort' => 1,
  ),
  'editability' => array(
    'status' => 1,
    'item_types' => array(
      '0' => '',
      '1' => '',
    ),
    'allowed_levels' => array(
      '0' => 1,
      '1' => 1,
    ),
    'allow_new_levels' => 0,
    'max_levels' => '1',
  ),
  'entity_count' => array(
    'enabled' => 0,
    'require_entity' => 0,
    'settings' => array(
      'count_children' => 0,
      'entity_types' => array(
        'node' => array(
          'count_node' => array(
            'article' => 0,
            'award_winners' => 0,
            'page' => 0,
            'blog' => 0,
            'book_highlight' => 0,
            'cpa_insights' => 0,
            'careers' => 0,
            'category_page' => 0,
            'rcpa_chapter' => 0,
            'rcpa_course' => 0,
            'rcpa_exam' => 0,
            'helpcenter_question' => 0,
            'home_reviews' => 0,
            'ipq_drs_question' => 0,
            'ipq_mcq_question' => 0,
            'ipq_research_question' => 0,
            'ipq_tbs_question' => 0,
            'ipq_tbs_journal_question' => 0,
            'ipq_tbs_form_question' => 0,
            'ipq_wc_question' => 0,
            'knowledge_base_article' => 0,
            'cpa_exam_learning_center' => 0,
            'media_coverage' => 0,
            'merchandise' => 0,
            'partners' => 0,
            'product_display' => 0,
            'question_and_answer' => 0,
            'slide' => 0,
            'state_requirements' => 0,
            'student_of_the_month' => 0,
            'team' => 0,
            'testimonials' => 0,
            'rcpa_topic' => 0,
            'topic_notes' => 0,
            'user_alert' => 0,
            'user_entitlement_product' => 0,
            'rcpa_video' => 0,
            'video_bookmark' => 0,
            'video_history' => 0,
            'video_tracks' => 0,
            'webcast' => 0,
            'webform' => 0,
            'news' => 0,
          ),
        ),
      ),
    ),
  ),
  'animation_delay' => '400',
  'special_items' => array(),
  'render_flat_select' => 0,
  'config_id' => 'taxonomy-field_knowledge_base',
);

$configs['taxonomy-field_knowledge_base'] = $config;
return $configs;
}
