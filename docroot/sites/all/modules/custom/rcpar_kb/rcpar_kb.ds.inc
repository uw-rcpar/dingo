<?php
/**
 * @file
 * rcpar_kb.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function rcpar_kb_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'alternate_search_result_display';
  $ds_view_mode->label = 'Alternate search result display';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['alternate_search_result_display'] = $ds_view_mode;

  return $export;
}
