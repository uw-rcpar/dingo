(function ($) {
  Drupal.behaviors.discount_verification = {
    attach: function (context, settings) {
      $('.verify-discount-form-right #loginnow').click(function(){
        if($("#verify-discount-login-form", context).is(":visible")){
          $("#verify-discount-login-form", context).hide();
        }else{
          $("#verify-discount-login-form", context).show();
        }
      });
    }
  }
}(jQuery));