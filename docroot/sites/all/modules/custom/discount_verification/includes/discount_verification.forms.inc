<?php

// Path where the discount files will be store
define('DISCOUNTS_URI', 'private://discounts/');

function discount_verification_forms() {
  global $user;

  $coupon = discount_verification_get_current_coupon();
  if (!$coupon) {
    return 'Invalid Coupon';
  }
  // LOGGED IN USERS
  if ($user->uid != 0) {
    $block = block_load('block', variable_get('dv_logged_user_text_block_delta'));

    $renderable_arr = _block_get_renderable_array(_block_render_blocks(array($block)));
    $rendered_block = render($renderable_arr);
    $form = drupal_get_form('verify_discount_login_form');

    $str = '<div class="verify-discount-create-account-form col-md-6">
        		<div class="verify-discount-form-left">
             <h2 class="verification-form-title">' . $coupon['name'] . ' Discount Application</h2>';
    if ($coupon['code'] != 'PS-STUDENT-VER') {
      $str .= '<span class="verification-form-sub-title">Fill out the fields below to apply for your ' . $coupon['percentage'] . '% discount</span>';
    }
    $str .= drupal_render($form) . '</div></div>';
    $str .= '<div class="verify-discount-login-form col-md-6">
        		<div class="verify-discount-form-right">
             ' .
      $rendered_block
      . '</div>';
  }
  // ANONYMOUS USERS
  else {

    // FRESH START
    if ($coupon['code'] == 'FS-STUDENT-VER') {
      $create_form = drupal_get_form('verify_discount_create_account_form');
      $str = '<div class="verify-discount-create-account-form col-md-6">';
      $str .= '<div class="verify-discount-form-left">'. drupal_render($create_form) . '</div>';
      $str .= '</div>';
    }

    // PREVIOUS ROGER STUDENTS
    if ($coupon['code'] == 'PS-STUDENT-VER') {
      $login_form = drupal_get_form('verify_discount_login_form');
      $str = '<div class="verify-discount-login-form col-md-6">';
      $str .= '<div class="verify-discount-form-right"><h2 class="verification-form-title">Already have a Student Account?</h2>';
      $str .= drupal_render($login_form);
      $str .= '</div>';
    }
  }
  return $str;
}

function verify_discount_create_account_form($form, &$form_state) {

  $form['full_name'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Full Name'),
    //'#default_value' => $node->title,
    '#required' => TRUE,
  );
  $form['email_new'] = array(
    '#type'        => 'textfield',
    '#title'       => t('Email Address'),
    '#description' => t('Must be a valid e‐mail address. Your e‐mail will not be made public and will only be used if you wish to receive a new password or agree to receive certain news or notifications.'),
    '#required'    => TRUE,
  );

  $coupon = discount_verification_get_current_coupon();

  // Only for Previous Review Course
  if ($coupon['code'] == 'FS-STUDENT-VER') {
    $v = variable_get('dv_previous_review_course', '');
    $parts = explode("\n", $v);
    $form['previous_course_new'] = array(
      '#type'     => 'select',
      '#title'    => t('Previous review course'),
      '#options'  => $parts,
      '#required' => TRUE,
    );
    $form['desired_course'] = array(
      '#type'          => 'select',
      '#title'         => t('Desired Course'),
      '#multiple'      => FALSE,
      '#required'      => TRUE,
      '#options'       => array(
        ''        => '- Select -',
        'Elite'   => 'Elite',
        'Premier' => 'Premier',
        'AUD'     => 'AUD',
        'BEC'     => 'BEC',
        'FAR'     => 'FAR',
        'REG'     => 'REG',
      ),
      '#default_value' => '',
    );
  }
  $parts = explode(' ', str_replace(',', ' ', variable_get('dv_filefield_allowed_ext', '')));
  $avail_ext = array();
  foreach ($parts as $p) {
    $p = trim($p);
    if ($p != '') {
      $avail_ext[] = $p;
    }
  }
  $exts = '';
  foreach ($avail_ext as $i => $p) {
    if ($i > 0 && $i < count($avail_ext) - 1) {
      $exts .= ', ';
    }
    else {
      if ($i > 0 && $i == count($avail_ext) - 1) {
        $exts .= ' and ';
      }
    }
    $exts .= $p;
  }

  $form['discount_verification'] = array(
    '#type'        => 'file',
    '#title'       => t('Discount Verification'),
    '#description' => t("We accept $exts file formats.<br />
			Files must be less than 2 MB."),
    '#post_render' => array('discount_verification_filefield_post_render'),
    '#required'    => TRUE,
  );

  $form['registration_captcha'] = array(
    '#type' => 'captcha',
  );
  $form['submit_button'] = array(
    '#type'  => 'submit',
    '#value' => t('Request Discount'),
  );
  return $form;
}

function verify_discount_create_account_form_validate($form, &$form_state) {
  $mail = $form_state['values']['email_new'];
  $mail_error = user_validate_mail($mail);
  if ($mail_error) {
    form_set_error('email_new', $mail_error);
  }
  $coupon = discount_verification_get_current_coupon();
  $discount_type = $coupon['code'];

  // we need to check for already submitted coupons, if user have one already approved coupon
  // we must throw an error
  $existing_coupon = db_select('discount_verification', 'dv')
    ->fields('dv', array('id'))
    ->condition('email', $mail, '=')
    ->condition('status', 1, '=')
    ->condition('deleted', 0, '=')
    ->condition('discount_type', $discount_type, '=')
    ->execute()
    ->fetchAssoc();

  if ($existing_coupon) {
    form_set_error('email', t('You already submitted a request for this discount type.<br>Please contact our Customer Care team for questions or concerns.'));
  }

  // we need to check for already submitted coupons
  $existing_coupon = db_select('discount_verification', 'dv')
    ->fields('dv', array('id'))
    ->condition('email', $mail, '=')
    ->condition('status', 2, '<>')
    ->condition('deleted', 0, '=')
    ->condition('discount_type', $discount_type, '=')
    ->execute()
    ->fetchAssoc();

  if ($existing_coupon) {
    form_set_error('email', t('You already submitted a request for this discount type.'));
  }

  $avail_ext = array(str_replace(',', ' ', variable_get('dv_filefield_allowed_ext', '')));
  $file = file_save_upload('discount_verification', array(
    // Validate extensions.
    'file_validate_extensions' => $avail_ext,
    'file_validate_size'       => array(variable_get('dv_filefield_max_size', '2') * 1024 * 1024),
  ));
  // If the file passed validation:
  if ($file) {
    $discount_uri = DISCOUNTS_URI;
    //validate that the directory exists to avoid errors
    if (file_prepare_directory($discount_uri, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      // Move the file into the Drupal file system.
      if ($file = file_move($file, $discount_uri, FILE_EXISTS_RENAME)) {
        // Save the file for use in the submit handler.
        $form_state['storage']['discount_verification'] = $file;
      }
      else {
        form_set_error('discount_verification', t("Failed to write the uploaded file to the site's file folder."));
      }
    }
  }
  else {
    form_set_error('discount_verification', t('No file was uploaded.'));
  }

  drupal_add_js(
    array(
      'discount_verification_current_form' => 'verify_discount_create_account_form',
    ),
    'setting'
  );

}

function verify_discount_create_account_form_submit($form, &$form_state) {

  $file = $form_state['storage']['discount_verification'];
  unset($form_state['storage']['discount_verification']);
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);

  $full_name = $form_state['values']['full_name'];

  $coupon = discount_verification_get_current_coupon();
  $discount_type = $coupon['code'];

  $v = variable_get('dv_previous_review_course', '');
  $parts = explode("\n", $v);

  if (isset($parts[$form_state['values']['previous_course_new']])) {
    $prev_course = $parts[$form_state['values']['previous_course_new']];
  }
  else {
    $prev_course = '';
  }
  if (isset($form_state['values']['desired_course'])) {
    $desired_course = $form_state['values']['desired_course'];
  }
  else {
    $desired_course = '';
  }
  $params = array(
    'email'           => $form_state['values']['email_new'],
    'uploaded_fid'    => $file->fid,
    'previous_course' => $prev_course,
    'desired_course'  => $desired_course
  );
  $discount_id = create_discount_verif_coupon($discount_type, $params);

  discount_verification_send_zoho_email($discount_id, $full_name);

  drupal_goto('discount-thank-you');
}

function discount_verification_filefield_post_render($content, $element) {
  $to_replace = '<input class="form-control form-file required';

  $c = discount_verification_get_current_coupon();
  $message = variable_get('dv_filefield_custom_msg_' . $c['code'], '');
  if ($message) {
    $message = '<div class="custom-msg">' . $message . '</div>';
  }
  $content = str_replace($to_replace, $message . $to_replace, $content);
  return $content;
}

function verify_discount_login_form($form, &$form_state) {
  global $user;
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'discount_verification') . '/js/discount_verification.js',
  );
  $logged_in = FALSE;
  if ($user->uid != 0) {
    $logged_in = TRUE;
  }

  $coupon = discount_verification_get_current_coupon();


  $v = isset($form_state['values']) ? $form_state['values'] : array();
  if (!$logged_in) {
    $form['email'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Email Address'),
      '#default_value' => isset($v['email']) ? $v['email'] : '',
      '#required'      => TRUE,
    );
    $form['pass'] = array(
      '#type'     => 'password',
      '#title'    => t('Password'),
      '#required' => TRUE,
    );
  }

  if ($logged_in && $coupon['code'] == 'PS-STUDENT-VER') {
    $form['PS-text'] = array(
      '#markup' => '<p style="margin-bottom: 20px;">We offer our past students of Select, Premier and Elite Course Packages an exclusive discount to re-enroll in any Roger CPA Review course package.</p>
                <p style="margin-bottom: 20px;">Proceed through the qualification process by clicking the button below.</p>'
    );
  }

  // Only for FRESH START
  if ($coupon['code'] == 'FS-STUDENT-VER') {
    $v = variable_get('dv_previous_review_course', '');
    $parts = explode("\n", $v);
    $form['previous_course'] = array(
      '#type'     => 'select',
      '#title'    => t('Previous review course'),
      '#options'  => $parts,
      '#required' => TRUE,
    );
  }

  if ($coupon['code'] == 'FS-STUDENT-VER' || $coupon['code'] == 'PS-STUDENT-VER') {
    $form['desired_course'] = array(
      '#type'          => 'select',
      '#title'         => t('Desired Course'),
      '#multiple'      => FALSE,
      '#required'      => TRUE,
      '#options'       => array(
        ''        => '- Select -',
        'Elite'   => 'Elite',
        'Premier' => 'Premier',
        'AUD'     => 'AUD',
        'BEC'     => 'BEC',
        'FAR'     => 'FAR',
        'REG'     => 'REG',
      ),
      '#default_value' => '',
    );
  }
  if ($coupon['code'] == 'PS-STUDENT-VER') {
    $form['#attributes'] = array('class' => 'ps-student');
  }
  if ($coupon['code'] == 'FS-STUDENT-VER') {
    $form['#attributes'] = array('class' => 'fs-student');
  }
  $parts = explode(' ', str_replace(',', ' ', variable_get('dv_filefield_allowed_ext', '')));
  $avail_ext = array();
  foreach ($parts as $p) {
    $p = trim($p);
    if ($p != '') {
      $avail_ext[] = $p;
    }
  }
  $exts = '';
  foreach ($avail_ext as $i => $p) {
    if ($i > 0 && $i < count($avail_ext) - 1) {
      $exts .= ', ';
    }
    else {
      if ($i > 0 && $i == count($avail_ext) - 1) {
        $exts .= ' and ';
      }
    }
    $exts .= $p;
  }

  if ($coupon['code'] != 'PS-STUDENT-VER') {
    $form['discount_verification'] = array(
      '#type'        => 'file',
      '#title'       => t('Discount Verification'),
      '#description' => t("We accept $exts file formats.<br />
			Files must be less than 2 MB."),
      '#post_render' => array('discount_verification_filefield_post_render'),
      '#required'    => TRUE,
    );
  }


  $form['submit_button'] = array(
    '#type'  => 'submit',
    '#value' => t('Request Discount'),
  );
  return $form;
}

function verify_discount_login_form_validate($form, &$form_state) {
  global $user;
  $logged_in = FALSE;
  if ($user->uid != 0) {
    $logged_in = TRUE;
  }

  if (!$logged_in) {
    $u = user_load_by_mail($form_state['values']['email']);
    $valid_user = FALSE;
    if ($u) {
      $valid_user = user_authenticate($u->name, $form_state['values']['pass']);
    }

    if (!$valid_user) {
      form_set_error('email', t('Incorrect email or password'));
      return;
    }
  }
  else {
    $u = $user;
  }

  $coupon = discount_verification_get_current_coupon();
  $discount_type = $coupon['code'];

  // we need to check for already submitted coupons, if user have one already approved coupon
  // we must throw an error
  $existing_coupon = db_select('discount_verification', 'dv')
    ->fields('dv', array('id'))
    ->condition('email', $u->mail, '=')
    ->condition('status', 1, '=')
    ->condition('deleted', 0, '=')
    ->condition('discount_type', $discount_type, '=')
    ->execute()
    ->fetchAssoc();

  if ($existing_coupon) {
    form_set_error('email', t('You already submitted a request for this discount type.<br>Please contact our Customer Care team for questions or concerns.'));
  }

  // we need to check for already submitted coupons
  $existing_coupon = db_select('discount_verification', 'dv')
    ->fields('dv', array('id'))
    ->condition('email', $u->mail, '=')
    ->condition('status', 2, '<>')
    ->condition('deleted', 0, '=')
    ->condition('discount_type', $discount_type, '=')
    ->execute()
    ->fetchAssoc();

  if ($existing_coupon) {
    form_set_error('email', t('You already submitted a request for this discount type.'));
  }

  if ($coupon['code'] != 'PS-STUDENT-VER') {
    $avail_ext = array(str_replace(',', ' ', variable_get('dv_filefield_allowed_ext', '')));
    $file = file_save_upload('discount_verification', array(
      // Validate extensions.
      'file_validate_extensions' => $avail_ext,
      'file_validate_size'       => array(variable_get('dv_filefield_max_size', '2') * 1024 * 1024),
    ));
    // If the file passed validation:
    if ($file) {
      $discount_uri = DISCOUNTS_URI;
      //validate that the directory exists to avoid errors
      if (file_prepare_directory($discount_uri, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
        // Move the file into the Drupal file system.
        if ($file = file_move($file, $discount_uri, FILE_EXISTS_RENAME)) {
          // Save the file for use in the submit handler.
          $form_state['storage']['discount_verification'] = $file;
        }
        else {
          form_set_error('discount_verification', t("Failed to write the uploaded file to the site's file folder."));
        }
      }
    }
    else {
      form_set_error('discount_verification', t('No file was uploaded.'));
    }
  }

  $coupon = discount_verification_get_current_coupon();
  if (!$coupon) {
    form_set_error('discount_verification', t('Invalid Coupon.'));
  }

  drupal_add_js(
    array(
      'discount_verification_current_form' => 'verify_discount_login_form',
    ),
    'setting'
  );
}

function verify_discount_login_form_submit($form, &$form_state) {
  global $user;
  $logged_in = FALSE;
  if ($user->uid != 0) {
    $logged_in = TRUE;
  }

  if (!$logged_in) {
    $u = user_load_by_mail($form_state['values']['email']);
    $user = $u;
    drupal_session_regenerate();
  }
  else {
    $u = $user;
  }

  $coupon = discount_verification_get_current_coupon();

  if (isset($form_state['storage']['discount_verification'])) {
    $file = $form_state['storage']['discount_verification'];
    unset($form_state['storage']['discount_verification']);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    $fid = $file->fid;
  }
  else {
    $fid = 0;
  }

  $discount_type = $coupon['code'];

  $v = variable_get('dv_previous_review_course', '');
  $parts = explode("\n", $v);

  if (isset($form_state['values']['previous_course']) && isset($parts[$form_state['values']['previous_course']])) {
    $prev_course = $parts[$form_state['values']['previous_course']];
  }
  else {
    $prev_course = '';
  }
  if (isset($form_state['values']['desired_course'])) {
    $desired_course = $form_state['values']['desired_course'];
  }
  else {
    $desired_course = '';
  }
  $params = array(
    'uploaded_fid'    => $fid,
    'previous_course' => $prev_course,
    'desired_course'  => $desired_course,
    'email'           => $u->mail
  );
  $discount_id = create_discount_verif_coupon($discount_type, $params);

  $full_name = $u->field_first_name['und'][0]['safe_value'] . ' ' . $u->field_last_name['und'][0]['safe_value'];
  discount_verification_send_zoho_email($discount_id, $full_name);

  drupal_goto('discount-thank-you');
}

