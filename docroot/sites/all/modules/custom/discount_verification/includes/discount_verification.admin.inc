<?php

function discount_verification_config_form() {
  $form = array();

  $form['dv_config'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Discount Verification Settings')
  );

  $form['dv_config']['dv_previous_review_course'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Previous Review Course Options'),
    '#description'   => t('Options for the previous review course option selector (add one per line).'),
    '#default_value' => variable_get('dv_previous_review_course', ''),
  );

  $form['dv_config']['dv_filefield_allowed_ext'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Allowed doc types'),
    '#description'   => t('Allowed extensions for "Discount verification" file field (comma separated values).'),
    '#default_value' => variable_get('dv_filefield_allowed_ext', ''),
  );

  $coupons = discount_verification_available_coupons();
  $form['dv_config']['filefield_msgs'] = array(
    '#type'  => 'fieldset',
    '#tree'  => TRUE,
    '#title' => t('Custom messages to show in "Discount verification" field')
  );

  foreach ($coupons as $c) {
    $form['dv_config']['filefield_msgs']['dv_filefield_custom_msg_' . $c['code']] = array(
      '#type'          => 'textarea',
      '#title'         => t('@coupon_name text', array('@coupon_name' => $c['name'])),
      '#default_value' => variable_get('dv_filefield_custom_msg_' . $c['code'], ''),
    );
  }

  $form['dv_config']['email'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Confirmation Email Settings')
  );


  $form['dv_config']['email']['dv_confirmation_email_subject'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Confirmation Email Subject'),
    '#description'   => t('Confirmation email subject to be send when the user requires a discount.'),
    '#default_value' => variable_get('dv_confirmation_email_subject', ''),
  );

  $dv_confirmation_email = variable_get('dv_confirmation_email', array('value' => '', 'format' => NULL));
  $form['dv_config']['email']['dv_confirmation_email'] = array(
    '#type'          => 'text_format',
    '#format'        => $dv_confirmation_email['format'],
    '#title'         => t('Confirmation Email'),
    '#description'   => t('Confirmation email to be send when the user requires a discount.'),
    '#default_value' => $dv_confirmation_email['value'],
    '#token_insert'  => TRUE,
  );


  $form['dv_config']['email']['dv_zoho_email_recipients'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Zoho Email Recipients'),
    '#description'   => t('Confirmation email subject to be send when the user requires a discount.'),
    '#default_value' => variable_get('dv_zoho_email_recipients', ''),
  );

  $form['dv_config']['email']['dv_zoho_email_subject'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Confirmation Email Subject'),
    '#description'   => t('Confirmation email subject to be send when the user requires a discount.'),
    '#default_value' => variable_get('dv_zoho_email_subject', ''),
  );

  $dv_zoho_email = variable_get('dv_zoho_email', array('value' => '', 'format' => NULL));

  $form['dv_config']['email']['dv_zoho_email'] = array(
    '#type'          => 'text_format',
    '#format'        => $dv_zoho_email['format'],
    '#title'         => t('Confirmation Email'),
    '#description'   => t('Confirmation email to be send when the user requires a discount.'),
    '#default_value' => $dv_zoho_email['value'],
    '#token_insert'  => TRUE,
  );

  $form['dv_config']['email']['token_help'] = array(
    '#theme'       => 'token_tree',
    '#token_types' => array('user'),
  );

  return system_settings_form($form);
}


function discount_verification_queue_form($form, &$form_state) {
  $filters = drupal_get_query_parameters();
  $status_filter = isset($filters['status']) ? $filters['status'] : 0;
  $discount_type_filter = (isset($filters['discount_type']) && $filters['discount_type'] != '') ? explode('+', $filters['discount_type']) : array();
  $email = (isset($filters['email']) && $filters['email'] != '') ? $filters['email'] : '';

  $query = db_select('discount_verification', 'dv')->extend('PagerDefault');
  $query->fields('dv', array(
    'id',
    'email',
    'coupon_code',
    'discount_type',
    'uploaded_fid',
    'previous_course',
    'status',
    'used',
    'created_at'
  ));
  $query->condition('dv.deleted', 0, '=');
  if ($status_filter != 3) { // 3 means all values
    $query->condition('dv.status', $status_filter, '=');
  }
  if (count($discount_type_filter) > 0) {
    $or = db_or();
    foreach ($discount_type_filter as $type) {
      $or->condition('discount_type', $type, '=');
    }
    $query->condition($or);
  }

  // Filter for email
  if ($email != '') {
    $query->condition('dv.email', $email, '=');
  }

  $query->orderBy('created_at', 'DESC');
  $query->limit(50);
  $res = $query->execute();
  $rows = array(
    '#tree' => TRUE
  );
  foreach ($res as $r) {
    if ($r->uploaded_fid) {
      $file = file_load($r->uploaded_fid);
      $file_url = file_create_url($file->uri);
    }
    else {
      $file = NULL;
    }

    switch ($r->status) {
      case '0':
        $status = 'Pending';
        break;
      case '1':
        $status = 'Approved';
        break;
      case '2':
        $status = 'Disapproved';
        break;
    }

    $u = user_load_by_mail($r->email);
    $legacy_entitlements = array();
    $expired_entitlements = array();
    $previous_entitlements = '';
    if ($u) {
      $expired_entitlements = user_entitlements_get_expired_entitlements($u);
      try {
        $wrapper = entity_metadata_wrapper('user', $u->uid);
        $archive_id = $wrapper->field_archive_uid->value();
        $legacy_entitlements = _discount_verification_get_legacy_entitlements($archive_id);
        $expired_entitlements = array_merge($expired_entitlements, $legacy_entitlements);
      }
      catch (EntityMetadataWrapperException $e) {
        watchdog(__FILE__, "function " . __FUNCTION__ . " entity error");
      }
      foreach ($expired_entitlements as $key => $entitlement) {
        if (is_numeric($key)) {
          $node_wrapper = entity_metadata_wrapper('node', $entitlement['#node']);
          if ($node_wrapper->field_product_sku->value()) {
            if ($previous_entitlements) {
              $previous_entitlements .= ', ';
            }
            $previous_entitlements .= $node_wrapper->field_product_sku->value();
          }
        }
      }
    }
    $rows[$r->id] = array(
      'selected'              => array(
        '#type' => 'checkbox',
      ),
      'email'                 => array(
        '#markup' => $r->email,
      ),
      'coupon_code'           => array(
        '#markup' => $r->coupon_code,
      ),
      'discount_type'         => array(
        '#markup' => $r->discount_type,
      ),
      'previous_course'       => array(
        '#markup' => $r->previous_course,
      ),
      'previous_entitlements' => array(
        '#markup' => $previous_entitlements,
      ),
      'created_at'            => array(
        '#markup' => $r->created_at,
      ),
      'file'                  => array(
        '#markup' =>
          $file ? l(t('Download'), $file_url, array('attributes' => array('download' => $r->name . '-' . $r->discount_type))) : '',
      ),
      'status'                => array(
        '#markup' => $status,
      ),
      'used'                  => array(
        '#type'     => 'checkbox',
        '#value'    => $r->used,
        '#disabled' => TRUE,
      ),
    );
  }


  $form['filters'] = array(
    '#type' => 'container',
  );

  $form['filters']['status'] = array(
    '#type'  => 'select',
    '#title' => t('Status'),

    '#options'       => array(
      0 => t('Pending'),
      1 => t('Approved'),
      2 => t('Disapproved'),
      3 => t('All'),
    ),
    '#default_value' => $status_filter,
    '#description'   => t('Filter the rows by the selected verification status.'),
  );

  $form['filters']['discount_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Type'),
    '#options'       => array(
      'FS-STUDENT-VER' => 'FS-STUDENT-VER',
      'CS-STUDENT-VER' => 'CS-STUDENT-VER',
      'PS-STUDENT-VER' => 'PS-STUDENT-VER',
      'SS-STUDENT-VER' => 'SS-STUDENT-VER',
      'PO-STUDENT-VER' => 'PO-STUDENT-VER',
    ),
    '#multiple'      => TRUE,
    '#default_value' => $discount_type_filter,
    '#description'   => t('Filter by discount type.'),
  );

  $form['filters']['email'] = array(
    '#type' => 'textfield',
    '#title' => 'Email search',
    '#description'   => t('Filter by student email.'),
    '#default_value' => $email,
  );


  $form['apply_filers'] = array(
    '#type'  => 'submit',
    '#value' => t('Apply'),
  );
  $form['table'] = array(
    '#theme'  => 'discount_verification_form_table',
    '#header' => array(
      '',
      t('Email'),
      t('Coupon Code'),
      t('Discount Type'),
      t('Previous Course'),
      t('Previous Entitlements'),
      t('Date'),
      t('File'),
      t('Status'),
      t('Used')
    ),
    'rows'    => $rows,
  );

  $form['operation'] = array(
    '#type'          => 'select',
    '#title'         => t('Operation'),
    '#options'       => array(
      1 => t('Approve all selected.'),
      2 => t('Disapprove all selected.'),
      3 => t('Delete all selected.'),
    ),
    '#default_value' => $status_filter,
    '#description'   => t('Apply the selected action to all checked rows.'),
  );

  $form['submit_button'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );

  $form['pager_pager'] = array('#theme' => 'pager');

  $form['#redirect'] = FALSE;
  return $form;
}

function discount_verification_queue_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Apply') {
    $form_state['redirect'] = array(
      $_GET['q'],
      array(
        'query' => array(
          'status'        => $form_state['values']['status'],
          'email'         => $form_state['values']['email'],
          'username'      => $form_state['values']['username'],
          'discount_type' => implode('+', $form_state['values']['discount_type']),
        ),
      ),
    );
  }
  else {
    foreach ($form_state['values']['rows'] as $id => $row) {
      if ($row['selected']) {
        if (in_array($form_state['values']['operation'], array(1, 2))) {
          $record = array(
            'id'         => $id,
            'status'     => $form_state['values']['operation'],
            'updated_at' => format_date(time(), 'custom', 'Y-m-d H:i:s')
          );
          drupal_write_record('discount_verification', $record, 'id');
        }
        else {
          if ($form_state['values']['operation'] == 3) {
            $record = array(
              'id'         => $id,
              'deleted'    => 1,
              'updated_at' => format_date(time(), 'custom', 'Y-m-d H:i:s')
            );
            drupal_write_record('discount_verification', $record, 'id');
          }
        }
      }
    }
    switch ($form_state['values']['operation']) {
      case 1:
        drupal_set_message(t('Selected discounts have been approved.'));
        break;
      case 2:
        drupal_set_message(t('Selected discounts have been disapproved.'));
        break;
      case 3:
        drupal_set_message(t('Selected discounts have been deleted.'));
        break;
    }
  }
}

function _discount_verification_get_legacy_entitlements($archive_id) {
  global $user;
  $query = db_select('archive_entitlements', 'arch')
    ->fields('arch', array('far', 'aud', 'reg', 'bec', 'far_offline', 'aud_offline', 'reg_offline', 'bec_offline'))
    ->condition('archive_id', $archive_id)//Published.
    ->execute();
  $res = $query->fetchAll();
  $legacy_entitlements = array();
  foreach ($res as $r) {
    // we create "fake" user entitlements for the legacy entitlements
    if ($r->far) {
      $node = new stdClass();
      $node->type = 'user_entitlement_product';
      $node->language = LANGUAGE_NONE;
      $node->uid = $user->uid;
      $node->field_product_sku["und"][0]["value"] = "FAR";
      $node->field_product_title["und"][0]["value"] = "FAR - CPA Exam Review";
      $node->field_product_id["und"][0]["value"] = 16;
      $legacy_entitlements[] = array("#node" => $node);
    }
    if ($r->aud) {
      $node = new stdClass();
      $node->type = 'user_entitlement_product';
      $node->language = LANGUAGE_NONE;
      $node->uid = $user->uid;
      $node->field_product_sku["und"][0]["value"] = "AUD";
      $node->field_product_title["und"][0]["value"] = "AUD - CPA Exam Review";
      $node->field_product_id["und"][0]["value"] = 13;
      $legacy_entitlements[] = array("#node" => $node);
    }
    if ($r->reg) {
      $node = new stdClass();
      $node->type = 'user_entitlement_product';
      $node->language = LANGUAGE_NONE;
      $node->uid = $user->uid;
      $node->field_product_sku["und"][0]["value"] = "REG";
      $node->field_product_title["und"][0]["value"] = "REG - CPA Exam Review";
      $node->field_product_id["und"][0]["value"] = 15;
      $legacy_entitlements[] = array("#node" => $node);
    }
    if ($r->bec) {
      $node = new stdClass();
      $node->type = 'user_entitlement_product';
      $node->language = LANGUAGE_NONE;
      $node->uid = $user->uid;
      $node->field_product_sku["und"][0]["value"] = "BEC";
      $node->field_product_title["und"][0]["value"] = "BEC - CPA Exam Review";
      $node->field_product_id["und"][0]["value"] = 14;
      $legacy_entitlements[] = array("#node" => $node);
    }
    if ($r->far_offline) {
      $node = new stdClass();
      $node->type = 'user_entitlement_product';
      $node->language = LANGUAGE_NONE;
      $node->uid = $user->uid;
      $node->field_product_sku["und"][0]["value"] = "FAR-OFF-LEC";
      $node->field_product_title["und"][0]["value"] = "FAR - Offline Lectures";
      $node->field_product_id["und"][0]["value"] = 47;
      $legacy_entitlements[] = array("#node" => $node);
    }
    if ($r->aud_offline) {
      $node = new stdClass();
      $node->type = 'user_entitlement_product';
      $node->language = LANGUAGE_NONE;
      $node->uid = $user->uid;
      $node->field_product_sku["und"][0]["value"] = "AUD-OFF-LEC";
      $node->field_product_title["und"][0]["value"] = "AUD - Offline Lectures";
      $node->field_product_id["und"][0]["value"] = 46;
      $legacy_entitlements[] = array("#node" => $node);
    }
    if ($r->reg_offline) {
      $node = new stdClass();
      $node->type = 'user_entitlement_product';
      $node->language = LANGUAGE_NONE;
      $node->uid = $user->uid;
      $node->field_product_sku["und"][0]["value"] = "REG-OFF-LEC";
      $node->field_product_title["und"][0]["value"] = "REG - Offline Lectures";
      $node->field_product_id["und"][0]["value"] = 48;
      $legacy_entitlements[] = array("#node" => $node);
    }
    if ($r->bec_offline) {
      $node = new stdClass();
      $node->type = 'user_entitlement_product';
      $node->language = LANGUAGE_NONE;
      $node->uid = $user->uid;
      $node->field_product_sku["und"][0]["value"] = "BEC-OFF-LEC";
      $node->field_product_title["und"][0]["value"] = "BEC - Offline Lectures";
      $node->field_product_id["und"][0]["value"] = 49;
      $legacy_entitlements[] = array("#node" => $node);
    }
  }

  return $legacy_entitlements;
}
