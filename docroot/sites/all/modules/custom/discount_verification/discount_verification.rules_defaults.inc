<?php

function discount_verification_default_rules_configuration() {
    $configs = array();
    $rule = '{ "rules_remove_invalid_discounts" : {
                "LABEL" : "Remove Invalid Discounts",
            "PLUGIN" : "reaction rule",
            "OWNER" : "rules",
            "REQUIRES" : [ "php", "rules", "commerce_coupon" ],
            "ON" : { "commerce_coupon_validate" : [] },
            "IF" : [
              { "php_eval" : { "code" : "return discount_verification_should_remove_coupon($coupon);" } }
            ],
            "DO" : [ { "commerce_coupon_action_is_invalid_coupon" : [] } ]
              }
            }';
    $configs['rules_remove_invalid_discounts'] = rules_import($rule);
    return $configs;
}
