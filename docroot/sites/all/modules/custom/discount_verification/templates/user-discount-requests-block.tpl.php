<?php
    $types = discount_verification_available_coupons();
?>
<div class="discount-requests">
    <?php foreach($user_coupons as $c): ?>
		<?php if(!$c->used): ?>
			<div class="discount-request">

				<table class="table db-discount-table">
					<tbody>
						<tr>
							<td rowspan="4" class="percent-cell">
								<div class="percent"><?php print $types[$c->discount_type]['percentage']; ?>%</div>
							</td>
							<td class="discount-heading-cell" colspan="2">
								<div class="discount-heading">Discount Type</div>
							</td>
						</tr>
						<tr>
							<td class="name-cell" colspan="2">
								<div class="name"><?php print $types[$c->discount_type]['name']; ?></div>
							</td>
						</tr>
						<tr>
							<td class="status-cell-heading">
								<div class="discount-heading">Status</div>
							</td>
							<td class="code-cell-heading">
								<div class="discount-heading">Discount Code</div>
							</td>
						</tr>
						<tr>
							<td class="status-cell">
								<div class="status <?php
									if ($c->status == 1){
										$status_class = 'status-approved';
										$status = t('Approved');
									} else if ($c->status == 2){
										$status_class = 'status-disapproved';
										$status = t('Denied');
									} else {
										$status_class = 'status-pending';
										$status = t('Pending');
									}
									print $status_class;
									?>">
									<?php print $status;  ?>
								</div>
							</td>
							<td class="code-cell <?php if ($c->status == 1) { print 'cell-shown'; } else { print 'cell-hidden'; } ?>">
								<div class="code"><?php if ($c->status == 1) { print $c->coupon_code; } ?></div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		<?php endif; ?>
    <?php endforeach; ?>
</div>
