<?php

/**
 * Implements hook_page_build()
 *
 */
function rcpar_user_tracking_page_build(&$page) {
  // Add this javascript file to every page, even admin pages.
  drupal_add_js(drupal_get_path('module', 'rcpar_user_tracking') . '/js/usertracking.js');

  // Send GA custom event payloads to the clientside
  // NOTE: This is now handled by server side code, but saving this method just in case.
  /*if (isset($_SESSION) && !empty($_SESSION['rcpar_ga_send'])) {
    $payloads = array();
    foreach ($_SESSION['rcpar_ga_send'] as $key => $data) {
      rcpar_user_tracking_ga_send($data->eventCategory, $data->eventLabel, $data->eventAction, $data->eventValue);

      $payloads[] = array(
        'hitType' => 'event',
        'eventCategory' => $data->eventCategory,
        'eventAction' => $data->eventAction,
        'eventLabel' => $data->eventLabel,
        'eventValue' => $data->eventValue,
      );
    }
    unset($_SESSION['rcpar_ga_send']);
    drupal_add_js(array('rcpar_ga_send' => $payloads), 'setting');
  }*/
}

/**
 * Implements hook_free_access_registration()
 *
 * Save pertinent cookie data with the user object on free access enrollments
 *
 * @param $type
 * - 'login' or 'register'
 * @param object $partner
 * - Partner node object
 * @param object $u
 * - User object
 */
function rcpar_user_tracking_free_access_registration($type, $partner, $u) {
  $needs_save = FALSE;
  $p = new RCPARPartner($partner);

  foreach ((array) $_COOKIE as $name => $value) {
    if (substr($name, 0, 17) == 'RCPARUserTracking') {
      // Get the name of this event from the pipe separated second half of the cookie name
      $parts = explode('|', $name);
      $event_name = $parts[1];

      // Get the JSON data from the cookie
      $value_decoded = json_decode($value);

      // Inject a numeric value for GA's eventValue field
      $value_decoded->eventValue = $u->uid;

      // Create an 'action' value that represents this partner and billing type
      $value_decoded->eventAction = 'enroll|' . $p->getPartnerTypeHumanReadable() . '|' . $p->getBillingType();

      // Add some extra data - timestamp and partner stuff
      $value_decoded->timestamp = time();
      $value_decoded->partnerId = $p->getNid();

      // Store the information in the data section of the user object
      $u->data['RCPARUserTracking'][$event_name] = $value_decoded;
      $record = array(
        'uid' => $u->uid,
        'name' => $name,
        'created' => REQUEST_TIME,
        'data' => $value_decoded,
        'category' => $value_decoded->eventCategory,
        'label' => $value_decoded->eventLabel,
        'action' => $value_decoded->eventAction,
      );
      drupal_write_record('rcpar_user_tracking', $record);

      // Send some data to Google Analytics
      rcpar_user_tracking_ga_send(
        $value_decoded->eventCategory,
        $value_decoded->eventLabel,
        $value_decoded->eventAction,
        $value_decoded->eventValue
      );

      // Previously we set a session variable so that the next page request could make the user send these GA
      // events on the client side.
      //$_SESSION['rcpar_ga_send'][$event_name] = $value_decoded;

      // Delete this cookie so we don't try to track the same one twice if the user registers
      // for something else
      unset($_COOKIE["RCPARUserTracking|$name"]);
      setcookie("RCPARUserTracking|$name", '', time() - 3600);

      // Indicate that the user object needs to be resaved
      $needs_save = TRUE;
    }
  }

  // If we added any data, save it
  if ($needs_save) {
    user_save($u);
  }
}

/**
 * Implements hook_user_delete
 * If a user account is being deleted, we'll remove any associated tracking data
 * with that account
 */
function rcpar_user_tracking_user_delete($account) {
  db_query("DELETE FROM {rcpar_user_tracking} WHERE uid = :uid", array(':uid' => $account->uid));
}

/**
 * Send a custom event to Google Analytics with cURL
 *
 * This is the server-side equivalent to doing something like:
 * ga('send', {
 *  hitType: 'event',
 *  eventCategory: 'Video',
 *  eventAction: 'play',
 *  eventLabel: 'cats.mp4'
 * });
 *
 * @param string $event_category
 * @param string $event_label
 * @param string $event_action
 * @param string $event_value
 * @param string|null $tracker_id
 * @param bool $debug
 */
function rcpar_user_tracking_ga_send($event_category, $event_label, $event_action, $event_value = '', $tracker_id = NULL, $debug = FALSE) {
  // Use the tracker defined by the contrib Google Analytics module if a specific one is not provided
  $tracker_id = is_null($tracker_id) ? variable_get('googleanalytics_account', 'UA-') : $tracker_id;

  // Build the data payload
  $data = array(
    'v' => 1,
    'tid' => $tracker_id,
    'cid' => rcpar_user_tracking_gen_uuid(),
    't' => 'event',
    'ec' => $event_category, // Event category
    'el' => $event_label, // Event Action
    'ea' => $event_action, // Event Label
    'ev' => $event_value, // Event Value
  );

  // Optionally use a debugging URL
  if($debug) {
    $url = 'https://www.google-analytics.com/debug/collect';
  }
  else {
    $url = 'https://www.google-analytics.com/collect';
  }

  // Build and encode the data we're sending
  $content = http_build_query($data);
  $content = utf8_encode($content);

  // Use the user agent of the user
  $user_agent = $_SERVER['HTTP_USER_AGENT'];

  // Make a cURL post with our data to Google
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_USERAGENT, $user_agent);
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HTTPHEADER,array('Content-type: application/x-www-form-urlencoded'));
  curl_setopt($ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
  curl_setopt($ch,CURLOPT_POST, TRUE);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $content);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,5); // The number of seconds to wait while trying to connect
  curl_setopt($ch,CURLOPT_TIMEOUT, 10); // The maximum number of seconds to allow cURL functions to execute.
  curl_exec($ch);
  curl_close($ch);
}

/**
 * Generates a unique ID for use in constructing a Google Analytics event payload
 * @return string
 */
function rcpar_user_tracking_gen_uuid() {
  return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
    mt_rand( 0, 0xffff ),
    mt_rand( 0, 0x0fff ) | 0x4000,
    mt_rand( 0, 0x3fff ) | 0x8000,
    mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
  );
}

/*
 * Implements hook_block_info().
 */
function rcpar_user_tracking_block_info() {
  $blocks = array();

  // GDPR pop up notice
  $blocks['rcpar_user_tracking_gdpr_modal'] = array(
    'info' => t('RCPAR user tracking - GDPR modal'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function rcpar_user_tracking_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    // Provides a pop up notice of GDPR privace policy
    case 'rcpar_user_tracking_gdpr_modal':
      $block['subject'] = '';


      // Set up the modal
      $link = l('Learn More', 'privacy-policy', array('attributes' => array('target' => '_blank')));
      $m = new RCPARModal('gdpr-popup-modal', array('gdpr-popup-modal-wrap'), array('data-backdrop' => 'false',));
      $m->setHeaderContent('<button type="button" class="close finish-break btn-decline" data-dismiss="modal" aria-hidden="true">&times;</button>');
      $m->setBodyContent('
        <p>This website uses cookies to ensure you get the best experience. ' . $link . '</p>
        <div class="gdpr-buttons">
          <button type="button" data-dismiss="modal" class="btn btn-primary btn-accept"> Accept and continue </button>
        </div>'
      );

      // Add the content to the render array, plus css/js assets
      $ptm = drupal_get_path('module', 'rcpar_user_tracking');
      $block['content'] = array(
        '#markup' => $m->render(),
        '#attached' => array(
          'css' => array("$ptm/css/GDPR_modal.css"),
          'js' => array("$ptm/js/GDPR_modal.js"),
        ),
      );

      break;
  }
  return $block;
}
