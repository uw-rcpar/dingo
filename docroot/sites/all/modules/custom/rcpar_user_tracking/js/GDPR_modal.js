(function($, Drupal, window, document, undefined) {
  $(document).ready(function() {
    // Show the modal if the user doesn't have the cookie
    if (document.cookie.indexOf('gdpr-status=') == -1) {
      $('#gdpr-popup-modal').modal('show');
    }

    // Set a cookie when 'accept' or the close button is clicked
    $("#gdpr-popup-modal .btn-accept").on('click', function() {
      document.cookie = "gdpr-status=1";
    });
    $("#gdpr-popup-modal .btn-decline").on('click', function() {
      document.cookie = "gdpr-status=0";
    });
  }); // END - document.ready
})(jQuery, Drupal, this, this.document); //END - Closure