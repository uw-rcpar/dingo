(function ($, Drupal, window, document, undefined) {
  /**
   * Send a custom google analytics event
   * @param payload
   */
  window.rcparSendGA = function (payload) {
    try {
      var logging = true;
      if (logging) {
        console.log(payload);
      }
      ga('send', payload);
    }
    catch (e) {
    }
  };


  /**
   * Sets a cookie that the backend will save with the user data when an order is completed
   * @param name
   * - String. Unique name for this event. IMPORTANT: Don't use pipe characters in the name, we
   * parse them specially on the backend.
   * @param data
   * - An object of additional data to store
   */
  window.rcparUserTrackingSaveCookie = function (name, data) {
    $.cookie('RCPARUserTracking|' + name, JSON.stringify(data), { path: '/' });
  };

  // Look for any custom google analytics events that we need to send from the backend
  $(document).ready(function () {
    try {
      if (Drupal.settings.rcpar_ga_send) {
        $.each(Drupal.settings.rcpar_ga_send, function (i, payload) {
          rcparSendGA(payload);
        });
      }
    }
    catch (e) {
      console.log(e);
    }
  }); //END - document.ready

})(jQuery, Drupal, this, this.document); //END - Closure