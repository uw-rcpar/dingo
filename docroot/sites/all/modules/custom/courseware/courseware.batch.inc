<?php

function courseware_update_vh_chapter_batch_form($form, $form_state) {

    $form['actions']['update'] = array(
        '#type' => 'submit',
        '#value' => t('Start Update Video Histories Process'),
        '#submit' => array('courseware_update_vh_chapter_batch'),
    );

    return $form;
}

/**
 * Process video histories
*/
function courseware_update_vh_chapter_batch($form, $form_state){
    $operations = array();

    //Get the active user entitlements
    $query = db_query("SELECT n.nid
    FROM users u
    INNER JOIN node n ON n.uid = u.uid AND n.type = 'user_entitlement_product'
    INNER JOIN field_data_field_exam_version_single_val y ON y.entity_id = n.nid AND y.bundle = 'user_entitlement_product' AND y.field_exam_version_single_val_tid = 25131
    WHERE u.status = 1");

    foreach ($query as $key => $entitlement) {
        $operations[] = array('courseware_update_vh_chapter', array($entitlement->nid));
    }

    // Batch
    $batch = array(
        'operations' => $operations,
        'finished' => 'courseware_update_vh_chapter_finish',
        'title' => t('Updating Video Histories'),
        'init_message' => t('Inicializing Update Process.'),
        'progress_message' => t("Processed @current out of @total video histories."),
        'error_message' => t('Error'),
        'file' => drupal_get_path('module', 'courseware') . '/courseware.batch.inc',
    );

    batch_set($batch);
}

/**
 * Update the video history's chapter 
*/
function courseware_update_vh_chapter($entitlement_nid, &$context){
    module_load_include('inc', 'courseware', 'services/video_history');

    // Get the default exam version
    $evs = exam_versions_years();
    $year = array_pop($evs);

    // Get the video histories related with the user entitlement
    $vhresult = db_query("SELECT vh.id, vh.chapter_reference, vh.topic_reference
      FROM eck_video_history vh
      WHERE vh.entitlement_product = :entitlement",
    array(':entitlement' => $entitlement_nid));

    foreach ($vhresult as $key => $vh) {
        // Get the correct chapter related with the topic
        $chapter_nid = courseware_service_chapter_get($vh->topic_reference, $year);
        if($chapter_nid != "" && $chapter_nid != $vh->chapter_reference) {
            $vh_update = db_update('eck_video_history')
            ->fields(array('chapter_reference' => $chapter_nid))
            ->condition('id', $vh->id)
            ->execute();
        }
    }
}

/**
 *
 * @param type $success false if one of the operations fail
 * @param type $results has a insert array of values to create on the table stats
 * @param type $operations
 *
 */
function courseware_update_vh_chapter_finish($success, $results, $operations) {
    if ($success) {
        drupal_set_message("Update Process Complete Succesfully");
    }
    else {
        // An error occurred.
        // $operations contains the operations that remained unprocessed.
        $error_operation = reset($operations);
        drupal_set_message(
            t('An error occurred while processing @operation with arguments : @args', array(
                '@operation' => $error_operation[0],
                '@args' => print_r($error_operation[0], TRUE))
            ), 'error'
        );
    }
}
