<script type ="text/javascript">
    var PDFJS = PDFJS || {};
    PDFJS.workerSrc = '<?php echo $path ?>/js/pdfjs2/build/pdf.worker.js';
    PDFJS.disableWorker = false;

    var Drupal = Drupal || {};
    Drupal.courseware = {
        courseYear:         '<?php echo $course_year ?>',
        vAccess:            '<?php echo _courseware_has_vAccess($entitlement_sku) ?>',
        uid:                '<?php echo $user_uid ?>',
        servicesRoot:       '<?php echo $services_root ?>',
        path:               '<?php echo $path ?>/',
        topicId:            '<?php echo $topic->nid ?>',
        isCram:             '<?php echo $is_cram ? 'true' : 'false' ?>',
        entitlementSku:     '<?php echo $entitlement_sku ?>',
        courseSectionSlug:  '<?php echo $course_section_slug ?>',
        chapterSlug:        '<?php echo $chapter_slug ?>',
        topicSlug:          '<?php echo $topic_slug ?>',
        topicModel:          <?php echo $topic_json ?>        
    };
</script>
<link rel="stylesheet" type="text/css" href="<?php  echo drupal_get_path('module', 'courseware');?>/app/css/videojs-animations.css">

<div class="navbar-wrapper course-<?php echo strtoupper($course_section_slug) ?>">
    <div class="course-navbar">
        <div class="div-coursenav">
            <div class="logo-holder">
                <img src="<?php echo sprintf('%s/img/course-logo.png', $path, $course_section_slug) ?>" />
            </div>
            <div class="topic-title">
                <h3><?php echo $title ?></h3>
                <h4><?php echo $subTitle;
                  // Show an edit link to this topic for admins
                  if (user_access('administer site configuration')) {
                    print  ' | ' .l('[Edit]', "node/{$topic->nid}/edit");
                  }
                  ?></h4>
            </div>
        </div>
        <div class="btn-group">
            <button type="button" class="navbar-toggle-2 btn dropdown-toggle main-menu" id="showRightPush">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
          <button type="button" class="navbar-toggle-notif hidden" id="showNotifications">
          </button>
            <div class="overlay-container"></div>

        </div>
    </div>
</div>

<div ng-app="CourseModule">
    <!-- <div ui-view class="course-content"></div> -->
    <div class="course-content">
        <div class="studyview" ng-controller="StudyController">
            <div class="container-fluid">
                <div class="row courseware-row">
                    <div class="col-sm-12 col-xs-12 col-md-6 col-lg-5 video-column" id="leftpane" rcpar-resize>

                        <?php if (!$video_debug): ?>
                        <div class="btn-toolbar btn-toolbar-md btn-toolbar-dark courseware-video-header-nav">
                            <div class="btn-group btn-group-md">

                                <?php if (!is_null($url_to_previous)): ?>
                                    <a target="_self" href="<?php echo $url_to_previous ?>" class="btn btn-studytool">
                                        <i class="fa fa-step-backward"></i>
                                        Previous <span class="vjs-hidden-sm">topic</span>
                                    </a>
                                <?php endif ?>

                                <a target="_self" href="<?php echo $url_to_course_outline ?>" class="btn btn-studytool">
                                    Course Outline
                                </a>

                                 <?php if ($url_to_next): ?>
                                    <a target="_self" href="<?php echo $url_to_next ?>" class="btn btn-studytool">
                                        Next <span class="vjs-hidden-sm">topic</span>
                                        <i class="fa fa-step-forward"></i>
                                    </a>
                                <?php endif ?>
                            </div>
                            <div class="btn-group pull-right btn-group-md">
                                <button class="btn btn-studytool" role="button" ng-click="toggleAutoplay()">Autoplay is
                                    <span class="label label-success" ng-show="prefs.prefs.autoplay === true">ON</span>
                                    <span class="label label-default" ng-show="prefs.prefs.autoplay === false">OFF</span>
                                </button>
                            </div>
                        </div>
                        <?php endif ?>

                        <rcparvideo></rcparvideo>

                        <?php if (!$video_debug): ?>
                        <rcparnotes></rcparnotes>
                        <?php endif ?>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7 book-column " id="rightpane">
			<div class="pdf-loading"><div class="pdf-loading-text">Loading...</div></div>
                <span class="select-course-book clearfix">
				<!--<span class="selected-course-year"> <span class="selected-course-year-highlight">{{ getCurrentCourseYear() }}</span>  Course </span>-->
        <?php
        // This was commented as part of https://jira.rogercpareview.com/browse/EC-19 resolution
				/*
        <span class="course-years">
					<span class="course-year course-year-2015" ng-class="{'active': getCurrentCourseYear() == 2015}" ng-click="switchYear(2015)" ng-show="isYearVisible(2015)">2015 Textbook</span>
					<span class="course-year course-year-2016" ng-class="{'active': getCurrentCourseYear() == 2016}" ng-click="switchYear(2016)" ng-show="isYearVisible(2016)">2016 Textbook</span>
				</span>
        */
        ?>
			</span>
                        <?php if (!$video_debug): ?>
                        <book></book>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
