<?php

/**
 * Implements hook_schema()
 */
function courseware_schema() {
  $schema['mb_download_history'] = array(
    'description' => 'Download history for mobile app users',
    'fields' => array(
      'id' => array(
        'description' => 'Unique id',
        'type' => 'serial',
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The users uid',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => 'Content ID (not restricted to only video).',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'entitlement_id' => array(
        'description' => 'Entitlement ID',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'download_status' => array(
        'description' => 'Download status. 0 = user deleted, 1 = user downloaded',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
      ),
      'section' => array(
        'description' => 'Applicable course section',
        'type' => 'varchar',
        'length' => 13,
      ),
      'topic' => array(
        'description' => 'Topic slug',
        'type' => 'varchar',
        'length' => 13,
      ),
      'file_type' => array(
        'description' => 'File type. (review_course, cram_course, audio, etc) It’s already part of the app and will just be passed in.',
        'type' => 'varchar',
        'length' => 50,
      ),
      'file_name' => array(
        'description' => 'File name',
        'type' => 'varchar',
        'length' => 255,
      ),
      'created_on' => array(
        'description' => 'Created on (UNIX timestamp)',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'modified_on' => array(
        'description' => 'Modified (UNIX timestamp)',
        'type' => 'int',
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'uid_top_nid' => array('uid', array('topic', 4), 'nid'),
      'ent_nid' => array('entitlement_id', 'nid'),
      'top_uid' => array(array('topic', 4), 'uid'),
    ),
  );

  return $schema;
}

/**
 * updates empty chapter reference values
 */
function courseware_update_7000() {
  module_load_include('inc', 'courseware', 'services/video_history');
  $query = db_select('eck_video_history', 'vh');
  $query->fields('vh');
  $query->condition('chapter_reference', "");
  $result = $query->execute();

  while ($record = $result->fetchObject()) {
    $chapter_id = courseware_service_chapter_get($record->topic_reference);
    db_update('eck_video_history')->fields(array(
    'chapter_reference' => $chapter_id,    
    ))
      ->condition('id',$record->id)->execute();
  }
}

/**
 * Add mb_download_history history table to schema
 */
function courseware_update_7001() {
  $new_table_name = 'mb_download_history';

  $table_schema = drupal_get_schema_unprocessed('courseware', $new_table_name);
  if (!db_table_exists($new_table_name)) {
    db_create_table($new_table_name, $table_schema);
  }
}