
var Prefs = angular.module( 'ui.rcpar.userprefs', ['ngCookies', 'matchmedia-ng', 'LocalStorageModule'] );

Prefs.service('Userprefs', function($cookieStore, $rootScope, $timeout, $window, matchmedia, localStorageService) {

    var self = this,
        unsub = {};

	this.defaultPrefs = {
        autoplay: false,
        lang: "EN",
        autoSubtitles: false,
        vol: 0.5,
        notesSize: '10px',
        notesFont: "'Helvetica Neue', Helvetica, Arial, sans-serif",
        bitrate: "high",
        resize: {}
    };

    try {
        this.prefs = localStorageService.get('userprefs');
    } catch(e) {
        this.prefs = $cookieStore.get('userprefs');
    }

	if (this.prefs === null || this.prefs === "") {
		this.prefs = angular.copy(this.defaultPrefs);
	} else if(this.prefs.resize === null) {
		this.prefs.resize = {};
	}

	this.sendSizePref = function() {
        $timeout(function() {
            if (self.prefs.resize !== null && self.prefs.resize[self.size] !== null) {
                $timeout(function() {
                    var val = (self.size === null ? null : self.prefs.resize[self.size]);
                    $rootScope.$broadcast("pane-width-change", [{"size": self.size, "val":val}]);
                });
            }
        });
	};

	this.setResize = function(size) {
        this.prefs.resize[this.size] = size;
        this.save();
	};

	this.getResize = function() {
        return {"size": this.size, "val":this.prefs.resize[this.size]};
	};

	this.removeResize = function() {
        delete this.prefs.resize[this.size];
        this.save();
	};

	this.save = function() {
        try {
            localStorageService.set('userprefs', this.prefs);
        }
        catch(e) {
            $cookieStore.put('userprefs', this.prefs);
        }
	};

    unsub.print = matchmedia.onPrint(function(mediaQueryList) {
        self.isPrint = mediaQueryList.matches;
    }, $rootScope);

    unsub.screen = matchmedia.onScreen(function(mediaQueryList) {
        self.isScreen = mediaQueryList.matches;
    }, $rootScope);

    unsub.phone = matchmedia.onPhone(function(mediaQueryList) {
      	if (mediaQueryList.matches) {
            self.size = 'phone';
            self.sendSizePref();
        }
    });

    unsub.tablet = matchmedia.onTablet( function(mediaQueryList) {
      	if (mediaQueryList.matches) {
            self.size = 'tablet';
            self.sendSizePref();
        }
    });

    unsub.desktop = matchmedia.onDesktop(function(mediaQueryList) {
      	if (mediaQueryList.matches) {
            self.size = 'desktop';
            self.sendSizePref();
        }
    });

    unsub.portrait = matchmedia.onPortrait(function(mediaQueryList) {
        self.isPortrait = mediaQueryList.matches;
    });

    unsub.landscape = matchmedia.onLandscape(function(mediaQueryList) {
        self.isLandscape = mediaQueryList.matches;
    });
});