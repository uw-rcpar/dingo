var HighlighterModule = angular.module("ui.rcpar.highlighter", ['ui.rcpar.coursenav']);

HighlighterModule.factory("Highlighter", function($rootScope, ContentModel, $http, $window, $document, $timeout, $sce, authentication, services, $log) {

    var drupal          = Drupal,
        topicId         = drupal.courseware.topicId,
        coords          = {},
        contents        = {},
        scale           = 1,
        didInit         = false,
        UPDATED         = 'highlightsUpdated',
        SCALE_CHANGE    = 'highlightsScaleChanged',
        highlights      = {},
        enabled         = false;

    function drawBoxes() {
        jQuery('#viewer .highlight_box').remove();

        // foreach page / box
        angular.forEach(highlights, function(page, pageNum) {
            angular.forEach(page, function(box, id) {
                drawBox(pageNum, id, box);
            });
        });
        
        applyEnabled();
    }

    function isEnabled() {
        return enabled;
    }
    
    function toggleEnabled() {
        return setEnabled(!enabled);
    }

    function setEnabled(_enabled) {
        enabled = _enabled;
        applyEnabled();
        return enabled;
    }

    function applyEnabled() {
        var pageContainerId,
            isSelectable = isEnabled() ? "enable" : "disable";

        // foreach pageContainer we disable or enable it
        jQuery('#viewer .page').each(function(i, page) {
            // only toggle if instance inited
            if (jQuery(page).selectable('instance')) {
                jQuery(page).selectable(isSelectable);
            }
        });
    }

    function syncBoxUiClick(evt) {
        // look to the pageContainer for pageNum data
        var pageNum = jQuery(this).parent().parent().data('pageNum'),
            boxId = jQuery(this).parent().attr('id');
        
        removeBox(pageNum, boxId);
    }
    
    // @todo: need to reference .textLayer index, e.g. the page, as the parent
    function drawBox(pageNum, id, box) {
        // ensure we save at the scale of 1 in our array
        // e.g. if PDF is at 50%, we save the DOM left of 100px as 200px
        var newBox = scaleFromTrue(box);

        var pageContainer = '#pageContainer' + pageNum;

        jQuery(pageContainer).append('<div class="highlight_box" id="' + id + '"><div class="close"><i class="glyphicon glyphicon-remove"></i></div></div>');

        //add css to generated div and make it resizable & draggable
        jQuery('#' + id)
            .css({
                 'top'       : parseInt(newBox.top) + 'px',
                 'left'      : parseInt(newBox.left) + 'px',
                 'width'     : parseInt(newBox.width) + 'px',
                 'height'    : parseInt(newBox.height) + 'px',
                 'position'  : 'absolute'
            })
            // .height(height)
            // .width(width)
            .draggable({
                // grid: [20, 20],
                stop: syncBoxUiDraggableStop
            })
            .resizable({
                stop: syncBoxUiResizableStop
            })
        // close "X" icon click
        .children('.close')
            .click(syncBoxUiClick)
        ;

        jQuery('#' + id + ' .click').click(syncBoxUiClick);
    }

    function syncBoxUiDraggableStop(evt, ui) {
        var id = this.id,
            pageNum = jQuery(this).parent().data('pageNum'),
            currentBox = scaleFromTrue(highlights[pageNum][id]),
            box = {
                top: ui.position.top,
                left: ui.position.left,
                width: currentBox.width,
                height: currentBox.height
            };

        addBox(pageNum, id, box);
    }

    function syncBoxUiResizableStop(evt, ui) {
        var id = this.id,
            pageNum = jQuery(this).parent().data('pageNum'),
            box = {
                top: ui.position.top,
                left: ui.position.left,
                width: ui.size.width,
                height: ui.size.height
            };

        addBox(pageNum, id, box);
    }

    // adds OR updates a box data
    function addBox(pageNum, id, box) {

        // we ensure pageNum is a string so that highlight[pageNum] is a
        // string key (object property) and not a numeric array key
        pageNum = pageNum + '';

        // ensure we save at the scale of 1 in our array
        // e.g. if PDF is at 50%, we save the DOM left of 100px as 200px
        var saveBox = scaleToTrue(box);

        // default to object value
        highlights[pageNum] = highlights[pageNum] || {};
        // ensure value is NOT an array, rather ensure it's an object
        highlights[pageNum] = jQuery.isArray(highlights[pageNum]) ? {} : highlights[pageNum];

        highlights[pageNum][id] = saveBox;

        $rootScope.$broadcast(UPDATED, highlights);
    }

    function removeBox(pageNum, id) {
        delete highlights[pageNum][id];

        $rootScope.$broadcast(UPDATED, highlights);
    }

    function calculatePageContainerId(pageNum) {
        return '#pageContainer' + pageNum;
    }

    function initPage(pageIndex) {

        // use defaults unless usable args were given
        // var selector = _selector || selector,
        var pageNum = pageIndex + 1,
            pageId = calculatePageContainerId(pageNum),
            pageContainer = jQuery(pageId),
            x_begin, y_begin, x_end, y_end;

        // store the page number on the element for start/stop selectable events
        pageContainer.data('pageNum', pageNum);

        pageContainer.selectable({
            // listener for the start drag event, user started dragging to create
            // a box, grab the begin x/y coords
            start: function(e) {
                if (!enabled) {
                    return false;
                }

                //get the mouse position on start
                x_begin = e.pageX;
                y_begin = e.pageY;
            },

            // listener for stop drag, the user just finished creating a box
            // use the begin and end x/y coords to save a box on the individual
            // pageContainer element
            stop: function(e) {
                if (!enabled) {
                    return false;
                }

                var offset = jQuery(pageContainer).offset(),
                    count = jQuery('.highlight_box').length,
                    pageNum = jQuery(this).data('pageNum'),
                    height, width, top, left, box;

                // ensure we are working with an object for this pageNum
                highlights = highlights || {};
                highlights[pageNum] = highlights[pageNum] || {};

                // ensure unique id
                var id = 'highlight_box_' + pageNum + '_' + count;
                while (highlights[pageNum].hasOwnProperty(id)) {
                    count++;
                    id = 'highlight_box_' + pageNum + '_' + count;
                }

                //get the mouse position on stop
                x_end = e.pageX;
                y_end = e.pageY;

                // calculate width / left
                if (x_end < x_begin) {
                    width = x_begin - x_end;
                    left = x_end;
                } else {
                    width = x_end - x_begin;
                    left = x_begin;
                }

                // calculate height / top
                if (y_end < y_begin) {
                    height = y_begin - y_end;
                    top = y_end;
                } else {
                    height =  y_end - y_begin;
                    top = y_begin;
                }

                // ensure position is relative to the viewer, not the document
                top = top - offset.top;
                left = left - offset.left;

                box = {
                    top: top,
                    left: left,
                    width: width,
                    height: height
                };

                addBox(pageNum, id, box);
            }
        });

        if (isEnabled()) {
            pageContainer.selectable('enable');
        } else {
            pageContainer.selectable('disable');
        }
        
        // only attach global Highlightser events once
        if (!didInit) {
            // ensure updates to the data trigger redraws
            $rootScope.$on(UPDATED, drawBoxes);
            $rootScope.$on(SCALE_CHANGE, drawBoxes);
        }

        // draw box's for the first time for a page
        drawBoxes();

        didInit = true;
    }

    function getBoxes() {
        return highlights;
    }

    // sets the values of the boxs, assumed we're loading these from the
    // database so we don't want to re-scale these - just inject them
    // directly to the highlights data array
    function setBoxes(_highlights) {
        if (typeof _highlights == 'undefined') {
            _highlights = {};
        }

        highlights = _highlights;

        drawBoxes();
    }

    function setScale(_scale) {
        scale = _scale;

        $rootScope.$broadcast(SCALE_CHANGE, { scale: scale });
    }

    // using the true scale of a box, convert the box to be scalled up or
    // down based on the current scale
    function scaleFromTrue(box) {
        var newBox = {};

        // alter the boxs value
        Object.keys(box).map(function(value, index) {
            newBox[value] = box[value] * scale;
        });

        return newBox;
    }

    // using the current scale, convert the box from the actual position
    // data to the transposed boundries
    function scaleToTrue(box) {
        var newBox = {};

        if (scale === 0 || isNaN(scale)) {
            $log.error('Division by zero: scaleToTrue()');
        }

        // alter the boxs value
        Object.keys(box).map(function(value, index) {
            newBox[value] = box[value] / scale;
        });

        return newBox;
    }

    var hl = {
        initPage: initPage,
        get: getBoxes,
        set: setBoxes,
        setScale: setScale,
        toggleEnabled: toggleEnabled,
        isEnabled: isEnabled,
        setEnabled: setEnabled,
        UPDATED: UPDATED,
        SCALE_CHANGE: SCALE_CHANGE
    };

    return hl;
});
