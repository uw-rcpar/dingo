var ServiceURLS = angular.module("rcpar.urls", []);

ServiceURLS.service("services", function() {

    this.config = function(mode) {
        var drupal = Drupal.courseware,
            base = drupal.servicesRoot,
            courseSelectionSlug = drupal.courseSectionSlug,
            chapterSlug = drupal.chapterSlug,
            topicSlug = drupal.topicSlug;

        this.uservideoview_post = base + 'courseware-services/videohistory';

        this.notes_get = base + 'courseware-services/note/';
        this.notes_post   = base + 'courseware-services/note/'; // new
        this.notes_put    = base + 'courseware-services/note/'; // update
        this.notes_delete = base + 'courseware-services/note/'; // delete

        this.videobookmark_post   = base + 'courseware-services/videobookmark/'; // new
        this.videobookmark_put    = base + 'courseware-services/videobookmark/'; // update
        this.videobookmark_delete = base + 'courseware-services/videobookmark/'; // delete

        this.highlights_get = base + 'courseware-services/bookhighlight/'; // read
        this.highlights_post = base + 'courseware-services/bookhighlight/'; // update, create
    };

});