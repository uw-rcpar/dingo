
angular.module('ui.rcpar.video', ['ui.rcpar.bootstrap', 'ui.rcpar.bookmarks'])

.directive('volumecontrol', function($rootScope, $timeout) {
    return {
        restrict: 'A',
        scope: false,
        replace: false,
        link: function ($scope, $elem, $attr) {
            if (!$rootScope.isIPhone) {
                $scope.slider = $elem.slider({
                    orientation: "vertical",
                    range: "min",
                    min: 0,
                    max: 1,
                    step: 0.01,
                    value: parseFloat($attr.defaultVolume),
                    slide: function (event, ui) {
                        $scope.player.volume(ui.value);
                    },
                    change: function (event, ui) {
                        $scope.sliderHandle.attr("aria-valuenow", ui.value);
                    }
                });
                $timeout(function() {
                    $scope.sliderHandle = $elem.find(".ui-slider-handle");
                    $scope.sliderHandle.attr("aria-valuemin", 0).attr("aria-valuemax", 1).attr("aria-valuenow", parseFloat($attr.defaultVolume)).attr("role", "slider");
                });
            }
        }
    };
})

.service('formatTime', function() {
    // convert seconds integer to hh:mm:ss string
    this.formatTime = function(seconds, guide) {
        // Default to using seconds as guide
        guide = guide || seconds;
        var s = Math.floor(seconds % 60),
            m = Math.floor(seconds / 60 % 60),
            h = Math.floor(seconds / 3600),
            gm = Math.floor(guide / 60 % 60),
            gh = Math.floor(guide / 3600);

        // handle invalid times
        if (isNaN(seconds) || seconds === Infinity) {
            // '-' is false for all relational operators (e.g. <, >=) so this setting
            // will add the minimum number of fields specified by the guide
            h = m = s = '-';
        }

        // Check if we need to show hours
        h = (h > 0 || gh > 0) ? h + ':' : '';

        // If hours are showing, we may need to add a leading zero.
        // Always show at least one digit of minutes.
        m = (((h || gm >= 10) && m < 10) ? '0' + m : m) + ':';

        // Check if leading zero is need for seconds
        s = (s < 10) ? '0' + s : s;

        return h + m + s;
    };

    // convert hh:mm:ss, mm:ss or ss strings to seconds integer
    this.unFormatTime = function(timeCode) {
        var seconds;

        // convert HH:MM:SS to seconds
        // var timeCode = '02:04:33'; // your input string
        var a = timeCode.split(':'); // split it at the colons

        // minutes are worth 60 seconds. Hours are worth 60 minutes.
        switch (a.length) {
            case 1:
                seconds = (+a[0]);
                break;
            case 2:
                seconds = (+a[0]) * 60 + (+a[1]);
                break;

            case 3:
                seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
                break;
            default:
                seconds = 0;
        }

        return seconds;
    };
})


.controller("SimpleConfirmModalController", function($scope, $modalInstance, title, message) {
    $scope.title = title;
    $scope.message = message;
    $scope.ok = function () {
        $modalInstance.close(true);
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
})

.directive('rcparvideo', function ($compile, $window, $sce, $rootScope, $timeout, $modal, $http, Userprefs, ContentModel, authentication, services, formatTime, $log) {

    var drupal = Drupal;

    $rootScope.videoPlayerTitle = drupal.courseware.courseSectionSlug + ' ' + drupal.courseware.topicSlug;
    $rootScope.videoPlayerSubTitle = drupal.courseware.topicModel.title;

    return {
        templateUrl: drupal.courseware.path + "views/template/video-controls.html",
        restrict: "E",
        replace: true,
        priority: 11,

        link: function ($scope, $elem, attrs) {

            function round(num, dec) {
                if (!dec) { dec = 0; }
                return Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
            }

            attrs.type = attrs.type || "video/mp4";
            $scope.isIPhone = $rootScope.isIPhone;
            $scope.isIPad = $rootScope.isIPad;
            $scope.safeToSaveVideoHistory = true;

            // called during start / stop to POST to server the data
            $scope.updateViewHistory = function() {
                // false when there are errors, to avoid losing spot sometimes
                // we'll skip the attempt to save
                if (!$scope.safeToSaveVideoHistory) {
                    $log.error("VideoJS error resulting in not updating the view history.");
                    return;
                }

                $rootScope.isSaving = true;

                var percentviewed = round(($scope.player.currentTime() / $scope.player.duration() ) * 100 , 1);
                // cap percent viewed per user error in drupal input vs actual video time
                if (percentviewed > 100) {
                    percentviewed = 100;
                }

                var video_history = {
                    "userid": authentication.getUserInfo().uid,
                    "topic": $scope.topic.id,
                    "video": $scope.model.videoid,
                    "time": String(Math.round($scope.player.currentTime() * 1000)),
                    "duration": round($scope.player.duration() * 1000, 1),
                    "percentviewed": String(percentviewed),
                    "entitlement_sku": drupal.courseware.entitlementSku,
                    "exam_version": drupal.courseware.courseYear
                };

                $http({
                    url: services.uservideoview_post,
                    method: "POST",
                    data: video_history
                }).then(function(data) {
                    $rootScope.iSaving = false;
                });
            };

            var w = $elem.parent()[0].offsetWidth;

            $elem.css('height', w * 0.5625 + 'px');

            ContentModel.getTopicModel().then(function (data) {
                var timeviewed = data.content.videos[0].timeviewed || -1;
                var bookmarks = [];
                var bookmarks_length = data.userdata.videobookmarks.length || 0;
                var videosrc = [];
                var resoptions = [];
                var tracks = [];
                var track;
                var i, src, track_length;
                var len = data.content.videos[0].urls.length;
                var vidWasDefaulted = false;

                // TODO:select video from topic playlist location. For now, one video per topic:
                $scope.model = data.content.videos[data.content.videos.length - 1];
                track_length = $scope.model.tracks.length;

                $scope.content = data.content;
                $scope.topic = data;
                $scope.userdata = {};
                $scope.userdata.bookmarks = {
                    'lastviewed': timeviewed, // this is used to pass along the 'start' timecode
                    'timestamps': []           // these are actually where video_bookmarks are stored
                };

                $scope.playbackRates = [
                    {label: '2x', value: 2},
                    {value: 1.75, label: '1&#x00BE;x'},
                    {value: 1.5, label: '1&#x00BD;x'},
                    {value: 1.25, label: '1&#x00BC;x'},
                    {value: 1, label: '1x', 'selected': true},
                    {value: 0.75, label: '&#x00BE;x'},
                    {value: 0.5, label: '&#x00BD;x'}
                ];

                if (data.userdata.videobookmarks !== null && data.userdata.videobookmarks.length > 0) {
                    // bookmarks = data.userdata.videobookmarks[0].timestamps || [];
                    bookmarks = data.userdata.videobookmarks || [];
                }

                for (i = 0; i < track_length; i++) {
                    track = {
                        'kind': 'subtitles',
                        'label': $scope.model.tracks[i].label,
                        'language': $scope.model.tracks[i].language,
                        'srclang':$scope.model.tracks[i].language,
                        'src': $scope.model.tracks[i].src
                        // 'options':{
                        //     'src': $scope.model.tracks[i].src
                        // }
                    };

                    if (Userprefs.prefs.autoSubtitles && (Userprefs.prefs.lang == track.language)) {
                        $scope.defaultTrack = track;
                        // track.dflt = function () {
                        //     return true;
                        // };
                    }

                    tracks.push(track);
                }

                // demo to show an option if no subtitles exist in the CMS
                // tracks.push({
                //     'kind': 'subtitles',
                //     'label': 'Label here',
                //     'language': 'fr',
                //     'srclang': 'en',
                //     'src': 'http://'
                // });

                var initialDurationStr = '00:00';

                for (i = 0; i < len; i++) {
                    // grab initial duration string from Drupal database
                    initialDurationStr = data.content.videos[0].length;

                    src = data.content.videos[0].urls[i];

                    if (src.url && src.url.length) {
                        var vid = {};
                        vid.src = src.url;
                        vid.type = attrs.type;
                        vid.bitrate = src.bitrate;
                        if (vid.bitrate == "low") {
                            vid.icon = "bitrate-icon-sd";
                            vid.label = '<i class="bitrate-icon-sd"></i> low bandwidth';
                        } else if (vid.bitrate == "high") {
                            vid.icon = "bitrate-icon-hd";
                            vid.label = '<i class="bitrate-icon-hd"></i> high bandwidth';
                        } else {
                            vid.icon = "bitrate-icon-sd";
                            vid.label = '<i class="bitrate-icon-hd"></i> unknown bandwidth';

                            // skip mid, we don't support these yet
                            // @todo: before we implement we need to find a 'mid' icon
                            // if we don't set an icon & label the source option
                            // will fail to display
                            continue;

                        }

                        resoptions.push(src.bitrate);

                        if (src.bitrate == Userprefs.prefs.bitrate) {
                            vid.selected = true;
                            vidWasDefaulted = true;
                        }

                        videosrc.push(vid);
                    } else {
                        alert("Error");
                    }
                }

                // for mobile devices search and force update the selected
                // item to be low quality by default
                //
                /*
                if ($scope.isIPhone || $scope.isIPad) {
                    angular.forEach(videosrc, function(value, key){
                        if (videosrc[key].bitrate == "low") {
                            videosrc[key].selected = true;
                        } else {
                            videosrc[key].selected = false;
                        }
                    });
                }
                */

                $scope.videosrc = videosrc;
                $scope.location = data.location;

                // $log.debug('$scope.videosrc', $scope.videosrc);
                // $log.debug('Bookmarks', bookmarks);

                $scope.playerConfig = {
                    'techOrder': ['html5'],
                    'preload': 'auto',
                    'autoplay': ( $rootScope.isIPhone ? false : Userprefs.prefs.autoplay ),
                    'reportTouchActivity': true,
                    'controls': true,
                    'tracks': tracks,
                    'lastviewed': timeviewed,

                    'children': {
                        'splashMenu':{
                            'topic':data.title,
                            'chapter': data.prefix
                        },
                        'customControlBar': {
                            'volumePopupMenu': {
                                defaultVolume: 0.5
                            },
                            'bookmarkProgressControl': {
                                'timestamps': bookmarks,
                                'lastviewed': '750555'// is this used? not from what i can see
                                //'lastviewed': null
                            },
                            'selectBitratePopupMenu': {
                                'src': $scope.videosrc
                            }
                        }
                    }
                };

                $scope.videosrc = videosrc;
                $scope.model.videoid = $scope.model.id;

                // @todo: this no longer needs to be a custom id, can probably
                // remove or change to be the videoid value, unless the element
                // actually uses the id, in which case a better name would
                // be dom_id or element_id etc...
                $scope.model.id = "videojs" + $scope.model.id;
                // element.attr('id', $scope.model.id);

                // a visual queue to the user to ask about resuming
                $scope.initSplash = function() {
                    if ($scope.userdata.bookmarks.lastviewed > 0) {

                        var lastviewed_seconds = $scope.userdata.bookmarks.lastviewed / 1000,
                            lastviewed_timestamp = formatTime.formatTime(lastviewed_seconds, 30 * 60);

                        $scope.player.splashPlayLabel = 'Start over <span class="hidden-xs-inline">from the beginning</span>';
                        $scope.player.splashResumeLabel = 'Resume <span class="hidden-xs-inline">where you left off at ' + lastviewed_timestamp + '</span>';
                    } else {
                        $scope.player.splashPlayLabel = 'Start watching';
                    }
                };

                // configure the videojs player
                // @see file:combined-video.js function:vjs.SplashMenu.prototype.createEl
                // @see htjps://github.com/videojs/video.js/blob/stable/docs/index.md
                $scope.player = videojs("studyvideo", $scope.playerConfig, function () {

                    var initialSourceIndex = 0;

                    for (var i = 0; i < $scope.videosrc.length; i++) {
                        if ($scope.videosrc[i].hasOwnProperty("selected") && $scope.videosrc[i].selected) {
                            initialSourceIndex = i;
                        }
                    }

                    // test 404 errors on video file
                    // $scope.videosrc[initialSourceIndex].src = 'OnAMissionStartedByMyOwnAdmission-NeverGonnaFindMe';

                    this.player_.src($scope.videosrc[initialSourceIndex]);

                    // $log.info('this.player_', this.player_);
                    // $log.info("Initial Video", $scope.videosrc[initialSourceIndex].src);

                    // set $scope.player to the Video.js player
                    $scope.player = this;

                    $scope.player.hasFirstPlay = false;
                    $scope.player.isIPhone = $rootScope.isIPhone;
                    $scope.player.isIPad = $rootScope.isIPad;
                    $scope.player.isFullsreen_ = false;
                    $scope.playerConfig.isPaused_ = this.player_.options_.autoplay;

                    if ($rootScope.isIPhone) {
                        $scope.playerConfig.playtoggleTitle = "Click to begin loading video";
                    } else {
                        $scope.playerConfig.playtoggleTitle = "Play video";
                    }

                    var self = this;

                    // attempt to set the initial duration, fire timeupdate to trigger DOM update
                    self.duration(formatTime.unFormatTime(initialDurationStr));
                    self.trigger('timeupdate');

                    self.on('contextmenu', function (e) {
                       // e.preventDefault();
                    });

                    self.on("waiting", function () {
                        // player has paused to load more data

                    });

                    self.on("pause", function () {
                        $scope.playerConfig.isPaused_ = true;
                        $scope.playerConfig.playtoggleTitle = "Resume playing";
                        $scope.updateViewHistory();
                        $scope.$apply();
                    });

                    self.on("play",function () {
                        $scope.playerConfig.isPaused_ = false;
                        $scope.playerConfig.playtoggleTitle = "Pause";
                        $scope.$apply();
                    });

                    self.on("ended", function () {
                        //when the end of the media resource is reached (currentTime == duration)
                        $scope.updateViewHistory();
                    });

                    self.on("firstplay", function (e) {
                        $scope.player.bookmarksEnabled = true;
                        $scope.player.hasFirstPlay = true;
                        $scope.playerConfig.isPaused_ = false;
                        $scope.playerConfig.playtoggleTitle = "Pause";

                        if (Userprefs.prefs.autoSubtitles) {
                            var tracks = $scope.player.textTracks();
                            for (var i = 0; i < tracks.length; i++) {
                                track = tracks[i];
                                if (track.options_.language == Userprefs.prefs.lang) {
                                    $scope.player.showTextTrack(track.id_);
                                    break;
                                }
                            }
                        }

                        $scope.$apply();
                    });

                    self.on('player-first-seek', function(e) {
                        $scope.player.bookmarksEnabled = true;
                        $scope.player.hasFirstPlay = true;
                        $scope.$apply();
                    });

                    self.on("loadedmetadata", function (e, d) {
                        $elem.css({
                            'height': 'auto',
                            'width':'100%'
                        });
                        if ($scope.player.tag === null) {
                            $scope.player.tag = $scope.player.player_.tech.el_;
                        }
                    });

                    self.on("loadeddata", function () {
                        //when the player has downloaded data at the current playback position
                    });

                    self.on("loadedalldata", function () {
                        //when the player has finished downloading the source data
                    });

                    self.on('durationchange', function() {
                        $scope.initBookmarks(self.duration());
                        $scope.$apply();
                    });

                    self.on('error', function (e) {
                        $log.error("Handling error in VideoJS");
                        $scope.player.errorMSG = e;
                        var error = $scope.player.player_.error_;

                        switch (error.code) {
                            case error.MEDIA_ERR_ABORTED: // The video download was cancelled or page is being navigated away from
                                break;
                            case error.MEDIA_ERR_NETWORK: // The video connection was lost, please confirm you are connected to the internet
                                alert("There was a problem with the network.");
                                $scope.safeToSaveVideoHistory = false;
                                break;
                            case error.MEDIA_ERR_CUSTOM:
                                break;
                            case error.MEDIA_ERR_DECODE:
                                // The video is bad or in a format that cannot be played on your browser
                                break;
                            case error.MEDIA_ERR_ENCRYPTED:
                                // The video you are trying to watch is encrypted and we do not know how to decrypt it
                                break;
                            case error.MEDIA_ERR_SRC_NOT_SUPPORTED:
                                // This video is either unavailable or not supported in this browser
                                break;
                            default:
                                // alert("Unknown error");
                                // $log.error(error);
                        }
                    });

                    self.on('new-bookmark', function(e) {
                        $scope.newBookmarkWidget(self.duration());
                    });

                   if (Userprefs.prefs.vol !== null) {
                        var vol = parseFloat(Userprefs.prefs.vol);
                        this.volume(vol);
                    }

                    try {
                        window.addEventListener("beforeunload", function(event) {
                            $log.debug('Saving time last viewed on page unload');
                            // best effort to trigger the save timestamp
                            $scope.player.pause();
                            // event.returnValue = "Some message that will prepend (in some browers) the default 'are you sure' verbage, this is ignored in some (e.g. firefox)";
                        });
                    }
                    catch(err) {
                        $log.info('This browser does not support the window.addEventListener method');
                    }
                });

                // resueme and play from a milisecond timestamp
                // note: html5 player currentTime
                $scope.setCurrentTimePlay = function(miliseconds) {
                    var seconds = miliseconds / 1000;
                    $scope.player.currentTime(seconds);
                    $scope.player.play();
                };

                $scope.showTextTrack = function(track, disablesamekind) {
                    if (track !== null) {
                        $scope.player.showTextTrack(track.id_, disablesamekind);
                        Userprefs.prefs.autoSubtitles = true;
                        Userprefs.prefs.lang = track.options_.language;
                    } else {
                        $scope.player.showTextTrack(null, 'subtitles');
                        Userprefs.prefs.autoSubtitles = false;
                    }

                    Userprefs.save();
                };

                $scope.player.ready(vjs.bind(this, function() {
                    $scope.initSplash();
                    $timeout(function() {
                        $compile($scope.player.customControlBar.el_)($scope);
                        $compile($scope.player.splashMenu.el_)($scope);
                        $timeout(function() {
                            jQuery(".vjs-splash-menu").addClass("in-on");
                        });
                    });

                    $scope.player.on("updatebookmark", $scope.updateBookmark);
                    $scope.player.on("createbookmark", $scope.createBookmark);

                    $rootScope.$broadcast("player-ready", [$scope.player, $scope.model]);
                }));

                $scope.playVideo = function () {
                    $scope.player.play();
                };

                $scope.togglePlay = function() {
                    if($scope.player.paused()) {
                        $scope.player.play();
                        $scope.playerConfig.isPaused_ = false;
                    }  else {
                        $scope.player.pause();
                        $scope.playerConfig.isPaused_ = true;
                    }
                };
                // Custom handler for keypress event
                $scope.keyToggle = function($event) {
                    if($event.which == 32){
                        $scope.togglePlay();
                    }
                };

                $scope.skipback = function() {
                    var t = $scope.player.currentTime() || 0;
                    if(t > 10) {
                        $scope.player.currentTime(t - 10);
                    } else {
                        $scope.player.currentTime(2);
                    }
                };

                $scope.initBookmarks = function(duration) {

                    var len = bookmarks.length,
                        bookmarkData;

                    // load video_bookmark data
                    for (var i = 0; i < len; i++) {
                        // $log.info(bookmarks[i].id, bookmarks[i]);
                        bookmarkData = bookmarks[i];
                        bookmarkData.lastviewed = false;
                        bookmarkData.duration = duration;
                        bookmarkData.time = parseFloat(bookmarkData.time) / 1000;
                        bookmarkData.percentLeft = ((bookmarkData.time / duration) || 0 ) * 100;
                        bookmarkData.formatTime = formatTime.formatTime(bookmarkData.time, duration);
                        bookmarkData.dirHTML = '';

                        $scope.userdata.bookmarks.timestamps.push(bookmarkData);
                    }
                };

                $scope.newBookmarkWidget = function(duration) {
                    var bookmarkData = {};
                    bookmarkData.note = '';
                    bookmarkData.lastviewed = false;
                    bookmarkData.id = null;
                    bookmarkData.duration = duration;
                    bookmarkData.time = $scope.player.currentTime(); //parseFloat(bookmarkData['time']) / 1000;
                    bookmarkData.percentLeft = ((bookmarkData.time / duration) || 0 ) * 100;
                    bookmarkData.formatTime = formatTime.formatTime(bookmarkData.time, duration);
                    bookmarkData.dirHTML = '';

                    $scope.userdata.bookmarks.timestamps.push(bookmarkData);
                };

                $scope.setVideoStream = function(stream) {
                    var h = $elem[0].offsetHeight;
                    $elem.css('height', h + 'px');
                    $scope.player.setVideoStream(stream);
                };

                // remove the bookmark from the timeline if the bookmark anchor / ui deletes itself
                $scope.$on('bookmark-delete' , function(e, args) {
                    $scope.userdata.bookmarks.timestamps.splice(args.bookmarkIndex, 1);
                });
            });

            $scope.createBookmark = function () {
                var userInfo = authentication.getUserInfo(),
                    data = $scope.player.bookmarkDataToUpdate,
                    payload = {};

                payload.user = userInfo.uid;
                payload.time = Math.floor(data.time);
                payload.topic = $scope.topic.id;
                payload.video = $scope.model.videoid;
                payload.note = data.note;
                payload.entitlement_sku = drupal.courseware.entitlementSku;

                $http({
                    method: "POST",
                    url: services.videobookmark_post,
                    data: payload,
                    headers: {'Content-Type': 'application/json'}
                }).success(function (data, status, headers, config) {
                    $scope.player.post_createNewBookmark(data);
                }).error(function (data, status, headers, config) {
                    $scope.player.post_createNewBookmark(data);
                });
            };

            $scope.updateBookmark = function () {
                var userInfo = authentication.getUserInfo(),
                    data = $scope.player.bookmarkDataToUpdate,
                    payload = {};

                payload.user = userInfo.uid;
                payload.time = Math.floor(data.time);
                payload.topic = $scope.topic.id;
                payload.video = $scope.model.videoid;
                payload.note = data.note;
                payload.id = data.id;
                payload.entitlement_sku = drupal.courseware.entitlementSku;

                $http({
                    method:"PUT",
                    url:services.videobookmark_put + payload.id,
                    data:payload,
                    headers: {'Content-Type': 'application/json'}
                }).success(function(data, status, headers, config) {
                    $scope.player.post_updateBookmark(data);
                    data.time = parseFloat(data.time);
                }).error(function(data, status, headers, config) {
                    $scope.player.post_updateBookmark(data);
                });
            };

            $scope.toggleFullscreen = function(){
                if (!$scope.player.isFullscreen()) {
                    $scope.player.isFullscreen_ = true;
                    $scope.player.requestFullscreen();
                    //this.controlText_.innerHTML = this.localize('Non-Fullscreen');
                } else {
                    $scope.player.exitFullscreen();
                    $scope.player.isFullscreen_ = false;
                    //this.controlText_.innerHTML = this.localize('Fullscreen');
                }
            };

            $scope.$on('$destroy', function() {
                if ($scope.player !== null) {
                    $scope.player.pause();
                    $scope.player.showTextTrack(null);
                    $scope.player.dispose();
                }
            });
        } //end link
    }; //end return
});

