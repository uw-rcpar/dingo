/**
 * The default behavior of our player is to notautomatically hide controls
 * unless we're in fullscreen mode. The default player had no way to implement
 * this without overriding these functions.
 * @type {vjs.CoreObject}
 */
vjs.RogerPlayer = vjs.Player.extend({
  init:function(tag, options, ready){
    vjs.Player.call(this, tag, options, ready);
  }
});

// vjs.RogerPlayer.prototype.updateBookmark_boundCallbackFn = Function;

vjs.RogerPlayer.prototype.updateBookmark = function(bookmark, boundCallbackFn){
  this['bookmarkDataToUpdate'] = vjs.obj.copy(bookmark);
  this['bookmarkDataToUpdate']['time'] = this['bookmarkDataToUpdate']['time'] * 1000;
  this.updateBookmark_boundCallbackFn = boundCallbackFn;
  this.trigger('updatebookmark');
};

vjs.RogerPlayer.prototype.post_updateBookmark = function(response) {
  this.updateBookmark_boundCallbackFn(response);
};

vjs.RogerPlayer.prototype.createNewBookmark = function(bookmark, boundCallbackFn){
  this['bookmarkDataToUpdate'] = vjs.obj.copy(bookmark);
  this['bookmarkDataToUpdate']['time'] = this['bookmarkDataToUpdate']['time'] * 1000;
  this.updateBookmark_boundCallbackFn = boundCallbackFn;
  this.trigger('createbookmark');
};

vjs.RogerPlayer.prototype.post_createNewBookmark = function(response) {
  this.updateBookmark_boundCallbackFn(response);

};

vjs.RogerPlayer.prototype.currentTrack = null;

/*
vjs.RogerPlayer.prototype.showTextTrack = function(track){

  var id, disableSameKind;
  if(track !== null) {
    id = track.id_;
    disableSameKind = track.kind();
  } else {
    id = null;
    disableSameKind = this.currentTrack.kind();
  }
  this.currentTrack = track;

  //now call super method
  vjs.Player.prototype.showTextTrack.call(this, id, disableSameKind);
  return this;
};
*/

vjs.RogerPlayer.prototype.showTextTrack = function(id, disableSameKind){
  var tracks = this.textTracks_,
      i = 0,
      j = tracks.length,
      track, showTrack, kind;

  // Find Track with same ID
  for (;i<j;i++) {
    track = tracks[i];
    if (track.id() === id) {
      track.show();
      showTrack = track;

      // Disable tracks of the same kind
    } else if (disableSameKind && track.kind() == disableSameKind && track.mode() > 0) {
      track.disable();
    }
  }

  // Get track kind from shown track or disableSameKind
  kind = (showTrack) ? showTrack.kind() : ((disableSameKind) ? disableSameKind : false);
  this.currentTrack = showTrack;
  // Trigger trackchange event, captionstrackchange, subtitlestrackchange, etc.
  if (kind) {
    this.trigger(kind+'trackchange');
  }

  return this;
};


vjs.RogerPlayer.prototype.setVideoStream = function(stream) {
  if(this.cache_.src !== stream.src) {

    var resumeTime = this.currentTime();
    var remainPaused = this.paused();
    if(!remainPaused) {
      this.pause();
    }
    //this.src('');

    var srcs = this.el_.getElementsByTagName('source');
    for(var i=0;i<srcs.length;i++){
      this.tech.el_.removeChild(srcs[i]);
    }

    this.src(stream);
    this.trigger('resolutionchange');
    this.ready(vjs.bind(this, function() {
      this.one('loadeddata', vjs.bind(this, function() {
        this.currentTime(resumeTime);
      }));
      this.currentTime(resumeTime);
      this.play();
    }));

  }
};


vjs.RogerPlayer.prototype.reportUserActivity = function(event){
    if(this.isFullscreen_) {
        this.userActivity_ = true;
    } else {
        this.userActivity_ = false;
    }
};

vjs.RogerPlayer.prototype.listenForUserActivity = function(){

  var onActivity, onMouseMove, onMouseDown, mouseInProgress, onMouseUp,
    activityCheck, inactivityTimeout, lastMoveX, lastMoveY;

  onActivity = vjs.bind(this, this.reportUserActivity);

  onMouseMove = function(e) {
    // #1068 - Prevent mousemove spamming
    // Chrome Bug: https://code.google.com/p/chromium/issues/detail?id=366970
    if(e.screenX != lastMoveX || e.screenY != lastMoveY) {
      lastMoveX = e.screenX;
      lastMoveY = e.screenY;
      onActivity();
    }
  };

  onMouseDown = function() {
    onActivity();
    // For as long as the they are touching the device or have their mouse down,
    // we consider them active even if they're not moving their finger or mouse.
    // So we want to continue to update that they are active
    clearInterval(mouseInProgress);
    // Setting userActivity=true now and setting the interval to the same time
    // as the activityCheck interval (250) should ensure we never miss the
    // next activityCheck
    mouseInProgress = setInterval(onActivity, 250);
  };

  onMouseUp = function(event) {
    onActivity();
    // Stop the interval that maintains activity if the mouse/touch is down
    clearInterval(mouseInProgress);
  };

  // Any mouse movement will be considered user activity
  this.on('mousedown', onMouseDown);
  this.on('mousemove', onMouseMove);
  this.on('mouseup', onMouseUp);

  // Listen for keyboard navigation
  // Shouldn't need to use inProgress interval because of key repeat
  //this.on('keydown', onActivity);
  //this.on('keyup', onActivity);

  var setupActivityCheckIntervals = vjs.bind(this, function() {
    // Run an interval every 250 milliseconds instead of stuffing everything into
    // the mousemove/touchmove function itself, to prevent performance degradation.
    // `this.reportUserActivity` simply sets this.userActivity_ to true, which
    // then gets picked up by this loop
    // http://ejohn.org/blog/learning-from-twitter/
    this.addClass('vjs-user-active');
    activityCheck = setInterval(vjs.bind(this, function() {
      // Check to see if mouse/touch activity has happened
      if (this.userActivity_) {
        // Reset the activity tracker
        this.userActivity_ = false;

        // If the user state was inactive, set the state to active
        this.userActive(true);

        // Clear any existing inactivity timeout to start the timer over
        clearTimeout(inactivityTimeout);

        var timeout = this.options()['inactivityTimeout'];
        if (timeout > 0) {
          // In <timeout> milliseconds, if no more activity has occurred the
          // user will be considered inactive
          inactivityTimeout = setTimeout(vjs.bind(this, function () {
            // Protect against the case where the inactivityTimeout can trigger just
            // before the next user activity is picked up by the activityCheck loop
            // causing a flicker
            if (!this.userActivity_) {
              this.userActive(false);
            }
          }), timeout);
        }
      }
    }), 250);
  });
  // Clean up the intervals when we kill the player
  this.on('dispose', function(){
    clearInterval(activityCheck);
    clearTimeout(inactivityTimeout);
  });
  this.on('fullscreenchange', vjs.bind(this, function() {
    if(this.isFullscreen_) {
      setupActivityCheckIntervals();

    } else {
      clearInterval(activityCheck);
      clearTimeout(inactivityTimeout);
    }
  }));
};