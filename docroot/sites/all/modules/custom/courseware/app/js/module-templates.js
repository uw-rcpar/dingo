angular.module("template/modal/modal-confirm.html", [])

.run(["$templateCache", function($templateCache) {

    $templateCache.put("template/modal/modal-confirm.html", [
        '<div class="modal-header">',
        '    <h3 class="modal-title" ng-bind-html="title"></h3>',
        '</div>',
        '<div class="modal-body" ng-bind-html="message"></div>',
        '<div class="modal-footer">',
        '    <button class="btn btn-default" ng-click="ok()">OK</button>',
        '    <button class="btn btn-default" ng-click="cancel()">Cancel</button>',
        '</div>'
    ].join("\n"));

    $templateCache.put("template/popover/popover-bookmark.html", [
        '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">',
        '    <div class="arrow"></div>',
        '    <div class="popover-inner">',
        '        <h3 class="popover-title" ng-bind="title" ng-show="title"></h3>',
        '        <div class="popover-content" ng-bind-html="content"></div>',
        '    </div>',
        '</div>'
    ].join("\n"));

    $templateCache.put("template/popover/bookmark-ui-lastviewed.html", [
        '<div class="vjs-bookmark-menu-ui" aria-live="polite" tabindex="0">',
        '    <p>',
        '        <span class="bookmark-time-label" ng-bind="bookmarkData.formatTime"></span>',
        '        <span>This is where you left off last time.</span><br />',
        '        <span class="bookmark-directions"></span>',
        '    </p>',
        '</div>'
    ].join("\n"));

    $templateCache.put("template/popover/bookmark-ui-usernote.html", [
        '<div bookmarkui class="vjs-bookmark-menu-ui" tabindex="0">',
        // , '    <div class="btn-toolbar btn-toolbar-dark">'
        // , '        <div class="btn-group">'
        // , '            <a href="#" class="btn btn-studytool" title="delete this bookmark" ng-click="closeBookmark()"><i class="fa fa-times"></i></a>'
        // , '        </div>'
        // , '    </div>'
        '    <p class="usernote-wrap">',
        '        <div class="usernote-actions clearfix">',
        '            <span class="bookmark-time-label label label-info pull-left"',
        '                ng-bind="bookmarkData.formatTime"></span>',
        '            <span class="bookmark-delete label label-danger pull-left"',
        '                ng-click="delete()">Delete</span>',
        '            <span class="bookmark-close label label-default pull-right"',
        '                ><i class="fa fa-times" ng-click="closeBookmark()"></i></span>',
        '            <span class="bookmark-status label label-success pull-right"',
        '                ng-bind-html="bookmarkData.actionMessage"></span>',
        '        </div>',
        '        <input type="text" class="usernote"',
        '            ng-change="noteIsEdited()"',
        '            tabindex="1"',
        '            ng-model="bookmarkData.note"',
        '            maxlength="100"',
        '            />',
        '    </p>',
        '</div>'
    ].join("\n"));

}]);
