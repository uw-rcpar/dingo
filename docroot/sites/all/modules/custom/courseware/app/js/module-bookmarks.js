angular.module( 'ui.rcpar.bookmarks', ['ngAria', 'ui.bootstrap', 'rcpar.urls', 'ui.rcpar.userprefs', 'template/modal/modal-confirm.html'])

// this directive manages the actions of the little icons that are inside the timeline. hovering over one
// should trigger the bookmark ui to show the tooltip
//
// this is where we use bookmark data, but the video module should load bookmark data into this directive
.directive('bookmarkanchor', function ($compile, $templateCache, $timeout, $rootScope, $parse, $window, $tooltip, $log) {

    var drupal = Drupal;

    // $log.info("Directive: bookmarkanchor");

    return {
        restrict:'EA',
        scope:true,
        priority:11,
        link:function($scope, $elem, $attr) {

            var drupal = drupal;

            // grab bookmark data for one bookmark from the video module
            $scope.bookmarkData = $scope.$parent.$eval($attr.bookmarkData);
            $scope.bookmarkIndex = $scope.$parent.$eval($attr.bookmarkIndex);
            $scope.player = $scope.$parent.$parent.player;

            $scope.isIos = $rootScope.isIPhone || $rootScope.isIPad;

            // used to manage the delay of mouseenter / mouseleave to close the bookmarkui popup
            $scope.mouseleaveTimeoutClosePromise = false;

            $scope.isOpen = false;

            // configuration for jquery popup
            var options = {
                html: true,
                placement: 'top',
                trigger: 'manual',
                container: $elem,
                viewport: '.vjs-tech',
                animation: false,
                template: $templateCache.get('template/popover/popover-bookmark.html'),
                maxWidth: $scope.player.width() - 20,
                minWidthSelector: '.usernote',
                eventDelegate: $scope.player,
                contentPrelayoutSelector: ".usernote",
                contentPrelayoutStyles: {
                    fontFamily: '"Open Sans", Arial, sans-serif',
                    fontSize: '14px',
                    display: 'inline',
                    fontWeight: 'normal',
                    wordWrap: 'break-word',
                    outlineWidth: '1px'
                },
                widthOffsetSelector: ".bookmark-time-label",
                focusMinWidth: 276
            };

            if (options.maxWidth >= 400) {
                options.maxWidth = Math.round(options.maxWidth - (options.maxWidth / (3 * 1.615) ));
            }

            // not 100% sure when 'lastViewed' is true per business rules
            if ($scope.bookmarkData.lastViewed) {
                options.content = $compile($templateCache.get('template/popover/bookmark-ui-lastviewed.html'))($scope);
            } else {
                options.content = $compile($templateCache.get('template/popover/bookmark-ui-usernote.html'))($scope);
            }

            $scope.pop = new jQuery.fn.popover.Constructor($elem.find('.popup-ui-container')[0], options);

            $scope.openBookmark = function(e) {
                // cancel timeout to close the popup if it's started
                $timeout.cancel($scope.mouseleaveTimeoutClosePromise);

                // only open if it's not visible yet
                if (!$scope.isOpen) {
                    // $log.debug('Opening Bookmark');
                    $scope.pop.show();

                    // is visible when open
                    $scope.isOpen = $scope.pop.tip().is(':visible');

                    // tell the world this bookmark was opened
                    $rootScope.$broadcast('bookmark-opened', {
                        bookmarkIndex: $scope.bookmarkIndex
                    });
                }
            };

            $scope.closeBookmark = function() {
                if ($scope.isEditing) {
                    return;
                }

                $scope.pop.hide();

                // is visible when open
                $scope.isMouseOverTooltip = false;
                $scope.isOpen = $scope.pop.tip().is(':visible');
            };

            // close this bookmark if another was opened in the world
            $rootScope.$on('bookmark-opened', function(evt, args) {
                if (args.bookmarkIndex != $scope.bookmarkIndex) {
                    $scope.closeBookmark();
                }
            });

            if (!$scope.isIos) {
                $elem.on('mouseover', function() {
                    $scope.isMouseOverTooltip = true;
                    $scope.openBookmark();
                });

                $elem.on('mouseleave', function() {
                    $scope.isMouseOverTooltip = false;
                    $timeout.cancel($scope.mouseleaveTimeoutClosePromise);
                    $scope.mouseleaveTimeoutClosePromise = $timeout(function() {
                        $scope.closeBookmark();
                    }, 500);
                });
            }

            $scope.gotoBookmark = function() {
                if (!$scope.player.hasFirstPlay) {
                    $scope.player.play();
                }

                // navigate player to this time in the timeline
                $scope.player.currentTime($scope.bookmarkData.time);
            };

            $scope.$on('$destroy', function () {
                $elem.off();
            });
        }
    };
})

// this directive manages the popup interface, the html and actions around editing the bookmark in the tooltip
// that pops up when creating a new or viewing an existing bookmark
.directive( 'bookmarkui', function($rootScope, $document, $timeout, $modal, $http, services, formatTime, $log) {

    return {
        restrict: 'EA',
        scope: false,
        priority: 9,

        link: function($scope, $elem, $attr) {

            $scope.isSaving = false;
            $scope.isEditing = false;
            $scope.isMouseOverTooltip = false;
            $scope.saveTimeoutPromise = false;

            // once an id is not null or not false, we can considerit not new
            $scope.isNew = function() {
                return $scope.bookmarkData.id ? false : true;
            };

            $scope.formatNote = function() {
                $scope.bookmarkData.displayNote = $scope.bookmarkData.note;
            };

            $scope.noteIsEdited = function() {
                // $log.debug('Starting save delay', $scope.bookmarkData.note);

                // used to prevent mouseout exits or to skip other actions we don't
                // want to happen when we started editing a note
                // should be set back to false when the bookmark has started to post
                // a save to the server
                $scope.isEditing = true;

                $scope.bookmarkData.actionMessage = 'Editing...';

                if ($scope.saveTimeoutPromise) {
                    $log.debug('Canceling previous timeout');
                    $timeout.cancel($scope.saveTimeoutPromise);
                }

                $scope.saveTimeoutPromise = $timeout(function() {
                    $log.debug('Saving bookmark');
                    $scope.bookmarkData.actionMessage = 'Pending...';
                    $scope.updateBookmark();
                }, 500);
            };

            $scope.updateBookmark = function() {
                $log.info('Sending data to server...');

                $scope.isSaving = true;
                $scope.isEditing = false;
                $scope.saveStart  = new Date().getTime();

                if ($scope.isNew()) {
                    $scope.bookmarkData.actionMessage = '<i class="fa fa-gear fa-spin"></i> Saving...';
                    $scope.player.createNewBookmark($scope.bookmarkData, $scope.onBookmarkUpdated);
                } else {
                    $scope.bookmarkData.actionMessage = '<i class="fa fa-gear fa-spin"></i> Updating...';
                    $scope.player.updateBookmark($scope.bookmarkData, $scope.onBookmarkUpdated);
                }
            };

            $scope.calculateBookmarkLayout = function() {
                // re-calculate the popup layout
                $timeout(function() {
                    $scope.pop.calcLayout();
                });
            };

            $scope.delete = function() {
                // preserve pause state or resume playing when done with
                // popup delete confirm box
                var preservePause = $scope.player.paused();
                if (!preservePause) {
                    preservePause = true;
                    $scope.player.pause();
                }

                var modalInstance = $modal.open({
                    templateUrl: 'template/modal/modal-confirm.html',
                    controller: 'SimpleConfirmModalController',
                    size: "sm",
                    backdrop:false,
                    windowClass:"blue-form shadow modal-vcenter",
                    resolve: {
                        title:function() {
                            return "Are you sure?";
                        },
                        message: function() {
                            return "You are about to delete the bookmark at " + $scope.bookmarkData.formatTime + ". This cannot be undone.";
                        }
                    }
                });

                modalInstance.result.then(function() {
                    // confirm delete
                    $log.info('Confirmed delete bookmark');

                    if (!$scope.isNew()) {
                        $log.info('Deleted an existing bookmark');
                        $scope.$emit('bookmark-delete', { bookmarkIndex: $scope.bookmarkIndex });
                    } else {
                        $log.info('Deleted a new bookmark');
                    }

                    $http({
                        method: "DELETE",
                        url: services.videobookmark_delete + $scope.bookmarkData.id
                    }).success(function(data, status, headers, config) {
                        $log.info('Delete success');
                    }).error(function(data, status, headers, config) {
                        $log.error('Delete fail');
                    });

                    if (preservePause) {
                        $scope.player.play();
                    }
                }, function () {
                    if (preservePause) {
                        $scope.player.play();
                    }
                });

            };

            $scope.onBookmarkUpdated = function(bookmarkData) {
                var duration = $scope.player.duration();

                // $log.debug('Updated: bookmarkData', bookmarkData);

                bookmarkData.lastviewed = false;
                bookmarkData.duration = duration;
                bookmarkData.time = parseFloat(bookmarkData.time) / 1000;
                bookmarkData.percentLeft = ((bookmarkData.time / duration) || 0 ) * 100;
                bookmarkData.formatTime = formatTime.formatTime(bookmarkData.time, duration);

                // if we changed the bookmark after the save started, use the users data from the DOM to ensure
                // we use the users last typed state of the bookmark
                // if it's different, this should trigger the save again
                bookmarkData.note = $scope.bookmarkData.note;
                
                $scope.bookmarkData = bookmarkData;

                $scope.formatNote();

                $scope.bookmarkData.actionMessage = '<i class="fa fa-check"></i> Bookmark saved';

                var saveEnd = new Date().getTime();
            };

            // open the bookmark window if clicking on the 'new bookmark' action
            $timeout(function() {
                if ($scope.isNew()) {
                    $scope.openBookmark();
                    $scope.updateBookmark();
                }

                $scope.formatNote();
            });
        }
    };
});
