// NotesModule.config
// NotesModule.controller: RCPARNotesController
// NotesModule.directive: rcparnotes
// NotesModule.directive: note"
//
NotesModule = angular.module("ui.rcpar.notes", ['ui.rcpar.bootstrap',  'rcpar.urls', 'textAngular'])

.config(['$provide', function($provide) {
    // this demonstrates how to register a new tool and add it to the default toolbar
    $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions){
        // $delegate is the taOptions we are decorating
        // register the tool with textAngular
        taRegisterTool('save', {
            display: '<button class="btn btn-default"><i class="fa fa-save"></i> save</button>',
            action: function(){
                this.$emit("on-toolbar-save");
            }
        });

        taRegisterTool('delete', {
            display: '<buton class="btn btn-default"><i class="fa fa-times"></i> delete</button>',
            action: function() {
                this.$emit("on-toolbar-delete");
            }
        });

        // $delegate is the taOptions we are decorating
        // here we override the default toolbars and classes specified in taOptions.
        taOptions.toolbar = [
            ['delete'],
            ['bold', 'italics', 'underline'],
            ['ul', 'ol'],
            ['undo', 'clear'],
            ['save']
            // ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
            // ['p']
            //,['justifyLeft','justifyCenter','justifyRight']
            //,['html', 'insertImage', 'insertLink', 'unlink']
        ];

        taOptions.classes = {
            focussed: 'focussed',
            toolbar: 'btn-toolbar btn-toolbar-xs',
            toolbarGroup: 'btn-group btn-group-xs',
            toolbarButton: 'btn btn-default',
            toolbarButtonActive: 'active',
            disabled: 'disabled',
            textEditor: 'form-control',
            htmlEditor: 'form-control'
        };

        return taOptions;
    }]);
}]);


NotesModule.controller("RCPARNotesController", function($scope, $http, $q,  $modal, $timeout, services, textAngularManager, ContentModel, $window, $log) {

    var drupal = Drupal;

    $scope.topicModel = drupal.courseware.topicModel;
    $scope.notes = drupal.courseware.topicModel.userdata.notes;
    $scope.blurListenerEnabled = true;

    // just load $scope.notes in from global scope now, rather
    // than make a promise just to load this later
    //
    // this.getNotes = function() {
    //     var notesPromise = ContentModel.getTopicModel();
    //     notesPromise.then(function(data) {
    //         $scope.topicModel = data;
    //         $scope.notes = data.userdata.notes;
    //     });
    //     return notesPromise;
    // };

    $scope.deleteNote = function(id) {

        // open a popup window to verify the delete action with the user
        var modalInstance = $modal.open({
            templateUrl: 'template/modal/modal-confirm.html',
            controller: "SimpleConfirmModalController",
            size: 'sm',
            resolve: {
                title: function () {
                    return "Are you sure you want to delete this note?";
                },
                message:function() {
                    return "You cannot undo this action.";
                }
            }
        });

        modalInstance.result.then(function () {
            // disable delete button while waiting
            $scope.deleteButton
                .removeClass('btn-default')
                .addClass('btn-success')
                .html('<i class="fa fa-gear fa-spin"></i> Deleting...');

            $http({
                method: "DELETE",
                url: [services.notes_delete, id].join('')
            }).success(function(data) {

                // after a successful delete call we then
                // remove the note from the model array
                for(var i = 0; i < $scope.notes.length; i++) {
                    if ($scope.notes[i].id == id) {
                        $scope.notes.splice(i, 1);
                        break;
                    }
                }

                // enable delete button again
                $scope.deleteButton
                    .removeClass('btn-success')
                    .addClass('btn-default')
                    .html('<i class="fa fa-times"></i> delete');
            });

        }, function () {

        });
    };

    $scope.saveNote = function(model) {

        model.topic = $scope.topicModel.id;
        model.entitlement_sku = drupal.courseware.entitlementSku;

        // call to server to update note
        return $http({
            method: "PUT",
            url: services.notes_post + model.id,
            data: model
        }).success(function(data) {
            // $scope.saveNote success
        });
    };

    $scope.createMode = false;

    $scope.newnote = { newNoteText: '' };

    $scope.newNote = function() {

        // enabled blur listener
        $scope.blurListenerEnabled = true;
        $scope.createMode = true;
        $scope.newnote.newNoteText = '';

        var editorScope = textAngularManager.retrieveEditor('newnoteinput').scope;

        $timeout(function(){
            // trigger newnoteinput focus
            editorScope.newnote = { newNoteText: '' };
            editorScope.displayElements.text.trigger('focus');
        });
    };

    // listen for save button click
    $scope.$on("on-toolbar-save", function(event) {
        if (event.defaultPrevented) {
            return;
        }

        // patch to avoid saving twice (click + blur)
        $scope.blurListenerEnabled = false;

        $scope.saveNewNote();
    });

    // function send new note to the server
    $scope.saveNewNote = function() {

        // disable save button while saving
        $scope.saveButton
            .removeClass('btn-default')
            .addClass('btn-success').html('<i class="fa fa-gear fa-spin"></i> Saving...');

        // post new note to the server
        $http({
            method: "POST",
            url: services.notes_put,
            data: {
                'note': $scope.newnote.newNoteText,
                'topic': $scope.topicModel.id,
                "entitlement_sku": drupal.courseware.entitlementSku
            }
        }).success(function(data) {
            $scope.notes.unshift(data.note);
            $scope.newnote.newNoteText = "";
            $scope.createMode = false;

            // enable save button again after saved success
            $scope.saveButton
                .removeClass('btn-success')
                .addClass('btn-default')
                .html('<i class="fa fa-save"></i> save');
        });
    };

    $scope.printNotes = function() {
        $window.open(drupal.settings.basePath + 'my-notes/print-chapter/' + $scope.topicModel.chapter_id, '_blank');
    };
});

NotesModule.directive("rcparnotes", function($timeout, $window, $log, textAngularManager) {

    var drupal = Drupal;

    return {
        templateUrl: drupal.courseware.path + 'views/template/notes.html',
        restrict: 'E',
        scope: true,
        replace: true,
        controller: "RCPARNotesController",
        compile: function compile(element, attrs) {
            return {
                pre: function ($scope, element, iAttrs, controller) {
                    // $scope.notepromise = controller.getNotes();
                },
                post: function  ($scope, $elem, attrs) {
                    $scope.saveButton = $elem.find('button[name="save"]');
                    $scope.deleteButton = $elem.find('button[name="delete"]');

                    // attach a blur event to the textAngular input for new notes
                    var editorScope = textAngularManager.retrieveEditor('newnoteinput').scope;
                    editorScope.displayElements.text.on('blur', function(evt) {
                        // checking blur listener, so we avoid saving a 2nd time
                        if ($scope.blurListenerEnabled) {
                            $scope.saveNewNote();
                        }
                    });
                }
            };
        }
    };
});

NotesModule.directive("note", function($http, $timeout, $document, services, textAngularManager, $log) {

    var drupal = Drupal;

    return {
        templateUrl: drupal.courseware.path + 'views/template/note.html',
        restrict: 'E',
        scope: {model:'='},
        replace: true,
        link: function($scope, $element, $attr) {

            $scope.model = $scope.$parent.$eval($attr.model);
            $scope.model.editMode = false;

            $scope.editNote = function(model) {
                model.editMode = true;
                $scope.alerts = [];

                if(!$scope.eventSetup) {
                    $scope.eventSetup = true;
                    var editorScope = textAngularManager.retrieveEditor('note_' + $scope.model.id).scope;
                    // watch the editor for changes
                    $scope.$watch("model.note", function(n, o) {
                        if(n && n !== o) {
                            $scope.model.isSaved = false;
                        }
                    });

                    // on blur of a note thats being edited (not a new / creating note)
                    editorScope.displayElements.text.on('blur', function(evt) {
                        if (!$scope.model.isSaved) {
                            // call to just save on blur
                            $scope.saveNote();

                            // UI to show the 'Not saved'
                            // $scope.addAlert({
                            //     msg: "You have not saved your changes. <a class='alert-link savenote'>Save now</a>",
                            //     id: "unsaved-warning"
                            // });
                            // $timeout(function() {
                            //     angular.element("#note_" + $scope.model.id + " .alert-link.savenote").on('click', function(e) {
                            //         alert('click');
                            //         e.preventDefault();
                            //         e.stopImmediatePropagation();
                            //         e.stopPropagation();
                            //         $scope.saveNote();
                            //     });
                            // });
                        }
                    });
                }
            };

            $scope.alerts = [];

            $scope.addAlert = function(alert) {
                var len = $scope.alerts.length;

                while(len--) {
                    var a = $scope.alerts[len];
                    if (a.id == alert.id) {
                        // pulse the alert, i guess
                        return;
                    }
                }

                $scope.alerts.push(alert);
            };

            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.closeAlertByID = function(id) {
                var len = $scope.alerts.length;
                while(len--) {
                    var a = $scope.alerts[len];
                    if(a.id == id) {
                        $scope.alerts.splice(len, 1);
                        len = $scope.alerts.length;
                    }
                }
            };

            $scope.deleteNote = function() {
                $scope.$parent.$parent.deleteNote($scope.model.id);
            };

            $scope.$on("on-toolbar-save", function(event) {
                event.preventDefault();
                $scope.saveNote();
            });

            $scope.$on("on-toolbar-delete", function(event) {
                event.preventDefault();
                $scope.$parent.$parent.deleteNote($scope.model.id);
            });

            $scope.saveButton = $element.find('button[name="save"]');

            $scope.saveNote = function() {
                $scope.model.editMode = false;
                $scope.saveButton.removeClass('btn-default').addClass('btn-success').html('<i class="fa fa-gear fa-spin"></i> Saving...');
                $scope.$parent.$parent.saveNote($scope.model).success(function(data) {

                    $scope.model.editMode = false;
                    $scope.model.isSaved = true;
                    $scope.closeAlertByID("unsaved-warning");
                    $scope.addAlert({msg: "Note has been saved.", type:"success", id:"saved"});
                    var editorScope = textAngularManager.retrieveEditor('note_' + $scope.model.id).scope;

                    $timeout(function(){
                        $scope.saveButton.removeClass('btn-success').addClass('btn-default').html('<i class="fa fa-save"></i> save');
                        editorScope.displayElements.text.trigger('blur');
                        $element.find(".focussed").removeClass("focussed");
                    }, 222);

                    $timeout(function() {
                        $scope.closeAlertByID("saved");
                    }, 5500);
                });
            };
        }
    };
});
