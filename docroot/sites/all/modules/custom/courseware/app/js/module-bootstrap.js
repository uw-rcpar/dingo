angular.module( 'ui.rcpar.bootstrap', ['ngAria', 'ui.bootstrap', 'rcpar.urls', 'ui.rcpar.userprefs', 'template/modal/modal-confirm.html'])

.directive('dir', function($compile, $parse) {
    return {
        restrict: 'EA',
        link: function(scope, element, attr) {
            scope.$watch(attr.content, function() {
                element.html($parse(attr.content)(scope));
                $compile(element.contents())(scope);
            }, true);
        }
    }
})

.factory('bsCurrentMediaQuery', function($window, $log) {

    // these should be in sync with the bootstrap media queries
    var media = {
        sm: 768,
        md: 992,
        lg: 1200
    }

    return function() {

        // @NOTE: this is accurate until there is a scroll bar on the window
        // once theres a scroll bar, this shifts a bit, however it depends on
        // the browser and it's UI I believe.
        var windowWidth = parseInt(jQuery("body").css("width"));

        // default to 'xs' but set to larger sizes, this falls in line with
        // the the methods used by twitter bootstrap, assume small screens
        // but detect larger
        var mediaQuery = "xs";

        if (windowWidth >= media.sm) {
            mediaQuery = 'sm';
        }
        if (windowWidth >= media.md) {
            mediaQuery = 'md';
        }
        if (windowWidth >= media.lg) {
            mediaQuery = 'lg';
        }

//        $log.debug("Screen size is ", mediaQuery);

        return mediaQuery
    }
});
