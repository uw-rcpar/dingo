
angular.module("CourseModule", ['ui.rcpar.video', 'ui.rcpar.book', 'ui.rcpar.notes', 'ui.rcpar.bootstrap'])

.config(function ($locationProvider, localStorageServiceProvider, $logProvider) {
    $logProvider.debugEnabled(true);
    localStorageServiceProvider.setPrefix("rcpa_courseware");
    localStorageServiceProvider.setStorageCookie(0, "/datastore");
})

.run(function ($rootScope, $window, services,  $timeout, $log) {

    // tell module-urls to use drupal versions of the urls
    services.config('drupal');

    $rootScope.isFullscreen = false;
    $rootScope.isSaving = false;
    $rootScope.isIPhone = false;
    $rootScope.isIPad = false;

    try {
        if (navigator.userAgent.match(/(iPhone|iPod touch);/i)) {
            $rootScope.isIPhone = true;
        }
        if (navigator.userAgent.match(/(iPad);/i)) {
            $rootScope.isIPad = true;
        }
    } catch (err) {
    }
})

.controller("StudyController", function ($scope, $rootScope, $http, $timeout, $q, services, ContentModel, Userprefs, authentication, $log) {

    // var drupal = Drupal,
    //     topicModel = drupal.courseware.topicModel;

    ContentModel.init();

    // $scope.auth = authentication;
    $scope.loadError = false;
    $scope.prefs = Userprefs;
    $scope.currentYear = Drupal.courseware.courseYear;

	$scope.switchYear = function(year) {
		$rootScope.$broadcast("switchYear", year);
		$scope.currentYear = parseInt(year);
	}

	$scope.isYearVisible = function(year) {
		return (Drupal.courseware.vAccess.indexOf(year) > -1);
	};

	    $scope.getCurrentCourseYear = function() {
		return $scope.currentYear;
	    };

    $scope.toggleAutoplay = function () {
        $scope.prefs.prefs.autoplay = ! $scope.prefs.prefs.autoplay;
        $scope.prefs.save();
    };
})

// this is the wrapper div for the 2 columns, a smarter / resizable 2 column layout
.directive('rcparResize', function ($window, $document, $timeout, $rootScope, Userprefs, $log, bsCurrentMediaQuery) {
    return {
        restrict: 'EA',
        scope: true,
        replace: true,
        link: function ($scope, $element, $attrs) {
            $scope.isResizeInit = false;
            $scope.windowEl = angular.element($window);

            $scope.suppressResize = (!$rootScope.isIPhone);

            $scope.doLayout = function(e) {
                if ($scope.prefSize.size == "desktop") {
                    $scope.leftWidth = $scope.leftpane[0].offsetWidth;
                    $scope.newRightWidth = $scope.containWidth - $scope.leftWidth - $scope.gutter;
                    $scope.rightpane.css({width: $scope.newRightWidth + 'px'});
                } else {
                    $scope.rightpane.css('width', '');
                    $scope.leftpane.css('width', '');
                }
            };

            $scope.resizer = null;

            $scope.$on("$destroy", function() {
                $scope.destroyResizer();
            });

            $rootScope.$on("player-ready", function(evt, datas) {
                if (!$rootScope.isIPhone) {

                    $scope.initResizer();

                    $scope.windowEl.on('resize', function() {

                        var screenSize = bsCurrentMediaQuery();
                        var bookColEl = jQuery('#rightpane');
                        var bookColContainerEl = jQuery('#rightpaneBookContainer');

                        if (screenSize == 'xs' || screenSize == 'sm') {
                            bookColEl.css('height', '800px');
                        } else {
                            bookColEl.css('height', '');
                        }

                        $scope.containWidth = jQuery('body')[0].offsetWidth;
                        $scope.prefSize = Userprefs.getResize();

                        if ($scope.prefSize.size == "desktop" ) {
                            $scope.maxWidth = $scope.containWidth - 398;
                            $scope.minWidth = 280;
                            $scope.resizer.css({"display":"block"});

                            var lwidth = $scope.leftpane[0].offsetWidth;

                            if (lwidth < $scope.minWidth ) {
                                $scope.leftpane.css({width: $scope.minWidth + "px"});
                            } else if (lwidth > $scope.maxWidth) {
                                $scope.leftpane.css({width: ($scope.maxWidth - 1) + "px"});
                            }

                            $scope.leftpane.resizable("option", "maxWidth", $scope.maxWidth);
                            $scope.leftpane.resizable("option", "minWidth", $scope.minWidth);
                        } else {
                            $scope.resizer.css({"display":"none"});
                        }

                        $scope.doLayout();
                    });
                }

            });

            $scope.initResizer = function() {
                $element.addClass("ui-widget-content");
                $scope.leftpane = jQuery("#leftpane");
                $scope.rightpane = jQuery("#rightpane");
                $scope.containWidth = jQuery('body').width();
                $scope.leftWidth = $scope.leftpane.width();
                $scope.rightWidth = $scope.rightpane.width();
                $scope.gutter = $scope.containWidth - $scope.leftWidth - $scope.rightWidth;
                $scope.newRightWidth = $scope.containWidth - $scope.leftWidth - $scope.gutter;

                var setWidthClass = function() {
                    // add 'vjs-sm' class so we can use CSS to alter the look / feel of the videp player pane
                    if ($scope.leftpane.width() < 710) {
                        $scope.leftpane.addClass('vjs-sm');
                    } else {
                        $scope.leftpane.removeClass('vjs-sm');
                    }
                };

                $scope.minWidth = 280;
                $scope.maxWidth = $scope.containWidth - 398;
                $timeout(function() {
                    $scope.prefSize = Userprefs.getResize();
                    $scope.leftpane.resizable({
                        autoHide: false
                        , distance: 10
                        , handles: "e"
                        , helper: "resizable-helper"
                        , ghost: true
                        , minWidth:$scope.minWidth
                        , maxWidth: $scope.maxWidth
                        // ,alsoResize:rightpane
                        , create: function (event, ui) {
                            $scope.isResizeInit = true;
                            $scope.resizer = jQuery(".ui-resizable-handle.ui-resizable-e");
                            $scope.gutter = 2;

                            if ($scope.prefSize != null) {
                                if ($scope.prefSize.size != "desktop") {
                                    $scope.resizer.hide();
                                } else if (!isNaN($scope.prefSize.val)) {

                                    $scope.leftpane.css({width: $scope.prefSize.val + 'px'});
                                    $scope.doLayout();
                                }
                            }

                            setWidthClass();
                        }
                        , stop: function (event, ui) {
                            $scope.doLayout();
                            setWidthClass();
                            Userprefs.setResize($scope.leftWidth);
                        }

                    });
                });
            };

            $scope.destroyResizer = function() {
                if( $scope.isResizeInit ) {
                    $scope.leftpane.resizable("destroy");
                }
                $element.removeClass("ui-widget-content");

                $scope.isResizeInit = false;
            };
        }
    }; // end return rcparResize
})

.directive('rcpaFixedCenter', function ($window) {
    return {
        restrict: "C",
        link: function ($scope, $elem, $attr) {
            $scope.fixSize = function () {
                var wHeight = $window.innerHeight;
                var bHeight = $elem.height();
                $elem.css({"margin-top": ((wHeight - bHeight) / 2) + 'px'});
            };
            var win = angular.element($window);
            win.bind('resize', function (e) {
                $scope.fixSize();
            });
            $scope.fixSize();
        }
    };
})

.directive('scaleToFit', function($window, $rootScope, $timeout) {
    return {
        restrict: "A",
        scope:false,
        link: function ($scope, $elem, $attr) {
            if (!$rootScope.isIPhone) {
                this.resize = function () {
                    $timeout(function () {
                        $elem.css("transform", "");

                        var elem = $elem[0];
                        var par = $elem.parent()[0];
                        var eWidth = jQuery($elem).width();
                        var pWidth = jQuery(par).width();

                        // var pos = $elem.offset();
                        if (eWidth > pWidth) {
                            $elem.css("transform", "scale(" + (pWidth / eWidth) + ")");

                        }
                    });
                };
                angular.element($window).on('resize', this.resize);
                $rootScope.$on("pane-width-change", this.resize);
            }
        }
    };
})
;
