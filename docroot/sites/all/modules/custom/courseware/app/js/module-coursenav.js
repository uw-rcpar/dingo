// Services: ContentModel
// Directives: a, progressCircle
// Factories: authentication

angular.module("ui.rcpar.coursenav", ["rcpar.urls",  'ui.rcpar.userprefs', 'ui.rcpar.bootstrap'])

.service("ContentModel", function($http, $q, $window, $templateCache, services, $log) {

    var drupal = Drupal;

    var self = this;

    this.init = function() {
        self.currentTopicModel = $q.defer();
        self.currentTopic = null;
    };

    // the nid of the current topic
    self.currentTopic = drupal.courseware.topicId;
    self.currentTopicModel = $q.defer();

    // fetch the Topic ID of the current topic
    this.getTopicModel = function() {
        // start to load the data from drupals urls
        self.currentTopic = drupal.courseware.topicId;
        self.loadTopicModel();

        // return the promis of a model, resolved later in the loadTopicModel function
        return self.currentTopicModel.promise;
    };

    // topic model is now pre-loaded in the Drupal namespace
    this.loadTopicModel = function() {
        self.currentTopicModel.resolve(drupal.courseware.topicModel);
    };
})

.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#' || String(attrs.href).indexOf('collapse_') != -1){
                elem.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
})

.directive('progressCircle', function($timeout) {
    return {
        restrict: 'A',
        scope: false,
        link: function($scope, $elem, $attr) {
            $timeout(function () {
                var val = parseInt($attr.pct);
                var $circle = $elem.find('.svg-bar');
                if (isNaN(val)) {
                    val = 0;
                } else {
                    var r = $circle.attr('r');
                    var c = Math.PI * (r * 2);
                    if (val < 0) { val = 0; }
                    if (val > 100) { val = 100; }
                    var pct = ((100 - val) / 100) * c;
                    $circle.css({strokeDasharray:c, strokeDashoffset: pct});
                }
            });
        }
    };
})

/**
 * This used to do login / logout...
 * Now, it's just a overloaded way of getting Drupal.courseware.uid
 *
 * It may be a good place however to add some angular / drupal helpers.
 */
.factory("authentication", function() {
    var drupal = Drupal,
        userInfo = {
            'uid': drupal.courseware.uid
        };

    function getUserInfo() {
        return userInfo;
    }

    return {
        getUserInfo: getUserInfo
    };
});
