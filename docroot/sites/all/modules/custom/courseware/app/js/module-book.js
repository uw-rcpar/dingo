// Directives: book

var BookModule = angular.module("ui.rcpar.book", ['ui.rcpar.coursenav', 'ui.rcpar.highlighter']);

BookModule.directive("book", function(ContentModel, $rootScope, $http, $window, $document, $timeout, $sce, authentication, services, $log, Highlighter) {

    var drupal = Drupal;

    return {
        templateUrl: drupal.courseware.path + 'views/pdfview-toolbar.html',
        restrict: "E",
        priority: 8,
        scope: true,
        replace: true,
        link: function($scope, $elem, $attr) {

            // use a patched version of the PDFJS viewer
            // main use of the patch is to wrap the object in
            // a function to lock the scope
            $scope.PDFView = window.pdfjsload(PDFJS);
            $scope.highlight_initialized = false;
            $scope.isWorking = false;
            $scope.loadingMessage = "";
            $scope.mode = 'pdf';
            $scope.contentLoaded = false;
            $scope.bookFrame = angular.element("#book-iframe");
            $scope.pdfDoc = null;
            $scope.highlightmode = false;
	    $scope.pdfInitialized = false;
	    if (window.location.host !== 'localhost') {
		var domain = window.location.protocol + "//" + window.location.host;
	    }
	    else {
		var domain = "localhost";
	    }
	
	    $scope.$on('switchYear', function(evt, year) {
		document.cookie = 'course_year=' + year + '; path=/';
		jQuery('.pdf-row').removeClass('highlights-visible');
		jQuery('.pdf-loading').addClass('visible');
		$scope.initModel(year);
	        $scope.currentYear = parseInt(year);
	    });

	    $scope.initModel = function(year) {
		    ContentModel.getTopicModel().then(function(topic_data) {
            $scope.content = topic_data.content;
            var len = $scope.content.pages.length;
            for (var i = 0; i < len; i++) {
                var pages = $scope.content.pages[i];
                if (pages.type == $scope.mode) {
                    if (year != '') {
                        $scope.bookUrl = pages['url_' + year];
                    }
                    $scope.initBook();
                }
            }
		    });
	    }

                    $scope.onPagesLoaded = function(evt) {
                        var pageNum = evt.originalEvent.detail.pageNum,
                            pageIndex = pageNum - 1,
                            pageEl = jQuery('#viewer .page').get(pageIndex);

                        Highlighter.initPage(pageIndex);

                        // load the first page of highlights after init
                        if (!$scope.firstPageLoaded) {
                            $scope.firstPageLoaded = true;
                            // Highlighter.setEnabled(false);
                            $scope.loadHighlights();
                        }
                    };

                    $document.bind('rcpa-pdf-page-rendered', $scope.onPagesLoaded);
                    // do stuff when the Highlighter says hightlights were updated
                    $scope.$on(Highlighter.UPDATED, function(evt, args) {
//		        jQuery('.pdf-row').addClass('highlights-visible');
                        $scope.saveHighlights();
                    });
                    // trigger highlighter scale change from the PDFView event scalechange
                    $window.addEventListener('scalechange', function(evt) {
                        Highlighter.setScale($scope.PDFView.currentScale);
                    });

	jQuery('.pdf-loading').addClass('visible');
	$scope.currentYear = parseInt(Drupal.courseware.courseYear);
	$scope.initModel($scope.currentYear);


            $scope.initBook = function() {
                $scope.isDirty = false;
                $scope.status = {};
                $scope.status.isOpen = false;
                $scope.status.isWorking = false;
                $scope.statusloadingMessage = "";
                $scope.highligher = null;
                $scope.highlights = {};

                $scope.firstPageLoaded = false;

                $scope.status.mode = 'pdf';
                $scope.status.contentLoaded = false;

                $scope.toggleHighlight = function() {
                    $scope.highlightmode = Highlighter.toggleEnabled();
                };

                $scope.pdfViewerInit = function() {
                    $scope.status.isWorking = true;
                    $scope.status.loadingMessage = "Loading book...";

                    $scope.PDFView.DEFAULT_FILE = $scope.bookUrl;

//		    if (!$scope.pdfInitialized) {
                    	$scope.PDFView.initialize().then($scope.PDFView.webViewerInitialized);
//			$scope.PDFView.uninitialize();
//			$scope.pdfInitialized = true;
//		    }

                };

                $scope.loadHighlights = function() {
                    var topicId = drupal.courseware.topicId;

                    $scope.status.isWorking = true;
                    $scope.status.loadingMessage = "Loading highlights...";
                    $http({
                        url: services.highlights_get + topicId + '/' + $scope.currentYear,
                        method: "GET"
                    }).then(function(response) {
                        $scope.highlights = response.data.data.highlights;
                        Highlighter.set($scope.highlights);
			jQuery('.pdf-loading').removeClass('visible');
		        jQuery('.pdf-row').addClass('highlights-visible');
//		        jQuery('.highlight_box').addClass('visible');
                        $scope.status.isWorking = false;
                    });
                };

                $scope.saveHighlights = function() {
                    var topicId = drupal.courseware.topicId;

                    $scope.status.isWorking = true;
                    $scope.status.loadingMessage = "Saving...";

                    $scope.highlights = Highlighter.get();

                    $http({
                        url: services.highlights_post + '/' + $scope.currentYear,
                        method: "POST",
                        data: {
                            'topic': topicId,
                            'highlights': $scope.highlights,
                            'entitlement_sku': drupal.courseware.entitlementSku
                        }
                    }).then(function(response) {
//		        jQuery('.pdf-row').addClass('highlights-visible');
                        $scope.status.isWorking = false;
                    });
                };

                $window.onbeforeunload = function (event) {
                    if ($scope.isDirty) {
                        $scope.saveHighlights();
                        var message = 'Please wait while we save your session data...';
                        if (typeof event == 'undefined') {
                            event = window.event;
                        }
                        if (event) {
                            event.returnValue = message;
                        }
                        return message;
                    }
                };

                // kickstart the pdf view window
                $scope.pdfViewerInit();
            };
        }
    };
});
