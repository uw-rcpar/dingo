Dev Deps
========

## Node.js

Bower

    $ npm install -g bower

- bower_components/
- bower.json

## Ruby

Bundler

    $ gem install bundler --no-ri --no-rdoc
    $ bundle install

Compass (part of bundle install)

- config.rb

