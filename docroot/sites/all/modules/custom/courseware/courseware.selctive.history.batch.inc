<?php

/**
 * Menu callback for
 */
function courseware_selective_history_batch_form($form, $form_state) {
  // Instructions
  $form['instructions'] = array(
    '#markup' => '<p>This form will selectively reset video history records.</p>'
  );

  // Active entitlements
  $form['active_entitlements'] = array(
    '#title' => 'Active entitlements only',
    '#description' => 'This operation will affect users with active entitlements only. This option cannot currently be changed.',
    '#type' => 'checkbox',
    '#default_value' => TRUE,
    '#disabled' => TRUE,
  );

  // Topic IDs
  $form['topic_ids'] = array(
    '#title' => 'Topic IDs to reset.',
    '#description' => 'Enter one ID per line. Only videos for the specified topics will be reset',
    '#type' => 'textarea',
  );

  // Exclude videos changed after
  $form['exclude_changed_after_date'] = array(
    '#title' => t('Exclude videos changed after'),
    '#description' => 'History records that have been updated after the specified date will not be changed. If empty,
      records will be updated regardless of when they were last watched.',
    '#type' => 'date_popup',
    '#date_type' => DATE_UNIX,
    '#date_format' => 'm/d/Y',
  );

  // Include ACT
  $form['include_act'] = array(
    '#title' => 'Include ACT',
    '#description' => 'It may be desirable to preserve ACT video history so professors have a historical record of student activities.
      Uncheck this box to preserve video records from ACT users.',
    '#type' => 'checkbox',
  );

  // VH join option
  $form['vh_join'] = array(
    '#title' => 'Join on history table',
    '#description' => 'Checking this will join on the eck_video_history table when initializing the job. This will make
      less operations overall, but it is much slower when starting up. If you have problems with timeouts when starting
      the batch, it is safe to disable this option.',
    '#default_value' => TRUE,
    '#type' => 'checkbox',
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Reset progress'),
  );

  return $form;
}

/**
 * Set a batch to selectively update user's video history
 */
function courseware_selective_history_batch_form_submit($form, $form_state) {
  set_time_limit(0);
  $operations = array();

  // Get topic IDs and other options from form values
  $topic_ids =  array_filter(array_map('trim', explode("\n", $form_state['values']['topic_ids'])));
  $include_act = $form_state['values']['include_act'];
  $exclude_after_date = !empty($form_state['values']['exclude_changed_after_date']) ? strtotime($form_state['values']['exclude_changed_after_date']) : 0;

  if($form_state['values']['vh_join']) {
    // Get a list of users with active, standard course entitlements
    $result = db_query("
      SELECT DISTINCT n.uid from node n
      INNER JOIN field_data_field_expiration_date fe on fe.entity_id=n.nid and fe.`field_expiration_date_value` > UNIX_TIMESTAMP()
      INNER JOIN field_data_field_course_type_ref t on t.entity_id=n.nid and t.field_course_type_ref_tid = 1464
      INNER JOIN eck_video_history vh ON vh.uid = n.uid
      WHERE vh.topic_reference IN (:topics)
    ",[':topics' => $topic_ids]);
  }
  else {
    // Same as above but without the join. Faster, but will return more superfluous results.
    $result = db_query("
      SELECT DISTINCT n.uid from node n
      INNER JOIN field_data_field_expiration_date fe on fe.entity_id=n.nid and fe.`field_expiration_date_value` > UNIX_TIMESTAMP()
      INNER JOIN field_data_field_course_type_ref t on t.entity_id=n.nid and t.field_course_type_ref_tid = 1464
    ");
  }

  // Add one batch operation per user
  foreach ($result as $key => $item) {
    $operations[] = array('courseware_selective_history_reset', array($item->uid, $topic_ids, $include_act, $exclude_after_date));
  }

  // Set up the batch
  $batch = array(
    'operations' => $operations,
    'finished' => 'courseware_selective_history_finish',
    'title' => t('Selectively reset video history'),
    'init_message' => t('Initializing update process.'),
    'progress_message' => t("Processed @current out of @total users."),
    'error_message' => t('Error'),
    'file' => drupal_get_path('module', 'courseware') . '/courseware.selctive.history.batch.inc',
  );
  batch_set($batch);
}

/**
 * Selectively reset video progress
 * @param int $uid
 * - The user whose progress to reset
 * @param int[]
 * - An array of topic IDs that we'll be resetting
 * @param bool $include_act
 * - If false, we will not affect history data from ACT entitlements
 * @param int $exclude_after_date
 * - A UNIX timestamp. If not 0, records changed after this date will not be reset.
 */
function courseware_selective_history_reset($uid, $topic_ids, $include_act, $exclude_after_date, &$context) {
  // Begin a query to get video history records for this user
  $query = db_select('eck_video_history', 'vh');
  $query->fields('vh', array('id', 'changed', 'last_position', 'topic_reference', 'entitlement_product'))
        ->condition('uid', $uid)
        ->condition('topic_reference', $topic_ids, 'IN');

  // If this option is in use, we won't update a record if its been changed after the exclusion date
  if($exclude_after_date > 0) {
    $query->condition('changed', $exclude_after_date, '<');
  }

  $result = $query->execute()->fetchAllAssoc('id');


  $ids_to_reset = array();
  $userEntsCache = new \RCPAR\Entities\UserEntitlementCollection();

  foreach ($result as $record) {
    try {
      // If the record is already set to 0, there's no need to do anything
      if ($record->last_position == 0) {
        continue;
      }

      // Load and cache this entitlement unless we have it cached already
      $eid = $record->entitlement_product;
      if (!$userEntsCache->getEntitlementById($eid)) {
        $e = new \RCPAR\Entities\UserEntitlement($eid);
        $userEntsCache->addEntitlement($e);
      }

      // Make sure the entitlement for this record is active
      if (!$userEntsCache->getEntitlementById($eid)->isActive()) {
        continue;
      }

      // If we're not including act, we need to verify that this record doesn't come from an ACT entitlement
      if (!$include_act) {
        // Skip this record if it's from an ACT entitlement
        if ($userEntsCache->getEntitlementById($eid)->isOnlineCourseACT()) {
          continue;
        }
      }

      // If we made it here, we need to reset the progress
      $ids_to_reset[] = $record->id;
    }
    catch (Exception $e) {
      continue;
    }
  }

  // Execute a single update query for all records that we plan to update
  if(!empty($ids_to_reset)) {
    db_update('eck_video_history')
      ->fields(array('last_position' => 0, 'percentage_completed' => 0, 'changed' => REQUEST_TIME))
      ->condition('id', $ids_to_reset, 'IN')
      ->execute();

    // Log these IDs in a file for tracking
    $fp = fopen('public://selective-vh-reset.log', 'a');
    $output = "UID: $uid\nVH IDS:\n" . implode(",", $ids_to_reset) . "\n\n";
    fputs($fp, $output);
    fclose($fp);
  }
}

/**
 * Batch completion callback
 * @param bool $success
 * - False if one of the operations failed
 * @param array $results
 * - has a insert array of values to create on the table stats
 * @param array $operations
 *
 */
function courseware_selective_history_finish($success, $results, $operations) {
  if ($success) {
    drupal_set_message("Progress reset completed successfully.");
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(
      t('An error occurred while processing @operation with arguments : @args', array(
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE)
        )
      ), 'error'
    );
  }
}
