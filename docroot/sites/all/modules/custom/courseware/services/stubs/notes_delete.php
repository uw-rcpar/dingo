<?php

$params = Array();

$context =  stream_context_create(
    array('http' => array('method' => 'DELETE'))
);

parse_str(file_get_contents('php://input', true, $context), $params);

header('Content-Type: application/json; charset=utf-8');
echo '{
    "success":"true",
    "id":"'.$params['id'].'"
}';
