<?php
error_reporting(-1);
ini_set('display_errors', 'On');

$bod  = file_get_contents("php://input");
if($bod) {
	$bod = json_decode($bod, true);
}
header('Content-Type: application/json; charset=utf-8');
echo '{
	"success":"true",
	"note": {"id":"40007",
			"note":"'.$bod['note'].'",
			"topic":"10001"
		}
}';