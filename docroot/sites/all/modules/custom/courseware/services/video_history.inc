<?php

/**
 * Menu callback for /courseware-services/videohistory
 * This callback is invoked by the courseware javascript in order to update a user's video history
 * progress.
 *
 * @return NULL
 * - No return value. This function will output JSON.
 */
function courseware_service_vh() {
  switch (strtolower($_SERVER['REQUEST_METHOD'])) {
    case 'post':
      $payload = json_decode(file_get_contents('php://input'), TRUE);
      $year = exam_version_get_tid_from_year_name(trim($payload['exam_version']));
      $video_history = _courseware_service_vh_save($payload, $year);
      break;
    default:
      drupal_add_http_header('Status', '501 Not Implemented');
      return '';
  }

  // print out JSON to the browser
  drupal_add_http_header('Content-Type', 'application/json');
  print json_encode($video_history);
}

/**
 * Create or Update a video_history node
 *
 * @param array $payload
 * - An array typically from a javascript request sending a JSON payload. Example:
 * array(
    'userid' => 1,
    'topic' => 4064,
    'video' => 4685,
    'time' => 34989, // In milliseconds (I think -jd)
    'duration' => 'Irrelevant, populated from video node',
    'percentviewed' => 42,
    'entitlement_sku' => 'FAR',
    'entitlement_id' => 12345, // Optional, will be respected if present or derived if not
   )
 *
 * @param int $exam_version
 * - Exam version term id
 *
 * @return array
 * - An array intended to be delivered to the client side via JSON. Has key 'status' on success with
 * 'message' and 'video_history_nid' keys inside. On failure, on array with key 'message' that contains
 * an error message.
 */
function _courseware_service_vh_save($payload, $exam_version = null) {
  try {
    $response = array();
    $uid = $payload['userid'];
    $user = user_load($uid);
    $topic = node_load($payload['topic']);
    $video = node_load($payload['video']);
    $entitlement_sku = $payload['entitlement_sku'];

    // Need a video and a user
    if (!$video || !$user) {
      throw new Exception('Video or user information missing');
    }

    // If not provided in the payload, determine the ID of the user's entitlement that matches this course SKU
    $entitlement_id = FALSE;
    if(!empty($payload['entitlement_id'])) {
      $entitlement_id = $payload['entitlement_id'];
    }
    // Look for an entitlement ID by querying for active entitlements
    else {
      $entitlements = user_entitlements_get_users_products($user);
      if (isset($entitlements['products']) && !empty($entitlements['products']) && isset($entitlements['products'][$entitlement_sku])) {
        $entitlement_id = $entitlements['products'][$entitlement_sku]['nid'];
      }
    }

    // Can't proceed without an entitlement
    if (!$entitlement_id) {
      throw new Exception('Entitlement ID missing');
    }

    // Set video related data
    $vh_entity = courseware_load_or_create_video_history($user->uid, $video->nid, $topic->nid, $entitlement_id);
    $video_wrapper = entity_metadata_wrapper('node', $video);
    $video_history_wrapper = entity_metadata_wrapper('video_history', $vh_entity);
    $video_history_wrapper->video_history_content->set(json_encode($payload));
    $video_history_wrapper->rcpa_video->set($video->nid);
    $video_history_wrapper->last_position->set(intval($payload['time']));
    $video_history_wrapper->total_duration->set($video_wrapper->field_video_duration->raw());
    $video_history_wrapper->percentage_completed->set(intval($payload['percentviewed']));

    // Set entitlement reference
    $video_history_wrapper->entitlement_product->set($entitlement_id);

    // Set title and section/chapter/topic info
    $topic_wrapper = entity_metadata_wrapper('node', $topic);
    $type = $topic_wrapper->field_course_type_ref->value();
    $section = $topic_wrapper->field_course_section->value();
    $chapter_id = courseware_service_chapter_get($topic->nid, $exam_version);
    $video_history_wrapper->title->set("{$user->uid} - {$topic_wrapper->getIdentifier()} Video History");
    $video_history_wrapper->section->set($section->name);
    $video_history_wrapper->course_type->set($type->name);
    $video_history_wrapper->chapter_reference->set($chapter_id);
    $video_history_wrapper->topic_reference->set($topic->nid);
    $video_history_wrapper->save();

    module_invoke_all('courseware_video_history_saved', $user);
    
    // Response for JSON
    $response['status'] = array(
      'message' => t('Video History created successfully'),
      'video_history_nid' => $video_history_wrapper->getIdentifier()
    );
  }
  catch (Exception $exc) {
    $response['message'] = "Error on creating / updating  the video history";
    watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
  }

  return $response;
}

function _courseware_service_vh_get($vh_id = null, $json = true) {
  global $user;
  $retuned_vh = array();
  $vhid = is_null($vh_id) ? arg(2) : $vh_id;
  $video_history = node_load($vhid);
  if ((in_array('administrator', $user->roles) || $video_history->uid == $user->uid) && !is_null($video_history)) {
    try {
      $video_history_wrapper = entity_metadata_wrapper('video_history', $video_history);
      $retuned_vh = array(
          'id' => $video_history->nid,
          'title' => $video_history_wrapper->label(),
          'date_created' => $video_history->created,
          'date_modified' => $video_history->changed,
          'topic_reference' => $video_history_wrapper->topic_reference->value(),
          'video_reference' => $video_history_wrapper->rcpa_video->value(),
      );
    } catch (Exception $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $exc->getMessage());
    }
  }

  if ($json) {
    echo json_encode($retuned_vh);
    exit;
  }
  return $retuned_vh;
}

/**
 * gets the chapter id based on its topic, reverse enginering cause not all topics
 * has the video chapter filled in.
 * @param int $topic_id
 * @param int $year
 * - Exam version term id
 * @return int chapter id
 */
function courseware_service_chapter_get($topic_id, $year = NULL) {

    //If year is null get the default exam version
    if (!$year) {
        $evs = exam_versions_years();
        $year = array_pop($evs);
    }

    $query = db_select('field_data_field_topic_per_exam_version', 'tr');
    $query->join('field_data_field_topic_reference', 'f', "f.entity_id=tr.field_topic_per_exam_version_target_id and f.bundle='topic_ref_by_exam_version'");
    $query->join('field_data_field_exam_version_single_val', 'y', "y.entity_id = f.entity_id");
    $query->fields('tr', array('entity_id'))
        ->condition('tr.bundle','rcpa_chapter')
        ->condition('f.field_topic_reference_target_id',$topic_id)
        ->condition('y.field_exam_version_single_val_tid',$year)
        ->range(0, 1);

    $result = $query->execute()->fetchObject();

    return is_object($result) ? $result->entity_id : "";
}