<?php

function _courseware_get_videos($videos = null, $course_type, $section, $topic_slug) {
    global $user;

    $videos_data = array();
        if (is_null($videos)) {
        return $videos_data;
    }

    foreach ($videos as $weight => $video) {
        try {
            $video_wrapper = entity_metadata_wrapper('node', $video);

            // @TODO: This really could be a function
            // e.g. user_entitlement_get_entitlement($user, $entitlement_sku)
            $topic_id = $video_wrapper->field_topic_id->value();
            $entitlement_sku = $section; // section in this case is the sku, e.g. 'BEC'
            $entitlement_nid = NULL;
            if (isset($user->user_entitlements['products'][$entitlement_sku]) && !empty($user->user_entitlements['products'][$entitlement_sku])) {
              $entitlement_nid = $user->user_entitlements['products'][$entitlement_sku]['nid'];
            } else {
              $nodes = user_entitlements_get_entitlements($user);
              $products = user_entitlements_get_entitlements_skus_and_id($nodes);
              $entitlement_nid = isset($products[$entitlement_sku]) ? $products[$entitlement_sku] : NULL;
            }

            $last_position = 0;
            if(!is_null($entitlement_nid)){
              $video_history = courseware_load_or_create_video_history($user->uid, $video->nid, $topic_id, $entitlement_nid);
              $video_history_wrapper = entity_metadata_wrapper('video_history', $video_history);
              $last_position =  $video_history_wrapper->last_position->value();
            }

            $course_type = ($video_wrapper->field_course_type_ref->value()->name == "Online Cram Course") ? 'cram_course': 'regular_course';
            $section = $video_wrapper->field_course_section->value()->name;
            
            $videos_data[] = array(
                'id' =>  $video_wrapper->getIdentifier(),
                'urls' => _courseware_videos_urls($course_type, $section, $video_wrapper->field_video_low_resolution->value(), $video_wrapper->field_video_medium_resolution->value(), $video_wrapper->field_video_high_resolution->value(), $topic_slug),
                'length' => $video_wrapper->field_video_duration->value(),
                'tracks' => _courseware_get_video_tracks($video, $course_type, $section, $topic_slug),
                'timeviewed' => $last_position,
            );
        } catch (EntityMetadataWrapperException $exc) {
            watchdog('courseware', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . $exc->getMessage());
        }
    }

    return $videos_data;
}

/**
 * Build data required to list array of videos to pass to course player
 *
 * @param array $videos The list of video nodes from the topic
 * @param string $full_cdn_url The base path to the Amazon s# bucket or other url to host videos from
 *
 * @return array An array that can be json encoded and sent to the courseware player
 */
function _courseware_get_video_tracks($video, $course_type, $section, $topic_slug) {

    $tracks = array();

    $year = user_entitlements_get_exam_version_for_course($section);
    $cdn_domainlist = _courseware_get_cdn_domain_list();
    if(is_null($year)){
      $year = variable_get('default_course_year', '2015');
    }

    $cdn_domain = $cdn_domainlist[$year];
    
    $full_url = t($cdn_domain, array('@course_type' => strtolower($course_type), '@section' => strtolower($section), '@type' => 'vtt'));

    if (is_null($video) || empty($video) || !$video) {
        return $tracks;
    }

    try {
        $video_wrapper = entity_metadata_wrapper('node', $video);
        $computed_filename = ($course_type == "cram_course") ? $section . "-Cram-" . $topic_slug . ".vtt" : $section . "-" . $topic_slug . ".vtt";
        $tracks[] = array(
            'label'     => 'English',
            'language'  => 'en',
            'src'       => $full_url . $computed_filename,
        );

    } catch (EntityMetadataWrapperException $exc) {
        watchdog('courseware', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }

    return $tracks;
}


/**
 * Formats the video url for array data value
 * @param string $course_type
 * - Either "regular_course" or "cram_course"
 * @param string $section
 * - Exam section: "AUD', "BEC", etc
 * @param $low
 * - Required but utterly useless and unused parameter
 * @param $mid
 * - Required but utterly useless and unused parameter
 * @param $high
 * - Required but utterly useless and unused parameter
 * @param string $topic_slug
 * - The topic slug from the courseware URL / topic demarcation.
 * For the URL /study/BEC/BEC-Intro/0.01, the topic slug is '0.01'
 * @return array
 * - Returns a numeric array containing three arrays, where the inner array is an associate array with the keys
 * 'bitrate' and 'url'.
 * The 'bitrate' key may be the values 'low', 'mid', or 'high'.
 * Example:
 * array(
 *   0 => ['bitrate' => 'low', 'url' => 'http://urlhere.com/xxxx']
 *   1 => ['bitrate' => 'mid', 'url' => 'http://urlhere.com/xxxx']
 *   2 => ['bitrate' => 'high', 'url' => 'http://urlhere.com/xxxx']
 * )
 */
function _courseware_videos_urls($course_type = "regular_course", $section, $low, $mid, $high, $topic_slug=null) {
  $urls = array();
  $year = user_entitlements_get_exam_version_for_course($section);
  $cdn_domainlist = _courseware_get_cdn_domain_list();
  if(is_null($year)){
    $year = variable_get('default_course_year', '2015');
  }
  
  $cdn_domain = $cdn_domainlist[$year];
  
  if ($section != null && $topic_slug != null) {
    $computed_filename = ($course_type == "cram_course") ? $section . "-Cram-" . $topic_slug . ".mp4" : $section . "-" . $topic_slug . ".mp4";
    $urls[] = array(
        'bitrate' => 'low',
        'url' => t($cdn_domain, array('@course_type'=> strtolower($course_type), '@section'=> strtolower($section), '@type'=> "iphone")).$computed_filename
    );
    $urls[] = array(
        'bitrate' => 'mid',
        'url' => t($cdn_domain, array('@course_type'=> strtolower($course_type), '@section'=> strtolower($section), '@type'=> "iphone")).$computed_filename
    );
    $urls[] = array(
        'bitrate' => 'high',
        'url' => t($cdn_domain, array('@course_type'=> strtolower($course_type), '@section'=> strtolower($section), '@type'=> "web")).$computed_filename
    );
  }

  return $urls;
}

/**
 * Get URLs to this topic's PDF for each exam year
 *
 * @param string $course_type
 * - 'regular_course' or 'cram_course'
 * @param string $section
 * - 'AUD', 'BEC', etc
 * @param string $topic_slug
 * - '0.01' or '1.01', etc
 * @return array
 * - An array of URLs keyed by exam version
 * array(
 *   2017 => 'https://d3mve0yjx0m9hm.cloudfront.net/regular_course/aud/book/aud-0.01.pdf',
 *   2018 => 'https://d3qv7ygy6og45a.cloudfront.net/regular_course/aud/book/aud-0.01.pdf',
 *
 */
function _courseware_get_topic_pdf_urls($course_type, $section, $topic_slug) {
  $full_urls = array();
  $domains_list = _courseware_get_cdn_domain_list();

  // Get the pdf filename for this topic. Alter format slightly for CRAM
  $computed_pdf_filename = "$section-$topic_slug.pdf";
  if(strtolower($course_type) == "cram_course") {
    $computed_pdf_filename = "$section-Cram-$topic_slug.pdf";
  }

  // Lowercase URL components
  $course_type = strtolower($course_type);
  $section = strtolower($section);

  // Build an array of URLS
  foreach ($domains_list as $year => $urlPlaceholderString) {
    // Sample output:
    // https://d3qv7ygy6og45a.cloudfront.net/regular_course/aud/book/aud-0.01.pdf
    $full_urls['url_' . $year] =
      t($urlPlaceholderString, array('@course_type' => $course_type, '@section' => $section, '@type' => 'book'))
      . $computed_pdf_filename;
  }

  return $full_urls;
}


/**
 * Get the notes related to a topic
 *
 * @param type $topic_id topic to reference topic notes
 * @return <array> list of notes for a topic
 */
function _courseware_get_topic_notes($topic_id) {
    global $user;

    $topic_notes = array();

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'topic_notes')
        ->fieldCondition('field_topic_reference', 'target_id', $topic_id)
        ->propertyCondition('status', 1)
        ->propertyCondition('uid', $user->uid)
        ->propertyOrderBy('created', $direction = 'DESC');

    $result = $query->execute();

    if (isset($result['node'])) {
        $notes = node_load_multiple(array_keys($result['node']));

        foreach ($notes as $nid => $note) {
            $note_wrapper = entity_metadata_wrapper('node', $note);
            $topic_notes[] = _courseware_format_note_json($note_wrapper);
        }
    }

    return $topic_notes;
}


function _courseware_get_bookmark($videos) {
    global $user;

    $video_ids = array();
    $bookmark_data = array();

    foreach ($videos as $weight => $video) {
        $video_ids[$video->nid] = $video->nid;
    }

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'video_bookmark')
        ->propertyCondition('status', 1)
        ->propertyCondition('uid', $user->uid)
        ->fieldCondition('field_rcpa_video', 'target_id', $video_ids)
        // ->addMetaData('account', user_load(1))
    ;

    $result = $query->execute();

    if (isset($result['node'])) {
        $bookmarks = node_load_multiple(array_keys($result['node']));

        try {
            foreach ($bookmarks as $bid => $bookmark) {
                $bookmark_wrapper = entity_metadata_wrapper('node', $bookmark);

                $bookmark_data[] = _courseware_format_bookmark_json($bookmark_wrapper);

                // $bookmar_data[]['video'] = $bid;
                // $bookmar_data[]['timestamps'][] = array(
                //     'time' => $bookmark_wrapper->field_time->value(),
                //     'note' => $bookmark_wrapper->field_video_bookmark_note->value(),
                //     'id' => $bookmark_wrapper->getIdentifier()
                // );
            }
        } catch (Exception $ex) {
            watchdog('courseware', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
        }
    }

    return $bookmark_data;
}

