<?php
/**
 * array(
 *    'topic_reference' => array(TOPIC_ID1 , TOPIC_ID2, TOPIC_ID3 ....)
 *    'rcpa_video' => array(VIDEO_ID1, VIDEO_ID2, VIDEO_ID3 ....)
 *    'entitlement_product' => array(ENTITLEMENT_ID)
 *    'time' => NUMBER
 *    'video_bookmark_note' => "text note"
 * )
 */

function courseware_service_bookmark($bookmark_id = null)
{
    switch (strtolower($_SERVER['REQUEST_METHOD'])) {
        // new
        case 'post':
            $payload = json_decode(file_get_contents('php://input'), true);
            $response = _courseware_service_bookmark_create($payload);
            break;
        // update
        case 'put':
            $payload = json_decode(file_get_contents('php://input'), true);
            $response = _courseware_service_bookmark_update($bookmark_id, $payload);
            break;
        case 'delete':
            $response = _courseware_service_bookmark_delete($bookmark_id);
            break;
        default:
            drupal_add_http_header('Status', '501 Not Implemented');
            return '';
    }

    if ($response == 403) {
        drupal_add_http_header('Status', '403 Forbidden');
        return '';
    } else {
        // print out JSON to the browser
        drupal_add_http_header('Content-Type', 'application/json');
        print json_encode($response);
    }
}

/**
 * Handle PUT request to update a bookmark by id
 */
function _courseware_service_bookmark_update($bookmark_id, $payload) {

    $sku = $payload['entitlement_sku'];

    $response = _courseware_service_bookmark_push($payload['note'], $payload['time'], $payload['topic'], $payload['video'], $sku, $bookmark_id);

    return $response;
}

/**
 * Handle a POST request to create a bookmark
 */
function _courseware_service_bookmark_create($payload) {

    $sku = $payload['entitlement_sku'];

    $response = _courseware_service_bookmark_push($payload['note'], $payload['time'], $payload['topic'], $payload['video'], $sku);

    return $response;
}

/**
 *
 * @param array $data
 *      ** NID the node id for video history
 *      ** data data that will be added to the new node or updated one
 * @return array
 */
function _courseware_service_bookmark_push($text, $time, $topic_nid, $video_nid, $entitlement_sku, $bookmark_nid = null) {
    global $user;

    $response = array();

    $bookmark = courseware_load_or_create_bookmark($user->uid, $bookmark_nid);

    // @todo: better authorization here or in the hook_menu
    $is_authorized = $bookmark->uid == $user->uid;

    if ($is_authorized) {
        try {
            $bookmark_wrapper = entity_metadata_wrapper('node', $bookmark);
            $bookmark_wrapper->status->set(NODE_PUBLISHED);
            $bookmark_wrapper->language->set(LANGUAGE_NONE);
            $bookmark_wrapper->field_video_bookmark_note->set($text);
            $bookmark_wrapper->field_time->set($time);
            $bookmark_wrapper->field_topic_reference->set(array($topic_nid));
            $bookmark_wrapper->field_rcpa_video->set(array($video_nid));

            //Entitlement reference
            $topic = node_load($topic_nid);
            $topic_wrapper = entity_metadata_wrapper('node', $topic);
            $type = $topic_wrapper->field_course_type_ref->value();
            $section = $topic_wrapper->field_course_section->value();
            $type_id = $type->tid;
            $section_id = $section->tid;

            if(isset($user->user_entitlements['products']) && !empty($user->user_entitlements['products'])){
              $entitlement_id = $user->user_entitlements['products'][$entitlement_sku]['nid'];
              $bookmark_wrapper->field_entitlement_product->set($entitlement_id);
            }
            
            $bookmark_wrapper->save();

            $response = _courseware_format_bookmark_json($bookmark_wrapper);

        } catch (Exception $ex) {
            $response['message'] = 'Error on creating / updating the video bookmark';
            watchdog('courseware', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
        }
    }

    return $response;
}


/**
 * Handles the DELETE request to remove a note created by a user.
 */
function _courseware_service_bookmark_delete($bookmark_id) {
    global $user;

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('entity_id', $bookmark_id, '=')
        ->entityCondition('bundle', 'video_bookmark')
        ->propertyCondition('status', 1)
        ->propertyCondition('uid', $user->uid);

    $result = $query->execute();
    if(is_array($result['node'])) {
      $result = array_pop($result['node']);
    } 
    else {
      $result = null;
    }

    if (is_null($result)) {
        return 404;
    }

    $bookmark = node_load($result->nid);
    $bookmark_wrapper =  entity_metadata_wrapper('node', $bookmark);

    // @todo: better authorization here or in the hook_menu
    $is_authorized = $bookmark->uid == $user->uid;

    if (!$is_authorized) {
        $response = 403;
    } else {
        $bookmark_wrapper->delete();

        $response =  array(
            'messsage' => t('Bookmark removed succefully'),
        );
    }

    return $response;
}
