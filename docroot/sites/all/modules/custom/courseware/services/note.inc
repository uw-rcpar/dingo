<?php

function courseware_service_note($note_nid = null)
{
    switch (strtolower($_SERVER['REQUEST_METHOD'])) {
        case 'post':
            $payload = json_decode(file_get_contents('php://input'), true);
            $response = _courseware_service_note_create($payload);
            break;
        case 'put':
            $payload = json_decode(file_get_contents('php://input'), true);
            $response = _courseware_service_note_update($payload, $note_nid);
            break;
        case 'delete':
            $response = _courseware_service_note_delete($note_nid);
            break;
        default:
            drupal_add_http_header('Status', '501 Not Implemented');
            return '';
    }

    // responce can be an integer if we want to handle other response types
    if ($response == 403) {
        drupal_add_http_header('Status', '403 Forbidden');
        return '';
    } else {
        // print out JSON to the browser
        drupal_add_http_header('Content-Type', 'application/json');
        print json_encode($response);
    }
}

/**
 * Handle the POST action to update by note_id
 */
function _courseware_service_note_update($payload, $note_nid = null) {

    if (is_null($note_nid)) {
        return drupal_not_found();
    }

    $sku = $payload['entitlement_sku'];

    $response = _courseware_service_note_push($payload['note'], $payload['topic'], $sku, $note_nid);

    return $response;
}

/**
 * Handle the PUT action to create a note by user / topic
 */
function _courseware_service_note_create($payload) {

    $sku = $payload['entitlement_sku'];

    $response = _courseware_service_note_push($payload['note'], $payload['topic'], $sku);

    return $response;
}

/**
 * Handle the DELETE request to remove a note from the system
 *
 * @param integer $note_nid The node id of the note to remove
 */
function _courseware_service_note_delete($note_nid) {
    global $user;

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('entity_id', $note_nid, '=')
        ->entityCondition('bundle', 'topic_notes')
        ->propertyCondition('status', 1)
        ->propertyCondition('uid', $user->uid);

    $result = $query->execute();
    $result = array_pop($result['node']);

    if (is_null($result)) {
        return 404;
    }

    $note = node_load($result->nid);
    $note_wrapper =  entity_metadata_wrapper('node', $note);

    // @todo: better authorization here or in the hook_menu
    $is_authorized = $note->uid == $user->uid;

    if (!$is_authorized) {
        $response = 403;
    } else {
        $note_wrapper->delete();

        $response =  array(
            'messsage' => t('Note deleted succefully'),
        );
    }

    return $response;
}

/**
 * Utility function to save or create a topic_notes typ node. If the note_nid
 * is left empty a new node is created. Otherwise an update action is made.
 *
 * @param String $note_text The body of the note node
 * @param Integer $topic_nid The node id of the topic node the note relates to
 * @param Integer $note_nid If updating a note, this is the node id
 *
 * @return array
 */
function _courseware_service_note_push($note_text, $topic_nid, $entitlement_sku, $note_nid = null) {
    global $user;

    $response = array();

    $topic = node_load($topic_nid);
    $topic_wrapper = entity_metadata_wrapper('node', $topic);

    $note = courseware_load_or_create_note($user->uid, $topic->nid, $note_nid);

    // @todo: better authorization here or in the hook_menu
    $is_authorized = $note->uid == $user->uid;

    if ($is_authorized) {
        try {
            $note_wrapper = entity_metadata_wrapper('node', $note);
            $note_wrapper->status->set(1);
            $note_wrapper->language->set(LANGUAGE_NONE);
            $note_wrapper->body->set(array('value' => $note_text, 'format' => 'full_html'));
            $note_wrapper->field_topic_reference->set(array($topic_nid));

            //Entitlement reference       
            $topic_wrapper = entity_metadata_wrapper('node', $topic);
            $type = $topic_wrapper->field_course_type_ref->value();
            $section = $topic_wrapper->field_course_section->value();                  
            $type_id = $type->tid;      
            $section_id = $section->tid;
            
            if(isset($user->user_entitlements['products']) && !empty($user->user_entitlements['products'])){
              $entitlement_id = $user->user_entitlements['products'][$entitlement_sku]['nid'];
              $note_wrapper->field_entitlement_product->set($entitlement_id);
            }
            $note_wrapper->save();

            $response = array(
                'message' => t('Note saved succefully'),
                'note' => _courseware_format_note_json($note_wrapper)
            );
        } catch (Exception $ex) {
            $response['message'] = 'Error on creating / updating the video note';
            watchdog('courseware', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
        }
    }

    return $response;
}
