<?php

function courseware_service_highlight($topic_nid = null , $highlight_year = NULL)
{
    $highlight_year = is_null($highlight_year) ? variable_get('default_course_year', '2015') : $highlight_year;
    
    switch (strtolower($_SERVER['REQUEST_METHOD'])) {
        case 'get':
            $book_highlight = _courseware_service_highlight_get($topic_nid,$highlight_year);
            break;
        case 'post':
            $payload = json_decode(file_get_contents('php://input'), true);
            $book_highlight = _courseware_service_highlight_save($payload,$highlight_year);
            break;
        default:
            drupal_add_http_header('Status', '501 Not Implemented');
            return '';
    }

    // print out JSON to the browser
    drupal_add_http_header('Content-Type', 'application/json');
    print json_encode($book_highlight);
}

/**
 * Create or Update a book_highlight node
 *
 * @param array $data
 *      ** NID the node id for book highlight
 *      ** data data that will be added to the new node or updated one
 * @return array
 */
function _courseware_service_highlight_save($payload , $course_year = 2015) {
    global $user;

    $response = array();
    $topic = node_load($payload['topic']);
    $highlights = $payload['highlights'];
    $entitlement_sku = $payload['entitlement_sku'];    

    $node = courseware_load_or_create_highlight($user->uid, $topic->nid, $course_year);

  // @todo: better authorization here or in the hook_menu
    $is_authorized = $node->uid == $user->uid;

    if ($is_authorized) {
        try {
            $highlight_wrapper = entity_metadata_wrapper('node', $node);
            $highlight_wrapper->status->set(1);
            $highlight_wrapper->language->set(LANGUAGE_NONE);
            $highlight_wrapper->field_highlight->set(json_encode($highlights));
            $highlight_wrapper->field_topic_reference->set(array($topic->nid));
            $highlight_wrapper->field_course_year->set($course_year);

            // entitlement reference
            $topic_wrapper = entity_metadata_wrapper('node', $topic);
            $type = $topic_wrapper->field_course_type_ref->value();
            $section = $topic_wrapper->field_course_section->value();
            $type_id = $type->tid;
            $section_id = $section->tid;

            if(isset($user->user_entitlements['products']) && !empty($user->user_entitlements['products'])){
              $entitlement_id = $user->user_entitlements['products'][$entitlement_sku]['nid'];
              $highlight_wrapper->field_entitlement_product->set($entitlement_id);
            }

            $highlight_wrapper->save();

            $response['status'] = array(
                'message' => t('Bookmark created succefully'),
                'book_highlight_nid' => $highlight_wrapper->getIdentifier(),
                'data' =>  _courseware_format_highlight_json($highlight_wrapper)
            );
        } catch (Exception $ex) {
            $response['message'] = "Error on creating / updating  the video history";
            watchdog('courseware', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
        }
    }

    return $response;
}

function _courseware_service_highlight_get($topic_nid = null, $highlight_year = 2015) {
  global $user;

    $response = array();
    $topic = node_load($topic_nid);

    $node = courseware_load_or_create_highlight($user->uid, $topic->nid,$highlight_year);

    // @todo: better authorization here or in the hook_menu
    $is_authorized = $node->uid == $user->uid;

    if ($is_authorized) {
        try {
            $highlight_wrapper = entity_metadata_wrapper('node', $node);

            $response = array(
                'message' => t('Bookmark loaded succefully'),
                'book_highlight_nid' => $highlight_wrapper->getIdentifier(),
                'data' =>  _courseware_format_highlight_json($highlight_wrapper)
            );
        } catch (Exception $ex) {
            $response['message'] = "Error on loading the video history";
            watchdog('courseware', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
        }
    }

    return $response;
}
