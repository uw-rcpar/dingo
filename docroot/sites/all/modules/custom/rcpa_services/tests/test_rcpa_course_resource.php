<?php

define('shortname', 'cr');

include_once 'serverinfo.php';

function rest_post_data($logged_user){
	/*
	* Server REST - node.create
	*/
	// REST Server URL
        $path = service . '/' . shortname;
	$request_url = server_request_url($path);
	// Node data
	$node_data = array(
	  'uid'=>'1',
	  'name'=>'admin',
	  'title' => 'A node created with services 3.x and REST',	  
	  'body[und][0][format]' =>'full_html',
	  'body[und][0][value]' => '<p>rest Body</p>',
     	  'field_course_section[und][values]' => '1455',     	  
	);
	$node_data = http_build_query($node_data);

	// Define cookie session
	$cookie_session = $logged_user->session_name . '=' . $logged_user->sessid;
	// cURL
	$curl = curl_init($request_url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $logged_user->token)); // Accept JSON response
	curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST
	curl_setopt($curl, CURLOPT_POSTFIELDS, $node_data); // Set POST data
	curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
	curl_setopt($curl, CURLOPT_COOKIE, "$cookie_session"); // use the previously saved session
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
	$response = curl_exec($curl);
	
	
	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	// Check if login was successful
	if ($http_code == 200) {
		// Convert json response as array
		$node = json_decode($response);
		return $node;
	}
	else {
		// Get error msg
		$http_message = curl_error($curl);
		return $http_message;
	}
}

function rest_get_data($logged_user, $nid = null){
	/*
	* Server REST - node.create
	*/
	// REST Server URL
	$path = service . '/' . shortname . "/{$nid}";
	

        $filters = array(
            'filter' => 'online',
            //'filter_1' => 'cram',
            //'filter_2' => 'offline',
        );
        $filters = http_build_query($filters);
                
        $request_url = server_request_url($path);
        $request_url .= is_null($nid) ?  '?'.$filters : '';        
	// Define cookie session
	$cookie_session = $logged_user->session_name . '=' . $logged_user->sessid;
	// cURL
	$curl = curl_init($request_url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $logged_user->token)); // Accept JSON response
	curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
	curl_setopt($curl, CURLOPT_COOKIE, "$cookie_session"); // use the previously saved session
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);        
	curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
	$response = curl_exec($curl);
	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	// Check if login was successful
	if ($http_code == 200) {
		// return json response
		return $response;
 		// Convert json response as array
		//$node = json_decode($response);
		//return $node;
	}
	else {
		// Get error msg
		$http_message = curl_error($curl);
		return $http_message;
	}
}

function rest_delete_data($logged_user, $nid){
	/*
	* Server REST - node.create
	*/
	// REST Server URL
	$path = service . '/' . shortname . "/{$nid}";
	$request_url = server_request_url($path);


	// Define cookie session
	$cookie_session = $logged_user->session_name . '=' . $logged_user->sessid;
	// cURL
	$curl = curl_init($request_url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $logged_user->token)); // Accept JSON response
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
	curl_setopt($curl, CURLOPT_COOKIE, "$cookie_session"); // use the previously saved session
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
	$response = curl_exec($curl);
	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	// Check if login was successful
	if ($http_code == 200) {
		// return json response
		return $response;
 		// Convert json response as array
		//$node = json_decode($response);
		//return $node;
	}
	else {
		// Get error msg
		$http_message = curl_error($curl);
		return $http_message;
	}
}

function rest_update_data($logged_user, $nid){
	/*
	* Server REST - node.create
	*/
	// REST Server URL
	$path = service . '/' . shortname . "/{$nid}";
	$request_url = server_request_url($path);
	// Node data
	$node_data = array(
	  'body[und][0][value]' => '<p>rest Body</p><p>Adding more data for courses' . rand() . '</p>',
	);
	$node_data = http_build_query($node_data);

	// Define cookie session
	$cookie_session = $logged_user->session_name . '=' . $logged_user->sessid;
	// cURL
	$curl = curl_init($request_url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'X-CSRF-Token: ' . $logged_user->token)); // Accept JSON response
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($curl, CURLOPT_POSTFIELDS, $node_data); // Set POST data
	curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
	curl_setopt($curl, CURLOPT_COOKIE, "$cookie_session"); // use the previously saved session
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
	$response = curl_exec($curl);
	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	// Check if login was successful
	if ($http_code == 200) {
		// Convert json response as array
		$node = json_decode($response);
		return $node;
	}
	else {
		// Get error msg
		$http_message = curl_error($curl);
		return $http_message;
	}
}


echo "<pre>";
$logdata = rest_connect();

//$output = rest_get_data($logdata);
//print_r($output);

$output = rest_get_data($logdata);
print_r($output);

//$node = rest_get_data($logdata, '720');
//echo "<br><br>";
//print_r($node);

//$update = rest_update_data($logdata, '243');
//echo "<br><br>";
//print_r($update);

//$delete = rest_delete_data($logdata, '727');
//echo "<br><br>";
//print_r($delete);



