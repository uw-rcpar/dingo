<?php

//node type rcpa_topic

function _node_rcpa_topic_resource_definition() {
  $resource = sprintf("resources/node_%s_resource", 'rcpa_topic');
  $node_resource = array(
    'rcpa_topic'.'_resource' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve a node ' . 'rcpa_topic',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_topic'.'_resource_retrieve',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to retrieve',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'help' => 'Create a node '.'rcpa_topic',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_topic'.'_resource_create',
          'args' => array(
            array(
              'name' => 'node',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to create of type '. 'rcpa_topic',
              'type' => 'array',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),
        'update' => array(
          'help' => 'Update a node of type '. 'rcpa_topic',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_topic'.'_resource_update',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to update',
            ),
            array(
              'name' => 'node',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to update',
              'type' => 'array',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'help' => t('Delete a node'),
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_topic'.'_resource_delete',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to delete',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
        'index' => array(
          'help' => 'List all nodes of type '.'rcpa_topic',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_topic'.'_resource_index',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'fields',
              'optional' => TRUE,
              'type' => 'string',
              'description' => 'The fields to get.',
              'default value' => '*',
              'source' => array('param' => 'fields'),
            ),
            array(
              'name' => 'parameters',
              'optional' => TRUE,
              'type' => 'array',
              'description' => 'Parameters array',
              'default value' => array(),
              'source' => array('param' => 'parameters'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'pagesize'),
            ),
          ),
          'access callback' => '_rcpa_node_index_resource_access',
          'access arguments' => array('view'),
        ),
      ),
    ),
  );
  
  return $node_resource;
}

/**
 * Returns the results of a node_load() for the specified node.
 *
 * This returned node may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $nid
 *   NID of the node we want to return.
 * @return
 *   Node object or FALSE if not found.
 *
 * @see node_load()
 */
function _node_rcpa_topic_resource_retrieve($nid) {
  $values = _rcpa_topic_resource_get_values($nid);
  return (object) $values;
}

/**
 * Creates a new node based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $node
 *   Array representing the attributes a node edit form would submit.
 * @return
 *   An associative array contained the new node's nid and, if applicable,
 *   the fully qualified URI to this resource.
 *
 * @see drupal_form_submit()
 */
function _node_rcpa_topic_resource_create($node) {
  global $user;
  // Adds backwards compatability with regression fixed in #1083242
  $node = _services_arg_value($node, 'node');
  if (!isset($node['name'])) {
    // Assign username to the node from $user created at auth step.
    if (isset($user->name)) {
      $node['name'] = $user->name;
    }
  }
  if(!isset($node['language'])) {
    $node['language'] = LANGUAGE_NONE;
  }
  
  $node['type'] = 'rcpa_topic';
  // Validate the node. If there is validation error Exception will be thrown
  // so code below won't be executed.
  _rcpa_services_node_resource_validate_type($node);

  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'node', 'node.pages');

  $node_type = $node['type'];

  // Setup form_state
  $form_state = array();
  $form_state['values'] = $node;
  $form_state['values']['op'] = variable_get('services_node_save_button_' . $node_type . '_resource_create', t('Save'));
  $form_state['programmed_bypass_access_check'] = FALSE;

  // Build a stub node object for the form in a similar way as node_add() does,
  // but always make the node author default to the current user (if the user
  // has permission to change it, $form_state['values'] will override this
  // default when the form is submitted).
  $stub_node = (object) array_intersect_key($node, array_flip(array('type', 'language')));
  $stub_node->name = $user->name;
  drupal_form_submit($node_type . '_node_form', $form_state, (object)$stub_node);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }
  // Fetch $nid out of $form_state
  $nid = $form_state['nid'];
  // Only add the URI for servers that support it.
  $node = array('nid' => $nid);
  if ($uri = services_resource_uri(array('node', $nid))) {
    $node['uri'] = $uri;
  }
  
  _rcpa_topic_get_clear_cache();
  return $node;
}

/**
 * Updates a new node based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $nid
 *   Node ID of the node we're editing.
 * @param $node
 *   Array representing the attributes a node edit form would submit.
 * @return
 *   The node's nid.
 *
 * @see drupal_form_submit()
 */
function _node_rcpa_topic_resource_update($nid, $node) {
  // Adds backwards compatability with regression fixed in #1083242
  $node = _services_arg_value($node, 'node');

  $node['nid'] = $nid;

  $old_node = node_load($nid);

  if ($old_node->type != 'rcpa_topic') {
    return services_error(t('Node @type not supported for this service', array('@type' => $old_node->type)), 404);
  }

  if (empty($old_node->nid)) {
    return services_error(t('Node @nid not found', array('@nid' => $old_node->nid)), 404);
  }

  // If no type is provided use the existing node type.
  if (empty($node['type'])) {
    $node['type'] = $old_node->type;
  }
  elseif ($node['type'] != $old_node->type) {
    // Node types cannot be changed once they are created.
    return services_error(t('Node type cannot be changed'), 406);
  }

  // Validate the node. If there is validation error Exception will be thrown
  // so code below won't be executed.
  _node_resource_validate_type($node);

  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'node', 'node.pages');

  $node_type = $node['type'];
  node_object_prepare($old_node);

  // Setup form_state.
  $form_state = array();
  $form_state['values'] = $node;
  $form_state['values']['op'] = variable_get('services_node_save_button_' . $node_type . '_resource_update', t('Save'));
  $form_state['node'] = $old_node;
  $form_state['programmed_bypass_access_check'] = FALSE;
  drupal_form_submit($node_type . '_node_form', $form_state, $old_node);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }

  $node = array('nid' => $nid);
  if ($uri = services_resource_uri(array('node', $nid))) {
    $node['uri'] = $uri;
  }
  
  _rcpa_topic_get_clear_cache();
  return $node;
}

/**
 * Delete a node given its nid.
 *
 * @param int $nid
 *   Node ID of the node we're deleting.
 * @return bool
 *   Always returns true.
 */
function _node_rcpa_topic_resource_delete($nid) {
  $node = node_load($nid);
  if($node->type != 'rcpa_topic'){
    return services_error(t('Node @nid is not of type ' . 'rcpa_topic', array('@nid' => $node->nid)), 404);
  }
  node_delete($nid);
  _rcpa_topic_get_clear_cache();
  return TRUE;
}

/**
 * Return an array of optionally paged nids baed on a set of criteria.
 *
 * An example request might look like
 *
 * http://domain/endpoint/node?fields=nid,vid&parameters[nid]=7&parameters[uid]=1
 *
 * This would return an array of objects with only nid and vid defined, where
 * nid = 7 and uid = 1.
 *
 * @param $page
 *   Page number of results to return (in pages of 20).
 * @param $fields
 *   The fields you want returned.
 * @param $parameters
 *   An array containing fields and values used to build a sql WHERE clause
 *   indicating items to retrieve.
 * @param $page_size
 *   Integer number of items to be returned.
 * @return
 *   An array of node objects.
 *
 * @todo
 *   Evaluate the functionality here in general. Particularly around
 *     - Do we need fields at all? Should this just return full nodes?
 *     - Is there an easier syntax we can define which can make the urls
 *       for index requests more straightforward?
 */
function _node_rcpa_topic_resource_index($page, $fields, $parameters, $page_size) {
  $cid = "node_rcpa_topic_index";
  if (count($_GET) > 1) {
    $cid = "{$cid}_" . implode("_", $_GET);
  }

  $results = &drupal_static(__FUNCTION__);
  if (!isset($results)) {
    if ($cache = cache_get($cid)) {
      $results = $cache->data;
    } else {
      $results = (object) _rcpa_topic_resource_get_values();

      cache_set($cid, $results, 'cache');
    }
  }

  return $results;
}

function _rcpa_topic_resource_get_values($nid = null) {
  $topics_data = array();
  
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'rcpa_topic');
  if(!is_null($nid)){
    $query->entityCondition('entity_id', $nid);
  }
  
  rcpa_services_filters_course_type($query);
  
  $query->propertyCondition('status', 1);
  $query->addMetaData('account', user_load(1));
  $result = $query->execute();

  $fields = rcpa_services_field_info_instances('rcpa_topic');  
  
  if (isset($result['node'])) {
    $topics = node_load_multiple(array_keys($result['node']));
    
    foreach ($topics as $weight => $topic) {
      try {                
        $topic_wrapper = entity_metadata_wrapper('node', $topic);
        $nid = $topic_wrapper->getIdentifier();
        $pagination = _rcpa_topic_get_previous_next_values($nid);
        $chapter_info = _rcpa_topic_get_previous_next_values($pagination->parent_id, false);
        
        $topics_data["topic_{$nid}"] = array(
            'title' => $topic_wrapper->label(),
            'id' => $topic_wrapper->getIdentifier(),
            'previous' => $pagination->prev,
            'next' => $pagination->next,
            'date_created' => $topic->created,
            'date_modified' => $topic->changed,            
            'content' => _rcpa_topic_get_videos($topic_wrapper->field_rcpa_video->value()),
            'course_type' => rcpa_services_get_course_type_name($topic_wrapper->field_course_type_ref->value()),
            'course_section' => $topic_wrapper->field_course_section->value()->name,
            'weight' => $pagination->weight,
            'parent_weight' => $chapter_info->weight
            //'pages' => _rcpa_topic_get_pages(),
            //'userdata' => _rcpa_topic_get_userdata()
        );
        $cdn_domainlist = _courseware_get_cdn_domain_list();
        $default_course_year = variable_get('default_course_year', '2015');          
        
        foreach ($fields as $key => $value) {
//        $topics_data["topic_{$nid}"][$value] = $topic_wrapper->$key->value();
          $cdn_domain = '';
          $type ='';
          $course_type = 'regular_course';
          if ($value == "page_url") {
            $type = 'book';
            $cdn_domain = $cdn_domainlist[$default_course_year];
          }
          $section = '';
          if ($topic_wrapper->field_course_section->raw()) {
            $term = taxonomy_term_load($topic_wrapper->field_course_section->raw());
            $section = $term->name;
          }

          // If Course Type Taxonomy setting is (Online CRAM Course 1466)
          if (rcpa_services_get_course_type_name($topic_wrapper->field_course_type_ref->value()) == "Online Cram Course") {
            $course_type = 'cram_course';
          }

          $topics_data["topic_{$nid}"][$value] = t($cdn_domain, array('@course_type'=> strtolower($course_type), '@section'=> strtolower($section), '@type'=>$type)) . $topic_wrapper->$key->value();
        }
      } catch (EntityMetadataWrapperException $exc) {
        watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
    }
  }

  return $topics_data;
}

function _rcpa_topic_get_previous_next_values($nid, $get_topics = true){
  $data = ($get_topics) ? _rcpa_topic_get_sibilings($nid) :_rcpa_chapter_get_sibilings($nid) ;
  $sibilings = $data['sibilings'];
  $prev = $next = $current = null;
  foreach ($sibilings as $delta => $topic) {
    if($topic->nid == $nid){
      $current = $delta;
    }
  }
  if(!is_null($current)){
    $prev = isset($sibilings[$current-1]) ? $sibilings[$current-1]->nid : null;
    $next = isset($sibilings[$current+1]) ? $sibilings[$current+1]->nid : null;
  }
  
  return (object) array(
      'next' => $next,
      'current' => $nid,
      'prev' => $prev,
      'weight' => is_null($current) ? 0 : $current,
      'parent_id' => $data['parent_nid']
  );
}

function _rcpa_topic_get_videos($videos = null) {  
  $videos_data = array();
  if (is_null($videos)) {
    return $videos_data;
  }

  $fields = rcpa_services_field_info_instances('rcpa_video'); 

  foreach ($videos as $weight => $video) {
    try {
      $video_wrapper = entity_metadata_wrapper('node', $video);
      $uri = entity_uri('node', $video);
      $video_url = url($uri['path'], array('absolute' => TRUE));
      $videos_data[] = array(
          'id' => $video_wrapper->getIdentifier(),
          'url' => $video_url,
          'length' => $video_wrapper->field_video_duration->value(),
          'percentviewed' => rcpa_services_get_video_history($video_wrapper->getIdentifier()),
          'course_type' => rcpa_services_get_course_type_name($video_wrapper->field_course_type_ref->value()),
          'weight' => $weight
      );
      $cdn_domainlist = _courseware_get_cdn_domain_list();
      $default_course_year = variable_get('default_course_year', '2015');  
      foreach ($fields as $key => $value) {
        $cdn_domain = '';
        $course_type = 'regular_course';
        $type ='';
        if (strstr($key, '_resolution') && $video_wrapper->$key->value()) {
          if(strstr($key, 'low_resolution')) {
            $type = 'iphone';
          } else {
            $type = 'web';
          }
          $cdn_domain = $cdn_domainlist[$default_course_year];
        }

        // If Course Type Taxonomy setting is (Online CRAM Course 1466)
        if (rcpa_services_get_course_type_name($video_wrapper->field_course_type_ref->value()) == "Online Cram Course") {
          $course_type = 'cram_course';
        }
        $section = '';
        // VTT mapping
//        if ($key == "vtt_en_url" && $video_wrapper->field_vtt_en_url->value()) {
        if ($key == "field_vtt_en_url" && $video_wrapper->field_vtt_en_url->value()) {
          $type = 'vtt';
          $cdn_domain = $cdn_domainlist[$default_course_year];
        }

        // Taxonomy lookup 
        if ($video_wrapper->field_course_section->raw()) {
          $term = taxonomy_term_load($video_wrapper->field_course_section->raw());
          $section = $term->name;
        }

        $videos_data[][$value] = t($cdn_domain, array('@course_type'=> strtolower($course_type), '@section'=> strtolower($section), '@type'=>$type)) . $video_wrapper->$key->value();
      }                
      
      $videos_tracks = _rcpa_topic_get_video_tracks($video_wrapper->field_video_tracks->value());
      
    } catch (EntityMetadataWrapperException $exc) {
      watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
  }

  return array('videos' => $videos_data, 'video_tracks' => $videos_tracks);
}

function _rcpa_topic_get_video_tracks($videos = null) {  
  $videos_data = array();
  if (is_null($videos)) {
    return $videos_data;
  }

  if (isset($videos)) {
   // foreach ($videos as $video) {
      try {
        $video_wrapper = entity_metadata_wrapper('node', $videos);
        $videos_data[] = array(
            'id' => $video_wrapper->getIdentifier(),
/*
            'vtt_en' => $video_wrapper->field_vtt_en->value(),
            'vtt_jp' => $video_wrapper->field_vtt_en->value(),
            'audio_en' => $video_wrapper->field_audio_en->value(),
            'audio_jp' => $video_wrapper->field_audio_jp->value(),
*/
            'vtt_en' => '/file-service/video-tracks/'. $video_wrapper->getIdentifier() .'/vtt-en',
            'vtt_jp' => '/file-service/video-tracks/'. $video_wrapper->getIdentifier() .'/vtt-jp',
            'audio_en' => '/file-service/video-tracks/'. $video_wrapper->getIdentifier() .'/audio-en',
            'audio_jp' => '/file-service/video-tracks/'. $video_wrapper->getIdentifier() .'/audio-jp',
        );
   
    } catch (EntityMetadataWrapperException $exc) {
        watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }

    //}
  }
  return $videos_data;
}

function rcpa_services_get_files($type, $id, $format) {
  $error = false;
  $output = '';
  
  $acceptable_formats = array('vtt-en','vtt-jp','audio-en','audio-jp');

  // Is of type we expect
  if (!in_array($format, $acceptable_formats)) {
    $error = true;
  }

  if (!$error) {
    $video_track = node_load($id);
    $video_wrapper = entity_metadata_wrapper('node', $video_track);
    $format = str_ireplace('-', '_', $format);
    $field = "field_{$format}";
    $file_contents = $video_wrapper->$field->value();

    // If not empty
    if (isset($file_contents)) {
      $ext = (is_array($file_contents['filename'])) ? $file_contents['filename'] : $format . '-' . $id . '.vtt';
      drupal_add_http_header('Content-type', 'application/force-download');
      drupal_add_http_header('Content-Transfer-Encoding', 'Binary');
      drupal_add_http_header('Content-length', strlen($output));
      drupal_add_http_header('Content-disposition', 'attachment; filename=' . $ext);
      drupal_add_http_header('Pragma', 'no-cache');
      drupal_add_http_header('Expires', '0');
      exit($output);
    } else {
      $error = true;    
    }
  }  

  if ($error) {
    drupal_set_message(t('File download failed, please check your request parameters.'), 'warning');
    return $output;
  }
 
  return $output;
}

function _rcpa_topic_get_pages() {  
  return (object) array();
}

function _rcpa_topic_get_userdata() {  
  return (object) array();
}

function _rcpa_topic_get_sibilings($nid, $exam_version = NULL) {
  // If the exam_version is empty default to the latest version
  $exam_version = (!$exam_version) ? exam_version_get_default_version() : $exam_version;
  $exam_years = exam_versions_years();
  $exam_version_tid = $exam_years[$exam_version];
  
  $sibilings = array();
  $result = db_query("SELECT DISTINCT n.nid
  FROM node n
  INNER JOIN field_data_field_topic_per_exam_version vtopic on vtopic.entity_id = n.nid
  INNER JOIN field_data_field_topic_reference topic on topic.entity_id = vtopic.field_topic_per_exam_version_target_id
  WHERE n.type = 'rcpa_chapter'
  AND n.status = 1
  AND topic.field_topic_reference_target_id = :nid",
  array(':nid' => $nid));

  $parent_id = 0;
  foreach ($result as $record) {
    $parent_id = $record->nid;
    break;
  }
  
  if ($parent_id != 0) {
    $chapter = node_load($parent_id);
    
    try {
      $chapter_wrapper = entity_metadata_wrapper('node', $chapter);
      $sibilings = _courseware_get_topics($chapter_wrapper, $exam_version_tid);
    } catch (EntityMetadataWrapperException $exc) {
      watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
  }
  
  return array('sibilings' => $sibilings , 'parent_nid' => $parent_id);
}

function _rcpa_topic_get_clear_cache(){
 cache_clear_all('node_rcpa_topic_index', 'cache', TRUE);  
}

function _rcpa_chapter_get_sibilings($nid, $exam_version = NULL) {
  // If the exam_version is empty default to the latest version
  $exam_version = (!$exam_version) ? exam_version_get_default_version() : $exam_version;
  $exam_years = exam_versions_years();
  $exam_version_tid = $exam_years[$exam_version];
  
  $sibilings = array();
  $result = db_query("SELECT DISTINCT n.nid
  FROM node n
  INNER JOIN field_data_field_chapter_per_exam_version vchapter on vchapter.entity_id = n.nid
  INNER JOIN field_data_field_topic_exam_chapter_ref chapter on chapter.entity_id = vchapter.field_chapter_per_exam_version_target_id
  WHERE n.type = 'rcpa_course'
  AND n.status = 1
  AND chapter.field_topic_exam_chapter_ref_target_id = :nid",
  array(':nid' => $nid));

  $parent_id = 0;
  foreach ($result as $record) {
    $parent_id = $record->nid;
    break;
  }
  
  if ($parent_id != 0) {
    $course = node_load($parent_id);
    
    try {
      $course_wrapper = entity_metadata_wrapper('node', $course);
      $sibilings = _courseware_get_chapters($course_wrapper, $exam_version_tid);
    } catch (EntityMetadataWrapperException $exc) {
      watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
    }
  }
  
  return array('sibilings' => $sibilings , 'parent_nid' => $parent_id);
}