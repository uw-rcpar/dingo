<?php

//node type rcpa_course 

function _node_rcpa_course_resource_definition() {
  $resource = sprintf("resources/node_%s_resource", 'rcpa_course');
  $node_resource = array(
    'rcpa_course'.'_resource' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve a node ' . 'rcpa_course',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_course'.'_resource_retrieve',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to retrieve',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'help' => 'Create a node '.'rcpa_course',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_course'.'_resource_create',
          'args' => array(
            array(
              'name' => 'node',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to create of type '. 'rcpa_course',
              'type' => 'array',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),
        'update' => array(
          'help' => 'Update a node of type '. 'rcpa_course',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_course'.'_resource_update',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to update',
            ),
            array(
              'name' => 'node',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to update',
              'type' => 'array',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'help' => t('Delete a node'),
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_course'.'_resource_delete',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to delete',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
        'index' => array(
          'help' => 'List all nodes of type '.'rcpa_course',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'rcpa_course'.'_resource_index',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'fields',
              'optional' => TRUE,
              'type' => 'string',
              'description' => 'The fields to get.',
              'default value' => '*',
              'source' => array('param' => 'fields'),
            ),
            array(
              'name' => 'parameters',
              'optional' => TRUE,
              'type' => 'array',
              'description' => 'Parameters array',
              'default value' => array(),
              'source' => array('param' => 'parameters'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'pagesize'),
            ),
          ),
          'access callback' => '_rcpa_node_index_resource_access',
          'access arguments' => array('view'),
        ),
      ),
    ),
  );
  
  return $node_resource;
}

function _node_rcpa_course_resource_retrieve($nid) {
  $course_nodes = course_resource_get_values($nid);

  return  (object) $course_nodes;
}

function _node_rcpa_course_resource_create($node) {
  global $user;
  // Adds backwards compatability with regression fixed in #1083242
  $node = _services_arg_value($node, 'node');
  if (!isset($node['name'])) {
    // Assign username to the node from $user created at auth step.
    if (isset($user->name)) {
      $node['name'] = $user->name;
    }
  }
  if(!isset($node['language'])) {
    $node['language'] = LANGUAGE_NONE;
  }
  
  $node['type'] = 'rcpa_course';
  // Validate the node. If there is validation error Exception will be thrown
  // so code below won't be executed.
  _rcpa_services_node_resource_validate_type($node);

  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'node', 'node.pages');

  $node_type = $node['type'];

  // Setup form_state
  $form_state = array();
  $form_state['values'] = $node;
  $form_state['values']['op'] = variable_get('services_node_save_button_' . $node_type . '_resource_create', t('Save'));
  $form_state['programmed_bypass_access_check'] = FALSE;  
  // Build a stub node object for the form in a similar way as node_add() does,
  // but always make the node author default to the current user (if the user
  // has permission to change it, $form_state['values'] will override this
  // default when the form is submitted).
  $stub_node = (object) array_intersect_key($node, array_flip(array('type', 'language')));
  $stub_node->name = $user->name;
  drupal_form_submit($node_type . '_node_form', $form_state, (object)$stub_node);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }
  // Fetch $nid out of $form_state
  $nid = $form_state['nid'];
  // Only add the URI for servers that support it.
  $node = array('nid' => $nid);
  if ($uri = services_resource_uri(array('node', $nid))) {
    $node['uri'] = $uri;
  }
  
  _node_rcpa_course_clear_cache();
  return $node;
}

function _node_rcpa_course_resource_update($nid, $node) {
  // Adds backwards compatability with regression fixed in #1083242
  $node = _services_arg_value($node, 'node');
  
  $node['nid'] = $nid;

  $old_node = node_load($nid);
  
  if ($old_node->type != 'rcpa_course') {
    return services_error(t('Node @type not supported for this service', array('@type' => $old_node->type)), 404);
  }

  
  if (empty($old_node->nid)) {
    return services_error(t('Node @nid not found', array('@nid' => $old_node->nid)), 404);
  }

  // If no type is provided use the existing node type.
  if (empty($node['type'])) {
    $node['type'] = $old_node->type;
  }
  elseif ($node['type'] != $old_node->type) {
    // Node types cannot be changed once they are created.
    return services_error(t('Node type cannot be changed'), 406);
  }

  // Validate the node. If there is validation error Exception will be thrown
  // so code below won't be executed.
  _node_resource_validate_type($node);

  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'node', 'node.pages');

  $node_type = $node['type'];
  node_object_prepare($old_node);

  // Setup form_state.
  $form_state = array();
  $form_state['values'] = $node;
  $form_state['values']['op'] = variable_get('services_node_save_button_' . $node_type . '_resource_update', t('Save'));
  $form_state['node'] = $old_node;
  $form_state['programmed_bypass_access_check'] = FALSE;
  drupal_form_submit($node_type . '_node_form', $form_state, $old_node);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }

  $node = array('nid' => $nid);
  if ($uri = services_resource_uri(array('node', $nid))) {
    $node['uri'] = $uri;
  }
  
  _node_rcpa_course_clear_cache();
  return $node;
}

function _node_rcpa_course_resource_delete($nid) {
  $node = node_load($nid);
  if($node->type != 'rcpa_course'){
    return services_error(t('Node @nid is not of type ' . 'rcpa_course', array('@nid' => $node->nid)), 404);
  }
  node_delete($nid);
  _node_rcpa_course_clear_cache();
  return TRUE;
}

function _node_rcpa_course_resource_index($page, $fields, $parameters, $page_size) {  
  $cid = "rcpa_course_index";
  if (count($_GET) > 1) {
    $cid = "{$cid}_" . implode("_", $_GET);
  }

  $results = &drupal_static(__FUNCTION__);

  if (!isset($results)) {
    if ($cache = cache_get($cid)) {
      $results = $cache->data;
    } else {
      $results = (object) course_resource_get_values();
      cache_set($cid, $results, 'cache');
    }
  }

  return $results;
}

function course_resource_get_values($nid = null) {
  $courses_data = array();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'rcpa_course');
  if (!is_null($nid)) {
    $query->entityCondition('entity_id', $nid);
  }

  rcpa_services_filters_course_type($query);

  $query->propertyCondition('status', 1);
  $query->addMetaData('account', user_load(1));
  $result = $query->execute();  
  $fields = rcpa_services_field_info_instances('rcpa_course');

  if (isset($result['node'])) {
    $courses = node_load_multiple(array_keys($result['node']));
    foreach ($courses as $course) {
      try {
        $course_wrapper = entity_metadata_wrapper('node', $course);        
        $section = $course_wrapper->field_course_section->value()->name;

        //Get chapters for this course
        $exam_version = user_entitlements_get_exam_version_for_course($section, $user);
        $exam_version = taxonomy_get_term_by_name($exam_version, 'exam_version');
        $exam_version_tid = key($exam_version);
        $chapter_list = _courseware_get_chapters($course_wrapper, $exam_version_tid);

        $nid = $course_wrapper->getIdentifier();
        $courses_data["{$section}_{$nid}"] = array(
            'section' => $section,
            'id' => $nid,
            'title' => $course_wrapper->label(),
            'date_created' => $course->created,
            'date_modified' => $course->changed,
            'course_type' => rcpa_services_get_course_type_name($course_wrapper->field_course_type_ref->value()),
            'chapters' => rcpa_course_get_chapters($chapter_list, $exam_version_tid),
        );
        foreach ($fields as $key => $value) {
          $courses_data["{$section}_{$nid}"][$value] = $course_wrapper->$key->value();
        }
      } catch (EntityMetadataWrapperException $exc) {
        watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
    }
  }

  return array('content' => $courses_data);
}

function rcpa_course_get_chapters($chapters = null, $exam_version_tid = null) {
  $chapters_data = array();
  if (is_null($chapters) || is_null($exam_version_tid)) {
    return $chapters_data;
  }
  
  $fields = rcpa_services_field_info_instances('rcpa_chapter');  
  
  foreach ($chapters as $weight => $chapter) {
      try {        
        $nid = $chapter->nid;
        $chapter_wrapper = entity_metadata_wrapper('node', $chapter);
        $topic_list = _courseware_get_topics($chapter_wrapper, $exam_version_tid);
        $chapters_data["chapter_{$nid}"] = array(
            'title' => $chapter_wrapper->label(),
            'id' => $nid,
            'course_type' => rcpa_services_get_course_type_name($chapter_wrapper->field_course_type_ref->value()),
            'topics' => rcpa_course_get_topics($topic_list),
            'weight' => $weight
        );
        
        foreach ($fields as $key => $value) {
           $chapters_data["chapter_{$nid}"][$value] = $chapter_wrapper->$key->value();
        }
        
      } catch (EntityMetadataWrapperException $exc) {
        watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
  }

  return $chapters_data;
}

function rcpa_course_get_topics($topics = null) {  
  $topics_data = array();
  if (is_null($topics)) {
    return $topics_data;
  }

  $fields = rcpa_services_field_info_instances('rcpa_topic');  
  
  foreach ($topics as $weight => $topic) {
    if (!is_null($topic)) {
      try {
        $nid = $topic->nid;
        $topic_wrapper = entity_metadata_wrapper('node', $topic);
        $topics_data["topic_{$nid}"] = array(
            'title' => $topic_wrapper->label(),
            'id' => $topic_wrapper->getIdentifier(),
            //'percentviewed' => rcpa_services_get_topic_video_history($topic_wrapper->getIdentifier()),
            'video_history' => rcpa_services_get_topic_video_history($topic_wrapper->getIdentifier()),
            'course_type' => rcpa_services_get_course_type_name($topic_wrapper->field_course_type_ref->value()),
            'weight' => $weight
        );

        foreach ($fields as $key => $value) {
          $topics_data["topic_{$nid}"][$value] = $topic_wrapper->$key->value();
        }
      } catch (EntityMetadataWrapperException $exc) {
        watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
    }
  }

  return $topics_data;

}


function _node_rcpa_course_clear_cache(){
 cache_clear_all('rcpa_course_index', 'cache', TRUE);  
}