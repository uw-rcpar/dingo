<?php

//node type book_highlight

function _node_book_highlight_resource_definition() {
  $resource = sprintf("resources/node_%s_resource", 'book_highlight');
  $node_resource = array(
    'book_highlight'.'_resource' => array(
      'operations' => array(
        'retrieve' => array(
          'help' => 'Retrieve a node ' . 'book_highlight',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'book_highlight'.'_resource_retrieve',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to retrieve',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'help' => 'Create a node '.'book_highlight',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'book_highlight'.'_resource_create',
          'args' => array(
            array(
              'name' => 'node',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to create of type '. 'book_highlight',
              'type' => 'array',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),
        'update' => array(
          'help' => 'Update a node of type '. 'book_highlight',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'book_highlight'.'_resource_update',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to update',
            ),
            array(
              'name' => 'node',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The node data to update',
              'type' => 'array',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'help' => t('Delete a node'),
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'book_highlight'.'_resource_delete',
          'args' => array(
            array(
              'name' => 'nid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'int',
              'description' => 'The nid of the node to delete',
            ),
          ),
          'access callback' => '_rcpa_node_resource_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
        'index' => array(
          'help' => 'List all nodes of type '.'book_highlight',
          'file' => array('type' => 'inc', 'module' => 'rcpa_services', 'name' => $resource),
          'callback' => '_node_'.'book_highlight'.'_resource_index',
          'args' => array(
            array(
              'name' => 'page',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'The zero-based index of the page to get, defaults to 0.',
              'default value' => 0,
              'source' => array('param' => 'page'),
            ),
            array(
              'name' => 'fields',
              'optional' => TRUE,
              'type' => 'string',
              'description' => 'The fields to get.',
              'default value' => '*',
              'source' => array('param' => 'fields'),
            ),
            array(
              'name' => 'parameters',
              'optional' => TRUE,
              'type' => 'array',
              'description' => 'Parameters array',
              'default value' => array(),
              'source' => array('param' => 'parameters'),
            ),
            array(
              'name' => 'pagesize',
              'optional' => TRUE,
              'type' => 'int',
              'description' => 'Number of records to get per page.',
              'default value' => variable_get('services_node_index_page_size', 20),
              'source' => array('param' => 'pagesize'),
            ),
          ),
          'access callback' => '_rcpa_node_index_resource_access',
          'access arguments' => array('view'),
        ),
      ),
    ),
  );
  
  return $node_resource;
}

/**
 * Returns the results of a node_load() for the specified node.
 *
 * This returned node may optionally take content_permissions settings into
 * account, based on a configuration setting.
 *
 * @param $nid
 *   NID of the node we want to return.
 * @return
 *   Node object or FALSE if not found.
 *
 * @see node_load()
 */
function _node_book_highlight_resource_retrieve($nid) {
  $node = node_load($nid);

  if ($node) {
    $uri = entity_uri('node', $node);
    $node->path = url($uri['path'], array('absolute' => TRUE));
    // Unset uri as it has complete entity and this
    // cause never ending recursion in rendering.
    unset($node->uri);
  }
  
  if ($node->type == 'book_highlight') {
    return _book_highlight_resource_get_values($node->nid);
  } elseif ($node->type == 'rcpa_topic') {
    return _book_highlight_resource_get_values(null, $node->nid);
  }

  return array();
}

/**
 * Creates a new node based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $node
 *   Array representing the attributes a node edit form would submit.
 * @return
 *   An associative array contained the new node's nid and, if applicable,
 *   the fully qualified URI to this resource.
 *
 * @see drupal_form_submit()
 */
function _node_book_highlight_resource_create($node) {
  global $user;
  // Adds backwards compatability with regression fixed in #1083242
  $node = _services_arg_value($node, 'node');
  $node = rcpa_services_set_auto_title($node);
  $node = rcpa_services_get_entitlement_reference($node);  
  $node = rcpa_services_get_topic_reference($node);
  
  if (!isset($node['name'])) {
    // Assign username to the node from $user created at auth step.
    if (isset($user->name)) {
      $node['name'] = $user->name;
    }
  }
  if(!isset($node['language'])) {
    $node['language'] = LANGUAGE_NONE;
  }
  
  $node['type'] = 'book_highlight';
  // Validate the node. If there is validation error Exception will be thrown
  // so code below won't be executed.
  _rcpa_services_node_resource_validate_type($node);

  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'node', 'node.pages');

  $node_type = $node['type'];

  // Setup form_state
  $form_state = array();
  $form_state['values'] = $node;
  $form_state['values']['op'] = variable_get('services_node_save_button_' . $node_type . '_resource_create', t('Save'));
  $form_state['programmed_bypass_access_check'] = FALSE;

  // Build a stub node object for the form in a similar way as node_add() does,
  // but always make the node author default to the current user (if the user
  // has permission to change it, $form_state['values'] will override this
  // default when the form is submitted).
  $stub_node = (object) array_intersect_key($node, array_flip(array('type', 'language')));
  $stub_node->name = $user->name;
  drupal_form_submit($node_type . '_node_form', $form_state, (object)$stub_node);

  if ($errors = form_get_errors()) {
    foreach ($errors as $key => $value) {
      if ($value = "entitlement product field is required.") {
        $errors[$key] = t("User has not access to the requested entitlement product or it is not defined in the requested topic");
      }
    }
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }
  // Fetch $nid out of $form_state
  $nid = $form_state['nid'];
  // Only add the URI for servers that support it.
  $node = array('nid' => $nid);
  if ($uri = services_resource_uri(array('node', $nid))) {
    $node['uri'] = $uri;
  }
  
  _book_highlights_clear_cache();
  return $node;
}

/**
 * Updates a new node based on submitted values.
 *
 * Note that this function uses drupal_form_submit() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $nid
 *   Node ID of the node we're editing.
 * @param $node
 *   Array representing the attributes a node edit form would submit.
 * @return
 *   The node's nid.
 *
 * @see drupal_form_submit()
 */
function _node_book_highlight_resource_update($nid, $node) {
  // Adds backwards compatability with regression fixed in #1083242
  $node = _services_arg_value($node, 'node');
  $node = rcpa_services_get_entitlement_reference($node);
  $node = rcpa_services_get_topic_reference($node);
  
  $node['nid'] = $nid;

  $old_node = node_load($nid);
  
  if (!isset($old_node->field_entitlement_product['und'][0]['target_id' ])) {
    $old_node = rcpa_services_get_entitlement_reference($old_node);
    if (!isset($old_node->field_entitlement_product['und'][0]['target_id' ])) {
      return services_error(t("User has no entitlements of this section"), 406);
    }
  }
  
  if ($old_node->type != 'book_highlight') {
    return services_error(t('Node @type not supported for this service', array('@type' => $old_node->type)), 404);
  }
  
  if (empty($old_node->nid)) {
    return services_error(t('Node @nid not found', array('@nid' => $old_node->nid)), 404);
  }

  // If no type is provided use the existing node type.
  if (empty($node['type'])) {
    $node['type'] = $old_node->type;
  }
  elseif ($node['type'] != $old_node->type) {
    // Node types cannot be changed once they are created.
    return services_error(t('Node type cannot be changed'), 406);
  }

  // Validate the node. If there is validation error Exception will be thrown
  // so code below won't be executed.
  _node_resource_validate_type($node);

  // Load the required includes for drupal_form_submit
  module_load_include('inc', 'node', 'node.pages');

  $node_type = $node['type'];
  node_object_prepare($old_node);

  // Setup form_state.
  $form_state = array();
  $form_state['values'] = $node;
  $form_state['values']['op'] = variable_get('services_node_save_button_' . $node_type . '_resource_update', t('Save'));
  $form_state['node'] = $old_node;
  $form_state['programmed_bypass_access_check'] = FALSE;
  drupal_form_submit($node_type . '_node_form', $form_state, $old_node);

  if ($errors = form_get_errors()) {
    foreach ($errors as $key => $value) {
      if ($value = "entitlement product field is required.") {
        $errors[$key] = t("User has not access to the requested entitlement product or it is not defined in the requested topic");
      }
    }
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }

  $node = array('nid' => $nid);
  if ($uri = services_resource_uri(array('node', $nid))) {
    $node['uri'] = $uri;
  }
  
  _book_highlights_clear_cache();
  return $node;
}

/**
 * Delete a node given its nid.
 *
 * @param int $nid
 *   Node ID of the node we're deleting.
 * @return bool
 *   Always returns true.
 */
function _node_book_highlight_resource_delete($nid) {
  $node = node_load($nid);
  if($node->type != 'book_highlight'){
    return services_error(t('Node @nid is not of type ' . 'book_highlight', array('@nid' => $node->nid)), 404);
  }
  node_delete($nid);
  _book_highlights_clear_cache();
  return TRUE;
}

function _node_book_highlight_resource_index($page, $fields, $parameters, $page_size) {
  $results = &drupal_static(__FUNCTION__);
  
  if (!isset($results)) {
    if ($cache = cache_get('book_highlight_index')) {
      $results = $cache->data;
    }
    else {
        $results = _book_highlight_resource_get_values();
        cache_set('book_highlight_index', $results, 'cache');
    }
  }
  return $results;
}

function _book_highlight_resource_get_values($nid = null, $topic_reference = null) {
  $book_highlight_data = array();
  global $user;
  
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'book_highlight');
  if(!is_null($nid)){
    $query->entityCondition('entity_id', $nid);
  }
  $query->propertyCondition('status', 1);
  $query->addMetaData('account', user_load(1));
  $query->propertyCondition('uid', $user->uid);  
  $result = $query->execute();
  
  $fields = rcpa_services_field_info_instances('book_highlight');  
  
  if (isset($result['node'])) {
    $book_highlight = node_load_multiple(array_keys($result['node']));    
    foreach ($book_highlight as $book_highlight) {
      try {        
       $book_highlight_wrapper = entity_metadata_wrapper('node', $book_highlight);     
        if (is_numeric($topic_reference)) {
          $values = $book_highlight_wrapper->field_topic_reference->raw();
          if (!in_array($topic_reference, $values)) {
            continue;
          }
        }
       
        $nid = $book_highlight_wrapper->getIdentifier();
        $book_highlight_data["book_highlight_{$nid}"] = array(            
            'id' => $nid,
            'title' => $book_highlight_wrapper->label(),   
            'date_created' => $book_highlight->created,
            'date_modified' => $book_highlight->changed,
            'topic_reference' => $book_highlight_wrapper->field_topic_reference->raw(),
            //'chapter_id' => _book_highlights_get_chapters_ids($book_highlight_wrapper->field_topic_exam_chapter_ref->value())
        );        
        foreach ($fields as $key => $value) {
           $book_highlight_data["book_highlight_{$nid}"][$value] = $book_highlight_wrapper->$key->value();
        }
      } catch (EntityMetadataWrapperException $exc) {
        watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
    }
  }

  return  $book_highlight_data;
}


function _book_highlights_get_chapters_ids($chapters = null) {
  $chapters_data = array();
  if (is_null($chapters)) {
    return $chapters_data;
  }  
  
  foreach ($chapters as $chapter) {
    $chapters_data[$chapter->nid] = $chapter->nid;
  }

  return $chapters_data;
}


function _book_highlights_clear_cache(){
 cache_clear_all('book_highlight_index', 'cache', TRUE);  
}