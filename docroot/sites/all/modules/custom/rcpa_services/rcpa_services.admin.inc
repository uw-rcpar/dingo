<?php
function rcpa_services_admin_settings($var) {
  $form = array();
  
  $form['cdn_settings'] = array(
      '#title' => t('CDN Settings'),
      '#type' => 'fieldset',
      '#collapsible' => false,
  );

  //Get exam versions
  $exam_versions = exam_versions_years();
  $options = array();
  foreach ($exam_versions as $year => $value) {
    $options[$year] = t($year . ' Course Content (videos, pdf, vtt)');
  }

  $form['cdn_settings']['default_course_year'] = array(
  '#title' => t('Default Course Year'),
  '#type' => 'select',
  
  '#options' => $options,
  '#description' => t('This selection will automatically change the "Default CDN Domain" below and is used for Course Player and CDN Version content display. This selection will be displayed by default unless the user selects another year in the display.'),
  '#default_value' => variable_get('default_course_year', '2015'),
  );

  $form['cdn_settings']['cdn_domain_list'] = array(
      '#title' => t('Default CDN Domain'),
      '#type' => 'textarea',
      '#default_value' => variable_get('cdn_domain_list', '2015|https://d3addzld9o2wat.cloudfront.net/@course_type/@section/@type/
2016|https://dp081jg0mjwkx.cloudfront.net/@course_type/@section/@type/'),
      '#description' => t('Use the "Default Course Year" selection above to switch versions:<br>
      2015 = https://d3addzld9o2wat.cloudfront.net/@course_type/@section/@type/<br>
      2016 = https://dp081jg0mjwkx.cloudfront.net/@course_type/@section/@type/')
  );

  $form['cdn_settings']['amazons3_bucket_list'] = array(
      '#title' => t('Default AWS bucket'),
      '#type' => 'textarea',
      '#default_value' => variable_get('amazons3_bucket_list', '2015|rcpar.courseware-v2.0
2016|rcpar.courseware-v2.1.2016'),
      '#description' => t('Use the "Default Course Year" selection above to switch buckets:<br>
      2015 = rcpar.courseware-v2.0<br>
      2016 = rcpar.courseware-v2.1.2016')
  );
   return system_settings_form($form);
}


function rcpa_services_export_csv($form, &$form_state) {
    
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Download'),
  );
  
  
  return $form;
}

function rcpa_services_export_csv_submit($form, &$form_state) {
  $type = isset($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : "rcpar_topic";
  module_load_include('inc', 'rcpa_services', 'resources/node_rcpa_topic_resource');
  /**
   * [12/23/14, 1:55:44 PM] Peter Atkins: Section = AUD, REG
[12/23/14, 1:55:52 PM] Peter Atkins: Chapter
[12/23/14, 1:55:59 PM] Peter Atkins: topics weighted
[12/23/14, 1:56:32 PM] Peter Atkins: values: node_id, title, original id, original chapter id, original topic id if exist
   */
   $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'rcpa_topic');        
  $query->propertyCondition('status', 1);
  $query->addMetaData('account', user_load(1));
  $result = $query->execute();
  $csv_data = array();
  $fields = rcpa_services_field_info_instances('rcpa_topic');  
  
  if (isset($result['node'])) {
    $topics = node_load_multiple(array_keys($result['node']));
    
    foreach ($topics as $nid => $topic) {
      try {                
        $topic_wrapper = entity_metadata_wrapper('node', $topic);            
        $pagination = _rcpa_topic_get_previous_next_values($nid);
        $section = is_object($topic_wrapper->field_course_section->value()) ? $topic_wrapper->field_course_section->value()->name   : "NONE";     
        $chapter = rcpa_services_get_chapter_by_topic($nid);
        $csv_data[$section][] = array(
            'title' => $topic_wrapper->label(),
            'nid' => $nid,
            'weight' => $pagination->weight,
            'original_id' => $topic_wrapper->field_original_id->value(),
            'original_chapter_id' => $topic_wrapper->field_chapter_id->value(),    
            'chapter' => is_null($chapter) ? "NONE" : $chapter->title
        );
         
      } catch (EntityMetadataWrapperException $exc) {
        watchdog('rcpa_service', 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__));
      }
    }
  }

 
  // Add the headers needed to let the browser know this is a csv file download.
  drupal_add_http_header('Content-Type', 'text/csv; utf-8');
  drupal_add_http_header('Content-Disposition', 'attachment; filename = topicsList.csv');
 
  // Instead of writing to a file, we write to the output stream.
  $fh = fopen('php://output', 'w');
 
  // Add a header row
  fputcsv($fh, array(t('Section'), t('Title'),t('Node id'), t('Weight'),t('Original ID'), t('Original Chapter ID'), t('Chapter')));
 
  // Loop through our nodes and write the csv data.
  foreach($csv_data as $section_name => $section) {
    foreach($section as $node) {
      fputcsv($fh, array($section_name, $node['title'], $node['nid'], $node['weight'],$node['original_id'],$node['original_chapter_id'],$node['chapter']));
    }
  }
 
  // Close the output stream.
  fclose($fh);  
  exit;
}


function rcpa_services_get_chapter_by_topic($nid) {
  $result = db_query("SELECT n.nid
    FROM node n
    INNER JOIN field_data_field_topic_per_exam_version echapter on echapter.entity_id = n.nid
    INNER JOIN field_data_field_topic_reference topic on topic.entity_id = echapter.field_topic_per_exam_version_target_id
    WHERE n.type = 'rcpa_chapter'
    AND n.status = 1
    AND topic.field_topic_reference_target_id = :nid",
    array(':nid' => $nid));

  $chapter = null;
  foreach ($result as $record) {
    $chapter = node_load($record->nid);
    break;
  }

  return $chapter;
}
