<?php
/**
 * @file
 * Contains constants class.
 */

/**
 * @file
 * This class represents constants used throughout project.
 */
class MiniorangeSAMLConstants {

  const BASE_URL = 'https://auth.miniorange.com';

}
