<?php
/**
 * @file
 * The RCPARAuthnRequest.php file for the rcpar_saml module. Based on miniorange_saml contrib module.
 *
 */

/**
 * The rcparAuthnRequest class.
 */
class RCPARAuthnRequest {

  /**
   * Initiate the SAML login.
   * @param $acs_url
   * @param array $params
   *  - Additional url parameters may be passed to give context to login request. For example, a jwt token may be passed
   *    to give the identity provider api access
   * @param $issuer
   *  - Base url making the request
   */
  public function initiateLogin($acs_url, $params = array(), $issuer) {
    $nameid_format = variable_get('rcpar_nameid_format', "");
    $sso_url = variable_get('rcpar_saml_idp_login_url', '');
    // Build a query string from our params array
    $query = (!empty($params)) ? '&' . http_build_query($params) : '';
    // Add the query to our relay_state
    $relay_state = ($query != '') ? $_SERVER['HTTP_REFERER'] . '?' . $query : $_SERVER['HTTP_REFERER'];
    $saml_request = Utilities::createAuthnRequest($acs_url, $issuer, $sso_url, $nameid_format);

    if (strpos($sso_url, '?') > 0) {
      $redirect = $sso_url . '&SAMLRequest=' . $saml_request . $query . '&RelayState=' . urlencode($relay_state);
    }
    else {
      $redirect = $sso_url . '?SAMLRequest=' . $saml_request . $query . '&RelayState=' . urlencode($relay_state);
    }
    drupal_goto($redirect);
    exit();
  }

}