<?php
define('DRUPAL_ROOT', dirname(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))))));

require_once DRUPAL_ROOT . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  
global $base_url;	
$site_url = substr($base_url, 0, strpos($base_url, '/sites'));   

$entity_id = $site_url;
$acs_url = $site_url . '/?q=samlassertion'; 
$certificate = file_get_contents( DRUPAL_ROOT . DIRECTORY_SEPARATOR . 'sites' . DIRECTORY_SEPARATOR . 'all' . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'miniorange_saml' . DIRECTORY_SEPARATOR .  'resources' . DIRECTORY_SEPARATOR . 'sp-certificate.crt' );

header('Content-Type: text/xml');
echo '<?xml version="1.0"?>
<md:EntityDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" validUntil="2020-10-28T23:59:59Z" cacheDuration="PT1446808792S" entityID="' . $entity_id . '">
  <md:SPSSODescriptor AuthnRequestsSigned="true" WantAssertionsSigned="true" protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">
    <md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</md:NameIDFormat>
	<md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:transient</md:NameIDFormat>
	<md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified</md:NameIDFormat>
	<md:NameIDFormat>urn:oasis:names:tc:SAML:1.1:nameid-format:persistent</md:NameIDFormat>
	<md:AssertionConsumerService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="' . $acs_url . '" index="1"/>
  </md:SPSSODescriptor>
  <md:Organization>
    <md:OrganizationName xml:lang="en-US">miniOrange</md:OrganizationName>
    <md:OrganizationDisplayName xml:lang="en-US">miniOrange</md:OrganizationDisplayName>
    <md:OrganizationURL xml:lang="en-US">http://miniorange.com</md:OrganizationURL>
  </md:Organization>
  <md:ContactPerson contactType="support">
    <md:GivenName>miniOrange</md:GivenName>
    <md:EmailAddress>info@miniorange.com</md:EmailAddress>
  </md:ContactPerson>
</md:EntityDescriptor>';