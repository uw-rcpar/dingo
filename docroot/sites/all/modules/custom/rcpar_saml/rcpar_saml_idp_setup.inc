<?php
/**
 * @file
 * Contains Service Provider information for rcpar SAML Login Module.
 */

include "includes/MetadataReader.php";

/**
 * Showing IdP Setup form info.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function rcpar_saml_idp_setup($form, &$form_state) {
  global $base_url;

  $form['rcpar_saml_idp_name'] = array(
    '#type' => 'textfield',
    '#title' => t('IdP Name'),
    '#default_value' => variable_get('rcpar_saml_idp_name', ''),
    '#attributes' => array('placeholder' => 'Identity Provider Name like ADFS, SimpleSAML etc.'),
  );

  $form['rcpar_saml_idp_issuer'] = array(
    '#type' => 'textfield',
    '#title' => t('IdP Entity ID or Issuer'),
    '#description' => t('<b>Note :</b> You can find the EntityID in Your IdP-Metadata XML file enclosed in <code>EntityDescriptor</code> tag having attribute<br> as <code>entityID</code>'),
    '#default_value' => variable_get('rcpar_saml_idp_issuer', ''),
    '#attributes' => array('placeholder' => 'Enter IdP Entity ID url or Issuer url '),
  );

  // Login URL
  $form['rcpar_saml_idp_login_url'] = array(
    '#type' => 'textfield',
    '#title' => t('SAML Login URL'),
    '#description' => t('<b>Note :</b> You can find the SAML Login URL in Your IdP-Metadata XML file enclosed in <code>SingleSignOnService</code> tag <br>(Binding type: HTTP-Redirect)'),
    '#default_value' => variable_get('rcpar_saml_idp_login_url', ''),
    '#attributes' => array('placeholder' => 'Single Sign-On Service URL (HTTP-Redirect binding) of your IdP'),
  );

  // Logout URL
  $form['rcpar_saml_idp_logout_url'] = array(
    '#type' => 'textfield',
    '#title' => t('SAML Logout URL'),
    '#description' => t('Logout URL provided by UWorld'),
    '#default_value' => variable_get('rcpar_saml_idp_logout_url', ''),
  );

  $form['rcpar_saml_idp_x509_certificate'] = array(
    '#type' => 'textarea',
    '#title' => t('x.509 Certificate Value'),
    '#cols' => '6',
    '#rows' => '5',
    '#default_value' => variable_get('rcpar_saml_idp_x509_certificate', ''),
    '#attributes' => array('placeholder' => 'Copy and Paste the content from the downloaded certificate or copy the content enclosed in X509Certificate tag (has parent tag KeyDescriptor use=signing) in IdP-Metadata XML file'),
    '#resizable' => False,
  );

  $form['markup_1'] = array(
    '#markup' => '<b>NOTE:</b> Format of the certificate:<br><b>-----BEGIN CERTIFICATE-----<br>XXXXXXXXXXXXXXXXXXXXXXXXXXX<br>-----END CERTIFICATE-----</b><br><br>'
  );

  $form['rcpar_saml_idp_config_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
    '#submit' => array('rcpar_saml_save_idp_config'),
  );

  $form['rcpar_saml_test_config_button'] = array(
    '#markup' => '<a class="btn btn-primary btn-large" style="padding:5px 10px;" id="test-config" data-url="' . rcpar_saml_get_test_url() . '" >'
      . 'Test Configuration</a><br><br>',
  );

  $form['rcpar_saml_test_show_SAML_request_button'] = array(
    '#markup' => '<a class="btn btn-primary btn-large" style="padding:5px 10px;" id="saml-request" data-url="' . rcpar_saml_get_saml_request() . '">'
      . 'Show SAML Request</a>&nbsp',
  );

  $form['rcpar_saml_test_show_SAML_response_button'] = array(
    '#markup' => '&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn btn-primary btn-large" style="padding:5px 10px;" id="saml-response" data-url="' . rcpar_saml_get_saml_resonse() . '">'
      . 'Show SAML Response</a><br><br>',
  );

  return $form;

}

/**
 * Get the test url
 * @return string
 */
function rcpar_saml_get_test_url() {
  global $base_url;
  $testUrl = $base_url . '/?q=testConfig';
  return $testUrl;
}

/**
 * Get the showSAMLrequest url
 * @return string
 */
function rcpar_saml_get_saml_request() {
  global $base_url;
  $SAMLrequestUrl = $base_url . '/?q=showSAMLrequest';
  return $SAMLrequestUrl;
}

/**
 * Get the SAMLresponse url
 * @return string
 */
function rcpar_saml_get_saml_resonse() {
  global $base_url;
  $SAMLresponseUrl = $base_url . '/?q=showSAMLresponse';
  return $SAMLresponseUrl;
}

/**
 * Configure IdP.
 */
function rcpar_saml_save_idp_config($form, &$form_state) {

  global $base_url;
  $idp_name = $form['rcpar_saml_idp_name']['#value'];
  $issuer = $form['rcpar_saml_idp_issuer']['#value'];
  $nameid_format = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified";
  $login_url = $form['rcpar_saml_idp_login_url']['#value'];
  $x509_cert_value = Utilities::sanitize_certificate($form['rcpar_saml_idp_x509_certificate']['#value']);
  $logout_url = $form['rcpar_saml_idp_logout_url']['#value'];
  $enable_login = $form['enable_uworld_configurations']['#value'];
  if (empty($idp_name) || empty($issuer) || empty($login_url)) {
    if (empty($idp_name)) {
      drupal_set_message(t('The <b>IDP Name</b> field is required.'), 'error');
    }
    if (empty($issuer)) {
      drupal_set_message(t('The <b>IdP Entity ID or Issuer</b> field is required.'), 'error');
    }
    if (empty($login_url)) {
      drupal_set_message(t('The <b>SAML Login URL</b> field is required.'), 'error');
    }
    return;
  }
  if ($enable_login == 1) {
    $enable_login = TRUE;
  }
  else {
    $enable_login = FALSE;
  }

  $sp_issuer = $base_url;
  variable_set('rcpar_saml_idp_name', $idp_name);
  variable_set('rcpar_saml_sp_issuer', $sp_issuer);
  variable_set('rcpar_saml_idp_issuer', $issuer);
  variable_set('rcpar_nameid_format', $nameid_format);
  variable_set('rcpar_saml_idp_login_url', $login_url);
  variable_set('rcpar_saml_idp_x509_certificate', $x509_cert_value);
  variable_set('rcpar_saml_idp_logout_url', $logout_url);

  drupal_set_message(t('Identity Provider Configuration successfully saved'));

}
