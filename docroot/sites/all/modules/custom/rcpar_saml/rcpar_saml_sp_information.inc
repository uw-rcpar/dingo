<?php
/**
 * @file
 * Contains Service Provider information for miniOrange SAML Login Module.
 */


/**
 * Showing Service Provider information.
 * @param $form
 * @param $form_state
 * @return mixed
 */
function rcpar_saml_sp_information($form, &$form_state) {
  global $base_url;

  $acs_url = $base_url . '/?q=samlassertion';
  $options = array();

  $options[0] = array(
    'attribute' => t('Issuer'),
    'value' => $base_url,
  );

  $options[1] = array(
    'attribute' => t('ACS URL'),
    'value' => $acs_url,
  );

  $options[2] = array(
    'attribute' => t('Audience URI'),
    'value' => $base_url,
  );

  $options[3] = array(
    'attribute' => t('Recipient URL'),
    'value' => $acs_url,
  );

  $options[4] = array(
    'attribute' => t('Destination URL'),
    'value' => $acs_url,
  );

  $options[5] = array(
    'attribute' => t('NameID Format'),
    'value' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
  );

  $header = array(
    'attribute' => array('data' => t('Attribute')),
    'value' => array('data' => t('Value')),
  );

  $form['fieldset']['spinfo'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
  );

  return $form;
}
