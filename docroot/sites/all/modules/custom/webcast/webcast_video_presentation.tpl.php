<style>
  #video_container {
    float: left;
    border: 1px solid red;
    width: 400px;
  }

  #slides {
    float: right;
    width: 400px;
    border: 1px solid red;
  }

  #presentation_container {
    margin-left: auto;
    margin-right: auto;
    width: 1000px;
  }

</style>

<?php $close = ' />'; ?>


<div id="presentation_container">
  <div id="video_container">
    <video width="400" id="video" controls>
      <source src="<?php print $video_src; ?>"/>
    </video>
  </div>
  <div id="slides">Slides go here</div>
  <div style="clear: both"></div>
</div>
