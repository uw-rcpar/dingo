<?php if ($presentation == 0 && $active == TRUE && $expired == FALSE): ?>
  <!--- show stand alone player --->
  <style>

    .center_video {
      margin: 0 auto;
      width: 1000px;
    }

  </style>

  <video class="center_video" width="600" id="video" controls>
    <source src="<?php print $video_src; ?>" type="video/mp4"/>
  </video>


<?php elseif ($presentation == 1 && $active == TRUE): ?>
  <!--- display presentation player and slides --->
  <style>
    #video_container {
      float: left;
      width: 470px;
    }

    #slides {
      float: right;
      width: 470px;

    }

    #presentation_container {
      margin-left: auto;
      margin-right: auto;
      width: 960px;
    }

  </style>

  <script type="text/javascript" src="http://popcornjs.org/code/dist/popcorn-complete.min.js"></script>
  <script>
    document.addEventListener("DOMContentLoaded", function () {

      var pop = Popcorn('#video', {
        pauseOnLinkClicked: true
      });

      //presentation image data passed to json object
      var pres_images = [
        <?php print $popcorn_data; ?>
      ];
      // Loop through the images
      pres_images.forEach(function (pres_image) {
        // Call setCountry() at the start of each country's start time.
        pop.image({
          start: pres_image.start,
          end: pres_image.end,
          target: 'slides',
          src: pres_image.src
          //onStart: function() { setCountry(country.country_name);
        });
      });
    });


  </script>
  <div id="presentation_container">
    <div id="video_container">
      <video width="470" id="video" controls>
        <source src="<?php print $video_src; ?>" type="video/mp4"/>
      </video>
    </div>
    <div id="slides"></div>
    <div style="clear: both"></div>
  </div>

<?php elseif ($active == FALSE && $expired == FALSE): ?>
  <!--- Display the comming soon message --->
  <h3>This webcast will be available on <?php print date_format(date_create($start_date), 'D, M j, Y'); ?>.</h3>
  <p>Please check back on that date to view the webcast in full.</p>

<?php elseif ($expired == TRUE): ?>
  <!--- Display the comming soon message --->
  <h3>This webcast expired on <?php print date_format(date_create($start_date), 'D, M j, Y'); ?>.</h3>
  <p>Check back soon to catch our next free webcast, or check out our
    <a href="/cpa-exam-learning-center">CPA Exam Learning Center</a> for more video resources.</p>

<?php endif; ?>
