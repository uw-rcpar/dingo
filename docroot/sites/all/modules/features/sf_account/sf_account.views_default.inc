<?php
/**
 * @file
 * sf_account.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sf_account_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'clone_of_salesforce_accounts';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_sf_account';
  $view->human_name = 'SalesForce Accounts';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'SalesForce Accounts';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    13 => '13',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'input_required' => 0,
      'text_input_required' => array(
        'text_input_required' => array(
          'value' => 'Select any filter and click on Apply to see results',
          'format' => 'filtered_html',
        ),
      ),
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'secondary_collapse_override' => '0',
    ),
    'title' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'field_account_type_value' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id' => 'id',
    'title' => 'title',
    'field_account_type' => 'field_account_type',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_account_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<a href="/admin/structure/entity-type/sf_account/sf_account/add" target="_blank">Add New Account</a>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  /* Field: SF Account: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_sf_account';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Field: SF Account: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_sf_account';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Field: SF Account: Account Type */
  $handler->display->display_options['fields']['field_account_type']['id'] = 'field_account_type';
  $handler->display->display_options['fields']['field_account_type']['table'] = 'field_data_field_account_type';
  $handler->display->display_options['fields']['field_account_type']['field'] = 'field_account_type';
  /* Field: SF Account: Account Tier */
  $handler->display->display_options['fields']['field_account_tier']['id'] = 'field_account_tier';
  $handler->display->display_options['fields']['field_account_tier']['table'] = 'field_data_field_account_tier';
  $handler->display->display_options['fields']['field_account_tier']['field'] = 'field_account_tier';
  $handler->display->display_options['fields']['field_account_tier']['label'] = 'Tier';
  /* Field: SF Account: UnitID */
  $handler->display->display_options['fields']['field_unitid']['id'] = 'field_unitid';
  $handler->display->display_options['fields']['field_unitid']['table'] = 'field_data_field_unitid';
  $handler->display->display_options['fields']['field_unitid']['field'] = 'field_unitid';
  $handler->display->display_options['fields']['field_unitid']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Field: FICE Code */
  $handler->display->display_options['fields']['field_fice_code']['id'] = 'field_fice_code';
  $handler->display->display_options['fields']['field_fice_code']['table'] = 'field_data_field_fice_code';
  $handler->display->display_options['fields']['field_fice_code']['field'] = 'field_fice_code';
  $handler->display->display_options['fields']['field_fice_code']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Field: OPEID */
  $handler->display->display_options['fields']['field_opeid']['id'] = 'field_opeid';
  $handler->display->display_options['fields']['field_opeid']['table'] = 'field_data_field_opeid';
  $handler->display->display_options['fields']['field_opeid']['field'] = 'field_opeid';
  $handler->display->display_options['fields']['field_opeid']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Field: State */
  $handler->display->display_options['fields']['field_college_state_list']['id'] = 'field_college_state_list';
  $handler->display->display_options['fields']['field_college_state_list']['table'] = 'field_data_field_college_state_list';
  $handler->display->display_options['fields']['field_college_state_list']['field'] = 'field_college_state_list';
  /* Field: SF Account: College Taxonomy Term */
  $handler->display->display_options['fields']['field_college_taxonomy_term']['id'] = 'field_college_taxonomy_term';
  $handler->display->display_options['fields']['field_college_taxonomy_term']['table'] = 'field_data_field_college_taxonomy_term';
  $handler->display->display_options['fields']['field_college_taxonomy_term']['field'] = 'field_college_taxonomy_term';
  /* Field: SF Account: Aliases */
  $handler->display->display_options['fields']['field_aliases']['id'] = 'field_aliases';
  $handler->display->display_options['fields']['field_aliases']['table'] = 'field_data_field_aliases';
  $handler->display->display_options['fields']['field_aliases']['field'] = 'field_aliases';
  $handler->display->display_options['fields']['field_aliases']['delta_offset'] = '0';
  /* Field: SF Account: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'eck_sf_account';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  /* Field: SF Account: Delete link */
  $handler->display->display_options['fields']['delete_link']['id'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['table'] = 'eck_sf_account';
  $handler->display->display_options['fields']['delete_link']['field'] = 'delete_link';
  /* Field: Bulk operations: SF Account */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_sf_account';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'skip_permission_check' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 0,
        'display_values' => array(
          'sf_account::field_approved' => 'sf_account::field_approved',
        ),
      ),
    ),
  );
  /* Filter criterion: SF Account: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'eck_sf_account';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
  );
  /* Filter criterion: SF Account: Account Type (field_account_type) */
  $handler->display->display_options['filters']['field_account_type_value']['id'] = 'field_account_type_value';
  $handler->display->display_options['filters']['field_account_type_value']['table'] = 'field_data_field_account_type';
  $handler->display->display_options['filters']['field_account_type_value']['field'] = 'field_account_type_value';
  $handler->display->display_options['filters']['field_account_type_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_account_type_value']['expose']['operator_id'] = 'field_account_type_value_op';
  $handler->display->display_options['filters']['field_account_type_value']['expose']['label'] = 'Account Type';
  $handler->display->display_options['filters']['field_account_type_value']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_account_type_value']['expose']['operator'] = 'field_account_type_value_op';
  $handler->display->display_options['filters']['field_account_type_value']['expose']['identifier'] = 'field_account_type_value';
  $handler->display->display_options['filters']['field_account_type_value']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_account_type_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    21 => 0,
  );
  /* Filter criterion: Field: FICE Code (field_fice_code) */
  $handler->display->display_options['filters']['field_fice_code_value']['id'] = 'field_fice_code_value';
  $handler->display->display_options['filters']['field_fice_code_value']['table'] = 'field_data_field_fice_code';
  $handler->display->display_options['filters']['field_fice_code_value']['field'] = 'field_fice_code_value';
  $handler->display->display_options['filters']['field_fice_code_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_fice_code_value']['expose']['operator_id'] = 'field_fice_code_value_op';
  $handler->display->display_options['filters']['field_fice_code_value']['expose']['label'] = 'FICE Code';
  $handler->display->display_options['filters']['field_fice_code_value']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_fice_code_value']['expose']['operator'] = 'field_fice_code_value_op';
  $handler->display->display_options['filters']['field_fice_code_value']['expose']['identifier'] = 'field_fice_code_value';
  $handler->display->display_options['filters']['field_fice_code_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    21 => 0,
  );
  /* Filter criterion: Field: OPEID (field_opeid) */
  $handler->display->display_options['filters']['field_opeid_value']['id'] = 'field_opeid_value';
  $handler->display->display_options['filters']['field_opeid_value']['table'] = 'field_data_field_opeid';
  $handler->display->display_options['filters']['field_opeid_value']['field'] = 'field_opeid_value';
  $handler->display->display_options['filters']['field_opeid_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_opeid_value']['expose']['operator_id'] = 'field_opeid_value_op';
  $handler->display->display_options['filters']['field_opeid_value']['expose']['label'] = 'OPEID';
  $handler->display->display_options['filters']['field_opeid_value']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_opeid_value']['expose']['operator'] = 'field_opeid_value_op';
  $handler->display->display_options['filters']['field_opeid_value']['expose']['identifier'] = 'field_opeid_value';
  $handler->display->display_options['filters']['field_opeid_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    21 => 0,
  );
  /* Filter criterion: Field: State (field_college_state_list) */
  $handler->display->display_options['filters']['field_college_state_list_value']['id'] = 'field_college_state_list_value';
  $handler->display->display_options['filters']['field_college_state_list_value']['table'] = 'field_data_field_college_state_list';
  $handler->display->display_options['filters']['field_college_state_list_value']['field'] = 'field_college_state_list_value';
  $handler->display->display_options['filters']['field_college_state_list_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_college_state_list_value']['expose']['operator_id'] = 'field_college_state_list_value_op';
  $handler->display->display_options['filters']['field_college_state_list_value']['expose']['label'] = 'State';
  $handler->display->display_options['filters']['field_college_state_list_value']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_college_state_list_value']['expose']['operator'] = 'field_college_state_list_value_op';
  $handler->display->display_options['filters']['field_college_state_list_value']['expose']['identifier'] = 'field_college_state_list_value';
  $handler->display->display_options['filters']['field_college_state_list_value']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_college_state_list_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    21 => 0,
  );
  /* Filter criterion: SF Account: Aliases (field_aliases) */
  $handler->display->display_options['filters']['field_aliases_value']['id'] = 'field_aliases_value';
  $handler->display->display_options['filters']['field_aliases_value']['table'] = 'field_data_field_aliases';
  $handler->display->display_options['filters']['field_aliases_value']['field'] = 'field_aliases_value';
  $handler->display->display_options['filters']['field_aliases_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_aliases_value']['expose']['operator_id'] = 'field_aliases_value_op';
  $handler->display->display_options['filters']['field_aliases_value']['expose']['label'] = 'Aliases';
  $handler->display->display_options['filters']['field_aliases_value']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_aliases_value']['expose']['operator'] = 'field_aliases_value_op';
  $handler->display->display_options['filters']['field_aliases_value']['expose']['identifier'] = 'field_aliases_value';
  $handler->display->display_options['filters']['field_aliases_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    21 => 0,
  );
  /* Filter criterion: SF Account: College Taxonomy Term (field_college_taxonomy_term) */
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['id'] = 'field_college_taxonomy_term_tid';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['table'] = 'field_data_field_college_taxonomy_term';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['field'] = 'field_college_taxonomy_term_tid';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['value'] = '';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['expose']['operator_id'] = 'field_college_taxonomy_term_tid_op';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['expose']['label'] = 'College Taxonomy Term';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['expose']['operator'] = 'field_college_taxonomy_term_tid_op';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['expose']['identifier'] = 'field_college_taxonomy_term_tid';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    21 => 0,
  );
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['vocabulary'] = 'state_college_selection';
  $handler->display->display_options['filters']['field_college_taxonomy_term_tid']['hierarchy'] = 1;
  /* Filter criterion: SF Account: UnitID (field_unitid) */
  $handler->display->display_options['filters']['field_unitid_value']['id'] = 'field_unitid_value';
  $handler->display->display_options['filters']['field_unitid_value']['table'] = 'field_data_field_unitid';
  $handler->display->display_options['filters']['field_unitid_value']['field'] = 'field_unitid_value';
  $handler->display->display_options['filters']['field_unitid_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_unitid_value']['expose']['operator_id'] = 'field_unitid_value_op';
  $handler->display->display_options['filters']['field_unitid_value']['expose']['label'] = 'UnitID';
  $handler->display->display_options['filters']['field_unitid_value']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['field_unitid_value']['expose']['operator'] = 'field_unitid_value_op';
  $handler->display->display_options['filters']['field_unitid_value']['expose']['identifier'] = 'field_unitid_value';
  $handler->display->display_options['filters']['field_unitid_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
    16 => 0,
    21 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/rcpar/salesforce-accounts';
  $export['clone_of_salesforce_accounts'] = $view;

  return $export;
}
