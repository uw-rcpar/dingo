<?php
/**
 * @file
 * sf_account.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function sf_account_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_state_college_selection_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_state_college_selection_pattern'] = $strongarm;

  return $export;
}
