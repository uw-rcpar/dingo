<?php


/**
 * Form builder in the callback for admin/settings/RCPAR/salesforce/import-missing-firms
 */
function sf_account_missing_firms_form($form, &$form_state) {
  $form['limit'] = array(
    '#type' => 'textfield',
    '#title' => 'Import limit count',
    '#description' => 'Only perform this number of imports'
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Import University Master List',
  );

  return $form;
}

/**
 * Form submit handler for sf_account_import_univ_list_form().
 * Imports universities from the master list .csv into the database.
 */
function sf_account_missing_firms_form_submit(&$form, &$form_state) {
  // Load up the file
  if (($handler = fopen(drupal_get_path('module', 'sf_account') . "/missing-firms.csv", "r")) === FALSE) {
    drupal_set_message("Failed to open file.", 'error');
    return '';
  }

  try {
    // Get the header of the CSV and find column indexes for columns we need.
    $header = fgetcsv($handler, 4000, ",");
    $indexes = array(
      'name' => array_search('Name', $header),
      'type' => array_search('RecordTypeId', $header),
      'sfid' => array_search('Id', $header),
      'tier' => array_search('Account_Tier__c', $header),
      // @todo - not sure if the state field we have in Drupal is the same state here, but we have to import it if we
      // have it, otherwise a Drupal save will erase the value in SF
      'state' => array_search('BillingState', $header),
    );

    // Throw an error if any of the indexes weren't found.
    foreach ($indexes as $index_name => $index) {
      if ($index === FALSE) {
        throw new Exception("Failed to find column index: $index_name");
      }
    }

    // Initialize some counters for debugging and user feedback
    $counters = [
      'row' => 0,
      'new' => 0,
      'existing' => 0,
      'fail' => 0,
      'newids' => [],
      'existingids' => [],
      'faildata' => []
    ];

    // Load each line of the CSV
    while (($line_data = fgetcsv($handler, 4000, ",")) !== FALSE) {
      // Get all of the data from this row that we care about
      $data = [];
      foreach ($indexes as $index_name => $index) {
        $data[$index_name] = trim($line_data[$index]);
      }

      // Check to see if we already have a mapping for this account. If we do, we don't need to import it.
      $hasMapping = db_query(
        "SELECT count(*) FROM salesforce_mapping_object WHERE salesforce_id = :sfid",
        [':sfid' => $data['sfid']]
      )->fetchField();
      if($hasMapping) {
        continue;
      }

      // Skip blank/erroneous rows
      if (empty($data['name'])) {
        continue;
      }

      // Save data for this row
      try {
        // Track our row count
        $counters['row']++;

        // Check if there is a limit on the number of imports specified, and respect it.
        if(!empty($form_state['values']['limit'])) {
          if($counters['row'] > $form_state['values']['limit']) {
            return;
          }
        }

        // Insert a record into eck_sf_account. We can't use entity save because that will cause a new account to
        // be synced up to Salesforce.
        $record = array(
          'type' => 'sf_account',
          'title' => $data['name'],
        );
        drupal_write_record('eck_sf_account', $record);

        // Now that we have the ID of the new entity, we can manually add some mapping table records
        /*$mapObject = array(
          'salesforce_id' => $data['sfid'],
          'entity_id' => $record['id'],
          'entity_type' => 'sf_account',
          'created' => REQUEST_TIME,
          'entity_updated' => REQUEST_TIME,
          'last_sync' => 0,
          'last_sync_action' => 'pull',
          'last_sync_message' => '',
        );
        drupal_write_record('salesforce_mapping_object', $mapObject);*/

        // Create mapping object, saved below.
        $mapping_object = entity_create('salesforce_mapping_object', array(
          'entity_id' => $record['id'],
          'entity_type' => 'sf_account',
          'salesforce_id' => $data['sfid'],
          'last_sync_message' => t('Mapping object created via !function.', array('!function' => __FUNCTION__)),
          'last_sync_status' => SALESFORCE_MAPPING_STATUS_SUCCESS,
          'last_sync_action' => 'push',
          'last_sync' => 0,
        ));
        $mapping_object->save();


        // Set the revision to match the new primary key and write it back to the DB
        //$mapObject['revision_id'] = $mapObject['salesforce_mapping_object_id'];
        //drupal_write_record('salesforce_mapping_object', $mapObject, array('salesforce_mapping_object_id'));

        // Now, load the Drupal entity and put in field data
        $sf_account = entity_load_single('sf_account', $record['id']);
        $wrapper = entity_metadata_wrapper('sf_account', $sf_account);
        $wrapper->field_account_type->set($data['type']);
        $wrapper->field_account_tier->set($data['tier']);
        $wrapper->field_college_state_list->set($data['state']);
        $wrapper->save();

        $counters['new']++;
        $counters['newids'][] = $wrapper->getIdentifier();
      }
      catch (Exception $e) {
        drupal_set_message('Error encountered on row: ' . $counters['row'] . '<br>' . $e->getMessage());
        $counters['fail']++;
        $counters['faildata'][] = print_r('<pre>' . $data . '</pre>', TRUE);
      }
    }
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
  finally {
    // Make sure our file handler always gets closed
    fclose($handler);

    // Print counters
    $message = "
      <h2>Counters</h2>
      Total rows: {$counters['row']} <br>
      New entities: {$counters['new']} <br>
      Updated entities: {$counters['existing']} <br>
      Failures: {$counters['fail']} <br>
      Failures (data): " . implode('<hr>', $counters['faildata']) . "
    ";
    drupal_set_message($message);

    // Print links to new entities
    $message = '<h2>New Accounts</h2>';
    foreach ($counters['newids'] as $id) {
      $message .= l($id, "sf_account/sf_account/$id") . '<br>';
    }
    drupal_set_message($message);

    // Print links to updated entities
    $message = '<h2>Accounts Updated</h2>';
    foreach ($counters['existingids'] as $id) {
      $message .= l($id, "sf_account/sf_account/$id") . '<br>';
    }
    drupal_set_message($message);
  }
}
