<?php

/**
 * Menu callback for /admin/settings/RCPAR/salesforce/univcleanup2
 * Convert data entered in field_college_name to entity references where possible.
 */
function sf_account_cleanup2_form($form, &$form_state) {
  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Execute Batch',
  );

  return $form;
}

/**
 * Form callback for sf_account_cleanup2_form
 */
function sf_account_cleanup2_form_submit($form, &$form_state) {
  sf_account_cleanup2();
}

/**
 * The batch callback. Build batch operations.
 */
function sf_account_cleanup2() {
  set_time_limit(0);
  $batch = array(
    'operations' => array(),
    'finished' => 'sf_account_cleanup2_finished',
    'title' => t('College name cleanup part 2'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'sf_account') . '/sf_account.cleanup2.batch.inc'
  );

  // Load up the file
  if (($handler = fopen(drupal_get_path('module', 'sf_account') . "/univ-master-list.csv", "r")) === FALSE) {
    drupal_set_message("Failed to open file.", 'error');
    return '';
  }

  try {
    // Get the header of the CSV and find column indexes for columns we need.
    $header = fgetcsv($handler, 4000, ",");
    $indexes = array(
      'fice' => array_search('FICE code', $header),
      'opeid' => array_search('OPEID Number', $header),
      'taxonomy' => array_search('Taxonomy ID', $header),
      'sfid' => array_search('Salesforce ID', $header),
      'unitid' => array_search('UnitID', $header),
      'name' => array_search('Institution Name ST', $header),
      'state' => array_search('State', $header),
      'acctprogram' => array_search('Accounting Program', $header),
      'alias' => array_search('Institution Alias', $header),
    );

    // Throw an error if any of the indexes weren't found.
    foreach ($indexes as $index_name => $index) {
      if ($index === FALSE) {
        throw new Exception("Failed to find column index: $index_name");
      }
    }

    // Load each line of the CSV
    while (($line_data = fgetcsv($handler, 4000, ",")) !== FALSE) {
      // Get all of the data from this row that we care about
      $data = [];
      foreach ($indexes as $index_name => $index) {
        $data[$index_name] = trim($line_data[$index]);
      }

      // Skip blank/erroneous rows
      if (empty($data['name'])) {
        continue;
      }

      // We're only looking at universities without an account program right now
      if ($data['acctprogram'] != 'No') {
        continue;
      }

      // If we have a related taxonomy, let's see how many people have enrolled with that taxonomy
      if (!empty($data['taxonomy'])) {
        $term = taxonomy_term_load($data['taxonomy']);

        // If it's not a valid term, we can't do anything
        if (!$term) {
          continue;
        }

        // Get uids for anyone who has this term ID selected as their college (and don't have a sf_account reference
        // set for their college)
        $uids1 = db_query("
          SELECT c.entity_id FROM field_data_field_college_state c
          LEFT JOIN field_data_field_college_sf_account sf ON sf.entity_type = c.entity_type AND sf.entity_id = c.entity_id
          WHERE c.entity_type = 'user' AND c.field_college_state_tid = :tid
          AND field_college_sf_account_target_id IS NULL",
          [':tid' => $data['taxonomy']]
        )->fetchCol();

        // Next, we'll look for users who entered this university name into the college name field (or somehow wound
        // up with it there because of the way the latest widget code worked) and don't have a sf_account reference
        // set for their college.
        $uids2 = db_query("
          SELECT c.entity_id FROM field_data_field_college_name c
          LEFT JOIN field_data_field_college_sf_account sf ON sf.entity_type = c.entity_type AND sf.entity_id = c.entity_id
          WHERE c.entity_type = 'user' 
          AND (c.field_college_name_value = :name1 OR c.field_college_name_value = :name2)
          AND sf.field_college_sf_account_target_id IS NULL",
          [':name1' => $term->name, ':name2' => $term->name . " ({$term->name})"]
        )->fetchCol();

        // Merge these together to make a single set of unique IDs
        $uids = array_unique(array_merge($uids1, $uids2));

        if (count($uids) < variable_get('sf_account_no_actprgm_threshold', 10)) {
          // This doesn't meet the threshold, we need to skip
          continue;
        }

        // This meets the threshold - we'll make a batch operation for it
        $batch['operations'][] = array('sf_account_cleanup2_process', array($data, $uids));
      }
      else {
        // If the row doesn't have a related taxonomy term, it's still possible that people have entered its name
        // into the college name field, but it's highly unlikely that we'll find exact matches. This is the best
        // we can do for now.
      }
    }
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage() . '<br>' . print_r($data, TRUE), 'error');
  }
  finally {
    // Make sure our file handler always gets closed
    fclose($handler);

    // Set up the batch.
    batch_set($batch);
    batch_process('admin/settings/RCPAR/salesforce/univcleanup2'); // The path to redirect to when done.
  }
}


/**
 * Process an individual university record from the spreadsheet.
 *
 * @param array $data
 * - Data from the CSV document
 * @param int[] $uids
 * - An array of user ids
 * @param $context
 * - Batch data
 */
function sf_account_cleanup2_process($data, $uids, &$context) {
  set_time_limit(0);
  if (empty($context['results'])) {
    // Initialize some counters for debugging and user feedback
    $counters = [
      'row' => 0,
      'new' => 0,
      'existing' => 0,
      'fail' => 0,
      'newids' => [],
      'existingids' => [],
      'uids' => [],
      'uids_failed' => [],
    ];
  }
  else {
    $counters = $context['results'];
  }

  // Create (or update) an sf_account from this row. Updating will only happen if we are resuming.
  $sf_account_wrapper = sf_account_set_data_from_row($data, $counters);

  // If we failed to create or update the university, we have to bail.
  if (!$sf_account_wrapper) {
    $context['results'] = $counters;
    return;
  }

  // Now that we have the entity saved, we can associate users with it
  foreach ($uids as $uid) {
    try {
      $user = user_load($uid);
      $wrapper = entity_metadata_wrapper('user', $user);// Clear the college name field
      $wrapper->field_college_name->set('');// Store the sf account reference
      $wrapper->field_college_sf_account->set($sf_account_wrapper->getIdentifier());
      $wrapper->save();// Add to counters
      $counters['uids'][] = $uid;
    }
    catch (Exception $e) {
      $counters['uids_failed'][] = $uid;
    }
  }

  $context['results'] = $counters;
}

/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function sf_account_cleanup2_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Batch completed!');

    $counters = $results;

    // Print counters
    $message = "
      <h2>Counters</h2>
      Total rows: {$counters['row']} <br>
      New entities: {$counters['new']} <br>
      Updated entities: {$counters['existing']} <br>
      Failures: {$counters['fail']} <br>
    ";
    drupal_set_message($message);

    // Print links to new entities
    $message = '<h2>New Accounts</h2>';
    foreach ($counters['newids'] as $id) {
      $message .= l($id, "sf_account/sf_account/$id") . '<br>';
    }
    drupal_set_message($message);

    // Print links to updated entities
    $message = '<h2>Accounts Updated</h2>';
    foreach ($counters['existingids'] as $id) {
      $message .= l($id, "sf_account/sf_account/$id") . '<br>';
    }
    drupal_set_message($message);

    // Print links to updated users
    $message = '<h2>User Accounts Updated</h2>';
    foreach ($counters['uids'] as $id) {
      $message .= l($id, "user/$id/edit") . '<br>';
    }
    drupal_set_message($message);

    // Print links to users that failed to update
    $message = '<h2>User Accounts FAILED to update:</h2>';
    foreach ($counters['uids'] as $id) {
      $message .= l($id, "user/$id/edit") . '<br>';
    }
    drupal_set_message($message, 'error');


  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}