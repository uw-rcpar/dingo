<?php


/**
 * Menu callback for /admin/settings/RCPAR/salesforce/userconvert
 * Convert stored university data on user accounts from taxonomy to sf_account entity references
 */
function sf_account_user_batch_form($form, &$form_state) {
  $form['resumeat'] = array(
    '#title' => 'Resume the batch at the given UID and above.',
    '#description' => 'Leave blank to start from the beginning.',
    '#type' => 'textfield',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Execute Batch',
  );

  return $form;
}

/**
 * Form callback for sf_account_user_batch_form
 */
function sf_account_user_batch_form_submit($form, &$form_state) {
  $resumeat = 0;
  if(!empty($form_state['values']['resumeat'])) {
    $resumeat = $form_state['values']['resumeat'];
  }
  sf_account_user_batch($resumeat);
}

/**
 * The batch callback. Build batch operations.
 */
function sf_account_user_batch($resumeat = 0) {
  $batch = array(
    'operations' => array(),
    'finished' => 'sf_account_user_batch_finished',
    'title' => t('SF Account User Convert'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'sf_account') . '/includes/sf_account.user.batch.inc'
  );

  // Get UID of all users with field_college_state or field_college_name supplied
  $uids1 = db_query("SELECT DISTINCT entity_id AS uid FROM field_data_field_college_state WHERE entity_type = 'user'")->fetchCol();
  $uids2 = db_query("SELECT DISTINCT entity_id AS uid FROM field_data_field_college_name WHERE entity_type = 'user'")->fetchCol();

  $uids = array_unique(array_merge($uids1, $uids2));
  sort($uids);

  foreach ($uids as $uid) {
    if($uid > $resumeat) {
      $batch['operations'][] = array('sf_account_user_batch_process', array($uid));
    }
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/RCPAR/salesforce/userconvert'); // The path to redirect to when done.
  }
}

/**
 * Process an individual user and convert their taxonomy term university reference to entity reference.
 * @param int $uid
 * - The user id to process.
 */
function sf_account_user_batch_process($uid, &$context) {
  $user = user_load($uid);
  $wrapper = entity_metadata_wrapper('user', $user);

  // Determine if we've made a change that needs saving
  $changed = FALSE;

  // Try to extract a taxonomy term
  $tid = $user->field_college_state[LANGUAGE_NONE][0]['tid'];
  if(!empty($tid)) {
    $sf_account = sf_account_load_by_termid($tid);
    if($sf_account) {
      $wrapper->field_college_sf_account->set($sf_account->id);
      $changed = TRUE;

      // If we were able to extract a taxonomy term, we want to clear the plain text field that stores university,
      // because this is only supposed to be used when the term is missing.
      $wrapper->field_college_name->set('');
    }
  }

  if($changed) {
    $wrapper->save();
  }

  // Store UIDs that were altered
  $context['results']['uids'][] = $user->uid;
}


/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function sf_account_user_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Batch completed! Processed ' . count($results['uids']) . ' users.');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}