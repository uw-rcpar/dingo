<?php


/**
 * Menu callback for /admin/settings/RCPAR/salesforce/collegename
 * Convert data entered in field_college_name to entity references where possible.
 */
function sf_account_collegename_batch_form($form, &$form_state) {
  $form['resumeat'] = array(
    '#title' => 'Resume the batch at the given UID and above.',
    '#description' => 'Leave blank to start from the beginning.',
    '#type' => 'textfield',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Execute Batch',
  );

  return $form;
}

/**
 * Form callback for sf_account_collegename_batch_form
 */
function sf_account_collegename_batch_form_submit($form, &$form_state) {
  $resumeat = 0;
  if (!empty($form_state['values']['resumeat'])) {
    $resumeat = $form_state['values']['resumeat'];
  }

  sf_account_collegename_batch($resumeat);
}

/**
 * The batch callback. Build batch operations.
 */
function sf_account_collegename_batch($resumeat = 0) {
  $batch = array(
    'operations' => array(),
    'finished' => 'sf_account_collegename_batch_finished',
    'title' => t('College name to entity ref convert'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'sf_account') . '/sf_account.collegename.batch.inc'
  );

  // Get a list of user IDs where there is some non-empty value in field_college_name
  $uids = db_query("
    SELECT entity_id FROM field_data_field_college_name 
    WHERE entity_type = 'user' AND field_college_name_value != '' AND entity_id >= :res 
    ORDER BY entity_id ASC", [':res' => $resumeat])->fetchCol();

  // Set up batches of x number of users per operation
  foreach (array_chunk($uids, variable_get('collegename_batch_size', 100)) as $uids_chunk) {
    $batch['operations'][] = array('sf_account_collegename_batch_process', array($uids_chunk));
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/RCPAR/salesforce/collegename'); // The path to redirect to when done.
  }
}


function sf_account_collegename_batch_process($uids, &$context) {
  foreach ($uids as $uid) {
    sf_account_collegename_batch_process_uid($uid, $context);
  }
}

/**
 * Process an individual user and convert their field_college_name into a sf_account university reference.
 * @param int $uid
 * - The user id to process.
 */
function sf_account_collegename_batch_process_uid($uid, &$context) {
  // Get the college name straight from the field table
  $college_name = db_query(
    "SELECT field_college_name_value FROM field_data_field_college_name WHERE entity_type = 'user' AND entity_id = :uid",
    [':uid' => $uid]
  )->fetchField();

  // Sometimes the taxonomy term ID is in the name within parentheses. For example: "NORTH GEORGIA COLLEGE (12011)"
  // answers of the users came in this form
  if (preg_match('/.*\(([0-9]*)\)/', $college_name, $matches)) {
    $tid = $matches[1];
  }
  // See if we can do a straight across match with a university taxonomy term
  else {
    $tid = db_query("SELECT tid FROM taxonomy_term_data WHERE vid = 6 AND name = :cn", [':cn' => $college_name])->fetchField();
  }

  // If we didn't get a term ID at this point, we've failed
  if (!$tid) {
    return;
  }

  // Try to get a matching sf_account
  $sf_account = sf_account_load_by_termid($tid);

  // If we don't have one, we've failed.
  if (!$sf_account) {
    return;
  }

  // If we're here, we have a loaded sf_account that we can associate with the user
  $user = user_load($uid);
  $wrapper = entity_metadata_wrapper('user', $user);
  $wrapper->field_college_sf_account->set($sf_account->id);

  // If we were able to extract a reference, we want to clear the plain text field that stores university,
  // because this is only supposed to be used when the reference is missing.
  $wrapper->field_college_name->set('');

  $wrapper->save();

  // Store UIDs that were altered
  $context['results']['uids'][] = $user->uid;
}


/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function sf_account_collegename_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Batch completed! Processed ' . count($results['uids']) . ' users.');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}