<?php

/**
 * Menu callback for /admin/settings/RCPAR/salesforce/resync-users
 * Push all existing users to Salesforces
 */
function sf_account_resync_users_batch_form($form, &$form_state) {
  $form['resumeat'] = array(
    '#title' => 'Resume the batch at the given UID and above.',
    '#description' => 'Leave blank to start from the beginning.<br>' . 
    'The last UID marked as processed was: ' . variable_get('sf_account_resync_users_resume', 0),
    '#type' => 'textfield',
  );

  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Execute Batch',
  );

  return $form;
}

/**
 * Form callback for sf_account_resync_users_batch_form
 * Initiates the batch job.
 */
function sf_account_resync_users_batch_form_submit($form, &$form_state) {
  $resumeat = 0;
  if (!empty($form_state['values']['resumeat'])) {
    $resumeat = $form_state['values']['resumeat'];
  }

  sf_account_resync_users_batch($resumeat);
}

/**
 * The batch callback. Build batch operations.
 * @param int $resumeat
 * - A user ID to start from. Allows us to resume a previously failed job.
 */
function sf_account_resync_users_batch($resumeat = 0) {
  $batch = array(
    'operations' => array(),
    'finished' => 'sf_account_resync_users_batch_finished',
    'title' => t('Re-sync users to SalesForce'),
    'init_message' => t('Processing...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Something went wrong.'),
    'file' => drupal_get_path('module', 'sf_account') . '/sf_account.resyncusers.batch.inc'
  );

  // Get a list of user IDs where there is some non-empty value in field_college_name
  $uids = db_query("
    SELECT uid FROM users
    WHERE uid >= :res
    ORDER BY uid ASC", [':res' => $resumeat])->fetchCol();

  // Set up batches of x number of users per operation
  foreach (array_chunk($uids, variable_get('sf_user_resync_batch_size', 100)) as $uids_chunk) {
    $batch['operations'][] = array('sf_account_resync_users_batch_process', array($uids_chunk));
  }

  batch_set($batch);
  // If running from command line do some backend drush voodoo
  if (drupal_is_cli()) {
    $batch['progressive'] = FALSE;
    // Process the batch with custom drush command.
    drush_backend_batch_process();
  }
  else {
    batch_process('admin/settings/RCPAR/salesforce/resync-users'); // The path to redirect to when done.
  }
}


/**
 * Batch operation callback.
 * @param int[] $uids
 * - Array of user ids to process.
 * @param $context
 * - The system's batch information.
 */
function sf_account_resync_users_batch_process($uids, &$context) {
  foreach ($uids as $uid) {
    sf_account_resync_users_batch_process_uid($uid, $context);
  }
  
  // Save our resume-at marker at the last processed uid
  variable_set('sf_account_resync_users_resume', $uid);
}

/**
 * Process an individual user and convert their field_college_name into a sf_account university reference.
 * @param int $uid
 * - The user id to process.
 */
function sf_account_resync_users_batch_process_uid($uid, &$context) {
  try {
    // Load the user - throw an exception if we can't
    $user = user_load($uid);
    if (!$user) {
      throw new Exception('User could not be loaded');
    }

    // Sync to Salesforce
    if(function_exists('salesforce_push_entity_crud')) {
      salesforce_push_entity_crud('user', $user, SALESFORCE_MAPPING_SYNC_DRUPAL_UPDATE);
    }
    else {
      drupal_set_message('Simulation only: ' . $uid);
    }
  }
  catch (Exception $e) {
    $context['results']['failed_uids'][] = $uid;
    return;
  }
}

/**
 * Page callback where the user is going to be redirected after the batch process is finished
 * @param bool $success
 * @param array $results
 * - Our statistics in an array
 * @param array $operations
 * - Contains the operations that remained unprocessed.
 */
function sf_account_resync_users_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Batch completed!');
    drupal_set_message('Failed UIDs: <pre>' . print_r($results['failed_uids'], TRUE) . '</pre>', 'error');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}