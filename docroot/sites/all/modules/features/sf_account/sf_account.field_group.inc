<?php
/**
 * @file
 * sf_account.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sf_account_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_univ_data|sf_account|sf_account|form';
  $field_group->group_name = 'group_univ_data';
  $field_group->entity_type = 'sf_account';
  $field_group->bundle = 'sf_account';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'University Data',
    'weight' => '3',
    'children' => array(
      0 => 'field_fice_code',
      1 => 'field_opeid',
      2 => 'field_college_state_list',
      3 => 'field_college_taxonomy_term',
      4 => 'field_aliases',
      5 => 'field_unitid',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-univ-data field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups[''] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('University Data');

  return $field_groups;
}
