<?php
/**
 * @file
 * sf_account.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sf_account_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function sf_account_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_entity_type_info().
 */
function sf_account_eck_entity_type_info() {
  $items = array(
    'sf_account' => array(
      'name' => 'sf_account',
      'label' => 'SF Account',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
      ),
    ),
  );
  return $items;
}
