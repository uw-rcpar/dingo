<?php
/**
 * @file
 * rcpar_order_entity.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function rcpar_order_entity_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_add_details|commerce_order|commerce_order|form';
  $field_group->group_name = 'group_add_details';
  $field_group->entity_type = 'commerce_order';
  $field_group->bundle = 'commerce_order';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional Shipping Comments',
    'weight' => '5',
    'children' => array(
      0 => 'field_order_additional_details',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Additional Shipping Comments',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => ' group-add-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_info|commerce_order|commerce_order|form';
  $field_group->group_name = 'group_additional_info';
  $field_group->entity_type = 'commerce_order';
  $field_group->bundle = 'commerce_order';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Student Account Info',
    'weight' => '6',
    'children' => array(
      0 => 'field_hear_about_us',
      1 => 'field_why_choose_rcpar',
      2 => 'field_employer',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Student Account Info',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => ' group-additional-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_custom_fields|commerce_order|commerce_order|form';
  $field_group->group_name = 'group_custom_fields';
  $field_group->entity_type = 'commerce_order';
  $field_group->bundle = 'commerce_order';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Employment Info',
    'weight' => '8',
    'children' => array(
      0 => 'field_custom_field_name_1',
      1 => 'field_custom_field_value_1',
      2 => 'field_custom_field_name_2',
      3 => 'field_custom_field_value_2',
      4 => 'field_custom_field_name_3',
      5 => 'field_custom_field_value_3',
      6 => 'field_employment_verification',
      7 => 'field_employment_start_date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Employment Info',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-custom-fields field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_direct_bill_payments|commerce_order|commerce_order|form';
  $field_group->group_name = 'group_direct_bill_payments';
  $field_group->entity_type = 'commerce_order';
  $field_group->bundle = 'commerce_order';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Direct Bill Payment',
    'weight' => '11',
    'children' => array(
      0 => 'field_partial_payment',
      1 => 'field_paid_in_full',
      2 => 'field_payment_amount',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Direct Bill Payment',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-direct-bill-payments field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_review_head|commerce_order|commerce_order|form';
  $field_group->group_name = 'group_review_head';
  $field_group->entity_type = 'commerce_order';
  $field_group->bundle = 'commerce_order';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional Information',
    'weight' => '10',
    'children' => array(),
    'format_type' => 'html-element',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'element' => 'div',
        'show_label' => 0,
        'label_element' => 'div',
        'classes' => 'group-review-head field-group-html-element',
        'attributes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uworld|commerce_order|commerce_order|form';
  $field_group->group_name = 'group_uworld';
  $field_group->entity_type = 'commerce_order';
  $field_group->bundle = 'commerce_order';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'UWorld',
    'weight' => '16',
    'children' => array(
      0 => 'field_uw_order_id',
      1 => 'field_uw_sf_id',
      2 => 'field_uw_partner_name',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-uworld field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups[''] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional Information');
  t('Additional Shipping Comments');
  t('Direct Bill Payment');
  t('Employment Info');
  t('Student Account Info');
  t('UWorld');

  return $field_groups;
}
