<?php
/**
 * @file
 * rcpar_order_entity.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rcpar_order_entity_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
