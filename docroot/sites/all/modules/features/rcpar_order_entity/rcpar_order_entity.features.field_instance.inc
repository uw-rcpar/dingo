<?php
/**
 * @file
 * rcpar_order_entity.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function rcpar_order_entity_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'commerce_order-commerce_order-commerce_coupon_order_reference'.
  $field_instances['commerce_order-commerce_order-commerce_coupon_order_reference'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'administrator' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => -4,
      ),
      'customer' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => -4,
      ),
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 22,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'commerce_coupon_order_reference',
    'label' => 'Coupon',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'replicate_entityreference_referenced_entities' => FALSE,
      'replicate_entityreference_referencing_entities' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-commerce_customer_billing'.
  $field_instances['commerce_order-commerce_order-commerce_customer_billing'] = array(
    'bundle' => 'commerce_order',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'administrator' => array(
        'label' => 'above',
        'module' => 'commerce_customer',
        'settings' => array(),
        'type' => 'commerce_customer_profile_reference_display',
        'weight' => -5,
      ),
      'customer' => array(
        'label' => 'above',
        'module' => 'commerce_customer',
        'settings' => array(),
        'type' => 'commerce_customer_profile_reference_display',
        'weight' => -5,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'commerce_customer',
        'settings' => array(),
        'type' => 'commerce_customer_profile_reference_display',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'commerce_customer_billing',
    'label' => 'Billing Address',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'commerce_customer',
      'settings' => array(),
      'type' => 'commerce_customer_profile_manager',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-commerce_customer_shipping'.
  $field_instances['commerce_order-commerce_order-commerce_customer_shipping'] = array(
    'bundle' => 'commerce_order',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'administrator' => array(
        'label' => 'above',
        'module' => 'commerce_customer',
        'settings' => array(),
        'type' => 'commerce_customer_profile_reference_display',
        'weight' => -5,
      ),
      'customer' => array(
        'label' => 'above',
        'module' => 'commerce_customer',
        'settings' => array(),
        'type' => 'commerce_customer_profile_reference_display',
        'weight' => -5,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'commerce_customer',
        'settings' => array(),
        'type' => 'commerce_customer_profile_reference_display',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'commerce_customer_shipping',
    'label' => 'Shipping Address',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'commerce_customer',
      'settings' => array(),
      'type' => 'commerce_customer_profile_manager',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-commerce_line_items'.
  $field_instances['commerce_order-commerce_order-commerce_line_items'] = array(
    'bundle' => 'commerce_order',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'administrator' => array(
        'label' => 'hidden',
        'module' => 'commerce_line_item',
        'settings' => array(
          'view' => 'commerce_line_item_table|default',
        ),
        'type' => 'commerce_line_item_reference_view',
        'weight' => -10,
      ),
      'customer' => array(
        'label' => 'hidden',
        'module' => 'commerce_line_item',
        'settings' => array(
          'view' => 'commerce_line_item_table|default',
        ),
        'type' => 'commerce_line_item_reference_view',
        'weight' => -10,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'commerce_line_item',
        'settings' => array(
          'view' => 'commerce_line_item_table|default',
        ),
        'type' => 'commerce_line_item_reference_view',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'commerce_line_items',
    'label' => 'Line items',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'commerce_line_item',
      'settings' => array(),
      'type' => 'commerce_line_item_manager',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-commerce_order_total'.
  $field_instances['commerce_order-commerce_order-commerce_order_total'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'administrator' => array(
        'label' => 'hidden',
        'module' => 'commerce_price',
        'settings' => array(
          'calculation' => FALSE,
        ),
        'type' => 'commerce_price_formatted_components',
        'weight' => -8,
      ),
      'customer' => array(
        'label' => 'hidden',
        'module' => 'commerce_price',
        'settings' => array(
          'calculation' => FALSE,
        ),
        'type' => 'commerce_price_formatted_components',
        'weight' => -8,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'commerce_price',
        'settings' => array(
          'calculation' => FALSE,
        ),
        'type' => 'commerce_price_formatted_components',
        'weight' => 1,
      ),
      'node_teaser' => array(
        'label' => 'hidden',
        'module' => 'commerce_price',
        'settings' => array(
          'calculation' => FALSE,
        ),
        'type' => 'commerce_price_formatted_components',
        'weight' => -8,
      ),
      'token' => array(
        'label' => 'hidden',
        'module' => 'commerce_price',
        'settings' => array(
          'calculation' => FALSE,
        ),
        'type' => 'commerce_price_formatted_components',
        'weight' => -8,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'commerce_order_total',
    'label' => 'Order total',
    'required' => TRUE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'commerce_price',
      'settings' => array(
        'currency_code' => 'default',
      ),
      'type' => 'commerce_price_full',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_credit_order_reference'.
  $field_instances['commerce_order-commerce_order-field_credit_order_reference'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'For Prepaid Debit orders this field relates to the Prepaid Credit order.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 27,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_credit_order_reference',
    'label' => 'Credit Order Reference',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'replicate_entityreference_referenced_entities' => FALSE,
      'replicate_entityreference_referencing_entities' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 15,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_custom_field_name_1'.
  $field_instances['commerce_order-commerce_order-field_custom_field_name_1'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 12,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_custom_field_name_1',
    'label' => 'Custom Field Name 1',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 17,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_custom_field_name_2'.
  $field_instances['commerce_order-commerce_order-field_custom_field_name_2'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 14,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_custom_field_name_2',
    'label' => 'Custom Field Name 2',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 19,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_custom_field_name_3'.
  $field_instances['commerce_order-commerce_order-field_custom_field_name_3'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 16,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_custom_field_name_3',
    'label' => 'Custom Field Name 3',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 21,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_custom_field_value_1'.
  $field_instances['commerce_order-commerce_order-field_custom_field_value_1'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 13,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_custom_field_value_1',
    'label' => 'Custom Field Value 1',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 18,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_custom_field_value_2'.
  $field_instances['commerce_order-commerce_order-field_custom_field_value_2'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 15,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_custom_field_value_2',
    'label' => 'Custom Field Value 2',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 20,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_custom_field_value_3'.
  $field_instances['commerce_order-commerce_order-field_custom_field_value_3'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 18,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_custom_field_value_3',
    'label' => 'Custom Field Value 3',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 22,
    ),
  );

  // Exported field_instance: 'commerce_order-commerce_order-field_employer'.
  $field_instances['commerce_order-commerce_order-field_employer'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_employer',
    'label' => 'Employer',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 11,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_employment_start_date'.
  $field_instances['commerce_order-commerce_order-field_employment_start_date'] = array(
    'bundle' => 'commerce_order',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 11,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_employment_start_date',
    'label' => 'Employment Start Date',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 16,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_employment_verification'.
  $field_instances['commerce_order-commerce_order-field_employment_verification'] = array(
    'bundle' => 'commerce_order',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_employment_verification',
    'label' => 'Employment Verification',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'description_field' => 0,
      'file_directory' => 'partners/employment_verifications',
      'file_extensions' => 'pdf doc docx png gif jpg jpeg',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'transliterate' => 0,
          ),
          'value' => 'partners/employment_verifications',
        ),
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 0,
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 15,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_hear_about_us'.
  $field_instances['commerce_order-commerce_order-field_hear_about_us'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_hear_about_us',
    'label' => 'How did you hear about us?',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 9,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_office_location'.
  $field_instances['commerce_order-commerce_order-field_office_location'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 10,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_office_location',
    'label' => 'Office location',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 9,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_order_additional_details'.
  $field_instances['commerce_order-commerce_order-field_order_additional_details'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_order_additional_details',
    'label' => 'Add any additional comments for shipping',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_paid_in_full'.
  $field_instances['commerce_order-commerce_order-field_paid_in_full'] = array(
    'bundle' => 'commerce_order',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 20,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_paid_in_full',
    'label' => 'Paid-in-Full',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 32,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_partial_payment'.
  $field_instances['commerce_order-commerce_order-field_partial_payment'] = array(
    'bundle' => 'commerce_order',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 19,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_partial_payment',
    'label' => 'Partial payment',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 33,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_partner_profile'.
  $field_instances['commerce_order-commerce_order-field_partner_profile'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 23,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_partner_profile',
    'label' => 'Partner Profile',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'replicate_entityreference_referenced_entities' => FALSE,
      'replicate_entityreference_referencing_entities' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_payment_amount'.
  $field_instances['commerce_order-commerce_order-field_payment_amount'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 21,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_payment_amount',
    'label' => 'Payment Amount',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 34,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_prepayment_active'.
  $field_instances['commerce_order-commerce_order-field_prepayment_active'] = array(
    'bundle' => 'commerce_order',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 26,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_prepayment_active',
    'label' => 'Prepayment Active',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 14,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_prepayment_credit'.
  $field_instances['commerce_order-commerce_order-field_prepayment_credit'] = array(
    'bundle' => 'commerce_order',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 25,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_prepayment_credit',
    'label' => 'Prepayment Credit',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'commerce_order-commerce_order-field_uw_order_id'.
  $field_instances['commerce_order-commerce_order-field_uw_order_id'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 28,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_uw_order_id',
    'label' => 'UW Order Id',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 17,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_uw_partner_name'.
  $field_instances['commerce_order-commerce_order-field_uw_partner_name'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 30,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_uw_partner_name',
    'label' => 'UW Partner Name',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 19,
    ),
  );

  // Exported field_instance: 'commerce_order-commerce_order-field_uw_sf_id'.
  $field_instances['commerce_order-commerce_order-field_uw_sf_id'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The raw text value of the partner\'s Salesforce ID imported from UWorld associated with this order, if any.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 29,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_uw_sf_id',
    'label' => 'UW SF ID',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 18,
    ),
  );

  // Exported field_instance:
  // 'commerce_order-commerce_order-field_why_choose_rcpar'.
  $field_instances['commerce_order-commerce_order-field_why_choose_rcpar'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'commerce_order',
    'field_name' => 'field_why_choose_rcpar',
    'label' => 'Why did you choose Roger CPA Review?',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 10,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add any additional comments for shipping');
  t('Billing Address');
  t('Coupon');
  t('Credit Order Reference');
  t('Custom Field Name 1');
  t('Custom Field Name 2');
  t('Custom Field Name 3');
  t('Custom Field Value 1');
  t('Custom Field Value 2');
  t('Custom Field Value 3');
  t('Employer');
  t('Employment Start Date');
  t('Employment Verification');
  t('For Prepaid Debit orders this field relates to the Prepaid Credit order.');
  t('How did you hear about us?');
  t('Line items');
  t('Office location');
  t('Order total');
  t('Paid-in-Full');
  t('Partial payment');
  t('Partner Profile');
  t('Payment Amount');
  t('Prepayment Active');
  t('Prepayment Credit');
  t('Shipping Address');
  t('The raw text value of the partner\'s Salesforce ID imported from UWorld associated with this order, if any.');
  t('UW Order Id');
  t('UW Partner Name');
  t('UW SF ID');
  t('Why did you choose Roger CPA Review?');

  return $field_instances;
}
