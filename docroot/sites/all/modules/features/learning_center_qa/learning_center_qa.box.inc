<?php
/**
 * @file
 * learning_center_qa.box.inc
 */

/**
 * Implements hook_default_box().
 */
function learning_center_qa_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'free_trial_get_started';
  $box->plugin_key = 'simple';
  $box->title = '<none>';
  $box->description = 'Free Trial - Get Started Today';
  $box->options = array(
    'body' => array(
      'value' => '<div class="free-trial-get-started"><a href="/cpa-courses/free-trial">ROGER CPA REVIEW FREE TRIAL. GET STARTED TODAY! <i class="icon glyphicon glyphicon-chevron-right" aria-hidden="true"></i></a></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['free_trial_get_started'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'qa_accounting_dictionary_link';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'QA - Accounting Dictionary link';
  $box->options = array(
    'body' => array(
      'value' => '<div class="qa-accounting-dictionary-link"><a href="/lc/accounting-dictionary" class="learn-more-link">
<div class="icon-lc pull-left"><img src="/sites/default/files/icon-dictionary2.png" alt="CPA Exam Accounting Dictionary"></div>
<div class="icon-lc">
<h2 class="icon box-link-heading">Accounting Dictionary</h2>
<div class="box-link-text">An A-Z dictionary of 500+ Accounting Terms. <span class="link-caret">&gt;</span></div>
<p>
</p></div>
 </a></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => 'block-right',
  );
  $export['qa_accounting_dictionary_link'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'qa_question_intro_text';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Question List Page intro text';
  $box->options = array(
    'body' => array(
      'value' => '<div class="row qa-heading"><div class="col-md-12"><div class="page-heading"><h1 class="page-title page-header">Questions & Answers</h1></div></div></div>

<div class="row"><div class="col-md-12"><div class="page-heading question-list-page-heading"><div class="field field-name-field-intro-text field-type-text-long field-label-hidden">Have questions about the CPA Exam? We’ve answered some of the toughest and most frequently asked questions about the CPA Exam, requirements, education and more!  </div></div></div></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['qa_question_intro_text'] = $box;

  return $export;
}
