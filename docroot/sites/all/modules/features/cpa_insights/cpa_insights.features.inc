<?php
/**
 * @file
 * cpa_insights.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cpa_insights_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "video_embed_field" && $api == "default_video_embed_styles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cpa_insights_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cpa_insights_node_info() {
  $items = array(
    'cpa_insights' => array(
      'name' => t('CPA Insights'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
