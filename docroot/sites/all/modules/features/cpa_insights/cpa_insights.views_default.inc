<?php
/**
 * @file
 * cpa_insights.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cpa_insights_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cpa_insights';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'CPA Insights';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'CPA Insights';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title Link */
  $handler->display->display_options['fields']['field_title_link']['id'] = 'field_title_link';
  $handler->display->display_options['fields']['field_title_link']['table'] = 'field_data_field_title_link';
  $handler->display->display_options['fields']['field_title_link']['field'] = 'field_title_link';
  $handler->display->display_options['fields']['field_title_link']['label'] = '';
  $handler->display->display_options['fields']['field_title_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_title_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_title_link']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'insights_image_and_video_thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Youtube Video Link - Thumbnail path */
  $handler->display->display_options['fields']['field_video_thumbnail_path']['id'] = 'field_video_thumbnail_path';
  $handler->display->display_options['fields']['field_video_thumbnail_path']['table'] = 'field_data_field_video';
  $handler->display->display_options['fields']['field_video_thumbnail_path']['field'] = 'field_video_thumbnail_path';
  $handler->display->display_options['fields']['field_video_thumbnail_path']['label'] = '';
  $handler->display->display_options['fields']['field_video_thumbnail_path']['alter']['text'] = '<img src="[field_video_thumbnail_path]" />';
  $handler->display->display_options['fields']['field_video_thumbnail_path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_thumbnail_path']['hide_empty'] = TRUE;
  /* Field: Content: Youtube Video Link */
  $handler->display->display_options['fields']['field_video']['id'] = 'field_video';
  $handler->display->display_options['fields']['field_video']['table'] = 'field_data_field_video';
  $handler->display->display_options['fields']['field_video']['field'] = 'field_video';
  $handler->display->display_options['fields']['field_video']['label'] = '';
  $handler->display->display_options['fields']['field_video']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video']['click_sort_column'] = 'video_url';
  $handler->display->display_options['fields']['field_video']['settings'] = array(
    'video_style' => 'cpa_insights_video',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Full Text */
  $handler->display->display_options['fields']['field_full_text']['id'] = 'field_full_text';
  $handler->display->display_options['fields']['field_full_text']['table'] = 'field_data_field_full_text';
  $handler->display->display_options['fields']['field_full_text']['field'] = 'field_full_text';
  $handler->display->display_options['fields']['field_full_text']['label'] = '';
  $handler->display->display_options['fields']['field_full_text']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_full_text']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_full_text']['hide_empty'] = TRUE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cpa_insights' => 'cpa_insights',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['cpa_insights'] = $view;

  return $export;
}
