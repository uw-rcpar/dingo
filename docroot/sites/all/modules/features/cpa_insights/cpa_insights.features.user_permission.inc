<?php
/**
 * @file
 * cpa_insights.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cpa_insights_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create cpa_insights content'.
  $permissions['create cpa_insights content'] = array(
    'name' => 'create cpa_insights content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any cpa_insights content'.
  $permissions['delete any cpa_insights content'] = array(
    'name' => 'delete any cpa_insights content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own cpa_insights content'.
  $permissions['delete own cpa_insights content'] = array(
    'name' => 'delete own cpa_insights content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any cpa_insights content'.
  $permissions['edit any cpa_insights content'] = array(
    'name' => 'edit any cpa_insights content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own cpa_insights content'.
  $permissions['edit own cpa_insights content'] = array(
    'name' => 'edit own cpa_insights content',
    'roles' => array(
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
