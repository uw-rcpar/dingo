<?php
/**
 * @file
 * team.features.inc
 */

/**
 * Implements hook_views_api().
 */
function team_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function team_image_default_styles() {
  $styles = array();

  // Exported image style: team_landing_page_photo.
  $styles['team_landing_page_photo'] = array(
    'label' => 'team_landing_page_photo',
    'effects' => array(
      28 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 220,
          'height' => 327,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: team_photo.
  $styles['team_photo'] = array(
    'label' => 'team_photo',
    'effects' => array(
      27 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 335,
          'height' => 485,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function team_node_info() {
  $items = array(
    'team' => array(
      'name' => t('Team'),
      'base' => 'node_content',
      'description' => t('Team member profiles.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
