<?php
/**
 * @file
 * rcpar_fixed_coupon.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function rcpar_fixed_coupon_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_fixed-commerce_coupon_code'.
  $field_instances['commerce_coupon-commerce_coupon_fixed-commerce_coupon_code'] = array(
    'bundle' => 'commerce_coupon_fixed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_code',
    'label' => 'Coupon Code',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_fixed-commerce_coupon_fixed_amount'.
  $field_instances['commerce_coupon-commerce_coupon_fixed-commerce_coupon_fixed_amount'] = array(
    'bundle' => 'commerce_coupon_fixed',
    'default_value' => array(
      0 => array(
        'amount' => 0,
        'currency_code' => 'USD',
        'data' => array(
          'components' => array(),
        ),
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'commerce_price',
        'settings' => array(
          'calculation' => FALSE,
        ),
        'type' => 'commerce_price_formatted_amount',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_fixed_amount',
    'label' => 'Fixed Amount',
    'required' => TRUE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'commerce_price',
      'settings' => array(
        'currency_code' => 'default',
      ),
      'type' => 'commerce_price_full',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_fixed-commerce_coupon_number_of_uses'.
  $field_instances['commerce_coupon-commerce_coupon_fixed-commerce_coupon_number_of_uses'] = array(
    'bundle' => 'commerce_coupon_fixed',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Number of times that coupon code can be used by any customer on the site, before it is set to inactive',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_number_of_uses',
    'label' => 'Maximum number of Uses',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_fixed-field_author'.
  $field_instances['commerce_coupon-commerce_coupon_fixed-field_author'] = array(
    'bundle' => 'commerce_coupon_fixed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Creator of the coupon.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_author',
    'label' => 'Author',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_fixed-field_coupon_exempt_tax'.
  $field_instances['commerce_coupon-commerce_coupon_fixed-field_coupon_exempt_tax'] = array(
    'bundle' => 'commerce_coupon_fixed',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_coupon_exempt_tax',
    'label' => 'Exempt Tax',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_fixed-field_description'.
  $field_instances['commerce_coupon-commerce_coupon_fixed-field_description'] = array(
    'bundle' => 'commerce_coupon_fixed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Description of the coupon.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_description',
    'label' => 'Description',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_fixed-field_exp_date'.
  $field_instances['commerce_coupon-commerce_coupon_fixed-field_exp_date'] = array(
    'bundle' => 'commerce_coupon_fixed',
    'deleted' => 0,
    'description' => 'Expiration date of the coupon.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_exp_date',
    'label' => 'Expiration date',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Author');
  t('Coupon Code');
  t('Creator of the coupon.');
  t('Description');
  t('Description of the coupon.');
  t('Exempt Tax');
  t('Expiration date');
  t('Expiration date of the coupon.');
  t('Fixed Amount');
  t('Maximum number of Uses');
  t('Number of times that coupon code can be used by any customer on the site, before it is set to inactive');

  return $field_instances;
}
