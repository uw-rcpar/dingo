(function ($) {
  Drupal.behaviors.rcpar_fixed_coupon = {
    attach: function (context, settings) {
      $('#commerce-coupon-form #edit-field-coupon-exempt-tax-und').click(function () {
        $(this).attr('checked', false);
        ConfirmDialog('Are you sure that this promo code should allow for tax exempt orders', this);        
      });
    }
  };

  function ConfirmDialog(message, checkbox) {
    $(this).attr('yes','uncheck');
    $('<div></div>').appendTo('body')
        .html('<div><h6>' + message + '?</h6></div>')
        .dialog({
          modal: true, title: 'Tax Exempt', zIndex: 10000, autoOpen: true,
          width: 'auto', resizable: false,
          buttons: {
            Yes: function () {
              $(checkbox).attr('checked', true);
              $(this).dialog("close");
            },
            No: function () {
              $(checkbox).attr('checked', false);
              $(this).dialog("close");
            }
          },
          close: function (event, ui) {
            $(this).remove();
          }
        });
  }
  ;
}(jQuery));