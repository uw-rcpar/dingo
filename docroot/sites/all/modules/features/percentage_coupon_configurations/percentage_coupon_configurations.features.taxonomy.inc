<?php
/**
 * @file
 * percentage_coupon_configurations.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function percentage_coupon_configurations_taxonomy_default_vocabularies() {
  return array(
    'variation_type' => array(
      'name' => 'Variation Type',
      'machine_name' => 'variation_type',
      'description' => 'Percentage  Coupon Variation',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
