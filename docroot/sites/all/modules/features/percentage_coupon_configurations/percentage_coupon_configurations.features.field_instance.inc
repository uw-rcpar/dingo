<?php
/**
 * @file
 * percentage_coupon_configurations.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function percentage_coupon_configurations_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-commerce_coupon_code'.
  $field_instances['commerce_coupon-commerce_coupon_pct-commerce_coupon_code'] = array(
    'bundle' => 'commerce_coupon_pct',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_code',
    'label' => 'Coupon Code',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-commerce_coupon_number_of_uses'.
  $field_instances['commerce_coupon-commerce_coupon_pct-commerce_coupon_number_of_uses'] = array(
    'bundle' => 'commerce_coupon_pct',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Number of times that coupon code can be used by any customer on the site, before it is set to inactive',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_number_of_uses',
    'label' => 'Maximum number of Uses',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-commerce_coupon_percent_amount'.
  $field_instances['commerce_coupon-commerce_coupon_pct-commerce_coupon_percent_amount'] = array(
    'bundle' => 'commerce_coupon_pct',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 2,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_decimal',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_percent_amount',
    'label' => 'Percentage Amount',
    'required' => TRUE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'max' => 100,
      'min' => 0,
      'prefix' => '',
      'suffix' => '%',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-field_author'.
  $field_instances['commerce_coupon-commerce_coupon_pct-field_author'] = array(
    'bundle' => 'commerce_coupon_pct',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Creator of the coupon.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 11,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_author',
    'label' => 'Author',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'replicate_entityreference_referenced_entities' => FALSE,
      'replicate_entityreference_referencing_entities' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 11,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-field_commerce_couponprodref'.
  $field_instances['commerce_coupon-commerce_coupon_pct-field_commerce_couponprodref'] = array(
    'bundle' => 'commerce_coupon_pct',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_commerce_couponprodref',
    'label' => 'Products',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'replicate_entityreference_referenced_entities' => FALSE,
      'replicate_entityreference_referencing_entities' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 9,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-field_description'.
  $field_instances['commerce_coupon-commerce_coupon_pct-field_description'] = array(
    'bundle' => 'commerce_coupon_pct',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Description of the coupon.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 12,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_description',
    'label' => 'Description',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 12,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-field_disable_affiliate_attribut'.
  $field_instances['commerce_coupon-commerce_coupon_pct-field_disable_affiliate_attribut'] = array(
    'bundle' => 'commerce_coupon_pct',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'This means that affiliates will not get credit for the sale if this discount/coupon is used and tracking is disabled',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 13,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_disable_affiliate_attribut',
    'label' => 'Disable Affiliate Attribution Tracking',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 13,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-field_exp_date'.
  $field_instances['commerce_coupon-commerce_coupon_pct-field_exp_date'] = array(
    'bundle' => 'commerce_coupon_pct',
    'deleted' => 0,
    'description' => 'Expiration date of the coupon.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 10,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_exp_date',
    'label' => 'Expiration date',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => 10,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-commerce_coupon_pct-field_variation_type'.
  $field_instances['commerce_coupon-commerce_coupon_pct-field_variation_type'] = array(
    'bundle' => 'commerce_coupon_pct',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 14,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'field_variation_type',
    'label' => 'Variation Type',
    'required' => 0,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'jquery_ui_multiselect' => array(
        'dividerLocation' => 0.5,
        'doubleClickable' => 1,
        'dragToAdd' => 1,
        'enabled' => 0,
        'height' => 0,
        'includeAddAll' => 1,
        'includeRemoveAll' => 1,
        'searchable' => 1,
        'selectedContainerOnLeft' => 1,
        'sortable' => 1,
        'width' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 14,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Author');
  t('Coupon Code');
  t('Creator of the coupon.');
  t('Description');
  t('Description of the coupon.');
  t('Disable Affiliate Attribution Tracking');
  t('Expiration date');
  t('Expiration date of the coupon.');
  t('Maximum number of Uses');
  t('Number of times that coupon code can be used by any customer on the site, before it is set to inactive');
  t('Percentage Amount');
  t('Products');
  t('This means that affiliates will not get credit for the sale if this discount/coupon is used and tracking is disabled');
  t('Variation Type');

  return $field_instances;
}
