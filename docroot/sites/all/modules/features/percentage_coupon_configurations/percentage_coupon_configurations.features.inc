<?php
/**
 * @file
 * percentage_coupon_configurations.features.inc
 */

/**
 * Implements hook_commerce_coupon_default_types().
 */
function percentage_coupon_configurations_commerce_coupon_default_types() {
  $items = array(
    'commerce_coupon_pct' => array(
      'type' => 'commerce_coupon_pct',
      'label' => 'Percentage coupon',
      'weight' => 0,
      'data' => NULL,
      'status' => 1,
      'module' => NULL,
      'rdf_mapping' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function percentage_coupon_configurations_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
