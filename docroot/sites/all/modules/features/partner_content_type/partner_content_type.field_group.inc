<?php
/**
 * @file
 * partner_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function partner_content_type_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_add_ons|node|partners|form';
  $field_group->group_name = 'group_add_ons';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_legacy_fields';
  $field_group->data = array(
    'label' => 'Add ons',
    'weight' => '75',
    'children' => array(
      0 => 'field_products_partners',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Add ons',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_add_ons|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_authorization_information|node|partners|form';
  $field_group->group_name = 'group_authorization_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Authorization Information',
    'weight' => '8',
    'children' => array(
      0 => 'field_use_email_validation',
      1 => 'field_upload_document_field',
      2 => 'field_partner_upload_instruction',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-authorization-information field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_authorization_information|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_create_new_account|node|partners|form';
  $field_group->group_name = 'group_create_new_account';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_legacy_fields';
  $field_group->data = array(
    'label' => 'Monitoring Center - Create New Account',
    'weight' => '79',
    'children' => array(
      0 => 'field_account_email',
      1 => 'field_account_notify',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Monitoring Center - Create New Account',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-create-new-account field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_create_new_account|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_custom_fields|node|partners|form';
  $field_group->group_name = 'group_custom_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_legacy_fields';
  $field_group->data = array(
    'label' => 'Custom Fields',
    'weight' => '77',
    'children' => array(
      0 => 'field_custom_name_1',
      1 => 'field_custom_name_2',
      2 => 'field_custom_name_3',
      3 => 'field_custom_description_1',
      4 => 'field_custom_description_2',
      5 => 'field_custom_description_3',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Custom Fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_custom_fields|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_enrollment_instructions|node|partners|form';
  $field_group->group_name = 'group_enrollment_instructions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Enrollment Instructions',
    'weight' => '17',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-enrollment-instructions field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_enrollment_instructions|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_firm_information|node|partners|form';
  $field_group->group_name = 'group_firm_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Firm Information',
    'weight' => '11',
    'children' => array(
      0 => 'field_show_office_location_check',
      1 => 'field_office_location_choices',
      2 => 'field_collect_firm_terms',
      3 => 'field_collect_start_date',
      4 => 'field_terms_and_conditions',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-firm-information field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_firm_information|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_landing_page|node|partners|form';
  $field_group->group_name = 'group_landing_page';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Landing Page',
    'weight' => '7',
    'children' => array(
      0 => 'field_additional_instructions',
      1 => 'field_asl_landing_page_header',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-landing-page field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_landing_page|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_legacy_fields|node|partners|form';
  $field_group->group_name = 'group_legacy_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Legacy Fields',
    'weight' => '12',
    'children' => array(
      0 => 'group_add_ons',
      1 => 'group_create_new_account',
      2 => 'group_custom_fields',
      3 => 'group_limit_orders',
      4 => 'group_misc',
      5 => 'group_partner_web_portal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Legacy Fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-legacy-fields field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_legacy_fields|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_limit_orders|node|partners|form';
  $field_group->group_name = 'group_limit_orders';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_legacy_fields';
  $field_group->data = array(
    'label' => 'Limit Orders',
    'weight' => '78',
    'children' => array(
      0 => 'field_limit_orders_',
      1 => 'field_max_orders',
      2 => 'field_actual_orders_',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Limit Orders',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-limit-orders field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_limit_orders|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_misc|node|partners|form';
  $field_group->group_name = 'group_misc';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_legacy_fields';
  $field_group->data = array(
    'label' => 'Misc',
    'weight' => '74',
    'children' => array(
      0 => 'field_rate_type_id',
      1 => 'field_direct_bill',
      2 => 'field_state_society',
      3 => 'field_for_credit',
      4 => 'field_partner_fee_interval',
      5 => 'field_reporting_enrollments',
      6 => 'field_redirect_to_checkout',
      7 => 'field_retail_package_value',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Misc',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-misc field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_misc|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_monitoring_center|node|partners|form';
  $field_group->group_name = 'group_monitoring_center';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_partner_info';
  $field_group->data = array(
    'label' => 'Monitoring Center / Accounting Classroom Trainer',
    'weight' => '7',
    'children' => array(
      0 => 'field_user_account',
      1 => 'field_active_partner',
      2 => 'field_act_sample_products',
      3 => 'field_rcpar_cp_ic_access',
      4 => 'field_act_assignable_products',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-monitoring-center field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_monitoring_center|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_notifications|node|partners|form';
  $field_group->group_name = 'group_notifications';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Notification Settings',
    'weight' => '9',
    'children' => array(
      0 => 'field_enable_notifications',
      1 => 'field_notification_contacts',
      2 => 'field_progress_notification',
      3 => 'field_notify_new_enrollment',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_notifications|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_partner_access|node|partners|form';
  $field_group->group_name = 'group_partner_access';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Partner Access Restrictions',
    'weight' => '4',
    'children' => array(
      0 => 'field_flag_restrict',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_partner_access|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_partner_info|node|partners|form';
  $field_group->group_name = 'group_partner_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Partner Information',
    'weight' => '3',
    'children' => array(
      0 => 'body',
      1 => 'field_course_sections',
      2 => 'field_abbreviated_name',
      3 => 'field_partner_name',
      4 => 'field_field_sales_tax',
      5 => 'field_archive_pid',
      6 => 'field_fixed_expiration_date',
      7 => 'field_intacct_customer_id',
      8 => 'field_terms',
      9 => 'field_partner_manager',
      10 => 'field_partner_name_bundle',
      11 => 'field_user_selected_course',
      12 => 'group_monitoring_center',
      13 => 'group_relative_expiration',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_partner_info|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_partner_web_portal|node|partners|form';
  $field_group->group_name = 'group_partner_web_portal';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_legacy_fields';
  $field_group->data = array(
    'label' => 'Web Portal',
    'weight' => '76',
    'children' => array(
      0 => 'field_partner_logo',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Web Portal',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_partner_web_portal|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_per_part_information|node|partners|form';
  $field_group->group_name = 'group_per_part_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Per Part Information',
    'weight' => '6',
    'children' => array(
      0 => 'field_per_part_pricing',
      1 => 'field_aud_price',
      2 => 'field_reg_price',
      3 => 'field_bec_price',
      4 => 'field_far_price',
      5 => 'field_per_part_pricing_options',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-per-part-information field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_per_part_information|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pricing_add_ons|node|partners|form';
  $field_group->group_name = 'group_pricing_add_ons';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Course Pricing',
    'weight' => '4',
    'children' => array(),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_pricing_add_ons|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_information|node|partners|form';
  $field_group->group_name = 'group_product_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Product Information',
    'weight' => '5',
    'children' => array(
      0 => 'field_show_prices',
      1 => 'field_full_price',
      2 => 'field_bundle_package',
      3 => 'field_display_retail_package_val',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Product Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-product-information field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_product_information|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_relative_expiration|node|partners|form';
  $field_group->group_name = 'group_relative_expiration';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_partner_info';
  $field_group->data = array(
    'label' => 'Relative Expiration Date',
    'weight' => '12',
    'children' => array(
      0 => 'field_interval',
      1 => 'field_course_duration',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-relative-expiration field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_relative_expiration|node|partners|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_shipping|node|partners|form';
  $field_group->group_name = 'group_shipping';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partners';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Shipping',
    'weight' => '10',
    'children' => array(
      0 => 'field_flat_rate_shipping',
      1 => 'field_full_shipping_rate',
      2 => 'field_partial_shipping_rate',
      3 => 'field_international',
      4 => 'field_shipping_flow',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-shipping field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_shipping|node|partners|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Add ons');
  t('Authorization Information');
  t('Course Pricing');
  t('Custom Fields');
  t('Enrollment Instructions');
  t('Firm Information');
  t('Landing Page');
  t('Legacy Fields');
  t('Limit Orders');
  t('Misc');
  t('Monitoring Center - Create New Account');
  t('Monitoring Center / Accounting Classroom Trainer');
  t('Notification Settings');
  t('Partner Access Restrictions');
  t('Partner Information');
  t('Per Part Information');
  t('Product Information');
  t('Relative Expiration Date');
  t('Shipping');
  t('Web Portal');

  return $field_groups;
}
