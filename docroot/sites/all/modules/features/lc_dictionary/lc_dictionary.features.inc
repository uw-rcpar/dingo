<?php
/**
 * @file
 * lc_dictionary.features.inc
 */

/**
 * Implements hook_views_api().
 */
function lc_dictionary_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function lc_dictionary_node_info() {
  $items = array(
    'cpa_exam_learning_center' => array(
      'name' => t('Learning Center'),
      'base' => 'node_content',
      'description' => t('Features free video lectures and content. Must be a registered user to use.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
