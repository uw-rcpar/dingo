<?php
/**
 * @file
 * lc_dictionary.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function lc_dictionary_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access lexicon'.
  $permissions['access lexicon'] = array(
    'name' => 'access lexicon',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'lexicon',
  );

  // Exported permission: 'administer lexicon'.
  $permissions['administer lexicon'] = array(
    'name' => 'administer lexicon',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'lexicon',
  );

  return $permissions;
}
