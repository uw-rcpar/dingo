<?php
/**
 * @file
 * lc_dictionary.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function lc_dictionary_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'dictionary_term_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Dictionary Term Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Dictionary Term Search';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Sort criterion: Taxonomy term: Name */
  $handler->display->display_options['sorts']['name']['id'] = 'name';
  $handler->display->display_options['sorts']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['name']['field'] = 'name';
  /* Contextual filter: Taxonomy term: Term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'dictionary' => 'dictionary',
  );
  /* Filter criterion: Taxonomy term: Name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['operator'] = 'contains';
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Enter a term here';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    13 => 0,
    12 => 0,
    14 => 0,
    15 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No search results found.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Taxonomy term: Name */
  $handler->display->display_options['arguments']['name']['id'] = 'name';
  $handler->display->display_options['arguments']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['name']['field'] = 'name';
  $handler->display->display_options['arguments']['name']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['name']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['name']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['name']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['name']['limit'] = '0';
  $handler->display->display_options['path'] = 'lc/accounting-dictionary/search';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['label'] = 'TID';
  $handler->display->display_options['fields']['tid']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  $handler->display->display_options['fields']['counter']['reverse'] = 0;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Taxonomy term: Term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'dictionary' => 'dictionary',
  );
  $handler->display->display_options['block_description'] = 'Previous and Next Terms';
  $export['dictionary_term_search'] = $view;

  $view = new view();
  $view->name = 'latest_from_blog';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Latest From Blog';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Latest From Blog';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<div><a class="view-more-blogs blogs-plus">+</a></div>';
  $handler->display->display_options['footer']['area']['format'] = 'full_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'blog' => 'blog',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['latest_from_blog'] = $view;

  $view = new view();
  $view->name = 'learning_center';
  $view->description = 'V2.0 - Learning Center layout';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Learning Center';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'CPA Exam Strategies';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'jcarousel';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Field: Content: Video Thumbnail */
  $handler->display->display_options['fields']['field_lc_video_thumbnail']['id'] = 'field_lc_video_thumbnail';
  $handler->display->display_options['fields']['field_lc_video_thumbnail']['table'] = 'field_data_field_lc_video_thumbnail';
  $handler->display->display_options['fields']['field_lc_video_thumbnail']['field'] = 'field_lc_video_thumbnail';
  $handler->display->display_options['fields']['field_lc_video_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_lc_video_thumbnail']['element_class'] = 'video-thumbnail';
  $handler->display->display_options['fields']['field_lc_video_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_lc_video_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_lc_video_thumbnail']['settings'] = array(
    'image_style' => 'lc_video_poster',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Field: Learning Center Category (field_learning_center_category) */
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['id'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['table'] = 'field_data_field_learning_center_category';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['field'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_options']['argument'] = '1470';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cpa_exam_learning_center' => 'cpa_exam_learning_center',
  );

  /* Display: CPA Exam Basics */
  $handler = $view->new_display('block', 'CPA Exam Basics', 'block');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'CPA Exam Basics';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'New to the CPA Exam? Start here with our most frequently asked exam questions! Get to know the basics, including information on CPA Exam format, content, and study recommendations.';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Field: Learning Center Category (field_learning_center_category) */
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['id'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['table'] = 'field_data_field_learning_center_category';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['field'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_options']['argument'] = '1469';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary_options']['items_per_page'] = '25';

  /* Display: Exam and Industry Updates */
  $handler = $view->new_display('block', 'Exam and Industry Updates', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'CPA Exam and Industry Updates';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'The CPA Exam has gone through many changes over the years, and is likely to continue evolving. Ensure that you don’t miss any important CPA Exam changes by watching these informational updates.';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Field: Learning Center Category (field_learning_center_category) */
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['id'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['table'] = 'field_data_field_learning_center_category';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['field'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_options']['argument'] = '1470';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary_options']['items_per_page'] = '25';

  /* Display: Exam Strategies */
  $handler = $view->new_display('block', 'Exam Strategies', 'block_2');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'Focus in on your CPA Exam strategy to get the most out of your study time. Watch these videos and get motivated to tackle the CPA Exam!';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Field: Learning Center Category (field_learning_center_category) */
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['id'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['table'] = 'field_data_field_learning_center_category';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['field'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_options']['argument'] = '1471';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary_options']['items_per_page'] = '25';

  /* Display: Help on Difficult Topics */
  $handler = $view->new_display('block', 'Help on Difficult Topics', 'block_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Help on Difficult Topics';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'These topics are tough for many students, so know that you’re not alone! Pulled straight from the Roger CPA Review course, these lessons cover some of the most difficult CPA Exam topics, making them easy to learn and understand.


<style>
#block-views-learning-center-block-3 .block-wrapper-inner {
  border-bottom: 1px solid #ffa300;
  padding-bottom: 20px;
}
</style>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Field: Learning Center Category (field_learning_center_category) */
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['id'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['table'] = 'field_data_field_learning_center_category';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['field'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_options']['argument'] = '1472';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary_options']['items_per_page'] = '25';

  /* Display: Sharpen your Soft Skills */
  $handler = $view->new_display('block', 'Sharpen your Soft Skills', 'block_4');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Sharpen your Soft Skills';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'Strengthen your soft skills and professional etiquette for your next career fair, interview, or work event.';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Field: Learning Center Category (field_learning_center_category) */
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['id'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['table'] = 'field_data_field_learning_center_category';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['field'] = 'field_learning_center_category_tid';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['default_argument_options']['argument'] = '3901';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_learning_center_category_tid']['summary_options']['items_per_page'] = '25';
  $export['learning_center'] = $view;

  return $export;
}
