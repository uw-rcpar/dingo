<?php
/**
 * @file
 * careers.box.inc
 */

/**
 * Implements hook_default_box().
 */
function careers_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'careers_glassdoor';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Find us on Glassdoor';
  $box->options = array(
    'body' => array(
      'value' => '<div class="glassdoor-btn"><a href="https://www.glassdoor.com/Overview/Working-at-Roger-CPA-Review-EI_IE929999.11,27.htm" class="jobs-btn" target="_blank" >Find us on Glassdoor <i class="fa fa-chevron-right"></i></a></a></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['careers_glassdoor'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'careers_header';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Heder for Careers listing and Job detail page';
  $box->options = array(
    'body' => array(
      'value' => '<div class="row first-row"><div class="col-sm-12 "><div class="page-heading careers-heading"><h1 class="page-title">Join Our Team</h1><div class="careers-subheading">Are you ready to start enjoying what you do? Ready to build a career you love? We’re always looking for ambitious and talented individuals to join our growing team. Check out our job listings below!</div><div class="clear-empty"></div></div></div></div>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['careers_header'] = $box;

  return $export;
}
