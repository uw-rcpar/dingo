<?php
/**
 * @file
 * careers.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function careers_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_careers:why-roger/careers.
  $menu_links['main-menu_careers:why-roger/careers'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'why-roger/careers',
    'router_path' => 'why-roger/careers',
    'link_title' => 'Careers',
    'options' => array(
      'identifier' => 'main-menu_careers:why-roger/careers',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -40,
    'customized' => 1,
  );
  // Exported menu link: menu-partner-with-us_careers:why-roger/careers.
  $menu_links['menu-partner-with-us_careers:why-roger/careers'] = array(
    'menu_name' => 'menu-partner-with-us',
    'link_path' => 'why-roger/careers',
    'router_path' => 'why-roger/careers',
    'link_title' => 'Careers',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-partner-with-us_careers:why-roger/careers',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Careers');

  return $menu_links;
}
