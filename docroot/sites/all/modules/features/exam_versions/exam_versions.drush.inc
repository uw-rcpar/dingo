<?php

/**
 * Implementation of hook_drush_command
 * @return mixed
 */
function exam_versions_drush_command() {
  $items['exam-versions-migrate-ipq-questions-section'] = array(
    'description' => 'This script migrates IPQ questions section information
          from the old (single exam version) format to the new multi valued (one per exam year) format.',
  );
  return $items;
}

/**
 * Callback for the exam-versions-migrate-ipq-questions-section
 */
function drush_exam_versions_migrate_ipq_questions_section() {
  drush_print(dt('Processing (this might take a while)...'));
  exam_versions_update_migrate_ipq_questions();
  drush_print(dt('Finished'));
}