<?php

/**
 * Provides a form to control Exam version settings
 * @return array form for admin
 */
function exam_versions_admin_settings() {
  $form = array();

  // Set form field to enable / disable Exam versioning
  $form['exam_versions_enable_versioning'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Exam Versioning'),
    '#description' => t('This will be applied on Checkout, Dashboard pages, Packing Slips, Order Confirmations, and Order "Quick Edit" Screen'),
    '#default_value' => variable_get('exam_versions_enable_versioning', FALSE),
  );

  // Set default version 
  $default = exam_version_get_default_version();
  // Use array with same key as value 
  foreach (exam_versions_years() as $year => $tid) {
    $options[$year] = $year;
  }

  $form['exam_versions_default_version'] = array(
    '#type' => 'select',
    '#title' => t('Select default version'),
    '#options' => $options,
    '#description' => t('If Exam versioning is disabled this value will be used as default'),
    '#default_value' => $default,
  );

  // Link for taxlonomy exam version terms
  $form['exam_versions_taxonomy_link'] = array(
    '#type' => 'item',
    '#markup' => l("Go to 'Exam Version' Taxonomy terms", "admin/structure/taxonomy/exam_version")
  );

  // Set form field to enable / disable textbook version selection in enrollment funnel
  $form['exam_versions_enable_textbook_versioning'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Textbook Versioning'),
    '#description' => t('This will allow users to choose between textbook versions on checkout during enrollment. Selection will be available on order confirmations and "Quick Edit" Screen'),
    '#default_value' => variable_get('exam_versions_enable_textbook_versioning', FALSE),
  );

  // Make marketing text editable with rich-text editor to allow for tasty cream-of-html sauce
  $defaults = array(
    'value' => '',
    'format' => filter_default_format(),
  );
  $exam_versions_textbook_markup = variable_get('exam_versions_textbook_markup', $defaults);
  $form['exam_versions_textbook_markup'] = array(
    '#type' => 'text_format',
    '#title' => t('Textbook Version Text'),
    '#description' => t('This text will appear above the textbook version selection menu in the enrollment funnel'),
    '#default_value' => $exam_versions_textbook_markup['value'],
    '#format' => $exam_versions_textbook_markup['format'],
  );

  return system_settings_form($form);
}

/**
 * Form handler for page callback of admin/settings/rcpar/system_settings/exam_versions/create
 * Creates a new exam version
 */
function exam_versions_create($form, &$form_state) {
  $form = array();

  // New version name
  $form['exam_version'] = array(
    '#type' => 'textfield',
    '#title' => t('Version'),
    '#description' => 'Creates a new version, use a 4 digits year as version EX: ' . date('Y') . '. By default is set next year',
    '#default_value' => date('Y', strtotime('next year')),
    '#size' => 10,
    '#maxlength' => 20,
    '#required' => TRUE,
  );

  // Source version
  $form['source_version'] = array(
    '#type' => 'select',
    '#title' => 'Source Exam Version',
    '#description' => 'This is the exam version that the new version will be copied from.',
    '#options' => array_flip(exam_versions_years()),
    '#default_value' => end(exam_versions_years()),
    '#required' => TRUE,
  );

  // Submit button
  $form['process'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array('exam_versions_create_submit'),
  );

  return $form;
}

/**
 * Submit handler for exam_versions_create() form
 * Essentially duplicates the previous exam version structure and adds it as a new version to
 * most node types in the system.
 *
 * 1. Creates a new exam version taxonomy term in the exam_version vocabulary
 * 2. Query for all nodes that have an entity reference field to an existing version entity (all
 * three of those version entity types are handled)
 * 3. Creates a new version entity by duplicating the latest one
 * 4. Appends the new version entity to the entity reference field on that node
 *
 * This covers chapter nodes, course nodes, and all IPQ question types.
 */
function exam_versions_create_submit($form, &$form_state) {
  $new_version_name = $form_state['values']['exam_version'];
  $source_version_tid = $form_state['values']['source_version'];

  if ($new_version_name) {
    exam_version_create_new_term_version($new_version_name);
    $exams = exam_versions_years();
    $version_id = $exams[$new_version_name];
    $operations = array();

    // Chapters - Query for all chapters that have an existing exam reference and add a batch process
    // operation to each
    // @todo - This query assumes that no other entity or node types are using field_data_field_topic_per_exam_version
    $query = db_query('SELECT entity_id as nid, field_topic_per_exam_version_target_id as entity_id ,delta
      FROM field_data_field_topic_per_exam_version E
      WHERE E.delta = (
        SELECT MAX(E2.delta)
        FROM field_data_field_topic_per_exam_version E2
        WHERE E2.entity_id = E.entity_id
      )');
    $result = $query->fetchAll();
    foreach ($result as $record) {
      $operations[] = array(
        'exam_versions_create_new_version_chapters_batch',
        array(
          'exam_version' => $version_id,
          'source_version_tid' => $source_version_tid,
          'node_data' => $record,
        ),
      );
    }

    // Courses - one batch operation for each course node with an exam version reference
    // @todo - This query assumes that no other entity or node types are using field_data_field_chapter_per_exam_version
    $query = db_query('SELECT entity_id as nid, field_chapter_per_exam_version_target_id as entity_id,delta
        FROM field_data_field_chapter_per_exam_version E
        WHERE E.delta = (
          SELECT MAX(E2.delta)
          FROM field_data_field_chapter_per_exam_version E2
          WHERE E2.entity_id = E.entity_id
        )');
    $result = $query->fetchAll();

    foreach ($result as $record) {
      $operations[] = array(
        'exam_versions_create_new_version_courses_batch',
        array(
          'exam_version' => $version_id,
          'source_version_tid' => $source_version_tid,
          'type' => $record,
        ),
      );
    }

    // IPQ questions - one batch operation for each question node with an exam reference
    // @todo - This query assumes that no other entity or node types are using field_data_field_section_per_exam_version
    $query = db_select('field_data_field_section_per_exam_version', 'f');
    $query->fields('f', array('bundle', 'entity_type'));
    $query->groupBy('bundle');
    $result = $query->execute()->fetchAll();

    if (is_array($result)) {
      foreach ($result as $type) {
        $operations[] = array(
          'exam_versions_create_batch',
          array(
            'exam_version' => $version_id,
            'source_version_tid' => $source_version_tid,
            'type' => $type, // example: array('bundle' => 'ipq_drs_question', 'entity_type' => 'node')
          ),
        );
      }
    }

    // Set the batch
    $batch = array(
      'operations' => $operations,
      'title' => t('Creating reference for exam version: ' . $new_version_name),
      'init_message' => t('Initializing the creation of references.'),
      'progress_message' => t('Processed @current out of @total entity types processed.'),
      'error_message' => t('Something when wrong creating the references.'),
      'finished' => 'exam_versions_new_version_create_finish',
    );
    batch_set($batch);
  }
}
