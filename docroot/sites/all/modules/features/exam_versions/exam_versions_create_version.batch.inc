<?php

define('EXAM_VERSION_VOCAB_NAME', 'exam_version');

/**
 * Creates a new taxonomy term in case does not exists for exam
 * versions
 * @param string $version
 * - name of the new term to be versioned, e.g. "2017", "2018", etc
 * @throws DrupalUpdateException
 */
function exam_version_create_new_term_version($version) {

  $v = taxonomy_vocabulary_machine_name_load(EXAM_VERSION_VOCAB_NAME);

  // Create this exam version vocabulary if it doesn't already exist
  if (!$v) {
    $edit = array(
      'name' => "Exam Version",
      'machine_name' => EXAM_VERSION_VOCAB_NAME,
      'description' => t(''),
      'module' => 'taxonomy',
    );
    $v = (object) $edit;
    taxonomy_vocabulary_save($v);
    $v = taxonomy_vocabulary_machine_name_load(EXAM_VERSION_VOCAB_NAME);
  }

  // Create a new taxonomy term for this version if it doesn't already exist
  if ($v) {
    $term_name = $version;
    // We only do something if we don't have a term with that name in the taxonomy
    if (!taxonomy_get_term_by_name($term_name, EXAM_VERSION_VOCAB_NAME)) {
      $termObj = new stdClass();
      $termObj->name = $term_name;
      $termObj->vid = $v->vid;
      taxonomy_term_save($termObj);
    }
  }
  else {
    // For this update to make sense, the Exam version taxonomy must exists.
    throw new DrupalUpdateException('Exam Version taxonomy was not found.');
  }
}

/**
 * Process each course and clones a new reference from the last version to
 * the chapters.
 *
 * @param int $new_version_tid
 * - term id of the new version reference
 * @param array $node_data
 * - array(nid, reference, delta) values
 */
function exam_versions_create_new_version_courses_batch($new_version_tid, $source_version_tid, $node_data) {
  if ($node = node_load($node_data->nid)) {
    // Get a wrapper and copy the version info
    $node_wrapper = entity_metadata_wrapper('node', $node);
    exam_versions_copy_version_ref($new_version_tid, $source_version_tid, $node_wrapper);
  }
}

/**
 * Process each course and clones a new reference from the last version to
 * the topics.
 *
 * @param int $new_version_tid
 * - Term id of the new version reference
 * @param object $node_data
 * - Contains keys: nid, entity_id, delta
 * Nid is the node of the parent entity (e.g. chapter node), entity_id is the ID of the latest
 * (latest meaning max delta) node_ref_by_exam_version entity reference on that node, and delta
 * is the delta of that entity reference field value.
 */
function exam_versions_create_new_version_chapters_batch($new_version_tid, $source_version_tid, $node_data) {
  if ($node = node_load($node_data->nid)) {
    // Get a wrapper and copy the version info
    $node_wrapper = entity_metadata_wrapper('node', $node);
    exam_versions_copy_version_ref($new_version_tid, $source_version_tid, $node_wrapper);
  }
}

/**
 * This batch operation creates a new exam reference entity for the new exam version
 * and appends it to all of the nodes of the given $type.
 * In practice, this adds the new exam version to all IPQ questions.
 *
 * @param int $exam_version
 * - term id of the new exam version
 * @param array $type
 * - Array with keys 'bundle' and 'entity_type'. the node type of the nodes that will apply the versions
 * @param array $context
 * - batch context
 */
function exam_versions_create_batch($exam_version, $source_version_tid, $type, &$context) {
  // If there is a sandbox count, we are still in the middle of processing nodes of this type
  if (isset($context['sandbox']['count'])) {
    // Increment the count to indicate this record has been processed
    $context['sandbox']['count']++;

    // Get the next index to use for the records array and grab the node id from the records
    $index = $context['sandbox']['count'];
    $nid = $context['sandbox']['records'][$index];

    // Add the new exam version to this node
    exam_versions_create_new_version_ipqs($exam_version, $source_version_tid, $nid);
  }
  // When there's no count in our sandbox, it means we have not begun processing this type and we're
  // ready to start a new one
  else {
    $context['sandbox']['count'] = 0;

    // Query for all entity IDs matching this node type in the entity reference field data
    $query = db_select('field_data_field_section_per_exam_version', 'f');
    $query->fields('f', array('entity_id'));
    $query->condition('bundle', $type->bundle);
    $result = $query->execute()->fetchAll();

    // Add each entity ID to the sandbox data
    if (is_array($result)) {
      foreach ($result as $node) {
        $context['sandbox']['records'][] = $node->entity_id;
      }
    }
  }

  // We'll mark this operation as finished when all of the records for this type have been processed
  $context['finished'] = $context['sandbox']['count'] / count($context['sandbox']['records']);

  // When all records have been processed, we'll reset the count and records so that the next operation will
  // start from scratch with a new type
  if ($context['finished'] === 1) {
    unset($context['sandbox']['count']);
    unset($context['sandbox']['records']);
  }
}

/**
 * Duplicates the last 'node_ref_by_exam_version' entity attached to the given
 * node and appends it to the node's entity reference field
 * (field_section_per_exam_version).
 *
 * @param int $new_version_tid
 * - Term id of the new exam version to reference
 * @param int $nid
 * - The node that we'll be adding the new version reference to
 */
function exam_versions_create_new_version_ipqs($new_version_tid, $source_version_tid, $nid) {
  if ($node = node_load($nid)) {
    // Get a wrapper and do the copy
    $node_wrapper = entity_metadata_wrapper('node', $node);
    exam_versions_copy_version_ref($new_version_tid, $source_version_tid, $node_wrapper);
  }
}

/**
 * @param int $new_version_tid
 * - The term ID of the new exam version
 * @param $source_version_tid
 * - The term ID of the exam version to copy from
 * @param $node_wrapper
 * - An entity metadata wrapper of the entity whose version info we're copying.
 */
function exam_versions_copy_version_ref($new_version_tid, $source_version_tid, $node_wrapper) {
  // A list of fields that hold version data
  $fields = array(
    'field_section_per_exam_version',
    'field_chapter_per_exam_version',
    'field_topic_per_exam_version',
  );

  // Iterate through each of the possible fields that could hold version information and copy it, if possible
  foreach ($fields as $reference_field) {
    try {
      // Get the existing exam references from the node
      $references = $node_wrapper->{$reference_field}->value();

      // We don't have this field, try the next
      if (empty($references)) {
        continue;
      }

      // Find the reference we want to copy from, and make sure we don't add the
      // new version reference if it's already on the node.
      foreach ($references as $ref) {
        $rw = entity_metadata_wrapper('node_ref_by_exam_version', $ref);
        if ($rw->field_exam_version_single_val->tid->value() == $new_version_tid) {
          // A reference to this exam version already exists, do nothing
          return;
        }

        // Looking for our source version in the node's existing version data...
        if ($rw->field_exam_version_single_val->tid->value() == $source_version_tid) {
          $source_reference = $ref;
        }
      }

      // Couldn't find the exam version to copy from, so don't copy anything.
      if (empty($source_reference)) {
        return;
      }

      // Grab the last exam reference entity and clone it so that modifications don't
      // affect the original entity. This is important primarily for IPQ questions being
      // saved in ipq_common_node_update(), where the values in the node object are written
      // to the ipq_question_pool table. Without cloning, the question pool is wrong, but the
      // reference entity itself is not saved so the changes are not permanent.
      $new_reference = clone $source_reference;

      // Create the new version entity - we'll remove the id so a new one is created
      // when saved, and set the new version tid
      unset($new_reference->id);
      $new_reference_wrapper = entity_metadata_wrapper('node_ref_by_exam_version', $new_reference);
      $new_reference_wrapper->field_exam_version_single_val->set($new_version_tid);
      $new_reference_wrapper->save();

      // Append the new entity to the entity reference field and save the node
      $node_wrapper->{$reference_field}[] = $new_reference_wrapper->getIdentifier();
      $node_wrapper->save();
    }
    catch (EntityMetadataWrapperException $e) {
      // Entities that don't have this field will throw an exception, we just catch it and continue on to the next
      continue;
    }
  }
}

/**
 * Finish processing batches.
 *
 * @param bool $success
 * @param array $results
 * @param array $operations
 */
function exam_versions_new_version_create_finish($success, $results, $operations) {
  if ($success) {
    drupal_set_message("Creation of new version Completes Successfully");
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(
      t('An error occurred while processing @operation with arguments : @args', array(
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        )
      ), 'error'
    );
  }
}