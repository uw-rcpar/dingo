<?php
/**
 * @file
 * exam_versions.features.inc
 */

/**
 * Implements hook_views_api().
 */
function exam_versions_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function exam_versions_eck_bundle_info() {
  $items = array(
    'node_ref_by_exam_version_chapter_ref_by_exam_version' => array(
      'machine_name' => 'node_ref_by_exam_version_chapter_ref_by_exam_version',
      'entity_type' => 'node_ref_by_exam_version',
      'name' => 'chapter_ref_by_exam_version',
      'label' => 'chapter_ref_by_exam_version',
      'config' => array(),
    ),
    'node_ref_by_exam_version_cou_chap_top_ref_by_exam_version' => array(
      'machine_name' => 'node_ref_by_exam_version_cou_chap_top_ref_by_exam_version',
      'entity_type' => 'node_ref_by_exam_version',
      'name' => 'cou_chap_top_ref_by_exam_version',
      'label' => 'cou_chap_top_ref_by_exam_version',
      'config' => array(
        'managed_properties' => array(),
      ),
    ),
    'node_ref_by_exam_version_topic_ref_by_exam_version' => array(
      'machine_name' => 'node_ref_by_exam_version_topic_ref_by_exam_version',
      'entity_type' => 'node_ref_by_exam_version',
      'name' => 'topic_ref_by_exam_version',
      'label' => 'topic_ref_by_exam_version',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function exam_versions_eck_entity_type_info() {
  $items = array(
    'node_ref_by_exam_version' => array(
      'name' => 'node_ref_by_exam_version',
      'label' => 'Node ref. by Exam Version',
      'properties' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function exam_versions_node_info() {
  $items = array(
    'rcpa_chapter' => array(
      'name' => t('Chapter'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'rcpa_course' => array(
      'name' => t('Course'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
