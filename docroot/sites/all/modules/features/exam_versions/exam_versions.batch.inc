<?php

/**
 * BATCH FILE TO PROCESS EXISTENT RESTRICTIONS
 */

 /**
 * Form to start the batch process
 * @param type $form
 * @param type $form_state
 */
function exam_versions_restrictions_versioning_form($form, $form_state) {
  //Get versions
  $exam_versions = exam_versions_years();

  $options = array();
  foreach ($exam_versions as $year => $value) {
      $options[$value] = $year;
  }

  $latest_version = exam_versions_get_latest_version();

  $form['exam_version'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $latest_version->tid, 
    '#required' => true,
    '#title' => t('Version to Copy'),
  );

  $form['partners_type'] = array(
    '#type' => 'radios',
    '#options' => array('regular' => 'Free Access', 'free' => 'Free Trial'),
    '#required' => true,
    '#title' => t('Partner Type'),
  );

  $form['actions'] = array(    
    '#type' => 'submit',
    '#value' => t('Start versions on restrictions'),
    '#submit' => array('exam_versions_restrictions_versioning'),    
  );

  return $form;
}

/**
 * Defines the operations to execute on batches
 * @param type $form
 * @param type $form_state
 */
function exam_versions_restrictions_versioning($form, $form_state) {
  $exam_version = $form_state['values']['exam_version'];

  //can be for free trial or other partners
  $partner_type = $form_state['values']['partners_type'];

  if ($partner_type == 'free') {
    //load partner freetrial
    $result = db_select('field_data_field_flag_restrict', 'r')
      ->fields('r', array('entity_id'))
      ->condition('bundle', 'partners')
      ->condition('r.entity_id', 134)
      ->groupBy('r.entity_id')
      ->execute();

    if($partner = $result->fetchObject()) {
      //load freetrial partner values to set the free trial entitlments that are active.
      $values = exam_versions_get_partner_values($partner->entity_id, $exam_version);
  
      //load all freetrial entitlements on the system that are active
      $result = db_query("SELECT r.*, MAX(r.delta) as deltamax
                FROM field_data_field_flag_restrict r  
                inner join field_data_field_partner_id p on p.entity_id=r.entity_id and p.field_partner_id_value = $partner->entity_id
  
                # bringing the dates to filter the ones that are currently active
                INNER JOIN field_data_field_expiration_date exp_date ON exp_date.entity_id = r.entity_id AND exp_date.entity_type = 'node'
                INNER JOIN field_data_field_original_expiration_date orig_exp_date ON orig_exp_date.entity_id = r.entity_id AND orig_exp_date.entity_type = 'node'
  
                where r.bundle='user_entitlement_product'
                AND 
                (
                  exp_date.field_expiration_date_value >= UNIX_TIMESTAMP() OR orig_exp_date.field_original_expiration_date_value >= UNIX_TIMESTAMP()
                )                          
                GROUP BY r.entity_id");
  
      foreach ($result as $entitlement) {
        $operations[] = array(
          'exam_versions_restrictions_versioning_entitlement_operation',
          array(
            'values' => $values,
            'entitlement' => $entitlement,
            'year_tid' => $exam_version
          )
        );
      }
    }
  }

  if ($partner_type == 'regular') {
    $query = db_select('field_data_field_flag_restrict', 'r')
        ->fields('r', array('entity_id'))
        ->condition('r.bundle', 'partners')
        ->condition('r.entity_id', array(111, 134), 'NOT IN')        
        ->groupBy('r.entity_id');
    $query->join('field_data_field_billing_type', 'bt', "bt.entity_id=r.entity_id and bt.field_billing_type_value='freetrial'");
    $result = $query->execute();

    //if partner exists
    while ($partner = $result->fetchObject()) {
      //load partner values to set the entitlments that are active.
      $values = exam_versions_get_partner_values($partner->entity_id, $exam_version);

      $operations[] = array(
        'exam_versions_restrictions_versioning_operation',
        array(
          'values' => $values,
          'pid' => $partner->entity_id,
          'year_tid' => $exam_version
        )
      );
    }
  }

  $operations[] = array(
    'exam_versions_restrictions_versioning_operation',
    array(
      'values' => array(),
      'pid' => 'last',
      'year_tid' => $exam_version
    )
  );


  $batch = array(
    'operations' => $operations,
    'finished' => 'exam_versions_restrictions_versioning_finished',
    'title' => t('Updating Entitlements'),
    'init_message' => t('Inicializing Update of entitlements.'),
    'progress_message' => t('Processed @current out of @total Entitlements.'),
    'error_message' => t('No entitlements were updated'),
    'file' => drupal_get_path('module', 'exam_versions') . '/exam_versions.batch.inc',
  );
  batch_set($batch);
}

/* * OPERATIONS* */

/**
 * 
 * @param type $pid partner id
 * @param type $tid_year term id for version
 * @return <array> of partner values to copy into the new restrictions
 */
function exam_versions_get_partner_values($pid, $tid_year) {
  $values = array();
  $result = db_select('field_data_field_flag_restrict', 'r')
      ->fields('r')
      ->condition('bundle', 'partners')
      ->condition('entity_id', $pid)
      ->condition('field_flag_restrict_exam_version', $tid_year)
      ->execute();

  while ($record = $result->fetchObject()) {
    $values[] = array(
      'field_flag_restrict_course' => $record->field_flag_restrict_course,
      'field_flag_restrict_chapter' => $record->field_flag_restrict_chapter,
      'field_flag_restrict_topic' => $record->field_flag_restrict_topic,
    );
  }

  return $values;
}


/**
 * Load all the entitlements belonging to the partnert sent on pid parameter, 
 * and are not expired.
 * @param type $values partner restrictions values
 * @param type $pid  partner id
 * @param type $context
 */
function exam_versions_restrictions_versioning_operation($values, $pid, $year_tid, &$context) {
  if ($pid == 'last') {
    $batch = &batch_get();
    $batch_next_set = $batch['current_set'] + 1;
    $batch_set = &$batch['sets'][$batch_next_set];
    foreach ($context['results'] as $pid) {
      $partner_values = array_pop($pid);
      foreach ($pid as $entitlement) {
        $batch_set['operations'][] = array('exam_versions_restrictions_versioning_entitlement_operation', array($partner_values, $entitlement, $year_tid));
      }
    }

    $batch_set['progress_message'] = t('Processed @current out of @total Entitlements');
    $batch_set['file'] = drupal_get_path('module', 'exam_versions') . '/exam_versions.batch.inc';
    $batch_set['total'] = count($batch_set['operations']);
    $batch_set['count'] = $batch_set['total'];

    _batch_populate_queue($batch, $batch_next_set);

    $context['message'] = "Transitioning to entitlements";
  }
  else {
    //load all  entitlements on the system that are active
    $ent_result = db_query("SELECT r.*, MAX(r.delta) as deltamax
      FROM field_data_field_flag_restrict r  
      inner join field_data_field_partner_id p on p.entity_id=r.entity_id and p.field_partner_id_value = :partner

      INNER JOIN node entitlement ON entitlement.nid = r.entity_id

      # bringing the dates to filter the ones that are currently active
      INNER JOIN field_data_field_expiration_date exp_date ON exp_date.entity_id = r.entity_id AND exp_date.entity_type = 'node'
      INNER JOIN field_data_field_original_expiration_date orig_exp_date ON orig_exp_date.entity_id = r.entity_id AND orig_exp_date.entity_type = 'node'
      where r.bundle='user_entitlement_product'
      AND 
      (
      exp_date.field_expiration_date_value >= UNIX_TIMESTAMP() OR orig_exp_date.field_original_expiration_date_value >= UNIX_TIMESTAMP()
      )

      GROUP BY r.entity_id", array(':partner' => $pid));

    foreach ($ent_result as $entitlement) {
      $context['results'][$pid][$entitlement->entity_id] = $entitlement;
    }
    $context['results'][$pid]['values'] = $values;
    $context['message'] = "Processing Partners";
  }
}

/**
 * 
 * @param type $partner_values values to copy from the origin freetrial
 * @param type $entitlement entitlement where we will be inserting to
 * @param type $year_tid taxonomy term equivalent to version
 * @param type $context
 */
function exam_versions_restrictions_versioning_entitlement_operation($partner_values, $entitlement, $year_tid, &$context) {
  $delta = $entitlement->deltamax;

  db_delete('field_data_field_flag_restrict')
  ->condition('bundle', 'user_entitlement_product')
  ->condition('entity_id', $entitlement->entity_id)
  ->condition('field_flag_restrict_exam_version', $year_tid)
  ->execute();

  $insert = db_insert('field_data_field_flag_restrict');
  $insert->fields(
      array(
        'entity_type',
        'bundle',
        'deleted',
        'delta',
        'entity_id',
        'revision_id',
        'language',
        'field_flag_restrict_course',
        'field_flag_restrict_chapter',
        'field_flag_restrict_topic',
        'field_flag_restrict_exam_version',
      )
  );

  foreach ($partner_values as $value) {
    $delta++;
    $insert->values(
        array(
          'entity_type' => 'node',
          'bundle' => 'user_entitlement_product',
          'deleted' => $entitlement->deleted,
          'delta' => $delta,
          'entity_id' => $entitlement->entity_id,
          'revision_id' => $entitlement->revision_id,
          'language' => 'und',
          'field_flag_restrict_course' => $value['field_flag_restrict_course'],
          'field_flag_restrict_chapter' => $value['field_flag_restrict_chapter'],
          'field_flag_restrict_topic' => $value['field_flag_restrict_topic'],
          'field_flag_restrict_exam_version' => $year_tid,
        )
    );
  }

  $insert->execute();
}

/**
 * finish batch process
 * @param type $success
 * @param type $results
 * @param type $operations
 */
function exam_versions_restrictions_versioning_finished($success, $results, $operations) {
  if ($success) {
    drupal_flush_all_caches();    
    drupal_set_message("Copy Process Complete Succesfully");
  } else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(
        t('An error occurred while processing @operation with arguments : @args', array(
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE))
        ), 'error'
    );
  }
}
