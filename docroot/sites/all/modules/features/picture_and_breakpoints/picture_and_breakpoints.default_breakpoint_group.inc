<?php
/**
 * @file
 * picture_and_breakpoints.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function picture_and_breakpoints_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'bootstrap';
  $breakpoint_group->name = 'Bootstrap';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.bootstrap.screen-xs-max',
    1 => 'breakpoints.theme.bootstrap.screen-sm-min',
    2 => 'breakpoints.theme.bootstrap.screen-sm-max',
    3 => 'breakpoints.theme.bootstrap.screen-md-min',
    4 => 'breakpoints.theme.bootstrap.screen-md-max',
    5 => 'breakpoints.theme.bootstrap.screen-lg-min',
  );
  $breakpoint_group->type = 'theme';
  $breakpoint_group->overridden = 0;
  $export['bootstrap'] = $breakpoint_group;

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'bootstrap_rcpar';
  $breakpoint_group->name = 'Bootstrap RCPAR Sub-theme';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.bootstrap_rcpar.large',
    1 => 'breakpoints.theme.bootstrap_rcpar.medium',
    2 => 'breakpoints.theme.bootstrap_rcpar.narrow',
    3 => 'breakpoints.theme.bootstrap_rcpar.mobile',
  );
  $breakpoint_group->type = 'theme';
  $breakpoint_group->overridden = 0;
  $export['bootstrap_rcpar'] = $breakpoint_group;

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'flexslider';
  $breakpoint_group->name = 'flexslider';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.bootstrap_rcpar.large',
    1 => 'breakpoints.theme.bootstrap_rcpar.medium',
    2 => 'breakpoints.theme.bootstrap_rcpar.narrow',
    3 => 'breakpoints.theme.bootstrap_rcpar.mobile',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['flexslider'] = $breakpoint_group;

  return $export;
}
