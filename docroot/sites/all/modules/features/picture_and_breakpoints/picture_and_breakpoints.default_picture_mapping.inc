<?php
/**
 * @file
 * picture_and_breakpoints.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function picture_and_breakpoints_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Bootstrap RCPAR Sub-theme';
  $picture_mapping->machine_name = 'bootstrap_rcpar_sub_theme';
  $picture_mapping->breakpoint_group = 'bootstrap_rcpar';
  $picture_mapping->mapping = array(
    'breakpoints.theme.bootstrap_rcpar.large' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_large_1x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_large_2x',
      ),
    ),
    'breakpoints.theme.bootstrap_rcpar.medium' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_medium_1x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_medium_2x',
      ),
    ),
    'breakpoints.theme.bootstrap_rcpar.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_narrow_1x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_narrow_2x',
      ),
    ),
    'breakpoints.theme.bootstrap_rcpar.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_mobile_1x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_mobile_2x',
      ),
    ),
  );
  $export['bootstrap_rcpar_sub_theme'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'flexslider';
  $picture_mapping->machine_name = 'flexslider';
  $picture_mapping->breakpoint_group = 'flexslider';
  $picture_mapping->mapping = array(
    'breakpoints.theme.bootstrap_rcpar.large' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'fs_breakpoints_theme_bootstrap_rcpar_large_1x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'breakpoints.theme.bootstrap_rcpar.medium' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'fs_breakpoints_theme_bootstrap_rcpar_medium_1x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'breakpoints.theme.bootstrap_rcpar.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'fs_breakpoints_theme_bootstrap_rcpar_narrow_1x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => '_none',
      ),
    ),
    'breakpoints.theme.bootstrap_rcpar.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'fs_breakpoints_theme_bootstrap_rcpar_mobile_1x',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => '_none',
      ),
    ),
  );
  $export['flexslider'] = $picture_mapping;

  return $export;
}
