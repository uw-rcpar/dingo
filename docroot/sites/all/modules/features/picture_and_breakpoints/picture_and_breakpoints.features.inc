<?php
/**
 * @file
 * picture_and_breakpoints.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function picture_and_breakpoints_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
  if ($module == "picture" && $api == "default_picture_mapping") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function picture_and_breakpoints_image_default_styles() {
  $styles = array();

  // Exported image style: fs_breakpoints_theme_bootstrap_rcpar_large_1x.
  $styles['fs_breakpoints_theme_bootstrap_rcpar_large_1x'] = array(
    'label' => 'fs_breakpoints_theme_bootstrap_rcpar_large_1x',
    'effects' => array(
      36 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 2000,
          'height' => 761,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: fs_breakpoints_theme_bootstrap_rcpar_medium_1x.
  $styles['fs_breakpoints_theme_bootstrap_rcpar_medium_1x'] = array(
    'label' => 'fs_breakpoints_theme_bootstrap_rcpar_medium_1x',
    'effects' => array(
      52 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1199,
          'height' => 456,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: fs_breakpoints_theme_bootstrap_rcpar_mobile_1x.
  $styles['fs_breakpoints_theme_bootstrap_rcpar_mobile_1x'] = array(
    'label' => 'fs_breakpoints_theme_bootstrap_rcpar_mobile_1x',
    'effects' => array(
      56 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 559,
          'height' => 213,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: fs_breakpoints_theme_bootstrap_rcpar_narrow_1x.
  $styles['fs_breakpoints_theme_bootstrap_rcpar_narrow_1x'] = array(
    'label' => 'fs_breakpoints_theme_bootstrap_rcpar_narrow_1x',
    'effects' => array(
      54 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 767,
          'height' => 292,
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: hp_blog_breakpoints_theme_bootstrap_rcpar_large_1x.
  $styles['hp_blog_breakpoints_theme_bootstrap_rcpar_large_1x'] = array(
    'label' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_large_1x',
    'effects' => array(
      48 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 450,
          'height' => 450,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: hp_blog_breakpoints_theme_bootstrap_rcpar_large_2x.
  $styles['hp_blog_breakpoints_theme_bootstrap_rcpar_large_2x'] = array(
    'label' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_large_2x',
    'effects' => array(
      47 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 900,
          'height' => 900,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: hp_blog_breakpoints_theme_bootstrap_rcpar_medium_1x.
  $styles['hp_blog_breakpoints_theme_bootstrap_rcpar_medium_1x'] = array(
    'label' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_medium_1x',
    'effects' => array(
      46 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 298,
          'height' => 298,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: hp_blog_breakpoints_theme_bootstrap_rcpar_medium_2x.
  $styles['hp_blog_breakpoints_theme_bootstrap_rcpar_medium_2x'] = array(
    'label' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_medium_2x',
    'effects' => array(
      45 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 596,
          'height' => 596,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: hp_blog_breakpoints_theme_bootstrap_rcpar_mobile_1x.
  $styles['hp_blog_breakpoints_theme_bootstrap_rcpar_mobile_1x'] = array(
    'label' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_mobile_1x',
    'effects' => array(
      42 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 51,
          'height' => 51,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: hp_blog_breakpoints_theme_bootstrap_rcpar_mobile_2x.
  $styles['hp_blog_breakpoints_theme_bootstrap_rcpar_mobile_2x'] = array(
    'label' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_mobile_2x',
    'effects' => array(
      41 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 102,
          'height' => 102,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: hp_blog_breakpoints_theme_bootstrap_rcpar_narrow_1x.
  $styles['hp_blog_breakpoints_theme_bootstrap_rcpar_narrow_1x'] = array(
    'label' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_narrow_1x',
    'effects' => array(
      44 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 213,
          'height' => 213,
        ),
        'weight' => 3,
      ),
    ),
  );

  // Exported image style: hp_blog_breakpoints_theme_bootstrap_rcpar_narrow_2x.
  $styles['hp_blog_breakpoints_theme_bootstrap_rcpar_narrow_2x'] = array(
    'label' => 'hp_blog_breakpoints_theme_bootstrap_rcpar_narrow_2x',
    'effects' => array(
      43 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 426,
          'height' => 426,
        ),
        'weight' => 3,
      ),
    ),
  );

  return $styles;
}
