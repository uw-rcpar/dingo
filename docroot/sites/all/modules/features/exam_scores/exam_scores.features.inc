<?php
/**
 * @file
 * exam_scores.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function exam_scores_eck_bundle_info() {
  $items = array(
    'exam_scores_exam_score' => array(
      'machine_name' => 'exam_scores_exam_score',
      'entity_type' => 'exam_scores',
      'name' => 'exam_score',
      'label' => 'Exam Score',
      'config' => array(
        'managed_properties' => array(
          'uid' => 'uid',
          'created' => 'created',
          'changed' => 'changed',
          'section' => 'section',
          'exam_date' => 'exam_date',
          'score' => 'score',
          'attempt' => 'attempt',
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function exam_scores_eck_entity_type_info() {
  $items = array(
    'exam_scores' => array(
      'name' => 'exam_scores',
      'label' => 'exam scores ',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'section' => array(
          'label' => 'Section',
          'type' => 'text',
          'behavior' => '',
        ),
        'exam_date' => array(
          'label' => 'Exam date',
          'type' => 'integer',
          'behavior' => '',
        ),
        'score' => array(
          'label' => 'Score',
          'type' => 'decimal',
          'behavior' => '',
        ),
        'attempt' => array(
          'label' => 'Attempt',
          'type' => 'integer',
          'behavior' => '',
        ),
      ),
    ),
  );
  return $items;
}
