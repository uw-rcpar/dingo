<?php
/**
 * @file
 * news_content_type.box.inc
 */

/**
 * Implements hook_default_box().
 */
function news_content_type_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'news_contact_page_header';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'News Contact page header';
  $box->options = array(
    'body' => array(
      'value' => '<div class="row first-row news-page-header">
        <div class="col-sm-12 ">
            <div class="page-heading">
                <div class="field-items">
                    <div class="field-item even" property="dc:title">
                        <h1 class="page-title">News & Press Contact</h1>
                    </div>
                </div>
<!--
                <div class="field field-name-field-intro-text field-type-text-long field-label-hidden">
                <div class="field-items">
                    <div class="field-item even">
                        <p>The fastest and easiest way to stay up-to-date on CPA Exam changes, industry news and inspirational stories. Anytime, anywhere.</p>
                    </div>
                </div>
            </div>
-->
            </div>
        </div>
    </div>

',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['news_contact_page_header'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'news_landing_page_header';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'News landing page header';
  $box->options = array(
    'body' => array(
      'value' => '<div class="row first-row news-page-header">
        <div class="col-sm-12 ">
            <div class="page-heading">
                <div class="field-items">
                    <div class="field-item even" property="dc:title">
                        <h1 class="page-title">News & Press</h1>
                    </div>
                </div>
<!--
                <div class="field field-name-field-intro-text field-type-text-long field-label-hidden">
                <div class="field-items">
                    <div class="field-item even">
                        <p>The fastest and easiest way to stay up-to-date on CPA Exam changes, industry news and inspirational stories. Anytime, anywhere.</p>
                    </div>
                </div>
            </div>
-->
            </div>
        </div>
    </div>

',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['news_landing_page_header'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'news_media_page_header';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'News Media Coverage Header';
  $box->options = array(
    'body' => array(
      'value' => '<div class="row first-row news-page-header">
        <div class="col-sm-12 ">
            <div class="page-heading">
                <div class="field-items">
                    <div class="field-item even" property="dc:title">
                        <h1 class="page-title">Media Coverage</h1>
                    </div>
                </div>
<!--
                <div class="field field-name-field-intro-text field-type-text-long field-label-hidden">
                <div class="field-items">
                    <div class="field-item even">
                        <p>The fastest and easiest way to stay up-to-date on CPA Exam changes, industry news and inspirational stories. Anytime, anywhere.</p>
                    </div>
                </div>
            </div>
-->
            </div>
        </div>
    </div>

',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['news_media_page_header'] = $box;

  return $export;
}
