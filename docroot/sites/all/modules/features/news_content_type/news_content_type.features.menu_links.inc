<?php
/**
 * @file
 * news_content_type.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function news_content_type_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_news:node/3815815.
  $menu_links['main-menu_news:node/3815815'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/3815815',
    'router_path' => 'node/%',
    'link_title' => 'News',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_news:node/3815815',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('News');

  return $menu_links;
}
