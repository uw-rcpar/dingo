<?php
/**
 * @file
 * news_content_type.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function news_content_type_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-news.
  $menus['menu-news'] = array(
    'menu_name' => 'menu-news',
    'title' => 'News',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('News');

  return $menus;
}
