<?php
/**
 * @file
 * homepage_slideshow.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function homepage_slideshow_image_default_styles() {
  $styles = array();

  // Exported image style: slide_image_480.
  $styles['slide_image_480'] = array(
    'label' => 'Slide Image 480',
    'effects' => array(
      23 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 480,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: slide_image_767.
  $styles['slide_image_767'] = array(
    'label' => 'Slide Image 767',
    'effects' => array(
      22 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 767,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function homepage_slideshow_node_info() {
  $items = array(
    'slide' => array(
      'name' => t('Slide'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
