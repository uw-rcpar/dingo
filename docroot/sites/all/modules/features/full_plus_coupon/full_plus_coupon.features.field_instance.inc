<?php
/**
 * @file
 * full_plus_coupon.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function full_plus_coupon_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'commerce_coupon-full_plus-commerce_coupon_code'.
  $field_instances['commerce_coupon-full_plus-commerce_coupon_code'] = array(
    'bundle' => 'full_plus',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_code',
    'label' => 'Coupon Code',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-full_plus-commerce_coupon_fixed_amount'.
  $field_instances['commerce_coupon-full_plus-commerce_coupon_fixed_amount'] = array(
    'bundle' => 'full_plus',
    'default_value' => array(
      0 => array(
        'amount' => 0,
        'currency_code' => 'USD',
        'data' => array(
          'components' => array(),
        ),
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'commerce_price',
        'settings' => array(
          'calculation' => FALSE,
        ),
        'type' => 'commerce_price_formatted_amount',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_fixed_amount',
    'label' => 'Fixed Amount',
    'required' => 1,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'commerce_price',
      'settings' => array(
        'currency_code' => 'default',
      ),
      'type' => 'commerce_price_full',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'commerce_coupon-full_plus-commerce_coupon_number_of_uses'.
  $field_instances['commerce_coupon-full_plus-commerce_coupon_number_of_uses'] = array(
    'bundle' => 'full_plus',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Number of times that coupon code can be used by any customer on the site, before it is set to inactive',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'commerce_coupon',
    'field_name' => 'commerce_coupon_number_of_uses',
    'label' => 'Maximum number of Uses',
    'required' => FALSE,
    'settings' => array(
      'commerce_user_profile_pane' => 0,
      'commerce_user_profile_pane_required' => 0,
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Coupon Code');
  t('Fixed Amount');
  t('Maximum number of Uses');
  t('Number of times that coupon code can be used by any customer on the site, before it is set to inactive');

  return $field_instances;
}
