<?php
/**
 * @file
 * full_plus_coupon.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function full_plus_coupon_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'commerce_coupon_code'.
  $field_bases['commerce_coupon_code'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'commerce_coupon',
    ),
    'field_name' => 'commerce_coupon_code',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 1,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'commerce_coupon_fixed_amount'.
  $field_bases['commerce_coupon_fixed_amount'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'commerce_coupon',
    ),
    'field_name' => 'commerce_coupon_fixed_amount',
    'indexes' => array(
      'currency_price' => array(
        0 => 'amount',
        1 => 'currency_code',
      ),
    ),
    'label' => 'Fixed Amount',
    'locked' => 0,
    'module' => 'commerce_price',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'commerce_price',
  );

  // Exported field_base: 'commerce_coupon_number_of_uses'.
  $field_bases['commerce_coupon_number_of_uses'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'commerce_coupon',
    ),
    'field_name' => 'commerce_coupon_number_of_uses',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  return $field_bases;
}
