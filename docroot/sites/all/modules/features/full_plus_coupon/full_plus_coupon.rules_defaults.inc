<?php
/**
 * @file
 * full_plus_coupon.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function full_plus_coupon_default_rules_configuration() {
  $items = array();
  $items['rules_redeem_a_full_plus'] = entity_import('rules_config', '{ "rules_redeem_a_full_plus" : {
      "LABEL" : "Redeem a Full Plus",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "6",
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules", "commerce_order", "commerce_coupon" ],
      "ON" : { "commerce_coupon_redeem" : [] },
      "IF" : [
        { "NOT php_eval" : { "code" : " return enroll_flow_rules_coupon_validation_msg(\\u0022full_plus\\u0022, $commerce_order, $coupon); " } },
        { "data_is" : { "data" : [ "coupon:type" ], "value" : "full_plus" } },
        { "entity_has_field" : { "entity" : [ "coupon" ], "field" : "commerce_coupon_fixed_amount" } },
        { "entity_has_field" : {
            "entity" : [ "commerce_order" ],
            "field" : "commerce_coupon_order_reference"
          }
        },
        { "NOT AND" : [
            { "data_is_empty" : { "data" : [ "coupon:commerce-coupon-fixed-amount" ] } }
          ]
        },
        { "data_is" : {
            "data" : [ "coupon:commerce-coupon-fixed-amount:amount" ],
            "op" : "\\u003E",
            "value" : 0
          }
        },
        { "data_is" : { "data" : [ "coupon:is-active" ], "op" : "=", "value" : true } },
        { "NOT php_eval" : { "code" : "return enroll_flow_order_has_coupon($commerce_order);" } },
        { "commerce_order_contains_product" : {
            "commerce_order" : [ "commerce_order" ],
            "product_id" : "FULL-DIS",
            "operator" : "\\u003E=",
            "value" : "1"
          }
        },
        { "php_eval" : { "code" : "return enroll_flow_rules_coupon_exclusion($commerce_order);" } }
      ],
      "DO" : [
        { "list_add" : {
            "list" : [ "commerce-order:commerce-coupon-order-reference" ],
            "item" : [ "coupon" ],
            "unique" : 1
          }
        },
        { "commerce_coupon_action_create_coupon_line_item" : {
            "USING" : {
              "commerce_coupon" : [ "coupon" ],
              "commerce_order" : [ "commerce-order" ],
              "amount" : [ "coupon:commerce-coupon-fixed-amount:amount" ],
              "component_name" : [ "coupon:price-component-name" ],
              "currency_code" : [ "coupon:commerce-coupon-fixed-amount:currency-code" ]
            },
            "PROVIDE" : { "commerce_coupon_line_item" : { "commerce_coupon_line_item" : "commerce coupon line item" } }
          }
        },
        { "php_eval" : { "code" : "rcpar_checkout_add_product_from_order($commerce_order,\\u0027FULL-6MEXT\\u0027);\\r\\n" } }
      ]
    }
  }');
  return $items;
}
