<?php
/**
 * @file
 * full_plus_coupon.features.inc
 */

/**
 * Implements hook_commerce_coupon_default_types().
 */
function full_plus_coupon_commerce_coupon_default_types() {
  $items = array(
    'full_plus' => array(
      'type' => 'full_plus',
      'label' => 'Full Plus',
      'weight' => 0,
      'data' => NULL,
      'status' => 1,
      'module' => NULL,
      'rdf_mapping' => array(),
    ),
  );
  return $items;
}
