<?php
/**
 * @file
 * home_page_items.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function home_page_items_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create home_reviews content'.
  $permissions['create home_reviews content'] = array(
    'name' => 'create home_reviews content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any home_reviews content'.
  $permissions['delete any home_reviews content'] = array(
    'name' => 'delete any home_reviews content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own home_reviews content'.
  $permissions['delete own home_reviews content'] = array(
    'name' => 'delete own home_reviews content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any home_reviews content'.
  $permissions['edit any home_reviews content'] = array(
    'name' => 'edit any home_reviews content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own home_reviews content'.
  $permissions['edit own home_reviews content'] = array(
    'name' => 'edit own home_reviews content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
