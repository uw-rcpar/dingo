<?php
/**
 * @file
 * home_page_items.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function home_page_items_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'home_page_blog';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Home page Blog';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'jcarousel';
  $handler->display->display_options['style_options']['wrap'] = 'circular';
  $handler->display->display_options['style_options']['visible'] = '3';
  $handler->display->display_options['style_options']['scroll'] = '3';
  $handler->display->display_options['style_options']['auto'] = '0';
  $handler->display->display_options['style_options']['autoPause'] = 1;
  $handler->display->display_options['style_options']['easing'] = '';
  $handler->display->display_options['style_options']['vertical'] = 0;
  $handler->display->display_options['style_options']['navigation'] = 'after';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_blog_posts_target_id']['id'] = 'field_blog_posts_target_id';
  $handler->display->display_options['relationships']['field_blog_posts_target_id']['table'] = 'field_data_field_blog_posts';
  $handler->display->display_options['relationships']['field_blog_posts_target_id']['field'] = 'field_blog_posts_target_id';
  /* Field: Content: Thumbnail */
  $handler->display->display_options['fields']['field_thumbnail']['id'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['field'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_thumbnail']['settings'] = array(
    'image_style' => 'homepage_blog_image',
    'image_link' => 'content',
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'homepage_blog_image',
    'image_link' => 'content',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'blog' => 'blog',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Promoted to front page */
  $handler->display->display_options['filters']['promote']['id'] = 'promote';
  $handler->display->display_options['filters']['promote']['table'] = 'node';
  $handler->display->display_options['filters']['promote']['field'] = 'promote';
  $handler->display->display_options['filters']['promote']['value'] = '1';
  $handler->display->display_options['filters']['promote']['group'] = 1;
  /* Filter criterion: Content: Thumbnail (field_thumbnail:fid) */
  $handler->display->display_options['filters']['field_thumbnail_fid']['id'] = 'field_thumbnail_fid';
  $handler->display->display_options['filters']['field_thumbnail_fid']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['filters']['field_thumbnail_fid']['field'] = 'field_thumbnail_fid';
  $handler->display->display_options['filters']['field_thumbnail_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_thumbnail_fid']['group'] = 2;
  /* Filter criterion: Content: Image (field_image:fid) */
  $handler->display->display_options['filters']['field_image_fid']['id'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['table'] = 'field_data_field_image';
  $handler->display->display_options['filters']['field_image_fid']['field'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_image_fid']['group'] = 2;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['optionset'] = 'clone_of_default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Thumbnail */
  $handler->display->display_options['fields']['field_thumbnail']['id'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['field'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_thumbnail']['type'] = 'picture';
  $handler->display->display_options['fields']['field_thumbnail']['settings'] = array(
    'picture_mapping' => 'bootstrap_rcpar_sub_theme',
    'fallback_image_style' => '',
    'lazyload' => 1,
    'lazyload_aspect_ratio' => 1,
    'image_link' => 'content',
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['type'] = 'picture';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'picture_mapping' => 'bootstrap_rcpar_sub_theme',
    'fallback_image_style' => '',
    'lazyload' => 1,
    'lazyload_aspect_ratio' => 1,
    'image_link' => 'content',
  );

  /* Display: Team Member Display */
  $handler = $view->new_display('block', 'Team Member Display', 'block_1');
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_blog_posts_node']['id'] = 'reverse_field_blog_posts_node';
  $handler->display->display_options['relationships']['reverse_field_blog_posts_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_blog_posts_node']['field'] = 'reverse_field_blog_posts_node';
  $handler->display->display_options['relationships']['reverse_field_blog_posts_node']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Thumbnail */
  $handler->display->display_options['fields']['field_thumbnail']['id'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['field'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_thumbnail']['settings'] = array(
    'image_style' => 'homepage_blog_image',
    'image_link' => 'content',
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'homepage_blog_image',
    'image_link' => 'content',
  );
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'reverse_field_blog_posts_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['nid']['validate_options']['types'] = array(
    'team' => 'team',
  );
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['relationship'] = 'field_blog_posts_target_id';
  $handler->display->display_options['filters']['type']['value'] = array(
    'blog' => 'blog',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Thumbnail (field_thumbnail:fid) */
  $handler->display->display_options['filters']['field_thumbnail_fid']['id'] = 'field_thumbnail_fid';
  $handler->display->display_options['filters']['field_thumbnail_fid']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['filters']['field_thumbnail_fid']['field'] = 'field_thumbnail_fid';
  $handler->display->display_options['filters']['field_thumbnail_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_thumbnail_fid']['group'] = 2;
  /* Filter criterion: Content: Image (field_image:fid) */
  $handler->display->display_options['filters']['field_image_fid']['id'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['table'] = 'field_data_field_image';
  $handler->display->display_options['filters']['field_image_fid']['field'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_image_fid']['group'] = 2;

  /* Display: ACT Professors */
  $handler = $view->new_display('block', 'ACT Professors', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Learn about industry and tech changes that are impacting your classroom';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Thumbnail */
  $handler->display->display_options['fields']['field_thumbnail']['id'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['field'] = 'field_thumbnail';
  $handler->display->display_options['fields']['field_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_thumbnail']['settings'] = array(
    'image_style' => 'homepage_blog_image',
    'image_link' => 'content',
  );
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'homepage_blog_image',
    'image_link' => 'content',
  );
  /* Field: Content: Act Professors page link */
  $handler->display->display_options['fields']['field_act_professors_page_link']['id'] = 'field_act_professors_page_link';
  $handler->display->display_options['fields']['field_act_professors_page_link']['table'] = 'field_data_field_act_professors_page_link';
  $handler->display->display_options['fields']['field_act_professors_page_link']['field'] = 'field_act_professors_page_link';
  $handler->display->display_options['fields']['field_act_professors_page_link']['label'] = '';
  $handler->display->display_options['fields']['field_act_professors_page_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_act_professors_page_link']['element_label_colon'] = FALSE;
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['view_node']['alter']['text'] = '[field_act_professors_page_link]';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'blog' => 'blog',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Thumbnail (field_thumbnail:fid) */
  $handler->display->display_options['filters']['field_thumbnail_fid']['id'] = 'field_thumbnail_fid';
  $handler->display->display_options['filters']['field_thumbnail_fid']['table'] = 'field_data_field_thumbnail';
  $handler->display->display_options['filters']['field_thumbnail_fid']['field'] = 'field_thumbnail_fid';
  $handler->display->display_options['filters']['field_thumbnail_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_thumbnail_fid']['group'] = 2;
  /* Filter criterion: Content: Image (field_image:fid) */
  $handler->display->display_options['filters']['field_image_fid']['id'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['table'] = 'field_data_field_image';
  $handler->display->display_options['filters']['field_image_fid']['field'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_image_fid']['group'] = 2;
  /* Filter criterion: Content: Feature on ACT Professors page (field_feature_on_act_professors_) */
  $handler->display->display_options['filters']['field_feature_on_act_professors__value']['id'] = 'field_feature_on_act_professors__value';
  $handler->display->display_options['filters']['field_feature_on_act_professors__value']['table'] = 'field_data_field_feature_on_act_professors_';
  $handler->display->display_options['filters']['field_feature_on_act_professors__value']['field'] = 'field_feature_on_act_professors__value';
  $handler->display->display_options['filters']['field_feature_on_act_professors__value']['value'] = array(
    1 => '1',
  );
  $export['home_page_blog'] = $view;

  $view = new view();
  $view->name = 'home_page_reviews_';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Home page Reviews ';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'See What Our Students Are Saying';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<a href="/why-roger/customer-reviews">More Customer Reviews ></a>';
  $handler->display->display_options['footer']['area']['format'] = 'full_html';
  /* Field: Content: Video Link 1 */
  $handler->display->display_options['fields']['field_video_link_1']['id'] = 'field_video_link_1';
  $handler->display->display_options['fields']['field_video_link_1']['table'] = 'field_data_field_video_link_1';
  $handler->display->display_options['fields']['field_video_link_1']['field'] = 'field_video_link_1';
  $handler->display->display_options['fields']['field_video_link_1']['label'] = '';
  $handler->display->display_options['fields']['field_video_link_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_link_1']['element_label_colon'] = FALSE;
  /* Field: Content: Video Link 2 */
  $handler->display->display_options['fields']['field_video_link_2']['id'] = 'field_video_link_2';
  $handler->display->display_options['fields']['field_video_link_2']['table'] = 'field_data_field_video_link_2';
  $handler->display->display_options['fields']['field_video_link_2']['field'] = 'field_video_link_2';
  $handler->display->display_options['fields']['field_video_link_2']['label'] = '';
  $handler->display->display_options['fields']['field_video_link_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_link_2']['element_label_colon'] = FALSE;
  /* Field: Content: Video Link 3 */
  $handler->display->display_options['fields']['field_video_link_3']['id'] = 'field_video_link_3';
  $handler->display->display_options['fields']['field_video_link_3']['table'] = 'field_data_field_video_link_3';
  $handler->display->display_options['fields']['field_video_link_3']['field'] = 'field_video_link_3';
  $handler->display->display_options['fields']['field_video_link_3']['label'] = '';
  $handler->display->display_options['fields']['field_video_link_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_link_3']['element_label_colon'] = FALSE;
  /* Field: Content: Video Thumbnail 1 */
  $handler->display->display_options['fields']['field_video_thumbnail_1']['id'] = 'field_video_thumbnail_1';
  $handler->display->display_options['fields']['field_video_thumbnail_1']['table'] = 'field_data_field_video_thumbnail_1';
  $handler->display->display_options['fields']['field_video_thumbnail_1']['field'] = 'field_video_thumbnail_1';
  $handler->display->display_options['fields']['field_video_thumbnail_1']['label'] = '';
  $handler->display->display_options['fields']['field_video_thumbnail_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_video_thumbnail_1']['alter']['text'] = '<div class="video-thumbnail">
<a href="javascript: void(0);" data-target="#reviewModal1" data-toggle="modal">[field_video_thumbnail_1]<img src="/sites/default/files/btn-play.png" class="play-btn"></a>
</div>

<!-- Modal -->
<div class="modal fade" id="reviewModal1" role="dialog" data-backdrop="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
<div class="clear-empty"></div>
</div>
<div class="modal-body">
<div class="video-wrapper"><video id="video" controls="" poster="[field_video_thumbnail_1]"><source src="[field_video_link_1]"></video></div>
</div>
</div>
</div>
</div>';
  $handler->display->display_options['fields']['field_video_thumbnail_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_thumbnail_1']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_video_thumbnail_1']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Video Thumbnail 2 */
  $handler->display->display_options['fields']['field_video_thumbnail_2']['id'] = 'field_video_thumbnail_2';
  $handler->display->display_options['fields']['field_video_thumbnail_2']['table'] = 'field_data_field_video_thumbnail_2';
  $handler->display->display_options['fields']['field_video_thumbnail_2']['field'] = 'field_video_thumbnail_2';
  $handler->display->display_options['fields']['field_video_thumbnail_2']['label'] = '';
  $handler->display->display_options['fields']['field_video_thumbnail_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_thumbnail_2']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_video_thumbnail_2']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Video Thumbnail 3 */
  $handler->display->display_options['fields']['field_video_thumbnail_3']['id'] = 'field_video_thumbnail_3';
  $handler->display->display_options['fields']['field_video_thumbnail_3']['table'] = 'field_data_field_video_thumbnail_3';
  $handler->display->display_options['fields']['field_video_thumbnail_3']['field'] = 'field_video_thumbnail_3';
  $handler->display->display_options['fields']['field_video_thumbnail_3']['label'] = '';
  $handler->display->display_options['fields']['field_video_thumbnail_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_thumbnail_3']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_video_thumbnail_3']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Review 1 */
  $handler->display->display_options['fields']['field_review_1']['id'] = 'field_review_1';
  $handler->display->display_options['fields']['field_review_1']['table'] = 'field_data_field_review_1';
  $handler->display->display_options['fields']['field_review_1']['field'] = 'field_review_1';
  $handler->display->display_options['fields']['field_review_1']['label'] = '';
  $handler->display->display_options['fields']['field_review_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_review_1']['alter']['text'] = '<div class="review-copy">[field_review_1]</div>';
  $handler->display->display_options['fields']['field_review_1']['element_label_colon'] = FALSE;
  /* Field: Content: Review 2 */
  $handler->display->display_options['fields']['field_review_2']['id'] = 'field_review_2';
  $handler->display->display_options['fields']['field_review_2']['table'] = 'field_data_field_review_2';
  $handler->display->display_options['fields']['field_review_2']['field'] = 'field_review_2';
  $handler->display->display_options['fields']['field_review_2']['label'] = '';
  $handler->display->display_options['fields']['field_review_2']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_review_2']['alter']['text'] = '<div class="review-copy">[field_review_2]</div>';
  $handler->display->display_options['fields']['field_review_2']['element_label_colon'] = FALSE;
  /* Field: Content: Review 3 */
  $handler->display->display_options['fields']['field_review_3']['id'] = 'field_review_3';
  $handler->display->display_options['fields']['field_review_3']['table'] = 'field_data_field_review_3';
  $handler->display->display_options['fields']['field_review_3']['field'] = 'field_review_3';
  $handler->display->display_options['fields']['field_review_3']['label'] = '';
  $handler->display->display_options['fields']['field_review_3']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_review_3']['alter']['text'] = '<div class="review-copy">[field_review_3]</div>';
  $handler->display->display_options['fields']['field_review_3']['element_label_colon'] = FALSE;
  /* Field: Content: Attribution and date */
  $handler->display->display_options['fields']['field_reference_1']['id'] = 'field_reference_1';
  $handler->display->display_options['fields']['field_reference_1']['table'] = 'field_data_field_reference_1';
  $handler->display->display_options['fields']['field_reference_1']['field'] = 'field_reference_1';
  $handler->display->display_options['fields']['field_reference_1']['label'] = '';
  $handler->display->display_options['fields']['field_reference_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_reference_1']['alter']['text'] = '<div class="review-attribution">[field_reference_1]</div>

<div class="field-name-field-details">
<div class="fivestar-default">
<div class="fivestar-widget-static fivestar-widget-static-vote fivestar-widget-static-5 clearfix">
<div class="star star-1 star-odd star-first"> <span id="schema_block" class="schema_review"><span itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating"><span class="on" itemprop="ratingValue">5</span><span itemprop="ratingCount">4</span><span itemprop="itemReviewed">CPA Exam Review Course</span></span></span>
</div>
<div class="star star-2 star-even"><span class="on"></span></div>
<div class="star star-3 star-odd"><span class="on"></span></div>
<div class="star star-4 star-even"><span class="on"></span></div>
<div class="star star-5 star-odd star-last"><span class="on"></span></div>
</div></div></div>


';
  $handler->display->display_options['fields']['field_reference_1']['element_label_colon'] = FALSE;
  /* Field: Content: Attribution and date */
  $handler->display->display_options['fields']['field_reference_2']['id'] = 'field_reference_2';
  $handler->display->display_options['fields']['field_reference_2']['table'] = 'field_data_field_reference_2';
  $handler->display->display_options['fields']['field_reference_2']['field'] = 'field_reference_2';
  $handler->display->display_options['fields']['field_reference_2']['label'] = '';
  $handler->display->display_options['fields']['field_reference_2']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_reference_2']['alter']['text'] = '<div class="review-attribution">[field_reference_2]</div>


<div class="field-name-field-details">
<div class="fivestar-default">
<div class="fivestar-widget-static fivestar-widget-static-vote fivestar-widget-static-5 clearfix">
<div class="star star-1 star-odd star-first"> <span id="schema_block" class="schema_review"><span itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating"><span class="on" itemprop="ratingValue">5</span><span itemprop="ratingCount">4</span><span itemprop="itemReviewed">CPA Exam Review Course</span></span></span>
</div>
<div class="star star-2 star-even"><span class="on"></span></div>
<div class="star star-3 star-odd"><span class="on"></span></div>
<div class="star star-4 star-even"><span class="on"></span></div>
<div class="star star-5 star-odd star-last"><span class="on"></span></div>
</div></div></div>



';
  $handler->display->display_options['fields']['field_reference_2']['element_label_colon'] = FALSE;
  /* Field: Content: Attribution and date */
  $handler->display->display_options['fields']['field_reference_3']['id'] = 'field_reference_3';
  $handler->display->display_options['fields']['field_reference_3']['table'] = 'field_data_field_reference_3';
  $handler->display->display_options['fields']['field_reference_3']['field'] = 'field_reference_3';
  $handler->display->display_options['fields']['field_reference_3']['label'] = '';
  $handler->display->display_options['fields']['field_reference_3']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_reference_3']['alter']['text'] = '<div class="review-attribution">[field_reference_3]</div>


<div class="field-name-field-details">
<div class="fivestar-default">
<div class="fivestar-widget-static fivestar-widget-static-vote fivestar-widget-static-5 clearfix">
<div class="star star-1 star-odd star-first"> <span id="schema_block" class="schema_review"><span itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating"><span class="on" itemprop="ratingValue">5</span><span itemprop="ratingCount">4</span><span itemprop="itemReviewed">CPA Exam Review Course</span></span></span>
</div>
<div class="star star-2 star-even"><span class="on"></span></div>
<div class="star star-3 star-odd"><span class="on"></span></div>
<div class="star star-4 star-even"><span class="on"></span></div>
<div class="star star-5 star-odd star-last"><span class="on"></span></div>
</div></div></div>



';
  $handler->display->display_options['fields']['field_reference_3']['element_label_colon'] = FALSE;
  /* Sort criterion: Global: Random */
  $handler->display->display_options['sorts']['random']['id'] = 'random';
  $handler->display->display_options['sorts']['random']['table'] = 'views';
  $handler->display->display_options['sorts']['random']['field'] = 'random';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'home_reviews' => 'home_reviews',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['home_page_reviews_'] = $view;

  return $export;
}
