<?php
/**
 * @file
 * home_page_items.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function home_page_items_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_home-reviews:node/add/home-reviews.
  $menu_links['navigation_home-reviews:node/add/home-reviews'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/home-reviews',
    'router_path' => 'node/add/home-reviews',
    'link_title' => 'Home Reviews',
    'options' => array(
      'identifier' => 'navigation_home-reviews:node/add/home-reviews',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Home Reviews');

  return $menu_links;
}
