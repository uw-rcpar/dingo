<?php
/**
 * @file
 * home_page_items.features.inc
 */

/**
 * Implements hook_views_api().
 */
function home_page_items_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function home_page_items_image_default_styles() {
  $styles = array();

  // Exported image style: home_review_thumbnail.
  $styles['home_review_thumbnail'] = array(
    'label' => 'home_review_thumbnail',
    'effects' => array(
      32 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 265,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: homepage_blog_image.
  $styles['homepage_blog_image'] = array(
    'label' => 'homepage_blog_image',
    'effects' => array(
      29 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 450,
          'height' => 450,
        ),
        'weight' => 3,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function home_page_items_node_info() {
  $items = array(
    'home_reviews' => array(
      'name' => t('Home Reviews'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function home_page_items_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: comment_node_home_reviews
  $schemaorg['comment']['comment_node_home_reviews'] = array(
    'rdftype' => array(
      0 => 'sioc:Post',
      1 => 'sioct:Comment',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'comment_body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'pid' => array(
      'predicates' => array(
        0 => 'sioc:reply_of',
      ),
      'type' => 'rel',
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
  );

  // Exported RDF mapping: home_reviews
  $schemaorg['node']['home_reviews'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_review_1' => array(
      'predicates' => array(),
    ),
    'field_review_2' => array(
      'predicates' => array(),
    ),
    'field_review_3' => array(
      'predicates' => array(),
    ),
    'field_reference_1' => array(
      'predicates' => array(),
    ),
    'field_reference_3' => array(
      'predicates' => array(),
    ),
    'field_reference_2' => array(
      'predicates' => array(),
    ),
    'field_video_thumbnail' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_video_thumbnail_1' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_video_thumbnail_2' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_video_thumbnail_3' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_video_link_1' => array(
      'predicates' => array(),
    ),
    'field_video_link_2' => array(
      'predicates' => array(),
    ),
    'field_video_link_3' => array(
      'predicates' => array(),
    ),
  );

  return $schemaorg;
}
