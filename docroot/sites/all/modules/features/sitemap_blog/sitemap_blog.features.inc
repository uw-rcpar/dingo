<?php
/**
 * @file
 * sitemap_blog.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sitemap_blog_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
