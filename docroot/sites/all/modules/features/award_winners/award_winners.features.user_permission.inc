<?php
/**
 * @file
 * award_winners.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function award_winners_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create award_winners content'.
  $permissions['create award_winners content'] = array(
    'name' => 'create award_winners content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any award_winners content'.
  $permissions['delete any award_winners content'] = array(
    'name' => 'delete any award_winners content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own award_winners content'.
  $permissions['delete own award_winners content'] = array(
    'name' => 'delete own award_winners content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'ds_switch award_winners'.
  $permissions['ds_switch award_winners'] = array(
    'name' => 'ds_switch award_winners',
    'roles' => array(),
    'module' => 'ds_extras',
  );

  // Exported permission: 'edit any award_winners content'.
  $permissions['edit any award_winners content'] = array(
    'name' => 'edit any award_winners content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own award_winners content'.
  $permissions['edit own award_winners content'] = array(
    'name' => 'edit own award_winners content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
