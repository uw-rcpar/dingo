<?php
/**
 * @file
 * award_winners.features.inc
 */

/**
 * Implements hook_views_api().
 */
function award_winners_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function award_winners_node_info() {
  $items = array(
    'award_winners' => array(
      'name' => t('Award Winners'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
