<?php
/**
 * @file
 * award_winners.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function award_winners_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:award_winners.
  $config['node:award_winners'] = array(
    'instance' => 'node:award_winners',
    'disabled' => FALSE,
    'config' => array(
      'title' => array(
        'value' => '2017 Watts Sells Award Winner: [node:field-name] | Roger CPA Review',
      ),
    ),
  );

  return $config;
}
