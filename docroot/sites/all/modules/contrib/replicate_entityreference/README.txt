Replicate Entityreference Extends Replicate module to manage the cloning of
entities referenced in entityreference fields.

Basics:
-------
When you clone an entity (node, taxonomy term, ...) containing an
entityreference, the referenced entities are not duplicated, and the cloned
entity still references the same entities as the original entity.
In some cases you may want to replicate the referenced entities when the parent
entity is replicated.

Replicate Entityreference adds two additional settings to the entityreference
field to allow site-builders to determine the behavior and implements a clean
duplication of referenced entities.
The replication is recursive, i.e will work on referenced entities containing
other entityreference fields and so on.

Dependencies:
-------------
- Replicate
- Entity API
- Entityreference

Configuration:
--------------
Each Entity Reference field will have two new instance settings:

- Replicate referenced entities when replicating host entity
  This option will deep-clone all the entities that are referenced through this
  field for the attached entity being replicated.
- Replicate host entities when replicating referenced entity
  This option will deep-clone all the entities that reference the entity being
  replicated.

Example:
Consider two entity types.

- Type A
- Type B, has an Entity Reference field to Type A

Also consider:
- Entity 1 of Type A
- Entity 2 of Type B, has a reference to Entity 1

When "Replicate referenced entities when replicating host entity" is chosen for
the Entity Reference field:

- Replicating Entity 2 will result in:
  - Replica of Entity 2
  - Replica of Entity 1
  - Replica of Entity 2 will reference the Replica of Entity 1
- Replicating Entity 1 will have no other effect, meaning it will result in:
  - Replica of Entity 1
  - Entity 2 will still reference Entity 1

When "Replicate host entities when replicating referenced entity" is chosen for
the Entity Reference field:

- Replicating Entity 1 will result in:
  - Replica of Entity 2
  - Replica of Entity 1
  - Replica of Entity 2 will reference the Replica of Entity 1
- Replicating Entity 2 will have no other effect, meaning it will result in:
  - Replica of Entity 2
  - Entity 2 will still reference Entity 1
  - Replica of Entity 2 will also reference Entity 1

