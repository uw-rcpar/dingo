<?php
/**
 * @file
 * Main methods of Replicate Entityreference module.
 */

/**
 * Implements hook_entity_insert().
 *
 * @see replicate_entityreference_replicate_entity_alter()
 */
function replicate_entityreference_entity_insert($entity, $entity_type) {
  // Only work after replicating an entity.
  if (isset($entity->replicate_entityreference_original_entity_id)) {
    $original_id = $entity->replicate_entityreference_original_entity_id;
    list($entity_id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
    $fields = field_info_field_map();

    $ops = array();

    foreach ($fields as $field_name => $field_info) {
      // Skip fields that aren't entityreference fields.
      if ($field_info['type'] != 'entityreference') {
        continue;
      }

      $field = field_info_field($field_name);
      // Skip any fields that don't target this entity type.
      if ($field['settings']['target_type'] != $entity_type) {
        continue;
      }

      // Go through all the instances of the field.
      foreach ($field_info['bundles'] as $host_type => $host_bundles) {
        foreach ($host_bundles as $host_bundle) {
          // Skip any instances that aren't set to replicate referencing entities.
          $instance = field_info_instance($host_type, $field_name, $host_bundle);
          if (!$instance['settings']['replicate_entityreference_referencing_entities']) {
            continue;
          }

          // Find all entities of the given type and bundle that reference the
          // original node that was being replicated.
          $efq = new EntityFieldQuery();
          $efq->entityCondition('entity_type', $host_type)
            ->entityCondition('bundle', $host_bundle)
            ->fieldCondition($field_name, 'target_id', $original_id);
          $result = $efq->execute();
          if (isset($result[$host_type])) {
            $host_ids = array_keys($result[$host_type]);
            if (count($host_ids)) {
              $host_entities = entity_load($host_type, $host_ids);
              foreach ($host_entities as $host_entity) {
                $clone = replicate_clone_entity($host_type, $host_entity);
                if ($clone) {
                  foreach ($clone->{$field_name} as $language => $values) {
                    // Find references to the original and replace with a
                    // reference to the replica.
                    foreach ($values as $delta => $value) {
                      if ($value['target_id'] == $original_id) {
                        $clone->{$field_name}[$language][$delta]['target_id'] = $entity_id;
                      }
                    }
                  }
                  entity_save($host_type, $clone);
                }
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Implements hook_field_info_alter().
 */
function replicate_entityreference_field_info_alter(&$info) {
  // Add an instance setting to entityreference fields to determine whether the
  // referenced entities on this field should be replicated when replicating
  // the host entity.
  if (isset($info['entityreference'])) {
    $info['entityreference']['instance_settings']['replicate_entityreference_referenced_entities'] = FALSE;
    $info['entityreference']['instance_settings']['replicate_entityreference_referencing_entities'] = FALSE;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function replicate_entityreference_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  // Only applies to entityreference fields.
  if ($form['#field']['type'] == 'entityreference') {
    // Add a field to the Field UI edit form for our settings.
    $default = $form['#instance']['settings']['replicate_entityreference_referenced_entities'];
    $form['instance']['settings']['replicate_entityreference_referenced_entities'] = array(
      '#type' => 'checkbox',
      '#title' => t('Replicate referenced entities when replicating host entity'),
      '#default_value' => $default,
    );

    $default = $form['#instance']['settings']['replicate_entityreference_referencing_entities'];
    $form['instance']['settings']['replicate_entityreference_referencing_entities'] = array(
      '#type' => 'checkbox',
      '#title' => t('Replicate host entities when replicating referenced entity'),
      '#default_value' => $default,
    );
  }
}

/**
 * Implements hook_help().
 */
function replicate_entityreference_help($path, $arg) {
  switch ($path) {
    case 'admin/help#replicate_entityreference':
      // Return a line-break version of the module README.txt.
      return check_markup(file_get_contents(dirname(__FILE__) . "/README.txt"));
  }
}

/**
 * Implements hook_replicate_entity_alter().
 *
 * @see replicate_entityreference_entity_insert()
 */
function replicate_entityreference_replicate_entity_alter(&$replica, $entity_type, $original) {
  // Since our replica hasn't been saved yet, we don't have the new entity_id.
  // So, in order to replicate referencing entities, we need to set a flag on
  // the replica with the original entity_id.
  list($id, $vid) = entity_extract_ids($entity_type, $original);
  $replica->replicate_entityreference_original_entity_id = $id;
  $replica->replicate_entityreference_original_revision_id = $vid;
}

/**
 * Implements hook_replicate_field_FIELD_TYPE().
 */
function replicate_entityreference_replicate_field_entityreference($entity, $entity_type, $field_name) {
  $field = field_info_field($field_name);
  // Sanity check, make sure it is an entityreference field before continuing.
  if ($field['type'] != 'entityreference') {
    return;
  }

  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $instance = field_info_instance($entity_type, $field_name, $bundle);
  $target_type = $field['settings']['target_type'];

  if ($instance['settings']['replicate_entityreference_referenced_entities']) {
    foreach ($entity->$field_name as $language => $values) {
      foreach ($values as $delta => $value) {
        if (!empty($value['target_id'])) {
          $new_target_id = replicate_entity_by_id($target_type, $value['target_id']);
          $entity->{$field_name}[$language][$delta] = array(
            'target_id' => $new_target_id,
          );
        }
      }
    }
  }
}
