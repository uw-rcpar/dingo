Commerce Upsell provides tools to add related products directly to the add to cart form that visitors can add by simply checking a box when adding a product to the cart.

To use the module:
* Add a product reference field to a product to hold the upsell product(s).
* Uncheck "Render fields from the referenced products when viewing this entity." This isn't required, but is probably what you want, since you don't want to render the related product's fields along with the actual product.
* Check "Use this field as an upsell field on add to cart forms."
* Configure the available options and save the field.
* Add some upsell products to the product
* ...
* Profit!

Sponsored by Kilpatrick Design