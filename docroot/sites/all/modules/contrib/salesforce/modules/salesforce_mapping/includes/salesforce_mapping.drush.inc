<?php

/**
 * @file
 * Drush integration for Salesforce_mapping.
 */

/**
 * Implements hook_drush_command().
 */
function salesforce_mapping_drush_command() {
  $items['sf-prune-revisions'] = [
    'description' => 'Delete old revisions of Mapped Objects, based on revision limit settings. Useful if you have recently changed settings, or if you have just updated to a version with prune support.',
    'aliases' => ['sfprune'],
    'arguments' => [
      'id' => 'Prune single entity.',
    ],
    'examples' => [
      'drush sfprune' => 'Run revision pruning for all entities.',
      'drush sfprune 1234' => 'Run revision pruning with an argument for mapping object ID "1234".',
    ],
  ];
  return $items;
}

/**
 * @param bool $id
 */
function drush_salesforce_mapping_sf_prune_revisions($id = FALSE) {

  $limit = variable_get('limit_mapped_object_revisions', 0);
  if ($limit <= 0) {
    drush_log('Mapped Object revisions limit is 0. No action taken.', 'warning');
    return;
  }

  if ($id) {
    drush_log("Running revision prune for id: $id", 'info');
    salesforce_mapping_prune_revisions($id);
    return;
  }

  $revision_table = 'salesforce_mapping_object_revision';
  // Create an object of type SelectQuery
  $query = db_select($revision_table, 'r');
  $query->fields('r', ['salesforce_mapping_object_id']);
  $query->having('COUNT(r.salesforce_mapping_object_id) > ' . $limit);
  $query->groupBy('r.salesforce_mapping_object_id');
  $ids = $query->execute()->fetchCol();

  if (empty($ids)) {
    drush_log("No Mapped Objects with more than $limit revision(s). No action taken.", 'warning');
    return;
  }

  drush_log('Found ' . count($ids) . ' mapped objects with excessive revisions. Will prune to ' . $limit . ' revision(s) each. This may take a while.', 'ok');
  $total = count($ids);
  $i = 0;
  $buckets = ceil($total / 20);
  if ($buckets == 0) {
    $buckets = 1;
  }

  foreach ($ids as $id) {
    if ($i++ % $buckets == 0) {
      drush_log("Pruned $i of $total records.", 'ok');
    }

    salesforce_mapping_prune_revisions($id);

  }
}
