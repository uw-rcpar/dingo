<?php

/**
 * Validate the address entered on checkout form.
 */
function commerce_avatax_validate_address($address) {
  $avatax = commerce_avatax_object();

  if (!$avatax) {
    watchdog('commerce_avatax', 'Could not connect to AvaTax for address validation.');
    return NULL;
  }

  // Prepare the address resolve request, we need to map the addressfield
  // components to the format expected by Avatax.
  $parameters = array();
  $address_mapping = commerce_avatax_address_fields_mapping();

  // Prepare the address to be validated.
  foreach ($address as $key => $value) {
    if (!isset($address_mapping[$key])) {
      continue;
    }
    $parameters[$address_mapping[$key]] = $value;
  }

  // Call the Avatax API for validating the address.
  $result = $avatax->addressesResolve($parameters);

  if (empty($result['success'])) {
    return array();
  }

  // If the address could not validated, return an empty array.
  if (is_array($result['result']) && isset($result['result']['messages'])) {
    $message = reset($result['result']['messages']);

    if ($message['severity'] === 'Error') {
      return array();
    }
  }

  // Return the list of validated addresses returned by the API call.
  if (!empty($result['result']['validatedAddresses'])) {
    return $result['result']['validatedAddresses'];
  }

  return array();
}

/**
 * Compare entered address and the validated addresses returned by AvaTax.
 */
function commerce_avatax_address_compare($original, array $validated_addresses = array()) {
  $result = array(
    'result' => '',
    'msg' => '',
    'suggestions' => array(),
  );

  // If the address couldn't be validated, stop there.
  if (empty($validated_addresses)) {
    $result['result'] = 'invalid';
    $result['msg'] = '<p>' . t('We could not validate the address entered. Please check that you have entered the correct address.') . '</p>';
    $result['msg'] .= '<p>' . t('Entered address is:') . '</p>' . theme('commerce_avatax_address', array('address' => $original));
    return $result;
  }
  // Map the response back to Addressfield names.
  $address_mapping = commerce_avatax_address_fields_mapping(TRUE);
  $valid_address = array();
  $address_mismatch = FALSE;

  // Create an array of field keys to compare.
  $keys_to_compare = drupal_map_assoc(array(
    'thoroughfare',
    'premise',
    'locality',
    'administrative_area',
    'country',
  ));

  $suggestions = array();
  // Loop over the validated addresses returned by Avatax.
  foreach ($validated_addresses as $validated_address) {
    foreach ($validated_address as $key => $value) {
      if (!isset($address_mapping[$key])) {
        continue;
      }
      $address_component_key = $address_mapping[$key];
      $valid_address[$address_component_key] = $value;

      if (!isset($keys_to_compare[$address_component_key])) {
        continue;
      }

      // Compare the user entered address & the validated address.
      if ($valid_address[$address_component_key] != $original[$address_component_key]) {
        $address_mismatch = TRUE;
      }
    }

    // Validate the postal code.
    $post_code_full_validation = variable_get(COMMERCE_AVATAX_VAR_PREFIX . 'address_postal_code', TRUE);
    $autocomplete_post_code = variable_get(COMMERCE_AVATAX_VAR_PREFIX . 'autocomplete_postal_code', TRUE);
    $validated_postal_code = $valid_address['postal_code'];
    $original_postal_code = $original['postal_code'];

    // Perform validation on the shortened zipcode version if necessary.
    if (!$post_code_full_validation || $autocomplete_post_code) {
      $validated_postal_code = substr($validated_postal_code, 0, 5);
      $original_postal_code = substr($original_postal_code, 0, 5);
    }
    // If there's no mismatch between the address entered and the validated
    // address, check if the zipcode matches.
    if (!$address_mismatch) {
      $address_mismatch = $validated_postal_code == $original_postal_code;
    }

    // Fill the addresses suggestions array.
    $suggestions[] = $valid_address;
  }

  if ($address_mismatch) {
    $result['result'] = 'needs_correction';
    $form = drupal_get_form('commerce_avatax_address_suggestion_form', $original, $suggestions);
    $result['msg'] = drupal_render($form);
    $result['suggestions'] = $suggestions;
  }
  else {
    $result['result'] = 'valid';
    $result['msg'] = '';
    $result['suggestions'] = $suggestions;
  }

  return $result;
}
