<?php

/**
 * Field handler to present an Affirm monthly payment message.
 */
class commerce_affirm_handler_field_monthly_payment_message_product extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['product_id'] = 'product_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $type = $this->options['commerce_affirm_page_type'];
    $product_id = $this->get_value($values, 'product_id');
    $product = commerce_product_load($product_id);
    $price = commerce_product_calculate_sell_price($product);

    $variables = array(
      'amount' => $price['amount'],
      'type' => $type,
      'product' => $product,
    );
    return theme('commerce_affirm_monthly_payment_message_' . $type, $variables);
  }


  function option_definition() {
    $options = parent::option_definition();
    $options['commerce_affirm_page_type'] =  array('default' => 'category');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['commerce_affirm_page_type'] = array(
      '#type' => 'select',
      '#options' => _commerce_affirm_page_types(),
      '#default_value' => $this->options['commerce_affirm_page_type'],
      '#title' => t('Affirm page type'),
      '#description' => t('The data-page-type attribute of the monthly payment message html element.'),
    );
  }

}
