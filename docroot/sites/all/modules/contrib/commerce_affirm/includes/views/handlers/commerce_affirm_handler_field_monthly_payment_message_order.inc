<?php

/**
 * Defines an area handler that renders an order's total field.
 */
class commerce_affirm_handler_field_monthly_payment_message_order extends views_handler_area {

  function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      // First look for an order_id argument.
      foreach ($this->view->argument as $argument) {
        if ($argument instanceof commerce_order_handler_argument_order_order_id) {
          // If it is single value...
          if (count($argument->value) == 1) {
            // Load the order.
            if ($order = commerce_order_load(reset($argument->value))) {
              $total = field_get_items('commerce_order', $order, 'commerce_order_total');
              $type = $this->options['commerce_affirm_page_type'];
              $variables = array(
                'amount' => $total[0]['amount'],
                'type' => $type,
              );
              return theme('commerce_affirm_monthly_payment_message', $variables);
            }
          }
        }
      }
    }
    return '';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['commerce_affirm_page_type'] =  array('default' => 'category');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['commerce_affirm_page_type'] = array(
      '#type' => 'select',
      '#options' => _commerce_affirm_page_types(),
      '#default_value' => $this->options['commerce_affirm_page_type'],
      '#title' => t('Affirm page type'),
      '#description' => t('The data-page-type attribute of the monthly payment message html element.'),
    );
  }

}
