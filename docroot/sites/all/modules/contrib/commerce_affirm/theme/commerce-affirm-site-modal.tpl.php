<?php

/**
 * @file
 * Default theme implementation to present the Affirm site modal.
 *
 * Available variables:
 * - $text: The link text.
 * - $type: The data-page-type attribute of the affirm html element.
 */
?>
<?php drupal_add_js(drupal_get_path('module', 'commerce_affirm') . '/js/commerce_affirm.js', array('weight' => -10)); ?>
  <a class="affirm-site-modal" data-page-type="<?php print $type; ?>"><?php print $text; ?></a>
