<?php
//Template for Courses tabs
//Variables:
//$active_ones
//$no_active_ones
//print arg(0) . arg(1);
$list = [];//html is saved here before rendering it to be able to sort it
?>
<?php if ((arg(0)=='node' && arg(1)=='6491') || (arg(0)=='dashboard' && arg(1)=='my-cram-courses') || (arg(0)=='node' && arg(1)=='15948')) { ?>
  <div class="course-outline-title-wrapper"><h2 class="course-outline-title">Course Outline</h2></div>
<?php } ?>
<div id="my-courses-tabs" class="tabs-wrapper" role="tabpanel">
  <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#course-activation" id="execute-modal-activation-after-ajax" style="display:none">
  </button>
  <div id="activation-container-of-values"></div>
  <div id="cram-status-message"></div>
  <ul class="nav nav-tabs" role="tablist">
    <?php
    $i = 0;
    foreach ($active_ones as $a_key => $a_value) {
      $i++; 
      ob_start();?>
      <li role="presentation" class="tab-item <?php print $a_value["class"] ?> tab-<?php print $i; ?> <?php print $a_key ?>-tab col-sm-3">
        <div class="radial-wrapper">
          <a href="#tab-<?php print $i; ?>" aria-controls="tab-<?php print $i; ?>" role="tab" data-toggle="tab" class="course-radial-link">
            <div class="radial-element" id="<?php print str_replace("-CRAM", "", $a_key); ?>">
              <p class="element-value"> <?php print $a_value['value'] ?> </p>
            </div>
            <div class="expire-date-class"><?php print $a_value["expire_date"] ?> </div>
          </a>
        </div>
      </li>
    <?php 
   $list[$a_key] = ob_get_contents();
    ob_end_clean();    
    }

    foreach ($delayed_ones as $d_key => $d_value) {
      $i++; ob_start();?>
      <li role="presentation" class="tab-item tab-<?php print $i; ?> <?php print $d_key ?>-tab col-sm-3">
     <span class="activate-delayed-ones">
	     <div class="radial-wrapper">
         <a href="<?php  print "/activate-delayed-courses-modal/".$d_value."/nojs" ?>" target="_blank" class="element-link use-ajax course-radial-link">
         <div class="add-link">
             <div class="radial-element" id="<?php print $d_key ?>">
               <p class="element-value"> <?php print 0 ?> </p>
              <div class="add-course"><div class="element-link">Activate Course</div></div>
             </div>
            </div>
        </a>
	    </div>
    </span>
      </li>

      <?php
    $list[$d_key] = ob_get_contents();
    ob_end_clean();
    }
    $r = 0;
    foreach ($no_active_ones as $no_key => $no_value) {
      $r++; ob_start();?>
      <li role="presentation" class="tab-item empty-tab tab-<?php print $r; ?> <?php print str_replace("-CRAM", "", $no_key); ?>-tab col-sm-3">
        <div class="radial-wrapper">
         <?php if (!variable_get('enable_uworld_configurations', FALSE)) : ?>
          <a href="/<?php print drupal_get_path_alias('node/395'); ?>" target="_blank" class="element-link course-radial-link">
          <?php endif; ?>    
            <div class="add-link">
              <div class="radial-static-element" id="static-<?php print $no_key ?>"></div>
              <div class="element-value"><?php print str_replace("-CRAM", "", $no_key); ?></div>
              <?php if (!variable_get('enable_uworld_configurations', FALSE)) : ?>
              <div class="plus-sign">                
                <div class="element-link">+</div>
              </div>
              <div class="add-course">                
                <div class="element-link"> +ADD COURSE</div>
              </div>
              <?php endif; ?>
              <div class="clear-empty"></div>
            </div>
          <?php if (!variable_get('enable_uworld_configurations', FALSE)) : ?>
          </a>
         <?php endif; ?>
        </div>
      </li>
    <?php 
    $list[$no_key] = ob_get_contents();
    ob_end_clean();    
    }
    $sort = array_keys($list);
    sort($sort);
    foreach ($sort as $key) {
      echo $list[$key];
    }
    ?>
  </ul>
  <div class="tab-content">
    <?php
    $n = 0;
    foreach ($active_ones as $a_key => $a_value) {
      $n++; ?>

      <div role="tabpanel" class="tab-pane fade<?php if($a_value["class"] == "active"){ ?> active in<?php } ?> tab-content-<?php print str_replace("-CRAM", "", $a_key) ?>" id="tab-<?php print $n; ?>">

        <?php
        if(isset($notes_option)){
          print rcpar_dashboard_get_my_courses_notes_view($a_value["nid"],$a_value['exam_version']);
        }else{
          print rcpar_dashboard_get_my_courses_view($a_value["nid"],FALSE,$a_value['exam_version']);
        }
        ?>
      </div>

    <?php } ?>
  </div>
</div>