<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>



  <?php
  // adding the solr search form for questions and answers
  // check if module exists first, because it doesn't exist on local devs
  if (module_exists('apachesolr_search')) {

    // identify the solr search page we want to use
    $search_page = apachesolr_search_page_load('questions-answers');
    // include file from solr module
    module_load_include('inc', 'apachesolr_search', 'apachesolr_search.pages');
    // get the form
    $form = drupal_get_form('apachesolr_search_custom_page_search_form',$search_page);
    // add a class to the form
    $form['#attributes']['class'][] = 'qa-search-form';
    // give the form a unique id
    $form['#id'] = "qa-search-form";
    // change the action on the form
    $form['#action'] = "/questions-answers/search/";
    // add placeholder text to search input
    $form['basic']['keys']['#attributes']['placeholder'] = 'Have a specific question?';
    $form['basic']['submit']['#value'] = '<i class="icon glyphicon glyphicon-search" aria-hidden="true"></i>';
    //print_r($form);
    // render the form
    print '<div class="qa-search-form">' . drupal_render($form) . '</div>';
  }

  ?>


  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>