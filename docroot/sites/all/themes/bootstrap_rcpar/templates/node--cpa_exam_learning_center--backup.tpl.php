<?php
//print_r($node);
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
 <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_short_description']);
      hide($content['field_presentation']);
      hide($content['field_video_link']);
      hide($content['field_transcript']);
      hide($content['field_handout']);
      hide($content['field_ebooks_in_this_series']);
      hide($content['field_ebooks_in_this_series_link']);
      	
    ?>
  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
    <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
   

	<?php //if (isset($node->field_video_link['und']['0']['safe_value'])) : ?>
		<!--<video width="100%" id="video" controls>
			<source src="<?php print $node->field_video_link['und']['0']['safe_value']; ?>" type="video/mp4" />
		</video>-->
	<?php //endif; ?>
	

<?php

$block = module_invoke('learning_center', 'block_view', 'learning_center_videos');
print render($block['content']);
?>
	<div class="row">
		<div class="col-md-8 col-left">
			<div class="column-inner">
				<h2 class="secondary-title"><?php print $node->field_short_description['und']['0']['safe_value']; ?></h2>
				<?php print render($content); ?>
			</div>
		</div>
		<div class="col-md-4 col-right">
			<div class="column-inner">
			<?php print render($content['links']); ?>
			
			<?php 
				if (!empty($node->field_handout['und']['0']['uri'])) {
					$uri= $node->field_handout['und']['0']['uri'];
			 ?>
			 <div class="handout-link"><a href="<?php print file_create_url($uri); ?>">Download the handout <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></a></div>

			 <?php } ?>
			</div>

		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			
		<?php if ($node->field_transcript['und']['0']['safe_value']) { ?>
	
       		<div role="tabpanel" class="transcript-wrapper">
		   		<ul class="nav nav-tabs">
			   		<li role="presentation"><a class="full-transcript-link" id="transcript">Full transcript <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></li>
			   	</ul>
		
			   
				<div class="transcript">
					<?php print $node->field_transcript['und']['0']['safe_value']; ?>aaa
				</div>

			</div>

	<?php } ?>
	
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">

	
	
	
	
      <?php 
      
	if (!empty($node->field_ebooks_in_this_series['und']['0']['uri'])) {
		$imguri= $node->field_ebooks_in_this_series['und']['0']['uri'];
     } else {
	     $imguri = '';
     }
       ?>
       <div class="ebooks-wrapper">
       <h3>eBooks in this series</h3>
       <?php if ($imguri) { ?>
       <div class="ebooks"><a href="<?php print $node->field_ebooks_in_this_series_link['und']['0']['safe_value']; ?>"><img src="<?php print image_style_url('ebooks_img', $imguri); ?>" /></a></div>
       <?php } else { ?>
       None currently available. 
       <?php }  ?>
       </div>
		</div>
	</div>

     
     
  </div>

  <div class="row comments-heading-row">
  	<div class="col-md-6">
	  <h3 class="comment-count"><?php print $node->comment_count; ?> Comment<?php if ($node->comment_count != 1) { ?>s<?php } ?></h3>
     </div>
  	<div class="col-md-6">
	  	<a class="add-comment-link">Add comment <span class="glyphicon glyphicon-comment" aria-hidden="true"></span></a>
  	</div>  	
  </div>
  <?php print render($content['comments']); ?>
</div>
