<?php
//Template for Audio Courses
//Variables:
//courses
?>

  <div class="upsell-courses audio-courses dashboard">
    <h2 class="block-title"> You May Also Like </h2>
    <div class="upsell-courses-content audio-courses-content">
	    <?php 
	    if (!empty($upsell_products)) {
	    foreach ($upsell_products as $key => $value) {           	      
            $img = $value['img'];
            $label = $value['label'];
            $desc = $value['desc'];
            $section = $value['section'];          
	      ?>
	      <div class="upsell-course-item audio-course-item no-gutter" id=<?php print "upsell-$section" ?>> 
	      	<div class="upsell-img audio-img col-md-4 col-sm-4 col-xs-4"><img src=<?php print $img ?> ></div>
		  	<div class="col-md-8 col-sm-8 col-xs-8">
		  	  <h4 class="upsell-title audio-title"> <?php print $section ?> - <?php print $label; ?> </h4> 
		  	  <div class="upsell-description audio-description"> <?php print $desc ?> </div> 
		  	</div>
		  	<div class="clear-empty"></div>
	      </div>         
	    <?php } }?> 
    </div> 
  </div>



