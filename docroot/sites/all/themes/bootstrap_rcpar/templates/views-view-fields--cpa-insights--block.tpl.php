<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php
// overriding this template to only show video thumbnail image when it exists
// otherwise we get a broken image
// also need to only link the title when there is a link provided
//print_r($row);
foreach ($fields as $id => $field):
  $row_number = $view->row_index;
// display video thumbnail without wrapping divs and added html
  if ($field->class == 'field-video-thumbnail-path') {
    $uri = $row->field_data_field_video_field_video_thumbnail_path;
    if (strpos($uri, 'public:') !== false) { ?>
      <div class="video-thumbnail-wrapper">
        <div class="<?php print $field->class; ?>"><a href="javascript: void(0);"  data-toggle="modal" data-target="#myModal-<?php print $row_number; ?>">
            <div class="play-button"></div><img src="<?php print image_style_url("insights_image_and_video_thumbnail", $uri); ?>" /></a></div>
        <div class="modal fade cpa-insight-modal" id="myModal-<?php print $row_number; ?>" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <?php print render($row->field_field_video); ?>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
      </div>
    <?php
    }
  // display title with link if link field is not empty
  } else if ($field->class == 'title' && isset($row->field_field_title_link) && !empty($row->field_field_title_link)) { ?>
    <div class="views-field-title"><a href="<?php print $view->render_field("field_title_link", $view->row_index); ?>" target="_blank"><?php print render($row->node_title); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
  <?php
  }
  else if ($field->class == 'body' && isset($row->field_field_full_text) && !empty($row->field_field_full_text)) { ?>

    <div class="partial-<?php print $row_number; ?> views-field-body field-partial" id="field-partial-text-<?php print $row_number; ?>">
     <?php print render($row->field_body); ?>
     <a id="full-<?php print $row_number; ?>" class="show-full-text"><i class="fa fa-plus" aria-hidden="true"></i>
     </a>
    </div>
    <div id="full-<?php print $row_number; ?>" class="full-<?php print $row_number; ?> views-field-body full">
      <?php print render($row->field_field_full_text); ?>
      <a id="partial-<?php print $row_number; ?>" class="show-partial-text"><i class="fa fa-minus" aria-hidden="true"></i>
      </a>
    </div>

    <?php

  }
  else {
  if (!empty($field->separator)):
    print $field->separator;
  endif;
  print $field->wrapper_prefix;
  print $field->label_html;
  print $field->content;
  print $field->wrapper_suffix;
}
endforeach;
?>