
<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>


<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<?php
// adding the solr search form for questions and answers
// check if module exists first, because it doesn't exist on local devs
if (module_exists('apachesolr_search')) {

  // identify the solr search page we want to use
  $search_page = apachesolr_search_page_load('questions-answers');
  // include file from solr module
  module_load_include('inc', 'apachesolr_search', 'apachesolr_search.pages');
  // get the form
  $form = drupal_get_form('apachesolr_search_custom_page_search_form',$search_page);
  // add a class to the form
  $form['#attributes']['class'][] = 'qa-search-form';
  // give the form a unique id
  $form['#id'] = "qa-search-form";
  // change the action on the form
  $form['#action'] = "/questions-answers/search/";
  // add placeholder text to search input
  $form['basic']['keys']['#attributes']['placeholder'] = 'Have a specific question?';
  $form['basic']['submit']['#value'] = '<i class="icon glyphicon glyphicon-search" aria-hidden="true"></i>';
  //print_r($form);
  // render the form
  print '<div class="qa-search-form">' . drupal_render($form) . '</div>';
}

?>


<?php
$number_of_rows = count($rows);
$firsthalf = array_slice($rows, 0, round($number_of_rows / 2));
$secondhalf = array_slice($rows, round($number_of_rows / 2));
?>
<div class="col-md-6 column-left">
<?php foreach ($firsthalf as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
</div>


<div class="col-md-6 column-right">
<?php foreach ($secondhalf as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
</div>
