<?php
//Template for Exam Review Content
//Variables:
//$uid
//$exams
//$average
$all_exams = rcpar_dashboard_entitlements_crams_options();
$exams = empty($exams) ? array() : $exams;
$daemon_hide_link = rcpar_dasboard_existing_courses_are_delayed($exams);
$launch_style = empty($exams) || $daemon_hide_link ? 'style="display:none"' : 'style="display:block"';
$launch = l(t('Launch Cram Course Outline <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'), 'dashboard/my-cram-courses', array('html' => TRUE));
?>

<div id="cram-status-message"></div>
<div id="cram-exams-review-content" class="exam-ipq-links">
  <?php $i = 0;
  foreach ($all_exams as $exam) {
    $i++; ?>
    <?php if (in_array($exam, array_keys($exams))) : ?>
      <div class="<?php print $exam; ?>-wrapper course-detail-wrapper col-md-3 col-sm-6 col-xs-6 <?php print 'section-' . $i; ?>">
        <?php
        /* ***************************************

          DISPLAY REGULAR EXAM RADIALS

        ************************************** */
        ?>
        <?php

        if (!isset($exams['is_delayed']) || !in_array($exam, $exams['is_delayed'])) {

          ?>
          <a href="/dashboard/my-cram-courses/<?php print str_replace("-CRAM", "", $exam); ?>">
            <div class="radial-element" id="<?php print $exam; ?>">
              <p class="element-value"> <?php print $exams[$exam] ?> </p>
            </div>
          </a>
          <span class="exam-ipq-link cram-course-ipq-link">&nbsp;</span>
          <div class="launch-course-link" <?php print $launch_style ?>><a href="/dashboard/my-cram-courses/<?php print str_replace("-CRAM", "", $exam); ?>">Course outline ></a><?php //echo $launch ?></div>
          <?php
        }
        ?>
        <?php
        /* ***************************************

            DISPLAY ACTIVATE COURSE RADIALS

          ************************************** */
        ?>
        <?php
        if (isset($exams['is_delayed']) && is_array($exams['is_delayed'])) {
          if (in_array($exam, $exams['is_delayed'])) {
            $delayed_nid = array_search($exam, $exams['is_delayed']);
            ?>
            <div class="radial-element">
              <div class="add-link exam-<?php print $exam ?>">
			        <span class="activate-delayed-ones">
                <a href="<?php print "/activate-delayed-courses-modal/" . $delayed_nid . "/nojs" ?>" target="_blank" class="element-link use-ajax">
                  <div class="radial-static-element" id="static-<?php print $exam ?>"></div>
                  <div class="element-value"><?php print str_replace("-CRAM", "", $exam); ?></div>
                  <div class="plus-sign"><div class="element-link">+</div></div>
                  <div class="add-course"><div class="element-link">Activate Course</div></div>
                  <div class="clear-empty"></div>
                </a>
              </span>
                <span class="exam-ipq-link">&nbsp;</span>
              </div>
            </div>

            <?php
          }
        }
        ?>
      </div>
    <?php else: ?>

      <?php
      /* ***************************************

               DISPLAY ADD COURSE RADIALS

       ************************************** */
      ?>
      <div class="<?php print $exam; ?>-wrapper course-detail-wrapper col-md-3 col-sm-6 col-xs-6 <?php print 'section-' . $i; ?>">
        <div class="radial-element">
          <div class="add-link exam-<?php print $exam ?>">
           <?php if (!variable_get('enable_uworld_configurations', FALSE)) : ?>
            <a href="/<?php echo drupal_get_path_alias('node/395'); ?>" target="_blank" class="element-link">
            <?php endif; ?>                
              <div class="radial-static-element" id="static-<?php print $exam ?>"></div>
              <div class="element-value"><?php print str_replace("-CRAM", "", $exam); ?></div>
              <?php if (!variable_get('enable_uworld_configurations', FALSE)) : ?>
              <div class="plus-sign">
                <div class="element-link">+</div>
              </div>
              <div class="add-course">                
                <div class="element-link">ADD COURSE</div>
              </div>
              <?php endif; ?>
              <div class="clear-empty"></div>
            <?php if (!variable_get('enable_uworld_configurations', FALSE)) : ?>
            </a>
            <?php endif; ?>
            <span class="exam-ipq-link">&nbsp;</span>
          </div>
        </div>
      </div>

    <?php endif ?>
  <?php } ?>
  <div class="clear-empty"></div>
</div>
