<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<?php $uri = $row->field_field_video_thumbnail_1['0']['rendered']['#item']['uri']; ?>
<div class="testimonial-slideshow-wrapper clearfix">
  <div class="testimonial-slideshow-video">
    <div class="testimonial-slideshow-video-image hidden-xs">
      <a href="javascript: void(0);" data-target="#modal<?php print $row->nid; ?>" data-toggle="modal"><?php print $fields['field_video_thumbnail_1']->content; ?><img src="/sites/default/files/btn-play.png" class="play-btn"/></a></div>
  </div>
  <div class="video-thumbnail-wrapper visible-xs"> <video id="video<?php print $row->nid; ?>" class="video testimonial-video" controls controlsList="nodownload" poster="<?php print image_style_url('testimonials_video_thumbnail', $uri); ?>">
      <source src="<?php print $row->field_field_video_link_1['0']['rendered']['#markup']; ?>">
    </video></div>
  <div class="testimonial-copy"><?php print $row->field_body['0']['rendered']['#markup']; ?></div>
</div>


<!-- -- modal ---->
<div class="modal fade video-modal" id="modal<?php print $row->nid; ?>" role="dialog" data-backdrop="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="clear-empty"></div>
      </div>
      <div class="modal-body">
        <div class="video-wrapper">

          <video id="video<?php print $row->nid; ?>" class="video testimonial-video" controls controlsList="nodownload" poster="<?php print image_style_url('testimonials_video_thumbnail', $uri); ?>">
            <source src="<?php print $row->field_field_video_link_1['0']['rendered']['#markup']; ?>">
          </video>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->
