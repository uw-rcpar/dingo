<?php

print render($form['form_id']);
print render($form['form_build_id']);
print render($form['form_token']);

// determine if we are editing a profile or resetting a password
$password_reset_page = 0;
if (arg(0) == 'user' && arg(1) == 'reset' && in_array('anonymous user', $user->roles)) {
  $password_reset_page = 1;
}
/****** PASSWORD RESET PAGE ******/
if ($password_reset_page) { ?>
  <div class="dashboard-wrapper">
    <div class="profile-wrapper no-gutter">
      <div class="row">
        <div class="col-xs-12">
          <h2 class="profile-heading">Reset Password</h2>
        </div>
      </div>
    </div>
  </div>

<div id="user-edit-<?php print $user->uid; ?>" class="user-edit-form container no-gutter">
  <div class="user-edit-section password-row single-column no-gutter">
    <?php print render($form['account']['pass']); ?>
    <?php print render($form['account']['current_pass']); ?>



      <div class="college-info <?php if (!isset($form['#user']->field_did_you_graduate_college[LANGUAGE_NONE][0]['value'])
        || !isset($form['#user']->field_graduation_month_and_year[LANGUAGE_NONE][0]['value'])
        || !isset($form['#user']->field_when_do_you_plan_to_start[LANGUAGE_NONE][0]['value'])) {
        print ' show-college-info'; }  ?>">
        <div class="college-info-required-fields">Please fill out any required fields below.</div>
      <?php print render($form['sales_funnel_widget']); ?>
      </div>

        <?php
        $form['actions']['submit']['#value'] = 'Save Profile Updates';
        print render($form['actions']); ?>

  </div>
</div>

<?php }

/****** USER PROFILE EDIT PAGE ******/
else { ?>
    <div class="dashboard-wrapper">
      <div class="profile-wrapper no-gutter">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="profile-heading">My Profile</h2>
          </div>
        </div>
      </div>
    </div>

    <div id="user-edit-<?php print $user->uid; ?>" class="user-edit-form container no-gutter">
      <div class="user-edit-section username-email-row no-gutter">
        <div class="col-sm-6">
          <div class="user-column-left">
            <?php print render($form['account']['name']); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="user-column-right">
            <?php print render($form['account']['mail']); ?>
          </div>
        </div>
        <div class="clear-empty"></div>
      </div>

      <div class="user-edit-section name-pass-row no-gutter">
        <div class="col-sm-6">
          <div class="user-column-left">
            <?php print render($form['field_first_name']); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="user-column-right">
            <?php print render($form['field_last_name']); ?>
          </div>
        </div>
        <div class="clear-empty"></div>
      </div>
        <!--
      <div class="user-edit-section password-row single-column no-gutter">
        <?php //print render($form['account']['pass']); ?>
      </div>
      -->
      <div class="user-edit-section password-row no-gutter">
          <!--
        <div class="col-sm-6">
          <div class="user-column-left">
            <?php //print render($form['account']['current_pass']); ?>
          </div>
        </div>
        -->
        <div class="col-sm-6">
          <div class="user-column-right">
            <?php
            $form['actions']['submit']['#value'] = 'Save Profile Updates';
            //$vars['element']['#attributes']['class'][] = $class;
            print render($form['actions']); ?>
          </div>
        </div>
        <div class="clear-empty"></div>
      </div>

      <div class="user-edit-divider password-reset-hidden"></div>

      <div class="user-edit-section picture-row single-column">
        <?php print render($form['picture']['picture_current']); ?>
        <?php print render($form['picture']['picture_upload']); ?>
        <?php print render($form['picture']['picture_delete']); ?>
        <div class="clear-empty"></div>
      </div>

      <div class="user-edit-divider"></div>

      <div class="user-edit-section timezone-row single-column">
        <?php print render($form['timezone']['timezone']); ?>
      </div>

      <?php
      // STUDENT FIELDS
      if (in_array('student enrolled', $user->roles) || in_array('student progress reporting', $user->roles) || in_array('student non-enrolled', $user->roles)) { ?>


        <div class="user-edit-divider"></div>
        <div class="user-edit-section student-row no-gutter">
          <div class="col-sm-6">
            <div class="user-column-left">
              <?php
              if (isset($form['field_country'])) {
                print render($form['field_country']);
              } ?>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="user-column-right">
              <?php if (isset($form['field_student_state'])) {
                print render($form['field_student_state']);
              } ?>
            </div>
          </div>
          <div class="clear-empty"></div>
        </div>

        <div class="user-edit-divider"></div>
        <div class="user-edit-section student-row college-info single-column">
          <?php print render($form['sales_funnel_widget']); ?>
        </div>
      <?php } ?>


      <?php
      // PROFESSOR FIELDS
      if (in_array('professor', $user->roles)) {
        //print 'professor';
        ?>

        <div class="user-edit-divider"></div>
        <div class="user-edit-section professor-row no-gutter">
          <div class="col-sm-6">
            <div class="user-column-left">
              <?php print render($form['field_college_state']); ?>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="user-column-right">
            </div>
          </div>
          <div class="clear-empty"></div>
        </div>


        <div class="user-edit-divider"></div>
        <div class="user-edit-section no-gutter">
          <div class="col-sm-6">
            <div class="user-column-left">
              <?php print render($form['field_courses_taught']); ?>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="user-column-right">
              <?php print render($form['field_interests']); ?>
            </div>
          </div>
          <div class="clear-empty"></div>
        </div>

      <?php } ?>
      <div class="user-edit-section actions-row">
        <?php
        print render($form['field_registration_type']);
        $form['actions']['submit']['#value'] = 'Save Profile Settings';
        print render($form['actions']); ?>
      </div>


    </div>
  <?php
}?>

