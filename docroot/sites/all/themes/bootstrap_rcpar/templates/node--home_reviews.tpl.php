<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php
// We hide the comments and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
          <?php
          $object = entity_metadata_wrapper('node', $node);
          $indexes = array(1, 2, 3);

          foreach ($indexes as $i) {
            try {
            $thumbnail_field = 'field_video_thumbnail_' . $i;
            $videolink_field = 'field_video_link_' . $i;
            $vtt_field = 'field_vtt_file_' . $i;
            ?>
          <div class="review-column col-<?php print $i; ?> col-sm-4">
            <div class="review-wrapper">
              <div class="review">
                <?php if (!empty($object->{$thumbnail_field}->value())) { // hide the thumbnail/video html if no thumbnail exists ?>
                  <div class="thumbnail-wrapper hidden-xs">
                    <a href="javascript: void(0);" data-target="#reviewModal<?php print $i; ?>" data-toggle="modal">
                      <?php print render($content[$thumbnail_field]); ?>
                      <i class="fa fa-play play-btn" aria-hidden="true"></i>
                    </a>
                  </div>
                  <div class="phone-video-wrapper visible-xs">
                    <video id="video<?php print $i; ?>" class="video" controls="" poster="<?php print file_create_url($object->{$thumbnail_field}->value()['uri']); ?>">
                      <source src="<?php print $object->{$videolink_field}->value(); ?>">
                      <?php if (!empty($object->{$vtt_field}->value())) { ?>
                        <track label="English" kind="subtitles" srclang="en" src="<?php print $object->{$vtt_field}->value(); ?>" default="">
                      <?php } ?>
                    </video>
                  </div>
                <?php } ?>
                <div class="quote-icon-wrapper"><img src="/sites/default/files/images/icon-quote.svg" alt="quotemark icon" /></div>
                <?php print render($content['field_review_' . $i]); ?>
                <div class="review-attribution"><?php print render($content['field_reference_' . $i]); ?></div>
              </div>
            </div>
            <?php if (!empty($object->{$thumbnail_field}->value()['uri'])) { // only include the modal if the thumbnail exists ?>
            <! -- modal -->
            <div class="modal fade review-modal" id="reviewModal<?php print $i; ?>" role="dialog" data-backdrop="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <div class="clear-empty"></div>
                  </div>
                  <div class="modal-body">
                    <div class="video-wrapper">
                      <video id="video<?php print $i; ?>" class="video" controls="" poster="<?php print file_create_url($object->{$thumbnail_field}->value()['uri']); ?>">
                        <source src="<?php print $object->{$videolink_field}->value(); ?>">
                        <?php if (!empty($object->{$vtt_field}->value())) { ?>
                        <track label="English" kind="subtitles" srclang="en" src="<?php print $object->{$vtt_field}->value(); ?>" default="">
                        <?php } ?>
                      </video>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- end modal -->
            <?php } ?>
          </div>
          <?php
            } // end try
            catch (EntityMetadataWrapperException $e) {
            }
          } // end for each
          ?>

  </div><!-- /content -->
</div> <!-- /node -->
