
<?php
$expired_groups = isset($_GET['expired_groups']) ? $_GET['expired_groups'] : 0;
?>
<?php print rcpar_dashboard_get_details(1) ?> 
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#video-details" id="execute-modal-after-ajax" style="display:none">
</button>
<div id="container-of-values"></div>

<div id="monitoring-tabs" class="tabs-wrapper" role="tabpanel">
    <div class="monitoring-filter">
        <ul class="nav nav-tabs" role="tablist">
            <li class="tab-item<?php if (!$expired_groups) { ?> active<?php } ?> tab-1" role="presentation"><a class="monitoring-filter-link all-groups-filter btn btn-default" href="/dashboard/monitoring-center">Active groups</a></li>

            <li class="tab-item <?php if ($expired_groups) { ?> active<?php } ?> tab-2" role="presentation"><a class="monitoring-filter-link all-groups-filter btn btn-default" href="/dashboard/monitoring-center?expired_groups=1">Expired groups</a></li>
            <?php if (!empty($partner_groups)) { ?>  
              <li class="col-sm-2 selected-filter-label">Select a Group Filter: </li>
              <li class="tab-item dropdown tab-3 col-sm-10" role="presentation"><a id="myTabDrop1" class="dropdown-toggle select-filter" aria-controls="myTabDrop1-contents" data-toggle="dropdown" href="#" aria-expanded="false"><span class="caret"></span>   <?php
                      $t = 0;
                      foreach ($partner_groups as $node_key => $node_groups) {
                        $t++;
                        $name = $groups_info[$node_key];
                        ?>        
                        <span class="<?php print drupal_html_class($name); ?>-content filter-name<?php if ($t == 1) { ?> active<?php } ?> gid-<?php print $node_key; ?>" id="<?php print drupal_html_class($name); ?>-name"><?php print $name; ?></span>


                        <?php
                      }
                      ?></a>
                  <ul id="myTabDrop1-contents" class="dropdown-menu" aria-labelledby="myTabDrop1">

                      <?php
                      $a = 0;
                      foreach ($partner_groups as $node_key => $node_groups) {
                        $a++;
                        $name = $groups_info[$node_key];
                        if ($a == 1) {
                          ?>        
                          <li role="presentation" class="tab-item group-tab-<?php print $a; ?> tab-<?php print $node_key ?> active">      
                            <?php }
                            else { ?>        
                          <li role="presentation" class="<?php if (isset($row_class)) {
                                print $row_class;
                              } ?> tab-item group-tab-<?php print $a; ?> tab-<?php print $node_key ?>">      
    <?php } ?>         
                              <a href="/rcpar/monitoring-tab/<?php print "$expired_groups/{$node_key}" ?>/nojs" aria-controls="tab-<?php print $a; ?>" role="tab" data-toggle="tab" class="dropdown-list use-ajax ajax-link" id="<?php print drupal_html_class($name); ?>">          
                              <?php print $name; ?>        
                            </a>

                        </li>    
  <?php } ?>   



                  </ul>

              </li>
    <?php } ?> 

        </ul>
    </div>      
<?php if (empty($partner_groups)) { ?>      

      <div class='empty-monitoring-center'>  
          <p> No content to display</p>
      </div>
<?php }
else { ?>          
      <div class="selected-tab-filter">Selected filter: 
      </div>

      <div class="tab-content">
          <?php
          
          reset($partner_groups);
          $first_partner_nid = key($partner_groups);
          $vars = array(
            'partner_groups' => $partner_groups[$first_partner_nid],
            'first_partner_nid' => $first_partner_nid
          );

          print theme('rcpar_dashboard_monitoring_tab_content', $vars);
          ?>

      </div>  <!-- tab-content  -->
<?php } ?>    
</div> <!-- #monitoring-tabs -->
