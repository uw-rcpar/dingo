<?php
extract($variables);

$hide_prices = false;
if (module_exists('rcpar_partners') && isset($_SESSION['rcpar_partner'])) {
  $hide_prices = rcpar_partners_hide_prices();
}
?>
<div class="message-inner">
  <div class="added-product-title clearfix">
    <div class="cart-image"><img src="/sites/all/themes/bootstrap_rcpar/css/img/icon-cart.svg" alt="Shopping cart icon" /></div>
    <div class="cart-added">Added to Your Cart<span class="cart-modify">You can modify your order in <?php print l('your cart', 'cart'); ?>.</div>
    <div class="cart-added-header"><span class="pull-left">PRODUCT</span><?php if (!$hide_prices) { ?><span class="pull-right">PRICE</span><?php } ?></div>
  </div>
  <div class="button-wrapper">
  	<?php if (arg(0)=='cart') { ?>
	    <div class="button continue"><a class="commerce-add-to-cart-confirmation-close-link commerce-add-to-cart-confirmation-close" href="/<?php print drupal_get_path_alias(); ?>"><?php echo t('Continue') ?></a></div>
  	<?php } else { ?>
      <?php
      // We intentionally make the 'checkout' link go to the cart page because the upsells appear there, and we want to
      // force the user to see these before going purchasing. ?>
      <div class="button checkout"><a class="btn-primary" href="<?php print url('cart'); ?>">CHECKOUT <img src="/<?php print path_to_theme(); ?>/css/img/cart-added-checkout.png"></a></div>
      <?php
      /* These are the original cart and checkout links before
       * <div class="button checkout"><a href="<?php print url('checkout'); ?>">CHECKOUT <img src="/<?php print path_to_theme(); ?>/css/img/cart-added-checkout.png"></a></div>
       * <div class="button continue"><?php echo l(t('View cart'), 'cart') ?></div>
       */
      ?>

	    <div class="button continue"><a class="commerce-add-to-cart-confirmation-close-link" href="/<?php print drupal_get_path_alias(); ?>"><?php echo t('Continue shopping') ?></a></div>
  	<?php } ?>
  	  <div class="cart-added-help">
    	  <div class="cart-questions">Questions?</div>
    	  <div class="divider"></div>
    	  <div class="cart-here">We're here for you!</div>
    	  <div class="cart-call hidden">Call Us: <span>1-877.764.4272</span></div>
    	  <div class="cart-chat"><a href="/contact">Chat With Us</a></div>
    	  
  	  </div>
  </div>
  <?php echo views_embed_view('confirm_message_product_display', 'default', $line_item_ids); ?>
  <a class="commerce-add-to-cart-confirmation-close" href="#">
    <span class="element-invisible">X</span>
  </a>
</div>

