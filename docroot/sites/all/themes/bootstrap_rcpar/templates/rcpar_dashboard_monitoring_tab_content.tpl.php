<?php
// If the old/original 'ipq' module is still enabled in this environment, we're still using the original monitoring
// center, and we want to load its original JS/CSS assets.
if(module_exists('ipq')) {
  drupal_add_js(drupal_get_path('module','ipq') . '/js/ipq-modal.js');
  drupal_add_css(drupal_get_path('module', 'ipq') . '/css/ipq.css');
}
else {
  // New monitoring center JS/CSS
  drupal_add_js(drupal_get_path('module','ipq_common') . '/js/ipq-modal.js');
  drupal_add_css(drupal_get_path('module', 'ipq_common') . '/css/ipq.css');
}

global $user;
$rcpar_dashboard_entitlements_options = rcpar_dashboard_entitlements_options();
ksort($rcpar_dashboard_entitlements_options);
          $y = 1;
          ?>
          <div role="tabpanel" class="active in tab-pane fade tab-content-<?php print $first_partner_nid ?>" id="tab-<?php print $y . '-main'; ?>"> 
          <?php $index = 0;
          foreach ($partner_groups['groups'] as $group_key => $group_info) { $index++;   ?>
          		<?php $accordion_id = key($group_info); ?>
                <div class="panel-group" id="accordion-<?php print $y . '-' . drupal_html_class($accordion_id); ?>" role="tablist" aria-multiselectable="true" >
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading<?php print $index; ?>">				                    
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-<?php print $y . '-' . drupal_html_class($accordion_id); ?>" href="#collapse<?php print $index; ?>" aria-expanded="true" aria-controls="collapse<?php print $index; ?>" class="collapsed"><?php print key($group_info); ?> </a></h4>				                
                        </div>
                        <div id="collapse<?php print $index; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php print $index; ?>">
                            <div class="panel-body">
                                <!--Header Group Info-->				                        
                                <div class="header-group-info group-material">				                            
                                    <label>CPA Exam Material</label> 				                            				                            
                                        <?php 
                                        $partner_sections = rcpar_dashboard_monitoring_get_partner_access_material($first_partner_nid);
                                        if (isset($partner_sections['fieldset_content'])) { ?>
								                                          
								          <div class="section-list">              
       
        
										        <script type="text/javascript">
										
										        (function ($) {
									            	$('body').on('click','#partner-section-show-more',function(){
											            $('.all-sections').height('auto');
											            $('.show-more').hide();
											            $('.show-less').show();
											        });  
									            	$('body').on('click','#partner-section-show-less',function(){
											            $('.all-sections').height('20px');
											            $('.show-more').show();
											            $('.show-less').hide();
											        });  
										        })(jQuery);
										
										      </script>  
	                                          <?php
	                                          $all_ps_rows = array();
	                                          $all_ps = '';
	                                          $ps_rows_string = '';
	                                          $ps_rows_divider = '';
	                                          $c = 0;
	                                          $total = count($partner_sections['fieldset_content']);
	                                          foreach ($partner_sections['fieldset_content'] as $ps_sku => $ps_rows) {
	                                            $c++;
	                                            if ($ps_sku != '') {
	                                              $ps_rows_string = "<strong> $ps_sku: </strong>" . implode(', ', $ps_rows);
	                                              //print "<b> $ps_sku: </b>" . implode(', ', $ps_rows) ;
	                                            }
	                                            else {
	                                              $ps_rows_string = implode(', ', $ps_rows);
	                                            }
	                                            
	                                            $c != $total ? $ps_rows_divider = ' | ' : $ps_rows_divider = '. ';
	                                            $all_ps_rows[] = $ps_rows_string . $ps_rows_divider;
	                                          }
	                                          $all_ps = implode('', $all_ps_rows);
	                                          ?>
	                                          <div class="sections-wrapper">
	                                          	<div class="partial-sections"><?php echo mb_strimwidth($all_ps, 0, 205, ''); ?></div>
	                                          	<div class="all-sections"><?php print $all_ps; ?></div>
	                                          </div>
	                                          
	                                          <button id="partner-section-show-more" class="partner-section-show-hide show-more btn btn-default">Show more</button>
	                                          <button id="partner-section-show-less" class="partner-section-show-hide show-less btn btn-default">Show less</button>
	                                          
                                          </div>
                                             
                                     
                                    <?php } else { ?>
                                         
								        <div class="section-list">
	                                          <div class="sections-wrapper">
									        <?php
			                                    $c = 0;
			                                    foreach ($partner_sections as $ps_sku => $ps_rows) {
			                                      print "<b> $ps_sku: </b>" . implode(', ', $ps_rows) ;
			                                      print $c == count($partner_sections) ? ' | ' : '';
			                                      $c++;
			                                    } ?>
	                                          </div>
		                                 </div>
									<?php } ?>
                                </div>

                                <div class="header-group-info group-dates col-sm-12">
                                    <div class="header-group-info group-start-date col-sm-3">
                                        <label>Access Start</label> <?php print $group_info[key($group_info)]['start_date'] ?>
                                    </div>

                                    <div class="header-group-info group-end-date col-sm-3">
                                        <label>Access End</label> <?php print $group_info[key($group_info)]['end_date'] ?>
                                    </div>
                                    <div style="clear: both;"></div>
                                </div><!--Ends Header Group Info-->
                                <!--Email List Group Info-->
                                <div class="group-email-list col-sm-12">
                                    <div>
                                        <div class="row">                                            
                                            <div class="col-sm-12">                                                    
                                                <div class="roster-heading col-sm-4">Roster</div>                                                    
                                                  <?php foreach ($rcpar_dashboard_entitlements_options as $sku) { ?>                                                                               
                                                      <div class="section-heading section-heading-<?php print $sku ?> col-sm-2"> <?php print $sku ?></div>
                                                  <?php } ?>
                                            </div>
                                        </div>
                                                <?php
                                                $i = 0;
                                                foreach ($group_info as $group_info_value) {
                                                  $email_count = count($group_info_value['students_info']);
                                                  foreach ($group_info_value['students_info'] as $email => $student_info) {
                                                  $i++; 
                                                  $ipq_classes = array('modal-load', 'link-disabled');
                                                  $section  = t("no entitlemts");
                                                  $sections = array();
                                                  $PID = isset($student_info['pid']) ? $student_info['pid'] : 0;
                                                  if ($student_info['uid'] &&  $group_key){                                                                  
                                                    $list = rcpar_partner_get_access_from_list($student_info['uid'],$PID,TRUE);
                                                    if(count($list) > 0){
                                                      if(isset($list[$user->uid])){
                                                        $ipq_classes = array('modal-load');
                                                        $sections = array_flip($list[$user->uid]);
                                                        $section = array_pop($list[$user->uid]);  
                                                      }                        
                                                    }
                                                  }
                                                  ?>
                                                  <div class="groups-wrapper row<?php if ($email_count == $i) { ?> last-row<?php } ?>">
                                                  <div class="group-wrapper" id="group-wrapper-<?php print $i; ?>-<?php print drupal_html_class($accordion_id); ?>">
												  <div class="monitoring-center-groups row"> 
                                                      <div class="group-row col-sm-12">
                                                          <div class="group-participant-email col-sm-4"><?php print $email; ?></div>
                                                          <?php foreach ($rcpar_dashboard_entitlements_options as $sku) { 
                                                            $percentage = '-';
                                                                if(isset($student_info['progress'][$sku]) && isset($sections[$sku])){
                                                                    $percentage =  $student_info['progress'][$sku] . '%';
                                                                }
                                                                
                                                            ?>                                                                               
		                                                  <div class="group-row-data group-row-data-<?php print $sku; ?> col-sm-2">
		                                                     <?php print $percentage	?>
		                                                  </div>                                             
                                                              <?php }  ?>                                                                                            
                                                          <div style="clear: both;"></div>
                                                      </div> 
												  </div>
                                                  </div>
                                                  <div class="group-wrapper-<?php print $i; ?>-<?php print drupal_html_class($accordion_id); ?>-overlay group-wrapper-overlay">
												  <div class="monitoring-center-groups row">                                  
                                                      <div class="group-row col-sm-12">  
                                                          <div class="group-participant-email active col-sm-4"><?php print $email; ?></div>                        
                                                          <div class='group-row-options col-sm-8'>
                                                              <?php
                                                              if($section != "no entitlemts"){
                                                                print l('LECTURE DETAILS', "ajax/monitoring/center/tabs/{$PID}/" . $student_info['uid'].'/'.$section,  array('attributes' => array('class' => 'modal-load ipq-lecture-link' )) );
                                                                print l('IPQ SCORE', '/dashboard/testcenter/history/'.$student_info['uid'].'/'.$group_key.'/'.$section,  array('attributes' => array('class' => 'modal-load ipq-score-link' )) );
                                                                print l('IPQ OVERVIEW', '/ajax/dashboard/testcenter/overview/'.$student_info['uid'].'/'.$group_key.'/'.$section,  array('attributes' => array('class' => 'modal-load ipq-overview-link' )) ); 
                                                              }else{ ?>
                                                              <div class='disabled-links no-active'>
                                                                <a href="#" class="ipq-lecture-link">LECTURE DETAILS</a>
                                                                <a href="#" class="ipq-score-link">IPQ SCORE</a>
                                                                <a href="#" class="ipq-overview-link">IPQ OVERVIEW</a>
                                                              </div>
                                                              <?php } ?>
                                                              
	                                                      </div>                                                          
                                                      </div>
												  </div>
                                                  </div>
                                                  </div>
                                                      <?php 
                                                   }
                                                }
                                                ?>
                                    </div>    
                                </div>
                                <!--End Email List Group Info-->
                            </div> <!-- panel body -->
                        </div> <!-- panel-collapse body -->                                             
                    </div> <!-- panel-default body -->
                </div> <!-- panel group --> 
  <?php } ?>

              <div class="modal fade" id="myModal">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                          </div>
                          <div class="modal-body">
                              <p></p>
                          </div>
                      </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->

          </div>
