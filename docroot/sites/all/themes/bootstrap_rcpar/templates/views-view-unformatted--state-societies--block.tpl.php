<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */

?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<form>
<select ONCHANGE="location = this.options[this.selectedIndex].value;" class="selectpicker">
<option value="">- Select One -</option>
<?php foreach ($rows as $id => $row): ?>

    <?php print $row; ?>

<?php endforeach; ?>
</select>
</form>
