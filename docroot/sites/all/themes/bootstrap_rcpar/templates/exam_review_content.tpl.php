<?php
//Template for Exam Review Content
//Variables:
//$uid
//$exams
//$average
$all_exams = rcpar_dashboard_entitlements_options();
$exams = empty($exams) ? array() : $exams;
$daemon_hide_link = rcpar_dasboard_existing_courses_are_delayed($exams);
$launch_style = empty($exams) || $daemon_hide_link ? 'style="display:none"' : 'style="display:block"';
$launch = l(t('Launch Course Outline <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>'), 'dashboard/my-courses', array('html' => TRUE));
//With this partner we validate that ASL / FSL / FREETRIAL don't apply
//to versioning, that with its method isBillingFreeAccess.

global $user;
$u = new \RCPAR\User($user);
// Note that a user might have get some entitlements from a partner and purchased others or even
// got them from another partner
// that's why we are creating an array with the partner information, indexed per entitlement SKU

// also, some messages are supposed to be display only in case we do have some entitlements
// where the user can change the exam version

$show_ev_change_global_msgs = false;
$entitlements_partners = array();
foreach ($all_exams as $exam) {
  if (in_array($exam, array_keys($exams))){
    $partner_id = $user->user_entitlements['products'][$exam]['partner_nid'];
    if ($partner_id){
      $entitlements_partners[$exam] = new RCPARPartner($partner_id);
      if (!$entitlements_partners[$exam]->isBillingFreeAccess()){
        // At least one entitlements is not from a ASL/FSL/freetrial order, we should show the messages
        $show_ev_change_global_msgs = true;
      }
    }
    else {
      // At least one entitlements is not from a ASL/FSL/freetrial order, we should show the messages
      $show_ev_change_global_msgs = true;
    }
  }
}
?>
<div id="cpa-status-message"></div>
<div id="exams-review-content" class="exam-ipq-links<?php if(exam_version_is_versioning_on()) { ?> exam-versions-exist<?php } ?>">
  <?php
  // For users who had unlimited access entitlements but have let them fully lapse, we'll display an extra message.
  // Note that this could possibly display for entitlements that came from cancelled orders, which is why it's
  // purposefully vague/generic.
  if($u->getValidEntitlements()->filterByOnlineCourse()->isEmpty() && $u->getAllEntitlements()->filterByUnlimitedAccess()->isNotEmpty()) {
    $msg = variable_get('unlimited_contact_notice', 'Need to reactivate your Unlimited access?  Please contact our support team for more information.');
    print "<div class=\"exam-version-notice\">$msg<br><br></div>";
  }
  ?>

  <?php if($show_ev_change_global_msgs && exam_version_is_versioning_on()) : ?>
    <div class="exam-version-notice">
      Tell us which Exam version you're taking and we'll customize your materials
      <a data-toggle="modal" data-target="#more-details-modal" class="more-details-please">What? More details please</a>
    </div>
  <?php endif ?>
  <?php
  $i = 0;
  foreach ($all_exams as $exam) {
    $i++;
    if (in_array($exam, array_keys($exams))) : ?>
      <div class="<?php print $exam; ?>-wrapper course-detail-wrapper col-md-3 col-sm-6 col-xs-6 <?php print 'section-' . $i; ?>">
        <?php
        /* ***************************************

          DISPLAY REGULAR EXAM RADIALS

        ************************************** */
        ?>
      <?php if(exam_version_is_versioning_on()) : ?>
          <div class="exam-version-selector">
            <?php if(!isset($entitlements_partners[$exam]) || !$entitlements_partners[$exam]->isBillingFreeAccess()) : ?>
              <?php if(!$exams_versions[$exam]): ?>
                <a href="#" class="open-exam-version-modal" data-section="<?php print $exam; ?>">Select your course materials</a>
              <?php elseif($exams_versions[$exam] == '2017') : ?>
                <div class="course-materials-label version-2017">2017 Course Materials</div>
              <?php else: ?>
                <div class="course-materials-label version-2016">2016 Course Materials</div>
                <a href="#" class="open-exam-version-modal" data-section="<?php print $exam; ?>">Upgrade to 2017</a>
              <?php endif ?>
            <?php endif ?>
          </div>
        <?php
      endif;
        if (!isset($exams['is_delayed']) || !in_array($exam, $exams['is_delayed'])) {

          ?>
          <a href="/dashboard/my-courses/<?php print $exam ?>">
            <div class="radial-element" id="<?php print $exam ?>">
              <p class="element-value"> <?php print $exams[$exam] ?> </p>
            </div>
          </a>
          <div class="launch-course-link" <?php print $launch_style ?>><a href="/dashboard/my-courses/<?php print $exam ?>">Course outline ></a><?php //echo $launch ?></div>
          <?php
          // Show a link to this section of IPQ, but only if we have access to it.
          if(ipq_common_access($exam)) {
            print l('IPQs >', 'ipq/overview', array('query' => array('section' => $exam), 'attributes' => array('class' => array('exam-ipq-link'))));
          }

          // If the user has access to this course but it is expiring soon, we'll tack on an "extend my access" link to the
          // regular display.
          if(in_array($exam, array_keys($expired_unlimited_exams))) {
              print l('Extend My Course', 'unlimited-access/activation', array('attributes' => array('class' => array('exam-ipq-link'))));
          }
        }
        ?>
        <?php
        /* ***************************************

            DISPLAY ACTIVATE COURSE RADIALS

          ************************************** */
        ?>
        <?php
        if (isset($exams['is_delayed']) && is_array($exams['is_delayed'])) {
          if (in_array($exam, $exams['is_delayed'])) {
            $delayed_nid = array_search($exam, $exams['is_delayed']);
            ?>
            <div class="exam-version-selector"></div>
            <div class="radial-element">
              <div class="add-link exam-<?php print $exam ?>">
			      <span class="activate-delayed-ones">
			      <a href="<?php print "/activate-delayed-courses-modal/" . $delayed_nid . "/nojs" ?>" target="_blank" class="element-link-all use-ajax">
			      	<div class="radial-static-element" id="static-<?php print $exam ?>"></div>
				  	  <div class="element-value"><?php print $exam ?></div>
			        <div class="plus-sign"><div class="element-link">+</div></div>
			        <div class="add-course"><div class="element-link">Activate Course</div></div>
			        <div class="clear-empty"></div>
			      </a>
			      </span>
                <div class="launch-course-link" <?php print $launch_style ?>><a href="/dashboard/my-courses/<?php print $exam ?>">Course outline ></a><?php //echo $launch ?></div>
                <?php
                // Show a link to this section of IPQ, but only if we have access to it.
                if (ipq_common_access($exam)) {
                  print l('IPQs >', 'ipq/overview', array(
                    'query' => array('section' => $exam),
                    'attributes' => array('class' => array('exam-ipq-link'))
                  ));
                }
                ?>
              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>
    <?php elseif (in_array($exam, array_keys($expired_unlimited_exams))) : ?>

      <?php
      /* ***************************************

               DISPLAY EXTEND MY ACCESS RADIALS

       ************************************** */
      ?>

        <div class="<?php print $exam; ?>-wrapper course-detail-wrapper col-md-3 col-sm-6 col-xs-6 <?php print 'section-' . $i; ?>">
            <a href="/unlimited-access/activation" target="_blank" class="element-link-all">
            <div class="exam-version-selector"></div>
            <div class="radial-element" id="<?php print $exam ?>">
                <p class="element-value"> <?php print $expired_unlimited_exams[$exam] ?> </p>
                <div class="add-link exam-<?php print $exam ?>">

                        <div class="add-course">
                            <br>
                            <div class="element-link">Extend My Course</div>
                        </div>
                        <div class="clear-empty"></div>
                </div>
            </div>
            </a>
        </div>


    <?php else: ?>

      <?php
      /* ***************************************

               DISPLAY ADD COURSE RADIALS

       ************************************** */
      ?>
      <div class="<?php print $exam; ?>-wrapper course-detail-wrapper col-md-3 col-sm-6 col-xs-6 <?php print 'section-' . $i; ?>">
        <div class="exam-version-selector"></div>
        <div class="radial-element">
          <div class="add-link exam-<?php print $exam ?>">
            <a href="/<?php echo drupal_get_path_alias('node/12'); ?>" target="_blank" class="element-link-all">
              <div class="radial-static-element" id="static-<?php print $exam ?>"></div>
              <div class="element-value"><?php print $exam ?></div>
              <div class="plus-sign">
                <div class="element-link">+</div>
              </div>
              <div class="add-course">
                <div class="element-link">ADD COURSE</div>
              </div>
              <div class="clear-empty"></div>
            </a>
          </div>
        </div>
      </div>

    <?php endif ?>
  <?php } ?>
  <div class="clear-empty"></div>
</div>

<?php
/* ***************************************

    "What? More details please" modal

 ************************************** */
?>

<?php if($show_ev_change_global_msgs  && exam_version_is_versioning_on()) : ?>
  <div class="modal exam-version-modal" id="more-details-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Effective April 2017, the CPA Exam <br>is undergoing significant changes</h4>
        </div>
        <div class="modal-body">
          <p>But rest assured - we've got you covered. Simply <strong>let us know whether you're taking the exam before or after April 1, 2017</strong> and we'll customize your course materials accordingly.</p>
          <p>Everything from your video lectures to content in the Interactive Practice Questions will be geared specifically to your selected Exam version.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">OK, got it!</button>
          <div class="tell-me-more"><a href="/cpa-exam/changes" target="_blank">Tell me more</a></div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<?php endif ?>