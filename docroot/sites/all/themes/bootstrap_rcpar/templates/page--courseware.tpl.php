<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['hello_bar']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['header']: Items for the header region.
 * - $page['lower_navigation']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<?php print ($variables['page_tracking_codes']); ?>
<?php if (!empty($page['hello_bar'])): ?>
	<div class="hello-bar container">
		<div class="row">
			<div class="col-sm-12">
				<?php print render($page['hello_bar']); ?>
			</div>
		</div>	
	</div>
<?php endif; ?>
<nav id="cbp-spmenu-s2" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
    <div id="dashboard-close-x" class="dashboard-close-button"></div>
     <?php if (!empty($page['lower_navigation'])): ?>
         <div class="navigation-nav">
             <?php print render($page['lower_navigation']); ?>
         </div>
    <?php endif; ?>
    <?php if (!empty($page['header'])): ?>
        <?php print render($page['header']); ?>
    <?php endif; ?>
</nav>

<!-- **************************** -->
<!-- NOTIFCATIONS -->
<!-- **************************** -->
<nav id="cbp-spmenu-notif" class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
</nav>
<div class="overlay-container"></div>
<!-- **************************** -->
<!-- END NOTIFCATIONS -->
<!-- **************************** -->


<div class="overlay-container"></div>

<?php print render($page['content']); ?>

