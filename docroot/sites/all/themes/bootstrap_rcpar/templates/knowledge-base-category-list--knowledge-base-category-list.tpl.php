<?php

/**
 * @file
 * Default theme implementation for entities.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="content"<?php print $content_attributes; ?>>
    <h2 class="browse-by-category"><?php print $title; ?></h2>
    <div class="category-box-wrapper categories-<?php print count($knowledge_base_category_list->field_category_reference['und']); ?>">
    <?php
    foreach ($knowledge_base_category_list->field_category_reference['und'] as $category) {?>
      <a href="/customer-care/<?php print arg(1);?>/search?cat=<?php print str_replace("_","-",$category['entity']->field_machine_name['und']['0']['value']); ?>" class="category-box">
      <?php $category_icon = image_style_url('kb_category_icon', $category['entity']->field_icon['und']['0']['uri']); ?>
        <div class="category-icon"><img src="<?php print $category_icon; ?>" alt="Icon representing <?php print $category['entity']->name; ?>"></div>
        <div class="category-name"><?php print $category['entity']->name; ?></div>
      </a>

    <?php } ?>
    </div>

  </div>
</div>
