<?php 
/*
  IPQ Link of actual page: http://testcenter.rogercpareviewdev.com/testmodule/setup.php#/my-scores?section=aud
*/
global $user;
$sections  = rcpar_partners_get_section_ids();
$list = rcpar_partner_get_access_from_list($uid,$pid,TRUE);
$u_sections = $list[$user->uid];
?>

<?php
    $a_classes = array('ajax-nav ipq-nav ipq-nav-aud');
    $b_classes = array('ajax-nav ipq-nav ipq-nav-bec');
    $r_classes = array('ajax-nav ipq-nav ipq-nav-reg');
    $f_classes = array('ajax-nav ipq-nav ipq-nav-far');

    switch(strtoupper($section)){
        case 'AUD':
            $a_classes[] = 'active';
            break;
        case 'BEC':
            $b_classes[] = 'active';
            break;
        case 'REG':
            $r_classes[] = 'active';
            break;
        case 'FAR':
            $f_classes[] = 'active';
            break;
    }
?>
<input type="hidden" id="details-uid" value="<?php print $uid?>">
<input type="hidden" id="details-pid" value="<?php print $pid?>">
<div id="setupcontainer" class="mcv-main-window">
	<div class="ipq-header score-history-header">
		<div class="container">
			<h3 class="top-heading pull-left col-sm-5"><?php print t('@section Lecture Detail', array('@section' => strtoupper($section))); ?></h3>
			<div class="ipq-header-nav pull-left col-sm-7">
                            <?php print in_array("AUD", $u_sections) ? 
                                l('AUD', "/ajax/monitoring/center/tabs/{$pid}/{$uid}/AUD", array('attributes' => array('class' => $a_classes, 'ipq-sec' => 'AUD'))) : 
                                l('AUD', '', array('attributes' => array('class' => array_merge($a_classes, array("no-active"))))); ?> 
                            <?php print in_array("BEC", $u_sections) ? 
                                l('BEC', "/ajax/monitoring/center/tabs/{$pid}/{$uid}/BEC", array('attributes' => array('class' => $b_classes, 'ipq-sec' => 'BEC'))) : 
                                l('BEC', '' . $uid  . '/BEC', array('attributes' => array('class' => array_merge($b_classes, array("no-active"))))); ?> 
                            <?php print in_array("FAR", $u_sections) ? 
                                l('FAR', "/ajax/monitoring/center/tabs/{$pid}/{$uid}/FAR", array('attributes' => array('class' => $f_classes, 'ipq-sec' => 'FAR'))) : 
                                l('FAR', '' . $uid  . '/FAR', array('attributes' => array('class' => array_merge($f_classes, array("no-active"))))); ?>
                            <?php print in_array("REG", $u_sections) ? 
                                l('REG', "/ajax/monitoring/center/tabs/{$pid}/{$uid}/REG", array('attributes' => array('class' => $r_classes, 'ipq-sec' => 'REG'))) : 
                                l('REG', '' . $uid  . '/REG', array('attributes' => array('class' => array_merge($r_classes, array("no-active"))))); ?> 
            </div>
		</div>
		<div class="user-information">
			<div class="container">
			    <div class="generated-on"> <span>Generated On:</span> <?php print format_date(time(), 'custom', 'n/j/Y g:ia'); ?></div>
			    <?php
			        $account = user_load($uid);
			        $email = '';
			        $fullname = '';
			        try {
			            $wapper = entity_metadata_wrapper('user', $account);
			            $email = $account->mail;
			            $fullname = $wapper->field_first_name->value() . ' ' . $wapper->field_last_name->value();
			        } catch (EntityMetadataWrapperException $exc) {
			            watchdog(__FILE__, "function ".__FUNCTION__." entity error");
			        }
			    ?>
			    <div class="user-fullname pull-left"><span class="user-fullname-label">Student:</span> <span class="user-fullname-wrapper"><?php print $fullname; ?></span></div>
			    <div class="user-email pull-left"><a href="mailto:<?php print $email; ?>"><?php print $email; ?></a></div>
			</div>
		</div>
    </div>

	
	<div class="ipq-content ipq-content-<?php print $section; ?>"></div>
	<div class="monitoring-center-courses monitoring-center-courses-<?php print $section; ?>">
  <?php      
      $version = exam_versions_get_entitlement_version($uid, $section);
      echo rcpar_dashboard_get_my_courses_view($sections[$section],FALSE,$version); ?>
	</div>
</div>
    