<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php print $output; ?>
<?php
    // if it's an offline lecture
    // we must also print the serial number
    global $user;

    if (strpos($_GET['q'], '/complete')) {
        $user_entitlements = user_entitlements_get_users_products($user, true);
        if(is_array($user_entitlements) && !empty($user_entitlements)){                
          foreach ($user_entitlements['products'] as $product) {
            if ( (count($product['installs']) == 0 || $product['expiration_date'] >= REQUEST_TIME) && isset($product['serial_number']) ) {
                if ($row->commerce_product_field_data_commerce_product_product_id == $product['product_id']) {
                    print '<div > Serial N°: <b>' . $product['serial_number'] . '</b></div>';
                }
            }
            if(exam_version_is_versioning_on() || exam_version_is_textbook_versioning_on()){
                // Attach textbook version  on product line item 
                if(isset($product['selected_textbook']) &&
                    $row->commerce_product_field_data_commerce_product_product_id == $product['product_id']){
                    print '<div > Textbook version: <b>' . $product['selected_textbook'] . '</b></div>';
                }
            }
          }
        }
    }

?>
