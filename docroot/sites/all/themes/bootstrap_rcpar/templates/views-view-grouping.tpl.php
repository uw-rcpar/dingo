<?php
/**
* @file
* This template is used to print a single grouping in a view.
*
* It is not actually used in default Views, as this is registered as a theme
* function which has better performance. For single overrides, the template is
* perfectly okay.
*
* Variables available:
* - $view: The view object
* - $grouping: The grouping instruction.
* - $grouping_level: Integer indicating the hierarchical level of the grouping.
* - $rows: The rows contained in this grouping.
* - $title: The title of this grouping.
* - $content: The processed content output that will normally be used.
*/
?>
<style>
@import url("/sites/all/themes/bootstrap_rcpar/css/style2.css?nkc8gi");
@import url("//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css");
@import url("/sites/all/themes/bootstrap/css/overrides.css?nkc8gi");
@import url("/sites/all/themes/bootstrap_rcpar/css/style.css?nkc8gi");
@import url("/sites/all/themes/bootstrap_rcpar/fonts/font-awesome/css/font-awesome.min.css?nkc8gi");
@import url("http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic&amp;subset=latin,latin-ext");
@import url("http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,500,500italic,700,700italic&amp;subset=latin,cyrillic-ext");
</style>
<div class="view-grouping">
	<h1><?php print $title; ?></h1>
	<div class="view-grouping-content">
	<?php print $content; ?>
	</div>
</div> 