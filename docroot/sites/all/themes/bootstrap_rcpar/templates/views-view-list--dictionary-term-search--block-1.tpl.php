<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="row">
  <?php 
  // get the current term ID from the URL
  $current_tid = arg(2);
   
  // count the number of results in the view - to be used in while loop below
  $view_result_count = count($view->result);
  
  // find the index of the current term in the view results object - to be used to find previous and next terms below
  foreach ($view->result as $id => $result):
    if ($result->tid === $current_tid) {
      $key = $id;
    }
  endforeach; 
  
  // start counting and doing loops -- look only for previous term and next term in the results
  $i = 0;
  while ($i <= $view_result_count) {
    foreach ($view->result as $id => $result):
      
      // previous term
      if ($i == $key-1) {
      // create the previous link term URL
      $previous_link = 'taxonomy/term/' . $result->tid;
       ?>
      <div class="term-list-link pull-left"><a href="<?php print url($previous_link); ?>">< <?php print $result->taxonomy_term_data_name; ?></a></div>
  <?php } ?>
      
      
  <?php  
      // next term
      if ($i == $key+1) { 
      // create the next link term URL
      $next_link = 'taxonomy/term/' . $result->tid;
      ?>
      <div class="term-list-link pull-right"><a href="<?php print url($next_link); ?>"><?php print $result->taxonomy_term_data_name; ?> ></a></div>
 <?php } ?>

    

<?php
  $i++;
    endforeach; // end for each loop
  } // end while
    
  ?>
</div>