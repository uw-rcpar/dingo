<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<div class="testimonial-author-wrapper">
  <?php
  $testimonial_author = user_load($row->users_node_uid);
  if ($testimonial_author->picture != NULL) { ?>
    <div class="testimonial-user-img"><?php print theme('user_picture', array('account' => $testimonial_author)); ?></div>
  <?php } ?>
  <div class="testimonial-author"><?php print $row->node_title; ?></div>
  <div class="testimonial-rating"><?php if(isset($row->field_field_rating[0]['rendered'])) { print render($row->field_field_rating[0]['rendered']); } ?></div>
</div>
<span class="schema-code" itemprop="author" itemscope itemtype="http://schema.org/Person">
<span class="schema-code" itemprop="name"><?php print $row->node_title; ?></span>
</span>
<span class="date-created schema-code"><meta itemprop="datePublished" content="2017-08-02">August 22nd, 2017</span>
<?php
$name = '';
if (is_numeric(arg(1))) {
  $product_node = node_load(arg(1));
  $name = $product_node->title;
}
else if (isset($row->_field_data['nid']['entity']->field_review_display_section['und'][0]['tid'])) {
  $term = taxonomy_term_load($row->_field_data['nid']['entity']->field_review_display_section['und'][0]['tid']);
  $name = $term->name;
}
?>
<span class="schema-code" itemprop="itemReviewed"  itemscope itemtype="http://schema.org/Product"><span itemprop="name"><?php print $name; ?></span></span>