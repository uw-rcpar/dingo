<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>    


<?php
// ******************************************************
//  teaser DISPLAY
// ******************************************************
?>

<?php
if (!$page) {

  $field_thumbnail_image = field_get_items('node', $node, 'field_thumbnail');
  //$thumbnail_image_url = file_create_url($field_thumbnail_image[0]['uri']);
  $thumbnail_image_url = $field_thumbnail_image[0]['uri'];
  ?>
  <div id="node-<?php print $node->nid; ?>" class="post <?php print $classes; ?>" <?php print $attributes; ?>>
      <div class="row content-row">

          <div class="blog-thumbnail col-md-2">
              <?php //print render($content['field_thumbnail']); ?>
              <?php
              //print theme('image_style', array('style_name' => 'blog_thumbnail', 'path' => $thumbnail_image_url));


              /* $filepath = '';
                if ($node->picture=='0') {
                $filepath = file_build_uri(variable_get('user_picture_default', ''));
                } elseif (isset($node->picture->uri)) {
                $filepath = $node->picture->uri;
                }
                //print $filepath;
                print theme('image_style', array('style_name' => 'blog_thumbnail', 'path' => $filepath));
                //print_r($node);

               */
              if (!empty($node->picture->uri)) {
                print theme('image_style', array('path' => $node->picture->uri, 'style_name' => 'blog_thumbnail'));
              } else {
                $custom_default_image_path = 'public://user-icon-md.png.png';
                print theme('image_style', array('path' => $custom_default_image_path, 'style_name' => 'blog_thumbnail'));
              }
              ?>
          </div>

          <div class="blog-teaser-info col-md-10">
              <?php print render($title_prefix); ?>
              <h2 class="blogTitle"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
              <?php print render($title_suffix); ?>

                  <?php if ($display_submitted): ?>
                <div class="submitted post-meta">
                    <?php
                    //print $submitted; 
                    //print_r($node);
                    ?>
                <?php print format_date($node->created, 'custom', 'l, F j, Y'); ?><span class="with-comment-count"> | <?php print $node->name; ?> <?php if ($node->comment == '2') { ?>| <a href="<?php print $node_url; ?>#comments"><?php print $node->comment_count; ?> Comment<?php if ($node->comment_count == 0 || $node->comment_count > 1) { ?>s<?php } ?></a><?php } ?></span>
                </div>
  <?php endif; ?>


              <div class="post-content content" <?php print $content_attributes; ?>>
                  <?php
                  // We hide the comments and links now so that we can render them later.
                  hide($content['comments']);
                  hide($content['field_thumbnail']);
                  hide($content['links']);
                  print render($content);
                  ?>

                  <div class="blog-read-full-entry">
                      <a href="<?php print $node_url; ?>" class="learn-more-link"><?php print t('Read Full Entry'); ?></a> <i class="icon glyphicon glyphicon-chevron-right" aria-hidden="true"></i>
                  </div>
              </div>


          </div>
      </div>
  </div>             
<?php } ?>

<?php
// ******************************************************
//  NODE DISPLAY
// ******************************************************
?>

        <?php if ($page) { ?>
  <div id="node-<?php print $node->nid; ?>" class="post <?php print $classes; ?>" <?php print $attributes; ?>>
      <div class="row content-row">                
  <?php print render($title_prefix); ?>

          <h2 class="blogTitle"><?php print $title; ?></h2><?php print render($title_suffix); ?>

              <?php if ($display_submitted): ?>
            <div class="submitted post-meta">
            <?php //print $submitted;  ?>
            <?php print format_date($node->created, 'custom', 'l, F j, Y'); ?><span class="with-comment-count"> | <?php print $node->name; ?> <?php if ($node->comment == '2') { ?>| <a href="<?php print $node_url; ?>#comments"><?php print $node->comment_count; ?> Comment<?php if ($node->comment_count == 0 || $node->comment_count > 1) { ?>s<?php } ?></a><?php } ?></span>
            </div>
              <?php endif; ?>

          <div class="post-content content" <?php print $content_attributes; ?>>
              <?php
              // We hide the comments and links now so that we can render them later.
              hide($content['comments']);
              hide($content['links']);
              print render($content);
              ?>

  <?php print render($content['links']); ?>
          </div>
  <?php if ($node->comment == '2') { ?>
            <div class="row comments-heading-row">
                <div class="col-md-6">
                    <h3 class="comment-count"><?php print $node->comment_count; ?> Comment<?php if ($node->comment_count != 1) { ?>s<?php } ?></h3>
                </div>

                <div class="col-md-6">
                    <a class="add-comment-link">Add comment</a>
                </div>
            </div>
  <?php } ?>
  <?php print render($content['comments']); ?>
      </div>
  </div>                

<?php } ?>
            




