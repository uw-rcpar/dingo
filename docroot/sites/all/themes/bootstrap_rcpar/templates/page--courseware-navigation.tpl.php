
<?php if (!empty($page['db_region_middle']) || !empty($page['content'])): ?>
<div class="middle-dashboard-wrapper no-gutter">
 <div class="dashboard-region-middle">
   <?php print render($page['content']); ?>
   <?php print render($page['db_region_middle']); ?>
 </div> 
</div>
<?php endif; ?>        
