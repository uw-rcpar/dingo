<div class="bricks-layout">
	<div class="row first-row">
	    <div class="col-sm-12 ">
	        <div class="field field-name-title field-type-ds field-label-hidden">
	        	<div class="field-items">
	        		<div class="field-item even" property="dc:title">
	        			<h1 class="page-title">Roger CPA Review</h1>
	        		</div>
	        	</div>
	        </div>
	    <div class="field field-name-field-intro-text field-type-text-long field-label-hidden">
	    	<div class="field-items">
	    		<div class="field-item even">
	    			<p>Please wait while we customize the dashboard for you. All your Video History, Bookmarks and settings are being updated. We request that you do not refresh, hit the back button or leave the page until this process is complete.</p>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>
<div class="importing-information">

<div><div class="step-one step">Starting Process... <div class="ajax-progress ajax-progress-throbber"><i class="glyphicon glyphicon-refresh glyphicon-spin"></i></div></div></div>

</div>


