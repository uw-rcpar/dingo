<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>
<?php
  
  // Since we can't use webform_preprocess to make template 
  // suggestions per https://www.drupal.org/node/1891220, 
  // we'll select the template file to use here

// get term ids for term pages
if (arg(0)=='taxonomy' && arg(1)=='term' && is_numeric(arg(2))) {
  $tid = arg(2);
  $term =taxonomy_term_load($tid);
}
// load node for node pages
$nid = arg(1);
if (arg(0) == 'node' && is_numeric($nid)) {
  $node = node_load($nid);
}
// for on accounting dictionary term pages
if (isset($term) && $term->vocabulary_machine_name == 'dictionary' || (arg(0)=='lc' && arg(1)=='accounting-dictionary' && arg(2)=='search')) {
  include ('webform-form-dictpage.tpl.php');
  return;
}
// form on Question and Answer pages
else if (isset($node) && $node->type == 'question_and_answer') {
  include ('webform-form-qa-page.tpl.php');
  return;
}
else {
  include ('webform-form-default.tpl.php'); 
}

