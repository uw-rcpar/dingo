<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
  $hide_prices = false;
  $hide_per_part_pricing = false;
  if (module_exists('rcpar_partners') && isset($_SESSION['rcpar_partner'])) {
	  $hide_prices = rcpar_partners_hide_prices();
	  $hide_per_part_pricing = rcpar_partners_hide_per_part_pricing();
	 // print 'yes';
  }
  //print_r($header);
  
  if(arg(1)=="order-by-partner"){ ?>
      
    <form action="/order-by-partner-action" method="post">
      <input type="hidden" name="direct_bill" value="direct_bill-<?php print arg(2)?>">    
  <?php } ?>

<table <?php if ($classes) { print 'class="'. $classes . '" '; } ?><?php print $attributes; ?>>
   <?php if (!empty($title) || !empty($caption)) : ?>
     <caption><?php print $caption . $title; ?></caption>
  <?php endif; ?>
  <?php if (!empty($header)) : ?>
    <thead>
      <tr>
        <?php foreach ($header as $field => $label): ?>
        <?php 
        		//show prices        		
	        	if (!$hide_prices && !rcpar_partners_hide_prices_after_purchase()) { ?>
		          <th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
		            <?php print $label; ?>
		          </th>
		        	
	        <?php
	        	//don't show prices 
	        	} else { ?>
					<th <?php if ($header_classes[$field]) { print 'class="'. $header_classes[$field] . '" '; } ?>>
		            <?php
	        		// don't show content if price field
	        		
	        		if ($field == 'commerce_total' || $field == 'php' || $field == 'commerce_unit_price' || $field == 'commerce_price') { ?><?php } else {?><?php print $label; ?><?php } ?>

					</th>
	        <?php } ?>
        <?php endforeach; ?>
      </tr>
    </thead>
  <?php endif; ?>
  <tbody>
    <?php foreach ($rows as $row_count => $row): ?>
      <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
        <?php foreach ($row as $field => $content): ?>
        	<?php 
        		//show prices        		
	        	if (!$hide_prices && !rcpar_partners_hide_prices_after_purchase()) { ?>
			        <td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
						<?php print $content; ?>
				  	</td>
		        	
	        <?php
	        	//don't show prices 
	        	} else { ?>
				
		        	<td <?php if ($field_classes[$field][$row_count]) { print 'class="'. $field_classes[$field][$row_count] . '" '; } ?><?php print drupal_attributes($field_attributes[$field][$row_count]); ?>>
					<?php
	        		// don't show content if price field
	        		if ($field == 'commerce_total' || $field == 'php' || $field == 'commerce_unit_price' || $field == 'commerce_price') { ?><?php } else {?><?php print $content; ?><?php } ?>
				  	</td>
				
	        <?php } ?>
          
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
        
        
  <?php if(arg(1)=="order-by-partner"){ ?>
      <input type="submit" value="Submit"></form>
    <?php } ?>
