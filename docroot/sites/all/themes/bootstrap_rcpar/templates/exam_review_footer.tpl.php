<?php
//Template for Exam Review Footer
//Variables:
//$uid
?>
<div id="footer-content" class="exam-footer-content no-gutter">
	<div class="col-md-6 column-first">
   <!--My Notes link-->
    <a class="exam-review-footer-item my-notes" href="/dashboard/my-courses/my-notes">My Notes</a>
    <!--Course Breakdowns link-->       
    <a class="exam-review-footer-item course-breakdowns <?php print rcpar_dashboard_footer_links_get_custom_classes('course_breakdowns')?>" href="/dashboard/my-courses/course-breakdowns">Course Breakdowns</a>
	</div>
	<div class="col-md-6 column-last">
    <!--AICPA link-->       
    <a class="exam-review-footer-item aicpa-questions <?php print rcpar_dashboard_footer_links_get_custom_classes('aicpa')?>" id="aicpa-id" href="/dashboard/my-courses/AICPA-released-questions">AICPA Released Questions</a>    
    <!-- Course Textbook Updates link-->       
    <a class="exam-review-footer-item course-textbook-updates <?php print rcpar_dashboard_footer_links_get_custom_classes('course_textbook')?>" href="/dashboard/my-courses/course-textbook-updates">Course Textbook Updates</a> 	</div>
    <div class="clear-empty"></div>   
    <div></div>
</div>