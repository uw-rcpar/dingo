<?php

/**
 * @file
 * Display Suite 1 column template with layout wrapper.
 */
 $comment_published = 1;
 if (isset($variables['status']) && $variables['status'] == 'comment-unpublished') {
   $comment_published = 0;
 }
 $user_roles = $variables['user']->roles;
 $moderator = 0;
 if (in_array('administrator', $user_roles) || in_array('moderator', $user_roles) || in_array('editor', $user_roles)) {
  $moderator = 1;
 }
 $display_type = '';
 if (isset($variables['form'])) {
	 $display_type = 'form';
	 //print_r($variables);
 } else {
	 $display_type = 'comment';
 }
 $comment_copy = '';
 if (isset($variables['comment_body']['0']['safe_value'])) {
	 $comment_copy = $variables['comment_body']['0']['safe_value'];
 } 
 $comment_copy;
 $user_picture = '';
 if (isset($variables['picture'])) {
	 $user_picture = $variables['picture'];
 } 
 $original_date = '';
 $comment_date = '';
 if (isset($variables['elements']['#comment']->created)) {
	 $original_date = $variables['elements']['#comment']->created;
 } 
 $comment_date = date("g:i A j M Y", $original_date);
 $comment_author = '';
 if (isset($variables['elements']['#comment']->name)) {
	 $comment_author = $variables['elements']['#comment']->name;
 }
 $comment_id = '';
 if (isset($variables['elements']['#comment']->cid)) {
	 $comment_id = $variables['elements']['#comment']->cid;
	 if ($comment_published == 0) {
	   $approve_token = '';
	   $approve_token = drupal_get_token("comment/' . $comment_id. '/approve");
	 }
 } 
 $node_id = '';
 if (isset($variables['elements']['#comment']->nid)) {
	 $node_id = $variables['elements']['#comment']->nid;
 } 

 
 
?>
<?php if ($display_type == 'form' && user_is_logged_in()) { ?>


<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-1col <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <<?php print $ds_content_wrapper ?> class="<?php print trim($ds_content_classes); ?>">
    <?php print $ds_content; ?>
  </<?php print $ds_content_wrapper ?>>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>

<?php } else { ?>

    <div id="comments" class="comment-wrapper">
        <a id="comment-1985"></a>

        <div class="comment comment-by-viewer clearfix <?php if ($comment_published) { ?>comment-published<?php } else { ?>comment-unpublished<?php } ?>" about="/comment/1985#comment-1985" typeof="sioc:Post sioct:Comment">
            <div class="row">
                <div class="col-md-2">
                    <div class="user-picture">
                        <?php print $user_picture; ?>
                    </div>
                </div>

                <div class="col-md-10">
                    <div class="comment-inner">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 property="dc:title" datatype="" class="comment-title"><?php print $comment_author; ?></h3>
                            </div>

                            <div class="col-md-6">
                                <div class="submitted">
                                    <?php print $comment_date; ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="content">
                                    <div class="field field-name-comment-body field-type-text-long field-label-hidden">
                                        <div class="field-items">
                                            <div class="field-item even" property="content:encoded">
                                                <?php print $comment_copy; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<?php if ($moderator) { ?>
                <ul class="links list-inline">
                    <li class="comment-delete first"><a href="/comment/<?php print $comment_id; ?>/delete">delete</a></li>
                    <li class="comment-edit"><a href="/comment/<?php print $comment_id; ?>/edit">edit</a></li>
                    <li class="comment-reply last"><a href="/comment/reply/<?php print $node_id; ?>/<?php print $comment_id; ?>">reply</a></li>
                    <?php if ($comment_published == 0) { ?>
                    <li class="comment-reply last"><a href="/comment/<?php print $comment_id; ?>/approve?token=<?php print $approve_token; ?>">approve</a></li>
                    <?php } ?>      
                </ul><?php } ?>
            </div>
        </div>

        <div class="comments-sec">
            <h3 class="title comment-form">Leave a Comment</h3>
        </div>
    </div>


<?php } ?>