<?php
/**
 * @file
 * Default theme implementation for displaying a section of terms in the Lexicon
 * overview page.
 *
 * This template renders a section of Lexicon terms in the Lexicon overview
 * page.
 *
 * Available variables:
 * - $lexicon_section: section object.
 *    - $lexicon_section->letter: The letter the section is for.
 *    - $lexicon_section->id: The id to be used as the anchor of the section.
 * - $lexicon_overview_items: Lexicon overview items as rendered by
 *   lexicon-overview-item.tpl.php
 */

/** CHANGES MADE IN THIS TEMPLATE: 

- added the lexicon divider div with bootstrap (col-sm-12) wrapper div
- changed the <dl> tags to <div> tags and added letter-list class and ID
- added the list-items-wrapper/row div
 */

?>

<div class="col-sm-12">
<div class="lexicon-divider"></div>
</div>
<?php if (isset($lexicon_section)) : ?>
  <a id="<?php print $lexicon_section->id; ?>"></a>
  <h2 class="lexicon-letter"><?php print drupal_strtoupper($lexicon_section->letter); ?></h2>
<?php endif; ?>
<?php if (!empty($lexicon_overview_items)) : ?>
  <div>
    <div class="letter-list"  id="<?php print $lexicon_section->letter; ?>-letter-list">
      <div class="list-items-wrapper row">
        <?php print $lexicon_overview_items; ?>
      </div>
    </div>
  </div>
<?php else :?>
  <p>
    <?php print(t('There are no terms to display.')); ?>
  </p>
<?php endif; ?>