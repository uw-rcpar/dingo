<?php
//print_r($node);
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="post <?php print $classes; ?> container no-gutter category-node"<?php print $attributes; ?>>
	<div class="row first-row">
	<?php
	$hasvideo = 'no-video';
	$hasimage = 'no-img';
	if (isset($node->field_video['und']['0']['video_url']) || isset($node->field_amazon_video_link['und']['0']['safe_value'])) {
		$hasvideo = 'video';
	}
	if (isset($node->field_image['und']['0']['uri'])) {
		$hasimage = 'img';
	}
	?>
	<?php if ($hasvideo == 'video') { ?>
	<div class="row">
	<div class="col-sm-12">
	<div class="page-heading">
		<?php print render($title_prefix); ?>
		<h1 class="page-title"><?php print $title; ?></h1>
		<?php print render($title_suffix); ?>	
	</div>
	</div>
	</div>
	<?php } ?>
		<?php
		  // We hide fields now so that we can render them later.
		  hide($content['comments']);
		  hide($content['links']);
		  hide($content['field_intro_text']);
		  hide($content['field_video']);
		  hide($content['field_amazon_video_link']);
		  hide($content['field_image']);
      hide($content['field_hero_banner']);
      hide($content['field_tablet_hero_image']);
      hide($content['field_phone_hero_image']);
      hide($content['field_hero_banner_text']);
      hide($content['body']);
		  hide($content['field_upper']);
		  hide($content['field_left_column_upper']);
		  hide($content['field_right_column_upper']);
		  hide($content['field_middle_left']);
		  hide($content['field_middle_right']);
		  hide($content['field_lower_left']);
		  hide($content['field_lower_right']);
		  hide($content['field_bottom_left']);
		  hide($content['field_bottom_right']);
      hide($content['field_bottom']);
		  print render($content);
		?>

		
	</div>


	<div class="row video-image-row <?php print $hasvideo; ?>-wrapper <?php print $hasimage; ?>-wrapper">
	
	
		<?php if ($hasvideo == 'no-video') { ?><div class="col-sm-12"><div class="page-heading"><?php } ?>
		
		<div class="<?php if ($hasimage == 'no-img' && $hasvideo == 'no-video') { ?>col-md-12 no-image<?php } else { ?>col-md-6<?php } ?> <?php print $hasvideo; ?>-left">
		
			<?php if ($hasvideo == 'video') { ?>
				<?php if (isset($node->field_video['und']['0']['video_url'])) : ?>
					<?php print render($content['field_video']); ?>
				<?php endif; ?>
				
				<?php if (isset($node->field_amazon_video_link['und']['0']['safe_value'])) : ?>
					<video width="400" id="video" controls>
					  <source src="<?php print $node->field_amazon_video_link['und']['0']['safe_value']; ?>" type="video/mp4" />
					</video>
				<?php endif; ?>
				
				<?php if ($content['body']) : ?>
					<?php print render($content['body']); ?>
				<?php endif; ?>
				
			<?php } else {	?>

        <?php if (isset($node->field_hero_banner['und']['0']['uri'])) { ?>
          <div class="with-hero-banner-container">
        <?php } else { ?>
          <?php print render($title_prefix); ?>
          <h1 class="page-title"><?php print $title; ?></h1>
          <?php print render($title_suffix); ?>

          <?php if ($content['field_intro_text']) : ?>
            <?php print render($content['field_intro_text']); ?>
          <?php endif; ?>
        <?php } ?>
        <?php if (isset($node->field_hero_banner['und']['0']['uri'])) { ?></div><?php } ?>

			<?php }	?>
		</div>
		
		<?php if ($hasimage == 'img' || $hasvideo == 'video') { ?>
			<div class="col-md-6 <?php print $hasvideo; ?>-right">
				<?php if ($hasvideo == 'video') { ?>
					<?php if ($content['field_intro_text']) : ?>
						<?php print render($content['field_intro_text']); ?>
					<?php endif; ?>
					
				<?php } else { 	?>
					<?php if ($content['field_image']) : ?>
						<?php $img_url = $content['field_image']['#object']->field_image['und']['0']['uri']; ?>
						<div class="background-img-container" style="background: no-repeat url('<?php print file_create_url($img_url); ?>')  center center; position: absolute; width: 100%;"></div>
					<?php endif; ?>
					
				<?php }	?>
			</div>
		
		<?php } ?>
		
		
		<?php if ($hasvideo == 'no-video') { ?><div class="clear-empty"></div></div></div><?php } ?>
	</div>

  <?php
  // desktops
  $hero_class = '';
  // 991px - portrait tablets images
  $tablet_hero_class = '';
  // 768px - phone images
  $phone_hero_class = '';
  if (isset($node->field_tablet_hero_image['und']['0']['uri'])) {
    $hero_class = ' hidden-xs hidden-sm';
    $tablet_hero_class = ' hidden-md hidden-lg';
  }
  if (isset($node->field_phone_hero_image['und']['0']['uri'])) {
    $hero_class = ' hidden-xs hidden-sm';
    $tablet_hero_class = ' hidden-md hidden-lg hidden-xs';
    $phone_hero_class = ' hidden-sm hidden-md hidden-lg';
  }
  ?>

  <?php if (isset($node->field_hero_banner['und']['0']['uri'])) { ?>
    <div class="row hero-banner-row <?php print $hero_class; ?>">
      <div style="background-image: url(<?php print file_create_url($node->field_hero_banner['und']['0']['uri']); ?>);background-repeat: no-repeat;background-size: cover;background-position:center top;">
        <div class="row">
          <div class="col-sm-4 hero-banner-text" style="height: <?php print $node->field_hero_banner["und"][0]["metadata"]["height"]; ?>px;">
            <div class="text-wrapper">
              <h1><?php print $title; ?></h1>
              <?php print render($content['field_intro_text']); ?>
            </div>
          </div>
          <div class="col-sm-8"></div>
        </div>
      </div>
    </div>
  <?php }	?>


  <?php if (isset($node->field_tablet_hero_image['und']['0']['uri'])) { ?>
    <div class="row hero-banner-row <?php print $tablet_hero_class; ?>">
      <div style="background-image: url(<?php print file_create_url($node->field_tablet_hero_image['und']['0']['uri']); ?>);background-repeat: no-repeat;background-size: cover;background-position:center top;">
        <div class="row">
          <div class="col-sm-4 hero-banner-text" style="height: <?php print $node->field_tablet_hero_image["und"][0]["metadata"]["height"]; ?>px;">
            <div class="text-wrapper">
              <h1><?php print $title; ?></h1>
              <?php print render($content['field_intro_text']); ?>
            </div>
          </div>
          <div class="col-sm-8"></div>
        </div>
      </div>
    </div>
  <?php }	?>


  <?php if (isset($node->field_phone_hero_image['und']['0']['uri'])) { ?>
    <div class="row hero-banner-row <?php print $phone_hero_class; ?>">
      <div style="background-image: url(<?php print file_create_url($node->field_phone_hero_image['und']['0']['uri']); ?>);background-repeat: no-repeat;background-size: cover;background-position:center top;">
        <div class="row">
          <div class="col-sm-4 hero-banner-text" style="height: <?php print $node->field_phone_hero_image["und"][0]["metadata"]["height"]; ?>px;">
            <div class="text-wrapper">
              <h1><?php print $title; ?></h1>
              <?php print render($content['field_intro_text']); ?>
            </div>
          </div>
          <div class="col-sm-8"></div>
        </div>
      </div>
    </div>
  <?php }	?>

  <?php if (isset($node->field_hero_banner['und']['0']['uri'])) { ?><div class="with-hero-banner-container"><?php } ?>
    <?php if ($hasvideo == 'no-video') { ?>
      <?php if ($content['body']) : ?>
      <div class="row body-row">
        <?php print render($content['body']); ?>
      </div>
      <?php endif; ?>
    <?php }	?>


    <?php if (isset($node->field_upper['und']['0']['safe_value'])) { ?>
    <div class="row upper-row">
      <?php print render($content['field_upper']); ?>
    </div>
    <?php }	?>

    <?php if (isset($node->field_left_column_upper['und']['0']['safe_value']) || isset($node->field_right_column_upper['und']['0']['safe_value']))  { ?>
    <div class="row first-column-row column-row">
      <div class="col-md-6 column-left">
        <?php if ($content['field_left_column_upper']) { ?>
        <?php print render($content['field_left_column_upper']); ?>
        <?php }	?>
      </div>
      <div class="col-md-6 column-right">
        <?php if ($content['field_right_column_upper']) { ?>
        <?php print render($content['field_right_column_upper']); ?>
        <?php }	?>
      </div>
    </div>
    <?php }	?>

    <?php if (isset($node->field_middle_left['und']['0']['safe_value']) || isset($node->field_middle_right['und']['0']['safe_value']))  { ?>
    <div class="row second-column-row column-row">
      <div class="col-md-6 column-left">
        <?php if ($content['field_middle_left']) { ?>
        <?php print render($content['field_middle_left']); ?>
        <?php }	?>
      </div>
      <div class="col-md-6 column-right">
        <?php if ($content['field_middle_right']) { ?>
        <?php print render($content['field_middle_right']); ?>
        <?php }	?>
      </div>
    </div>
    <?php }	?>

    <?php if (isset($node->field_lower_left['und']['0']['safe_value']) || isset($node->field_lower_right['und']['0']['safe_value']))  { ?>
    <div class="row second-column-row column-row">
      <div class="col-md-6 column-left">
        <?php if ($content['field_lower_left']) { ?>
        <?php print render($content['field_lower_left']); ?>
        <?php }	?>
      </div>
      <div class="col-md-6 column-right">
        <?php if ($content['field_lower_right']) { ?>
        <?php print render($content['field_lower_right']); ?>
        <?php }	?>
      </div>
    </div>
    <?php }	?>

    <?php if (isset($node->field_bottom_left['und']['0']['safe_value']) || isset($node->field_bottom_right['und']['0']['safe_value']))  { ?>
    <div class="row third-column-row column-row">
      <div class="col-md-6 column-left">
        <?php if ($content['field_bottom_left']) { ?>
        <?php print render($content['field_bottom_left']); ?>
        <?php }	?>
      </div>
      <div class="col-md-6 column-right">
        <?php if ($content['field_bottom_right']) { ?>
        <?php print render($content['field_bottom_right']); ?>
        <?php }	?>
      </div>
    </div>
    <?php }	?>
    <?php if (isset($content['field_bottom']))  { ?>	<div class="row bottom-row">
      <?php print render($content['field_bottom']); ?>
    </div>
    <?php }	?>

    <?php if (isset($node->field_hero_banner['und']['0']['uri'])) { ?></div><?php } ?>

	
	
</div>
