<?php if (count($user_entitlements) > 0): ?>
    <h2 class="block-title"><?php echo t('Offline Lecture Serial Numbers') ?></h2>
    <div id="entitlements-serials">
        <?php foreach($user_entitlements as $product): ?>
        <div class="audio-course-item no-gutter">
        	<div class="col-md-4 col-sm-4"><img src="/sites/all/themes/bootstrap_rcpar/css/img/offline-cpa-review-lecture-sm.png" alt="Offline Lectures" /></div>
        	<div class="col-md-8 col-sm-8">
	            <div class="serial-title">
	                <?php echo $product['title'] ?>
	            </div>
	            <div class="serial-container">
	                <div class="content">                    <?php /*
	                    //'Course Type: ' . $product['course_type'] . '<br>'.
	                    //'Start Date: ' . date('m/d/Y', $product['start_date']) . '<br>'.
	                    //'Order ID: ' . $product['order_id'] . '<br>'.
	                    //'Product ID: ' . $product['product_id'] . '<br>'.
	                    //'Product SKU: ' . $product['sku'] . '<br>'.
	                    //'Product Title: ' . $product['title'] . '<br>'.
	                    //'Expiration Date: ' . date('m/d/Y', $product['expiration_date']) . '<br>' .z
	                    //'Duration: ' . $product['course_duration'] . ' ' . $product['default_interval'] . '<br>' .
	                    //'Duration Extension: ' . $product['duration_extended'] . '<br>' .
	                    */ ?>
	                    Serial No.: <?php echo $product['serial_number']; ?> <br>
	                    <?php //Installs: <?php echo count($product['installs']); <br> ?>
	                    <?php //'Allowed Installs: ' . $product['allowed_installs'] . '<br>'. ?>
	                </div>
	            </div>
        	</div>
        	<div class="clear-empty"></div>
        </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>