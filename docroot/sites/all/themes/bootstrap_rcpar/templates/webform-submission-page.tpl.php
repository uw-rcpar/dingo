<?php

/**
 * @file
 * Customize the navigation shown when editing or viewing submissions.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $mode: Either "form" or "display". May be other modes provided by other
 *          modules, such as "print" or "pdf".
 * - $submission: The Webform submission array.
 * - $submission_content: The contents of the webform submission.
 * - $submission_navigation: The previous submission ID.
 * - $submission_information: The next submission ID.
 */
?>

<?php /* Print discount approval form block */
 if (zoho_rcpar_exists_remotly($submission->data, $node->nid)) {
    $button = drupal_get_form("zoho_rcpar_sumission_button",$submission,$node->nid);
    echo drupal_render($button);
}

if (is_string($submission_content) && strpos($submission_content,'verifications') !== false) {
	    $verifications = 1;
	} else {
		$verifications = 0;
	}
	if (module_exists('discount_approval') && $verifications) {
		$block = block_load('discount_approval', 'da_approval_form');
                $block_data = _block_get_renderable_array(_block_render_blocks(array($block)));
		$output = drupal_render($block_data);
		print $output;
	}
 

?>

<?php if ($mode == 'display' || $mode == 'form'): ?>
  <div class="clearfix">
    <?php print $submission_actions; ?>
    <?php print $submission_navigation; ?>
  </div>
<?php endif; ?>

<?php print $submission_information; ?>

<div class="webform-submission">
  <?php print render($submission_content); ?>
</div>

<?php if ($mode == 'display' || $mode == 'form'): ?>
  <div class="clearfix">
    <?php print $submission_navigation; ?>
  </div>
<?php endif; ?>








