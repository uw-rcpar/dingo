<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
        "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces;?>>
<head profile="<?php print $grddl_profile; ?>">
  <?php
  // Single place to include tracking code like: GA, GTM, Optimizely
  print $tracking_codes;
  ?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <?php print $head; ?>
  <title><?php if ($field_bookmark_title) { print $field_bookmark_title; } else { print $head_title; } ?></title>
  <?php print $styles; ?>
  <!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <?php print $scripts; ?>
</head>

<body class="<?php print $classes; ?> maintenance-page page-why-roger">

<header id="navbar" role="banner" class="<?php print $navbar_classes; ?> container">
  <div class="row">
    <div class="col-md-3">
      <?php if ($logo): ?>
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>
    </div>
    <div class="col-md-9">
      <div class="primary-nav-wrapper nav-bottom">
      </div>
    </div>
  </div><!-- end .row -->
</header>


<div class="main-container container category-page">
	<div class="row">
		 <?php print $messages;  ?>
		<section id="first-section" class="">
			<div class="col-sm-12 no-video-wrapper">
			<div class="page-heading">
			 <h1 class="page-title text-center">Site Under Scheduled Maintenance</h1>
			</div>
			</div>
			 <div class="maintenance-content text-center"><?php print $content; ?></div>
		</section>
  </div>
</div>


<footer class="footer container-fluid">
  	<div class="clear-empty"></div>
</footer>

<style>
  body.in-maintenance {margin-top: 0 !important;}
  .in-maintenance .page-heading {
    background-color: #28a9e0;
    padding-bottom: 40px;
  }
  .in-maintenance .page-heading::before, .in-maintenance .page-heading::after {
    content: "";
    position: absolute;
    background: #28a9e0;
    top: 0;
    bottom: 0;
    width: 9999px;
    z-index: 1;
  }
</style>
</body>
</html>