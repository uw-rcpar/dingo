<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
//print_r($content);
?>
<?php

// only show content for certain node types and roles
$display_content = 0;

// Load the currently logged in user.
global $user;

if (in_array('administrator', $user->roles) || in_array('student enrolled', $user->roles) || in_array('moderator', $user->roles)) {
  $display_content = 1;
}

// get the node's author
$node_author = user_load($node->uid);
// get node author's email address to display for moderators and adminstrators
if (in_array('moderator', $user->roles) || in_array('administrator', $user->roles)) {
  $user_email = '<div class="mod_user_email">User email: ' . $node_author->mail . '</div>';
}

if ($display_content) {

  // Hide comments and links now so that we can render them later.
  hide($content['comments']);
  hide($content['links']);
  hide($content['field_tags']);
  hide($content['flag_flag']);
  hide($content['field_general_topic']);

  // flag this item link
  if ($content['flag_flag'] && (in_array('administrator', $user->roles) || in_array('moderator', $user->roles))) { ?>
    <div class="flag-link"><?php print render($content['flag_flag']); ?></div>
    <?php
  }

    ?>

  <div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix hhc-node"<?php print $attributes; ?>>


    <div class="content"<?php print $content_attributes; ?>>

      <?php print render($title_prefix); ?>
      <h2 class="title" <?php print $title_attributes; ?>>
        <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
      </h2>
      <?php print render($title_suffix); ?>


      <div class="node-content">
        <?php
        // get the node body and convert to plain text
        $node_body = drupal_html_to_text($body['0']['safe_value']);
        // remove new lines and trailing space
        $node_body = trim(preg_replace('/\s+/', ' ', $node_body));
        // truncate and add ellipse
        print truncate_utf8(rtrim($node_body), 240, TRUE, TRUE);
        ?>
      </div>


      <div class="row">
        <div class="col-sm-6 author-info">
          <span class="author-icon">
            <?php
            // display node author's profile picture if it exists
            if (isset($node_author->picture) && $node_author->picture != '') {
              print theme('image_style', array('style_name' => 'hhc_user_img', 'path' => $node_author->picture->uri, 'alt' => $node->name . '\'s picture'));
            }

            ?>
          </span>
          Posted by
          <span class="author-name">
            <?php
            if (in_array('administrator', $user->roles) || in_array('moderator', $user->roles)) {
              print '<a href="' . base_path() . 'homework-help-center/user-posts/' . $node->uid . '">' . $node->name . '</a>';
            }
            else {
              print $node->name;
            }
            ?>
          </span>
          on  <span class="created-date"><?php echo date('n/j/Y', $node->created); ?> at <?php echo date('g:ia', $node->created); ?></span>
        </div>
        <div class="col-sm-6 views-answers-count">
          <?php
          // display node view count
          $nid = $node->nid;
          $node_stats = statistics_get($nid);
          if (isset($node_stats) && $node_stats) {
            print '<span class="view-count">' . format_plural($node_stats['totalcount'], '1 View', '@count Views') . '</span>';
          }
          //display dividing line
          if (isset($node_stats) && $node_stats && $node->comment_count > 0) {
            print ' | ';
          }
          // display comment count
          if ($node->comment_count > 0) {
            print '<span class="answers-count">' . format_plural($node->comment_count, '1 Answer', '@count Answers') . '</span>';
          }
          ?>
        </div>
      </div>
      <div class="user-email">
        <?php print $user_email; ?>
      </div>

    </div>

  </div>
<?php }
// if user doesn't have permissions to view content, only display message
else { ?>


  <div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

    <p style="margin-top: 40px;">You are not authorized to view this content.</p>
  </div>

<?php } ?>