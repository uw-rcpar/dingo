<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */

?>
<?php print ($variables['page_tracking_codes']); ?>
<?php if (!empty($page['hello_bar'])): ?>
  <div class="hello-bar container">
    <div class="row">
      <div class="col-sm-12">
        <?php print render($page['hello_bar']); ?>
      </div>
    </div>
  </div>
<?php endif; ?>
<!--
DESKTOP HEADER
-->
<?php $main_menu = render($primary_nav); ?>
<header id="navbar" role="banner" class="<?php print $navbar_classes; ?> container site-header">
  <div class="row">
    <div class="col-md-4 col-sm-8 col-xs-8">
      <?php if ($logo): ?>
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>
      <?php if (!empty($site_name)): ?>
        <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>
    </div>
    <div class="col-md-8 col-sm-4 col-xs-4">
      <div class="primary-nav-wrapper nav-bottom clearfix">
        <?php if (!empty($page['navigation'])): ?>
          <div class="navigation-nav cart-nav-link">
            <?php print render($page['navigation']); ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($page['lower_navigation'])): ?>
          <div class="navigation-nav user-nav-link">
            <?php print render($page['lower_navigation']); ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($primary_nav)): ?>
          <div class="primary-nav" role="navigation">
            <?php print $main_menu ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div><!-- end .row -->
</header>

<!--
MOBILE HEADER
-->
<header id="navbar-mobile" role="banner" class="<?php print $navbar_classes; ?> container">
  <div class="row">
    <div class="col-md-4 col-sm-8 col-xs-8">
      <?php if ($logo): ?>
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>
      <?php if (!empty($site_name)): ?>
        <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>
    </div>
    <div class="col-md-8 col-sm-4 col-xs-4">
      <div class="primary-nav-mobile-wrapper">
        <div class="btn-menu"></div>
      </div>
    </div>
    <nav id="mainnav">
      <div class="navbar">
        <?php if (!empty($primary_nav)): ?>
          <div class="primary-nav" role="navigation">
            <?php print $main_menu; ?>
          </div>
        <?php endif; ?>
      </div>
    </nav>
  </div><!-- end .row -->
</header>


<div class="main-container container-fluid">

  <?php print $messages; ?>
  <?php if (!empty($tabs)): ?>
    <?php print render($tabs); ?>
  <?php endif; ?>
  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->

  <div class="row content-row">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php print render($title_suffix); ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>



      <?php if ($page['home_row1']): ?>
        <div class="row">
          <?php print render($page['home_row1']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['home_row2']): ?>
        <div class="row">
          <?php print render($page['home_row2']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['home_row3']): ?>
        <div class="row">
          <?php print render($page['home_row3']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['home_row4']): ?>
        <div class="row">
          <?php print render($page['home_row4']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['home_row4']): ?>
        <div class="row">
          <?php print render($page['home_row5']); ?>
        </div>
      <?php endif; ?>



      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>
<?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
  <!-- Footer -->
  <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-xs-12 footer-first">
          <?php print render($page['footer_firstcolumn']); ?>
        </div>
        <div class="col-sm-3 col-xs-12 footer-second">
          <?php print render($page['footer_secondcolumn']); ?>
        </div>
        <div class="col-sm-3 col-xs-12 footer-third">
          <?php print render($page['footer_thirdcolumn']); ?>
          <div class="clearfix"></div>
        </div>
        <div class="col-sm-3 col-xs-12 footer-fourth">
          <?php print render($page['footer_fourthcolumn']); ?>
        </div>
        <div class="clear-empty"></div>
      </div><!-- End Row -->
    </div><!-- .container -->
  </div><!-- #footer -->
<?php endif; ?>

<footer class="footer">
  <div class="container">
    <?php print render($page['footer']); ?>
  </div>
</footer>
