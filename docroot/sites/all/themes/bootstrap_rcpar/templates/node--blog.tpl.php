<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>


<?php
// ******************************************************
//  search results display
// ******************************************************
?>

<?php
if (!$page) {
  // main search result display
  if ($view_mode == 'alternate_search_result_display') {
    ?>
    <div id="node-<?php print $node->nid; ?>" class="post <?php print $classes; ?>" <?php print $attributes; ?>>
      <div class="row content-row">
        <div class="post-content content" <?php print $content_attributes; ?>>
          <?php print render($title_prefix); ?>
          <h2 class="blogTitle"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
          <?php print render($title_suffix); ?>
          <?php
          hide($content['comments']);
          hide($content['field_thumbnail']);
          hide($content['links']);
          print render($content);
          ?>
        </div>

      </div>
    </div>


    <?php
  }

  // ******************************************************
  //  teaser display
  // ******************************************************
  else {

    $field_thumbnail_image = field_get_items('node', $node, 'field_thumbnail');
    $thumbnail_image_url = $field_thumbnail_image[0]['uri'];
    ?>
    <div id="node-<?php print $node->nid; ?>" class="post <?php print $classes; ?>" <?php print $attributes; ?>>
        <div class="row content-row">

            <div class="blog-thumbnail col-md-2">
                <?php
                if (!empty($node->picture->uri)) {
                  print theme('image_style', array('path' => $node->picture->uri, 'style_name' => 'blog_thumbnail'));
                } else {
                  $custom_default_image_path = 'public://user-icon-md.png.png';
                  print theme('image_style', array('path' => $custom_default_image_path, 'style_name' => 'blog_thumbnail'));
                }
                ?>
            </div>
          <div class="visible-xs visible-sm mobile-blog-title">
            <?php print render($title_prefix); ?>
            <h2 class="blogTitle"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
            <?php print render($title_suffix); ?>
          </div>

            <div class="blog-teaser-info col-md-10">
              <div class="hidden-xs hidden-sm">
                <?php print render($title_prefix); ?>
                <h2 class="blogTitle"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
                <?php print render($title_suffix); ?>
              </div>
                    <?php if ($display_submitted): ?>
                  <div class="submitted post-meta">
                  <span class="with-comment-count"><?php print $node->name; ?> <?php if ($node->comment == '2') { ?>| <a href="<?php print $node_url; ?>#comments"><?php print $node->comment_count; ?> Comment<?php if ($node->comment_count == 0 || $node->comment_count > 1) { ?>s<?php } ?></a><?php } ?></span>
                  </div>
    <?php endif; ?>

          <div class="post-content content" <?php print $content_attributes; ?>>
            <?php
            // We hide the comments and links now so that we can render them later.
            hide($content['comments']);
            hide($content['field_thumbnail']);
            hide($content['links']);
            hide($content['field_teaser']);
            if (!empty($field_teaser)) {
              print render($content['field_teaser']);
            }
            else {
              print render($content);
            }
            ?>

            <div class="blog-read-full-entry">
              <a href="<?php print $node_url; ?>" class="learn-more-link"><?php print t('Read Full Entry'); ?></a>
              <i class="icon glyphicon glyphicon-chevron-right" aria-hidden="true"></i>
            </div>
          </div>


        </div>
      </div>
    </div>
  <?php }
}
?>

<?php
// ******************************************************
//  NODE DISPLAY
// ******************************************************
?>
<?php if ($page) { ?>
  <div id="node-<?php print $node->nid; ?>" class="post <?php print $classes; ?>" <?php print $attributes; ?>>
    <div class="row content-row">
      <?php print render($title_prefix); ?>

      <h2 class="blogTitle"><?php print $title; ?></h2><?php print render($title_suffix); ?>

      <?php if ($display_submitted): ?>
        <div class="submitted post-meta">
          <?php //print $submitted;  ?>
          <span class="with-comment-count"><?php print $node->name; ?> <?php if ($node->comment == '2') { ?>|
              <a href="<?php print $node_url; ?>#comments"><?php print $node->comment_count; ?> Comment<?php if ($node->comment_count == 0 || $node->comment_count > 1) { ?>s<?php } ?></a><?php } ?></span>
        </div>
      <?php endif; ?>

      <div class="post-content content" <?php print $content_attributes; ?>>
        <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_teaser']);
        hide($content['field_categories']);
        if (in_array('administrator', $GLOBALS['user']->roles) || in_array('editor', $GLOBALS['user']->roles)) { ?>
          <div style="font-weight: bold; background-color: #eee; padding: 0px 10px 10px 10px; margin-bottom: 10px 0;">
            <h3 style="font-weight: bold; padding-top: 5px;">EDITORS:</h3>
            <p>Below is the "teaser" of the page, which will display to regular users on the main Blog landing page. This only displays here for editors to create a teaser. </p>
          </div>
          <?php
          print render($content['field_teaser']);
        } ?>

        <?php
        if (in_array('administrator', $GLOBALS['user']->roles) || in_array('editor', $GLOBALS['user']->roles)) { ?>
          <div style="font-weight: bold; background-color: #eee; padding: 0px 10px 10px 10px; margin: 10px 0;">
            <h3 style="font-weight: bold; padding-top: 5px;">EDITORS:</h3>
            <p>Below is the body, or main content, of the page. This is what will show to regular visitors.</p>
          </div>
        <?php } ?>
        <?php
        print render($content);

        // Check if the sharethis icon links and categories field already exist in the pagebuilder markup and print them if they don't.
        if (!strstr($content['body'][0]['#markup'],'az_view-blog_categories-block')) {
          print render($content['field_categories']);
        }
        if (!strstr($content['body'][0]['#markup'],'az_block-sharethis-sharethis_block')) {
          $block = module_invoke('sharethis', 'block_view', 'sharethis_block');
          print render($block['content']);
        }
        print render($content['links']); ?>
      </div>
      <?php if ($node->comment == '2') { ?>
        <div class="row comments-heading-row">
          <div class="col-md-6">
            <h3 class="comment-count"><?php print $node->comment_count; ?> Comment<?php if ($node->comment_count != 1) { ?>s<?php } ?></h3>
          </div>

          <div class="col-md-6">
            <a class="add-comment-link">Add comment</a>
          </div>
        </div>
      <?php } ?>
      <?php print render($content['comments']); ?>
    </div>
  </div>

<?php } ?>
            




