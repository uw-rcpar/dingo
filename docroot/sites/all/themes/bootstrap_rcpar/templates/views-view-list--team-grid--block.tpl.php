<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php print $list_type_prefix; ?>
    <?php $i = 1; ?>
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes_array[$id]; ?><?php if ($i % 4 == 0) { print ' fourth'; } ?>" id="views-row-<?php print $i; ?>"><?php print $row; ?></li>
    <?php  $i++; endforeach; ?>
  <?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>