<!-- Modal -->
<div class="modal fade" id="course-activation" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Upgrade your Free Trial Course</h4>
      </div>
      <div class="modal-body">
        <p>Please confirm that you want to disable your Free Trial and UPGRADE to a paid course.</p>
      </div>
      <div class="modal-footer">                
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>     
        <a class="use-ajax" href= <?php print "/delete-entitlements-freetrial/".$uid."/nojs"?>>
            <button type="button" class="btn btn-primary" >Yes! Upgrade Me. </button>
        </a>    
      </div>
    </div>
  </div>
</div>