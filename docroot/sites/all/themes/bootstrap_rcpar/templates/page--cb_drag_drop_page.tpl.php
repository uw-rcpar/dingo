<?php print ($variables['page_tracking_codes']); ?>
<?php
// Render tabs up top so they don't make the page layout look weird
if (!empty($tabs)) {
  print '<div id="drupal-tabs-wrapper">';
  print render($tabs);
  print '</div>';
}
?>


<?php if (!empty($page['hello_bar'])): ?>
  <div class="hello-bar container">
    <div class="row">
      <div class="col-sm-12">
        <?php print render($page['hello_bar']); ?>
      </div>
    </div>
  </div>
<?php endif; ?>

<!--
DESKTOP HEADER
-->
<?php $main_menu = render($primary_nav); ?>
<header id="navbar" role="banner" class="<?php print $navbar_classes; ?> container site-header">
  <div class="row">
    <div class="col-md-4 col-sm-8 col-xs-8">
      <?php if ($logo): ?>
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>
      <?php if (!empty($site_name)): ?>
        <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>
    </div>
    <div class="col-md-8 col-sm-4 col-xs-4">
      <div class="primary-nav-wrapper nav-bottom clearfix">
        <?php if (!empty($page['navigation'])): ?>
          <div class="navigation-nav cart-nav-link">
            <?php print render($page['navigation']); ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($page['lower_navigation'])): ?>
          <div class="navigation-nav user-nav-link">
            <?php print render($page['lower_navigation']); ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($primary_nav)): ?>
          <div class="primary-nav" role="navigation">
            <?php print $main_menu ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div><!-- end .row -->
</header>

<!--
MOBILE HEADER
-->
<header id="navbar-mobile" role="banner" class="<?php print $navbar_classes; ?> container">
  <div class="row">
    <div class="col-md-4 col-sm-8 col-xs-8">
      <?php if ($logo): ?>
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>
      <?php if (!empty($site_name)): ?>
        <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <?php endif; ?>
    </div>
    <div class="col-md-8 col-sm-4 col-xs-4">
      <div class="primary-nav-mobile-wrapper">
        <div class="btn-menu"></div>
      </div>
    </div>
    <nav id="mainnav">
      <div class="navbar">
        <?php if (!empty($primary_nav)): ?>
          <div class="primary-nav" role="navigation">
            <?php print $main_menu; ?>
          </div>
        <?php endif; ?>
      </div>
    </nav>
  </div><!-- end .row -->
</header>



<div class="main-container container-fluid">

  <?php print $messages; ?>

  <header role="banner" id="page-header">
    <?php if (!empty($site_slogan)): ?>
      <p class="lead"><?php print $site_slogan; ?></p>
    <?php endif; ?>

    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->

  <div class="row content-row">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <a id="main-content"></a>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif; ?>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>



      <?php if ($page['home_row1']): ?>
        <div class="row">
          <?php print render($page['home_row1']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['home_row2']): ?>
        <div class="row">
          <?php print render($page['home_row2']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['home_row3']): ?>
        <div class="row">
          <?php print render($page['home_row3']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['home_row4']): ?>
        <div class="row">
          <?php print render($page['home_row4']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page['home_row4']): ?>
        <div class="row">
          <?php print render($page['home_row5']); ?>
        </div>
      <?php endif; ?>



      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>

<?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
  <!-- Footer -->
  <div id="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-3 col-xs-12 footer-first">
          <?php print render($page['footer_firstcolumn']); ?>
        </div>
        <div class="col-sm-3 col-xs-12 footer-second">
          <?php print render($page['footer_secondcolumn']); ?>
        </div>
        <div class="col-sm-3 col-xs-12 footer-third">
          <?php print render($page['footer_thirdcolumn']); ?>
          <div class="clearfix"></div>
        </div>
        <div class="col-sm-3 col-xs-12 footer-fourth">
          <?php print render($page['footer_fourthcolumn']); ?>
        </div>
        <div class="clear-empty"></div>
      </div><!-- End Row -->
    </div><!-- .container -->
  </div><!-- #footer -->
<?php endif; ?>
<footer class="footer">
  <div class="container">
    <?php print render($page['footer']); ?>
  </div>
</footer>
