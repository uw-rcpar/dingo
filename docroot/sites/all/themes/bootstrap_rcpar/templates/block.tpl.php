<?php
/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see bootstrap_preprocess_block()
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see bootstrap_process_block()
 * @see template_process()
 *
 * @ingroup themeable
 */

// Initialize vars
$wrapper_open = '';
$wrapper_close = '';
$colclass = '';

switch ($block_html_id) {
  // add bootstrap column classes for certain blocks
  case 'block-block-66': // Create an Account - login page block
  case 'block-block-116': // Login - register page block
    $colclass = ' col-md-6';
    break;

  case 'block-webform-client-block-15868': // Give Us Your Feedback webform block
  case 'block-rcpa-hcenter-menu-center': // Help Center menu block
    $colclass = ' col-md-8';
    break;

  case 'block-block-118': // Contact Us - Support page block
  case 'block-views-exp-help-center-page': // Help Center view block - Moderator view
  case 'block-views-exp-help-center-page-1': // Help Center view block - Student view
    $colclass = ' col-md-4';
    break;

  case 'block-system-main': // Main content block (on user login/register/password pages)
    if ((arg(0) == 'user' && arg(1) == 'register') || (arg(0) == 'user' && arg(1) == 'login') || (arg(0) == 'user' && arg(1) == 'password') || (arg(0) == 'user' && !user_is_logged_in())) {
      $colclass = ' col-md-6';
    }
    break;

  // add wrapper html for learning center blocks
  case 'block-views-learning-center-block':
  case 'block-views-learning-center-block-2':
  case 'block-views-learning-center-block-1':
  case 'block-views-learning-center-block-3':
    $wrapper_open = '<div class="row"><div class="col-sm-12">';
    $wrapper_close = '</div></div>';
    break;
  default:
    $colclass = '';
}

?>

<section id="<?php print $block_html_id; ?>" <?php if ($block_html_id=='block-search-form') { ?> role="search"<?php } ?> class="<?php print $classes; ?> clearfix<?php print $colclass; ?>"<?php print $attributes; ?>>
  <div class="block-wrapper-outer">
    <div class="block-wrapper">
      <div class="block-wrapper-inner">
        <?php
        if ($title) {
          print render($title_prefix);
          print $wrapper_open;
          if ($block->css_class == 'helping-students-pass') {
            print '<h1' . $title_attributes . '>' . $title . '</h1>';
          }
          else {
            print '<h2' . $title_attributes . ' id="title-' . $block_html_id . '">' . $title . '</h2>';
          }
          print $wrapper_close;
          print render($title_suffix);
        }
        print $content;
        ?>
      </div>
    </div>
  </div>
</section> <!-- /.block -->

