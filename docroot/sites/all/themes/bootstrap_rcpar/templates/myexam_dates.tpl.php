<?php 
extract($variables);
$userpage=0;
if (arg(0)=="user" && is_numeric(arg(1))) {
	$userpage=1;
}

?>

<div class="next-date-wrapper">
	<div class="next-date"><?php print $next_exam; ?></div>
	<div class="next-date-info">
		<div class="next-date-copy"><?php print t('Days until'); ?><br /><?php print t('your next exam'); ?></div>
		<div class="next-date-form"><?php 
		if ($userpage) {
		  print $date_form;
		}
	  ?></div>
  </div>
	<div class="clear-empty"></div>
</div>
  
  <div class="notice-date-exams"></div>
<?php 
if (!$userpage) {
  print $date_form;
}
  ?>