<?php 
//Template for Courseware Navigation tabs
//Variables:
//$active_ones
//$no_active_ones
kpr($active_ones);exit;
?>

<div id="my-courses-tabs" class="tabs-wrapper courseware" role="tabpanel">  
<ul class="nav nav-tabs" role="tablist">  
 <?php 
 $i = 0;
 foreach ($active_ones as $a_key => $a_value) {
 $i++;?>  
  <li role="presentation" class="tab-item <?php print $a_value["class"] ?> tab-<?php print $i; ?> <?php print $a_key ?>-tab col-sm-1">
     <a href="#tab-<?php print $i; ?>" aria-controls="tab-<?php print $i; ?>" role="tab" data-toggle="tab">
       <?php print $a_key ?>  
    </a>
  </li>    
 <?php } ?>
 <?php 
 $r = 0;
 foreach ($no_active_ones as $no_key => $no_value) {
 $r++; ?>
  <li role="presentation" class="tab-item empty-tab tab-<?php print $r; ?> <?php print $no_key ?>-tab col-sm-1">
      <!--<a  href="/<?php print drupal_get_path_alias('node/12'); ?>" target="_blank" class="element-link">   -->      
        <div class="add-link">                              
          <div class="element-value"><?php print $no_key ?></div>         
          <div class="add-course"><a  href="/node/<?php print drupal_get_path_alias('node/12'); ?>" target="_blank" class="element-link"> +ADD COURSE</a></div>            
        </div>
     <!--</a>  -->
  </li>    
<?php } ?>
</ul>   
    <div class="tab-content">
 <?php 
 $n = 0;
 foreach ($active_ones as $a_key => $a_value) {
 $n++; ?>         
      <div role="tabpanel" class="tab-pane fade<?php if($a_value["class"] == "active"){ ?> active in<?php } ?> tab-content-<?php print $a_key ?>" id="tab-<?php print $n; ?>">      
        <?php            
          print rcpar_dashboard_get_my_courses_view($a_value["nid"],FALSE,$a_value['exam_version']);
        ?>
      </div>	
 <?php } ?>  
    </div>
</div>

