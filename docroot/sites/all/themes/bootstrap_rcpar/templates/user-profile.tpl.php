<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
global $user;
$user_wrapper = $user;
?>
<!--
<div id="profile-links">
	<ul class="option-set">
            <li><a href="/dashboard">My Dashboard</a></li>
            <li><a class="selected">My Profile</a></li>
            <li><a href="/user/<?php print $user->uid; ?>/edit">Edit Email/Password</a></li>
            <li><a href="">Billing Profile</a></li>
            <li><a href="">Payment Info</a></li>
            <li><a href="/user/<?php print $user->uid; ?>/orders">Order History</a></li>
            <li><a href="/user/<?php print $user->uid; ?>/subscriptions">Subscriptions</a></li>
    </ul>
</div>

<div class="user-picture-wrapper"><?php $image = theme('user_picture', array('account' => $user));
print $image; ?></div>
<div class="user-info">
  <div><?php print $user->name; ?></div>
  <div><?php print $user->mail; ?></div>
</div> 
<div class="clear-empty"></div>




<h2>Account information</h2>
<ul class="user-profile-category user-information clearfix">
  <?php if (user_access('administer')) { ?>
  <li>
  <?php 
//  $user_profile['info']['aes_password']['#title'] = '';
//  print render($user_profile['info']); ?>
  </li>
  <?php } ?>
  <li class="account">
  <h3 class="user-profile-item shipping-address">Account</h3>
  <div class="user-profile-item shipping-address">
  <div class="user-profile-item-wrapper">
  <div>Username: <?php print $user->name; ?></div>
  <div>Email: <?php print $user->mail; ?></div>
  </div>
  <a href="/user/<?php print $user->uid; ?>/edit" class="btn btn-large">Update email/password</a></div>
</li>
  <li class="profile">
  <h3 class="user-profile-item shipping-address">Profile</h3>
  <a href="/user/<?php print $user->uid; ?>/edit" class="btn btn-large">Edit profile</a></div>
</li>

<li>
  <h3 class="user-profile-item billing-address">Billing Profile</h3>
  <div class="user-profile-item billing-address">
   <div class="user-profile-item-wrapper">
  <div>Name: name here</div>
  <div>Address: sdfsdfsd</div>
  </div>
  <a href="/user/<?php print $user->uid; ?>/edit-profile" class="btn btn-large">Manage Billing Profile</a></div>
</li>

<li>
  <h3 class="user-profile-item billing-address">Payment Info</h3>
  <div class="user-profile-item billing-address">
   <div class="user-profile-item-wrapper">
  <div>Spend Budget: </div>
  <div>Card Type: </div>
  <div>Card Number: </div>
  <div>Card Expiration: </div>
  </div>
  <a href="/user/<?php print $user->uid; ?>/edit-profile" class="btn btn-large">Manage Payment Info</a></div>
</li>

<li>
  <h3 class="user-profile-item billing-address">Order History</h3>

  <a href="/user/<?php print $user->uid; ?>/orders" class="btn btn-large">View Order History</a></div>
</li>
<li>
  <h3 class="user-profile-item billing-address">Subscriptions</h3>

  <a href="/user/<?php print $user->uid; ?>/subscriptions" class="btn btn-large">Manage subscriptions</a></div>
</li>
</ul>

-->


