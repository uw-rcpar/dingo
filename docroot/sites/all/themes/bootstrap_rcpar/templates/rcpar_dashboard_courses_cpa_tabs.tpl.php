<?php 
//Template for Courses tabs
//Variables:
//$active_ones
//$no_active_ones

//With this partner we validate that ASL / FSL / FREETRIAL don't apply
//to versioning, that with its method isBillingFreeAccess.
$entitlements_partners = array();
foreach ($active_ones as $a_key => $a_value) {
  $partner_id = $user->user_entitlements['products'][$a_key]['partner_nid'];
  if ($partner_id){
    $entitlements_partners[$a_key] = new RCPARPartner($partner_id);
  }
}
?>
<?php if (arg(0)=='node' && arg(1)=='6426' || arg(0)=='dashboard' && arg(1)=='my-courses') { ?>
<div class="course-outline-title-wrapper"><h2 class="course-outline-title">Course Outline</h2></div>
<?php } ?>
<div id="my-courses-tabs" class="tabs-wrapper" role="tabpanel">
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#course-activation" id="execute-modal-activation-after-ajax" style="display:none">
</button>
<div id="activation-container-of-values"></div>
<div id="cpa-status-message"></div>
<ul class="nav nav-tabs" role="tablist">  
 <?php 
 $i = 0;
 foreach ($active_ones as $a_key => $a_value) {
 $i++; ?>  
  <li role="presentation" class="tab-item <?php print $a_value["exam_version"] ?> <?php print $a_value["class"] ?> tab-<?php print $i; ?> <?php print $a_key ?>-tab col-sm-3">
     <div class="radial-wrapper">
       <?php
       /* ******************************************************************************************************
          Exam version materials not chosen (also: do not show materials selection for free access partners)
        ******************************************************************************************************* */

       if (!$a_value['exam_version'] && (!isset($entitlements_partners[$a_key])  || !$entitlements_partners[$a_key]->isBillingFreeAccess())) {  ?>

         <a href='<?php
         if ($a_value['exam_version'] != '2017') {
           $ops = array('query' => array('destination' => 'dashboard/my-courses/'.$a_key, 'force_sel' => $a_key));
         }
         else {
           $ops = array();
         }
         print url('dashboard', $ops);
         ?>' class="course-radial-link no-exam-selected">
           <div class="radial-element" id="<?php print $a_key ?>">
             <p class="element-value"> <?php print $a_value['value'] ?> </p>
           </div>
           <div class="expire-date-class"> <?php print $a_value["expire_date"] ?>bbb</div>
           <?php // only show versioning if versioning is turned on
           if (exam_version_is_versioning_on()) { ?><div class="materials-selection">
             <div class="open-exam-version-modal">Select materials</div>
           </div>
           <?php } ?>
         </a>

         <?php
         /* **********************************
            Exam version materials chosen
          *********************************** */

       }
       else { ?>

         <a href="#<?php print $a_key ?>-tab" aria-controls="<?php print $a_key ?>-tab" role="tab" data-toggle="tab" class="course-radial-link">
           <div class="radial-element" id="<?php print $a_key ?>">
             <p class="element-value"> <?php print $a_value['value'] ?> </p>
           </div>
         </a>
         <div class="expire-date-class"> <?php print $a_value["expire_date"] ?></div>
         <?php
         // only show versioning if versioning is turned on
         if (exam_version_is_versioning_on()) {
         // do not show materials selection for free access partners
           if(!isset($entitlements_partners[$a_key]) || !$entitlements_partners[$a_key]->isBillingFreeAccess())  { ?>
           <div class="materials-selection">
             <div class="materials-selection-chosen section-<?php print $a_key ?>"><?php print $a_value['exam_version']; ?> materials
               <button class="info-icon" data-toggle="popover" data-trigger="click" data-html="true" title="<?php print $a_key ?> <?php print $a_value['exam_version']; ?> Course Materials"
                       data-content="<p>Your <?php print $a_key ?> course materials are set to the <?php print $a_value['exam_version']; ?> version of the Exam.
                       You can edit settings for course materials on your <a href='<?php print url('dashboard'); ?>'>dashboard</a>.</p>"
                       data-placement="bottom" data-template="<div class='popover materials-selection-popover' role='tooltip'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div></div>">
                 <img src="/sites/all/themes/bootstrap_rcpar/css/img/icon-info.svg" width="15">
               </button>
             </div>
           </div>
           <?php
           }
         }
       } ?>
     </div>
  </li>    
 <?php } 
 foreach ($delayed_ones as $d_key => $d_value) {    
   $i++; ?>
  <li role="presentation" class="tab-item tab-<?php print $i; ?> <?php print $d_key ?>-tab col-sm-3">
     <span class="activate-delayed-ones">
     <a href="<?php  print "/activate-delayed-courses-modal/".$d_value."/nojs" ?>" target="_blank" class="element-link use-ajax"> 
     <div class="add-link">   
	     <div class="radial-wrapper">
	       <div class="radial-element" id="<?php print $d_key ?>"> 
	         <p class="element-value"> <?php print 0 ?> </p> 
		      <div class="add-course"><div class="element-link">Activate Course</div></div>         
	       </div>      
	      </div>       
	  </div>                 
    </a>    
    </span>
  </li>   
 <?php 
 }
 $r = 0;
 foreach ($no_active_ones as $no_key => $no_value) {
 $r++; ?>
   <?php if (!isset($exp_unlimited[$no_key])) : ?>
      <li role="presentation" class="tab-item empty-tab tab-<?php print $r; ?> <?php print $no_key ?>-tab col-sm-3">
          <a  href="/<?php print drupal_get_path_alias('node/12'); ?>" target="_blank" class="element-link">
            <div class="add-link">
              <div class="radial-static-element" id="static-<?php print $no_key ?>"></div>
              <div class="element-value"><?php print $no_key ?></div>
              <div class="plus-sign"><div class="element-link">+</div></div>
              <div class="add-course"><div class="element-link"> +ADD COURSE</div></div>
              <div class="clear-empty"></div>
           </div>
         </a>
      </li>
   <?php else: ?>
     <li role="presentation" class="tab-item tab-<?php print $i; ?> <?php print $d_key ?>-tab col-sm-3">
     <span class="unlimited-access-extend">
     <a href="/unlimited-access/activation" target="_blank" class="element-link">
     <div class="add-link">
	     <div class="radial-wrapper">
	       <div class="radial-element" id="<?php print $no_key ?>">
	         <p class="element-value"> <?php print $exp_unlimited[$no_key]['value'] ?> </p>
		      <div class="add-course"><div class="element-link">Extend My Access</div></div>
	       </div>
	      </div>
	  </div>
    </a>
    </span>
    </li>
   <?php endif ?>
<?php } ?>
</ul>
  <div class="tab-content">
    <?php
    $n = 0;
    foreach ($active_ones as $a_key => $a_value) {
      $n++;
      // exam version class - if versioning is on, get it from the $active_ones array
      if (exam_version_is_versioning_on()) {
        $exam_version_class = 'exam-version-' . $a_value['exam_version'];
      }
      // otherwise use the default version from admin settings
      else {
        $exam_version_class = 'exam-version-' . exam_version_get_default_version();
      }
      ?>

      <div role="tabpanel" class="tab-pane fade<?php if ($a_value["class"] == "active") { ?> active in<?php } ?> tab-content-<?php print $a_key ?>  <?php print $exam_version_class; ?>" id="<?php print $a_key ?>-tab">

        <?php
        if (isset($notes_option)) {
          print rcpar_dashboard_get_my_courses_notes_view($a_value["nid"], $a_value['exam_version']);
        }
        elseif (isset($monitoring_case)) {
          print rcpar_dashboard_mc_get_my_courses_view($a_value["nid"], $a_value['exam_version']);
        }
        else {
          print rcpar_dashboard_get_my_courses_view($a_value["nid"], FALSE, $a_value['exam_version']);
        }
        ?>
      </div>

    <?php } ?>
  </div>
</div>

