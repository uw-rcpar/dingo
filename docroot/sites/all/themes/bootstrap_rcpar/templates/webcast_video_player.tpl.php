<div class="learning-center-video-area">
  <?php if ($presentation == 0 && $active == TRUE && $expired == FALSE): ?>
    <!--- show stand alone player --->


    <div class="row">
      <div class="col-md-8" style="margin-left: auto; margin-right: auto; float: none;">
        <video class="center_video" id="video" poster="<?php print file_create_url($thumbnail); ?>" controls>
          <source src="<?php print $video_src; ?>" type="video/mp4"/>
          <?php if ($vtt_url != ''): ?>
              <track label="English" kind="subtitles" srclang="en" src="<?php print $vtt_url; ?>" default>
          <?php endif; ?>
        </video>
      </div>
    </div>


  <?php elseif ($presentation == 1 && $active == TRUE): ?>
    <!--- display presentation player and slides --->


    <script type="text/javascript" src="<?php print $GLOBALS['base_url']; ?>/sites/all/libraries/popcorn/popcorn.min.js"></script>
    <script>
      document.addEventListener("DOMContentLoaded", function () {

        var pop = Popcorn('#video', {
          pauseOnLinkClicked: true
        });

        //pop.play();

        /*pop.image({
            start: 2,
            end: 10,
            target: 'slides',
            src: 'http://www.rogercpareview.com/broadcast/slide_controller/content/backup_slides/big/Slide1.PNG'
        });*/

        /*var pres_images = [
          { start: 0, end: 160, src: "http://localhost/dingo_drupal/sites/default/files/Slide1.PNG" },
          { start: 160, end: 240, src: "http://localhost/dingo_drupal/sites/default/files/Slide2.PNG" },
          { start: 240, end: 250, src: "http://localhost/dingo_drupal/sites/default/files/Slide3.PNG" },
        ];*/
        //presentation image data passed to json object
        var pres_images = [
          <?php print $popcorn_data; ?>
        ];
        // Loop through the images
        pres_images.forEach(function (pres_image) {
          // Call setCountry() at the start of each country's start time.
          pop.image({
            start: pres_image.start,
            end: pres_image.end,
            target: 'slides',
            src: pres_image.src
            //onStart: function() { setCountry(country.country_name);
          });
        });
      });


    </script>
    <div id="presentation_container">
      <div id="video_container" class="col-md-4">
        <div class="video-with-slides-wrapper">
          <video width="100%" id="video" poster="<?php print file_create_url($thumbnail); ?>" controls>
            <source src="<?php print $video_src; ?>" type="video/mp4"/>
            <?php if ($vtt_url != ''): ?>
                <track label="English" kind="subtitles" srclang="en" src="<?php print $vtt_url; ?>">
            <?php endif; ?>
          </video>
        </div>
      </div>
      <div id="slides" class="col-md-8" style="border: 1px solid #666;"></div>
      <div style="clear: both"></div>
    </div>


  <?php elseif ($active == FALSE && $expired == FALSE): ?>
    <!--- Display the comming soon message --->
    <h3>This webcast will be available on <?php print date_format(date_create($start_date), 'D, M j, Y'); ?>.</h3>
    <p>Please check back on that date to view the webcast in full.</p>

  <?php elseif ($expired == TRUE): ?>
    <!--- Display the comming soon message --->
    <h3>This webcast expired on <?php print date_format(date_create($end_date), 'D, M j, Y'); ?>.</h3>
    <p>Check back soon to catch our next free webcast, or check out our
      <a href="/cpa-exam-learning-center">CPA Exam Learning Center</a> for more video resources.</p>

  <?php endif; ?>
</div>

<?php if ($active == TRUE) { ?>
  <!-- Google Code for Webcast Signup Conversion Page -->
  <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1072665269;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "TcmCCODGtHAQtaW-_wM";
    var google_remarketing_only = false;
    /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
    <div style="display:inline;">
      <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1072665269/?label=TcmCCODGtHAQtaW-_wM&amp;guid=ON&amp;script=0"/>
    </div>
  </noscript>
<?php } ?>
