<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php
//print_r($content);
$extras_pane = 0;

if ($content['field_product']['#view_mode'] == 'extras_pane') {
  $extras_pane = 1;
}

// Initialize var
$extra_class = '';

// This tpl is used by product displays but is also leveraged by rcpar_partners for the display of partner landing pages.
// Determine whether we should modify the page to show partner information checking if we have an active partner and if
// we're on the partner URL.
$partner_helper = new RCPARPartner();
$show_partner = FALSE;
if ($partner_helper->isPartnerPageLoaded()) {
  $partner = $partner_helper->getWrapper();
  $show_partner = TRUE;
}
?>


<?php
// ******************************************************
//  EXTRAS PANE DISPLAY
// ******************************************************
?>

<?php if ($extras_pane) { ?>

  <div id="node-<?php print $node->nid; ?>" class="post <?php print $classes; ?>"<?php print $attributes; ?>>
    <div class="content">
      <?php
      $product_image = $content['product:field_product_image']['#object']->field_product_image['und']['0']['uri'];
      if ($product_image) {
        print '<div class="extras-image"><img src="' . image_style_url('cart_page_image', $product_image) . '" alt="' . $str . '"></div>';
      } ?>
      <div class="extras-title"><?php print $title; ?></div>
      <?php print render($content); ?>
      <div class="clear-empty"></div>
    </div>
  </div>

<?php }
else { ?>

  <?php
  // ******************************************************
  //  NODE DISPLAY
  // ******************************************************
  ?>


  <?php
  hide($content['comments']);
  hide($content['links']);
  hide($content['field_details']);
  hide($content['field_left_content']);
  hide($content['field_core_materials']);
  hide($content['field_package_extras']);
  hide($content['field_faq']);
  hide($content['field_additonal_references']);
  hide($content['field_specs']);
  hide($content['product_image']);
  hide($content['field_image']);
  $packageclass = '';
  $partnerclass = '';
  if (isset($content['field_core_materials']['#object']->field_core_materials['und']['0']['value'])) {
    $packageclass = 'package';
  }
  else {
    $packageclass = 'non-package';
  }
  if (rcpar_partners_get_partner_object()) {
    $partnerclass = ' partner';
  } ?>
  <div id="node-<?php print $node->nid; ?>" class="post <?php print $classes; ?> container no-gutter node-<?php print $packageclass . $partnerclass; ?>"<?php print $attributes; ?>>
    <div class="row">
      <div class="col-sm-12">
        <div class="product-<?php print $packageclass; ?>-wrapper">
          <div class="product-<?php print $packageclass; ?>-heading heading page-heading">


            <?php
            // ******************************************************
            //  PACKAGE PRODUCT DISPLAY
            // ******************************************************
            ?>
            <?php
            switch ($packageclass) {
              case "package":
                // if user is accessing page via partner/direct bill, and package value field is supposed to show,
                // and page is select course package, then add an extra class to hide the default pricing info
                if ($show_partner && $partner_helper->isRetailPriceVisible()) {
                  $extra_class = ' display-retail-value';
                }
                ?>
                <div class="commerce-product-field commerce-product-field-field-product-image field-field-product-image node-<?php print $node->nid; ?>-product-field-product-image">
                  <div class="field field-name-field-product-image field-type-image field-label-hidden">
                    <div class="field-items">
                      <div class="field-item even">

                        <div class="package-product-img">
                          <?php print theme('image_style', array(
                            'style_name' => 'product_package',
                            'path'       => $content['product:field_product_image']['#object']->field_product_image['und'][0]['uri']
                          )); ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <?php print render($title_prefix); ?>
                  <h2<?php print $title_attributes; ?> class="product-page-title col-sm-6" itemprop="name"><?php print $title; ?></h2>
                  <?php print render($title_suffix); ?>
                  <div class="add-wrapper col-sm-6">
                    <?php

                    // price and sale price for non-partner bundles
                    $price = '';
                    $on_sale = 0;
                    $sale_price = '';
                    $price_class = '';

                    if (isset($content['product:commerce_price']['0']['#markup'])) {
                      $price = $content['product:commerce_price']['0']['#markup'];
                    }
                    if (isset($content['product:commerce_price']['#object']->field_commerce_saleprice_on_sale['und']['0']['value'])) {
                      $on_sale = 1;
                    }
                    if (isset($content['product:commerce_price']['#object']->field_commerce_saleprice['und']['0']['amount'])) {
                      $sale_price = commerce_currency_format($content['product:commerce_price']['#object']->field_commerce_saleprice['und']['0']['amount'], 'USD', '', $convert = TRUE);
                    }
                    if ($on_sale) {
                      $price_class = 'on-sale';
                    }
                    else {
                      $price_class = 'regular';
                    }


                    // if user is accessing page via partner/direct bill and page is select course package
                    if ($show_partner) {
                      // Hide the default price display of the select course package. We will only display the prices
                      // set by the partner
                      drupal_add_css('.total-bundle-price {display:none;}', 'inline');

                      // if package value field is supposed to show, display it
                      if ($partner_helper->isRetailPriceVisible()) {
                        print '<div class="retail-value-price">Total Package Value: ' . commerce_currency_format($partner_helper->getRetailPrice(), 'USD') . '</div>';
                      }
                      // if firm bundle price is supposed to show, display it
                      if ($partner_helper->isBundlePriceVisible()) {
                        print '<div class="firm-price">Partner price: $' . $partner->field_full_price->value() . '</div>';
                      }
                      // if partner is a direct bill partner, add copy explaining partner pays for bundle
                      if ($partner_helper->isBillingDirectBill()) {
                        print '<div class="direct-bill-note">NOTE: Your organization will be billed directly for this order.</div>';
                      }
                      // if partner is a prepaid partner, add copy explaining partner prepaid for bundle
                      elseif ($partner_helper->isBillingPrepaid()) {
                        print '<div class="direct-bill-note">NOTE: Your organization has prepaid for this order.</div>';
                      }
                    }
                  ?>


                    <div class="price <?php print $price_class; ?>">
                      <?php if ($on_sale) { ?>
                        <!--<span class="price-label"><table class="table table-striped"><tbody><tr><td>Price: </td></tr></tbody></table></span>-->
                        <span class="regular-price-amount"><?php print $price; ?></span><span class="sale-price-amount"><table class="table table-striped"><tbody><tr><td><?php print $sale_price; ?></td></tr></tbody></table></span>
                      <?php }
                      else { ?>
                        Price: <span class="regular-price-amount"><?php print $price; ?></span>
                      <?php } ?>
                    </div>
                    <?php print render($content['field_product']); ?>
                  </div>
                </div><!-- /row -->
                <div class="col-sm-6 package-left<?php print $extra_class; ?>">

                  <?php if ($content['field_details']['#object']->field_details['und']['0']['value']) { ?>
                    <?php print render($content['field_details']); ?>
                  <?php } ?>

                </div>

                <div class="col-sm-6 package-right">

                  <?php
                  // print video if there is one
                  if (isset($content['field_video'])) {
                    print render($content['field_video']);
                  }
                  if (isset($content['field_right_content']) && !empty($content['field_right_content'])) {
                    print '<div class="right-content">';
                    print render($content['field_right_content']);
                    print '</div>';
                  }
                  ?>
                </div>
                <?php
                if ($show_partner) {
                  // get landing page instructions
                  $lp_instruct = rcpar_partners_get_addl_instructions();
                  // Add markup if we have instructions
                  if ($lp_instruct) { ?>
                    <div class="row partner-row">
                      <div class="col-md-12 text-col">
                        <?php print rcpar_partners_get_addl_instructions(); ?>
                      </div>
                    </div>
                    <?php

                  }
                }

                // ******************************************************
                //  NON-PACKAGE PRODUCT DISPLAY
                // ******************************************************

                break;
              case "non-package":
                ?>
                <?php print render($title_prefix); ?>
                <h2<?php print $title_attributes; ?> class="product-page-title" itemprop="name"><?php print $title; ?></h2>
                <?php print render($title_suffix); ?>
                <div class="col-sm-6 <?php print $packageclass; ?>-left">
                  <?php
                  $result = 0;
                  $testimonial_term = bootstrap_rcpar_get_current_product_display_page($node->nid);
                  $view = views_get_view_result("testimonial_rotator", "block_5", $testimonial_term);
                  $result = count($view);
                  if ($result) {
                    ?>
                    <div class="testimonial-rating col-sm-5">
                      <div class="clearfix fivestar-average-stars fivestar-average-text">
                        <div class="fivestar-static-item">
                          <div class="form-type-item form-item form-group">
                            <div class="form-item-inner">
                              <div class="fivestar-summary fivestar-summary-average-count">
                                <span class="average-rating">Customer rating:</span></span>
                              </div>
                              <div class="fivestar-default">
                                <div class="fivestar-widget-static fivestar-widget-static-vote fivestar-widget-static-5 clearfix">
                                  <a id="link-reviews" class="product-link">
                                    <div class="star star-1 star-odd star-first">
												    <span id="schema_block" class="schema_review">
												        <span itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                                  <span class="on" itemprop="ratingValue">5</span>
                                  <span itemprop="ratingCount"><?php print $result; ?></span>
                                  <span itemprop="itemReviewed"><?php print $title; ?></span>
												        </span>
												    </span>
                                    </div>

                                    <div class="star star-2 star-even"><span class="on"></span></div>
                                    <div class="star star-3 star-odd"><span class="on"></span></div>
                                    <div class="star star-4 star-even"><span class="on"></span></div>
                                    <div class="star star-5 star-odd star-last"><span class="on"></span></div>
                                  </a>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="read-reviews col-sm-7">
                      <a class="product-link scroll-link" id="link-reviews">Read Reviews</a>
                    </div>
                  <?php } ?>
                  <?php
                  // print video if there is one
                  if (isset($content['field_video'])) {
                    print render($content['field_video']);
                  }
                  else {
                    if (isset($content['product:field_product_image']['#object']->field_product_image['und']['0']['uri'])) {
                      if (isset($content['product:field_product_image']['#object']->field_product_image['und']['0']['filename'])) {
                        $str = $content['product:field_product_image']['#object']->field_product_image['und']['0']['filename'];
                        $str = str_replace('.png', '', str_replace('-', ' ', $str));
                      }
                      else {
                        $str = '';
                      }

                      $product_image = $content['product:field_product_image']['#object']->field_product_image['und']['0']['uri'];
                    }
                    elseif (isset($content['field_image']['#object']->field_image['und']['0']['uri'])) {
                      if (isset($content['field_image']['#object']->field_image['und']['0']['filename'])) {
                        $str = $content['field_image']['#object']->field_image['und']['0']['filename'];
                        $str = str_replace('.png', '', str_replace('-', ' ', $str));
                        $product_image = $content['field_image']['#object']->field_image['und']['0']['uri'];
                      }
                    }
                    if ($product_image || isset($content['field_left_content'])) {
                      ?>

                      <div class="commerce-product-field commerce-product-field-field-product-image field-field-product-image node-<?php print $node->nid; ?>-product-field-product-image">
                        <div class="field field-name-field-product-image field-type-image field-label-hidden">
                          <div class="field-items">
                            <div class="field-item even">
                              <?php
                              if ($product_image) {
                                print '<img src="' . image_style_url('product_image', $product_image) . '" alt="' . $str . '">';
                              }
                              elseif (isset($content['field_left_content'])) {
                                print render($content['field_left_content']);
                              }
                              ?>
                            </div>
                          </div>
                        </div>
                      </div>

                      <?php
                    }
                  }
                  ?>
                </div>

                <div class="col-sm-6 <?php print $packageclass; ?>-right">
                  <?php if (isset($content['field_details']['#object']) && $content['field_details']['#object']->field_details['und']['0']['value']) { ?>
                    <?php print render($content['field_details']); ?>
                  <?php } ?>
                  <?php
                  if ($node->nid == 311 && !rcpar_dashboard_display_extension_products_box()) {
                    if (variable_get('enable_uworld_configurations', FALSE)) {
                      drupal_goto('node/6');
                    }
                    else{
                      $block = block_load('block', 126);
                      $output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($block))));
                      print $output;
                    }                    
                  }
                  else {
                    print render($content['field_product']);
                  }
                  ?>
                  <?php print render($content['field_product_upsell']); ?>
                </div>

                <?php
                break;
            }
            ?>
            <div class="clear-empty"></div>
          </div>
        </div>
      </div>
    </div>
    <?php
    // ******************************************************
    //    STICKY NAV
    // ******************************************************
    ?>

    <div class="row">
      <div class="col-sm-12">
        <div class="content-nav-bar sticky-nav">
          <?php if (isset($content['body'])) { ?>
            <a class="product-link scroll-link" id="link-overview">Overview</a>
          <?php } ?>

          <!-- commenting this out because this field is not being utilized in this display (only currently in use in the variation display)
          <?php if (isset($content['field_product_features']['#object']->field_product_features['und']['0']['value']) && $content['field_product_features']['#object']->field_product_features['und']['0']['value']) { ?>
            <a class="product-link scroll-link" id="link-product-features">Product Features</a>
          <?php } ?>
          -->

          <?php if (isset($content['field_core_materials']['#object']->field_core_materials['und']['0']['value']) && $content['field_core_materials']['#object']->field_core_materials['und']['0']['value']) { ?>
            <a class="product-link scroll-link" id="link-core-materials"><?php print render($content['field_core_materials']['#title']); ?></a>
          <?php } ?>

          <?php if (isset($content['field_package_extras']['#object']->field_package_extras['und']['0']['value']) && $content['field_package_extras']['#object']->field_package_extras['und']['0']['value']) { ?>
            <a class="product-link scroll-link" id="link-package-extras"><?php print render($content['field_package_extras']['#title']); ?></a>
          <?php } ?>

          <?php //print_r($content['field_popular_add_ons']);
          if (isset($content['field_popular_add_ons']['#object']->field_popular_add_ons['und']['0']['value']) && $content['field_popular_add_ons']['#object']->field_popular_add_ons['und']['0']['value']) { ?>
            <a class="product-link scroll-link" id="link-popular-addons">Popular Add-Ons</a>
          <?php } ?>


          <?php
          $result = 0;
          $testimonial_term = bootstrap_rcpar_get_current_product_display_page($node->nid);
          $view = views_get_view_result("testimonial_rotator", "block_5", $testimonial_term);
          $result = count($view);
          if ($result) {
            ?>
            <a class="product-link scroll-link" id="link-reviews">Customer Reviews</a>
          <?php } ?>

          <?php if (isset($content['field_faq']['#object']->field_faq['und']['0']['value']) && $content['field_faq']['#object']->field_faq['und']['0']['value']) { ?>
            <a class="product-link scroll-link" id="link-faq"><?php print render($content['field_faq']['#title']); ?></a>
          <?php } ?>
          <!-- commenting this out because this field is not being utilized in this display (only currently in use in the variation display)
          <?php if (isset($content['field_specs']['#object']->field_specs['und']['0']['value']) && $content['field_specs']['#object']->field_specs['und']['0']['value']) { ?>
            <a class="product-link scroll-link" id="link-specs"><?php print render($content['field_specs']['#title']); ?></a>
          <?php } ?>
          -->

          <a class="product-link-add-to-cart back-top-link"><i class="fa fa-chevron-up"></i></a>

        </div>
      </div>
    </div>
    <?php
    // ******************************************************
    //    OVERVIEW
    // ******************************************************
    ?>
    <div class="row">
      <div class="col-sm-12">
        <div id="link-overview-content" itemprop="description">
          <?php
          if (isset($content['body'])) {
            print render($content['body']);
          }
          elseif (isset($content['field_product']['#object']->body['und']['0']['value']) && $content['field_product']['#object']->body['und']['0']['value']) {
            print render($content['body']);
          }
          ?>
        </div>

      </div>
    </div>
    <?php
    // ******************************************************
    //    CORE MATERIALS
    // ******************************************************
    ?>
    <?php if (isset($content['field_core_materials']['#object']->field_core_materials['und']['0']['value']) && $content['field_core_materials']['#object']->field_core_materials['und']['0']['value']) { ?>
      <div class="row">
        <div class="col-sm-12">
          <span id="link-<?php print drupal_html_class($content['field_core_materials']['#title']); ?>-content" class="product-package-header"><?php //print render($content['field_core_materials']['#title']);  ?></span>
        </div>
      </div>
      <div class="row content-core-materials">
        <div class="col-sm-12">
          <?php print render($content['field_core_materials']); ?>
        </div>
      </div>
    <?php } ?>
    <?php
    // ******************************************************
    //    PRODUCT FEATURES
    // ******************************************************
    ?>
    <!-- commenting this out because this field is not being utilized in this display (only currently in use in the variation display)
    <?php if (isset($content['field_product_features']['#object']->field_product_features['und']['0']['value']) && $content['field_product_features']['#object']->field_product_features['und']['0']['value']) { ?>
      <div class="row">
        <div class="col-sm-12">
          <h2 id="link-<?php print drupal_html_class($content['field_product_features']['#title']); ?>-content" class="product-package-header"><?php print render($content['field_product_features']['#title']); ?></h2>
        </div>
      </div>
      <div class="row content-product-features">
        <div class="col-sm-12">
          <?php print render($content['field_product_features']); ?>
        </div>
      </div>
    <?php } ?>
    -->

    <?php
    // ******************************************************
    //    PACKAGE EXTRAS
    // ******************************************************
    ?>
    <?php if (isset($content['field_package_extras']['#object']->field_package_extras['und']['0']['value']) && $content['field_package_extras']['#object']->field_package_extras['und']['0']['value']) { ?>
      <!--<div class="row package-extras-row"><div class="col-sm-12">
          <div class="product-subheading">
              
                  <h2 id="link-<?php print drupal_html_class($content['field_package_extras']['#title']); ?>-content" class="product-package-header"><?php print render($content['field_package_extras']['#title']); ?></h2>
              </div>

          </div>
      </div></div>-->
      <div class="row package-extras-row">
        <div class="col-sm-12">
          <?php print render($content['field_package_extras']); ?>
        </div>
      </div>
    <?php } ?>

    <?php
    // ******************************************************
    //    POPULAR ADD-ONS
    // ******************************************************
    ?>
    <?php if (isset($content['field_popular_add_ons']['#object']->field_popular_add_ons['und']['0']['value']) && $content['field_popular_add_ons']['#object']->field_popular_add_ons['und']['0']['value']) { ?>


      <!--<div class="row popular-add-ons-row"><div class="col-sm-12">
          <div class="product-subheading">
              
                  <h2 id="link-popular-addons-content" class="product-package-header">Popular Add-Ons</h2>

          </div>
      </div></div>-->
      <div class="row content-popular-add-ons popular-add-ons-row">
        <div class="col-sm-12">
          <?php print render($content['field_popular_add_ons']); ?>
        </div>
      </div>
    <?php } ?>


  </div>

  <?php
  // ******************************************************
  //    CUSTOMER REVIEWS
  // ******************************************************
  ?>
  <?php
  $result = 0;
  $testimonial_term = bootstrap_rcpar_get_current_product_display_page($node->nid);
  $view = views_get_view_result("testimonial_rotator", "block_5", $testimonial_term);
  $result = count($view);
  if ($result) {
    ?>
    <div itemscope itemtype="http://schema.org/Product" class="schema">
      <span itemprop="name"><?php print $title; ?></span>
      <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        Rated <span itemprop="ratingValue">5</span> stars
        based on <span itemprop="reviewCount"><?php print $result; ?></span> customer reviews
      </div>
    </div>
    <div class="customer-reviews-container">
      <div class="row">
        <div class="col-sm-12">
          <h2 id="link-reviews-content" class="product-package-header">Latest
            Reviews</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?php print views_embed_view("testimonial_rotator", "block_5", $testimonial_term); ?>
        </div>
      </div>
    </div>
  <?php } ?>
  <?php
  // ******************************************************
  //    SPECS
  // ******************************************************
  ?>

  <!-- commenting this out because this field is not being utilized in this display (only currently in use in the variation display)
  <?php if (isset($content['field_specs']['#object']->field_specs['und']['0']['value']) && $content['field_specs']['#object']->field_specs['und']['0']['value']) { ?>
    <div class="row">
      <div class="col-sm-12">
        <h2 id="link-<?php print drupal_html_class($content['field_specs']['#title']); ?>-content" class="product-package-header"><?php print render($content['field_specs']['#title']); ?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <?php print render($content['field_specs']); ?>
      </div>
    </div>
  <?php } ?>
  -->

  <?php
  // ******************************************************
  //    FAQ
  // ******************************************************
  ?>
  <?php if (isset($content['field_faq']['#object']->field_faq['und']['0']['value']) && $content['field_faq']['#object']->field_faq['und']['0']['value']) { ?>
    <div class="row faq-row">
      <div class="col-sm-12">
        <h2 id="link-<?php print drupal_html_class($content['field_faq']['#title']); ?>-content" class="product-package-header"><?php print render($content['field_faq']['#title']); ?></h2>
      </div>
    </div>
    <div class="row faq-row">
      <div class="col-sm-12">
        <?php print render($content['field_faq']); ?>
      </div>
    </div>
  <?php } ?>
  <?php
  // ******************************************************
  //    ADDITIONAL REFERENCES
  // ******************************************************
  ?>
  <?php if (isset($content['field_additional_references']['#object']->field_faq['und']['0']['value']) && $content['field_additional_references']['#object']->field_additional_references['und']['0']['value']) { ?>
    <div class="row additional-references">
      <div class="col-sm-12">
        <h3 class="product-package-sub-header"><?php print render($content['field_additional_references']['#title']); ?></h3>
      </div>
    </div>
    <div class="row additional-references">
      <div class="col-sm-12">
        <?php print render($content['field_additional_references']); ?>
      </div>
    </div>
  <?php } ?>

<?php } ?>