<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * This view mode/tpl is used when viewing HHC content from the
 * "My Questions" tab or homework-help-center/user-posts/xxxx
 */
// This tpl identical to the below file. Template suggestions would be more
// appropriate then this include.
// node--helpcenter_question.tpl.php also contains very similar logic and
// it would be nice if all three files could share a preprocess function
// in order to separate logic from presentation.
include (drupal_get_path('theme', 'bootstrap_rcpar') . '/templates/node--alternate_search_result_display_helpcenter_question.tpl.php');