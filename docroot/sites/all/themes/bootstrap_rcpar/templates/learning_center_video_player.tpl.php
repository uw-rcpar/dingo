<div class="learning-center-video-area">
  <?php

  //youtube will never be a presentation
  if ($video_source == 'youtube') {
    ?>
    <div class="row">
      <div class="col-md-8">
        <iframe id="player" width="100%" height="225" src="https://www.youtube.com/embed/<?php echo $youtube_id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3&enablejsapi" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>

  <?php } elseif ($presentation == 0) { ?>


    <div class="row">
      <div class="col-md-8">
        <video width="99%" id="video" controls poster="<?php print $video_thumbnail; ?>">
          <source src="<?php print $video_src; ?>" type="video/mp4"/>
          <?php if ($vtt_url != ''): ?>
              <track label="English" kind="subtitles" srclang="en" src="<?php print $vtt_url; ?>" >
          <?php endif; ?>
        </video>
      </div>
      <div class="col-md-4">
        <?php print render($sharelinks); ?>
      </div>
    </div>


  <?php } elseif ($presentation == 1) { ?>

    <script type="text/javascript" src="<?php print $GLOBALS['base_url']; ?>/sites/all/libraries/popcorn/popcorn.min.js"></script>
    <script>
      document.addEventListener("DOMContentLoaded", function () {

        var pop = Popcorn('#video', {
          pauseOnLinkClicked: true
        });

        //pop.play();

        /*pop.image({
         start: 2,
         end: 10,
         target: 'slides',
         src: 'http://www.rogercpareview.com/broadcast/slide_controller/content/backup_slides/big/Slide1.PNG'
         });*/

        /*var pres_images = [
         { start: 0, end: 160, src: "http://localhost/dingo_drupal/sites/default/files/Slide1.PNG" },
         { start: 160, end: 240, src: "http://localhost/dingo_drupal/sites/default/files/Slide2.PNG" },
         { start: 240, end: 250, src: "http://localhost/dingo_drupal/sites/default/files/Slide3.PNG" },
         ];*/
        //presentation image data passed to json object
        var pres_images = [
          <?php print $popcorn_data; ?>
        ];
        // Loop through the images
        pres_images.forEach(function (pres_image) {
          // Call setCountry() at the start of each country's start time.
          pop.image({
            start: pres_image.start,
            end: pres_image.end,
            target: 'slides',
            src: pres_image.src
            //onStart: function() { setCountry(country.country_name);
          });
        });
      });


    </script>
    <div id="presentation_container" class="row">
      <div id="video_container" class="col-md-7">
        <div class="video-with-slides-wrapper">
          <video width="99%" id="video" poster="<?php print $video_thumbnail; ?>" controls>
            <source src="<?php print $video_src; ?>" type="video/mp4"/>
            <?php if ($vtt_url != ''): ?>
                <track label="English" kind="subtitles" srclang="en" src="<?php print $vtt_url; ?>" >
            <?php endif; ?>
          </video>
        </div>
      </div>
      <div class="col-md-5">
        <div id="slides"></div>
      </div>
      <div style="clear: both"></div>
    </div>

  <?php } ?>

  <?php
  //if the user is not logged in show the modal window on play
  if (!user_is_logged_in()) {
    ?>

    <div style="visibility: hidden;">
      <?php print $modal_link; ?>
    </div>

    <script>
      /*
       var vid = document.getElementById("video");
       vid.onplay = function() {
       vid.pause();
       //simulate the modal button click
       document.getElementsByClassName("ctools-use-modal")[0].click();
       //alert("The video has been paused");
       };
       */
    </script>

  <?php } ?>

</div>