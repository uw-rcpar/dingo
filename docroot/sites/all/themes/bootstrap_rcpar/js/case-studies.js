(function ($) {
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      slideshow: false,
      touch: true,
      animationLoop: false,
    });
  });
})(jQuery);