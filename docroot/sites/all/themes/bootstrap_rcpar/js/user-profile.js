(function($) {
  Drupal.behaviors.user_profile = {
    attach: function (context, settings) {
      // Rebuild form UI since jquery may hav acted on it.
      // This is necessary for the sales_funnel_widget fields on the user profile edit form
      if ($.isFunction($('.selectpicker').selectpicker('refresh'))) {
        $('.selectpicker', context).selectpicker('refresh');
      }
    }
  };
})(jQuery);