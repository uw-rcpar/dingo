(function ($) {

  // a function to read url query strings - used on my courses page
  // and pages with dynamic links
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };

  function slideTo(element) {
    $('html,body').animate({
      scrollTop: $(element).offset().top - 100
    }, 'slow');
  }

  function adjustForNavbarHeight(navbar) {
    if ($(window).width() > 1023 && $('.video-banner').length) {
      return 0;
    }
    else {
      return navbar.outerHeight();
    }
  }
  $(document).ready(function () {
    $('.selectpicker').selectpicker('refresh');


    $('.pricing-slider .choice').on('click', function () {
      $('.pricing-slider-row').hide();
      $('#pricing-slider-' + $(this).attr('id')).show();
      $('.pricing-slider .choice').toggleClass('chosen');
    });

    $('#read-link').on('click', function () {
      slideTo('#reviews-header');
    });

    $('#buy-now-hero-btn').on('click', function () {
      slideTo('.pricing-heading');
    });



    // A simple mechanism to prevent a link from being clicked multiple times - can help prevent
    // race conditions. See PROD-478.
    $('.disable-on-click').click(function (e) {
      $(this).addClass('disabled');

      // Disable subsequent clicks.
      this.onclick = function(event) {
        event.preventDefault();
      };
    });

    // show hide link - used in pagebuilder element
    $('.show-hide').click(function () {
      if ($('.show-hide-content').length) {
        $('.show-hide-link').toggle();
        $('.show-hide-content').removeClass('hidden-content');
        $('.show-hide-content').toggle();
      }
    });


    // Disable selectpicker on scholarship form
    if ($('body').hasClass('page-why-roger-cpa-review-scholarship')) {
      $('.webform-component--graduation-date .selectpicker').selectpicker('destroy');
      $('.webform-component--study-date .selectpicker').selectpicker('destroy');
    }
    // Disable selectpicker on scholarship form
    if ($('body').hasClass('page-international')) {
      $('.webform-component--graduation-date .selectpicker').selectpicker('destroy');
    }

    // Pause flexslider slideshow if modal is open.
    $('.slide-modal').on('shown.bs.modal', function (e) {
        $('.flexslider').flexslider('pause');
    });
    $('.slide-modal').on('hidden.bs.modal', function (e) {
      $('.flexslider').flexslider('play');
    });
    /* disable slideshow swipe for mobile */
    if ($('.flexslider.optionset-hpbanner').length > 0) {
      $('.flexslider.optionset-hpbanner').flexslider({
        animation: "slide",
        controlNav: false,
        touch:false,
        after: function(slide) {
        }
      });
    }

    // Make main nav sticky
    if (!$('body').hasClass("section-dashboard")) {

      /* desktop header */
      var headerPos = $('header#navbar').offset().top + $('header#navbar').outerHeight();
      /* mobile header */
      var headerPosMob = $('#navbar-mobile').offset().top;
      /* free trial sitcky tab */
      if ($('.free-trial-sticky').length) {
        var stickyPosMob = $('.free-trial-sticky').offset().top;
      }
      /* hello bar */
      var helloBarHeight = 0;
      if ($('.container.hello-bar').length) {
        helloBarHeight = $('.container.hello-bar').outerHeight();
      }
      $(window).on('ready scroll resize load', function () {
        var winPos = $(this).scrollTop();
        // DESKTOP
        if ($(window).width() > 1023) {
          if (winPos > headerPos) {
            $('header#navbar').addClass('float-header').css('top','0px');
            $('body').addClass('scrolled');
            winScrolled = true;
          }
          else {
            $('header#navbar').removeClass('float-header');
            $('body').removeClass('scrolled');
            winScrolled = false;
          }
        }
        // TABLETS AND PHONES
        if ($(window).width() < 1024) {
          if (winPos > headerPosMob) {
            $('#navbar-mobile').addClass('float-header').css('top','0px');
            $('body').addClass('scrolled');
            winScrolled = true;
          }
          else {
            $('#navbar-mobile').removeClass('float-header');
            $('body').removeClass('scrolled');
            winScrolled = false;
          }
        }
        // Only adjust body top margin to provide space for hello bar and navbar if the page hasn't been scrolled.
        if (!winScrolled) {
          // Get the hello bar height in case it has changed.
          if ($('.container.hello-bar').length) {
            helloBarHeight = $('.container.hello-bar').outerHeight();
          }
          if ($(window).width() < 1024) {
            $('#navbar-mobile').css({top: helloBarHeight + 'px'});
            $('body').css('margin-top', helloBarHeight + adjustForNavbarHeight($('#navbar-mobile')) + 'px');
          }
          else {
            $('#navbar').css({top: helloBarHeight + 'px'});
            $('body').css('margin-top', helloBarHeight + adjustForNavbarHeight($('#navbar')) + 'px');
          }
        }
        else {
          $('body').css('margin-top', '0px');
        }

        // If the body of the page has a top margin it messes up the position of the monthpicker.
        // Monthpickers have dynamic unique IDs, so we have to find them on the page by looking for a child div
        // that has ".ui-datepicker-header.mtz-monthpicker" classes and get the parent ID.
        $('.ui-datepicker-header.mtz-monthpicker').each(function(index) {
          var parent_id = $(this).parent().attr('id');
          // Apply a negative top margin to compensate for body top margin.
          $('#' + parent_id).css('margin-top', '-' + $('body').css('margin-top'));
        });

        /* mobile free trial thingie */
        if ($('.free-trial-sticky').length) {
          if (winPos >= stickyPosMob) {
            $('.free-trial-sticky').addClass('float-header').css({top: $('#navbar-mobile').outerHeight() + 'px'});
            $('body').addClass('scrolled');
          }
          else {
            $('.free-trial-sticky').removeClass('float-header');
            $('body').removeClass('scrolled');
          }
        }

      });
    }

    // Toggle for mobile nav
    $('.navbar-toggle').unbind('click').click(function () {
      $('#sidebar-menu').toggleClass('active');
    });
    $('#close-x').unbind('click').click(function () {
      $('#sidebar-menu').toggleClass('active');
    });

    // Change a link in the page based on URL querystring parameter.
    if ($('.dynamic-link').length) {
      var link = getUrlParameter('link');
      if (link) {
        $('.dynamic-link').prop('href', '/' + link);
      }
    }
    // do stuff for sticky tabs on my courses page
    if ($('body').hasClass('page-dashboard-my-courses')) {
      // instatiate sticky tabs - this will open specific tabs based on hash in the url
      // the hash should be tab-1, tab-2, tab-3, or tab-4 (matching the classes in the html)
      $('.nav-tabs').stickyTabs();
      // need to add active class to the tab based on hash in url
      var hash = window.location.hash.substring(1);
      if (hash) {
        $('#my-courses-tabs li.' + hash).addClass("active-tab");
      }
      // get the section from the query string
      var section = getUrlParameter('section');
      if (section) {
        // open the section by clicking the header link
        $('.view-course-elements h3.' + section).click();
        // scroll page to section - requires a delay to be sure stickytabs script has finished
        setTimeout(function (){
          $('html, body').animate({scrollTop: $('.view-course-elements h3.' + section).offset().top}, 'fast');
        }, 800);
      }
    }

    // cpa insights page
    // move the anchor tags within the previous html element so there is no line break
    if ($('.show-full-text').length) {
      $('.show-full-text').each(function () {
        $(this).prev().append($(this));
      });
    }
    if ($('.show-partial-text').length) {
      $('.show-partial-text').each(function () {
        $(this).prev().append($(this));
      });
    }
    // show and hide full and partial body text
    $('.show-full-text, .show-partial-text').click(function () {
      $('#' + this.id).parent().parent().toggle();
      $('.' + this.id).toggle();
    });
    // end cpa insights page

    /* don't use retina images for blog posts */
    $('.section-blog .node-blog img').attr('data-no-retina');
    $('.selectpicker').selectpicker({noneSelectedText: ''});
    $("select.selectpicker").focus(function () {
      $(this).next(".bootstrap-select").find('.selectpicker').focus();
    });
    // This allows for tabbing open hamburger menu
    $('.navbar-toggle-2').focusin(
      function () {
        $(this).click();
      }
    );
    $('#cbp-spmenu-s2 section:last-child li:last-child a').focusout(
      function () {
        $('#dashboard-close-x').click();
      }
    );
    $('.btn-menu').on('click', function () {
      $('#mainnav').slideToggle(300);
      $(this).toggleClass('active');
    });
    $('#navbar-mobile #mainnav .sub-menu li a.dropdown-toggle').unbind('mouseenter mouseleave');
    // androids need touchstart, so we'll use it for all mobile devices
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      $('#navbar-mobile #mainnav li .btn-submenu').on('touchstart', function () {
        $(this).toggleClass('active').next('ul').slideToggle(300);
      });
      $('#navbar-mobile #mainnav li a.dropdown-toggle').on('click', function (event) {
        //event.preventDefault();
      });
      // desktop needs click
    } else {
      $('#navbar-mobile #mainnav li .btn-submenu').on('click', function () {
        $(this).toggleClass('active').next('ul').slideToggle(300);
      });
    }



    // accessibility stuff for modals
    $('.modal:not(#ipq-tools)').on('blur', ':focusable', function() {
      var modal = $(this).closest('.modal');
      if(($(modal).data('bs.modal') || {}).isShown){
        setTimeout(function(){
          // We check if the focused element is inside the modal, otherwise, we send the focus to the
          // close button
          if($(modal).has($(':focus')).length == 0 ){
            $(modal).find('.close').focus();
          }
        },10);
      }
      return false;
    });
    var lastFocus;
    // when a modal is being opened
    $('.modal').on('show.bs.modal', function (e) {
      //get the last active element on the page
      lastFocus = document.activeElement;
    });
    // just after a modal has opened, focus on it's "close" button
    $('.modal').on('shown.bs.modal', function (e) {
      if ($(this).find('.close').length > 0) {
        $(this).find('.close').focus();
      }
    });

    // Content sticky nav - state requirements pages, product display nodes, CPA Exam Study Planners page

    if ($('.sticky-nav').length) {
      // heading is whatever row is before the row that contains the sticky nav.
      var heading = $('.sticky-nav').closest('.row').closest('.row');

      var offset = heading.offset();
      var top = offset.top;
      var bottom = top + heading.outerHeight();

      var contentBar = $('.content-nav-bar').outerHeight();

      // add fixed header class when window has scrolled far enough
      $(window).scroll(function () {
        if ($(window).scrollTop() >= top) {
          $('.content-nav-bar').addClass('fixed-header');
        }
        else {
          $('.content-nav-bar').removeClass('fixed-header');
        }
      });

      // links in sticky content nav need to scroll to the relevent content

      $('.scroll-link').click(function () {
        // Remove siblings active class then add it to the current element that was clicked.
        $('.content-nav-bar').children().removeClass('active');
        $(this).addClass('active');
        // If clicked when navbar isn't static
        if ($('.content-nav-bar').hasClass('fixed-header')) {
          // html structure is different for CPA EXam Videos page
          if ($('body').hasClass('page-lc-cpa-exam-videos')) {
            $('html, body').animate({scrollTop: $('#' + 'block-' + this.id).offset().top - contentBar * 2}, 'fast');
          }
          else {
            $('html, body').animate({scrollTop: $('#' + this.id + '-content').offset().top - contentBar - 20 * 2}, 'fast');
          }
        }
        // If clicked when navbar is static
        else {
          // html structure is different for CPA EXam Videos page
          if ($('body').hasClass('page-lc-cpa-exam-videos')) {
            $('html, body').animate({scrollTop: $('#' + 'block-' + this.id).offset().top - contentBar}, 'fast');
          }
          else {
            $('html, body').animate({scrollTop: $('#' + this.id + '-content').offset().top - contentBar - 40 * 2}, 'fast');
          }
        }
      });

      // this link scrolls to top of the page
      $('.back-top-link').click(function () {
        $('html, body').animate({scrollTop: 0}, 'fast');
      });

    }
    // end of content sticky nav state requirements and product display nodes

    // state requirements page - state selector
    $('.state_req').bind('change', function () {
      var url = $(this).val();
      if (url) {
        window.location = url;
      }
      return false;
    });

    // category pages background image height adjustment
    if ($('.background-img-container').length) {
      var left_div_height = $('.page-heading').innerHeight();
      $('.background-img-container').css("height", left_div_height);
    }
    // move webform on Roadmap dedicated page to after main page content
    if ($('body').hasClass('page-free-resources-roadmap-to-success')) {
      $('.region-content .block-webform').insertAfter('#block-system-main');
    }
    // monitoring center dropdown list
    if ($('.dropdown-list').length) {
      $('.dropdown-list').click(function () {
        $('.filter-name').removeClass('active');
        $('#' + $(this).attr('id') + '-name').addClass('active');
      });
    }

    // dashboard and courseware slide push menu
    if ($('body').hasClass('page-study') || $('body').hasClass('page-study-cram') || $('body').hasClass('section-dashboard') || $('body').hasClass('page-act')) {
      var menuLeft = document.getElementById('cbp-spmenu-s1'),
        menuRight = document.getElementById('cbp-spmenu-s2'),
        menuTop = document.getElementById('cbp-spmenu-s3'),
        menuBottom = document.getElementById('cbp-spmenu-s4'),
        showRightPush = document.getElementById('showRightPush'),
        body = document.body;

      showRightPush.onclick = function () {
        classie.toggle(this, 'active');
        classie.toggle(body, 'cbp-spmenu-push-toleft');
        classie.toggle(menuRight, 'cbp-spmenu-open');
        disableOther('showRightPush');
      };

      function disableOther(button) {
        if (button !== 'showRightPush') {
          classie.toggle(showRightPush, 'disabled');
        }
      }

      /* upgrade your course mobile arrows */
      if ($('.bubble-mobile-arrow').length && $('.bubble-mobile-wrapper .fa-check').length && $('.bubble-mobile').length) {
        $(window).on('ready scroll resize load', function () {
          bubbleOffset = $('.bubble-mobile').offset().left;
          checkMarkOffset = $('.bubble-mobile-wrapper .fa-check').first().offset().left - bubbleOffset - 9;
          $('.bubble-mobile-arrow').css({left: checkMarkOffset});
        });
      }
    }
    // END dashboard and courseware slide push menu

    // CPA Exam Study Planners - calendar show and hide events
    $('.morning-1 i, .morning-2 i, .morning-3 i').addClass('fa-caret-down');
    $('.evening-1 i, .evening-2 i, .evening-3 i').addClass('fa-caret-left');

    $('.morning-1').click(function () {
      $('.morning-content-1').slideUp();
      $('.evening-content-1').slideUp();
      var caret = $(this).find("i:first");
      var content = $(this).next();
      contentDisplay();

      function contentDisplay() {
        //All carets pointed left with content closed
        $(".sat-cal-caret").removeClass("fa-caret-down");
        $(".sat-cal-caret").addClass("fa-caret-left");

        //Show clicked content and change caret direction
        if ($(content).is(":hidden")) {
          $(content).slideDown();
          $(caret).removeClass("fa-caret-left");
          $(caret).addClass("fa-caret-down");
        }
      }
    });

    $('.evening-1').click(function () {
      $('.morning-content-1').slideUp();
      $('.evening-content-1').slideUp();
      var caret = $(this).find("i:first");
      var content = $(this).next();
      contentDisplay();

      function contentDisplay() {
        //All carets pointed left with content closed
        $(".sat-cal-caret").removeClass("fa-caret-down");
        $(".sat-cal-caret").addClass("fa-caret-left");

        //Show clicked content and change caret direction
        if ($(content).is(":hidden")) {
          $(content).slideDown();
          $(caret).removeClass("fa-caret-left");
          $(caret).addClass("fa-caret-down");
        }
      }
    });

    $('.morning-2').click(function () {
      $('.morning-content-2').slideUp();
      $('.evening-content-2').slideUp();
      var caret = $(this).find("i:first");
      var content = $(this).next();
      contentDisplay();

      function contentDisplay() {
        //All carets pointed left with content closed
        $(".sun-cal-caret").removeClass("fa-caret-down");
        $(".sun-cal-caret").addClass("fa-caret-left");

        //Show clicked content and change caret direction
        if ($(content).is(":hidden")) {
          $(content).slideDown();
          $(caret).removeClass("fa-caret-left");
          $(caret).addClass("fa-caret-down");
        }
      }
    });

    $('.evening-2').click(function () {
      $('.morning-content-2').slideUp();
      $('.evening-content-2').slideUp();
      var caret = $(this).find("i:first");
      var content = $(this).next();
      contentDisplay();

      function contentDisplay() {
        //All carets pointed left with content closed
        $(".sun-cal-caret").removeClass("fa-caret-down");
        $(".sun-cal-caret").addClass("fa-caret-left");

        //Show clicked content and change caret direction
        if ($(content).is(":hidden")) {
          $(content).slideDown();
          $(caret).removeClass("fa-caret-left");
          $(caret).addClass("fa-caret-down");
        }
      }
    });

    $('.morning-3').click(function () {
      $('.morning-content-3').slideUp();
      $('.evening-content-3').slideUp();
      var caret = $(this).find("i:first");
      var content = $(this).next();
      contentDisplay();

      function contentDisplay() {
        //All carets pointed left with content closed
        $(".sat-cal-caret-3").removeClass("fa-caret-down");
        $(".sat-cal-caret-3").addClass("fa-caret-left");

        //Show clicked content and change caret direction
        if ($(content).is(":hidden")) {
          $(content).slideDown();
          $(caret).removeClass("fa-caret-left");
          $(caret).addClass("fa-caret-down");
        }
      }
    });

    $('.evening-3').click(function () {
      $('.morning-content-3').slideUp();
      $('.evening-content-3').slideUp();
      var caret = $(this).find("i:first");
      var content = $(this).next();
      contentDisplay();

      function contentDisplay() {
        //All carets pointed left with content closed
        $(".sat-cal-caret-3").removeClass("fa-caret-down");
        $(".sat-cal-caret-3").addClass("fa-caret-left");

        //Show clicked content and change caret direction
        if ($(content).is(":hidden")) {
          $(content).slideDown();
          $(caret).removeClass("fa-caret-left");
          $(caret).addClass("fa-caret-down");
        }
      }
    });
    // END CPA Exam Study Planners - calendar show and hide events


    // Troubleshooting Guide page - scroll links
    $('.content-link').click(function () {
      $('html, body').animate({scrollTop: $('#' + this.id + '-content').offset().top - 30}, 'fast');
    });
    $('.top-link').click(function () {
      $('html, body').animate({scrollTop: 0}, 'fast');
    });
    // END Troubleshooting Guide page - scroll links


    /* Add the calendar icon to the datepicker graduation date fields on ASL login create account page */
    if ($("body").hasClass("page-rcpar-asl")) {
      $("#rcpar-asl-create-account-form #edit-graduation-date-new-datepicker-popup-0").datepicker({
        showOn: 'both',
        buttonImage: '/sites/all/themes/bootstrap_rcpar/css/img/icon_calendar.svg',
        buttonImageOnly: true
      });
      $("#rcpar-asl-login-form #edit-graduation-date-datepicker-popup-0").datepicker({
        showOn: 'both',
        buttonImage: '/sites/all/themes/bootstrap_rcpar/css/img/icon_calendar.svg',
        buttonImageOnly: true
      });
    }

    $(".webform-component--date-passed div").removeClass("webform-container-inline");

    // Discount Verification forms

    // for some coupons, we only have one form on the page (eg: PS-STUDENT-VER)
    // for those cases we don't need to
    if ($('.not-logged-in .verify-discount-form-right, .not-logged-in .verify-discount-form-left').length > 1) {
      $('.not-logged-in .verify-discount-form-right form').hide();
    }

    // login and register on checkout page
    if ($("body").hasClass("page-checkout") && document.getElementById("block-block-116") !== null) {
      // login link
      $('#user-login-form').removeClass('col-md-6');
      $('#block-system-main').addClass('page-checkout-checkout');
      $('#block-block-116 .register-link-wrapper a').click(function () {
        $('#commerce-checkout-form-login #user-login-form').show();
        $('#commerce-checkout-form-login #edit-account').hide();
      });
      // regsiter links
      $('#edit-account').removeClass('col-md-6');
      $('#block-block-66 .register-link-wrapper a').removeClass('use-ajax').removeAttr('href');
      $('#block-block-66 .register-link-wrapper a').click(function () {
        $('#commerce-checkout-form-login #edit-account').show();
        $('#commerce-checkout-form-login #user-login-form').hide();
      });
    }
    if (Drupal.settings.discount_verification_current_form == 'verify_discount_login_form') {
      $('#loginnow').click();
    }

    // stop video in modal from playing when modal closes
    $('.modal').on('hidden.bs.modal', function (e) {
      try {
        $(this).find('video').get(0).pause();
      }
      catch (e) {
      }
    });

    // stop videos in carousel from playing when user navigates to knew item in carousel
    $('.flex-control-paging li a').on('click', function (e) {
      try {
        $('video').each(function(){
          $(this).get(0).pause();
        });
      }
      catch (e) {
      }
    });
    // read more collapse text change - in use on CPA Exam Changes page
    $('.read-more-collapse').click(function () {
      $(this).toggleClass('opened');
      if ($(this).hasClass('opened')) {
        $(this).html('Read less on ' + $(this).attr('id') + ' changes <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
      }
      else {
        $(this).html('Read more on ' + $(this).attr('id') + ' changes <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>');
      }
    });
    $('.cpa-insight-modal').on('hidden.bs.modal', function (e) {
      $('#' + $(this).attr('id') + ' iframe').attr('src', $('#' + $(this).attr('id') + ' iframe').attr('src'));
    });
    // Disable/Enable Graduation Date
    $('body.page-user-login, body.page-user-register, body.page-user-edit').on('change', '#edit-field-did-you-graduate-college-und-0 , #edit-commerce-user-profile-pane-field-did-you-graduate-college-und-0', function () {
      if ($(this).is(':checked')) {
        $(".form-item-field-graduation-month-and-year-und-0-value-month button").addClass("disabled");
        $(".form-item-field-graduation-month-and-year-und-0-value-year button").addClass("disabled");
        $(".form-item-field-college-state-list-und button").addClass("disabled");
        $(".form-item-commerce-user-profile-pane-field-college-state-list-und button").addClass("disabled");
        $("#college_wrapper .form-type-select").remove();
        // Remove form required span to college state list if "did you graduate college" is No
        $('#edit-field-college-state-list .form-required').remove();
      }
    });
    $('body.page-user-login, body.page-user-register, body.page-user-edit').on('change', '#edit-field-did-you-graduate-college-und-1 , #edit-commerce-user-profile-pane-field-did-you-graduate-college-und-1', function () {
      if ($(this).is(':checked')) {
        $(".form-item-field-graduation-month-and-year-und-0-value-month button").removeClass("disabled");
        $(".form-item-field-graduation-month-and-year-und-0-value-month li").removeClass("disabled");
        $(".form-item-field-graduation-month-and-year-und-0-value-year button").removeClass("disabled");
        $(".form-item-field-graduation-month-and-year-und-0-value-year li").removeClass("disabled");
        $(".form-item-field-college-state-list-und button").removeClass("disabled");
        $(".form-item-field-college-state-list-und li").removeClass("disabled");
        $(".form-item-commerce-user-profile-pane-field-college-state-list-und button").removeClass("disabled");
        $(".form-item-commerce-user-profile-pane-field-college-state-list-und li").removeClass("disabled");
        // add form required span to college state list if "did you graduate college" is Yes
        $('#edit-field-college-state-list label').append('<span class="form-required">*</span>');
      }
    });

    // add validation for college name field on user register and profile edit form
    $('form.register-form, form.profile-form').on('submit', function () {
      // if "Did you graduate college" field is Yes, "College state" field is selected, and "College name" field is not selected, create an error.
      if ($('#edit-field-did-you-graduate-college-und-1').val() == '1' && $('#edit-field-college-state-list-und').val() != '_none' && $('.college-names-select').val() == '0') {
        $('#college_wrapper').prepend('<div class="college-error-message alert-danger alert messages">You must select a college.</div>').addClass('error');
        return false;
      }
    });

    //Reset
    function clear_form_elements(ele) {

      $(ele).find(':input').each(function () {
        switch (this.type) {
          case 'password':
          case 'text':
          case 'textarea':
          case 'email':
            $(this).val(null);
          case 'checkbox':
          case 'radio':
            this.checked = false;
        }
      });
      $('.webform-component--billing-address , .webform-component--shipping-address-phone').find('.webform-component-select').find('.pull-left').html('- Select -');
      $('.webform-component--billing-address , .webform-component--shipping-address-phone').find('.webform-component-select .dropdown-menu li').removeClass("selected");
    }

    $('.webform-component--reset-form input').click(function () {
      clear_form_elements(this.form);
    });
    //Ends Reset

    // Functions to hide show
    function inquiry_case(selected_case) {
      if (!selected_case) {
        //Hide unwanted fields
        $('.webform-component--partner-discount-document-copy').hide();
        //$('.webform-component--terms-and-conditions-select').hide();
        $('#edit-submitted-documents-partner-ajax-wrapper').hide();
      }

      if (selected_case != 1 || selected_case != 2 || selected_case != 3) {
        $('#edit-submitted-documents-partner-ajax-wrapper').hide();
        $('.webform-component-markup .webform-component--partner-discount-document-copy').hide();
        $('.webform-component--partner-discount-document-copy').hide();
      }
    }

    $('#edit-submitted-current-roger-students-student-support').on('change', function () {
      var selection = $(this).val();
      if (selection == 1) {
        $('.webform-component--what-is-your-connection-type').find('.webform-conditional-disabled').removeClass('webform-conditional-disabled');
        $('.webform-component--what-is-your-connection-type select').removeAttr('disabled');
        $('.webform-component--common-problems').show();
        $('.webform-component--what-is-your-connection-type').show();
      }
      else {
        $('.webform-component--common-problems').hide();
        $('.webform-component--what-is-your-connection-type').find(':input').addClass('webform-conditional-disabled');
        $('.webform-component--what-is-your-connection-type').find(':input').attr('disabled', true);
        $('.webform-component--what-is-your-connection-type').hide();
      }

      $('.webform-component--your-question-fieldset').find('.webform-conditional-disabled').removeClass('webform-conditional-disabled');
      $('.webform-component--your-question-fieldset textarea').removeAttr('disabled');
      $('.webform-component--your-question-fieldset').show();
      $('.webform-component--your-question-fieldset .form-item').show();
    });

    $.each(['show', 'hide'], function (i, ev) {
      var el = $.fn[ev];
      $.fn[ev] = function () {
        this.trigger(ev);
        return el.apply(this, arguments);
      };
    });

    $('.webform-component-fieldset , .webform-component-select , .webform-component-fieldset').on('show', function () {
      $('.selectpicker').selectpicker('refresh');
    });

    /// Same as Billing Address functionality
    $('.webform-component--shipping-address-phone').parent().parent().removeAttr('id');
    $('.webform-component--shipping-address-phone').on('show', function () {
      $(this).parent().parent().removeAttr('id');
    });
    /// ENDS contact form


    /// Bootstrap exceptions
    $('#customer-profile-shipping-ajax-wrapper #customer-profile-shipping-ajax-wrapper').unwrap();
    $('#commerce-shipping-service-ajax-wrapper #commerce-shipping-service-ajax-wrapper').unwrap();
    /// Ends Bootstrap exceptions

    $('fieldset.container-inline').removeClass('container-inline');
    $('.webform-component--current-roger-students #edit-submitted-current-roger-students-student-support-1, .webform-component--current-roger-students #edit-submitted-current-roger-students-student-support-2, .webform-component--current-roger-students #edit-submitted-current-roger-students-student-support-3, .webform-component--current-roger-students #edit-submitted-current-roger-students-student-support-4').change(function () {
      location.href = "/user/login?destination=contact";
    });

    // hide contact form submit button if verifications is chosen

    $('#edit-submitted-products-and-information-fieldset-products-and-information').on('change', function () {
      var selection = $(this).val();
      if (selection == 4) {
        $('.webform-client-form .webform-submit').hide();
      }
      else {
        $('.webform-client-form .webform-submit').show();
      }
    });


    $('#showRightPush').click(function () {
      $('.overlay-container').toggle();
    });
    $('#dashboard-close-x').click(function () {
      $('.cbp-spmenu').removeClass('cbp-spmenu-open');
      $('body').removeClass('cbp-spmenu-push-toleft');
      $('.overlay-container').toggle();
    });

    /*----------------------------------------------------*/
    /*	extras checkout page cart show and hide
    /*---------------------------------------------------- */
    
    if ($("body").hasClass("page-checkout-complete")) {
      $('#block-views-commerce-cart-summary-block-1 h2').addClass("open");
    }
    $('#block-views-commerce-cart-summary-block-1 h2').click(function () {
      // this height stuff is to stop the animation from jumping
      $height = $('#block-views-commerce-cart-summary-block-1 .view-content').height();
      $('#block-views-commerce-cart-summary-block-1 .view-content').css('height', $height);
      $('#block-views-commerce-cart-summary-block-1 .view-content').slideToggle("slow", function () {
        // Animation complete.
        $('#block-views-commerce-cart-summary-block-1 .view-content').css('overflow', 'visible');
      });
      $('#block-views-commerce-cart-summary-block-1 .view-footer').slideToggle("slow", function () {
        // Animation complete.
        $('#block-views-commerce-cart-summary-block-1 .view-footer').css('overflow', 'visible');
      });

      $('#block-views-commerce-cart-summary-block-1 h2').toggleClass("open");
    });

    /*----------------------------------------------------*/
    /*	student of the month filter form
    /*----------------------------------------------------*/

    $('#edit-field-month-value-value-year').on('change', function () {
      var base_url = '/why-roger/student-success-stories?field_month_value[value][year]=';
      var url = $(this).val(); // get selected value
      if (url) { // require a URL
        window.location = base_url + url; // redirect
      }
      return false;
    });

    /*------------------------------------------------------------*/
    /*	bootstrap popovers on course outline page fix for chrome
    /*------------------------------------------------------------*/
    var el = $('[data-toggle="popover"]');

    // Apply a fix for an issue in chrome browser
    // Class use-only-bootstrap will avoid this behaviour
    if (!el.hasClass("use-only-bootstrap")) {
      el
        .on('click', function (e) {
          var el = $(this);
          setTimeout(function () {
            el.popover('show');
          }, 200); // Must occur after document click event below.
        })
        .on('shown.bs.popover', function () {
          $(document).on('click.popover', function () {
            el.popover('hide'); // Hides all
          });
        })
        .on('hide.bs.popover', function () {
          $(document).off('click.popover');
        });
    }
    /*----------------------------------------------------*/
    /*	Affirm checkout button change
    /*----------------------------------------------------*/

    $('#edit-commerce-payment-payment-method-affirmcommerce-payment-affirm').on('click', function () {
      $('#edit-continue').html('Proceed with Monthly Payments');
    });
    $('#edit-commerce-payment-payment-method-authnet-aimcommerce-payment-authnet-aim').on('click', function () {
      $('#edit-continue').html('Place Order');
    });

    /*----------------------------------------------------*/
    /*	breakpoint changes
     /*----------------------------------------------------*/

    $(window).bind('enterBreakpoint320', function () {

      // BLOG LANDING PAGE
      // For phones, move the search form block to before the first blog post
      // and move the blog subscribe block to after the first blog post.
      if ($('body').hasClass('page-blog')) {
        $('#block-rcpar-solr-rcpar-blog-solr-search').insertBefore($('.node').first());
        $('#block-rcpar-mods-rcpar-mods-entity-form-block-blog-subscription').insertAfter($('.node').first());
        $('.blog-feed').insertAfter($('#block-rcpar-mods-rcpar-mods-entity-form-block-blog-subscription'));
      }

      // roadmap page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-free-resources-roadmap-to-success')) {
        $('.region-content .block-webform').addClass('mobile-position');
      }
      // partner page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-why-roger-our-partners')) {
        $('#block-webform-client-block-15872').addClass('mobile-position').insertBefore($('.partner-spotlight'));
      }
      // cpa exam changes page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-exam')) {
        $('.region-content .block-webform').addClass('mobile-position').insertAfter($('.video-left'));
      }
      // cpa exam changes page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-exam-tax-reform-resources')) {
        $('.region-content .block-webform').addClass('mobile-position').insertAfter($('.body-row'));
      }
      // case studies landing page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-smartpath-smartpath-case-studies')) {
        $('.region-content .block-webform').addClass('mobile-position').insertAfter($('.view-case-studies'));
      }
      // mobile app page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-courses-mobile-app')) {
        $('.region-content .block-webform').addClass('mobile-position').insertAfter($('#block-system-main'));
      }
      if ($('body').hasClass('page-cpa-insights')) {
        $('#block-webform-client-block-15872').addClass('mobile-position').insertBefore($('.webform-content-right'));
        $('.webform-content-right').addClass('mobile-position');
      }

      // homepage reviews need to be repositioned for mobile
      if ($('body').hasClass('front')) {
        $('#block-views-home-page-reviews-block .field-reference-1').insertAfter($('#block-views-home-page-reviews-block .field-review-1'));
        $('#block-views-home-page-reviews-block .field-reference-2').insertAfter($('#block-views-home-page-reviews-block .field-review-2'));
        $('#block-views-home-page-reviews-block .field-reference-3').insertAfter($('#block-views-home-page-reviews-block .field-review-3'));
      }

    });

    $(window).bind('exitBreakpoint320', function () {
      // roadmap page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-free-resources-roadmap-to-success')) {
        $('.region-content .block-webform').removeClass('mobile-position');
      }
      // partner page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-why-roger-our-partners')) {
        $('#block-webform-client-block-15872').removeClass('mobile-position').insertBefore($('#block-system-main'));
      }
      // cpa exam changes page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-exam')) {
        $('.region-content .block-webform').removeClass('mobile-position').insertBefore($('#block-system-main'));
      }
      // cpa exam results page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-exam-tax-reform-resources')) {
        $('.region-content .block-webform').removeClass('mobile-position').insertBefore($('#block-system-main'));
      }
      // case studies landing page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-smartpath-smartpath-case-studies')) {
        $('.region-content .block-webform').removeClass('mobile-position').insertBefore($('#block-system-main'));
      }
      // mobile app page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-courses-mobile-app')) {
        $('.region-content .block-webform').addClass('mobile-position').insertBefore($('#block-system-main'));
      }
      if ($('body').hasClass('page-cpa-insights')) {
        $('#block-webform-client-block-15872').removeClass('mobile-position').insertBefore($('#block-system-main'));
        $('.webform-content-right').removeClass('mobile-position');
      }

    });

    $(window).bind('enterBreakpoint480', function () {

      // BLOG LANDING PAGE
      // For phones move the search form block to before the first blog post
      // and move the blog subscribe block to after the first blog post.
      if ($('body').hasClass('page-blog')) {
        $('#block-rcpar-solr-rcpar-blog-solr-search').insertBefore($('.node').first());
        $('#block-rcpar-mods-rcpar-mods-entity-form-block-blog-subscription').insertAfter($('.node').first());
        $('.blog-feed').insertAfter($('#block-rcpar-mods-rcpar-mods-entity-form-block-blog-subscription'));
      }

      // roadmap page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-free-resources-roadmap-to-success')) {
        $('.region-content .block-webform').addClass('mobile-position');
      }
      // partner page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-why-roger-our-partners')) {
        $('#block-webform-client-block-15872').addClass('mobile-position').insertBefore($('.partner-spotlight'));
      }
      // cpa exam changes page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-exam')) {
        $('.region-content .block-webform').addClass('mobile-position').insertAfter($('.video-left'));
      }
      // cpa exam changes page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-exam-tax-reform-resources')) {
        $('.region-content .block-webform').addClass('mobile-position').insertAfter($('.body-row'));
      }
      // case studies landing page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-smartpath-smartpath-case-studies')) {
        $('.region-content .block-webform').addClass('mobile-position').insertAfter($('.view-case-studies'));
      }
      // mobile app page webform needs to be repositioned for mobile
      if ($('body').hasClass('page-cpa-courses-mobile-app')) {
        $('.region-content .block-webform').addClass('mobile-position').insertAfter($('#block-system-main'));
      }
      if ($('body').hasClass('page-cpa-insights')) {
        $('#block-webform-client-block-15872').addClass('mobile-position').insertBefore($('.webform-content-right'));
        $('.webform-content-right').addClass('mobile-position');
      }

      // homepage reviews need to be repositioned for mobile
      if ($('body').hasClass('front')) {
        $('#block-views-home-page-reviews-block .field-reference-1').insertAfter($('#block-views-home-page-reviews-block .field-review-1'));
        $('#block-views-home-page-reviews-block .field-reference-2').insertAfter($('#block-views-home-page-reviews-block .field-review-2'));
        $('#block-views-home-page-reviews-block .field-reference-3').insertAfter($('#block-views-home-page-reviews-block .field-review-3'));
      }

    });

      $(window).bind('exitBreakpoint480', function () {
        // roadmap page webform needs to be repositioned for mobile
        if ($('body').hasClass('page-free-resources-roadmap-to-success')) {
          $('.region-content .block-webform').removeClass('mobile-position');
        }
        // partner page webform needs to be repositioned for mobile
        if ($('body').hasClass('page-why-roger-our-partners')) {
          $('#block-webform-client-block-15872').removeClass('mobile-position').insertBefore($('#block-system-main'));
        }
        // cpa exam page webform needs to be repositioned for mobile
        if ($('body').hasClass('page-cpa-exam')) {
          $('.region-content .block-webform').removeClass('mobile-position').insertBefore($('#block-system-main'));
        }
        // cpa exam page webform needs to be repositioned for mobile
        if ($('body').hasClass('page-cpa-exam-tax-reform-resources')) {
          $('.region-content .block-webform').removeClass('mobile-position').insertBefore($('#block-system-main'));
        }
        // case studies landing page webform needs to be repositioned for mobile
        if ($('body').hasClass('page-smartpath-smartpath-case-studies')) {
          $('.region-content .block-webform').removeClass('mobile-position').insertBefore($('#block-system-main'));
        }
        // mobile app page webform needs to be repositioned for mobile
        if ($('body').hasClass('page-cpa-courses-mobile-app')) {
          $('.region-content .block-webform').addClass('mobile-position').insertBefore($('#block-system-main'));
        }
        if ($('body').hasClass('page-cpa-insights')) {
          $('#block-webform-client-block-15872').removeClass('mobile-position').insertBefore($('#block-system-main'));
          $('.page-cpa-insights .webform-content-right').removeClass('mobile-position');
        }

      });

      $(window).bind('enterBreakpoint768', function () {
        // Move blog sidebar blocks back where they belong for tablets and desktops
        if ($('body').hasClass('page-blog')) {
          $('.region-sidebar-second').prepend($('#block-rcpar-mods-rcpar-mods-entity-form-block-blog-subscription'));
          $('.blog-feed').insertAfter($('#block-rcpar-mods-rcpar-mods-entity-form-block-blog-subscription'));
          $('#block-rcpar-solr-rcpar-blog-solr-search').insertAfter($('#block-block-122'));
        }
        // homepage reviews need to be repositioned for mobile
        if ($('body').hasClass('front')) {
          $('#block-views-home-page-reviews-block .row-field-reference-1').append($('#block-views-home-page-reviews-block .field-reference-1'));
          $('#block-views-home-page-reviews-block .row-field-reference-1').append($('#block-views-home-page-reviews-block .field-reference-2'));
          $('#block-views-home-page-reviews-block .row-field-reference-1').append($('#block-views-home-page-reviews-block .field-reference-3'));
        }
        // Home page video banner
        if ($('.video-banner').length) {
          $('.video-banner').height('auto');
        }

      });

    $(window).bind('enterBreakpoint1024', function () {
      $('#mainnav').hide();
      // Home page video banner
      if ($('.video-banner').length) {
        var topPos = $('.video-banner').offset().top;
        var winHeight = $(window).height();
        if (winHeight-topPos < $('.video-banner img').height()) {
          $('.video-banner').height(winHeight-topPos);
        }
        else {
          $('.video-banner').height($('.video-banner img').height());
        }
      }
    });
      
      $(window).setBreakpoints();


    }
  );

})(jQuery);

