(function ($) {

  $(window).load(function() {

    $('#slider').flexslider({
      animation: "slide",
      controlNav: "thumbnails",
      animationLoop: false,
      slideshow: false,
    });

    // Stop video in flexslider from playing when another slide's thumbnail is clicked.
    $('.flex-control-nav li').on('click touchend', function() {
      try {
        $('#slider').find('video').get(0).pause();
      }
        catch (e) {
      }
    });

    // Stop video in flexslider from playing when flexslider is swiped.
    $('.flex-viewport li').on('touchend', function() {
      // Check the flexslider's UL (slide-container) transform css property.
      // This is the only way to know if the video is being swiped away from.
      // The video is always the third element in the slider, and the transform
      // property will always be "matrix(1, 0, 0, 1, -450, 0)" if the slide being
      // shown is the second slide.
      // This is an arbitrary way of doing this but it works in this instance because
      // the node template always puts the video in the third (and last) slide.
      var el = document.getElementById('slide-container');
      var st = window.getComputedStyle(el, null);
      var tr = st.getPropertyValue("transform");
      if (tr == 'matrix(1, 0, 0, 1, -450, 0)') {
        try {
          $('#slider').find('video').get(0).pause();
        }
        catch (e) {
        }
      }
    });


  });
})(jQuery);