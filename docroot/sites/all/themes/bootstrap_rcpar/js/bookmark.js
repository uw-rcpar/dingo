jQuery(function($) {
  $('#bookmark-this').click(function(e) {
    var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    // IE allows adding a bookmark from a link
    if (window.external && ('AddFavorite' in window.external)) {
      // IE Favorites
      window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {

      // other browsers are more finicky so we'll open a modal with
      // instructions for keystrokes
      $('#bookmark-modal').modal('show');
      // change the copy for Macs
      if (/Mac/i.test(navigator.userAgent)) {
        $('.keystroke').html('Cmd');
      }
    }

    return false;
  });
});
