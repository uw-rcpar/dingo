(function($) {
  
    $(document).ready(function() {
        /*----------------------------------------------------*/
        /*	learning center transcript show and hide
         /*----------------------------------------------------*/

        $('.full-transcript-link').click(function() {
            $('.open-transcript-wrapper').toggle();
            $('.close-transcript-wrapper').toggle();
            $('.transcript').toggle();
        });
        $('.close-transcript-link').click(function() {
            $('.open-transcript-wrapper').toggle();
            $('.close-transcript-wrapper').toggle();
            $('.transcript').toggle();
        });


        /*----------------------------------------------------*/
        /*	learning center comment show and hide
         /*----------------------------------------------------*/

        $('.add-comment-link').click(function() {
            $('#comment-form').toggle();

            var item = $('#comment-form');
            var offset = item.offset();
            var top = offset.top;
            var bottom = top + item.outerHeight();

            $('html, body').animate({scrollTop: $('#comment-form').offset().top - 50}, 'fast');

        });

        /*----------------------------------------------------*/
        /*	learning center latest blog posts show and hide
         /*----------------------------------------------------*/

        $('.view-more-blogs').click(function() {
          if ($(this).hasClass('blogs-plus')) {
            $('.view-more-blogs').removeClass('blogs-plus');
            $('.view-more-blogs').addClass('blogs-minus');
            $('.view-more-blogs').html('-');
            $('.node .latest-blog ul li').show();
          } else {
            $('.view-more-blogs').removeClass('blogs-minus');
            $('.view-more-blogs').addClass('blogs-plus');
            $('.view-more-blogs').html('+');
            $('.node .latest-blog ul li').hide();
            $('.node .latest-blog ul li.views-row-1').show();
            $('.node .latest-blog ul li.views-row-2').show();
            $('.node .latest-blog ul li.views-row-3').show();
          }         
        });


        /*----------------------------------------------------*/
        /*	learning center term search move label to placeholder
         /*----------------------------------------------------*/
        $('#views-exposed-form-dictionary-term-search-page-1 :input').each(function(index, elem) {
            var eId = $(elem).attr('id');
            var label = null;
            if (eId && (label = $(elem).parents('form').find('label[for='+eId+']')).length === 1) {
                $(elem).attr('placeholder', $(label).html().trim());
                $(label).addClass('hide-label');
            }
        });
        
        /*----------------------------------------------------*/
        /*	learning center alphabar sticky nav
        /*----------------------------------------------------*/
        if ($(".dictionary-heading").length && $("body").hasClass("page-lc")) {
          // get the top position of the page header
          var dictionary_heading = $('.dictionary-heading');
          var heading_offset = dictionary_heading.offset();
          var heading_top = heading_offset.top;
          
          // calculate the space needed to scroll before 
          // the alphabar needs to be fixed in position 
          var bottom = heading_top + dictionary_heading.outerHeight();
          
          // when the window is scrolled past the point 
          // the alphabar needs to become fixed,
          // give it the fixed-header class
          // otherwise remove it
          $(window).scroll(function() {
            if ($(window).scrollTop() >= bottom) {
              $('.lexicon-links').addClass('fixed-header');
              $('.lexicon-list').addClass('with-fixed-header');
            } else {
              $('.lexicon-links').removeClass('fixed-header');
              $('.lexicon-list').removeClass('with-fixed-header');
            }
          });
        }

        // inline links need to be adjusted because
        // the alphabar sticky nav covers them up
        
        // this will adjust the scroll position 
        // when clicking on an inline link
        // while already on the dictionary page
        $('.lexicon-item').click(function() {
          var link_id = this.href.split("#")[1];
          var letter_linkid_pos = $('#' + link_id).offset();
          $('html, body').animate({scrollTop: letter_linkid_pos.top - 204}, 'fast');
        });
        
        
        // this will adjust the scroll position
        // when loading the dictionary page
        // from another page
        
        // to top right away
            if ( window.location.hash ) scroll(0,0);
            // void some browsers issue
            setTimeout( function() { scroll(0,0); }, 1);

            // any position
            $(function() {
                // your current click function
                $('.scroll').on('click', function(e) {
                    e.preventDefault();
                    $('html, body').animate({
                        scrollTop: $($(this).attr('href')).offset().top - 204 + 'px'
                    }, 1000, 'swing');
                });
                // *only* if we have anchor on the url
                if(window.location.hash) {
                    // smooth scroll to the anchor id
                    $('html, body').animate({
                        scrollTop: $(window.location.hash).offset().top - 204 + 'px'
                    }, 1000, 'swing');
                }
            });
        
        /*----------------------------------------------------*/
        /*	learning center dictionary term list
        /*----------------------------------------------------*/
        
        // This function can be used to wrap elements on the page in divs.
        // Used on dictionary page to create two columns, below.
        $.fn.wrapMatch = function(count, className) {
          var length = this.length;
          for(var i = 0; i < length ; i+=count) {
            this.slice(i, i+count).wrapAll('<div '+((typeof className == 'string')?'class="'+className+'"':'')+'/>');
          }
          return this;
        }; 
        
        // for each letter with terms listed,
        // wrap more than 10 terms in a div
        $('.letter-list').each(function (index, value){
          // get the element's id to only target each
          // section of letters
          var elem_id = $(this).attr('id');
          // pass it through our function
          $('#' + elem_id + ' .letter-list-item').wrapMatch(100,'col-sm-6 col-xs-6');
        });
        
		});
})(jQuery);        
