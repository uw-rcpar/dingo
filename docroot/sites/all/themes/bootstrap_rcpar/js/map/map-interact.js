﻿(function($) {
  $(document).ready(function () {  


var map_config = {
	'default':{
		'bordercolor':'#ffffff', //inter-state borders
		'lakescolor':'#ffffff', //lakes color
		'shadowcolor':'#000000', //shadow color below the map
		'shadowOpacity':'0', //shadow opacity, value, 0-100
		'namescolor':'#ffffff', //color of the abbreviations 
		'namesShadowColor':'#666666', //tooltip shadow color
	},
	'map_1':{
		'namesId':'AL',//name's ID (Don't change it)
		'name': 'ALABAMA',  //state name
		'url':'/cpa-exam/about/requirements-by-state/alabama', //Goto URL
		'target':'_self', //open link in new window:_blank, open in current window:_self
		'upcolor':'#386aab', //state's color when page loads
		'overcolor':'#4684c5', //state's color when mouse hover
		'overcolor':'#4684c5',//state's color when mouse clicking
		'enable':true,//true/false to enable/disable this state
	},
	'map_2':{
		'namesId':'AK',
		'name': 'ALASKA',
		'url':'/cpa-exam/about/requirements-by-state/alaska',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_3':{
		'namesId':'AZ',
		'name': 'ARIZONA',
		'url':'/cpa-exam/about/requirements-by-state/arizona',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_4':{
		'namesId':'AR',
		'name': 'ARKANSAS',
		'url':'/cpa-exam/about/requirements-by-state/arkansas',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_5':{
		'namesId':'CA',
		'name': 'CALIFORNIA',
		'url':'/cpa-exam/about/requirements-by-state/california',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_6':{
		'namesId':'CO',
		'name': 'COLORADO',
		'url':'/cpa-exam/about/requirements-by-state/colorado',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_7':{
		'namesId':'CT',
		'name': 'CONNECTICUT',
		'url':'/cpa-exam/about/requirements-by-state/connecticut',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_8':{
		'namesId':'DE',
		'name': 'DELAWARE',
		'url':'/cpa-exam/about/requirements-by-state/delaware',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_9':{
		'namesId':'FL',
		'name': 'FLORIDA',
		'url':'/cpa-exam/about/requirements-by-state/florida',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_10':{
		'namesId':'GA',
		'name': 'GEORGIA',
		'url':'/cpa-exam/about/requirements-by-state/georgia',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_11':{
		'namesId':'HI',
		'name': 'HAWAII',
		'url':'/cpa-exam/about/requirements-by-state/hawaii',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_12':{
		'namesId':'ID',
		'name': 'IDAHO',
		'url':'/cpa-exam/about/requirements-by-state/idaho',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_13':{
		'namesId':'IL',
		'name': 'ILLINOIS',
		'url':'/cpa-exam/about/requirements-by-state/illinois',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_14':{
		'namesId':'IN',
		'name': 'INDIANA',
		'url':'/cpa-exam/about/requirements-by-state/indiana',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_15':{
		'namesId':'IA',
		'name': 'IOWA',
		'url':'/cpa-exam/about/requirements-by-state/iowa',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_16':{
		'namesId':'KS',
		'name': 'KANSAS',
		'url':'/cpa-exam/about/requirements-by-state/kansas',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_17':{
		'namesId':'KY',
		'name': 'KENTUCKY',
		'url':'/cpa-exam/about/requirements-by-state/kentucky',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_18':{
		'namesId':'LA',
		'name': 'LOUISIANA',
		'url':'/cpa-exam/about/requirements-by-state/louisiana',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_19':{
		'namesId':'ME',
		'name': 'MAINE',
		'url':'/cpa-exam/about/requirements-by-state/maine',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_20':{
		'namesId':'MD',
		'name': 'MARYLAND',
		'url':'/cpa-exam/about/requirements-by-state/maryland',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_21':{
		'namesId':'MA',
		'name': 'MASSACHUSETTS',
		'url':'/cpa-exam/about/requirements-by-state/massachusetts',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_22':{
		'namesId':'MI',
		'name': 'MICHIGAN',
		'url':'/cpa-exam/about/requirements-by-state/michigan',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_23':{
		'namesId':'MN',
		'name': 'MINNESOTA',
		'url':'/cpa-exam/about/requirements-by-state/minnesota',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_24':{
		'namesId':'MS',
		'name': 'MISSISSIPPI',
		'url':'/cpa-exam/about/requirements-by-state/mississippi',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_25':{
		'namesId':'MO',
		'name': 'MISSOURI',
		'url':'/cpa-exam/about/requirements-by-state/missouri',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_26':{
		'namesId':'MT',
		'name': 'MONTANA',
		'url':'/cpa-exam/about/requirements-by-state/montana/',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_27':{
		'namesId':'NE',
		'name': 'NEBRASKA',
		'url':'/cpa-exam/about/requirements-by-state/nebraska',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_28':{
		'namesId':'NV',
		'name': 'NEVADA',
		'url':'/cpa-exam/about/requirements-by-state/nevada',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_29':{
		'namesId':'NH',
		'name': 'NEW HAMPSHIRE',
		'url':'/cpa-exam/about/requirements-by-state/new-hampshire',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_30':{
		'namesId':'NJ',
		'name': 'NEW JERSEY',
		'url':'/cpa-exam/about/requirements-by-state/new-jersey',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_31':{
		'namesId':'NM',
		'name': 'NEW MEXICO',
		'url':'/cpa-exam/about/requirements-by-state/new-mexico',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_32':{
		'namesId':'NY',
		'name': 'NEW YORK',
		'url':'/cpa-exam/about/requirements-by-state/new-york',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_33':{
		'namesId':'NC',
		'name': 'NORTH CAROLINA',
		'url':'/cpa-exam/about/requirements-by-state/north-carolina',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_34':{
		'namesId':'ND',
		'name': 'NORTH DAKOTA',
		'url':'/cpa-exam/about/requirements-by-state/north-dakota',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_35':{
		'namesId':'OH',
		'name': 'OHIO',
		'url':'/cpa-exam/about/requirements-by-state/ohio',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_36':{
		'namesId':'OK',
		'name': 'OKLAHOMA',
		'url':'/cpa-exam/about/requirements-by-state/oklahoma',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_37':{
		'namesId':'OR',
		'name': 'OREGON',
		'url':'/cpa-exam/about/requirements-by-state/oregon',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_38':{
		'namesId':'PA',
		'name': 'PENNSYLVANIA',
		'url':'/cpa-exam/about/requirements-by-state/pennsylvania',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_39':{
		'namesId':'RI',
		'name': 'RHODE ISLAND',
		'url':'/cpa-exam/about/requirements-by-state/rhode-island',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_40':{
		'namesId':'SC',
		'name': 'SOUTH CAROLINA',
		'url':'/cpa-exam/about/requirements-by-state/south-carolina',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_41':{
		'namesId':'SD',
		'name': 'SOUTH DAKOTA',
		'url':'/cpa-exam/about/requirements-by-state/south-dakota',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_42':{
		'namesId':'TN',
		'name': 'TENNESSEE',
		'url':'/cpa-exam/about/requirements-by-state/tennessee',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_43':{
		'namesId':'TX',
		'name': 'TEXAS',
		'url':'/cpa-exam/about/requirements-by-state/texas',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_44':{
		'namesId':'UT',
		'name': 'UTAH',
		'url':'/cpa-exam/about/requirements-by-state/utah',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_45':{
		'namesId':'VT',
		'name': 'VERMONT',
		'url':'/cpa-exam/about/requirements-by-state/vermont',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_46':{
		'namesId':'VA',
		'name': 'VIRGINIA',
		'url':'/cpa-exam/about/requirements-by-state/virginia',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_47':{
		'namesId':'WA',
		'name': 'WASHINGTON',
		'url':'/cpa-exam/about/requirements-by-state/washington',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_48':{
		'namesId':'WV',
		'name': 'WEST VIRGINIA',
		'url':'/cpa-exam/about/requirements-by-state/west-virginia',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},	
	'map_49':{
		'namesId':'WI',
		'name': 'WISCONSIN',
		'url':'/cpa-exam/about/requirements-by-state/wisconsin',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_50':{
		'namesId':'WY',
		'name': 'WYOMING',
		'url':'/cpa-exam/about/requirements-by-state/wyoming',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
	'map_51':{
		'namesId':'DC',
		'name': 'WASHINGTON DC',
		'url':'/cpa-exam/about/requirements-by-state/district-of-columbia',
		'target':'_self',
		'upcolor':'#386aab',
		'overcolor':'#4684c5',
		'overcolor':'#4684c5',
		'enable':true,
	},
}


$(function(){
			addEvent('map_1');
			addEvent('map_2');
			addEvent('map_3');
			addEvent('map_4');
			addEvent('map_5');
			addEvent('map_6');
			addEvent('map_7');
			addEvent('map_8');
			addEvent('map_9');
			addEvent('map_10');
			addEvent('map_11');
			addEvent('map_12');
			addEvent('map_13');
			addEvent('map_14');
			addEvent('map_15');
			addEvent('map_16');
			addEvent('map_17');
			addEvent('map_18');
			addEvent('map_19');
			addEvent('map_20');
			addEvent('map_21');
			addEvent('map_22');
			addEvent('map_23');
			addEvent('map_24');
			addEvent('map_25');
			addEvent('map_26');
			addEvent('map_27');
			addEvent('map_28');
			addEvent('map_29');
			addEvent('map_30');
			addEvent('map_31');
			addEvent('map_32');
			addEvent('map_33');
			addEvent('map_34');
			addEvent('map_35');
			addEvent('map_36');
			addEvent('map_37');
			addEvent('map_38');
			addEvent('map_39');
			addEvent('map_40');
			addEvent('map_41');
			addEvent('map_42');
			addEvent('map_43');
			addEvent('map_44');
			addEvent('map_45');
			addEvent('map_46');
			addEvent('map_47');
			addEvent('map_48');
			addEvent('map_49');
			addEvent('map_50');
			addEvent('map_51');
})

$(function(){
	//lakes's color and border color
	if($('#lakes').find('path').eq(0).attr('fill') != 'undefined'){
		$('#lakes').find('path').attr({'fill':map_config['default']['lakescolor']}).css({'stroke':map_config['default']['bordercolor']});
	}

	//configuration for title text's shadow
	$('.tip').css({
		'box-shadow':'1px 2px 4px '+map_config['default']['namesShadowColor'],
		'-moz-box-shadow':'2px 3px 6px '+map_config['default']['namesShadowColor'],
		'-webkit-box-shadow':'2px 3px 6px '+map_config['default']['namesShadowColor'],
	});

	//configuration for map shadow
	if($('#shadow').find('path').eq(0).attr('fill') != 'undefined'){
		var shadowOpacity = map_config['default']['shadowOpacity'];
		var shadowOpacity = parseInt(shadowOpacity);
		if (shadowOpacity >=100){shadowOpacity = 1;}else if(shadowOpacity <=0){shadowOpacity =0;}else{shadowOpacity = shadowOpacity/100;}
		
		$('#shadow').find('path').attr({'fill':map_config['default']['shadowcolor']}).css({'fill-opacity':shadowOpacity})
	}
});

function addEvent(id,relationId){
	var _obj = $('#'+id);
	var _Textobj = $('#'+id+','+'#'+map_config[id]['namesId']);
	var _h = $('#map').height();

	$('#'+map_config[id]['namesId']).find('tspan').attr({'fill':map_config['default']['namescolor']});

		_obj.attr({'fill':map_config[id]['upcolor'],'stroke':map_config['default']['bordercolor']});
		_Textobj.attr({'cursor':'default'});
		if(map_config[id]['enable'] == true){
			_Textobj.attr({'cursor':'pointer'});
			_Textobj.hover(function(){
				//moving in/out effect
				$('#tip').show().html(map_config[id]['name']);
				_obj.css({'fill':map_config[id]['overcolor']})

			},function(){
				$('#tip').hide();
				$('#'+id).css({'fill':map_config[id]['upcolor']});
			})
			//clicking effect
			_Textobj.mousedown(function(){
				$('#'+id).css({'fill':map_config[id]['downcolor']});
			})
			_Textobj.mouseup(function(){
				$('#'+id).css({'fill':map_config[id]['overcolor']});
				if(map_config[id]['target'] == '_blank'){
					window.open(map_config[id]['url']);	
				}else{
					window.location.href=map_config[id]['url'];
				}
			})
			_Textobj.mousemove(function(e){
				var x=e.pageX-120, y=e.pageY+(-138);
				var tipw=$('#tip').outerWidth(), tiph=$('#tip').outerHeight(), 
				x=(x+tipw>$(document).scrollLeft()+$(window).width())? x-tipw-(20*2) : x
				y=(y+tiph>$(document).scrollTop()+$(window).height())? $(document).scrollTop()+$(window).height()-tiph-10 : y
				$('#tip').css({left:x, top:y})
			})
		}	
}


  });
})( jQuery );