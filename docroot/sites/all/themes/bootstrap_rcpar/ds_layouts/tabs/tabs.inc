<?php
function ds_tabs() {
  return array(
    'label' => t('Tabs'),
    'regions' => array(
      'top' => t('Top'),
      'upper' => t('Upper'),
      'topleft' => t('Upper Left'),
      'topright' => t('Upper Right'),
      'central' => t('Middle'),
      'tabs' => t('Tabs'),
      'bottom' => t('Bottom'),
    ),
    'form' => TRUE,
  );
}