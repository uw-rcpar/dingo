<?php
/**
 * @file
 * Bootstrap 6-6 bricks template for Display Suite.
 */
?>

<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes; ?> tabs-layout">
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>
  <?php if ($top) : ?>
    <div class="row first-row">
      <<?php print $top_wrapper; ?> class="col-sm-12 <?php print $top_classes; ?>">
			<div class="page-heading">
				<?php print $top; ?>
			</div>
      </<?php print $top_wrapper; ?>>
    </div>
  <?php endif; ?>
  <?php if ($upper) : ?>
    <div class="row upper-row">
      <<?php print $upper_wrapper; ?> class="col-sm-12 <?php print $upper_classes; ?>">
        <?php print $upper; ?>
      </<?php print $upper_wrapper; ?>>
    </div>
  <?php endif; ?>
  <?php if ($topleft || $topright) : ?>
    <div class="row second-row clearfix">
      <<?php print $topleft_wrapper; ?> class="col-sm-6 <?php print $topleft_classes; ?>">
        <?php print $topleft; ?>
      </<?php print $topleft_wrapper; ?>>
      <<?php print $topright_wrapper; ?> class="col-sm-6 <?php print $topright_classes; ?>">
        <?php print $topright; ?>
  <!-- Please call pinit.js only once per page -->
<script async defer data-pin-hover="true" data-pin-color="red" src="//assets.pinterest.com/js/pinit.js"></script>
      </<?php print $topright_wrapper; ?>>
    </div>
  <?php endif; ?>
<?php if ($central) : ?>
  <div class="row third-row">
  <<?php print $central_wrapper; ?> class="col-sm-12 <?php print $central_classes; ?>">
  <?php print $central; ?>
  </<?php print $central_wrapper; ?>>
  </div>
<?php endif; ?>
  <?php if ($tabs) : ?>
    <div class="row fourth-row">

    <div class="col-sm-12">
      <div class="content-nav-bar sticky-nav">
        <a class="product-link scroll-link" id="link-exam-req">CPA Exam Requirements</a><a class="product-link scroll-link" id="link-lic-req">License Requirements</a><a class="product-link scroll-link" id="link-state-board">State Board Contacts</a><a class="product-link scroll-link" id="link-resources">Resources</a><a class="product-link scroll-link" id="link-fees">Fees</a><a class="product-link-add-to-cart back-top-link"><i class="fa fa-chevron-up"></i></a>
      </div><!-- end .content-nav-bar -->
    </div><!-- end .col-sm-12 -->

    <div class="row">
      <div class="col-sm-12">
        <h2 id="link-exam-req-content" class="state-req-header first-header">CPA Exam Requirements</h2>
        <h3>Education Requirements</h3>
        <?php print render($content['field_education_requirements']); ?>
        <h3>Residency Requirements</h3>
        <?php print render($content['field_residency_requirements']); ?>
      </div><!-- end .col-sm-12 -->
    </div><!-- end .row -->



    <div class="row">
      <div class="col-sm-12">
        <h2 id="link-lic-req-content"  class="state-req-header">License Requirements</h2>
        <?php print render($content['field_license_requirements']); ?>
      </div><!-- end .col-sm-12 -->
    </div><!-- end .row -->



    <div class="row">
      <div class="col-sm-12">
        <h2 id="link-state-board-content" class="state-req-header">State Board Contacts</h2>
        <?php print render($content['field_state_board_contacts']); ?>
      </div><!-- end .col-sm-12 -->
    </div><!-- end .row -->



    <div class="row">
      <div class="col-sm-12">
        <h2 id="link-resources-content" class="state-req-header">Resources</h2>
        <?php print render($content['field_resources']); ?>
      </div><!-- end .col-sm-12 -->
    </div><!-- end .row -->


    <div class="row">
      <div class="col-sm-12">
        <h2 id="link-fees-content" class="state-req-header">Fees</h2>
        <?php print render($content['field_fees']); ?>
      </div><!-- end .col-sm-12 -->
    </div><!-- end .row -->

  <?php endif; ?> 
  <?php if ($bottom) : ?>
    <div class="row fourth-row">
      <<?php print $bottom_wrapper; ?> class="col-sm-12 <?php print $bottom_classes; ?>">
        <?php print $bottom; ?>
      </<?php print $bottom_wrapper; ?>>
    </div>
  <?php endif; ?> 
</<?php print $layout_wrapper ?>>


<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
