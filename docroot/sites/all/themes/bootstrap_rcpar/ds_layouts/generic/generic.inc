<?php
function ds_generic() {
  return array(
    'label' => t('Generic'),
    'regions' => array(
      'top' => t('Top'),
      'upper' => t('Upper'),
      'topleft' => t('Upper Left'),
      'topright' => t('Upper Right'),
      'central' => t('Middle'),
      'middleleft' => t('Middle Left'),
      'middleright' => t('Middle Right'),
      'lower' => t('Lower'),
      'lowerleft' => t('Lower Left'),
      'lowerright' => t('Lower Right'),
      'bottom' => t('Bottom'),
      'bottomleft' => t('Bottom Left'),
      'bottomright' => t('Bottom Right'),
      'footer' => t('Footer'),
    ),
    'form' => TRUE,
    'image' => FALSE,
  );
}