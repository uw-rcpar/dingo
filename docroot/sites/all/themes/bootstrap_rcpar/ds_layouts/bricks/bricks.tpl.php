<?php
/**
 * @file
 * Bootstrap 6-6 bricks template for Display Suite.
 */
?>
<?php
// do a bunch of stuff for hero images with text overlay
$hero_img = false;
$overlay_title = '';
$overlay_content = '';
$title_align = '';
$overlay_height = '';
if (isset($field_hero_image['und'][0]['value']) && $field_hero_image['und'][0]['value'] == '1' && isset($field_image_basic_page[LANGUAGE_NONE][0]['uri'])) {
  $hero_img = true;
}
if ($field_overlay_content["und"][0]["value"] == '1') {
  $overlay_content = ' overlay-content';
}
if (isset($field_title_position["und"][0]["value"])) {
  $title_align = $field_title_position["und"][0]["value"];
  switch ($title_align) {
    case 'over-left':
    case 'over-right':
    case 'over-center':
      $overlay_title = ' overlay-title';
      $overlay_height = 'height: ' . $field_image_basic_page["und"][0]["height"] . 'px;';
      $hero_bg_style = 'background-image: url(' . file_create_url($field_image_basic_page[LANGUAGE_NONE][0]["uri"]) .');background-repeat: no-repeat;background-size: cover;background-position:center top;';
      $hero_bg_style_tablet = 'background-image: url(' . file_create_url($field_tablet_hero_image[LANGUAGE_NONE][0]["uri"]) .');background-repeat: no-repeat;background-size: cover;background-position:center top;';
      $hero_bg_style_phone = 'background-image: url(' . file_create_url($field_phone_hero_image[LANGUAGE_NONE][0]["uri"]) .');background-repeat: no-repeat;background-size: cover;background-position:center top;';
  }
}


// desktops
$hero_class = '';
// 991px - portrait tablets images
$tablet_hero_class = '';
// 768px - phone images
$phone_hero_class = '';
if (isset($field_tablet_hero_image["und"][0]["uri"])) {
  $hero_class = ' hidden-xs hidden-sm';
  $tablet_hero_class = ' hidden-md hidden-lg';
}
if (isset($field_phone_hero_image["und"][0]["uri"])) {
  $hero_class = ' hidden-xs hidden-sm';
  $tablet_hero_class = ' hidden-md hidden-lg hidden-xs';
  $phone_hero_class = ' hidden-sm hidden-md hidden-lg';
}


if (isset($field_intro_text_position["und"][0]["value"])) {
  $intro_align = ' intro-' . $field_intro_text_position["und"][0]["value"];
}

?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes; ?> bricks-layout
<?php if ($hero_img) { print 'with-hero-image'; } ?>
  <?php print $overlay_content; ?>
  <?php print $overlay_title; ?>
  <?php print ' title-' . $title_align; ?>
  <?php print $intro_align;  ?>">

  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>
  <?php if ($top) : ?>
    <?php if ($variables["node"]->type != 'page' || !$hero_img) { ?>
    <div class="row first-row">
      <<?php print $top_wrapper; ?> class="<?php print $top_classes; ?> text-wrapper" style="<?php print $overlay_height; ?>">
      <div class="text-wrapper-inner">
        <?php print $top; ?>
      </div>
      </<?php print $top_wrapper; ?>>
      </div>
    <?php
    }
    else {
      ?>
    <!-- desktop -->
    <?php if ($hero_img) { ?>
      <div class="row first-row hero-large <?php print $hero_class; ?>" style="<?php print $hero_bg_style; ?>">
        <<?php print $top_wrapper; ?> class="<?php print $top_classes; ?> text-wrapper" style="<?php print $overlay_height; ?>">
        <div class="text-wrapper-inner">
          <?php print $top; ?>
        </div>
        </<?php print $top_wrapper; ?>>
      </div>
    <?php  } ?>
    <!-- tablet -->
    <?php if (!empty($tablet_hero_class)) { ?>
      <div class="row first-row hero-medium <?php print $tablet_hero_class; ?>"  style="<?php print $hero_bg_style_tablet; ?>">
        <<?php print $top_wrapper; ?> class="<?php print $top_classes; ?> text-wrapper" style="<?php print $overlay_height; ?>">
        <div class="text-wrapper-inner">
          <?php print $top; ?>
        </div>
        </<?php print $top_wrapper; ?>>
        </div>
    <?php  } ?>
    <!-- phone -->
    <?php if (!empty($phone_hero_class)) { ?>
      <div class="row first-row hero-small <?php print $phone_hero_class; ?>" style="<?php print $hero_bg_style_phone; ?>">
        <<?php print $top_wrapper; ?> class="<?php print $top_classes; ?> text-wrapper" style="<?php print $overlay_height; ?>">
        <div class="text-wrapper-inner">
          <?php print $top; ?>
        </div>
        </<?php print $top_wrapper; ?>>
      </div>
    <?php  } ?>

    <?php } ?>
  <?php endif; ?>


  <?php
  /**
   * hero image banner
   */
  ?>
<?php if ($hero_img && !$overlay_title) {?>
  <div class="hero-image"><?php print '<img src="' . file_create_url($field_image_basic_page[LANGUAGE_NONE][0]['uri']) .'" />'; ?></div>
<?php } ?>


  <div class="content-wrapper<?php if (($field_overlay_content["und"][0]["value"] == '1')) { print ' has-hero-image'; } ?>">

    <?php if ($upper) : ?>
      <div class="row upper-row">
        <<?php print $upper_wrapper; ?> class="col-sm-12 <?php print $upper_classes; ?>">
          <?php print $upper; ?>
        </<?php print $upper_wrapper; ?>>
      </div>
    <?php endif; ?>
    <?php if ($topleft || $topright) : ?>
      <div class="row second-row">
        <<?php print $topleft_wrapper; ?> class="col-sm-6 <?php print $topleft_classes; ?>">
          <?php print $topleft; ?>
        </<?php print $topleft_wrapper; ?>>
        <<?php print $topright_wrapper; ?> class="col-sm-6 <?php print $topright_classes; ?>">
          <?php print $topright; ?>
        </<?php print $topright_wrapper; ?>>
      </div>
    <?php endif; ?>
    <?php if ($central) : ?>
      <div class="row third-row">
        <<?php print $central_wrapper; ?> class="col-sm-12 <?php print $central_classes; ?>">
          <?php print $central; ?>
        </<?php print $central_wrapper; ?>>
      </div>
    <?php endif; ?>
    <?php if ($middleleft || $middleright) : ?>
      <div class="row fourth-row">
        <<?php print $middleleft_wrapper; ?> class="col-sm-6 <?php print $middleleft_classes; ?>">
          <?php print $middleleft; ?>
        </<?php print $middleleft_wrapper; ?>>
        <<?php print $middleright_wrapper; ?> class="col-sm-6 <?php print $middleright_classes; ?>">
          <?php print $middleright; ?>
        </<?php print $middleright_wrapper; ?>>
      </div>
    <?php endif; ?>
    <?php if ($lower) : ?>
      <div class="row fifth-row">
        <<?php print $lower_wrapper; ?> class="col-sm-12 <?php print $lower_classes; ?>">
          <?php print $lower; ?>
        </<?php print $lower_wrapper; ?>>
      </div>
    <?php endif; ?>
    <?php if ($lowerleft || $lowerright) : ?>
      <div class="row sixth-row">
        <<?php print $lowerleft_wrapper; ?> class="col-sm-6 <?php print $lowerleft_classes; ?>">
          <?php print $lowerleft; ?>
        </<?php print $lowerleft_wrapper; ?>>
        <<?php  print $lowerright_wrapper; ?> class="col-sm-6 <?php print $lowerright_classes; ?>">
          <?php print $lowerright; ?>
        </<?php print $lowerright_wrapper; ?>>
      </div>
    <?php endif; ?>
    <?php if ($bottom) : ?>
      <div class="row seventh-row">
        <<?php print $bottom_wrapper; ?> class="col-sm-12 <?php print $bottom_classes; ?>">
          <?php print $bottom; ?>
        </<?php print $bottom_wrapper; ?>>
      </div>
    <?php endif; ?>
    <?php if ($bottomleft || $bottomright) : ?>
      <div class="row eighth-row">
        <<?php print $bottomleft_wrapper; ?> class="col-sm-6 <?php print $bottomleft_classes; ?>">
          <?php print $bottomleft; ?>
        </<?php print $bottomleft_wrapper; ?>>
        <<?php print $bottomright_wrapper; ?> class="col-sm-6 <?php print $bottomright_classes; ?>">
          <?php print $bottomright; ?>
        </<?php print $bottomright_wrapper; ?>>
      </div>
    <?php endif; ?>
    <?php if ($footer) : ?>
      <div class="row ninth-row">
        <<?php print $footer_wrapper; ?> class="col-sm-12 <?php print $footer_classes; ?>">
          <?php print $footer; ?>
        </<?php print $footer_wrapper; ?>>
      </div>
    <?php endif; ?>
  </div> <!-- / content-wrapper -->
</<?php print $layout_wrapper ?>>


<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
