<?php

/**
 * Implements hook_preprocess
 */
function bootstrap_rcpar_preprocess(&$vars, $hook) {
  switch ($hook) {
    case 'page':
      // Default node tabs navigation for Bootstrap theme 
      if (user_access('publish all content') && isset($vars['tabs']) && isset($vars['node'])) {
        $primary_tabs = $vars['tabs']['#primary'];
        $nid = $vars['node']->nid;
        $tabs = '<ul class="nav nav-tabs">';
        foreach ($primary_tabs as $tb) {
          $path = '/'.str_replace('%', $nid, $tb['#link']['path']);
          $tabs .= isset($tb['#active']) ?
            '<li class="active "><a href="' . $path . '">' . $tb['#link']['title'] . '</a></li>' :
            '<li><a href="' . $path . '">' . $tb['#link']['title'] . '</a></li>';
        }
        $tabs .= '</ul>';
        $vars['tabs'] = $tabs;

      }
    case 'html':
      // custom html and page template for courseware
      if (in_array(arg(0), array('study', 'study-cram'))) {
        drupal_add_js(path_to_theme() . '/js/slidepushmenus/classie.js', array('scope' => 'header'));
        drupal_add_css(path_to_theme() . '/js/slidepushmenus/component.css');
        $vars['theme_hook_suggestion'] = $hook . '__courseware';
      }
      if (drupal_match_path(current_path(), 'dashboard/my-courses*')) {
        drupal_add_js(path_to_theme() . '/js/jquery-stickytabs/jquery.stickytabs.js');
      }
  }
}
/**
 * @file
 * template.php
 */
function bootstrap_rcpar_preprocess_html(&$variables) {
  global $user;

  drupal_add_css(path_to_theme() . '/css/styles.css');

  //$variables['tracking_codes'] = '<!-- Optimizely --><script src="https://cdn.optimizely.com/js/10424003465.js"></script>';
  //minimize page flickering
  // Check to see if we should render the Google Optimize Page hiding snippet
  $variables['tracking_codes'] = rcpar_mods_get_google_code_snippet('google_optimize_script');

  // Google Ecommerce transaction tracking - add Google DataLayer tracking code.
  // Only should be sent on the checkout complete page
  if (arg(0)=='checkout' && arg(2)=='complete') {
    $variables['tracking_codes'] .= user_entitlements_set_google_tag();
  }

  //standard tag manager code
  $variables['tracking_codes'] .= "<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','" . variable_get('gtm_id') . "');</script>
<!-- End Google Tag Manager --> ";

  // Add metatag to verify site for pinterest
  $variables['tracking_codes'] .= "\r\n<meta name=\"p:domain_verify\" content=\"3f859be15b6068310ae22124437b6b3c\"/>";


  // Add script for Einstein Salesforce tracking pixel.
  $variables['tracking_codes'] .= "\r\n<script type=\"text/javascript\" src=\"https://100018043.collect.igodigital.com/collect.js\"> </script>";

  // Begin Google Analytics session control. If the GA start session control snippet
  // is configured for the page a new GA session will begin each time the page is loaded.
  // If the GA end session snippet is configured for the page the current GA session will end.
  // A typical use case for ending a session is a "Thank you" page for new lead forms.

  // Check if the page is configured to start a new google analytics session
  $ga_snipet = rcpar_mods_get_google_code_snippet('ga_session_start_script');

  // If the page is not configured to start a new GA session, it may be configured to end a session
  if (empty($ga_snipet)) {
    $ga_snipet = rcpar_mods_get_google_code_snippet('ga_session_end_script');
  }

  // If a GA session snippet is present, throw that puppy on the page
  if (!empty($ga_snipet)) {
    drupal_add_js($ga_snipet,  array('type'=>'inline', 'scope'=>'header', 'weight'=>20));
  }//<--end GA session control

  // add body class for the shows IPQ access
  if (ipq_common_access()) {
    $variables['classes_array'][] = 'ipq-user';
  } else {
    $variables['classes_array'][] = 'not-ipq-user';
  }
  // add body class to cart page if upsell block is displayed
  if (isset($variables['page']['content']['enroll_flow_enroll_flow_upsell_extras']) && $variables['page']['content']['enroll_flow_enroll_flow_upsell_extras']) {
    $variables['classes_array'][] = 'with-upsells';
  }

  if (arg(0)=='study') {
    $variables['classes_array'][] = 'section-dashboard';
  }
  // Hide IPQ Legacy link in hamburger menu for users that created their account after June 28, 2016
  $ipqdate = strtotime('June 28, 2016 00:00:00 PST');
  if ($user->created > $ipqdate) {
    $variables['classes_array'][] = 'new-account';
  }
  // Add JS files for Pinterest button on blog images
  if (arg(0) == 'node' && is_numeric(arg(1)) && !arg(2)) {
    // If this is true, this is a blog entry with an URL like blog/entry-name-here
    $node = menu_get_object();
    if ($node->type == 'blog') {
      drupal_add_js(drupal_get_path('module','rcpar_mods') . '/js/pinit.js');
      drupal_add_js(drupal_get_path('module','rcpar_mods') . '/js/pinit-positioning.js');
    }
  }
  else if (current_path() == 'blog') {
    // If this is true, the current page is the main blog page
    drupal_add_js(drupal_get_path('module','rcpar_mods') . '/js/pinit.js');
    drupal_add_js(drupal_get_path('module','rcpar_mods') . '/js/pinit-positioning.js');
  }

  // On the shopping cart page, add a body class if the cart is empty
  if (drupal_match_path(current_path(), 'cart')) {
    // Wrap in try/catch in the off chance that the there is an issue loading the class or the module has been disabled,
    // or some other thing which probably shouldn't happen
    $cart_empty = FALSE;
    try {
      $order = commerce_cart_order_load($GLOBALS['user']->uid);
      if ($order) {
        // Help alleviate dependency on the module that provides this class (rcpar_checkout) by testing for its existence.
        if (!class_exists('RCPARCommerceOrder')) {
          throw new Exception('RCPARCommerceOrder class does not exist');
        }
        $order_obj = new RCPARCommerceOrder($order);
        if ($order_obj->getItemCount() == 0) {
          $cart_empty = TRUE;
        }
      }
      else {
        // if order is not set, cart will be empty
        $cart_empty = TRUE;
      }
      if ($cart_empty) {
        $variables['classes_array'][] = 'empty-cart';
      }

    }
    catch (Exception $exc) {
      watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . ' MESSAGE ' . $exc->getMessage());
    }
  }

  $variables['messages'] = theme('status_messages');
  if (isset($variables['messages']) && !empty($variables['messages'])) {

    $variables['classes_array'][] = 'with-messages';
  }
  $variables['field_bookmark_title'] = '';
  $node = menu_get_object();
  if (isset($variables['page']['hello_bar'])) {
    $variables['classes_array'][] = 'hello-bar';
  }
  if ($node && isset($node->nid)) {
    $node = node_load($node->nid);
    node_build_content($node);
    // if there is a bookmark title field, use it for the bookmark title variable
    if (empty($node->field_bookmark_title['und']['0']['safe_value'])) {
      $variables['field_bookmark_title'] = '';
    } else {
      $variables['field_bookmark_title'] = $node->field_bookmark_title['und']['0']['safe_value'];
    }
    // except for career postings, those get a static bookmark title
    if ($node->type == 'careers') {
      $variables['field_bookmark_title'] = 'Join Our Team | Roger CPA Review';
    }

    if ($node->type == 'helpcenter_question') {
      // drupal_add_js($theme_path . '/js/slidepushmenus/modernizr.custom.js', array('scope' => 'header'));
      drupal_add_js(path_to_theme() . '/js/slidepushmenus/classie.js', array('scope' => 'header'));
      drupal_add_css(path_to_theme() . '/js/slidepushmenus/component.css');
    }
    // add css to Johnstone and Wahlen chapter and login pages
    if ($node->nid == '18030' || $node->nid == '18031' || $node->nid == '18033' || $node->nid == '18035') {
      drupal_add_css(path_to_theme() . '/css/johnstone.css');
    }
  }
  // Add  blog css
  if (arg(0)=='blog' || $node->type == 'blog') {
    drupal_add_css(path_to_theme() . '/css/blog.css');
  }

  // These pages get static bookmark titles
  if (arg(0)=='blog' && arg(1)=='search') {
    $variables['field_bookmark_title'] = 'Search | Roger CPA Review';
  }
  if (arg(0) == 'ipq') {
    $variables['field_bookmark_title'] = 'Interactive Practice Questions | Roger CPA Review';
  }
  if (arg(0) == 'ipq' && arg(1) == 'smartpath') {
    $variables['field_bookmark_title'] = 'SmartPath&trade; | Roger CPA Review';
  }
  if (arg(0) == 'why-roger' && arg(1) == 'customer-reviews') {
    $variables['field_bookmark_title'] = 'Roger CPA Review Customer Reviews';
  }
  if (arg(0)=='search' && arg(1)=='site' && arg(2) =='') {
    $variables['field_bookmark_title'] = 'Search | Roger CPA Review';
  }
  if (arg(0)=='search' && arg(1)=='site' && arg(2) != '') {
    $variables['field_bookmark_title'] = 'Search Results | Roger CPA Review';
  }
  // Add unique classes for each page and website section
  $path = drupal_get_path_alias($_GET['q']);
  $pathclass = drupal_html_class($path);
  list($section, ) = explode('/', $path, 2);
  $variables['classes_array'][] = 'page-' . $pathclass .' ';
  $variables['classes_array'][] = 'section-' . $section .' ';
  if (rcpar_partners_hide_prices() || rcpar_partners_hide_prices_after_purchase()) {
    $variables['classes_array'][] = 'hide-prices';
  }
  if (rcpar_partners_get_direct_bill()) {
    $variables['classes_array'][] = 'direct-bill';
  }
  if (rcpar_partners_get_free_trial()) {
    $variables['classes_array'][] = 'free-trial';
  }
  if (arg(0)=="user" && is_numeric(arg(1)) || arg(0)=="user" && arg(1)=="reset" || arg(0)=='homework-help-center' || arg(2)=='helpcenter-question' || $section == "homework-help-center" || ($node && isset($node->nid) && $node->type == 'helpcenter_question') || arg(0)=='exam_scores' || (arg(0)=="unlimited-access" && arg(1)=="activation")) {
    $variables['classes_array'][] = 'user-profile-page section-dashboard';
  }
  if ($section == "ipq" || $section == "act") {
    $variables['classes_array'][] = 'section-dashboard';
  }
  // Add body class if uworld saml login is enabled
  $saml_login_enabled = variable_get('enable_uworld_configurations', FALSE);
  if ($saml_login_enabled) {
    $variables['classes_array'][] = 'enabled';
  }
  if ($section == 'questions-answers' || $section == 'qa' || arg(0) == 'search' && arg(1) == 'questions-answers') {
    drupal_add_css(path_to_theme() . '/css/question-answer.css');
  }
  if ($section == 'news') {
    drupal_add_css(path_to_theme() . '/css/news.css');
  }
  if ($section == 'cpa-insights') {
    drupal_add_css(path_to_theme() . '/css/cpa-insights.css');
  }
  if ($path == 'cpa-exam' || $path == 'free-resources/2017-exam-roadmap-success' || $path == 'free-resources/roadmap-to-success' || $path == 'learning-center/webcast' || $path == 'learning-center/webcast/thank-you' || $path == 'watt-sells-award-winners' || $path == 'international' || $path == 'smartpath-case-studies' || $path == 'smartpath/smartpath-case-studies' || $path == 'cpa-courses/mobile-app' || $path == 'cpa-exam/tax-reform-resources') {
    drupal_add_css(path_to_theme() . '/css/forms.css');
  }
  if (drupal_is_front_page()) {
    drupal_add_css(path_to_theme() . '/css/homepage.css');
  }
  // add css file for basic pages
  if (isset($node) && $node->type == 'page' || $node->type == 'landing_pages') {
    drupal_add_css(path_to_theme() . '/css/basic-page.css', array('group' => CSS_THEME));
  }
  // add team css file for team pages
  if (isset($node) && $node->type == 'team' || $path == 'about/team') {
    drupal_add_css(path_to_theme() . '/css/team.css', array('group' => CSS_THEME));
  }
  // add landing pages css file for landing pages
  if (isset($node) && $node->type == 'landing_pages' ||  $node->type == 'drag_and_drop_basic') {
    drupal_add_css(path_to_theme() . '/css/landing-pages.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme() . '/css/forms.css', array('group' => CSS_THEME));
  }
  /* dashboard pages css */
  if ($section == 'dashboard' || arg(0)=='exam_scores' || (arg(0)=="unlimited-access" && arg(1)=="activation")) {
    drupal_add_css(path_to_theme() . '/css/dashboard/dashboard.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme() . '/css/dashboard/my-courses.css', array('group' => CSS_THEME));
  }
  switch ($path) {
    case 'dashboard/study-planners':
      drupal_add_css(path_to_theme() . '/css/dashboard/study-planners.css', array('group' => CSS_THEME));
      break;
    case 'dashboard/monitoring-center':
    case 'act/reports':
      drupal_add_css(path_to_theme() . '/css/dashboard/my-courses.css', array('group' => CSS_THEME));
      drupal_add_css(path_to_theme() . '/css/dashboard/monitoring-center.css', array('group' => CSS_THEME));
      break;
    case 'dashboard/support':
      drupal_add_css(path_to_theme() . '/css/dashboard/support.css', array('group' => CSS_THEME));
      break;
  }
  // add css for user profile order history page
  if (arg(0) == 'user' && is_numeric(arg(1)) && arg(2)=='orders') {
    drupal_add_css(path_to_theme() . '/css/dashboard/order-history.css', array('group' => CSS_THEME));
  }
  // add css for user profile page
  if (arg(0) == 'user' && is_numeric(arg(1)) && user_is_logged_in()) {
    drupal_add_css(path_to_theme() . '/css/dashboard/dashboard.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme() . '/css/dashboard/user.css', array('group' => CSS_THEME));
  }
  // add js for user profile edit and reset password page page
  if (arg(0) == 'user' && arg(1) == 'reset' || arg(2)=='edit') {
    drupal_add_js(path_to_theme() . '/js/user-profile.js', array('scope' => 'header'));
  }
  // add mobile css
  drupal_add_css(path_to_theme() . '/css/mobile.css', array('group' => CSS_THEME));
  drupal_add_css(path_to_theme() . '/css/mobile-nav.css', array('group' => CSS_THEME));
  // add cpa-exam/results page css
  if ($path == 'cpa-exam/results') {
    drupal_add_css(path_to_theme() . '/css/exam-results.css', array('group' => CSS_THEME));
  }
  // case studies css
  if (strpos($path, 'smartpath') !== false) {
    drupal_add_css(path_to_theme() . '/css/case-studies.css');
    drupal_add_library('flexslider', 'flexslider');
    drupal_add_js(path_to_theme() . '/js/case-studies.js');
  }
  // add learning center js file to learning center pages

  if (strpos($path, 'lc/accounting-dictionary') !== false
    || $path=='learning-center'
    || strpos($path, 'lc/cpa-exam-videos') !== false) {
    drupal_add_js(path_to_theme() . '/js/learning-center.js', array('scope' => 'header'));
  }
  // Add css and js for package product page layout.
  if ($path =='cpa-courses/select'  || $path == 'cpa-courses/premier' || $path == 'cpa-courses/elite') {
    $variables['classes_array'][] = 'page-cpa-courses-variation';
    drupal_add_css(path_to_theme() . '/css/package-products.css');
    drupal_add_library('flexslider', 'flexslider');
    drupal_add_js(path_to_theme() . '/js/package-product.js');
  }

  // add bookmark js file to login page
  if (drupal_match_path(current_path(), 'user/login') || (drupal_match_path(current_path(), 'user') && !user_is_logged_in())) {
    drupal_add_js(path_to_theme() . '/js/bookmark.js', array('scope' => 'header'));
  }
  // change page title for Dictionary term pages
  if (arg(0)=='taxonomy' && arg(1)=='term' && is_numeric(arg(2))) {
    $tid = arg(2);
    $term = taxonomy_term_load($tid);
    if ($term->vocabulary_machine_name == 'dictionary') {
      $variables['head_title'] = $term->name . ' Accounting Term | Roger CPA Review';
    }
  }

  if ($variables['user']) {
    foreach($variables['user']->roles as $key => $role) {
      $role_class = 'role-' . str_replace(' ', '-', $role);
      $variables['classes_array'][] = $role_class;
    }
    if (in_array('student enrolled', $variables['user']->roles)) {
      $variables['classes_array'][] = 'student-enrolled';
    } else {
      $variables['classes_array'][] = 'student-non-enrolled';
    }
  }

  $theme_path = path_to_theme();
  drupal_add_js($theme_path . '/js/retina.js', array('scope' => 'footer'));
  //drupal_add_js($theme_path . '/js/breakpoints.js', array('scope' => 'header'));
  //print_r($variables);

  if (arg(0) == 'node' && arg(1)=='98' || arg(0) == 'node' && arg(1)=='5094') {
    drupal_add_js($theme_path . '/js/map/map-interact.js', array('scope' => 'header'));
  }
  drupal_add_js($theme_path . '/js/breakpoints.js', array('scope' => 'header'));
  // add bootstrap accessibility plugin
  drupal_add_js($theme_path . '/js/bootstrap-accessibility.js', array('scope' => 'header'));
  drupal_add_css($theme_path . '/css/bootstrap-accessibility.css', array('scope' => 'header'));

  drupal_add_css('https://use.typekit.net/njc3ufl.css', array('type' => 'external'));


  $filepath = path_to_theme() . '/fonts/font-awesome/css/font-awesome.min.css';
  drupal_add_css($filepath, array(
    'group' => CSS_THEME,
  ));
  drupal_add_css($theme_path . '/js/bootstrap-select/dist/css/bootstrap-select.css');
  //drupal_add_css('http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css');
  drupal_add_js($theme_path . '/js/bootstrap-select/dist/js/bootstrap-select.js', array('scope' => 'header'));
  //drupal_add_js('http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js', array('scope' => 'header'));

  if ($section=="dashboard"
    || (arg(0)=="user" && user_is_logged_in())
    || (arg(0)=="user" && arg(1)=="reset")
    || arg(0)=='homework-help-center'
    || arg(2)=='helpcenter-question'
    || $section == "homework-help-center"
    || $section == "ipq"
    || $section == RCPAR_CP_URL
    || arg(0) == 'exam_scores'
    || (arg(0)=="unlimited-access" && arg(1)=="activation")
  ) {
    // drupal_add_js($theme_path . '/js/slidepushmenus/modernizr.custom.js', array('scope' => 'header'));
    drupal_add_js($theme_path . '/js/slidepushmenus/classie.js', array('scope' => 'header'));
    drupal_add_css($theme_path . '/js/slidepushmenus/component.css');
    // these pages also get different html template
    $variables['theme_hook_suggestion'] = 'html__dashboard';
  }
  // add css for homework help cetner
  if (arg(0)=='homework-help-center'
    || arg(2)=='helpcenter-question'
    || $section == "homework-help-center") {
    drupal_add_css(path_to_theme() . '/css/dashboard/dashboard.css', array('group' => CSS_THEME));
  }
  // add css for award winners

  if ($section=="watt-sells-award-winners") {
    drupal_add_css(path_to_theme() . '/css/award-winners.css', array('group' => CSS_THEME));
  }
  // add css for act professors page
  if ($section=="act-now" || $path=="act-now/thank-you") {
    drupal_add_css(path_to_theme() . '/css/act-professors.css', array('group' => CSS_THEME));
  }
  // add css for alternative products page
  if ($path == 'cpa-courses' || $path == 'cpa-courses-v2' || $path == 'courses-and-products-0') {
    drupal_add_css(path_to_theme() . '/css/cpa-courses.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme() . '/css/animate.css');
  }

  if (isset($node->field_hero_banner["und"][0]["uri"])) {
    $variables['classes_array'][] = 'with-hero-banner';
  }
  if ($node->type=='page') {
    $variables['classes_array'][] = 'basic-page';
    if ($node->field_hero_image["und"][0]["value"] == 1) {
      $variables['classes_array'][] = 'with-hero-banner';
    }
  }
}

/**
 * A function to determine if the current path should be excluded.
 * @param $paths_to_exclude
 * @return bool
 */
function bootstrap_rcpar_exclude_path($paths_to_exclude) {
  if (drupal_match_path(request_path(),implode("\n", $paths_to_exclude))) {
    return true;
  }
  return false;
}


/**
 * Implements hook_preprocess_page().
 * @param $vars
 */
function bootstrap_rcpar_preprocess_page(&$vars) {
  // Tracking code that belongs in the body of the page
  $vars['page_tracking_codes'] = '';
  // Einstein tracking codes
  $excluded_einstein_paths = array('dashboard*', 'user*', 'ipq*', 'act*', 'study*', 'exam_scores*');
  if (!bootstrap_rcpar_exclude_path($excluded_einstein_paths)) {
    global $base_url;
    global $base_path;
    $vars['page_tracking_codes'] .= "\n" . '<!--Einstein Salesforce pixel-->';
    $vars['page_tracking_codes'] .= "\n" . '<script type="text/javascript">';
    $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["setOrgId", "100018043"]);';
    // product page views - product display nodes
    if ($vars['node']->type == 'product_display' && drupal_get_path_alias() != 'cpa-review-course/process-replacement-products') {
      // track page view by sku
      // Assign SKUS
      switch (drupal_get_path_alias()) {
        case 'cpa-courses/cram':
          $item = 'FULL-CRAM';
          break;
        case 'cpa-courses/flashcards':
          $item = 'FULL-EFC';
          break;
        case 'cpa-courses/audio':
          $item = 'FULL-AUD';
          break;
        case 'cpa-courses/mobile-offline-access':
          $item = 'FULL-MOB-OFF';
          break;
        case 'cpa-courses/6-month-extension':
          $item = 'FULL-6MEXT';
          break;
        case 'cpa-courses/textbooks':
          $item = 'FULL-BK-2019';
          break;
        default:
          $item = $vars['node']->title;
      }
      $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["trackPageView", { "item" : "' . $item  . '" }]);';

      // load default product
      if (!empty($vars['node']->field_product)) {
        $default_product = commerce_product_load($vars['node']->field_product['und'][0]['product_id']);
        $wrapper = entity_metadata_wrapper('commerce_product', $default_product);
        // product catalog streaming update - loop through all products on page
        foreach ($vars['node']->field_product['und'] as $index => $product_id) {
          $product = commerce_product_load($product_id['product_id']);
          $wrapper = entity_metadata_wrapper('commerce_product', $product);
          $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["updateItem",';
          $vars['page_tracking_codes'] .=   "\n" . '{';
          $vars['page_tracking_codes'] .=    "\n" . '"item_type": "product",';
          $vars['page_tracking_codes'] .=    "\n" . '"item": "' . $wrapper->sku->value() . '",';
          $vars['page_tracking_codes'] .=    "\n" . '"name": "' .  $wrapper->label() . '",';
          $vars['page_tracking_codes'] .=    "\n" . '"url": "' . $base_url . $base_path . drupal_get_path_alias() . '",';
          $vars['page_tracking_codes'] .=    "\n" . '"unique_id": "' . $wrapper->sku->value() . '",';
          $vars['page_tracking_codes'] .=    "\n" . '"available": "Y"';
          $vars['page_tracking_codes'] .=  "\n" . '}';
          $vars['page_tracking_codes'] .= "\n" . ']);' ."\n";
        }
      }
    }
    // product page views - main course package products (they use drag and drop page builder node type)
    elseif (
      drupal_match_path(drupal_get_path_alias(current_path()), 'cpa-courses/select')
      || drupal_match_path(drupal_get_path_alias(current_path()), 'cpa-courses/elite')
      || drupal_match_path(drupal_get_path_alias(current_path()), 'cpa-courses/premier')
    ) {
      // Assign SKUS
      switch (drupal_get_path_alias()) {
        case 'cpa-courses/select':
          $sku = 'FULL';
          break;
        case 'cpa-courses/elite':
          $sku = 'FULL-ELITE';
          break;
        case 'cpa-courses/premier':
          $sku = 'FULL-PREM';
          break;
      }
      // Select is a special case
      if ($sku == 'FULL') {
        $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["trackPageView", { "item" : "' . $sku . '" }]);';
        // product catalog streaming update
        $rcb = new RCPARCommerceBundle($sku);
        if ($rcb->isLoaded()) {
          foreach ($rcb->getProducts() as $product_id => $product_info) {
            $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["updateItem",';
            $vars['page_tracking_codes'] .=   "\n" . '{';
            $vars['page_tracking_codes'] .=    "\n" . '"item_type": "product",';
            $vars['page_tracking_codes'] .=    "\n" . '"item": "' . $product_info['sku'] . '",';
            $vars['page_tracking_codes'] .=    "\n" . '"name": "'. $product_info['title'] .'",';
            $vars['page_tracking_codes'] .=    "\n" . '"url": "' . $base_url . $base_path . drupal_get_path_alias() . '",';
            $vars['page_tracking_codes'] .=    "\n" . '"unique_id": "' . $product_info['sku'] . '",';
            $vars['page_tracking_codes'] .=    "\n" . '"available": "Y"';
            $vars['page_tracking_codes'] .=  "\n" . '}';
            $vars['page_tracking_codes'] .= "\n" . ']);' ."\n";
          }
        }
      }
      // premier and elite
      else {
        $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["trackPageView", { "item" : "' . $sku . '" }]);';
        $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["updateItem",';
        // product catalog streaming update
        $vars['page_tracking_codes'] .=   "\n" . '{';
        $vars['page_tracking_codes'] .=    "\n" . '"item_type": "product",';
        $vars['page_tracking_codes'] .=    "\n" . '"item": "' . $sku . '",';
        $vars['page_tracking_codes'] .=    "\n" . '"name": "'. $vars['node']->title .'",';
        $vars['page_tracking_codes'] .=    "\n" . '"url": "' . $base_url . $base_path . drupal_get_path_alias() . '",';
        $vars['page_tracking_codes'] .=    "\n" . '"unique_id": "' . $sku . '",';
        $vars['page_tracking_codes'] .=    "\n" . '"available": "Y"';
        $vars['page_tracking_codes'] .=  "\n" . '}';
        $vars['page_tracking_codes'] .= "\n" . ']);' ."\n";
      }
    }
    // non-product page views
    else {
      $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["trackPageView"]);';
    }
    // checkout/order tracking
    if (drupal_match_path(current_path(), "checkout/*/complete")) {
      $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["trackConversion", { "cart": [';
      $order = commerce_order_load(arg(1));
      $entity_wrapper = entity_metadata_wrapper('commerce_order', $order);
      $shipping_cost = '';
      // loop through line items in the order and add tracking codes
      foreach ($entity_wrapper->commerce_line_items as $line_item_wrapper) {
        try {
          // products
          if ($line_item_wrapper->value()->type == 'product') {
            $product = commerce_product_load($line_item_wrapper->value()->commerce_product['und'][0]['product_id']);
            $vars['page_tracking_codes'] .= "\n" . '{"item" : "' . $product->title . '",';
            $vars['page_tracking_codes'] .= "\n" . '"quantity":  "' . $line_item_wrapper->value()->quantity . '" ,';
            $vars['page_tracking_codes'] .= "\n" . '"price" : "' . abs(commerce_currency_amount_to_decimal($line_item_wrapper->value()->commerce_unit_price['und'][0]['amount'], 'USD')) . '" ,';
            $vars['page_tracking_codes'] .= "\n" . '"unique_id" : "' . $product->sku . '" } ,';
          }
          // shipping
          elseif ($line_item_wrapper->value()->type == 'shipping') {
            $shipping_cost = $line_item_wrapper->value()->commerce_total['und'][0]['amount'];
          }
          // discount
          elseif ($line_item_wrapper->value()->type == 'commerce_coupon') {
            $vars['page_tracking_codes'] .= "\n" . '"discount" : "' . abs(commerce_currency_amount_to_decimal($line_item_wrapper->value()->commerce_unit_price['und'][0]['amount'], 'USD')) . '",';
          }
        }
        catch (EntityMetadataWrapperException $ex) {
        }
      }
      // order id
      $vars['page_tracking_codes'] .= "\n" . '"order_number" : "' . $order->order_id. '",';
      // shipping
      $vars['page_tracking_codes'] .= "\n" . '"shipping" : "' . commerce_currency_amount_to_decimal($shipping_cost, 'USD') . '",';
      $vars['page_tracking_codes'] .= "\n" . '}]);';
    }
    // search pages tracking
    if (strpos(current_path(),'search') !== false) {
      $search_term = '';
      $search_type = '';
      $cond = arg(0);
      switch ($cond) {
        case 'search':
          $search_term = arg(2);
          $search_type = 'site';
          break;
        case 'blog':
          $search_term = arg(2);
          $search_type = 'blog';
          break;
        case 'customer-care':
          $search_term = arg(3);
          if ($_REQUEST['cat'] != '') {
            $search_term = $_REQUEST['cat'];
          }
          $search_type = 'customer care';
          break;
        case 'homework-help-center':
          $search_term = arg(2);
          $search_type = 'HHC';
          break;
      }
      $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["trackPageView", { "search" : "' . $search_term .'" }]);';
      $vars['page_tracking_codes'] .= "\n" . '_etmc.push(["trackPageView", { "category" : "' . $search_type . '" }]);';
    }
    $vars['page_tracking_codes'] .= "\n" . '</script>';
    $vars['page_tracking_codes'] .= "\n" . '<!--End Einstein Salesforce pixel-->' . "\n";
  }

  // Marketing cloud Campaign Tracker tracking codes
  $vars['page_tracking_codes'] .= "\n<!-- Marketing cloud Campaign Tracker-->\n";
  $vars['page_tracking_codes'] .= "<script>";
  $vars['page_tracking_codes'] .= "\nwindow._cloudAmp = window._cloudAmp || {};";
  $vars['page_tracking_codes'] .= "\n_cloudAmp.forms = [];";
  $vars['page_tracking_codes'] .= "\n(function () {";
  $vars['page_tracking_codes'] .= "\n  var scripts = document.getElementsByTagName('script'),";
  $vars['page_tracking_codes'] .= "\n     sLen = scripts.length,";
  $vars['page_tracking_codes'] .= "\n     ca_script = document.createElement('script'),";
  $vars['page_tracking_codes'] .= "\n     head = document.getElementsByTagName('head'),";
  $vars['page_tracking_codes'] .= "\n     protocol = document.location.protocol,";
  $vars['page_tracking_codes'] .= "\n     httpsDomain = '1d5ef9e9369608f625a8-878b10192d4a956595449977ade9187d.ssl.cf2.rackcdn.com',";
  $vars['page_tracking_codes'] .= "\n     httpDomain = 'trk.cloudamp.net',";
  $vars['page_tracking_codes'] .= "\n     filename = 'ctk.js',";
  $vars['page_tracking_codes'] .= "\n     srcDomain = protocol === 'http:' ? httpDomain : httpsDomain;";
  $vars['page_tracking_codes'] .= "\n ca_script.type = 'text/javascript';";
  $vars['page_tracking_codes'] .= "\n ca_script.async = true;";
  $vars['page_tracking_codes'] .= "\n ca_script.src = protocol + '//' + srcDomain + '/' + filename;";
  $vars['page_tracking_codes'] .= "\n head[0].appendChild(ca_script);";
  $vars['page_tracking_codes'] .= "\n	})();";
  $vars['page_tracking_codes'] .= "\n</script>";
  $vars['page_tracking_codes'] .= "\n <!--End Marketing cloud Campaign Tracker-->\n";

  // Google tag manager tracking codes
  $vars['page_tracking_codes'] .= "\n" . '<!-- Google Tag Manager (noscript) -->';
  $vars['page_tracking_codes'] .= "\n" . '<noscript>';
  $vars['page_tracking_codes'] .= "\n" . '<iframe src="https://www.googletagmanager.com/ns.html?id=<?php print variable_get(\'gtm_id\'); ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe>';
  $vars['page_tracking_codes'] .= "\n" . '</noscript>';
  $vars['page_tracking_codes'] .= "\n" . '<!-- End Google Tag Manager (noscript) -->' . "\n";

  // Link connect tracking codes
  global $conf;
  $disable_LinkConnector = isset($conf['disable_LinkConnector']) ? $conf['disable_LinkConnector'] : FALSE;
  if (!$disable_LinkConnector) {
    $vars['page_tracking_codes'] .= "\n" . '<!--LinkConnector pixel-->';
    $vars['page_tracking_codes'] .= "\n" . '<script async defer type="text/javascript" src="//www.linkconnector.com/uts_lp.php?cgid=900134"></script>';
    $vars['page_tracking_codes'] .= "\n" . '<!--END LinkConnector pixel-->' . "\n";
  }
  else {
    $vars['page_tracking_codes'] .= "\n" . '<!-- LinkConnector pixel output will appear here on production. -->';
  }

  // Dictionary term pages that have no content display
  // a paragraph of content with no unique class or id
  // that needs to be hidden. This will wrap it in a div
  // we can target with CSS.
  if(isset($vars['page']['content']['system_main']['no_content'])) {
    $vars['page']['content']['system_main']['no_content']['#prefix'] = '<div class="no-content-wraper">';
    $vars['page']['content']['system_main']['no_content']['#suffix'] = '</div>';
  }
  // Page builder pages that use a template that contain sidebar snippets are not getting their snippet css files loaded.
  // This will add the correct css files to the page.
  if (isset($vars['node']->body)) {
    $body_value = $vars['node']->body['und'][0]['value'];
  }
  if (isset($vars['node']->cb_body)) {
    $body_value = $vars['node']->cb_body['und'][0]['value'];
  }
  if ($body_value && strstr($body_value,'az-element') !== FALSE) {
    // Create a new DomDocument object
    $dom = new DomDocument;
    // Load the HTML
    $dom->loadHTML($body_value);
    // Create a new XPath object
    $xpath = new DomXPath($dom);
    // Query all <div> nodes containing specified class name
    $nodes = $xpath->query("//div[@class='az-element']");
    // Create an array of css files to add
    $css_files_to_add = array();
    foreach ($nodes as $i => $node) {
      // Get the stylesheet link tag in each node
      $links = $node->getElementsByTagName('link');
      foreach($links as $link) {
        // Get the href value of the link tag and add to array.
        $css_link = $link->getAttribute('href');
        $css_files_to_add[] = $css_link;
      }
    }
    // Filter the array values to get rid of repeats
    $css_files_to_add = array_unique($css_files_to_add);
    // Add the css files to the page.
    foreach ($css_files_to_add as $index => $file) {
      $file_location = str_replace('/sites/all/themes/bootstrap_rcpar','',$file);
      drupal_add_css(path_to_theme() . $file_location, array('group' => CSS_THEME));
    }
  }

  if (isset($vars['node']->cb_body) && strstr($vars['node']->cb_body['und'][0]['value'],'contains-flexslider') !== FALSE) {
    drupal_add_library('flexslider', 'flexslider');
    drupal_add_js(path_to_theme() . '/js/package-product.js');
  }

  // Add unique classes for each page and website section
  $path = drupal_get_path_alias($_GET['q']);
  $pathclass = drupal_html_class($path);
  list($section, ) = explode('/', $path, 2);
  $nid = arg(1);
  if (arg(0) == 'node' && is_integer($nid)) {
    $node = node_load($nid);
    switch ($node->type) {
      case 'blog':
        $vars['title'] = t('Blog');
        break;
    }
  }

  if (isset($vars['node']->type)) {
    // Add page builder css

    if ($vars['node']->type == 'cb_drag_drop_page' || $vars['node']->type == 'landing_pages' || $vars['node']->type == 'blog' || $vars['node']->type == 'drag_and_drop_basic') {
      drupal_add_css(path_to_theme() . '/css/page-builder.css');
      drupal_add_css(path_to_theme() . '/css/forms.css', array('group' => CSS_THEME));
      drupal_add_css(drupal_get_path('module', 'rcpar_mods') . '/css/token-custom-form.css');
    }

    // We don't want to apply this on taxonomy or view pages
    // Splice (2) is based on existing default suggestions. Change it if you need to.
    array_splice($vars['theme_hook_suggestions'], -1, 0, 'page__' . $vars['node']->type);
    // Get the url_alias and make each item part of an array
    $url_alias = drupal_get_path_alias($_GET['q']);
    $split_url = explode('/', $url_alias);

    // Add the full path template pages
    // Insert 2nd to last to allow page--node--[nid] to be last
    $cumulative_path = '';
    foreach ($split_url as $path) {
      $cumulative_path .= '__' . $path;
      $path_name = 'page' . $cumulative_path;
      array_splice($vars['theme_hook_suggestions'], -1, 0, str_replace('-', '_', $path_name));
    }
    // This does just the page name on its own & is considered more specific than the longest path
    // (because sometimes those get too long)
    // Also we don't want to do this if there were no paths on the URL
    // Again, add 2nd to last to preserve page--node--[nid] if we do add it in
    if (count($split_url) > 1) {
      $page_name = end($split_url);
      array_splice($vars['theme_hook_suggestions'], -1, 0, 'page__' . str_replace('-', '_', $page_name));
    }

    // OVERRIDE BREADCRUMB FOR NEWS PAGES
    if ($section == 'news') {
      // Build Breadcrumbs
      $n_breadcrumb = array();
      $n_breadcrumb[] = l('Home', '');
      $node = node_load(arg(1));
      if ($node->title == 'News') {
        $n_breadcrumb[] = 'News';
      } else {
        $n_breadcrumb[] = l('News', 'news');
      }
      if ($node->title == 'News & Press Contact') {
        $n_breadcrumb[] = 'Contact';
      }
      if ($node->title == 'News & Press') {
        $n_breadcrumb[] = 'Contact';
      }
      // Set Breadcrumbs
      drupal_set_breadcrumb($n_breadcrumb);
    }

    // OVERRIDE BREADCRUMB FOR SMARTPATH MARKETING PAGES
    if ($section == 'smartpath') {
      // Build Breadcrumbs
      $sp_breadcrumb = array();
      $sp_breadcrumb[] = l('Home', '');
      // If on a smartpath sub page, change smartpath breadcrumb
      if ($path != $section) {
        $sp_breadcrumb[] = l('SmartPath Predictive Technology™', 'smartpath');
      }
      // Set Breadcrumbs
      drupal_set_breadcrumb($sp_breadcrumb);
    }

    // change page title for hhc posts
    if ($vars['node']->type == 'helpcenter_question') {
      $vars['title']= 'Homework Help Center';
    }
  }

  if ( (arg(0)=="user" && user_is_logged_in()) || (arg(0)=="user" && arg(2)=="edit") || (arg(0)=="user" && arg(1)=="reset") ||  arg(0)=='exam_scores' || (arg(0)=="unlimited-access" && arg(1)=="activation")) {
    $vars['theme_hook_suggestions'][] = 'page__dashboard';
  }
  if (arg(0)=='homework-help-center' || arg(2)=='helpcenter-question' || $section == "homework-help-center") {
    $vars['theme_hook_suggestions'][] = 'page__help_center';
  }
  // these pages get static page titles
  if (arg(0)=="user" && arg(1)=="login") {
    $vars['title'] = t('Login or Create an Account');
  }
  if (arg(0)=="user" && arg(1)=="password") {
    $vars['title'] = t('Request Password');
  }
  if (arg(0)=="user" && arg(1)=="register") {
    $vars['title'] = t('Create an Account or Login');
  }
  if (arg(0)=="discount-verification") {
    $vars['title'] = t('Verify Your Discount');
  }
  if (arg(0)=='search' && arg(1)=='site' && arg(2)=='') {
    $vars['title']= 'Search';
  }
  if (arg(0)=='search' && arg(1)=='site' && arg(2)!='') {
    $vars['title']= 'Search Results';
  }
  if (arg(0)=='homework-help-center' && arg(1)=='search' || arg(0)=='homework-help-center' && arg(1)=='tags') {
    $vars['title']= 'Homework Help Center';
  }
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $term = taxonomy_term_load(arg(2));
    $vid = $term->vid;
    if ($vid == '1') {
      $vars['title']= 'Homework Help Center';
    }
  }
  if (arg(0)=='partner' && arg(1)=='validation-emails' && is_numeric(arg(2))) {
    $vars['theme_hook_suggestions'][] = 'page__partneremails';
  }

  // OVERRIDE BREADCRUMB FOR LEARNING CENTER SUB PAGES
  if ($section == 'lc') {
    // Build Breadcrumbs
    $lc_breadcrumb = array();
    $lc_breadcrumb[] = l('Home', '');
    $lc_breadcrumb[] = l('Learning Center', 'learning-center');
    if (strpos($path, 'accounting-dictionary/')) {
      $lc_breadcrumb[] = l('Accounting Dictionary', 'lc/accounting-dictionary');
    }
    if (isset($vars['node']->type) && $vars['node']->type=='cpa_exam_learning_center') {
      $lc_breadcrumb[] = l('CPA Exam Videos', 'lc/cpa-exam-videos');
    }
    // Set Breadcrumbs
    drupal_set_breadcrumb($lc_breadcrumb);
  }


  // OVERRIDE BREADCRUMB FOR Question and Answer PAGES
  if ($section == 'qa') {
    // Build Breadcrumbs
    $qa_breadcrumb = array();
    $qa_breadcrumb[] = l('Home', '');
    $qa_breadcrumb[] = l('Questions & Answers', 'questions-answers');
    // Set Breadcrumbs
    drupal_set_breadcrumb($qa_breadcrumb);
  }
  if (arg(0)=='questions-answers' && arg(1)=='search') {
    // Build Breadcrumbs
    $qa_breadcrumb = array();
    $qa_breadcrumb[] = l('Home', '');
    $qa_breadcrumb[] = l('Questions & Answers', 'questions-answers');
    $qa_breadcrumb[] = 'Search';
    // Set Breadcrumbs
    drupal_set_breadcrumb($qa_breadcrumb);
  }

  // OVERRIDE BREADCRUMB FOR Blog Search Results
  if (arg(0)=='blog' && arg(1)=='search') {
    // Build Breadcrumbs
    $sb_breadcrumb = array();
    $sb_breadcrumb[] = l('Home', '');
    $sb_breadcrumb[] = l('Blog', 'blog');
    if (arg(2)) {
      $sb_breadcrumb[] = l('Search', 'blog/search');
      $sb_breadcrumb[] = 'Search results';
    }
    else {
      $sb_breadcrumb[] = 'Search';
    }
    // Set Breadcrumbs
    drupal_set_breadcrumb($sb_breadcrumb);
  }

  // OVERRIDE BREADCRUMB FOR Search Results
  if (arg(0)=='search' && arg(1)=='site') {
    // Build Breadcrumbs
    $sb_breadcrumb = array();
    $sb_breadcrumb[] = l('Home', '');
    if (arg(2)) {
      $sb_breadcrumb[] = l('Search', 'search');
      $sb_breadcrumb[] = 'Search results';
    }
    else {
      $sb_breadcrumb[] = 'Search';
    }
    // Set Breadcrumbs
    drupal_set_breadcrumb($sb_breadcrumb);
  }
  // OVERRIDE BREADCRUMB FOR HHC
  if (arg(0)=='homework-help-center') {
    // Build Breadcrumbs
    $hhc_breadcrumb = array();
    $hhc_breadcrumb[] = l('My Dashboard', 'dashboard');
    if (arg(1)=='search') {
      if (arg(2)) {
        $hhc_breadcrumb[] = l('Homework Help Center', 'homework-help-center');
        $hhc_breadcrumb[] = 'Search';
      }
      else {
        $hhc_breadcrumb[] = l('Homework Help Center', 'homework-help-center');
        $hhc_breadcrumb[] = 'Search';
      }
    }
    if (arg(1)=='my-posts') {
      $hhc_breadcrumb[] = l('Homework Help Center', 'homework-help-center');
      $hhc_breadcrumb[] = 'My Posts';
    }
    if (arg(0)=='homework-help-center' && arg(1)=='tags') {
      $hhc_breadcrumb[] = l('Homework Help Center', 'homework-help-center');
      $hhc_breadcrumb[] = 'Tags';
    }
    // Set Breadcrumbs
    drupal_set_breadcrumb($hhc_breadcrumb);
  }
  // OVERRIDE BREADCRUMB for Dashboard pages
  if ($section == 'dashboard') {
    // Build Breadcrumbs
    $db_breadcrumb = array();
    $db_breadcrumb[] = l('My Dashboard', 'dashboard');
    if (strpos($path, 'my-courses') !== false) {
      $db_breadcrumb[] = 'Course Outline';
    }
    if (strpos($path, 'my-cram-courses') !== false) {
      $db_breadcrumb[] = 'Cram Course Outline';
    }
    // Set Breadcrumbs
    drupal_set_breadcrumb($db_breadcrumb);
  }
  global $user;
  if (!in_array('administrator', $user->roles) && variable_get('maintenance_mode') == 1 && arg(0) != 'user') {
    $vars['theme_hook_suggestions'][] = 'maintenance_page';
  }
}

function bootstrap_rcpar_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $list_attributes = $variables['attributes'];
  $arraycheck = array();
  if (!empty($variables['attributes']['class'])) {
    $arraycheck = $variables['attributes']['class'];
  }
  $checkout_progress = 0;
  if (in_array('commerce-checkout-progress', $arraycheck)) {
    $checkout_progress = 1;
  }
  if (in_array('breadcrumb', $arraycheck)) {
    $type = 'ul';
  }


  // Drupal core only supports #title as a string. This implementation supports
  // heading level, and attributes as well.
  $heading = '';
  if (!empty($title)) {
    // If it's a string, normalize into an array.
    if (is_string($title)) {
      $title = array(
        'text' => $title,
      );
    }
    // Set defaults.
    $title += array(
      'level' => 'h3',
      'attributes' => array(),
    );
    // Heading outputs only when it has text.
    if (!empty($title['text'])) {
      $heading .= '<' . $title['level'] . drupal_attributes($title['attributes']) . '>';
      $heading .= empty($title['html']) ? check_plain($title['text']) : $title['text'];
      $heading .= '</' . $title['level'] . '>';
    }
  }

  if (in_array('breadcrumb', $arraycheck)) {
    $output = '<div class="item-list breadcrumb-list">';
  } else {
    $output = '<div class="item-list">';
  }

  if ($items) {
    $output .= '<' . $type . drupal_attributes($list_attributes) . '>';
    $i = 0;
    foreach ($items as $key => $item) {
      $attributes = array();

      if (is_array($item)) {
        $i++;
        $value = '';
        if (isset($item['data'])) {
          // Allow data to be renderable.
          if (is_array($item['data']) && (!empty($item['data']['#type']) || !empty($item['data']['#theme']))) {
            $value .= drupal_render($item['data']);
          }
          else {
            $value .= $item['data'];
          }
        }
        $attributes = array_diff_key($item, array('data' => 0, 'children' => 0));

        // Append nested child list, if any.
        if (isset($item['children'])) {
          // HTML attributes for the outer list are defined in the 'attributes'
          // theme variable, but not inherited by children. For nested lists,
          // all non-numeric keys in 'children' are used as list attributes.
          $child_list_attributes = array();
          foreach ($item['children'] as $child_key => $child_item) {
            if (is_string($child_key)) {
              $child_list_attributes[$child_key] = $child_item;
              unset($item['children'][$child_key]);
            }
          }
          $value .= theme('item_list', array(
            'items' => $item['children'],
            'type' => $type,
            'attributes' => $child_list_attributes,
          ));
        }
      }
      else {
        $value = $item;
      }

      if ($checkout_progress) {
        if ($value == 'Payment server') {
          $i--;
        } else {
          $output .= '<li' . drupal_attributes($attributes) . '><div class="number">' . $i . '</div>' . $value . "</li>\n";
        }

      }
      else if ($arraycheck == 'hotblocks-list') {
        $output .= '<li>' . $attributes['0'] . "</li>\n";
      }
      else {
        $output .= '<li' . drupal_attributes($attributes) . '>' . $value . "</li>\n";
      }

    }
    $output .= "</$type></div>";
  }

  // Output the list and title only if there are any list items.
  if (!empty($output)) {
    $output = $heading . $output;
  }

  return $output;
}


function bootstrap_rcpar_form($variables) {
  if ($variables['element']) {
    $element = $variables['element'];
    if (isset($element['#action'])) {
      $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
    }
    //print $variables['element']['form_id']['#id'];
    element_set_attributes($element, array('method', 'id'));
    if (empty($element['#attributes']['accept-charset'])) {
      $element['#attributes']['accept-charset'] = "UTF-8";
    }
    $formtitle = '';
    $formheading = '';
    if ($variables['element']['form_id']['#id'] == 'edit-user-login') {
      $formtitle = '<h2 class="form-title">Login</h2>';
    }
    if ($variables['element']['form_id']['#id'] == 'edit-user-register-form') {
      $formtitle = '<h2 class="form-title">Create An Account</h2>';
      $formheading = '<div class="form-heading">I do not have an account and need to create one</div>';
    }
    if ($variables['element']['form_id']['#id'] == 'edit-user-pass') {
      $formtitle = '<h2 class="form-title">Forgot your password?</h2>';
    }
    if ($variables['element']['form_id']['#id'] == 'edit-user-login') {
      $formheading = '<div class="form-heading">I have an account and need to login</div>';
    }
    $forgotpw = '';
    if (($variables['element']['form_id']['#id'] == 'edit-user-login') || ($variables['element']['#id'] == 'verify-discount-login-form' && !user_is_logged_in())) {
      $forgotpw = '<div class="forgot-password-link"><a href="/user/password">Forgot Password?</a></div>';
    }

    // Anonymous DIV to satisfy XHTML compliance.
    return $formtitle . '<form' . drupal_attributes($element['#attributes']) . '><div>' . $formheading . $element['#children'] . $forgotpw .'</div></form>';
  }
}

/**
 * Replacement for theme_form_element().
 */
function bootstrap_rcpar_webform_element($variables) {
  // Ensure defaults.
  $variables['element'] += array(
    '#title_display' => 'before',
  );

  $element = $variables['element'];

  // All elements using this for display only are given the "display" type.
  if (isset($element['#format']) && $element['#format'] == 'html') {
    $type = 'display';
  }
  else {
    $type = ($element['#webform_component']['type'] == 'select' && isset($element['#type'])) ? $element['#type'] : $element['#webform_component']['type'];
  }

  // Convert the parents array into a string, excluding the "submitted" wrapper.
  $nested_level = $element['#parents'][0] == 'submitted' ? 1 : 0;
  $parents = str_replace('_', '-', implode('--', array_slice($element['#parents'], $nested_level)));

  $wrapper_attributes = isset($element['#wrapper_attributes']) ? $element['#wrapper_attributes'] : array('class' => array());
  $wrapper_classes = array(
    'form-item',
    'webform-component',
    'webform-component-' . str_replace('_', '-', $type),
    'webform-component--' . $parents,
  );
  if (isset($element['#title_display']) && strcmp($element['#title_display'], 'inline') === 0) {
    $wrapper_classes[] = 'webform-container-inline';
  }
  $wrapper_attributes['class'] = array_merge($wrapper_classes, $wrapper_attributes['class']);
  $output = '<div ' . drupal_attributes($wrapper_attributes) . '><div class="webform-inner">' . "\n";

  // If #title_display is none, set it to invisible instead - none only used if
  // we have no title at all to use.
  if ($element['#title_display'] == 'none') {
    $variables['element']['#title_display'] = 'invisible';
    $element['#title_display'] = 'invisible';
    if (empty($element['#attributes']['title']) && !empty($element['#title'])) {
      $element['#attributes']['title'] = $element['#title'];
    }
  }
  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . webform_filter_xss($element['#field_prefix']) . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . webform_filter_xss($element['#field_suffix']) . '</span>' : '';

  switch ($element['#title_display']) {
    case 'inline':
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">' . $element['#description'] . "</div>\n";
  }
  if (arg(0)=='admin') {

    $output .= "</div>\n";
  } else {

    $output .= '<div class="clear-empty"></div>'."</div></div>\n";

  }
  return $output;
}


function bootstrap_rcpar_form_element_label($variables) {
  $element = $variables['element'];

  // This is also used in the installer, pre-database setup.
  $t = get_t();
  

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';
  //print $element['#title'];
  if (arg(0)=='user' && arg(1)=='login' && $element['#title']=='Username') {
    $title = 'E-mail address';
  }
  elseif (isset($element['#id']) && $element['#id']=="edit-pass-pass1") {
    $title = 'New Password';
  }
  elseif (isset($element['#id']) && $element['#id']=="edit-pass-pass2") {
    $title = 'Confirm New Password';
  }
  //fix for quetions on ipq
  elseif (isset($element['#id']) && strstr($element['#id'], 'edit-possible-answers-')) {
    $title = $element['#title'];
  }
  else {
    $title = filter_xss_admin($element['#title']);
  }


  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }
  if (isset($element['#id']) && $element['#id'] == "edit-subscriptions-2") {
    $title = 'Send updates to this blog to my inbox';
  }
  // The leading whitespace helps visually separate fields from inline labels.
  //print $element['#id'];

  if (isset($element['#id']) && ($element['#id']=="edit-submitted-month" || $element['#id']=="edit-submitted-year" || $element['#id']=="edit-submitted-expected-graduation-date" || $element['#id']=="edit-submitted-graduation-month" || $element['#id']=="edit-submitted-graduation-year" || $element['#id']=="edit-submitted-study-month" || $element['#id']=="edit-submitted-study-year" || $element['#id']=="edit-submitted-graduation-date-month" || $element['#id']=="edit-submitted-graduation-date-year")) {
    $output = ' <div class="col-xs-5"><label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label></div>\n";
  } elseif (isset($element['#id']) && ($element['#id'] == 'edit-submitted-graduation-date-month-ja' || $element['#id'] == 'edit-submitted-graduation-date-year-ja' || $element['#id']=="edit-submitted-study-for-the-cpa-exam-month-ja" || $element['#id']=="edit-submitted-study-for-the-cpa-exam-year-ja")) {
    $output = ' <div class="col-xs-7"><label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label></div>\n";
  } else {
    $output = ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
  }


  return $output;
}

function bootstrap_rcpar_fieldset($variables) {
  $element = $variables['element'];
  //print_r($element);
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    $fieldsettitle = $element['#title'];
    $testfor = 'I&#039;m a registered user';
    $testfor2 = 'I&#039;m not a registered user';
    if (strpos($fieldsettitle,$testfor) !== false) {
      $output .= '<div class="widget-title">';
      $output .= '<h3>Registered User</h3>';
      $output .= '</div>';
      $output .= '<div class="form-heading">I am a registered user.</div>';
    }
    elseif (strpos($fieldsettitle,$testfor2) !== false) {
      $output .= '<div class="widget-title">';
      $output .= '<h3>Create an Account</h3>';
      $output .= '</div>';
      $output .= '<div class="form-heading">I am not a registered user.</div>';
    } else {
      // Always wrap fieldset legends in a SPAN for CSS positioning.
      $output .= '<legend><span class="fieldset-legend">' . $element['#title'] . '</span></legend>';
    }
  }
  $output .= '<div class="fieldset-wrapper">';
  if (isset($element['#id']) && $element['#id']=='edit-commerce-payment') {
    $output .= '<div class="payment-heading-wrapper"><div class="payment-heading">Secure Credit Card Payment</div><div class="payment-copy">This is a secure 128-bit SSL encrypted payment</div></div>';
  }
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset>\n";
  return $output;
}

/**
 * Theming function for checkout progress item list.
 *
 * @param $variables
 *   An associative array containing:
 *   - items: Array of items to be displayed in the list. The key is
 *     the Page ID.  The value is the Page Title.
 *   - type: The type of list to return (e.g. "ul", "ol").
 *   - attributes: The attributes applied to the list element.
 *   - link: (bool) List should contain links to previously visited pages.
 *   - current_page: The page ID of the current page.
 */
function bootstrap_rcpar_commerce_checkout_progress_list($variables) {
  $path = drupal_get_path('module', 'commerce_checkout_progress');
  drupal_add_css($path . '/commerce_checkout_progress.css');

  extract($variables);

  // Option to display back pages as links.
  if ($link) {
    // Load the *shopping cart* order. It gets deleted on last page.
    if ($order = commerce_cart_order_load($GLOBALS['user']->uid)) {
      $order_id = $order->order_id;
    }
  }

  // This is where we build up item list that will be themed
  // This variable is used with $variables['link'], see more in inside comment.
  $visited = TRUE;
  // Our list of progress pages.
  $progress = array();
  foreach ($items as $page_id => $page) {
    $class = array();
    if ($page_id === $current_page) {
      $class[] = 'active';
      // Active page and next pages should not be linked.
      $visited = FALSE;
    }
    if (isset($items[$current_page]) && $page_id === $items[$current_page]['prev_page']) {
      $class[] = 'previous';
    }
    if (isset($items[$current_page]) && $page_id === $items[$current_page]['next_page']) {
      $class[] = 'next';
    }
    // @TODO: Calculate width based on 100 / qty of pages.
    $class[] = $page_id;
    $data    = t($page['title']);

    if ($visited) {
      $class[] = 'visited'; // Issue #1345942.

      // On checkout complete page, the checkout order is deleted.
      if (isset($order_id) && $order_id) {
        // If a user is on step 1, clicking a link next steps will be redirect them back.
        // Only render the link on the pages those user has already been on.
        // Make sure the loaded order is the same one found in the URL.
        if (arg(1) == $order_id) {
          $href = isset($page['href']) ? $page['href'] : "checkout/{$order_id}/{$page_id}";
          $data = l(filter_xss($data), $href, array('html' => TRUE));
        }
      }
    }

    $item = array(
      'data'  => $data,
      'class' => $class,
    );
    // Only set li title if the page has help text.
    if (isset($page['help'])) {
      //#1322436 Filter help text to be sure it contains NO html.
      $help = strip_tags($page['help']);
      // Make sure help has text event after filtering html.
      if (!empty($help)) {
        $item['title'] = $help;
      }
    }
    // Add item to progress array.
    $progress[] = $item;
  }

  return theme('item_list', array(
    'items' => $progress,
    'type' => $type,
    'attributes' => array('class' => array('commerce-checkout-progress', 'clearfix', 'inline',)),
  ));
}


function bootstrap_rcpar_field($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';

  foreach ($variables['items'] as $delta => $item) {
    // Override the date format display for graduation and study date user profile fields (also displays in checkout student info pane).
    if ($variables['element']['#field_name'] == 'field_graduation_month_and_year' || $variables['element']['#field_name'] == 'field_when_do_you_plan_to_start') {
      $item['#markup'] = '<span class="date-display-single" property="dc:date" datatype="xsd:dateTime" content="' . format_date(strtotime($variables['element']['#items'][0]['value']), 'custom', 'm/Y') . '">' . format_date(strtotime($variables['element']['#items'][0]['value']), 'custom', 'm/Y') . '</span>';
    }
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $fieldclasses = $variables['classes'];
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

function bootstrap_rcpar_select($variables) {
  $webform_conditionals_page = 0;
  if (arg(0)=='node' && arg(1)=='114' && arg(2)=='webform' && arg(3)=='conditionals') {
    $webform_conditionals_page = 1;
  }

  $contactus_page = 0;
  if (arg(0)=='node' && arg(1)=='114' && arg(2)=='') {
    $contactus_page = 1;
  }

  $quiz_take_page = 0;
  if (arg(0)=='ipq' && arg(2)=='take') {
    $quiz_take_page = 1;
  }
  $entity_edit_page = 0;
  if (arg(0)=='component_item') {
    $entity_edit_page = 1;
  }
  $element = $variables['element'];
  $field_installs = 0;
  if (isset($element['#parents']['0']) && $element['#parents']['0']=='field_installs') {
    $field_installs = 1;
  }

  element_set_attributes($element, array('id', 'name', 'size'));
  $calendardates = 0;

  $multipleselect = 0;

  $research_select = 0;
  $select_class = $element['#attributes']['class'];
  if (in_array('research-select',$select_class)) {
    $research_select = 1;
  }
  $nps_select = 0;
  if (in_array('nps-select',$select_class)) {
    $nps_select = 1;
  }
  if ($webform_conditionals_page || $contactus_page || $quiz_take_page || $field_installs || $calendardates || $multipleselect || $entity_edit_page || $research_select || $nps_select) {
    _form_set_class($element, array('form-select', 'form-control'));
  }
  else {
    _form_set_class($element, array('form-select selectpicker'));
  }
  // the key #data_tokens means that the select has tokens for the bootstrap-select live search feature on key words
  // this means we use enroll_enroll_flow_form_select_options instead of default form_select_options to return the select and it's options
  if (array_key_exists('#data_tokens', $element)){
    $output = '<select' . drupal_attributes($element['#attributes']) . ' data-live-search="true">' . enroll_flow_form_select_options($element) . '</select>';
  }
  elseif (isset($element['#attributes']['id'])) {
    if ($element['#attributes']['id'] == 'edit-submitted-date-passed-month' || $element['#attributes']['id'] == 'edit-submitted-date-passed-day' || $element['#attributes']['id'] == 'edit-submitted-date-passed-year') {
      $calendardates = 1;
    };
    if ($element['#attributes']['id'] == 'edit-submitted-what-courses-do-you-teach-select-all-that-apply') {
      $multipleselect = 1;
    }
    if ($webform_conditionals_page || $contactus_page || $quiz_take_page || $field_installs || $calendardates || $multipleselect || $entity_edit_page  || $research_select || $nps_select) {
      _form_set_class($element, array('form-select'));
    }
    else {
      _form_set_class($element, array('form-select selectpicker'));
    }

    if (($element['#attributes']['id'] == 'edit-commerce-fieldgroup-pane-group-additional-info-field-college-state-und')
      || ($element['#attributes']['id'] == 'edit-commerce-user-profile-pane-field-college-state-und')
      || ($element['#attributes']['id'] == 'edit-customer-profile-shipping-commerce-customer-address-und-0-country')
      || ($element['#attributes']['id'] == 'edit-customer-profile-shipping-commerce-customer-address-und-0-administrative-area')
      || ($element['#attributes']['id'] == 'edit-customer-profile-billing-commerce-customer-address-und-0-country')
      || ($element['#attributes']['id'] == 'edit-customer-profile-billing-commerce-customer-address-und-0-administrative-area')
      || ($element['#attributes']['id'] == 'edit-submitted-discount-verification-state-discount-verification')
      || ($element['#attributes']['id'] == 'edit-submitted-contact-information-state')) {
      $output = '<select' . drupal_attributes($element['#attributes']) . ' data-live-search="true">' . form_select_options($element) . '</select>';
    }
    elseif ($element['#attributes']['id'] == 'edit-submitted-month' || $element['#attributes']['id'] == 'edit-submitted-year' || $element['#id']=="edit-submitted-expected-graduation-date" || $element['#id']=="edit-submitted-graduation-month" || $element['#id']=="edit-submitted-graduation-year" || $element['#id']=="edit-submitted-study-month" || $element['#id']=="edit-submitted-study-year" || $element['#id']=="edit-submitted-graduation-date-month" || $element['#id']=="edit-submitted-graduation-date-year") {
      $output = '<div class="col-xs-7"><select' . drupal_attributes($element['#attributes']) . ' data-live-search="true">' . form_select_options($element) . '</select></div>';
    }
    elseif ($element['#attributes']['id'] == 'edit-submitted-graduation-date-month-ja' || $element['#attributes']['id'] == 'edit-submitted-graduation-date-year-ja' || $element['#id']=="edit-submitted-study-for-the-cpa-exam-month-ja" || $element['#id']=="edit-submitted-study-for-the-cpa-exam-year-ja") {
      $output = '<div class="col-xs-5"><select' . drupal_attributes($element['#attributes']) . ' data-live-search="true">' . form_select_options($element) . '</select></div>';
    }
    else {
      $output = '<select' . drupal_attributes($element['#attributes']) . ' data-live-search="true">' . form_select_options($element) . '</select>';
    }
  }
  else {
    $output = '<select' . drupal_attributes($element['#attributes']) . ' data-live-search="true">' . form_select_options($element) . '</select>';
  }
  return $output;
}


function bootstrap_rcpar_file($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'file';
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-file'));

  $output = '';
  /*if ($element['#id']=='edit-submitted-documents-upload') {
	  $output = '<div class="discount-copy">Please upload proof of your prior course (receipt, invoice, email qualifying you for their own previous student discount). Discounts are provided to past CPA Review courses that qualify. Certain restrictions may apply.</div>';
  }*/
  $output .='<input' . drupal_attributes($element['#attributes']) . ' />';
  return $output;
}



/**
 * Theme function for the commerce order.
 */
function bootstrap_rcpar_commerce_email_order_items($variables) {
  $table = bootstrap_rcpar_commerce_email_prepare_table($variables['commerce_order_wrapper']);
  return theme('table', $table);
}

/**
 * Returns HTML for a textfield form element.
 * Currently only overriden to add "monthpicker" class to grad and study date fields.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #title, #value, #description, #size, #maxlength,
 *     #required, #attributes, #autocomplete_path.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_textfield()
 *
 * @ingroup theme_functions
 */
function bootstrap_rcpar_textfield(array $variables) {

  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'maxlength',
  ));
  _form_set_class($element, array('form-text'));

  // Change from original bootstrap theme function:
  // Add monthpicker class to Sales Funnel Widget grad date and study date textfields
  if ($element['#name']=='sales_funnel_widget[field_graduation_month_and_year][und][0][value][date]'
    || $element['#name']=='sales_funnel_widget[field_when_do_you_plan_to_start][und][0][value][date]'
    || $element['#name']=='commerce_user_profile_pane[sales_funnel_widget][field_graduation_month_and_year][und][0][value][date]'
    || $element['#name']=='commerce_user_profile_pane[sales_funnel_widget][field_when_do_you_plan_to_start][und][0][value][date]') {
    $element['#attributes']['class'][] = 'monthpicker';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  if ($element['#autocomplete_path'] && !empty($element['#autocomplete_input'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    // Append a hidden autocomplete element.
    $autocomplete = array(
      '#type' => 'hidden',
      '#value' => $element['#autocomplete_input']['#url_value'],
      '#attributes' => array(
        'class' => array('autocomplete'),
        'disabled' => 'disabled',
        'id' => $element['#autocomplete_input']['#id'],
      ),
    );
    $output .= drupal_render($autocomplete);

    // Append icon for autocomplete "throbber" or use core's default throbber
    // whose background image must be set here because sites may not be
    // at the root of the domain (ie: /) and this value cannot be set via CSS.
    if (!isset($element['#autocomplete_icon'])) {
      $element['#autocomplete_icon'] = _bootstrap_icon('refresh', '<span class="autocomplete-throbber" style="background-image:url(' . file_create_url('misc/throbber.gif') . ')"></span>');
    }

    // Only add an icon if there is one.
    if ($element['#autocomplete_icon']) {
      $output .= '<span class="input-group-addon">' . $element['#autocomplete_icon'] . '</span>';

      // Wrap entire element in a input group if not already in one.
      if (!isset($element['#input_group']) && !isset($element['#input_group_button'])) {
        $input_group_attributes = isset($element['#input_group_attributes']) ? $element['#input_group_attributes'] : array();
        if (!isset($input_group_attributes['class'])) {
          $input_group_attributes['class'] = array();
        }
        if (!in_array('input-group', $input_group_attributes['class'])) {
          $input_group_attributes['class'][] = 'input-group';
        }
        $output = '<div' . drupal_attributes($input_group_attributes) . '>' . $output . '</div>';
      }
    }
  }
  return $output;
}


/**
 * Returns a table header and rows for theming.
 *
 * @param $wrapper
 *   A metadata-wrapped commerce_order entity
 *
 * @return array
 *   Array suitable for use by theme('table' ...)
 */
function bootstrap_rcpar_commerce_email_prepare_table($wrapper) {
  global $user;
  $user_offline_lectures = array();

  if (isset($_SESSION['serial_numbers'])) {
    /** @var \RCPAR\Entities\UserEntitlement $product */
    foreach ($_SESSION['serial_numbers'] as $key => $product) {
      if ((count($product['installs']) == 0 || $product['expiration_date'] >= REQUEST_TIME) && !empty($product['serial_number'])) {
        $user_offline_lectures[$product['product_id']] = $product['serial_number'];
      }
    }
  }
  $user_entitlements = user_entitlements_get_users_products($user, true);
  foreach($user_entitlements['products'] as $product) {
    $key = $product['product_id'];
    $already_set = array_keys($user_offline_lectures);
    if ((count($product['installs']) == 0 || $product['expiration_date'] >= REQUEST_TIME) && isset($product['serial_number']) && !in_array($key, $already_set) ) {
      $user_offline_lectures[$product['product_id']] = $product['serial_number'];
    }
  }

  $hide_prices = $use_PAL_confirmation = false;
  $p = new RCPARPartner();
  if (module_exists('rcpar_partners') && $p->isLoaded()) {
    $hide_prices = rcpar_partners_hide_prices();
    $use_PAL_confirmation = $p->isPartnerPAL();
  }

  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $amount = number_format(commerce_currency_amount_to_decimal($wrapper->commerce_order_total->amount->value(), $currency_code), 2);
  if ($hide_prices || $use_PAL_confirmation) {
    $header = array(
      array('data' => t('Product'), 'style' => array('text-align: left;background: none repeat scroll 0 0 #505050;color: #ffffff;font-size: 13px;font-weight: bold;padding: 15px 0 15px 20px;width: 378px;')),
      array('data' => t('Qty'), 'style' => array('text-align: left;
	background: none repeat scroll 0 0 #505050;color: #ffffff;font-size: 13px;font-weight: bold;padding: 15px 0 15px 20px;width: 202px;')),
    );
  } else {
    $header = array(
      array('data' => t('Product'), 'style' => array('text-align: left;
	background: none repeat scroll 0 0 #505050;color: #ffffff;font-size: 13px;font-weight: bold;padding: 15px 0 15px 20px;width: 202px;')),
      array('data' => t('Qty'), 'style' => array('text-align: left;
	background: none repeat scroll 0 0 #505050;color: #ffffff;font-size: 13px;font-weight: bold;padding: 15px 0 15px 20px;width: 202px;')),
      array('data' => t('Price (@currency_code)', array('@currency_code' => $currency_code)), 'style' => array('text-align: left;background: none repeat scroll 0 0 #505050;color: #ffffff;font-size: 13px;font-weight: bold;padding: 15px 0 15px 20px;width: 250px;'))
    );
  }


  // Get textbooks information
  $commerce_order_obj = new RCPARCommerceOrder($wrapper->raw());
  $saved_order_textbooks = $commerce_order_obj->getTextbooksVersions();

  // Get versioning or textbook versioning flag to enable/disable textbook version display on Packing label
  $use_versioning = exam_version_is_versioning_on() || exam_version_is_textbook_versioning_on();

  $rows = array();
  foreach ($wrapper->commerce_line_items as $delta => $line_item_wrapper) {

    switch ($line_item_wrapper->type->value()) {
      case 'product':
        // Special treatment for a product, since we want to get the title from
        // from the product entity instead of the line item.
        $title = htmlentities($line_item_wrapper->commerce_product->title->value(), ENT_QUOTES, "UTF-8");
        $title .= commerce_email_order_item_attributes($line_item_wrapper->commerce_product->product_id->value());

        $prod_id = $line_item_wrapper->commerce_product->product_id->value();
        $sku = $line_item_wrapper->commerce_product->sku->value();
        if (isset($user_offline_lectures[$prod_id])){
          $title .= '<div> Serial Number: <b>' . $user_offline_lectures[$prod_id] . '</b></div>';
        }

        // Attach textbook version
        if(!empty($saved_order_textbooks) && isset($saved_order_textbooks[$sku]) && $use_versioning){
          $title.= '<div> Textbook version: <b>'.$saved_order_textbooks[$sku].'</b></div>';
        }

        if ($hide_prices || $use_PAL_confirmation) {
          $rows[] = array(
            'data' => array(
              array('data' => $title, 'style' => array('text-align: left; color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
              array('data' => $line_item_wrapper->quantity->value(), 'style' => array('text-align: left; color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
              array('data' => '', 'style' => array('text-align: left;color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
            )
          );
        } else {
          $rows[] = array(
            'data' => array(
              array('data' => $title, 'style' => array('text-align: left;  color: #666666;font-size: 13px; vertical-align: top;padding: 15px 0 15px 20px;')),
              array('data' => $line_item_wrapper->quantity->value(), 'style' => array('text-align: left;  color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px; ')),
              array('data' => number_format(commerce_currency_amount_to_decimal($line_item_wrapper->commerce_unit_price->amount->value(), $currency_code), 2), 'style' => array('text-align: left;color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
            )
          );
        }

        break;
      case 'commerce_coupon':
        // Special treatment for a commerce coupon, since we want to hide the coupon type
        // See DEV-833
        $coupon_name = explode(':', $line_item_wrapper->line_item_label->value());
        $coupon_name[0] = 'Promotional Code';
        $coupon_name = implode(':', $coupon_name);
        if ($hide_prices) {
          $rows[] = array(
            'data' => array(
              array('data' => htmlentities($coupon_name, ENT_QUOTES, "UTF-8"), 'style' => array('text-align: left;')),
              array('data' => '', 'style' => array('text-align: left;color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
              array('data' => '', 'style' => array('text-align: left;color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
            )
          );
        }
        else {
          $rows[] = array(
            'data' => array(
              array('data' => htmlentities($coupon_name, ENT_QUOTES, "UTF-8"), 'style' => array('text-align: left;padding: 15px 0 15px 20px;')),
              array('data' => '', 'style' => array('text-align: left; color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
              array('data' => number_format(commerce_currency_amount_to_decimal($line_item_wrapper->commerce_unit_price->amount->value(), $currency_code), 2), 'style' => array('text-align: left; color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
            )
          );
        }
        break;
      default:
        // Use this for any other line item.
        if($use_PAL_confirmation){
          break;
        }
        if ($hide_prices) {
          $rows[] = array(
            'data' => array(
              array('data' => htmlentities($line_item_wrapper->line_item_label->value(), ENT_QUOTES, "UTF-8"), 'style' => array('text-align: left;')),
              array('data' => 1, 'style' => array('text-align: left;color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
              array('data' => '', 'style' => array('text-align: left;color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
            )
          );
        } else {
          $rows[] = array(
            'data' => array(
              array('data' => htmlentities($line_item_wrapper->line_item_label->value(), ENT_QUOTES, "UTF-8"), 'style' => array('text-align: left;padding: 15px 0 15px 20px;')),
              array('data' => 1, 'style' => array('text-align: left; color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
              array('data' => number_format(commerce_currency_amount_to_decimal($line_item_wrapper->commerce_unit_price->amount->value(), $currency_code), 2), 'style' => array('text-align: left; color: #666666;font-size: 13px;vertical-align: top;padding: 15px 0 15px 20px;')),
            )
          );
        }
        break;
    }
  }

  $data = $wrapper->commerce_order_total->data->value();
  if (!empty($data['components']) && !$hide_prices && !$use_PAL_confirmation) {

    foreach ($data['components'] as $key => &$component) {

      if ($data['components'][$key]['name'] == 'base_price') {
        $rows[] = array(
          'data' => array(
            ' ',
            array('data' => t('Subtotal:'), 'style' => array('color: #666666;font-size: 13px;font-weight: bold; text-align: right;padding: 15px 20px 15px 20px;border-top: 1px solid #eee;')),
            array('data' => number_format(commerce_currency_amount_to_decimal($data['components'][$key]['price']['amount'], $currency_code), 2), 'style' => array('color: #666666;font-size: 13px;font-weight: bold; text-align: left;padding: 15px 0 15px 20px;border-top: 1px solid #eee;')),
          )
        );
      }
      elseif (preg_match('/^tax\|/', $data['components'][$key]['name'])) {
        $rows[] = array(
          'data' => array(
            ' ',
            array('data' => $data['components'][$key]['price']['data']['tax_rate']['display_title'] . ':', 'style' => array('color: #666666;font-size: 13px;font-weight: bold; text-align: right;padding: 15px 20px 15px 20px;border-top: 1px solid #eee;')),
            array('data' => number_format(commerce_currency_amount_to_decimal($data['components'][$key]['price']['amount'], $currency_code), 2), 'style' => array('color: #666666;font-size: 13px;font-weight: bold; text-align: left;padding: 15px 0 15px 20px;border-top: 1px solid #eee;')),
          )
        );
      }
      // percentage coupons (used by discount validation module)
      elseif (preg_match('/^commerce_coupon_pct_(.*)/', $data['components'][$key]['name'], $matches)) {
        $coupons = discount_verification_available_coupons();
        $coupon_key = strtoupper(str_replace('_','-',$matches[1]));
        if (isset($coupons[$coupon_key]) ){
          $discount_name = $coupons[$coupon_key]['name'] . ' Discount';
        } else {
          $discount_name = 'Discount';
        }
        $rows[] = array(
          'data' => array(
            ' ',
            array('data' => $discount_name . ':', 'style' => array('color: #666666;font-size: 13px;font-weight: bold; text-align: right;padding: 15px 20px 15px 20px;border-top: 1px solid #eee;')),
            array('data' => number_format(commerce_currency_amount_to_decimal($data['components'][$key]['price']['amount'], $currency_code), 2), 'style' => array('color: #666666;font-size: 13px;font-weight: bold; text-align: left;padding: 15px 0 15px 20px;border-top: 1px solid #eee;')),
          )
        );
      }
    }
  }
  if (!$hide_prices && !$use_PAL_confirmation) {
    $rows[] = array(
      'data' => array(
        ' ',
        array('data' => t('Total:'), 'style' => array('color: #666666;font-size: 13px;font-weight: bold; text-align: right;padding: 15px 20px 15px 20px;border-top: 1px solid #eee;')),
        array('data' => $amount, 'style' => array('color: #666666;font-size: 13px;font-weight: bold; text-align: left;padding: 15px 0 15px 20px;border-top: 1px solid #eee;')),
      )
    );
  }
  return array('header' => $header, 'rows' => $rows, 'attributes' => array('style' => array('width: 90%; border: 0;border-collapse: collapse;margin-top: 40px;')));
}


function bootstrap_rcpar_table($variables) {
  $header = $variables['header'];
  $rows = $variables['rows'];
  $attributes = $variables['attributes'];
  $caption = $variables['caption'];
  $colgroups = $variables['colgroups'];
  $sticky = $variables['sticky'];
  $empty = $variables['empty'];

  // Add sticky headers, if applicable.
  if(!empty($header) && $sticky) {
    drupal_add_js('misc/tableheader.js');
    // Add 'sticky-enabled' class to the table to identify it for JS.
    // This is needed to target tables constructed by this function.
    $attributes['class'][] = 'sticky-enabled';
  }

  $output = '<table' . drupal_attributes($attributes) . ">\n";

  if (isset($caption)) {
    $output .= '<caption>' . $caption . "</caption>\n";
  }

  // Format the table columns:
  if (count($colgroups)) {
    foreach ($colgroups as $number => $colgroup) {
      $attributes = array();

      // Check if we're dealing with a simple or complex column
      if (isset($colgroup['data'])) {
        foreach ($colgroup as $key => $value) {
          if ($key == 'data') {
            $cols = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cols = $colgroup;
      }

      // Build colgroup
      if (is_array($cols) && count($cols)) {
        $output .= ' <colgroup' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cols as $col) {
          $output .= ' <col' . drupal_attributes($col) . ' />';
        }
        $output .= " </colgroup>\n";
      }
      else {
        $output .= ' <colgroup' . drupal_attributes($attributes) . " />\n";
      }
    }
  }

  // Add the 'empty' row message if available.
  if (!count($rows) && $empty) {
    $header_count = 0;
    foreach ($header as $header_cell) {
      if (is_array($header_cell)) {
        $header_count += isset($header_cell['colspan']) ? $header_cell['colspan'] : 1;
      }
      else {
        $header_count++;
      }
    }
    $rows[] = array(array(
      'data' => $empty,
      'colspan' => $header_count,
      'class' => array('empty', 'message'),
    ));
  }

  // Format the table header:
  if(!empty($header) && $sticky) {
    $ts = tablesort_init($header);
    // HTML requires that the thead tag has tr tags in it followed by tbody
    // tags. Using ternary operator to check and see if we have any rows.
    $output .= (count($rows) ? ' <thead><tr>' : ' <tr>');
    foreach ($header as $cell) {
      $cell = tablesort_header($cell, $header, $ts);
      $output .= _theme_table_cell($cell, TRUE);
    }
    // Using ternary operator to close the tags based on whether or not there are rows
    $output .= (count($rows) ? " </tr></thead>\n" : "</tr>\n");
  }
  else {
    $ts = array();
  }

  // Format the table rows:
  if (count($rows)) {
    $output .= "<tbody>\n";
    $flip = array(
      'even' => 'odd',
      'odd' => 'even',
    );
    $class = 'even';
    foreach ($rows as $number => $row) {
      // Check if we're dealing with a simple or complex row
      if (isset($row['data'])) {
        $cells = $row['data'];
        $no_striping = isset($row['no_striping']) ? $row['no_striping'] : FALSE;

        // Set the attributes array and exclude 'data' and 'no_striping'.
        $attributes = $row;
        unset($attributes['data']);
        unset($attributes['no_striping']);
      }
      else {
        $cells = $row;
        $attributes = array();
        $no_striping = FALSE;
      }
      if (count($cells)) {
        // Add odd/even class
        if (!$no_striping) {
          $class = $flip[$class];
          $attributes['class'][] = $class;

          // Add class if element is a coupon entry
          foreach ($attributes['class'] as $k=>$v) {
            if (strstr($v, 'component-type-commerce-coupon-') !== FALSE) {
              $attributes['class'][] = ' component-type-commerce-coupon';
            }
          }
        }

        // Build row
        $output .= ' <tr' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cells as $cell) {
          $cell = tablesort_cell($cell, $header, $ts, $i++);
          $output .= _theme_table_cell($cell);
        }
        $output .= " </tr>\n";
      }
    }
    $output .= "</tbody>\n";
  }

  $output .= "</table>\n";
  return $output;
}


function bootstrap_rcpar_format_comma_field($field_category, $node, $limit = NULL) {
  $category_arr = array();
  $category = '';
  if (!empty($node->{$field_category}[LANGUAGE_NONE])) {
    foreach ($node->{$field_category}[LANGUAGE_NONE] as $item) {
      $term = taxonomy_term_load($item['tid']);
      if ($term) {
        $category_arr[] = l($term->name, 'taxonomy/term/' . $item['tid']);
      }

      if ($limit) {
        if (count($category_arr) == $limit) {
          $category = implode(', ', $category_arr);
          return $category;
        }
      }
    }
  }
  $category = implode(', ', $category_arr);

  return $category;
}

/**
 * Overrides theme_menu_link().
 */
function bootstrap_rcpar_menu_link(array $variables) {
  global $user;

  $element = $variables['element'];

  // 8585 is the Study Planer menu item on dashboard/my-courses/* pages
  // we shouldn't show it to ASL or PAL users without other products purchased
  if ($variables['element']['#original_link']['mlid'] == 8484) {
    $user_is_ACT_or_PAL = user_entitlements_is_user_act_or_pal($user);
    $user_has_paid_entitlements = user_entitlements_user_has_paid_entitlements($user);
    // if the user is ASL or PAL and has no other paid entitlements,
    // we shouldn't allow them to access to the study-planners page
    if ($user_is_ACT_or_PAL && !$user_has_paid_entitlements) {
      return;
    }
  }

  // Hide Free Trial button from the partner registration pages for:
  // * University/Free Access
  // * Partner/Free Access
  // * Publisher/Free Access
  if(arg(0) == 'partner' && arg(1) != '' && $element['#original_link']['mlid'] == 49631) {
    $partner = new RCPARPartner(arg(1));
    if($partner->isLoaded()){
      if($partner->isBillingFreeAccess() &&
        ($partner->isPartnerTypeUniversity()
          || $partner->isPartnerTypeFirm()
          || $partner->isPartnerTypePublisher()))
      {
        $element['#attributes']['class'][] = 'hidden';
      }
    }
  }

  // this set of "ifs" remove the Interactive Practice Questions link if the user does not have access
  if (isset($variables['element']['#theme'])) {
    // the mlids in the array represent links with the path to ipq/overview (8547 is legacy link)
    // 8551 is study planners link
    if (in_array($variables['element']['#original_link']['mlid'], array(8482, 8547, 24369, 33056, 8551))) {
      if (!ipq_common_access()) {
        return;
      }
      // Also, for study planers link, we need that the user is not an ASL or PAL user without other
      // entitlements
      if ($variables['element']['#original_link']['mlid'] == 8551) {
        $user_is_ACT_or_PAL = user_entitlements_is_user_act_or_pal($user);
        $user_has_paid_entitlements = user_entitlements_user_has_paid_entitlements($user);
        // if the user is ASL or PAL and has no other paid entitlements,
        // we shouldn't allow them to access to the study-planners page
        if ($user_is_ACT_or_PAL && !$user_has_paid_entitlements) {
          return;
        }
      }
    }
  }

  if ($element['#original_link']['mlid'] == 17030) {
    global $user;
    $beta_users = array(
      27090,
      36229,
      27566,
      35794,
      36456,
      36694,
      36768,
      26707,
      27962,
      33451,
      36285,
      34706,
      33344,
      33751,
      33874,
      35149,
      36179,
      35696,
      33777,
      35239,
      34590,
      34707,
      37068,
      21916
    );
    if (!in_array($user->uid, $beta_users)) {
      return;
    }
  }
  //adding number of items in cart to cart menu link
  $cart_item_total = '0';
  $cart_title = '';

  if (module_exists("rcpar_dashboard")) {
    if (rcpar_dashboard_remove_menu_item($element['#href'])) {
      return NULL;
    }
  }

  // Replace Username token in menu
  if (strstr($element['#title'], "%username Dashboard")) {
    $element['#title'] = str_ireplace("%username", $user->name . '\'s', $element['#title']);
  }

  if ($element['#title'] == 'Cart') {
    $uid = 0;
    $items = array();
    $total = 0;
    $currency = 'USD';

    if (user_is_logged_in()) {
      $uid = $user->uid;
    }

    $order = commerce_cart_order_load($uid);

    $product_ids = enroll_flow_get_line_item_product_ids($order);
    $cart_item_total = count($product_ids);
    $element['#localized_options']['html'] = TRUE;
    // $cart_title = '<span class="cart-title">' . $element['#title'] . '</span><span class="cart-count-notch notch-items-' . $cart_item_total . '">&nbsp;</span><span class="cart-count items-' . $cart_item_total . '">' . $cart_item_total . '</span>';
    $cart_title = '<span class="cart-title">' . $element['#title'] . '</span>';
    $element['#title'] = $cart_title;
    if ($cart_item_total) {
      $element['#localized_options']['attributes']['class'][] = 'cart-filled';
    }

  }

  $sub_menu = '';
  $element_aria = '';
  $element_tabindex = '';
  if ($element['#below']) {
    $mobile_submenu_toggle = '';
    // Prevent dropdown functions from being added to management menu so it
    // does not affect the navbar module.
    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    }
    elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
      $mobile_submenu_toggle = '<span class="btn-submenu"></span>';
      // Add our own wrapper.
      unset($element['#below']['#theme_wrappers']);
      $sub_menu = '<ul class="sub-menu" role="menu" aria-hidden="true">' . drupal_render($element['#below']) . '</ul>';
      // Generate as standard dropdown.
      $element['#attributes']['class'][] = 'dropdown';
      $element['#localized_options']['html'] = TRUE;

      // Set dropdown trigger element to # to prevent inadvertant page loading
      // when a submenu link is clicked.
      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
    }
    else {
      unset($element['#below']['#theme_wrappers']);
      $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';
      $mobile_submenu_toggle = '<span class="btn-submenu"></span>';
      $element['#title'] .= '<span class="btn-submenu-desktop"></span>';
      $element['#attributes']['class'][] = 'dropdown';
      $element['#localized_options']['html'] = TRUE;
      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      $element['#localized_options']['attributes']['data-hover'] = 'dropdown';
    }
    $element_aria = ' aria-haspopup="true"';
  }
  if (!$element['#original_link']['has_children']) {
    if (in_array('first', $element['#attributes']['class'])) {
      $element_tabindex = ' tabindex="0"';
    }
  }
  // On primary navigation menu, class 'active' is not set on active menu item.
  // @see https://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  // my account page menu
  if (($element['#theme'] == 'menu_link__menu_account_settings') || ($element['#theme'] == 'menu_link__menu_my_courses___tools')) {
    $element['#localized_options']['html'] = TRUE;
    $element['#title'] = $element['#original_link']['link_title'];
    $element['#localized_options']['attributes'] = isset($element['#localized_options']['attributes']) ? $element['#localized_options']['attributes'] : $element['#original_link']['options']['attributes'];
    $output = l($element['#title'] . '<i class="icon glyphicon glyphicon-chevron-right" aria-hidden="true"></i>', $element['#href'], $element['#localized_options']);
  }

  else {
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  }
  return '<li' . drupal_attributes($element['#attributes']) . ' role="menuitem"' . $element_aria . $element_tabindex . '>' . $output . $mobile_submenu_toggle .$sub_menu .  "</li>\n";

}


function bootstrap_rcpar_bootstrap_search_form_wrapper($variables) {
  $output = '<div class="input-group">';
  $output .= $variables['element']['#children'];
  $output .= '<span class="input-group-btn">';
  $output .= '<button type="submit" class="btn btn-default"><span class="hidden-xs hidden-sm hidden-xsm">';
  $output .= _bootstrap_icon('search');
  $output .= '</span><span class="hidden-md hidden-lg">';
  $output .= _bootstrap_icon('search');
  $output .= '</span><span class="reader-instructions">Search our site</span></button>';
  $output .= '</span>';
  $output .= '</div>';
  return $output;
}

function bootstrap_rcpar_preprocess_block(&$variables) {

  $allBlocks = block_list($variables['block']->region);
  $count = count($allBlocks);
  switch ($count) {
    case 0:
      $count = 'empty';
      break;

    case 1:
      $count = 'one';
      break;

    case 2:
      $count = 'two';
      break;

    case 3:
      $count = 'three';
      break;

    case 4:
      $count = 'four';
      break;

    case 5:
      $count = 'five';
      break;

    case 6:
      $count = 'six';
      break;

    default:
      $count = 'default';
  }

  $variables['classes_array'][] = 'max-'. ($count);
  $variables['classes_array'][] = drupal_html_class('block-' . $variables['block']->module);

  if (($variables['block']->region == 'home_row1')
    || ($variables['block']->region == 'home_row2')
    || ($variables['block']->region == 'home_row3')
    || ($variables['block']->region == 'home_row4')
    || ($variables['block']->region == 'home_row5')) {
    if ($count == 'two') {
      $variables['classes_array'][] = 'col-xs-12 col-sm-12 col-md-6';
    }
  }

  // For custom user - account menu
  if ($variables['block_html_id'] == 'block-rcpar-mods-rcpar-mods-user-menu') {
    $variables['classes_array'][] = 'custom-user-links';
  }
  // For international webform
  if(isset($variables['elements']['#block']) && $variables['elements']['#block']->subject == "Get your Free International eBook"){
    $variables['classes_array'][] = 'rcpar-international-webform';
  }
  // Replace text token with free trial upgrade form in 'Dashboard Free Trial alert - almost expired' block.
  if (isset($variables['block']->machine_name) && $variables['block']->machine_name == 'free_trial_almost_expired_block'){
    global $user;
    $form = drupal_get_form('user_entitlements_delete_entitlements_form', $user->uid);
    $form['view_details']['#prefix'] = '';
    $form['view_details']['#value'] = 'Upgrade Now';
    $form['#attributes'] = array('class' => 'free-trial-upgrade');
    $block_content = $variables['content'];
    $variables['content'] = str_replace('[free-trial-upgrade-form]',render($form),$block_content);
  }

}


/**
 * Overrides theme_menu_tree()
 * @param $variables
 * @return string
 */
function bootstrap_rcpar_menu_tree(&$variables) {
  if (($variables['theme_hook_original']=='menu_tree__menu_account_settings') || ($variables['theme_hook_original']=='menu_tree__menu_my_courses___tools')) {
    return '<ul class="menu nav nav-pills nav-stacked">' . $variables['tree'] . '</ul>';
  } else {
    // For accessibility we are adding an aria-labelledby attribute
    // which will correspond with the block title's ID in the block template,
    // based on the block's ID (which is created from the block's module and delta)
    return '<ul class="menu nav" aria-labelledby="' . drupal_html_id('title-block-' . $variables['#tree']['#block']->module . '-' . $variables['#tree']['#block']->delta) . '">' . $variables['tree'] . '</ul>';
  }
}

/**
 * Overrides theme_menu_tree__primary()
 * Bootstrap parent theme already overrides this.
 * This will override the Bootstrap override.
 * @param $variables
 * @return string
 */
function bootstrap_rcpar_menu_tree__primary(&$variables) {
  return '<ul class="menu nav navbar-nav" role="menubar" aria-haspopup="true">' . $variables['tree'] . '</ul>';
}


// providing a theme template for the profile edit form and page
function bootstrap_rcpar_theme() {
  return array(
    'user_profile_form' => array(
      // Forms always take the form argument.
      'arguments' => array('form' => NULL),
      'render element' => 'form',
      'template' => 'templates/user-profile-edit',
    ),
  );
}

// the user profile edit form is using the sales_funnel_widget fields
// which require ajax for the college state dropdown
// the ajax won't work unless this is included
// (also applies to the password reset form--password reset is the profile edit form)
function bootstrap_rcpar_preprocess_user_profile_form(&$variables) {
  global $user;
  $form_id = 'user_profile_form';
  $form_state = array();
  $form_state['build_info']['args'] = array($user);

  $variables['rendered'] = drupal_build_form($form_id, $form_state);
}

function bootstrap_rcpar_button($variables) {
  $element = $variables['element'];
  $label = $element['#value'];
  //print_r($variables);

  element_set_attributes($element, array('id', 'name', 'value', 'type'));  // If a button type class isn't present then add in default.
  $button_classes = array(
    'btn-default',
    'btn-primary',
    'btn-success',
    'btn-info',
    'btn-warning',
    'btn-danger',
    'btn-link',
  );
  $class_intersection = array_intersect($button_classes, $element['#attributes']['class']);
  if (empty($class_intersection)) {
    $element['#attributes']['i'][] = 'btn-default';
  }
  if (isset($element['#attributes']['id'])) {
    if ($element['#attributes']['id'] == 'edit-checkout') {
      $element['#attributes']['class'][] = 'btn-default btn-primary';
    }
    if ($element['#attributes']['id'] == 'edit-continue-shopping') {
      $element['#attributes']['class'][] = 'btn-default btn-success';
    }
    if ($element['#attributes']['id'] == 'edit-continue') {
      $element['#attributes']['class'][] = 'btn-default btn-primary';
    }
    if ($element['#attributes']['id'] == 'edit-back') {
      $element['#attributes']['class'][] = 'btn-default btn-primary';
    }
  }
  if (isset($element['#value']) && $element['#value'] == 'Save') {
    //$label = 'Subscribe';
  }
  if (isset($element['#value']) && $element['#value'] == 'Log in') {
    $label = 'Login';
  }


  // Add in the button type class.
  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];



  $path = current_path();
  $path_alias = drupal_lookup_path('alias',$path);

  // Add chevron for add to cart buttons
  if ($element['#value']=='Add to cart') {
    $chevron = ' <i class="fa fa-chevron-right"></i>';
    $element['#attributes']['class'][] = 'btn-add-to-cart';
    // Change button text on courses and products page
    if ($path_alias == 'cpa-courses') {
      $label = 'Buy It Now';
    }
  } else {
    $chevron = '';
  }

  // Add chevron for Join the Community email subscription form
  if ($path_alias == 'learning-center') {
    $chevron = '<i class="fa fa-chevron-right"></i>';
    $label = '';
  }

  // change button for Dictionary term search form to glyphicon
  if ($element['#id']=='edit-submit-dictionary-term-search') {
    $chevron = '<i class="icon glyphicon glyphicon-search" aria-hidden="true"></i>';
    $label = '';
  }
  // This line break adds inherent margin between multiple buttons.
  return '<button' . drupal_attributes($element['#attributes']) . '>' . $label . $chevron ."</button>\n";
}

/**
 * Used to filter and display the "Customer Reviews - content type testimonials on product pages.
 * Invoked from node--product_display.tpl.php
 */
function bootstrap_rcpar_get_current_product_display_page($product_display_id = NULL){
  $term_id = 1481;

  if(!$product_display_id && arg(0) == "node" && ctype_digit(arg(1))) {
    $product_display_id = arg(1);
  }

  if ($product_display_id) {
    // Node ids of course packages changed when commerce_bundle was introduced.  We're using node URL alias here now
    // instead of nid. It may be more reliable to do the same with the other products in the future as well. -jd
    $alias = drupal_get_path_alias("node/$product_display_id");
    switch ($alias) {
      // Select Course Package - Formerly nid 12:
      case 'cpa-courses/select':
        $term_id = 1487;
        break;
      // Premier Course Package - Formerly nid 301:
      case 'cpa-courses/premier':
        $term_id = 1488;
        break;
      // Elite Course Package - Formerly nid 8594:
      case 'cpa-courses/elite':
        $term_id = 1489;
        break;
      // New mobile app product pages:
      case 'cpa-courses/mobile-app':
      case 'cpa-courses/mobile-offline-access':
        $term_id = 25336;
        break;

    }

    switch ($product_display_id) {
      //select
      case 12:
        //$term_id = 1480;
        $term_id = 1487;
        break;
      //premier
      case 301:
        //$term_id = 1481;
        $term_id = 1488;
        break;
      //elite
      case 8594:
        //$term_id = 1482;
        $term_id = 1489;
        break;
      //Online Cram Course
      case 298:
      case 299:
      case 395:
        //$term_id = 1415;
        $term_id = 1490;
        break;
      //Offline Lectures
      case 684:
        //$term_id = 1486;
        $term_id = 1491;
        break;
      //Audio Lectures
      case 727:
        //$term_id = 1412;
        $term_id = 1492;
        break;
      //Flash Drive Cram Course
      case 297:
        //$term_id = 1483;
        $term_id = 1493;
        break;
      //Flashcards
      case 725:
        //$term_id = 1484;
        $term_id = 1494;
        break;
      //Digital Flashcards App
      case 15919:
        $term_id = 1495;
        break;
      //MC Questions App
      case 6582:
        //$term_id = 1413;
        $term_id = 1496;
        break;
      //Fresh Start Discount
      case 38:
        //$term_id = 1413;
        $term_id = 1505;
        break;
      //College Student Discount
      case 78:
        //$term_id = 1413;
        $term_id = 1506;
        break;
      //Previous Roger Student Discount
      case 39:
        //$term_id = 1413;
        $term_id = 1507;
        break;
      //Interactive CPA Exam Practice Questions
      case 6581:
        //$term_id = 1413;
        $term_id = 1497;
        break;
      //CPA Review 6 Month Extension
      case 311:
        //$term_id = 1413;
        $term_id = 1500;
        break;
    }
  }

  return $term_id;
}

/**
 * Implements hook_views_pre_render.
 * Overriding the success stories block and adding css to slideshow view block.
 * @param $view
 */
function bootstrap_rcpar_views_pre_render(&$view) {
  // Overriding the success stories block.
  if ($view->name == 'success_stories' && $view->current_display == 'block_1') {
    foreach ($view->result as $result) {
      // Prevent body field from rendering if the node is a case study.
      if ($result->_field_data['nid']['entity']->type == 'case_study') {
        $result->field_body[0]['rendered']['#access'] = FALSE;
      }
    }
  }
  // Adding css for slideshow view block.
  if ($view->name == 'slideshow' && $view->current_display == 'block') {
    drupal_add_css(drupal_get_path('theme', 'bootstrap_rcpar') .'/css/slideshow-templates.css');
  }
  // Adding css for case studies view block.
  if ($view->name == 'case_studies' && $view->current_display == 'block') {
    drupal_add_css(drupal_get_path('theme', 'bootstrap_rcpar') .'/css/case-studies.css');
  }
}


function bootstrap_rcpar_preprocess_views_view(&$vars) {
  if (!empty($vars['feed_icon'])) {
    preg_match('/<a href="(.+)">/', $vars['feed_icon'], $match);
    $url = parse_url($match[1]);
    $vars['feed_icon'] = l("Download CSV",$url['path'],array('attributes' => array('class' => array("btn","btn-primary","btn-lg"))));
  }
  // change the title of the QA Question List view to be the category
  if ($vars['view']->name == 'qa_question_list') {
    // get the category
    // count the number of results, because for some reason if two terms are
    // assigned to the same node, they'll both show up here
    $category_count = count($vars['view']->result['0']->field_field_category);
    // if more than one category in the array, we have to find the one
    // that matches the view arg/URL path
    if ($category_count > 1) {
      $categories = $vars['view']->result['0']->field_field_category;
      foreach ($categories as $category) {
        // replace spaces with dashes and
        // convert the string to all lowercase
        $category_link = str_replace(' ', '-', strtolower(trim($category['rendered']['#title'])));
        if ($category_link == $vars['view']->args['0']) {
          $category_name = $category['rendered']['#title'];
        }
      }
    }
    else {
      $category_name = $vars['view']->result['0']->field_field_category['0']['rendered']['#title'];
    }

    //update the title
    $vars['view']->build_info['title'] = $category_name . ' Q&A';
  }
  if ($vars['view']->name == 'testimonial_rotator' && $vars['view']->current_display == 'block_7') {
    drupal_add_css(drupal_get_path('theme', 'bootstrap_rcpar') .'/css/testimonials.css');
  }
  // Add the countdown timer script to the slideshow if it's needed.
  if ($vars['name']=='slideshow' && strstr($vars['rows'], 'id="countdown"')) {
    drupal_add_css(drupal_get_path('theme', 'bootstrap_rcpar') .'/js/countdown/jquery.countdown.css');
    drupal_add_js(path_to_theme() . '/js/countdown/jquery.countdown.js', array('scope'=>'header', 'weight'=>10));
    drupal_add_js(path_to_theme() . '/js/countdown/jquery.countdown.start.js', array('scope'=>'header', 'weight'=>20));
  }
}

function bootstrap_rcpar_preprocess_bootstrap_panel(&$variables){
  if(isset($variables['element']['payment_methods']) && $variables['prefix']){
    $variables["prefix"] = FALSE;
  }
}


/**
 * @file
 * status-messages.func.php
 */


function bootstrap_rcpar_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
    'info' => t('Informative message'),
  );

  // Map Drupal message types to their corresponding Bootstrap classes.
  // @see http://twitter.github.com/bootstrap/components.html#alerts
  $status_class = array(
    'status' => 'success',
    'error' => 'danger',
    'warning' => 'warning',
    // Not supported, but in theory a module could send any type of message.
    // @see drupal_set_message()
    // @see theme_status_messages()
    'info' => 'info',
  );

  foreach (drupal_get_messages($display) as $type => $messages) {
    $class = (isset($status_class[$type])) ? ' alert-' . $status_class[$type] : '';
    $output .= "<div class=\"messages $type alert alert-block$class\">\n";
    $output .= "  <a class=\"close\" data-dismiss=\"alert\" href=\"#\">&times;</a>\n";

    if (!empty($status_heading[$type])) {
      $output .= '<h4 class="element-invisible">' . $status_heading[$type] . "</h4>\n";
    }

    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }

    $output .= "</div>\n";
  }
  return $output;
}




/**
 * Themes a price components table.
 *
 * @param $variables
 *   Includes the 'components' array and original 'price' array.
 */
function bootstrap_rcpar_commerce_price_formatted_components($variables) {

  //print_r($variables);
  // Add the CSS styling to the table.
  drupal_add_css(drupal_get_path('module', 'commerce_price') . '/theme/commerce_price.theme.css');

  // Build table rows out of the components.
  $rows = array();

  //print_r($variables['components']);
  $discount_class = ' no-discount';

  if (isset($variables['components']['discount'])) {
    $discount_class = ' discount';
  }
  foreach ($variables['components'] as $name => $component) {
    //print_r($component);
    $table_cell_class = drupal_html_class($component['title']);
    $rows[] = array(
      'data' => array(
        array(
          'data' => $component['title'],
          'class' => array('component-title'),
        ),
        array(
          'data' => $component['formatted_price'],
          'class' => array('component-total', $table_cell_class),
        ),
      ),
      'class' => array(drupal_html_class('component-type-' . $name), $discount_class),
    );
  }

  return theme('table', array('rows' => $rows, 'attributes' => array('class' => array('commerce-price-formatted-components'))));
}


/**
 * @file
 * form-element.func.php
 */

/**
 * Overrides theme_form_element().
 */
function bootstrap_rcpar_form_element(&$variables) {
  $element = &$variables['element'];
  $is_checkbox = FALSE;
  $is_radio = FALSE;
  //print_r($variables);
  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }

  // Check for errors and set correct error class.
  if (isset($element['#parents']) && form_get_error($element)) {
    $attributes['class'][] = 'error';
  }

  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(
        ' ' => '-',
        '_' => '-',
        '[' => '-',
        ']' => '',
      ));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  if (!empty($element['#autocomplete_path']) && drupal_valid_path($element['#autocomplete_path'])) {
    $attributes['class'][] = 'form-autocomplete';
  }
  $attributes['class'][] = 'form-item';

  // See http://getbootstrap.com/css/#forms-controls.
  if (isset($element['#type'])) {
    if ($element['#type'] == "radio") {
      $attributes['class'][] = 'radio';
      $is_radio = TRUE;
    }
    elseif ($element['#type'] == "checkbox") {
      $attributes['class'][] = 'checkbox';
      $is_checkbox = TRUE;
    }
    else {
      $attributes['class'][] = 'form-group';
    }
  }

  if (isset($element['#id'])) {
    if ($element['#id'] == "edit-customer-profile-shipping-commerce-customer-address-und-0-first-name" ||
      $element['#id'] == "edit-customer-profile-shipping-commerce-customer-address-und-0-last-name" ||
      $element['#id'] == "edit-customer-profile-billing-commerce-customer-address-und-0-first-name" ||
      $element['#id'] == "edit-customer-profile-billing-commerce-customer-address-und-0-last-name"
    ) {
      $attributes['class'][] = 'col-md-6';
    }
  }

  if (isset($element['#id'])) {
    if ($element['#id'] == "edit-commerce-coupon-coupon-code") {
      $attributes['class'][] = 'col-sm-6 col-xs-6';
    }
  }


  $description = FALSE;
  $tooltip = FALSE;
  // Convert some descriptions to tooltips.
  // @see bootstrap_tooltip_descriptions setting in _bootstrap_settings_form()
  if (!empty($element['#description'])) {
    $description = $element['#description'];
    if (theme_get_setting('bootstrap_tooltip_enabled') && theme_get_setting('bootstrap_tooltip_descriptions') && $description === strip_tags($description) && strlen($description) <= 200) {
      $tooltip = TRUE;
      $attributes['data-toggle'] = 'tooltip';
      $attributes['title'] = $description;
    }
  }


  $output = '<div' . drupal_attributes($attributes) . '><div class="form-item-inner">' ."\n";



  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }

  $prefix = '';
  $suffix = '';
  if (isset($element['#field_prefix']) || isset($element['#field_suffix'])) {
    // Determine if "#input_group" was specified.
    if (!empty($element['#input_group'])) {
      $prefix .= '<div class="input-group">';
      $prefix .= isset($element['#field_prefix']) ? '<span class="input-group-addon">' . $element['#field_prefix'] . '</span>' : '';
      $suffix .= isset($element['#field_suffix']) ? '<span class="input-group-addon">' . $element['#field_suffix'] . '</span>' : '';
      $suffix .= '</div>';
    }
    else {
      $prefix .= isset($element['#field_prefix']) ? $element['#field_prefix'] : '';
      $suffix .= isset($element['#field_suffix']) ? $element['#field_suffix'] : '';
    }
  }

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      if ($is_radio || $is_checkbox) {
        $output .= ' ' . $prefix . $element['#children'] . $suffix;
      }
      else {
        $variables['#children'] = ' ' . $prefix . $element['#children'] . $suffix;
      }
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if ($description && !$tooltip) {
    if (isset($element['#id']) && $element['#id'] == 'edit-name') {
      $output .= '<p class="help-block">Must be a valid email address.</p>';
    } elseif (isset($element['#id']) && $element['#id'] == 'edit-mail') {
      $output .= '<p class="help-block">Must be a valid e‐mail address. Your e‐mail will not be made public and will only be used if you wish to receive a new password or agree to receive certain news or notifications.</p>';
    } elseif (isset($element['#id']) && $element['#id'] == 'edit-current-pass') {
      $output .= '<p class="help-block">Please provide your current password to save
these changes. <a href="/user/password">Forgot password?</a></p>';
    } else {
      $output .= '<p class="help-block">' . $element['#description'] . "</p>\n";
    }
  }


  $output .= "</div></div>\n";

  return $output;
}


/**
 * Implements hook_form_alter().
 */
function bootstrap_rcpar_form_alter(array &$form, array &$form_state = array(), $form_id = NULL) {
  if ($form_id) {
    // IDs of forms that should be ignored. Make this configurable?
    // @todo is this still needed?
    $form_ids = array(
      'node_form',
      'system_site_information_settings',
      'user_profile_form',
      'node_delete_confirm',
    );
    // Only wrap in container for certain form.
    if (!in_array($form_id, $form_ids) && !isset($form['#node_edit_form']) && isset($form['actions']) && isset($form['actions']['#type']) && ($form['actions']['#type'] == 'actions')) {
      $form['actions']['#theme_wrappers'] = array();
    }
    switch ($form_id) {
      case 'system_theme_settings':
        // Include the settings form here.
        bootstrap_include('bootstrap', 'theme/settings.inc');
        //_bootstrap_settings_form($form, $form_state);
        break;

      case 'search_form':
        // Add a clearfix class so the results don't overflow onto the form.
        $form['#attributes']['class'][] = 'clearfix';

        // Remove container-inline from the container classes.
        $form['basic']['#attributes']['class'] = array();

        // Hide the default button from display.
        $form['basic']['submit']['#attributes']['class'][] = 'element-invisible';

        // Implement a theme wrapper to add a submit button containing a search
        // icon directly after the input element.
        $form['basic']['keys']['#theme_wrappers'] = array('bootstrap_search_form_wrapper');
        $form['basic']['keys']['#title'] = '';
        $form['basic']['keys']['#attributes']['placeholder'] = t('Search');
        break;

      case 'search_block_form':
        $form['#attributes']['class'][] = 'form-search';

        $form['search_block_form']['#title'] = '';
        $form['search_block_form']['#attributes']['placeholder'] = t('Search our site');

        // Hide the default button from display and implement a theme wrapper
        // to add a submit button containing a search icon directly after the
        // input element.
        $form['actions']['submit']['#attributes']['class'][] = 'element-invisible';
        $form['search_block_form']['#theme_wrappers'] = array('bootstrap_search_form_wrapper');

        // Apply a clearfix so the results don't overflow onto the form.
        $form['#attributes']['class'][] = 'content-search';
        break;

      case 'apachesolr_search_custom_page_search_form':
        if ($form['#search_page']['page_id'] == 'questions_answers') {
          $form['#attributes']['class'][] = 'qa-search-form';
          $form['basic']['keys']['#attributes']['placeholder'] = t('Have a specific topic?');
          $form['basic']['submit']['#value'] = '<i class="icon glyphicon glyphicon-search" aria-hidden="true"></i>';
        }
        break;
      // add unique class to register form
      case 'user_register_form':
        $form['#attributes']['class'][] = 'register-form';
        break;
      // add unique class to profile form
      case 'user_profile_form':
        $form['#attributes']['class'][] = 'profile-form';
        break;
    }

  }
}


/**
 * Themes an empty shopping cart block's contents.
 */
function bootstrap_rcpar_commerce_cart_empty_block() {
  return '<div class="view-header"><p><a id="close-cart"><i class="fa fa-close"></i></a></p></div><div class="view-content"><div class="cart-empty-block">' . t('Your shopping cart is empty.') . '</div></div><div class="view-footer"><div class="line-item-summary"><ul class="links list-inline"><li class="line-item-summary-checkout last"><a href="/cpa-courses" rel="nofollow">Continue Shopping</a></li></ul></div></div>';
}

/**
 * Themes an empty shopping cart page.
 */
function bootstrap_rcpar_commerce_cart_empty_page() {
  $output  = '<div class="cart-empty-page">';
  $output .= '<div class="empty-cart-heading"><div class="row">';
  $output .= '<div class="col-sm-4">PRODUCT</div>';
  $output .= '<div class="col-sm-4">DESCRIPTION</div>';
  $output .= '<div class="col-sm-4">PRICE</div>';
  $output .= '</div><!-- /row--> </div> <!-- /empty-cart-heading-->';
  $output .= '<div class="cart-empty-copy">';
  $output .= t('Your Cart is empty.');
  $output .= '<div class="cont-shopping"><a href="/cpa-courses" rel="nofollow">Continue Shopping</a></div>';
  $output .= '</div> <!-- /cart-empty-copy-->';
  $output .= '</div> <!-- /cart-empty-page-->';

  return $output;
}

/**
 * Theme callback for a normal meta tag.
 *
 * The format is:
 * <meta name="[name]" content="[value]" />
 */
function bootstrap_rcpar_metatag($variables) {
  $element = &$variables['element'];
  $args = array(
    '#name'  => 'name',
    '#value' => 'content',
  );
  element_set_attributes($element, $args);
  unset($element['#value']);

  // Adding this to truncate description metatag to 160 characters.
  // Mainly in use for Dictionary taxonomy terms, which is using a custom field
  // for the description metatag--the term definiton, which can be quite long
  if ($variables['element']['#name']=='description') {
    $description = $variables['element']['#attributes']['content'];
    if(strlen($description) > 160) {
      $description = wordwrap($description, 160);
      $description = explode("\n", $description, 2);
      $description = $description[0] . '...';
      $variables['element']['#attributes']['content'] = $description;
    }
  }

  return theme('html_tag', $variables);
}

function bootstrap_rcpar_preprocess_node(&$variables, $hook) {
  // This is a hack to allow custom tokens to be used in page builder nodes.
  if ($variables['node']->type=='cb_drag_drop_page' || $variables['node']->type=='blog') {
    global $user;
    if (!user_access('edit via glazed builder')) {
      if ($variables['node']->type=='cb_drag_drop_page') {
        $body_markup = $variables['content']['cb_body'][0]['#markup'];
        $body_markup = str_replace('%5B', '[', $body_markup);
        $body_markup = str_replace('%5D', ']', $body_markup);
        $variables['content']['cb_body'][0]['#markup'] = check_markup($body_markup, 'full_html_with_tokens');
      }
      if ($variables['node']->type=='blog') {
        $body_markup = $variables['content']['body'][0]['#markup'];
        $body_markup = str_replace('%5B', '[', $body_markup);
        $body_markup = str_replace('%5D', ']', $body_markup);
        $variables['content']['body'][0]['#markup'] = check_markup($body_markup, 'full_html_with_tokens');
        $teaser_markup = $variables['content']['field_teaser'][0]['#markup'];
        $teaser_markup = str_replace('%5B', '[', $teaser_markup);
        $teaser_markup = str_replace('%5D', ']', $teaser_markup);
        $variables['content']['field_teaser'][0]['#markup'] = check_markup($teaser_markup, 'full_html_with_tokens');
      }
    }
  }
  $view_mode = $variables['view_mode'];
  $content_type = $variables['type'];
  $variables['theme_hook_suggestions'][] = 'node__' . $view_mode;
  $variables['theme_hook_suggestions'][] = 'node__' . $view_mode . '_' . $content_type;

  $view_mode_preprocess = 'bootstrap_rcpar_preprocess_node_' . $view_mode . '_' . $content_type;
  if (function_exists($view_mode_preprocess)) {
    $view_mode_preprocess($variables, $hook);
  }

  $view_mode_preprocess = 'bootstrap_rcpar_preprocess_node_' . $view_mode;
  if (function_exists($view_mode_preprocess)) {
    $view_mode_preprocess($variables, $hook);
  }
  // taxonomy term node template suggestion
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $variables['theme_hook_suggestions'][] = 'node__' . 'term' . '_' . $content_type;
  }
}

/**
 * Brief message to display when no results match the query.
 *
 * @see search_help()
 */
function bootstrap_rcpar_apachesolr_search_noresults() {
  if (!drupal_match_path(current_path(), 'customer-care/*/search')) {
    return t('<h2 class="search-results-heading">Search results</h2><div class="solr-search-no-results">There are no results matching the selected search criteria.</div>');
  }
}


/**
 * Implements theme_file_upload_help().
 * Overrides bootstrap popover theming to fix weird behavior
 */
function bootstrap_rcpar_file_upload_help($variables) {
  // If popover's are disabled, just theme this normally.
  if (!bootstrap_setting('popover_enabled')) {
    return theme_file_upload_help($variables);
  }

  $build = array();
  if (!empty($variables['description'])) {
    $build['description'] = array(
      '#markup' => $variables['description'] . '<br>',
    );
  }

  $descriptions = array();
  $upload_validators = $variables['upload_validators'];
  if (isset($upload_validators['file_validate_size'])) {
    $descriptions[] = t('Files must be less than !size.', array('!size' => '<strong>' . format_size($upload_validators['file_validate_size'][0]) . '</strong>'));
  }
  if (isset($upload_validators['file_validate_extensions'])) {
    $descriptions[] = t('Allowed file types: !extensions.', array('!extensions' => '<strong>' . check_plain($upload_validators['file_validate_extensions'][0]) . '</strong>'));
  }
  if (isset($upload_validators['file_validate_image_resolution'])) {
    $max = $upload_validators['file_validate_image_resolution'][0];
    $min = $upload_validators['file_validate_image_resolution'][1];
    if ($min && $max && $min == $max) {
      $descriptions[] = t('Images must be exactly !size pixels.', array('!size' => '<strong>' . $max . '</strong>'));
    }
    elseif ($min && $max) {
      $descriptions[] = t('Images must be between !min and !max pixels.', array('!min' => '<strong>' . $min . '</strong>', '!max' => '<strong>' . $max . '</strong>'));
    }
    elseif ($min) {
      $descriptions[] = t('Images must be larger than !min pixels.', array('!min' => '<strong>' . $min . '</strong>'));
    }
    elseif ($max) {
      $descriptions[] = t('Images must be smaller than !max pixels.', array('!max' => '<strong>' . $max . '</strong>'));
    }
  }

  if ($descriptions) {
    $id = drupal_html_id('upload-instructions');
    $build['instructions'] = array(
      '#theme' => 'link__file_upload_requirements',
      // @todo remove space between icon/text and fix via styling.
      '#text' => _bootstrap_icon('question-sign') . ' ' . t('More information'),
      '#path' => '#',
      '#options' => array(
        'attributes' => array(
          'data-toggle' => 'popover',
          'data-target' => "#$id",
          'data-html' => TRUE,
          'data-placement' => 'bottom',
          'data-trigger' => 'focus',
          'data-title' => t('File requirements'),
        ),
        'html' => TRUE,
        'external' => TRUE,
      ),
    );
    $build['requirements'] = array(
      '#theme_wrappers' => array('container__file_upload_requirements'),
      '#attributes' => array(
        'id' => $id,
        'class' => array('element-invisible', 'help-block'),
      ),
    );
    $build['requirements']['validators'] = array(
      '#theme' => 'item_list__file_upload_requirements',
      '#items' => $descriptions,
    );
  }
  return drupal_render($build);
}

/**
 * Theme the username title of the user login form
 * and the user login block.
 */
function bootstrap_rcpar_lt_username_title($variables) {
  switch ($variables['form_id']) {
    case 'user_login':
      return t('E-mail address');
      break;
    
    case 'user_login_block':
      // Label text for the username field when shown in a block.
      return t('E-mail address');
      break;
  }
}

/**
 * Implements hook_css_alter().
 * Load local version of bootstrap CSS.
 */
function bootstrap_rcpar_css_alter(&$css) {
  // Overriding the cdn bootstrap css to instead use a local version of the file.
  $key = '';
  // If the cdn is unreachable, the default bootstrap version is loaded.
  if (array_key_exists("//cdn.jsdelivr.net/bootstrap/3.3.5/css/bootstrap.min.css",$css)) {
    $key = "//cdn.jsdelivr.net/bootstrap/3.3.5/css/bootstrap.min.css";
  }
  // If the cdn is working, the version we have selected in the theme settings is loaded.
  if (array_key_exists("//cdn.jsdelivr.net/bootstrap/3.0.2/css/bootstrap.min.css",$css)) {
    $key = "//cdn.jsdelivr.net/bootstrap/3.0.2/css/bootstrap.min.css";
  }
  // Replace either.
  if (!empty($key)) {
    $css_replace = array();
    $css_replace = $css[$key];
    $css_replace["data"] = drupal_get_path('theme', 'bootstrap_rcpar') . '/css/bootstrap/bootstrap.min.css';
    unset($css[$key]);
    $css[] = $css_replace;
  }
}
/**
 * Implements hook_js_alter().
 *  Load local version of bootstrap JS.
 */
function bootstrap_rcpar_js_alter(&$js) {
  // Overriding the cdn bootstrap js to instead use a local version of the file.
  $key = '';
  // If the cdn is unreachable, the default bootstrap version is loaded.
  if (array_key_exists("//cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js",$js)) {
    $key = "//cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js";
  }
  // If the cdn is working, the version we have selected in the theme settings is loaded.
  if (array_key_exists("//cdn.jsdelivr.net/bootstrap/3.0.2/js/bootstrap.min.js",$js)) {
    $key = "//cdn.jsdelivr.net/bootstrap/3.0.2/js/bootstrap.min.js";
  }
  // Replace either.
  if (!empty($key)) {
    $js_replace = array();
    $js_replace = $js[$key];
    $js_replace["data"] = drupal_get_path('theme', 'bootstrap_rcpar') . '/js/bootstrap/bootstrap.min.js';
    unset($js_replace['type']);
    unset($js[$key]);
    $js_replace['preprocess'] = FALSE;
    $js[] = $js_replace;
  }
}