<?php

// Don't allow opn access to update.php
$update_free_access = FALSE;

// Salt for one-time login links and cancel links, form tokens, etc.
$drupal_hash_salt = '-i93OeRMQIjZkCffWr4PpwgaIjz_TvE7-D6NISeddHY';

/**
 * PHP settings.
 *
 * The following ini_set calls come from Drupal core's standard settings.php
 * configuration.
 *
 * Some distributions of Linux (most notably Debian) ship their PHP
 * installations with garbage collection (gc) disabled. Since Drupal depends on
 * PHP's garbage collection for clearing sessions, ensure that garbage
 * collection occurs by using the most common settings.
 */
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);

/**
 * Set session lifetime (in seconds), i.e. the time from the user's last visit
 * to the active session may be deleted by the session garbage collector. When
 * a session is deleted, authenticated users are logged out, and the contents
 * of the user's $_SESSION variable is discarded.
 */
ini_set('session.gc_maxlifetime', 200000);

/**
 * Set session cookie lifetime (in seconds), i.e. the time from the session is
 * created to the cookie expires, i.e. when the browser is expected to discard
 * the cookie. The value 0 means "until the browser is closed".
 */
ini_set('session.cookie_lifetime', 2000000);

// Drupal Core fast 404 settings - @todo: since we're using fast_404.module, these are redundant/inert
$conf['404_fast_paths_exclude'] = '/\/(?:styles)\//';
$conf['404_fast_paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

/**
 * Memcache settings
 * https://docs.acquia.com/acquia-cloud/performance/memcached
 */
$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
$conf['cache_default_class'] = 'MemCacheDrupal';
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache'; // cache_form should be in non-volatile storage
$conf['cache_class_cache_smartpath'] = 'DrupalDatabaseCache'; // cache_smartpath should be in non-volatile storage
$conf['memcache_key_prefix'] = 'rogercpa';

// Add in stampede protection - recommendation from Acquia: https://insight.acquia.com/support/tickets/617515 **/
$conf['memcache_stampede_protection'] = TRUE;

// Per memcache readme and https://www.drupal.org/node/2419757, stampede protection on some cache bins
// should be ignored
$conf['memcache_stampede_protection_ignore'] = array(
  // Ignore some cids in 'cache_bootstrap'.
  'cache_bootstrap' => array(
    'module_implements',
    'variables',
    'lookup_cache',
    'schema:runtime:*',
    'theme_registry:runtime:*',
    '_drupal_file_scan_cache',
  ),
  // Ignore all cids in the 'cache' bin starting with 'i18n:string:'
  'cache' => array(
    'i18n:string:*',
  ),
  // Disable stampede protection for these entire bins:
  'cache_path',
  'cache_rules',
  'cache_views',
);

# Move semaphore out of the database and into memory for performance purposes
$conf['lock_inc'] = './sites/all/modules/contrib/memcache/memcache-lock.inc';
/***********************************/

// this seems to be managed by Acquia
// $cookie_domain = '.rogercpareview.com';

// We need to ensure that we do NOT enable the Shield module on production.
// Recommended code for settings file per acquia docs 
// https://docs.acquia.com/articles/password-protect-your-non-production-environments-acquia-hosting
if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  switch ($_ENV['AH_SITE_ENVIRONMENT']) {
    case 'prod':
      // Enable shield module for this page process-replacement-products
      // See DEV-415.
      if(preg_match("/^\/cpa-review-course\/process-replacement-products/", $_SERVER['REQUEST_URI'])) {
        $conf['shield_print'] = '';
        $conf['shield_user'] = 'customercare';
        $conf['shield_pass'] = 'CPAlive2018'; 
      } else {
        // Disable Shield on prod by setting the shield_user variable to NULL        
        $conf['shield_user'] = NULL;  
      }
      break;
    case 'integrate':
    default:
      // If we are not in the production environment, enable RCPAR Devel module
      include_once("sites/all/modules/custom/rcpar_devel/rcpar_devel.inc");
      break;
  }
}
else {
  // Non-acquia - custom environment. Enable RCPAR Devel module
  include_once("sites/all/modules/custom/rcpar_devel/rcpar_devel.inc");
}

/**
 * Fast 404 settings:
 */
// This path may need to be changed if the fast 404 module is in a different location.
include_once('./sites/all/modules/contrib/fast_404/fast_404.inc');

# Disallowed extensions. Any extension in here will not be served by Drupal and
# will get a fast 404.
$conf['fast_404_exts'] = '/^(?!robots).*\.(txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp|svg)$/i';

# Array of whitelisted URL fragment strings that conflict with fast_404.
$conf['fast_404_string_whitelisting'] = array('cdn/farfuture', '/advagg_', 'system/files');

# Default fast 404 error message.
$conf['fast_404_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

# Call the extension checking now. This will skip any logging of 404s.
fast_404_ext_check();

// Memory overrides. These settings were previously in a custom config file for Acquia production environment
// (/mnt/gfs/home/rogercpa/prod/config//mnt/gfs/home/rogercpa/prod/config/rogercpa_custom_settings.inc)
// Adding them here now so that they can be inherited for all environments
// admin pages
if (isset($_GET['q']) && strpos($_GET['q'], 'admin') === 0) {
  ini_set('memory_limit', '512M');
}
// checkout/*/payment
if (isset($_GET['q']) && strpos($_GET['q'], 'checkout') == 0) {
  ini_set('memory_limit', '512M');
}

// path to our local dev file
$conf['local_dev_settings_file'] = implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'settings.local.php'));

// Acquia settings (handles database, cookie etc.)
// RCPAR Custom settings if they exist.
if (file_exists('/home/rogercpa/prod/config/rogercpa_custom_settings.inc')) {
  // This file sets database credentials, MySQLs 'tx_isolation' setting, defines the
  // 'testcenter' DB connection (now deprecated, should probably be removed) and sets
  // memory limits for the admin/* and checkout/* pages to 512mb
  require '/home/rogercpa/prod/config/rogercpa_custom_settings.inc';
}
elseif (file_exists('/var/www/site-php')) {
  require '/var/www/site-php/rogercpa/rogercpa-settings.inc';
}
elseif (file_exists($conf['local_dev_settings_file'])) {
  require $conf['local_dev_settings_file'];
}

// Configure our temporary directory to be in a mount location that is shared across all webservers
if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  $conf['file_temporary_path'] = "/mnt/gfs/{$_ENV['AH_SITE_GROUP']}.{$_ENV['AH_SITE_ENVIRONMENT']}/tmp";
}

/**
 * Set the 'composer_manager_vendor_dir' configuration variable in code to prevent an odd but potentially
 * fatal error with the composer autoloader.
 *
 * Although the 'composer_manager_vendor_dir' is configurable at /admin/config/system/composer-manager/settings,
 * we set it explicitly here in code in order to prevent an odd error that we experienced in production:
 * "RuntimeException: Autoloader not found: /sites/all/libraries/composer/autoload.php in
 * composer_manager_register_autoloader()"
 *
 * The error appears to have occurred when a DB performance issue caused composer_manager_beta5_compatibility()
 * to be unable to load a value for the vendor dir, which in turns causes the function to set a new default
 * value. The new value does not point to the location of our autoloader file, and fatal PHP errors start
 * happening all across the site. Defining the variable in code will prevent this value from ever being reset
 * in the event that further DB performance issues arise.
 * @see composer_manager_beta5_compatibility()
 */
$conf['composer_manager_vendor_dir'] = 'sites/all/vendor';

// Disable Shield for CrossBrowserTesting's test bot - http authentication causes problems for the automated
// tests. NOTE: It is vitally important that this code be below the inclusion of the Acquia settings file,
// otherwise the IP address of the reverse proxy will be cached because ip_address() does not yet know we
// are behind a reverse proxy
if(ip_address() == '216.37.72.238') {
  $conf['shield_user'] = NULL;
}

// This is the error reporting level on production - set explicitly here so that it can be inherited to
// other dev environments. If we wanted to change the error reporting level on prod, it would also need
// to be done here since this line will override anything included above.
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);