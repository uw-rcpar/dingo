; Specify the version of Drupal being used.
core = 7.x
; Specify the api version of Drush Make.
api = 2
defaults[projects][subdir] = contrib
;applying patch https://www.drupal.org/project/salesforce/issues/2792309#comment-13262387
;we are trying to avoid the size of the version table on salesforce since is too large.
; with this patch we can do that as well as limit the number in the future.
projects[salesforce][version] = 3.3-beta3
projects[salesforce][patch][] = https://www.drupal.org/files/issues/2019-10-24/salesforce_mapping_prune_revisions-2792309-20.patch