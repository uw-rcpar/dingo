<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
        "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html lang="en" dir="ltr">
  <head profile="http://www.w3.org/1999/xhtml/vocab">
    <title>Roger CPA Review</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Google Optimize Page-hiding snippet -->
    <style>.async-hide { opacity: 0 !important} </style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
        h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
        (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
      })(window,document.documentElement,'async-hide','dataLayer',4000,
        {'GTM-PL6CRTD':true});</script>
    <!-- End Google Optimize Page-hiding snippet -->

    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-N4KZWH');</script>
    <!-- End Google Tag Manager -->


    <!-- Google Universal Analytics code with Optimize plugin -->

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-4923653-1', {"cookieDomain":"auto"});
      ga('require', 'GTM-PL6CRTD');
      // user login status
      // ga("set", "dimension1", "Logged-in");
      // user role
      // ga("set", "dimension2", "");
    </script>
    <!-- End Google Universal Analytics code -->


    <script type="text/javascript" src="//code.jquery.com/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="/sites/all/themes/bootstrap_rcpar/js/QueryString.js"></script>
    <script type="text/javascript">

      var RCPARMB = (function() {
        // locally scoped Object
        var self = {};

        /**
         * Make a request to the API and return data
         * @param method
         * @param url
         * @param data
         * @param callback
         */
        self.request = function(method, url, data, callback) {
          $.ajax({
            type: method,
            url: url,
            data: data,
            dataType: 'json',
            success: function(data, textStatus, jqXHR) {
              // This is where our data will be returned.
              callback(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
              // Build a custom data object for our callback so it can interpret this in a way it expects
              var data = {
                code: 0, // Dummy code, doesn't really matter
                error: textStatus,
                message: 'Sorry, we encountered an unexpected error: ' + errorThrown
              };
              callback(data);
            }
          });
        };

        /**
         * Get mobile content.
         * @todo - document parameters
         */
        self.getMobileContent = function(callback, alias) {
          self.request('POST', '/api/mb/' + alias, {}, function(data) {
            callback(data);
          });
        };

        /**
         * Get product access
         * @todo - document parameters
         */
        self.getProductAccess = function(callback, uid, section, type) {
          // Define URL params to pass to the api request
          var data = {
            uid: uid,
            section: section,
            type: type
          };

          self.request('POST', '/api/user/productaccess', data, function(data) {
            callback(data);
          });
        };

        return self;

      })();

      /**
       * Main execution function
       */
      function main() {
        // Get the last part of the URL as the product type
        var pathParts = window.location.pathname.split('/');
        var productType = pathParts.pop();

        var uid = $.QueryString['uid'];
        var section = $.QueryString['section'];

        // Initialize data with a default error state
        data = {};
        data.error = 'Not found';
        data.message = 'Not found';

        linkAlteration = '';
        // This is a CRAM course that needs to be activated
        switch(productType) {
          // STANDARD REVIEW COURSE
          case 'review-course':
            // Determine the status of this product by asking the API
            RCPARMB.getProductAccess(function(data) {
              if(data.code != 200) {
                handleErrors(data);
                return;
              }

              // Check the status of this review course to decide which content to show
              switch(data.status) {
                // The user had this review course, but it expired
                case 'expired': this.getPageAndRender('course-expired', linkAlteration); return;

                // The user has never purchased this review course
                case 'unowned':
                  // If the user has no other review courses, we will need to alter the link on the page.
                  if (data.entitlements.length > 0) {
                    linkAlteration  = '/select';
                  }
                  this.getPageAndRender('course-unowned', linkAlteration);
                  return;
              }

              // Don't have a page for this status
              handleErrors(data);
              return;

            }, uid, section, productType);
            return;

          // CRAM COURSE
          case 'cram-course':
            // Determine the status of this product by asking the API
            RCPARMB.getProductAccess(function(data) {
              if(data.code != 200) {
                handleErrors(data);
                return;
              }

              switch(data.status) {
                // The user had this review course, but it expired
                case 'expired': this.getPageAndRender('cram-course-expired', linkAlteration); return;

                // The user has never purchased this review course
                case 'unowned': this.getPageAndRender('cram-course-unowned', linkAlteration); return;
              }

              // Don't have a page for this status
              handleErrors(data);
              return;

            }, uid, section, productType);
            return;

          // UNACTIVATED CRAM COURSE
          case 'cram-course-activate': this.getPageAndRender('cram-course-activate', linkAlteration); return;

          // NO DOWNLOAD ACCESS
          case 'downloads': this.getPageAndRender('downloads', linkAlteration); return;
        }

        // If we ended up here somehow, there must have been a weird product type passed in
        // to the URL. Trigger an error state.
        handleErrors(data);
      }

      /**
       * Get mobile content for the given URL alias and render it in the page regions
       */
      function getPageAndRender(alias, linkAlteration) {
        RCPARMB.getMobileContent(function(data){
          if(data.code == 200) {
            // no api errors, show content
            $('#title').html(data.title);
            $('#body').html(data.body);
            $('#region-top').html(data.regionTop);
            $('#region-middle').html(data.regionMiddle);
            $('#region-bottom').html(data.regionBottom);
            // Remove loading class from body tag. This is used to display initial "loading" dummy content.
            $('body').removeClass('loading');
            // Add the user id as a querystring to the link on the page for tracking purposes.
            oldUrl = ($('.btn a').attr('href'));
            queryStringDelimiter = '?';
            if(oldUrl.indexOf('?') != -1){
              queryStringDelimiter = '&';
            }
            newUrl = oldUrl + linkAlteration + queryStringDelimiter + 'section=' + $.QueryString['section'] + '&uid=' + $.QueryString['uid'];
            $('.btn a').attr('href', newUrl);
          }
          else {
            handleErrors(data);
          }
        }, alias);
      }

      /**
       * Handle errors that return from API callbacks and display something meaningful to the user
       * @param data
       */
      function handleErrors(data) {
        // Specific error messaging - content not found.
        if(data.error == 'Not found') {
          $('#body').html('<div class="message error">' + data.message + '</div>');
          $('#title').html('');
          $('#region-top').html('');
          $('#region-middle').html('');
          $('#region-bottom').html('');
          $('body').removeClass('loading');
        }
        else {
          // Anything else would be like an unexpected API error, site crashed, etc.
          $('#body').html('<div class="message error">Error: ' + data.message + '</div>');
          $('#title').html('');
          $('#region-top').html('');
          $('#region-middle').html('');
          $('#region-bottom').html('');
          $('body').removeClass('loading');
        }
      }
    </script>

    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,500,500italic,700,700italic&amp;subset=latin,cyrillic-ext" media="all" />
    <style>
      @import url("/sites/all/themes/bootstrap_rcpar/fonts/font-awesome/css/font-awesome.min.css?p9yoez");
    </style>
    <style>
      html {
        font-family: sans-serif;
        -webkit-text-size-adjust: 100%;
      }
      html {
        font-size: 62.5%;
      }
      body {
        margin: 0;
        font-family: "Open Sans", "Arial", "Helvetica", sans-serif;
        font-size: 18px;
      }
      p {
        margin: 0;
        padding: 10px 0 10px;
      }
      p.no-space{
        padding-top: 0;
      }
      a,
      a:visited {
        color: #2daade;
        text-decoration: none;
      }
      .btn a,
      .btn a:visited {
        color: #fff;
        background-color: #64bb47;
        display: block;
        font-size: 16px;
        text-transform: uppercase;
        font-weight: bold;
        padding: 14px 15px;
        vertical-align: middle;
        text-align: center;
        white-space: nowrap;
        letter-spacing: 3px;
        min-width: 200px;
        width: 50%;
        margin-right: auto;
        margin-left: auto;
      }

      #body {
        background-color: #ffffff;
        font-size: 24px;
        margin: 0;
        color: #ffffff;
        font-weight: 500;
        text-align: center;
        background-image: url('/sites/default/files/images/mb-sky-bg.svg');
        background-repeat: no-repeat;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        padding: 2%;
      }
      .loading #body {
        background-image: none;
        padding-top: 14%;
      }
      .loading #region-middle,
      .loading .icon-products {
        display: none;
      }
      .loading .btn a {
        color: #46c1f4;
        opacity: .2;
      }
      .loading #region-bottom {
        padding-top: 7%;
      }
      @keyframes placeHolderShimmer{
        0%{
          background-position: -468px 0
        }
        100%{
          background-position: 468px 0
        }
      }
      .animate {
        animation-duration: 1s;
        animation-fill-mode: forwards;
        animation-iteration-count: infinite;
        animation-name: placeHolderShimmer;
        animation-timing-function: linear;
        background: #f6f7f8;
        background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
        background-size: 800px 104px;
        width: 275px;
        height: 24px;
        margin: 10px auto 10px auto;
      }
      .larger {
        width: 230px;
        height: 51px;
      }
      #region-top {
        color: #000;
        background-color: #fff;
        font-size: 18px;
        text-align: center;
        padding: 2%;
      }
      #region-middle {
        color: #000;
        background-color: #fff;
        font-size: 18px;
        text-align: center;
        padding: 2% 2% 0 2%;
      }
      #region-bottom {
        color: #9f9f9e;
        background-color: #fff;
        font-size: 16px;
        text-align: center;
        padding: 0 2% 2% 2%;
      }
      .icon {
        width: 25%;
        height: auto;
        margin-left: auto;
        margin-right: auto;
      }
      /* media queries */
      /* iphone 5 */
      @media only screen and (min-width : 320px) {
        .icon {
          width: 32%;
        }
        #body,
        #region-top {
          padding: 7%;
        }
        #region-middle {
          padding-top: 0;
        }
        .loading #region-bottom {
          padding-top: 7%;
        }
      }
      /* iphone 6-7-8 */
      @media only screen and (min-width : 375px) {
        .icon {
          width: 32%;
        }
        #body,
        #region-top {
          padding: 10%;
        }
        #region-middle {
          padding-top: 0;
        }
        .loading #region-bottom {
          padding-top: 7%;
        }
      }
      /* iphone X */
      @media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 3) {
        .icon {
          width: 32%;
        }
        #body,
        #region-top {
          padding: 12% 7%;
        }
        #region-middle {
          padding-top: 0;
        }
        .loading #region-bottom {
          padding-top: 7%;
        }
      }
      /* iphone 7 plus */
      @media only screen and (min-width : 414px) {
        #body,
        #region-top {
          padding: 12%;
        }
        #region-middle {
          padding-top: 0;
        }
        .loading #region-bottom {
          padding-top: 7%;
        }
      }
      /* ipad mini */
      @media only screen and (min-width : 768px) {
        #body,
        #region-top {
          padding: 7%;
          font-size: 32px;
        }
        #region-middle {
          padding-top: 0;
          font-size: 32px;
        }
        .btn a,
        .btn a:visited {
          font-size: 34px;
        }
        #region-bottom {
          font-size: 22px;
        }
        .loading #region-bottom {
          padding-top: 7%;
        }
      }
      /* ipad regular */
      @media only screen and (min-width : 834px) {
        #body,
        #region-top {
          padding: 12%;
        }
        #region-middle {
          padding-top: 0;
        }
        .loading #region-bottom {
          padding-top: 7%;
        }
      }
      /* ipad pro */
      @media only screen and (min-width : 1024px) {
        #body,
        #region-top {
          padding: 12%;
          font-size: 46px;
        }
        #region-middle {
          padding-top: 0;
        }
        .btn a,
        .btn a:visited {
          font-size: 34px;
        }
        #region-bottom {
          font-size: 22px;
        }
        .loading #region-bottom {
          padding-top: 7%;
        }
      }
    </style>
  </head>
  <body onLoad="main();" class="loading">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N4KZWH"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
    <div id="body">
      <div class="animate"></div>
      <div class="animate"></div>
      <div class="icon icon-products"><img src="/sites/default/files/images/icon-mb-loading.svg" /></div>
      <div class="animate"></div>
    </div>

    <div id="region-top">
      <div class="animate"></div>
      <div class="animate"></div>
    </div>
    <div id="region-middle"><div class="btn"><a href="/cpa-courses/select">SHOP NOW</a></div></div>
    <div id="region-bottom">
      <div class="animate larger"></div>
    </div>
  </body>
</html>