<?php

namespace RCPAR;

use RCPAR\Entities\Notifications\Broadcast;
use RCPAR\Entities\Notifications\Channel;
use RCPAR\Entities\Notifications\Notification;
use RCPAR\Entities\Notifications\NotificationCollection;

class NotificationManager {
  protected $user;
  protected $channel;

  public function __construct(User $user, Channel $channel) {

    $this->user = $user;
    $this->channel = $channel;
  }

  /**
   * Causes notifications to be issued for the current user over the currently configured channel.
   * This essentially just causes records in the notifications table to be built, but does not return anything.
   */
  public function issueNotifications() {
    // Issue notifications from broadcasts
    $this->issueNotificationsFromBroadcasts(
      $this->getUsersActiveBroadcasts()
    );
  }

  /**
   * Get all notifications sent to the user regardless of status
   *
   * @param int $limit
   * - Limit the number returned - defaults to 20
   * @return NotificationCollection
   */
  public function getAll($limit = 20) {
    $query = db_select(Notification::$tableName, 't')
      ->fields('t')// All fields
      ->condition('t.uid', $this->getUser()->getUid())
      ->condition('t.broadcast_channel_id', $this->getChannel()->getId())
      ->condition('t.expire', REQUEST_TIME, '>')// Expire should be in the future - greater than now
      ->orderBy('t.created', 'DESC')
      ->range(0, $limit);
    $query->join('node', 'n', 'n.nid = t.notification_template_id'); // Ensure that the notification template hasn't been deleted by joining
    $query->condition('n.status', 1); // Don't return notifications for unpublished templates
    $notifs = $query->execute();

    // Build and return a collection
    $collection = new NotificationCollection();
    foreach ($notifs as $rowObj) {
      $collection->add(new Notification($rowObj));
    }

    return $collection;
  }

  /**
   * @param string $title
   * @param string $message
   * @param string $type
   * @param int $expiration
   * @return Notification
   */
  public function createNotification($title, $teaser, $message, $type, $expiration) {
    return Notification::createPrivateFromMessage($this->getUser(), $title, $teaser, $message, $type, $expiration);
  }

  /**
   * Get all active broadcasts that the user has an available notification for
   * @return Broadcast[]
   */
  protected function getUsersActiveBroadcasts() {
    $broadcasts = array();

    // The channel we're requesting has been designated in the constructor. Get it's active broadcasts,
    // and return every active broadcast that the user has not yet received a receipt for.
    foreach ($this->getChannel()->getActiveBroadcasts() as $broadcast) {
      // Don't add this broadcast if we've already added it on another channel
      if (isset($broadcasts[$broadcast->getId()])) {
        continue;
      }

      // Don't issue a receipt if we already have one
      if ($broadcast->hasReceipt($this->getUser())) {
        continue;
      }
      $broadcasts[$broadcast->getId()] = $broadcast;
    }

    return $broadcasts;
  }

  /**
   * @param Broadcast[] $broadcasts
   */
  protected function issueNotificationsFromBroadcasts($broadcasts) {
    foreach ($broadcasts as $broadcast) {
      // Check if this is the intended audience
      foreach ($broadcast->getAudience() as $audience) {
        if($this->userIsWithinAudience($audience)) {
          $broadcast->issueUnreadNotification($this->getUser());
          break;
        }
      }
    }
  }

  /**
   * Determine if the user is part of the given audience.
   *
   * @param string $audience
   * - A hardcoded value representing one of the available values in field_audience.
   * 'paid', 'fsl', or 'freetrial'.
   * @return bool
   * - True if the current user is a "member" of this audience.
   */
  protected function userIsWithinAudience($audience) {
    $ents = $this->getUser()->getValidEntitlements();

    switch ($audience) {
      case 'paid':
        if($ents->filterByPaid()->isNotEmpty()) {
          return TRUE;
        }
        break;

      case'fsl':
        if($ents->filterByPartnerType(firm_partner)->filterByBillingType('freetrial')->isNotEmpty()) {
          return TRUE;
        }
        break;

      case'freetrial':
        if ($ents->filterByFreeTrial()->isNotEmpty()) {
          return TRUE;
        }
        break;

      case'exp_unlimited':

        // If entitlements are expiring within 15 days show the notification
        if ($ents->filterByOnlineCourse()
          ->filterByUnlimitedAccess()
          ->filterByExpiringWithin("15 days")
          ->isNotEmpty()) {
          return true;
        }

        break;
    }

    return FALSE;
  }

  /**
   * Store unread, private notifications in our collection
   * @deprecated - We're not doing this right now
   * @throws \Exception
   */
  protected function issuePrivateNotifications() {
    $privateChannel = Channel::getPrivateChannel();

    $result = db_query("
      SELECT * FROM notif_notifications 
      WHERE uid = :uid AND broadcast_channel_id = :cid AND is_read = 0 
      AND (expire > :now OR expire = 0)", array(
        ':uid' => $this->getUser()->getUid(),
        ':cid' => $privateChannel->getId(),
        ':now' => REQUEST_TIME,
      )
    );

    // Build a notification from each record and add it to our collection
    $collection = new NotificationCollection();
    foreach ($result as $row) {
      $collection->add(new Notification($row));
    }
    return $collection;
  }

  /**
   * Find all unread notifications for this user and add them to our collection
   * @return NotificationCollection
   */
  public function getUnreadNotifications() {
    $result = db_query("
      SELECT * FROM notif_notifications 
      WHERE uid = :uid AND is_read = 0
      AND broadcast_channel_id = :channel 
      AND (expire > :now OR expire = 0)", array(
        ':uid' => $this->getUser()->getUid(),
        ':now' => REQUEST_TIME,
        ':channel' => $this->getChannel()->getId(),
      )
    );

    // Build a notification from each record and add it to our collection
    $collection = new NotificationCollection();
    foreach ($result as $row) {
      $collection->add(new Notification($row));
    }
    return $collection;
  }

  /**
   * @return User
   */
  protected function getUser() {
    return $this->user;
  }

  /**
   * @return Channel
   */
  protected function getChannel() {
    return $this->channel;
  }

}