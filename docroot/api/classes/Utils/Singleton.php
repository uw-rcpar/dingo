<?php

namespace RCPAR\Utils;

/**
 * Singleton class
 */
class Singleton {
  /**
   * Call this method to get singleton
   * @return self
   */
  public static function Instance() {
    static $inst = null;
    if ($inst === null) {
      $inst = new self();
    }
    return $inst;
  }

  /**
   * Extending class should define __construct() as private so nobody else can instantiate it,
   * but we don't make it private here otherwise it can't be extended
   */
  // private function __construct() {}
}