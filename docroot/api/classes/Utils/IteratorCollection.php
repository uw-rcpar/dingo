<?php

namespace RCPAR\Utils;
use \Iterator;

/**
 * Class IteratorCollection
 * A class for wrapping a custom collection that you can iterate on and use basic, array-like
 * methods for info and manipulation.
 * @package RCPAR\Utils
 */
class IteratorCollection implements Iterator {
  // array for this collection
  protected $collection = array();

  private $position = 0;

  public function __construct() {
    $this->position = 0;
  }

  public function rewind() {
    $this->position = 0;
  }

  public function current() {
    return $this->collection[$this->position];
  }

  public function key() {
    return $this->position;
  }

  public function next() {
    ++$this->position;
  }

  public function valid() {
    return isset($this->collection[$this->position]);
  }

  //////////// END iterator stuff ////////////////////

  /**
   * True when there are no items in the collection.
   * @return bool
   */
  public function isEmpty() {
    return empty($this->collection);
  }

  /**
   * True when there is at least one item in the collection.
   * @return bool
   */
  public function isNotEmpty() {
    return !$this->isEmpty();
  }

  /**
   * Returns the number of items in the collection
   * @return int
   */
  public function count() {
    return count($this->collection);
  }

  /**
   * Add an item to the collection
   */
  public function add($item) {
    $this->collection[] = $item;
  }

  /**
   * @return array
   */
  public function getCollection() {
    return $this->collection;
  }
}