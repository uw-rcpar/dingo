<?php

namespace RCPAR\Utils;
use \Flight;

class JSONResponse extends Singleton {
  protected $code = 200;

  // String - name of the error
  protected $error = FALSE;

  // String - message describing the error or request
  protected $message = '';

  protected $payload = array();

  /**
   * Call this method to get singleton
   * @return self
   */
  public static function Instance() {
    static $inst = null;
    if ($inst === null) {
      $inst = new JSONResponse();
      self::convertJsonBodyToRequestParams();
    }
    return $inst;
  }

  /**
   * As a simple and backwards compatible method to allow existing endpoints to work with parameters posted via
   * JSON payload in the request body, this method will look for the JSON data in the request body, decode it, and
   * transfer any values found into the equivalent GET, POST, and REQUEST super globals so that they can be read
   * by PHP without any additional effort
   */
  public static function convertJsonBodyToRequestParams() {
    $postBody = file_get_contents('php://input');
    if (!empty($postBody)) {
      $decoded = json_decode($postBody, TRUE);
      if(is_array($decoded)) {
        foreach ($decoded as $key => $value) {
          $_GET[$key] = $value;
          $_POST[$key] = $value;
          $_REQUEST[$key] = $value;
        }
      }
    }
  }

  public function addToPayload($key, $data) {
    $this->payload[$key] = $data;
  }

  public function setPayload($data, $merge = TRUE) {
    if($merge) {
      $this->payload = array_merge($this->payload, $data);
    }
    else {
      $this->payload = $data;
    }
  }

  public function send($returnOnly = FALSE) {
    $full_payload = array_merge($this->payload, array(
      'code' => $this->code,
      'error' => $this->error,
      'message' => $this->message,
    ));

    if($returnOnly) {
      return $full_payload;
    }
    Flight::json($full_payload);
  }



  /**
   * @param bool $error
   * @return JSONResponse
   */
  public function setError($error) {
    $this->error = $error;
    return $this;
  }

  /**
   * @param string $message
   * @return JSONResponse
   */
  public function setMessage($message) {
    $this->message = $message;
    return $this;
  }

  /**
   * @param int $code
   * @return JSONResponse
   */
  public function setCode($code) {
    $this->code = $code;
    return $this;
  }

  /**
   * @return bool|string
   */
  public function getError() {
    return $this->error;
  }

  /**
   * Set code, message, error and throw an exception.
   * @throws \Exception
   */
  public function unauthorizedAndDie() {
    $this->setCode(401)
      ->setMessage('Could not decode a valid JWT token')
      ->setError('Not authorized');
    throw new \Exception();
  }
}
