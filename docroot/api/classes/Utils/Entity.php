<?php

namespace RCPAR\Utils;

use \EntityMetadataWrapperException;

class Entity {
  protected $wrapper;
  protected $entityType = 'node';
  protected $entity;
  protected $isLoaded = FALSE;

  function __construct($entity = NULL) {
    $this->entity = $entity;
    $this->isLoaded = $this->setWrapper();
  }

  public function isLoaded() {
    return $this->isLoaded;
  }

  function getEntity() {
    return $this->getWrapper()->value();
  }

  /**
   * Sets an entity metadata wrapper for the user
   * @return bool
   * - True if set, false if there was a problem
   */
  function setWrapper() {
    try {
      $this->wrapper = entity_metadata_wrapper($this->entityType, $this->entity);
      return is_object($this->wrapper);
    }
    catch (EntityMetadataWrapperException $exc) {
      // Swallow the error
    }
    return FALSE;
  }

  /**
   * Get the entity metadata wrapper for this user.
   * If the wrapper has not been set yet, this function will set it first
   * @return \EntityDrupalWrapper
   */
  function getWrapper() {
    if(empty($this->wrapper)) {
      $this->setWrapper();
    }

    return $this->wrapper;
  }

  /**
   * Helper function to get the value of a field the entity wrapper.
   * This function allows us to avoid the boilerplate of checking for the wrapper, supplying a fallback default,
   * and handling EntityMetadataWrapperException exceptions.
   *
   * @param string $fieldname
   * - A field name on the node
   * @param bool $default
   * - Default value to return if there is any problem getting the value (an EntityMetadataWrapperException for example)
   * @return mixed
   * - Will return whatever value the field stores, which could be just about anything.
   */
  protected function getFieldValue($fieldname, $default = FALSE) {
    if ($this->getWrapper()) {
      try {
        return $this->getWrapper()->{$fieldname}->value();
      }
      catch (EntityMetadataWrapperException $e) {
        // Swallow error - we'll return the default
      }
    }
    return $default;
  }

}