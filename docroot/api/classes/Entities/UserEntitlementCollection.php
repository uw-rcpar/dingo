<?php

namespace RCPAR\Entities;

use Countable;
use RCPAR\Utils\IteratorCollection;

class UserEntitlementCollection extends IteratorCollection implements Countable {

  /**
   * Get the current item in the collection (Iterator method).
   * It's not necessary to implement this here since it's already in the RCPARIterator train,
   * but it's nice to have type-hinting so that foreach() code understands the type of variable
   * that is being iterated on.
   * @return UserEntitlement
   */
  public function current() {
    return parent::current();
  }

  /**
   * Gets the first entitlement in the collection. This is especially useful when you expect
   * that a collection contains only one entitlement.
   * @return UserEntitlement
   */
  public function getFirst() {
    $this->rewind();
    return $this->current();
  }

  /**
   * Returns a numeric array of UserEntitlement objects in the current collection
   * @return UserEntitlement[]
   */
  public function getEntitlements() {
    return $this->getCollection();
  }

  /**
   * Add an entitlement on to the collection. Will prevent duplicate entitlements from being added
   * if an entitlement with the same node ID is already in the collection.
   * @param UserEntitlement $e
   */
  public function addEntitlement(UserEntitlement $e) {
    // Add the entitlement to our collection, but only if we don't already have it
    if ($this->getEntitlementById($e->getNid()) === FALSE) {
      $this->add($e);
    }
  }

  /**
   * Remove entitlements from the collection that match any of the IDs given.
   * @param int[] $ids
   */
  public function removeEntitlementsByIds($ids = array()) {
    if (empty($ids)) {
      return $this;
    }

    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      // Keep it in the collection if it's not in the list that we're removing
      if (!in_array($e->getNid(), $ids)) {
        $filtered->add($e);
      }
    }
    $this->collection = $filtered->collection;
  }

  /**
   * Get an entitlement from the collection matching the given node id, or FALSE if not found.
   *
   * @param int $nid
   * - Node ID of an entitlement
   * @return bool|UserEntitlement
   */
  public function getEntitlementById($nid) {

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->getNid() == $nid) {
        return $e;
      }
    }

    return FALSE;
  }

  /**
   * * Get an entitlement from the collection matching the given SKU, or FALSE if not found.
   *
   * @param string $sku
   * - Product SKU of an entitlement
   * @return bool|UserEntitlement
   */
  public function getEntitlementBySku($sku) {

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->getProductSku() == $sku) {
        return $e;
      }
    }

    return FALSE;
  }

  /**
   * Returns an array containing all the node IDs of the entitlements in this collection.
   * @return int[]
   * - Array of node ids, keyed by node id
   */
  public function getIds() {
    $ids = array();

    /** @var UserEntitlement $e */
    foreach ($this->getEntitlements() as $e) {
      $ids[$e->getNid()] = $e->getNid();
    }

    return $ids;
  }

  /**
   * Returns the earliest creation date of UserEntitlement objects in the current collection
   * @return int
   */
  public function getEarliestCreatedDate() {
    $created_dates = array();

    /** @var UserEntitlement $e */
    foreach ($this->getEntitlements() as $e) {
      $created_dates[] = $e->getCreatedTime();
    }
    sort($created_dates);
    return $created_dates[0];
  }

  /**
   * Returns an array containing all the SKUs of the entitlements in this collection.
   * @return string[]
   */
  public function getSkus() {
    $skus = array();

    /** @var UserEntitlement $e */
    foreach ($this->getEntitlements() as $e) {
      $skus[$e->getProductSku()] = $e->getProductSku();
    }
    return $skus;
  }

  /**
   * Returns the current entitlement collection after filtering out any entitlements
   * that are not active.
   * @return UserEntitlementCollection
   */
  public function filterByActive() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->isActive()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   *  Identifies only the entitlements that have valid expiration dates. All others are consider invalid.
   * @return UserEntitlementCollection
   */
  public function filterByValid() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->isValid()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Identifies only the entitlements that have unlimited access. All others are consider invalid.
   * @return UserEntitlementCollection
   */
  public function filterByUnlimitedAccess() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->getUnlimitedAccess()) {
        $filtered->add($e);
      }
    }

    return $filtered;

  }

  /**
   * Compares expiration dates of entitlements with identical skus and returns
   * unique entitlements with the latest exp date
   * @return UserEntitlementCollection
   */
  public function filterByLatestExpirationDate() {
    $filtered = new self();
    foreach($this->getSkus() as $sku) { // Iterate over each of the unique SKUs in this collection
      $entsOfThisSku = $this->filterBySkus(array($sku)); // Get a collection of all entitlements of just this SKU
      $filtered->add($entsOfThisSku->getLastExpired()); // Get the entitlement object with the latest expiration among the whole collection
}
    return $filtered;
  }

  /**
   * Returns the current entitlement collection after filtering out all but expired entitlements.
   * An entitlement will not be considered expired if it is unpublished - the published status
   * will take precedence. An entitlement that is unpublished and expired is more like an entitlement
   * that you never had at all rather than one that you had and expired. An entitlement that is
   * not yet activated (e.g. CRAM) will not be considered expired, despite the fact that its expiration
   * date is in the past.
   * @return UserEntitlementCollection
   */
  public function filterByExpired() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if(!$e->isActive() && $e->isPublished()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Returns an entitlement collection that will expire within the given time range.
   * @param string $time
   *  - A date/time string. Valid formats are explained in https://www.php.net/manual/en/datetime.formats.php
   * @return UserEntitlementCollection
   */
  public function filterByExpiringWithin($time) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    $end_date = strtotime($time);

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if (($e->getExpirationDate() >= time() && $e->getExpirationDate() <= $end_date)) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Returns an entitlement collection where the expiration is within the given time range, relative to the operator.
   * In other words, filterByExpiration('+15days', '>') will return a collection where the expiration is greater than
   * 15 days from now.
   * @param int $timestamp
   *  - A UNIX timestamp
   * @param string $operator
   * - '<', '<=', '>', '>=', '==', '!='
   * @return UserEntitlementCollection
   */
  public function filterByExpiration($timestamp, $operator, $endTime = NULL) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($this->filterValByOperator($e->getExpirationDate(), $timestamp, $operator)) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }


  /**
   * Compare one value to another value with the given operator.
   *
   * @param int|float $val
   * @param int|float $compareTo
   * @param string $op
   * - '<', '<=', '>', '>=', '==', '!='
   * @return bool
   */
  protected function filterValByOperator($val, $compareTo, $op) {
    switch ($op) {
      case '<': return $val < $compareTo; break;
      case '<=': return $val <= $compareTo; break;
      case '>': return $val > $compareTo; break;
      case '>=': return $val >= $compareTo; break;
      case '==': return $val == $compareTo; break;
      case '!=': return $val != $compareTo; break;
    }

    // This should never happen, unless the user has passed in an invalid operator.
    return false;
  }


  /**
   * Returns a new collection with the entitlements from the given collection added to the current collection.
   * @param UserEntitlementCollection $c
   * @return $this
   */
  public function merge(UserEntitlementCollection $c) {
    /** @var UserEntitlement $e */
    foreach ($c->getEntitlements() as $e) {
      $this->add($e);
    }

    return $this;
  }

  /**
   * Remove all entitlements except those that match the provided SKUs
   *
   * @param string[] $skuList
   * - An array of product SKUs
   * @return UserEntitlementCollection
   */
  public function filterBySkus($skuList) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if(in_array($e->getProductSku(), $skuList)) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Removes all standard course entitlements (AUD, BEC, FAR, REG) that derive from an ACT partner.
   * @return UserEntitlementCollection
   */
  public function removeACTFromCollection() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($e->isOnlineCourseACT() || $e->isTypeACT()) {
        continue;
      }
      $filtered->add($e);
    }

    return $filtered;
  }

  /**
   * Remove all entitlements except standard online courses
   * @return UserEntitlementCollection
   */
  public function filterByOnlineCourse() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->isTypeOnlineCourse()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Remove all entitlements except standard online courses
   * @return UserEntitlementCollection
   */
  public function filterByCramCourse() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->isTypeCram()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Remove all entitlements except standard online courses
   * @return UserEntitlementCollection
   */
  public function filterByOnlineAndCramCourse() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->isTypeOnlineCourse() || $e->isTypeCram()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Remove all but "paid" entitlements. A paid entitlement is considered to be anything where
   * money exchanged hands - a normal purchase, firm direct bill or credit card, univ DB or CC.
   * Entitlements that came from any type of free access partnership are excluded.
   * @return UserEntitlementCollection
   */
  public function filterByPaid() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if (!$e->getPartner()->isBillingFreeAccess()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Remove all but "Free Trial" entitlements. Free trial entitlements come from "Normal / free access" partners,
   * which currently is limited to what we call "the" free trial.
   *
   * @return UserEntitlementCollection
   */
  public function filterByFreeTrial() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($e->getPartner()->isFreeTrial()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Remove all but "Free Trial" entitlements. Free trial entitlements come from "Normal / free access" partners,
   * which currently is limited to what we call "the" free trial.
   *
   * @param int $type
   * - See the following global constants:
   * define("firm_partner", 1);
   * define("free_trial", 2);
   * define("campus_sample_course", 3);
   * define("publisher", 4);
   *
   * @return UserEntitlementCollection
   */
  public function filterByPartnerType($type) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($e->getPartner()->getPartnerType() == $type) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Returns a collection of entitlements that have NO partner relationship.
   *
   * @return UserEntitlementCollection
   */
  public function filterByPartnerless() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if(!$e->getPartnerId()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Returns a collection of entitlements that have any type of partner relationship.
   *
   * @return UserEntitlementCollection
   */
  public function filterByPartnered() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->getPartnerId()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Returns a collection of entitlements that have a specific Partner ID or IDs.
   * @param int|array $pid
   * @return UserEntitlementCollection
   */
  public function filterByPartnerId($pid) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();
    
    if (is_array($pid)) {
      /** @var UserEntitlement $e */
      foreach ($this as $e) {
        if(in_array($e->getPartnerId(),$pid)) {
          $filtered->add($e);
        }
      }
    }
    else {
      /** @var UserEntitlement $e */
      foreach ($this as $e) {
        if($e->getPartnerId() == $pid) {
          $filtered->add($e);
        }
      }
    }
    return $filtered;
  }

  /**
   * Filter by partner's billing type
   *
   * @param string $type
   * - See: \RCPARPartner - isBillingFreeAccess(), isBillingCreditCard(), isBillingDirectBill(), and
   * isBillingPrepaid()
   *
   * @return UserEntitlementCollection
   */
  public function filterByBillingType($type) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($e->getPartner()->getBillingType() == $type) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Filter by entitlements that match the given course section (regardless of type)
   * @param string $section
   * - "AUD", "BEC", "FAR", or "REG"
   * @return UserEntitlementCollection
   */
  public function filterBySectionName($section) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($e->getCourseSectionName() == $section) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Filter by entitlements that have mobile offline access
   * @return UserEntitlementCollection
   */
  public function filterByMobileOfflineAccess() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($e->getMobileOfflineAccess()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Filter by entitlements that have been marked as changed (need to be saved)
   * @return UserEntitlementCollection
   */
  public function filterByChanged() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($e->isChanged()) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

     /**
   * Filter by entitlements that have the given bundle
   * @param string $bundle
   * - A product bundle in field_bundled_product like 'FULL-DIS', 'FULL-ELITE-DIS', etc
   * @return UserEntitlementCollection
   */
  public function filterByBundle($bundle) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if ($e->getBundledProduct() == $bundle) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Filter by entitlements created between the given start and end times.
   *
   * @param int $start
   * - UNIX timestamp of the beginning of the date range.
   * @param int $end
   * - UNIX timestamp of the end of the date range.
   * @return UserEntitlementCollection
   */
  public function filterByCreatedRange($start, $end) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if($e->getCreatedTime() >= $start && $e->getCreatedTime() <= $end) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Filter by entitlements configured as "offline lecture" products, as defined by
   * the system settings (enroll_flow_variable_get('offline_lecture', $single))
   */
  public function filterByOfflineLecture() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if (user_entitlements_is_offline_lectures($e->getProductId())) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Returns entitlements in the collection that come from UWorld, which is determined by looking for a value
   * in the UWorld subscription ID field.
   * @return UserEntitlementCollection
   */
  public function filterByUworld() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      if(!empty($e->getUworldSubscriptionId())) {
        $filtered->add($e);
      }
    }

    return $filtered;
  }

  /**
   * Checks to see if all four sections of the standard online course are in this collection.
   * NOTE: If you are looking for four ACTIVE parts, you need to run that filter yourself. This
   * method only checks that they're in the collection irrespective of their status.
   * @return bool
   */
  public function isContainingFullCourse() {
    $sections = array();
    /** @var UserEntitlement $e */
    foreach ($this->filterByOnlineCourse() as $e) {
      $sections[$e->getCourseSectionName()] = TRUE;
    }

    return sizeof($sections) == 4;
  }

  /**
   * Checks to see if all four sections of the CRAM course are in this collection.
   * @see isContainingFullCourse() for notes about filtering.
   * @return bool
   */
  public function isContainingFullCramCourse() {
    $sections = array();
    /** @var UserEntitlement $e */
    foreach ($this->filterByOnlineCourse() as $e) {
      $sections[$e->getCourseSectionName()] = TRUE;
    }

    return sizeof($sections) == 4;
  }


  /**
   * Determine if user entitlements are from an Elite partner.
   * Having entitlements from an "elite partner" means that the student purchased a full custom package
   * from one of the partners designated as "elite" on a pre-determined list.
   * The list indicates that these partners are moving to an elite status on the new platform.
   * Having a single part or auxiliary thing like a CRAM does not qualify the user for this upgrade.
   * @return bool
   */
  public function isContainingElitePartnerPackage() {
    $pids = explode("\r\n", variable_get('elite_partners'));
    if ($this->filterByPartnerId($pids)->filterByBundle('FULL-DIS')->isNotEmpty()) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }


  /**
   * Get the timestamp of the latest change of all the entitlements in this collection
   * @return int
   */
  public function getLastChanged() {
    $dates = array();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      $dates[] = $e->getChangedTime();
    }

    if(!empty($dates)) {
      return max($dates);
    }

    return 0;
  }

  /**
   * Return the entitlement with the latest expiration date
   * @return UserEntitlement
   */
  public function getLastExpired() {
    $dates = array();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      $dates[$e->getNid()] = $e->getExpirationDate();
    }
    // Get the highest nid from our array
    $max_nid = array_keys($dates, max($dates));

    return $this->getEntitlementById($max_nid[0]);

  }

  /**
   * Return the original collection after removing all of the entitlements present
   * in the $removalQueue collection
   * @param UserEntitlementCollection $removalQueue
   */
  public function remove(UserEntitlementCollection $removalQueue) {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new self();

    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      // If an entitlement in the removal queue is present in the current collection,
      // don't put it in the filtered collection that we will return.
      if ($removalQueue->getEntitlementById($e->getNid())) {
        continue;
      }

      // This entitlement is not in the removal queue, add it in.
      $filtered->add($e);
    }

    return $filtered;
  }

  /**
   * Save all entitlements in this collection
   */
  function saveAll() {
    /** @var UserEntitlement $e */
    foreach ($this as $e) {
      $e->save();
    }
  }

  /**
   * @deprecated - @todo: this isn't finished yet, but if we can migrate all of the original
   * filterByXXX functions to this new object, it would be sweet.
   * @return UserEntitlementCollectionFilter
   */
  function filterBy() {
    return new UserEntitlementCollectionFilter($this);
  }

}
