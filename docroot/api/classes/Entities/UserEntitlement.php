<?php

namespace RCPAR\Entities;

use RCPAR\WrappersDelight\UserEntitlementProductNodeWrapper;
use \RCPARPartner;
use \EntityMetadataWrapperException;

class UserEntitlement extends UserEntitlementProductNodeWrapper {
  protected $user;

  // Term ID for 'Online Course' course type (on user entitlement node)
  public static $COURSE_TYPE_ONLINE_COURSE = 1464;

  // Term ID for 'Offline Course' course type (on user entitlement node)
  public static $COURSE_TYPE_OFFLINE_COURSE = 1465;

  // Term ID for 'Online Cram Course' course type (on user entitlement node)
  public static $COURSE_TYPE_CRAM_COURSE = 1466;

  // Term ID for 'ACT' course type (on user entitlement node)
  public static $COURSE_TYPE_ACT = 6321;

  // Having this available just makes it easier to debug since you can see the property in front of you
  protected $sku;



  /**
   * UserEntitlement constructor.
   * @param int|object $entity_or_id
   * - Node id or loaded user entitlement node object
   * @param null|string $entity_type
   */
  public function __construct($entity_or_id) {
    // Allow an node to be loaded if an nid was passed in
    if(is_numeric($entity_or_id)) {
      $entity_or_id = node_load($entity_or_id);
    }

    parent::__construct($entity_or_id);

    $this->sku = $this->getProductSku();
  }

  /**
   * Retrieve an entity value by name.
   *
   * @param $name
   *
   * @return mixed
   */
  public function get($name, $default = FALSE) {
    if ($this->entity) {
      try {
        return $this->entity->{$name}->value();
      }
      catch (EntityMetadataWrapperException $e) {
        watchdog('UserEntitlement class', 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      }
    }

    return $default;
  }


  public function getExamVersionTid() {
    $ex = $this->getExamVersionSingleVal();
    if(is_object($ex)) {
      return $ex->tid;
    }

    // If we're here, no exam version is set
    return 0;
  }

  public function getExamVersionName() {
    $ex = $this->getExamVersionSingleVal();
    if(is_object($ex)) {
      return $ex->name;
    }

    // If we're here, no exam version is set
    return '';
  }

  /**
   * Returns true if this entitlement has a value set for exam version
   * @return bool
   */
  public function hasExamVersionSet() {
    $tid = $this->getExamVersionTid();
    if($tid !== 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * If this entitlement has content restrictions, this function will return an
   * array of those topic IDs that the user is allowed to access
   *
   * @return array
   * - A numeric array of topic IDs
   */
  public function getAllowedTopicIds() {
    // Build a list of topic restrictions.
    $restrictions = array();
    if ($this->getContentRestricted() && $this->hasExamVersionSet()) {
      foreach ($this->getFlagRestrict() as $item) {
        if($item['topic']) {
          $restrictions[] = $item['topic'];
        }
      }
    }

    return $restrictions;
  }

  /**
   * Get the field value of field_flag_restrict
   *
   * Must be overridden from the base class because this is a custom field type
   * @return array
   */
  public function getFlagRestrict() {
    $node = $this->value();
    if(!empty($node->field_flag_restrict[LANGUAGE_NONE])) {
      return $node->field_flag_restrict[LANGUAGE_NONE];
    }

    return array();
  }

  /**
   * Return the name of the course section
   *
   * @return string
   * - 'AUD', 'BEC', etc
   */
  public function getCourseSectionName() {
    // ACT entitlements are not set up to have a course section reference. This is a documented todo,
    // but to compensate we'll parse the section from the SKU if it's an ACT entitlement
    if($this->isTypeACT()) {
      // PAL entitlements are considered to be ACT too, but they truly don't have a section. Check if this
      // is one of the university style act entitlements, e.g. ACT-STUDENT-FAR, etc.
      if(stristr($this->getProductSku(), 'ACT-STUDENT-')) {
        return explode('-', $this->getProductSku())[2];
      }
    }

    // Otherwise, we return the course section as defined in the course section field
    $name = '';
    $sec = $this->getCourseSection();
    if(is_object($sec)) {
      $name = $sec->name;
    }

    return $name;
  }

  /**
   * Return the name of the course type
   *
   * @return string
   * - 'Online Course', 'Online Cram Course', etc
   */
  public function getCourseTypeName() {
    $name = '';
    $sec = $this->getCourseTypeRef();
    if(is_object($sec)) {
      $name = $sec->name;
    }

    return $name;
  }

  /**
   * Return the term ID of the course type
   *
   * @return int
   */
  public function getCourseTypeId() {
    $tid = 0;
    $sec = $this->getCourseTypeRef();
    if(is_object($sec)) {
      $tid = $sec->tid;
    }

    return $tid;
  }

  /**
   * Determine if the entitlement is "active".
   * Returns TRUE when
   * - The entitlement is activated (not a 'delayed'/unactivated CRAM)
   * - The entitlement node is published
   * - The entitlement expiration date is in the future
   * @return bool
   */
  public function isActive() {
    
    //if the entitlement is referenced from the uworld site isnot valid
    if($this->getUworldSubscriptionId()){
      return FALSE;
    }
    
    // Unpublished entitlements are not active
    if(!$this->isPublished()) {
      return FALSE;
    }

    // Delayed entitlements (aka unactivated CRAMs) aren't active
    if($this->getActivationDelayed()) {
      return FALSE;
    }
    
    // True if the expiration date is in the future
    return $this->getExpirationDate() > REQUEST_TIME;
  }

  /**
   * Checks that the entitlement is published, not expired, has delayed activation still set (makes all of these valid entitlements)
   * @return bool
   */
  public function isValid() {

    //if the entitlement is referenced from the uworld site isnot valid
    if($this->getUworldSubscriptionId()){
      return FALSE;
    }
    
    // Unpublished entitlements are not active
    if(!$this->isPublished()) {
      return FALSE;
    }

    // Is active and Delayed entitlements (aka unactivated CRAMs) aren't active
    if($this->isActive() || $this->getActivationDelayed()) {
      return TRUE;
    }
    
    // all other cases are determined to not be active
    return FALSE;
  }

  /**
   * Get mobile offline access for the given entitlement
   *
   * @todo - in the near future, this should just retrieve field_unlimited_access. This is just a temporary workaround
   * This method can be removed once field_unlimited_access is properly being populated
   * and the parent function can be used
   *
   * @return bool
   */
  public function getMobileOfflineAccess() {
    // This was a temporary workaround to check the package
    /*
    // Only applies to regular course and cram
    if($this->isTypeOnlineCourse() || $this->isTypeCram()) {
      // Premier and Elite bundles will get access by default
      $bundle = $this->getBundledProduct();
      if($bundle == 'FULL-PREM-DIS' || $bundle == 'FULL-ELITE-DIS') {
        return TRUE;
      }
    }*/

    $fieldval = $this->get('field_mobile_offline_access');

    // Don't return NULL values, which will be returned when the field is not there.
    if(is_null($fieldval)) {
      return FALSE;
    }

    return $fieldval;
  }

  /**
   * Get a partner object associated with the entitlement
   * @return RCPARPartner
   */
  public function getPartner() {
    return new RCPARPartner($this->getPartnerId());
  }

  /**
   * True if it's a CRAM course
   * @return bool
   */
  public function isTypeCram() {
    return $this->getCourseTypeId() == self::$COURSE_TYPE_CRAM_COURSE;
  }

  /**
   * True if it's an ACT entitlement
   * @return bool
   */
  public function isTypeACT() {
    return $this->getCourseTypeId() == self::$COURSE_TYPE_ACT;
  }

  /**
   * True if it's the standard online course
   * @return bool
   */
  public function isTypeOnlineCourse() {
    return $this->getCourseTypeId() == self::$COURSE_TYPE_ONLINE_COURSE;
  }

  /**
   * This function returns true if a standard online course (AUD, BEC, FAR, REG) was issued
   * from an ACT-enabled partner.
   * @todo - In the future, we intend to have a property on the entitlement that can be read,
   * rather than having to dynamically read the partner, which could be changed at any time.
   *
   * @return bool
   */
  public function isOnlineCourseACT() {
    // Only applies to standard online courses. True if there is a partner and it's an ACT partner
    return $this->isTypeOnlineCourse() && $this->getPartner()->isActPartner();
  }
}
