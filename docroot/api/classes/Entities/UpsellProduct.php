<?php

namespace RCPAR\Entities;

use RCPAR\WrappersDelight\UpsellCommerceProductWrapper;
use \EntityMetadataWrapperException;

class UpsellProduct extends UpsellCommerceProductWrapper {
  // Term ID for 'Online Course' course type (on user entitlement node)
  public static $COURSE_TYPE_ONLINE_COURSE = 1464;

  // Term ID for 'Offline Course' course type (on user entitlement node)
  public static $COURSE_TYPE_OFFLINE_COURSE = 1465;

  // Term ID for 'Online Cram Course' course type (on user entitlement node)
  public static $COURSE_TYPE_CRAM_COURSE = 1466;

  // Term ID for 'ACT' course type (on user entitlement node)
  public static $COURSE_TYPE_ACT = 6321;

  // Having this available just makes it easier to debug since you can see the property in front of you
  protected $sku;

  /**
   * @param int|object $entity_or_id
   * - Product id or loaded entity object
   */
  public function __construct($entity_or_id) {
    parent::__construct($entity_or_id);

    $this->sku = $this->getSku();
  }

  /**
   * Return the name of the course section
   *
   * @return string
   * - 'AUD', 'BEC', etc
   */
  public function getCourseSectionName() {
    $name = '';
    $sec = $this->getCourseSection();
    if (is_object($sec)) {
      $name = $sec->name;
    }

    return $name;
  }

  /**
   * Retrieve an entity value by name.
   *
   * @param $name
   * @return mixed
   */
  public function get($name, $default = FALSE) {
    if ($this->entity) {
      try {
        return $this->entity->{$name}->value();
      }
      catch (EntityMetadataWrapperException $e) {
        watchdog('UpsellProduct class', 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
      }
    }

    return $default;
  }

  /**
   * Return the name of the course type.
   *
   * @return string
   * - 'Online Course', 'Online Cram Course', etc
   */
  public function getCourseTypeName() {
    $name = '';

    $sec = $this->getCourseTypeRef();
    if (is_object($sec)) {
      $name = $sec->name;
    }

    return $name;
  }

  /**
   * Return the term ID of the course type
   *
   * @return int
   */
  public function getCourseTypeId() {
    $tid = 0;
    $sec = $this->getCourseTypeRef();
    if (is_object($sec)) {
      $tid = $sec->tid;
    }

    return $tid;
  }
}
