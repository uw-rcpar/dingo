<?php

namespace RCPAR\Entities\Notifications;

use RCPAR\User;

class Broadcast extends NotificationTemplate {
  private $channel;

  /**
   * Issue a receipt that the given user has received a broadcast over a particular channel.
   * NOTE: Call setChannel() on this broadcast FIRST to indicate which channel it is being broadcast on.
   * @param User $u
   * @return Notification
   */
  public function issueUnreadNotification(User $u) {
    $n = Notification::createFromBroadcast($u, $this);
    $n->save();
    return $n;
  }

  /**
   * Check if the user has a receipt for this broadcast
   * In other words, does this user have a previously read notification that matches this
   * notification template.
   * @param User $u
   */
  public function hasReceipt(User $u) {
    return db_query("
    SELECT id 
    FROM notif_notifications 
    WHERE uid = :uid AND notification_template_id = :ntid",
      array(':uid' => $u->getUid(), ':ntid' => $this->getId()))
      ->fetchField();
  }

  /**
   * Get the user's receipt (a loaded notification instance) of this broadcast, if they
   * have one.
   * @param User $u
   * @return Notification|bool
   */
  public function getReceipt(User $u) {
    $notifId = $this->hasReceipt($u);
    if($notifId) {
      return new Notification($notifId);
    }

    return FALSE;
  }

  /**
   * Get Channels that this Broadcast is in by querying notif_templates_channels
   * NOTE: This returns an array, but a broadcast is now only allowed to be assigned a single channel.
   * @return Channel[]
   */
  public function getChannels() {
    $channels = array();

    $ids = db_query("SELECT channels_id FROM notif_templates_channels WHERE templates_id = :id",
      [':id' => $this->getId()])->fetchCol();

    // Build an array of Channels
    foreach ($ids as $channel_id) {
      $channels[] = new Channel($channel_id);
    }

    return $channels;
  }

  /**
   * Get channel ids that this Broadcast is in
   * NOTE: This returns an array, but a broadcast is now only allowed to be assigned a single channel.
   * @return Int[]
   */
  public function getChannelIds() {
    $channel_ids = array();
    /** @var Channel $channel */
    foreach ($this->getChannels() as $channel) {
      $channel_ids[] = $channel->getId();
    }
    return $channel_ids;
  }

  /**
   * Specify a specific channel for this broadcast
   * NOTE: This has become a little redundant with getChannels() now that braodcasts are only allowed on one channel.
   * @param Channel $channel
   */
  function setChannel(Channel $channel) {
    $this->channel = $channel;
  }

  /**
   * Get the channel that this broadcast has been specified to broadcast on.
   * NOTE: This has become a little redundant with getChannels() now that braodcasts are only allowed on one channel.
   * @return Channel
   */
  function getChannel() {
    return $this->channel;
  }
}