<?php

namespace RCPAR\Entities\Notifications;

use RCPAR\User;
use RCPAR\WrappersDelight\NotificationsTemplateNodeWrapper;

class NotificationTemplate extends NotificationsTemplateNodeWrapper {

  /**
   * Return the token-transformed message from a template
   * @return string
   */
  public function getMessage(User $user) {
    // todo - the user should probably be used somehow
    return token_replace($this->getBody());
  }

  /**
   * Add a channel.
   * @param int $channel_id
   * @throws \Exception
   */
  public function addChannel($channel_id) {
    db_insert('notif_templates_channels')->fields(array(
      'channels_id' => $channel_id,
      'templates_id' => $this->getId(),
    ))->execute();
  }

  /**
   * Delete a channel.
   * @param int $channel_id
   * @throws \Exception
   */
  public function deleteChannel($channel_id) {
    db_query("DELETE FROM {notif_templates_channels} WHERE channels_id = :id AND templates_id = :nid", array(
        ':id' => $channel_id,
        ':nid' => $this->getId()
      )
    );
  }

  /**
   * True if the template is active and not expired
   * @return bool
   */
  public function isActive() {
    return $this->isPublished() && $this->getExpire() > REQUEST_TIME;
  }

  /**
   * True if the template is unpublished or expired
   * @return bool
   */
  public function isInactive() {
    return !$this->isActive();
  }

}
