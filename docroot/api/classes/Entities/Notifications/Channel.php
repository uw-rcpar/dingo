<?php

namespace RCPAR\Entities\Notifications;

use RCPAR\Modeling\ChannelModel;
use RCPAR\User;

class Channel extends ChannelModel {
  protected $u;

  const PRIVATE_CHANNEL = 'private';

  public function __construct($channelID_or_object = NULL) {
    if (is_numeric($channelID_or_object)) {
      $channel = self::getChannelById($channelID_or_object);
      $this->entity = $channel->getEntity();
    }
    else {
      parent::__construct($channelID_or_object);
    }
  }

  /**
   * Get the channel designated as private (made for individual communications)
   * @return Channel
   * @throws \Exception
   */
  public static function getPrivateChannel() {
    return self::getChannelByName(self::PRIVATE_CHANNEL);
  }

  /**
   * Get an array of all available notification channels
   * @return Channel[]
   */
  public static function getAllChannels() {
    $items = array(
      // All user channel
      /*0 => (object) array(
        'id' => 1,
        'name' => 'All active students',
        'description' => 'All users with an active entitlement',
        'machine_name' => 'all_active',
        'weight' => 1,
      ),*/

      // Mobile app
      1 => (object) array(
        'id' => 2,
        'name' => 'Mobile App',
        'description' => 'Users accessing via the mobile application',
        'machine_name' => 'mobile_app',
        'weight' => 2,
      ),

      // rogercpareview.com
      2 => (object) array(
        'id' => 3,
        'name' => 'RogerCPAReview.com',
        'description' => 'Users accessing through the rogercpareview.com website',
        'machine_name' => 'rogercpareview_com',
        'weight' => 3,
      ),
      /*// Private
      3 => (object) array(
        'id' => 4,
        'name' => 'Private',
        'description' => 'Used for individual notifications',
        'machine_name' => 'private',
        'weight' => 3,
      ),*/
    );

    // Build an array of Channel objects
    $channels = array();
    foreach ($items as $obj) {
      $channels[] = new Channel($obj);
    }

    return $channels;
  }

  /**
   * Get a channel object by machine name
   * @param string $name
   * @return Channel
   * @throws \Exception
   */
  public static function getChannelByName($name) {
    $channels = self::getAllChannels();
    foreach ($channels as $channel) {
      if($channel->getMachineName() == $name) {
        return $channel;
      }
    }

    // If we couldn't find the named channel, throw an exception
    throw new \Exception('Channel not found');
  }



  /**
   * Get a channel object by channel id
   * @param string $id
   * @return Channel
   * @throws \Exception
   */
  public static function getChannelById($id) {
    $channels = self::getAllChannels();
    foreach ($channels as $channel) {
      if($channel->getId() == $id) {
        return $channel;
      }
    }

    // If we couldn't find the named channel, throw an exception
    throw new \Exception('Channel not found');
  }


  /**
   * @deprecated - A channel's content is now requested specifically by the notification manager, we
   * don't make any particular association between a channel and a user.
   *
   * @param User $user
   * @param null $clientInfo - @todo: How to handle client info?
   * Option A: a new class in Entities with properties like 'OS', 'Age', 'Sex', etc. I guess the REST
   * endpoint would build this option
   * Option B: A new class, but we use dependency injection to add it into our User object. That, or something
   * like that I think makes more sense. hmm...
   *
   * We would also try simulating data here in order to do testing. e.g., return TRUE for certain machine names
   *
   * @return bool
   */
  public function isUserSubscribed(User $user, $clientInfo = NULL) {
    // Hard coding the channel subscription logic here for now, maybe in the future there will be a more
    // sophisticated plugin system or other method of organization.
    // @todo - More sophsticated/better organized subscription logic
    switch($this->getMachineName()) {

      // All users with at least one active entitlement
      case 'all_active':
        if($user->getValidEntitlements()->isNotEmpty()) {
          return TRUE;
        }
        break;

      // User's accessing via the mobile app
      case 'mobile_app':
        // To detect the mobile app, we check to see if this request is coming over the Amazon API gateway
        // In the future, it would be nice to abstract information about the client somewhere else and have
        // a more reliable check. For now, we can count on the fact that no other clients are using this gateway
        $headers = getallheaders();
        if (!empty($headers['x-amzn-apigateway-api-id'])) {
          if ($headers['x-amzn-apigateway-api-id'] == 'n3xjkdejog') {
            return TRUE;
          }
        }
        break;

      // User's accessing over the website
      case 'rogercpareview_com':
        // We'll assume that anything NOT leveraging the Amazon API gateway is traffic over our desktop website
        // This is not a great long term solution though
        if (!empty($headers['x-amzn-apigateway-api-id'])) {
          return TRUE;
        }
        break;
    }

    // If we got here, we weren't subscribed to this channel
    return FALSE;
  }

  /**
   * Get broadcasts for the channel that are active and have not expired
   * @return Broadcast[]
   */
  public function getActiveBroadcasts() {
    $broadcasts = array();
    foreach ($this->getBroadcasts() as $b) {
      if ($b->isActive()) {
        $broadcasts[] = $b;
      }
    }
    return $broadcasts;
  }


  /**
   * Get broadcasts for the channel that are inactive or expired
   * @return Broadcast[]
   */
  public function getInactiveBroadcasts() {
    $broadcasts = array();
    foreach ($this->getBroadcasts() as $b) {
      if ($b->isInactive()) {
        $broadcasts[] = $b;
      }
    }
    return $broadcasts;
  }

  /**
   * Get all Broadcasts that are associated with the channel regardless of status
   * @return Broadcast[]
   */
  public function getBroadcasts() {
    $broadcasts = array();

    // Get the template_ids from notif_templates_channels. Join against the node table to ensure the template still exists.
    $result = db_query("SELECT templates_id FROM notif_templates_channels ntf INNER JOIN node n ON n.nid = ntf.templates_id WHERE channels_id = :id",
      array(':id' => $this->getId()))->fetchCol();

    if ($result) {
      // Build new Broadcast objects for each ID and add to the return set
      foreach ($result as $template_id) {
        $b = new Broadcast($template_id);
        $b->setChannel($this);
        $broadcasts[] = $b;
      }
    }
    return $broadcasts;
  }

  /**
   * Get recent notifications from this channel
   *
   * @param int $limit
   * - Limit the number of results returned
   * @return Notification[]
   */
  public function getRecentNotifications($limit = 10) {
    $items = array();

    // Query the db
    $result = db_select(Notification::$tableName, 't')
      ->fields('t') // All fields
      ->condition('broadcast_channel_id', $this->getId(), '=')
      ->range(0,  $limit)
      ->execute();
    foreach ($result as $record) {
      $items[] = new Notification($record);
    }

    return $items;
  }
}
