<?php

namespace RCPAR\Entities\Notifications;

use RCPAR\Modeling\NotificationModel;
use RCPAR\User;

class Notification extends NotificationModel {
  public static function createFromBroadcast(User $u, Broadcast $b) {
    // Create a notification that's marked unread
    return self::create()
      // Basic properties
      ->setUid($u->getUid())
      ->setIsRead(0)
      ->setReadOn(0)
      ->setCreated(REQUEST_TIME)

      // Broadcast
      ->setBroadcastChannelId($b->getChannel()->getId())

      // Template
      ->setNotificationTemplateId($b->getId())
      ->setType($b->getType())// inherit type from the template
      ->setTitle($b->getTitle())
      ->setTeaser($b->getTeaser())
      ->setMessage($b->getMessage($u))// get transformed message from broadcast
      ->setExpire($b->getExpire()); // The receipt expires when the broadcast expires, which means we can delete it
  }

  /**
   * @param User $u
   * @param NotificationTemplate $nt
   * @param int $expiration
   * - Timestamp
   * @return NotificationModel
   * @throws \Exception
   */
  public static function createPrivateFromTemplate(User $u, NotificationTemplate $nt, $expiration) {
    return parent::create()
      // Basic properties
      ->setUid($u->getUid())
      ->setExpire($expiration)
      ->setIsRead(0)
      ->setReadOn(0)
      ->setCreated(REQUEST_TIME)

      // Message - from template
      ->setNotificationTemplateId($nt->getId())
      ->setType($nt->getType())
      ->setTitle($nt->getTitle())
      ->setTeaser($nt->getTeaser())
      ->setMessage($nt->getMessage($u))

      // Channel (always private)
      ->setBroadcastChannelId(Channel::getPrivateChannel()->getId());
  }

  public static function createPrivateFromMessage(User $u, $title, $teaser, $message, $type, $expiration) {
    return parent::create()
      // Basic properties
      ->setUid($u->getUid())
      ->setExpire($expiration)
      ->setIsRead(0)
      ->setReadOn(0)
      ->setCreated(REQUEST_TIME)

      // Message
      ->setNotificationTemplateId(0) // set 0 to indicate none
      ->setType($type)
      ->setTitle($title)
      ->setTeaser($teaser)
      ->setMessage($message)

      // Channel (0 for none)
      ->setBroadcastChannelId(Channel::getPrivateChannel()->getId());
  }

  /**
   * Return the Broadcast associated with this notification, if any.
   * We only consider a linked template to be a Broadcast if it's not on a private channel
   * @return Broadcast|bool
   */
  public function getBroadcast() {
    $channel = $this->getChannel();
    if($channel->getMachineName() != Channel::PRIVATE_CHANNEL) {
      return new Broadcast($this->getNotificationTemplateId());
    }
    return FALSE;
  }

  /**
   * Return the Broadcast associated with this notification, if any.
   * @return NotificationTemplate|bool
   */
  public function getNotificationTemplate() {
    if($ntid = $this->getNotificationTemplateId()) {
      return new NotificationTemplate($ntid);
    }
    return FALSE;
  }

  /**
   * Get the user this notification was sent to
   * @return User
   */
  public function getUser() {
    return new User($this->getUid());
  }

  /**
   * Get the Channel associated with this notification
   * @return Channel
   */
  public function getChannel() {
    return new Channel($this->getBroadcastChannelId());
  }

}