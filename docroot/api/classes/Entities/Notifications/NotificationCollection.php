<?php

namespace RCPAR\Entities\Notifications;

use RCPAR\Utils\IteratorCollection;


class NotificationCollection extends IteratorCollection {

  /**
   * Get the current item in the collection (Iterator method).
   * It's not necessary to implement this here since it's already in the RCPARIterator train,
   * but it's nice to have type-hinting so that foreach() code understands the type of variable
   * that is being iterated on.
   * @return Notification
   */
  public function current() {
    return parent::current();
  }

  /**
   * @return Notification[]
   */
  public function getNotifications() {
    return parent::getCollection();
  }

  /**
   * @param Notification[] $notifications
   */
  public function setNotifications($notifications) {
    $this->collection = $notifications;
  }

  /**
   * Get an array of all notification ids in this collection
   * @return int[]
   */
  public function getIds() {
    $ids = array();
    foreach ($this->getNotifications() as $notification) {
      $ids[] = $notification->getId();
    }
    return $ids;
  }

  /**
   * Mark all notifications in this collection as read and update the database
   * @return int
   * - Number of records updated
   */
  public function markAllRead() {
    // Can't mark read if we have nothing
    if ($this->isEmpty()) {
      return 0;
    }

    // We don't call save here to avoid the class "N+1" query anti pattern. We still want our objects
    // reflect reality though.
    /** @var Notification $notification */
    foreach ($this->getNotifications() as $notification) {
      $notification->markRead();
    }

    // Do the same thing but all at once in a DB query
    $num_updated = db_update('notif_notifications')
      ->fields(array(
        'is_read' => 1,
        'read_on' => REQUEST_TIME))
      ->condition('id', $this->getIds(), 'IN')
      ->execute();

    return $num_updated;
  }
}