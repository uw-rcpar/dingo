<?php
/**
 * @todo - This filter class is a sweet way to organize the filter methods that are piling up
 * in UserEntitlementCollection, but it's not finished yet and shouldn't be used.
 */

namespace RCPAR\Entities;


class UserEntitlementCollectionFilter {
  protected $collection;

  public function __construct(UserEntitlementCollection $collection) {
    $this->setCollection($collection);
  }

  public function onlineCourse() {
    /** @var UserEntitlementCollection $filtered */
    $filtered = new UserEntitlementCollection();

    /** @var UserEntitlement $e */
    foreach ($this->getCollection() as $e) {
      if($e->isTypeOnlineCourse()) {
        $filtered->addEntitlement($e);
      }
    }

    return $filtered;
  }

  public function ACT() {

  }

  /**
   * @param UserEntitlementCollection $collection
   * @return UserEntitlementCollectionFilter
   */
  public function setCollection($collection) {
    $this->collection = $collection;
    return $this;
  }

  /**
   * @return UserEntitlementCollection
   */
  public function getCollection() {
    return $this->collection;
  }


}