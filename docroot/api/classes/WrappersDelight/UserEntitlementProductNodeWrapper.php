<?php

namespace RCPAR\WrappersDelight;

/**
 * @file
 * class UserEntitlementProductNodeWrapper
 */

class UserEntitlementProductNodeWrapper extends WdNodeWrapper {

  public $entity_type = 'node';
  public static $bundle = 'user_entitlement_product';

  /**
   * Create a new user_entitlement_product node.
   *
   * @param array $values
   * @param string $language
   * @return static
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    $values += array('entity_type' => 'node', 'bundle' => self::$bundle, 'type' => self::$bundle);
    $entity_wrapper = parent::create($values, $language);
    return new static($entity_wrapper->value());
  }

  /**
   * Retrieves field_default_interval
   *
   * @return mixed
   */
  public function getDefaultInterval($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_default_interval', $format, $markup_format);
  }

  /**
   * Sets field_default_interval
   *
   * @param $value
   *
   * @return $this
   */
  public function setDefaultInterval($value, $format = NULL) {
    $this->setText('field_default_interval', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_duration_extended
   *
   * @return mixed
   */
  public function getDurationExtended() {
    return $this->get('field_duration_extended');
  }

  /**
   * Sets field_duration_extended
   *
   * @param $value
   *
   * @return $this
   */
  public function setDurationExtended($value) {
    $this->set('field_duration_extended', $value);
    return $this;
  }

  /**
   * Retrieves field_expiration_date
   *
   * @return mixed
   */
  public function getExpirationDate() {
    return $this->get('field_expiration_date');
  }

  /**
   * Sets field_expiration_date
   *
   * @param int $value
   * - A UNIX timestamp.
   *
   * @return $this
   */
  public function setExpirationDate($value) {
    $this->set('field_expiration_date', $value);
    return $this;
  }

  /**
   * Retrieves field_course_duration
   *
   * @return mixed
   */
  public function getCourseDuration($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_course_duration', $format, $markup_format);
  }

  /**
   * Sets field_course_duration
   *
   * @param $value
   *
   * @return $this
   */
  public function setCourseDuration($value, $format = NULL) {
    $this->setText('field_course_duration', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_order_id
   *
   * @return mixed
   */
  public function getOrderId($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_order_id', $format, $markup_format);
  }

  /**
   * Sets field_order_id
   *
   * @param $value
   *
   * @return $this
   */
  public function setOrderId($value, $format = NULL) {
    $this->setText('field_order_id', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_product_sku
   *
   * @return mixed
   */
  public function getProductSku($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_product_sku', $format, $markup_format);
  }

  /**
   * Sets field_product_sku
   *
   * @param $value
   *
   * @return $this
   */
  public function setProductSku($value, $format = NULL) {
    $this->setText('field_product_sku', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_product_id
   *
   * @return mixed
   */
  public function getProductId() {
    return $this->get('field_product_id');
  }

  /**
   * Sets field_product_id
   *
   * @param $value
   *
   * @return $this
   */
  public function setProductId($value) {
    $this->set('field_product_id', $value);
    return $this;
  }

  /**
   * Retrieves field_extended_interval
   *
   * @return mixed
   */
  public function getExtendedInterval($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_extended_interval', $format, $markup_format);
  }

  /**
   * Sets field_extended_interval
   *
   * @param $value
   *
   * @return $this
   */
  public function setExtendedInterval($value, $format = NULL) {
    $this->setText('field_extended_interval', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_extended_course_duration
   *
   * @return mixed
   */
  public function getExtendedCourseDuration($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_extended_course_duration', $format, $markup_format);
  }

  /**
   * Sets field_extended_course_duration
   *
   * @param $value
   *
   * @return $this
   */
  public function setExtendedCourseDuration($value, $format = NULL) {
    $this->setText('field_extended_course_duration', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_original_expiration_date
   *
   * @return mixed
   */
  public function getOriginalExpirationDate($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_original_expiration_date', $format, $markup_format);
  }

  /**
   * Sets field_original_expiration_date
   *
   * @param $value
   *
   * @return $this
   */
  public function setOriginalExpirationDate($value, $format = NULL) {
    $this->setText('field_original_expiration_date', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_product_title
   *
   * @return mixed
   */
  public function getProductTitle($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_product_title', $format, $markup_format);
  }

  /**
   * Sets field_product_title
   *
   * @param $value
   *
   * @return $this
   */
  public function setProductTitle($value, $format = NULL) {
    $this->setText('field_product_title', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_allowed_installs
   *
   * @return mixed
   */
  public function getAllowedInstalls() {
    return $this->get('field_allowed_installs');
  }

  /**
   * Sets field_allowed_installs
   *
   * @param $value
   *
   * @return $this
   */
  public function setAllowedInstalls($value) {
    $this->set('field_allowed_installs', $value);
    return $this;
  }

  /**
   * Retrieves field_installs
   *
   * @return mixed
   */
  public function getInstalls($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getTextMultiple('field_installs', $format, $markup_format);
  }


  /**
   * Sets field_installs
   *
   * @param $value
   *
   * @return $this
   */
  public function setInstalls($value, $format = NULL) {
    $this->setTextMultiple('field_installs', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_serial_number
   *
   * @return mixed
   */
  public function getSerialNumber($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_serial_number', $format, $markup_format);
  }

  /**
   * Sets field_serial_number
   *
   * @param $value
   *
   * @return $this
   */
  public function setSerialNumber($value, $format = NULL) {
    $this->setText('field_serial_number', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_content_restricted
   *
   * @return mixed
   */
  public function getContentRestricted() {
    return $this->get('field_content_restricted');
  }

  /**
   * Sets field_content_restricted
   *
   * @param $value
   *
   * @return $this
   */
  public function setContentRestricted($value) {
    $this->set('field_content_restricted', $value);
    return $this;
  }

  /**
   * Retrieves field_course_type_ref
   *
   * @return mixed
   */
  public function getCourseTypeRef() {
    return $this->get('field_course_type_ref');
  }

  /**
   * Sets field_course_type_ref
   *
   * @param $value
   *
   * @return $this
   */
  public function setCourseTypeRef($value) {
    $this->set('field_course_type_ref', $value);
    return $this;
  }

  /**
   * Retrieves the referenced taxonomy term object in field_course_section
   *
   * @return object
   */
  public function getCourseSection() {
    return $this->get('field_course_section');
  }

  /**
   * Sets field_course_section
   *
   * @param int $value
   * - Term ID of a course section
   *
   * @return $this
   */
  public function setCourseSection($value) {
    $this->set('field_course_section', $value);
    return $this;
  }

  /**
   * Retrieves field_flag_restrict
   * Note that this field does not work with entity_metadata_wrapper, so we have to access the node directly.
   * @return array
   */
  public function getFlagRestrict() {
    try {
      if ($node = $this->entity->value()) {
        return isset($node->field_flag_restrict[LANGUAGE_NONE]) ? $node->field_flag_restrict[LANGUAGE_NONE] : array();
      }
    }
    catch (\EntityMetadataWrapperException $e) {
      watchdog('rcpar_partners', 'See ' . __FUNCTION__ . '() ' . $e->getTraceAsString(), NULL, WATCHDOG_ERROR);
    }
    return array();
  }

  /**
   * Sets field_flag_restrict
   * Note that this field does not work with entity_metadata_wrapper, so we have to access the node directly.
   *
   * @param $value
   *
   * @return $this
   */
  public function setFlagRestrict($value) {
    try {
      $this->entity->value()->field_flag_restrict[LANGUAGE_NONE] = $value;
    }
    catch (\EntityMetadataWrapperException $e) {
      // Swallow error on failure
    }

    // Mark as changed
    $this->setIsChanged();

    return $this;
  }

  /**
   * Retrieves field_partner_id
   *
   * @return mixed
   */
  public function getPartnerId() {
    return $this->get('field_partner_id');
  }

  /**
   * Sets field_partner_id
   *
   * @param $value
   *
   * @return $this
   */
  public function setPartnerId($value) {
    $this->set('field_partner_id', $value);
    return $this;
  }

  /**
   * Retrieves field_activation_delayed
   *
   * @return bool
   */
  public function getActivationDelayed() {
    return $this->get('field_activation_delayed');
  }

  /**
   * Sets field_activation_delayed
   *
   * @param bool $value
   *
   * @return $this
   */
  public function setActivationDelayed($value) {
    $this->set('field_activation_delayed', $value);
    return $this;
  }

  /**
   * Retrieves field_bundled_product
   * This is the SKU of the product that the entitlement was bundled with, if any.
   *
   * @return string
   * - 'FULL-DIS', 'FULL-ELITE-DIS', etc
   */
  public function getBundledProduct() {
    return $this->get('field_bundled_product');
  }

  /**
   * Sets field_bundled_product
   *
   * @param $value
   *
   * @return $this
   */
  public function setBundledProduct($value) {
    $this->set('field_bundled_product', $value);
    return $this;
  }

  /**
   * Retrieves field_exam_version_single_val
   *
   * @return mixed
   */
  public function getExamVersionSingleVal() {
    return $this->get('field_exam_version_single_val');
  }

  /**
   * Sets field_exam_version_single_val
   *
   * @param int $value
   * - Taxonomy term ID for an exam version
   *
   * @return $this
   */
  public function setExamVersionSingleVal($value) {
    $this->set('field_exam_version_single_val', $value);
    return $this;
  }

  /**
   * Retrieves field_ipq_access
   *
   * @return mixed
   */
  public function getIpqAccess() {
    return $this->get('field_ipq_access');
  }

  /**
   * Sets field_ipq_access
   *
   * @param $value
   *
   * @return $this
   */
  public function setIpqAccess($value) {
    $this->set('field_ipq_access', $value);
    return $this;
  }

  /**
   * Retrieves field_unlimited_access
   *
   * @return bool
   */
  public function getUnlimitedAccess() {
    return $this->get('field_unlimited_access');
  }

  /**
   * Retrieves field_unlimited_access
   *
   * @return bool
   */
  public function getMobileOfflineAccess() {
    return $this->get('field_mobile_offline_access');
  }

  /**
   * Sets field_unlimited_access
   *
   * @param $value
   *
   * @return $this
   */
  public function setUnlimitedAccess($value) {
    $this->set('field_unlimited_access', $value);
    return $this;
  }

  /**
   * Retrieves field_hhc_access
   *
   * @return mixed
   */
  public function getHhcAccess() {
    return $this->get('field_hhc_access');
  }

  /**
   * Sets field_hhc_access
   *
   * @param $value
   *
   * @return $this
   */
  public function setHhcAccess($value) {
    $this->set('field_hhc_access', $value);
    return $this;
  }

  /**
   * Sets field_mobile_offline_access
   *
   * @param $value
   *
   * @return $this
   */
  public function setMobileOfflineAccess($value) {
    $this->set('field_mobile_offline_access', $value);
    return $this;
  }

  /**
   * Sets field_uworld_subscription_id
   *
   * @param int $value
   * @return $this
   */
  public function setUworldSubscriptionId($value) {
    $this->set('field_uworld_subscription_id', $value);
    return $this;
  }

  /**
   * Gets field_uworld_subscription_id
   *
   * @return int
   */
  public function getUworldSubscriptionId() {
    return $this->get('field_uworld_subscription_id');
  }
  
  /**
   * Retrieves field_activation_date
   * This is currently being used to map the UWorld data only and is not connected to RCPAR entitlement activations
   * @return mixed
   */
  public function getActivationDate() {
    return $this->get('field_activation_date');
  }

  /**
   * Sets field_activation_date
   *
   * @param int $value
   * - A UNIX timestamp.
   *
   * @return $this
   */
  public function setActivationDate($value) {
    $this->set('field_activation_date', $value);
    return $this;
  }

}
