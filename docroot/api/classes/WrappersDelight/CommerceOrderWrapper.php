<?php

namespace RCPAR\WrappersDelight;

use EntityFieldQuery;
use RCPARCommerceOrderEntity;

/**
 * @file
 * class CommerceOrderWrapper
 */
class CommerceOrderWrapper extends WdCommerceOrderWrapper {

  protected $entity_type = 'commerce_order';
  private static $bundle = 'commerce_order';

  /**
   * Create a new commerce_order commerce_order and save it.
   *
   * @param array $values
   * @param string $language
   * @return CommerceOrderWrapper
   */
  public static function createNew($uid, $order_status = 'checkout_checkout') {
    // We can't create it as completed (the state get changed during our manipulation)
    // so we just create it on 'checkout_checkout' state
    $order = commerce_order_new($uid, $order_status);

    // Save the order to get its ID (we need it to create the order lines)
    commerce_order_save($order);

    return new self($order);
  }

  /**
   *
   * @param int $uwOrderId
   * - A numeric order ID from UWorld that has been stored on the
   * @return CommerceOrderWrapper|NULL
   */
  public static function FindOrderByUworldId($uwOrderId) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_order')
          ->entityCondition('bundle', 'commerce_order')
          ->fieldCondition('field_uw_order_id', 'value', $uwOrderId, '=')// @todo - real field name?
          ->addMetaData('account', user_load(1)); // Run the query as user 1.
    $result = $query->execute();

    if (!(empty($result)) && isset($result['commerce_order'])) {
      $ids = array_keys($result['commerce_order']);

      // If we found a matching order...
      if (!empty($ids)) {
        // Load it up an wrap it snugly in our commerce class
        $order = commerce_order_load(array_pop($ids));
        if ($order) {
          return new self($order);
        }
      }
    }

    // Couldn't find a matching order if we're here, return a blank instance
    return NULL;
  }
  /**
   *
   * @param int $userId
   * - A numeric drupal userID from UWorld that has been stored on the
   * @return CommerceOrderWrapper|NULL
   */
  public static function FindOrderByUserId($userId) {
    $order_id = commerce_cart_order_id($userId);
    if ($order_id) {
      // Load it up an wrap it snugly in our commerce class
      $order = commerce_order_load($order_id);
      if ($order) {
        return new self($order);
      }
    }


    // Couldn't find a matching order if we're here, return a blank instance
    return NULL;
  }

  /**
   * Return a UPS tracking number for the given order ID.
   *
   * @param int $order_id
   *   - the entity order id
   * @return string|null
   *  - The tracking number if exists for this order, NULL if not.
   */
  public static function getUpsTrackingId($order_id) {
    $select = db_select('rcpar_ups_labels_log', 'l');
    $select->addField('l', 'tracking_number');
    $select->condition('order_id', $order_id);
    $result = $select->execute()->fetchColumn();
    return $result ? $result : NULL;
  }

  /**
   * Retrieves commerce_line_items
   *
   * @return mixed
   */
  public function getCommerceLineItems() {
    return $this->get('commerce_line_items');
  }

  /**
   * Sets commerce_line_items
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommerceLineItems($value) {
    $this->set('commerce_line_items', $value);
    return $this;
  }

  /**
   * Retrieves commerce_order_total
   *
   * @return mixed
   */
  public function getCommerceOrderTotal() {
    return $this->get('commerce_order_total');
  }

  /**
   * Sets commerce_order_total
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommerceOrderTotal($value) {
    $this->set('commerce_order_total', $value);
    return $this;
  }

  /**
   * Retrieves commerce_customer_billing
   *
   * @return object
   * - Stdclass entity commerce profile entity
   */
  public function getCommerceCustomerBilling() {
    return $this->get('commerce_customer_billing');
  }

  /**
   * Sets commerce_customer_billing
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommerceCustomerBilling($value) {
    $this->set('commerce_customer_billing', $value);
    return $this;
  }

  /**
   * Retrieves commerce_customer_shipping
   *
   * @return object
   * - Stdclass entity commerce profile entity
   */
  public function getCommerceCustomerShipping() {
    return $this->get('commerce_customer_shipping');
  }

  /**
   * Gets a customer's shipping address
   *
   * @return array
   */
  public function getCustomerShippingAddress() {
    $shipping = $this->getCommerceCustomerShipping();
    $address = $shipping->commerce_customer_address[LANGUAGE_NONE];
    if ($address) {
      return $address;
      }
    return $address = array();
  }

  /**
   * Sets commerce_customer_shipping
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommerceCustomerShipping($value) {
    $this->set('commerce_customer_shipping', $value);
    return $this;
  }

  /**
   * Retrieves commerce_coupon_order_reference
   *
   * @return WdEntityWrapper[]
   */
  public function getCommerceCouponOrderReference() {
    $values = $this->get('commerce_coupon_order_reference');
    foreach ($values as $i => $value) {
      $values[$i] = new WdEntityWrapper($value);
    }
    return $values;
  }

  /**
   * Sets commerce_coupon_order_reference
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommerceCouponOrderReference($value) {
    if (is_array($value)) {
      foreach ($value as $i => $v) {
        if ($v instanceof WdEntityWrapper) {
          $value[$i] = $v->value();
        }
      }
    }
    else {
      if ($value instanceof WdEntityWrapper) {
        $value = $value->value();
      }
    }

    $this->set('commerce_coupon_order_reference', $value);
    return $this;
  }

  /**
   * Adds a value to commerce_coupon_order_reference
   *
   * @param $value
   *
   * @return $this
   */
  public function addToCommerceCouponOrderReference($value) {
    if ($value instanceof WdEntityWrapper) {
      $value = $value->value();
    }
    $existing_values = $this->get('commerce_coupon_order_reference');
    if (!empty($existing_values)) {
      foreach ($existing_values as $i => $existing_value) {
        if (!empty($existing_value) && entity_id('commerce_coupon', $existing_value) == entity_id('commerce_coupon', $value)) {
          return $this;  // already here
        }
      }
    }
    $existing_values[] = $value;
    $this->set('commerce_coupon_order_reference', $existing_values);
    return $this;
  }

  /**
   * Removes a value from commerce_coupon_order_reference
   *
   * @param $value
   *   Value to remove.
   *
   * @return $this
   */
  function removeFromCommerceCouponOrderReference($value) {
    if ($value instanceof WdEntityWrapper) {
      $value = $value->value();
    }
    $existing_values = $this->get('commerce_coupon_order_reference');
    if (!empty($existing_values)) {
      foreach ($existing_values as $i => $existing_value) {
        if (!empty($existing_value) && entity_id('commerce_coupon', $existing_value) == entity_id('commerce_coupon', $value)) {
          unset($existing_values[$i]);
        }
      }
    }
    $this->set('commerce_coupon_order_reference', array_values($existing_values));
    return $this;
  }


  /**
   * Retrieves field_order_additional_details
   *
   * @return mixed
   */
  public function getOrderAdditionalDetails($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_order_additional_details', $format, $markup_format);
  }

  /**
   * Sets field_order_additional_details
   *
   * @param $value
   *
   * @return $this
   */
  public function setOrderAdditionalDetails($value, $format = NULL) {
    $this->setText('field_order_additional_details', $value, $format);
    return $this;
  }

  /**
   * Retrieves field_hear_about_us
   *
   * @return mixed
   */
  public function getHearAboutUs() {
    return $this->get('field_hear_about_us');
  }

  /**
   * Sets field_hear_about_us
   *
   * @param $value
   *
   * @return $this
   */
  public function setHearAboutUs($value) {
    $this->set('field_hear_about_us', $value);
    return $this;
  }

  /**
   * Retrieves field_why_choose_rcpar
   *
   * @return mixed
   */
  public function getWhyChooseRcpar() {
    return $this->get('field_why_choose_rcpar');
  }

  /**
   * Sets field_why_choose_rcpar
   *
   * @param $value
   *
   * @return $this
   */
  public function setWhyChooseRcpar($value) {
    $this->set('field_why_choose_rcpar', $value);
    return $this;
  }

  /**
   * Retrieves field_employer
   *
   * @return mixed
   */
  public function getEmployer($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_employer', $format, $markup_format);
  }

  /**
   * Sets field_employer
   *
   * @param $value
   *
   * @return $this
   */
  public function setEmployer($value, $format = NULL) {
    $this->setText('field_employer', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_partner_profile
   *
   * @return PartnersNodeWrapper
   */
  public function getPartnerProfile() {
    $value = $this->get('field_partner_profile');
    if (!empty($value)) {
      $value = new PartnersNodeWrapper($value);
    }
    return $value;
  }

  /**
   * Sets field_partner_profile
   *
   * @param $value
   *
   * @return $this
   */
  public function setPartnerProfile($value) {
    if (is_array($value)) {
      foreach ($value as $i => $v) {
        if ($v instanceof WdNodeWrapper) {
          $value[$i] = $v->value();
        }
      }
    }
    else {
      if ($value instanceof WdNodeWrapper) {
        $value = $value->value();
      }
    }

    $this->set('field_partner_profile', $value);
    return $this;
  }

  /**
   * Retrieves field_custom_field_name_1
   *
   * @return mixed
   */
  public function getCustomFieldName1($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_custom_field_name_1', $format, $markup_format);
  }

  /**
   * Sets field_custom_field_name_1
   *
   * @param $value
   *
   * @return $this
   */
  public function setCustomFieldName1($value, $format = NULL) {
    $this->setText('field_custom_field_name_1', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_custom_field_value_1
   *
   * @return mixed
   */
  public function getCustomFieldValue1($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_custom_field_value_1', $format, $markup_format);
  }

  /**
   * Sets field_custom_field_value_1
   *
   * @param $value
   *
   * @return $this
   */
  public function setCustomFieldValue1($value, $format = NULL) {
    $this->setText('field_custom_field_value_1', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_custom_field_name_2
   *
   * @return mixed
   */
  public function getCustomFieldName2($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_custom_field_name_2', $format, $markup_format);
  }

  /**
   * Sets field_custom_field_name_2
   *
   * @param $value
   *
   * @return $this
   */
  public function setCustomFieldName2($value, $format = NULL) {
    $this->setText('field_custom_field_name_2', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_custom_field_value_2
   *
   * @return mixed
   */
  public function getCustomFieldValue2($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_custom_field_value_2', $format, $markup_format);
  }

  /**
   * Sets field_custom_field_value_2
   *
   * @param $value
   *
   * @return $this
   */
  public function setCustomFieldValue2($value, $format = NULL) {
    $this->setText('field_custom_field_value_2', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_custom_field_name_3
   *
   * @return mixed
   */
  public function getCustomFieldName3($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_custom_field_name_3', $format, $markup_format);
  }

  /**
   * Sets field_custom_field_name_3
   *
   * @param $value
   *
   * @return $this
   */
  public function setCustomFieldName3($value, $format = NULL) {
    $this->setText('field_custom_field_name_3', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_custom_field_value_3
   *
   * @return mixed
   */
  public function getCustomFieldValue3($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_custom_field_value_3', $format, $markup_format);
  }

  /**
   * Sets field_custom_field_value_3
   *
   * @param $value
   *
   * @return $this
   */
  public function setCustomFieldValue3($value, $format = NULL) {
    $this->setText('field_custom_field_value_3', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_employment_verification
   *
   * @return mixed
   */
  public function getEmploymentVerification() {
    return $this->get('field_employment_verification');
  }

  /**
   * Sets field_employment_verification
   *
   * @param $value
   *
   * @return $this
   */
  public function setEmploymentVerification($value) {
    $this->set('field_employment_verification', $value);
    return $this;
  }

  /**
   * Retrieves field_employment_verification as a URL
   *
   * @param string $image_style
   *   (optional) Image style for the URL
   * @param bool $absolute
   *   Whether to return an absolute URL or not
   *
   * @return string
   */
  public function getEmploymentVerificationUrl($absolute = FALSE) {
    $file = $this->get('field_employment_verification');
    if (!empty($file)) {
      $file = url(file_create_url($file['uri']), array('absolute' => $absolute));
    }
    return $file;
  }


  /**
   * Retrieves field_partial_payment
   *
   * @return mixed
   */
  public function getPartialPayment() {
    return $this->get('field_partial_payment');
  }

  /**
   * Sets field_partial_payment
   *
   * @param $value
   *
   * @return $this
   */
  public function setPartialPayment($value) {
    $this->set('field_partial_payment', $value);
    return $this;
  }

  /**
   * Retrieves field_paid_in_full
   *
   * @return mixed
   */
  public function getPaidInFull() {
    return $this->get('field_paid_in_full');
  }

  /**
   * Sets field_paid_in_full
   *
   * @param $value
   *
   * @return $this
   */
  public function setPaidInFull($value) {
    $this->set('field_paid_in_full', $value);
    return $this;
  }

  /**
   * Retrieves field_payment_amount
   *
   * @return mixed
   */
  public function getPaymentAmount($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_payment_amount', $format, $markup_format);
  }

  /**
   * Sets field_payment_amount
   *
   * @param $value
   *
   * @return $this
   */
  public function setPaymentAmount($value, $format = NULL) {
    $this->setText('field_payment_amount', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_office_location
   *
   * @return mixed
   */
  public function getOfficeLocation($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_office_location', $format, $markup_format);
  }

  /**
   * Sets field_office_location
   *
   * @param $value
   *
   * @return $this
   */
  public function setOfficeLocation($value, $format = NULL) {
    $this->setText('field_office_location', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_employment_start_date
   *
   * @return mixed
   */
  public function getEmploymentStartDate() {
    return $this->get('field_employment_start_date');
  }

  /**
   * Sets field_employment_start_date
   *
   * @param $value
   *
   * @return $this
   */
  public function setEmploymentStartDate($value) {
    $this->set('field_employment_start_date', $value);
    return $this;
  }

  /**
   * Retrieves field_textbooks_selection
   *
   * @return mixed
   */
  public function getTextbooksSelection($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getTextMultiple('field_textbooks_selection', $format, $markup_format);
  }


  /**
   * Sets field_textbooks_selection
   *
   * @param $value
   *
   * @return $this
   */
  public function setTextbooksSelection($value, $format = NULL) {
    $this->setTextMultiple('field_textbooks_selection', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_prepayment_credit
   *
   * @return mixed
   */
  public function getPrepaymentCredit() {
    return $this->get('field_prepayment_credit');
  }

  /**
   * Sets field_prepayment_credit
   *
   * @param $value
   *
   * @return $this
   */
  public function setPrepaymentCredit($value) {
    $this->set('field_prepayment_credit', $value);
    return $this;
  }

  /**
   * Retrieves field_prepayment_active
   *
   * @return mixed
   */
  public function getPrepaymentActive() {
    return $this->get('field_prepayment_active');
  }

  /**
   * Sets field_prepayment_active
   *
   * @param $value
   *
   * @return $this
   */
  public function setPrepaymentActive($value) {
    $this->set('field_prepayment_active', $value);
    return $this;
  }

  /**
   * Retrieves field_credit_order_reference
   *
   * @return CommerceOrderCommerceOrderWrapper
   */
  public function getCreditOrderReference() {
    $value = $this->get('field_credit_order_reference');
    if (!empty($value)) {
      $value = new CommerceOrderCommerceOrderWrapper($value);
    }
    return $value;
  }

  /**
   * Sets field_credit_order_reference
   *
   * @param $value
   *
   * @return $this
   */
  public function setCreditOrderReference($value) {
    if (is_array($value)) {
      foreach ($value as $i => $v) {
        if ($v instanceof WdCommerceOrderWrapper) {
          $value[$i] = $v->value();
        }
      }
    }
    else {
      if ($value instanceof WdCommerceOrderWrapper) {
        $value = $value->value();
      }
    }

    $this->set('field_credit_order_reference', $value);
    return $this;
  }

  /////////////////////////////////////////////////////

  /**
   * Add a sales tax line item to the order
   * @param int $amount
   */
  public function addAvataxLineItem($amount, $deleteExistingLineItems = TRUE) {
    // Clear out existing tax line items, but don't save the order yet
    if ($deleteExistingLineItems) {
      commerce_avatax_delete_tax_line_items($this->getEntityMetadataWrapper(), TRUE);
    }

    // Create avatax line item
    $sales_tax = array(
      'amount' => $amount * 100,
      'currency_code' => commerce_default_currency(),
      'data' => array(),
    );
    $line_item_wrapper = commerce_avatax_add_line_item($this->getEntityMetadataWrapper(), $sales_tax, 'sales_tax');

    // Add the line item data as a property of the order.
    $this->getOrder()->avatax['avatax'] = $line_item_wrapper->value();

    // Avatax has a bizarre behavior where it registers a shutdown function that deletes tax line items if it can't
    // get a response back from the tax service. This is utter garbage and made me tear my hair out. You can't go
    // freaking deleting data from an order right after it's saved just because you feel like it. When the entity_insert
    // hook is called, it triggeres rcpar_commerce_views_entity_insert() which triggers rcpar_commerce_views_order_save()
    // which calls commerce_avatax_calculate_tax() which calls commerce_avatax_delete_tax_line_items() which deletes our
    // line items as Drupal is shutting down right after we've programmatically created an order. To avoid this atrocity,
    // we set a flag that is used later to prevent this nonsense from happening. You can probably tell I'm irritated about
    // this.
    $this->getOrder()->taxProgrammatic = TRUE;
  }

  /**
   * @param int $amount
   * @param int $typeId
   * - See _commerce_ups_service_list()
   */
  public function addShippingLineItem($amount, $typeId, $deleteExistingLineItems = TRUE) {
    // Clear out existing shipping line items, but don't save the order yet
    if ($deleteExistingLineItems) {
      commerce_shipping_delete_shipping_line_items($this->getOrder(), $skip_save = TRUE);
    }

    // Get a list of services from the UPS module
    $services = _commerce_ups_service_list();

    // Build the custom unit price array.
    $unit_price = array(
      'amount' => $amount * 100,
      'currency_code' => commerce_default_currency(),
      'data' => array(),
    );

    // Add a price component for the custom amount.
    $unit_price['data'] = commerce_price_component_add(
      $unit_price,
      'shipping',
      $unit_price,
      TRUE,
      FALSE
    );

    // Create the line item
    $line_item = commerce_shipping_line_item_new($services[$typeId]['slug'], $unit_price, $this->getId());
    
    // Add the line item to the order
    commerce_shipping_add_shipping_line_item($line_item, $this->getOrder());
  }

  /**
   * Return the Drupal stdclass order object entity
   * @return object
   */
  public function getOrder() {
    return $this->getEntityMetadataWrapper()->value();
  }

  public function setCreated($value) {
    $this->set('created', $value);
    return $this;
  }

  public function setChanged($value) {
    $this->set('created', $value);
    return $this;
  }

  public function setPlaced($value) {
    $this->set('placed', $value);
    return $this;
  }

  public function setMail($value) {
    $this->set('mail', $value);
    return $this;
  }

  public function setUwOrderId($value) {
    $this->set('field_uw_order_id', $value);
    return $this;
  }

  public function getUwOrderId() {
    return $this->get('field_uw_order_id');
  }

  /**
   * Retrieves field_uw_sf_id
   *
   * @return string
   */
  public function getUwSfId($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_uw_sf_id', $format, $markup_format);
  }

  /**
   * Sets field_uw_sf_id
   *
   * @param string $value
   *
   * @return $this
   */
  public function setUwSfId($value, $format = NULL) {
    $this->setText('field_uw_sf_id', $value, $format);
    return $this;
  }

  /**
   * Retrieves field_uw_partner_name
   *
   * @return string
   */
  public function getUwPartnerName($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_uw_partner_name', $format, $markup_format);
  }

  /**
   * Sets field_uw_partner_name
   *
   * @param string $value
   *
   * @return $this
   */
  public function setUwPartnerName($value, $format = NULL) {
    $this->setText('field_uw_partner_name', $value, $format);
    return $this;
  }

  /**
   * Deletes all line items on an order.
   *
   * @param bool $skip_save
   *  - Boolean indicating whether or not to skip saving the order
   *    in this function.
   */
  public function deleteAllLineItems($skip_save = FALSE) {
    $order_wrapper = $this->getEntityMetadataWrapper();
    $order = $this->getOrder();

    // When deleting more than one line item, metadata_wrapper will give problems
    // if deleting while looping through the line items. So first remove from
    // order and then delete the line items.
    $line_item_ids = array();

    foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      // Store its ID for later deletion and remove the reference from the line
      // item reference field.
      $line_item_ids[] = $line_item_wrapper->line_item_id->value();
      $order_wrapper->commerce_line_items->offsetUnset($delta);
    }

    // If we found any shipping line items...
    if (!empty($line_item_ids)) {
      // First save the order to update the line item reference field value.
      if (!$skip_save) {
        commerce_order_save($order);
      }

      // Then delete the line items.
      commerce_line_item_delete_multiple($line_item_ids);
    }
  }

  /**
   * @param string $sku
   * - A product SKU
   * @param bool $skip_save
   *  - Boolean indicating whether or not to skip saving the order
   *    in this function.
   * @param array $line_item_data
   *  - Line item meta data for bundled products
   * @return object|bool
   * - Returns the saved line item, or false on failure.
   */
  public function addProductLineItem($sku, $skip_save = FALSE, $line_item_data = array()) {
    // Do not proceed without a valid order.
    if (empty($this->getOrder())) {
      return FALSE;
    }

    $product = commerce_product_load_by_sku($sku);
    $line_item = commerce_product_line_item_new($product, 1, $this->getId(), $line_item_data);

    // Save the incoming line item now so we get its ID.
    commerce_line_item_save($line_item);

    // Add it to the order's line item reference value.
    $order_wrapper = $this->getEntityMetadataWrapper();
    $order_wrapper->commerce_line_items[] = $line_item;

    // Save the updated order.
    if (!$skip_save) {
      $this->save();
    }
    // Otherwise, we still need to update the order total to take for account
    // added shipping line item.
    else {
      commerce_order_calculate_total($this->getOrder());
    }

    return $line_item;
  }

  public function isShippable() {
    return rcpar_partner_products_has_weight($this->getOrder());
  }

  /**
   * Get order status.
   * @return string
   */
  public function getStatus() {
    return $this->get('status');
  }

  /**
   * Set the order status.
   * @param string $value
   * @return $this
   */
  public function setStatus($value) {
    $this->set('status', $value);
    return $this;
  }

  /**
   * Add a coupon line item to the order with a specified amount.
   * @param object $coupon
   * - Loaded coupon entity.
   * @param int $amount
   * - A commerce amount in the default currency. This is a normal dollar amount multiplied by 100. So if the coupon is
   * $100.00, pass in 10000.
   */
  public function addFixedCouponLineItem($coupon, $amount) {
    module_load_include('inc', 'commerce_coupon', 'commerce_coupon.rules');
    //commerce_coupon_action_create_coupon_line_item($coupon, $this->getOrder(), $amount, 'base_price', commerce_default_currency());
    commerce_coupon_action_create_coupon_line_item($coupon, $this->getOrder(), $amount, 'discount', commerce_default_currency());
  }

  /**
   * Deletes a line item in the order by product sku
   * @param string $sku
   * @param bool $skip_save
   * @return void
   */
  public function deleteProductLineItem($sku, $skip_save = FALSE) {
    $order_wrapper = $this->getEntityMetadataWrapper();
    $order = $this->getOrder();
    $line_item_id = null;
    $order_line_items = $order_wrapper->commerce_line_items->value();
    foreach ($order_line_items as $lineitem) {
      $line_wrapper = entity_metadata_wrapper('commerce_line_item', $lineitem);
      if ($line_wrapper->getBundle() == "product" && $line_wrapper->commerce_product->sku->value() == $sku) {
        $line_item_id = $line_wrapper->line_item_id->value();
        commerce_line_item_delete($line_item_id);
      }
    }

    // If we found a sku to delete...
    if (!is_null($line_item_id) && !$skip_save) {
      commerce_order_save($order);
    }
  }

}
