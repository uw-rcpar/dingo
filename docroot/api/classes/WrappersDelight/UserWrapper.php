<?php

namespace RCPAR\WrappersDelight;

use EntityFieldQuery;

/**
 * @file
 * class UserWrapper
 */

class UserWrapper extends WdUserWrapper {

  protected $entity_type = 'user';
  private static $bundle = 'user';

  public function __construct($entity, $entity_type = NULL) {
    // Don't try to load if we don't have any entity value
    if (empty($entity)) {
      return;
    }

    // Try to load from an ID if possible
    if (is_numeric($entity)) {
      $entity = entity_load_single($this->entity_type, $entity);
      if ($entity) {
        $this->entity = entity_metadata_wrapper($this->entity_type, $entity);
      }

      // Even if the entity wasn't loaded, we'll return to prevent the parent constructor from trying to wrap a numeric
      // value in an EntityMetadataWrapper.
      return;

    }

    parent::__construct($entity, $entity_type);
  }

  /**
   * Find a user by UWorld user ID
   * @param int $uwUid
   * - A numeric user ID from UWorld.
   * @return static|NULL
   */
  public static function FindUserByUworldUid($uwUid) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'user')
          ->fieldCondition('field_uwuid', 'value', $uwUid, '=')
          ->addMetaData('account', user_load(1)); // Run the query as user 1.
    $result = $query->execute();

    if (!(empty($result)) && isset($result['user'])) {
      $ids = array_keys($result['user']);

      // If we found a matching order...
      if (!empty($ids)) {
        // Load it up an wrap it snugly in our commerce class
        $user = user_load(array_pop($ids));
        if ($user) {
          return new static($user);
        }
      }
    }

    // Couldn't find a matching user if we're here, return null
    return NULL;
  }


  /**
   * @return bool
   */
  public function isLoaded() {
    if(empty($this->entity)) {
      return FALSE;
    }

    if(!empty($this->getUid())) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Create a new user user.
   *
   * @param array $values
   * @param string $language
   * @return static
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    $values += array('entity_type' => 'user', 'bundle' => self::$bundle, 'type' => self::$bundle);
    $entity_wrapper = parent::create($values, $language);
    return new static($entity_wrapper->value());
  }

  /**
   * @return int
   */
  public function getUid() {
    return $this->getIdentifier();
  }

  /**
   * Retrieves field_registration_type
   *
   * @return mixed
   */
  public function getRegistrationType($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_registration_type', $format, $markup_format);
  }

  /**
   * Sets field_registration_type
   *
   * @param $value
   *
   * @return $this
   */
  public function setRegistrationType($value, $format = NULL) {
    $this->setText('field_registration_type', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_campus
   *
   * @return mixed
   */
  public function getCampus($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_campus', $format, $markup_format);
  }

  /**
   * Sets field_campus
   *
   * @param $value
   *
   * @return $this
   */
  public function setCampus($value, $format = NULL) {
    $this->setText('field_campus', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_phone_number
   *
   * @return mixed
   */
  public function getPhoneNumber($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_phone_number', $format, $markup_format);
  }

  /**
   * Sets field_phone_number
   *
   * @param $value
   *
   * @return $this
   */
  public function setPhoneNumber($value, $format = NULL) {
    $this->setText('field_phone_number', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_firm
   *
   * @return mixed
   */
  public function getFirm($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_firm', $format, $markup_format);
  }

  /**
   * Sets field_firm
   *
   * @param $value
   *
   * @return $this
   */
  public function setFirm($value, $format = NULL) {
    $this->setText('field_firm', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_courses_taught
   *
   * @return mixed
   */
  public function getCoursesTaught() {
    return $this->get('field_courses_taught');
  }

  /**
   * Sets field_courses_taught
   *
   * @param $value
   *
   * @return $this
   */
  public function setCoursesTaught($value) {
    $this->set('field_courses_taught', $value);
    return $this;
  }

  /**
   * Retrieves field_interests
   *
   * @return mixed
   */
  public function getInterests() {
    return $this->get('field_interests');
  }

  /**
   * Sets field_interests
   *
   * @param $value
   *
   * @return $this
   */
  public function setInterests($value) {
    $this->set('field_interests', $value);
    return $this;
  }

  /**
   * Retrieves field_other_cpa_group
   *
   * @return mixed
   */
  public function getOtherCpaGroup() {
    return $this->get('field_other_cpa_group');
  }

  /**
   * Sets field_other_cpa_group
   *
   * @param $value
   *
   * @return $this
   */
  public function setOtherCpaGroup($value) {
    $this->set('field_other_cpa_group', $value);
    return $this;
  }

  /**
   * Retrieves field_student_state
   *
   * @return mixed
   */
  public function getStudentState() {
    return $this->get('field_student_state');
  }

  /**
   * Sets field_student_state
   *
   * @param $value
   *
   * @return $this
   */
  public function setStudentState($value) {
    $this->set('field_student_state', $value);
    return $this;
  }

  /**
   * Retrieves field_did_you_graduate_college
   *
   * @return mixed
   */
  public function getDidYouGraduateCollege() {
    return $this->get('field_did_you_graduate_college');
  }

  /**
   * Sets field_did_you_graduate_college
   *
   * @param $value
   *
   * @return $this
   */
  public function setDidYouGraduateCollege($value) {
    $this->set('field_did_you_graduate_college', $value);
    return $this;
  }

  /**
   * Retrieves field_college_state
   *
   * @return mixed
   */
  protected function getCollegeState() {
    return $this->get('field_college_state');
  }

  /**
   * Sets field_college_state
   *
   * @param $value
   *
   * @return $this
   */
  public function setCollegeState($value) {
    $this->set('field_college_state', $value);
    return $this;
  }

  /**
   * Retrieves field_graduation_month_and_year
   *
   * @return mixed
   */
  public function getGraduationMonthAndYear() {
    return $this->get('field_graduation_month_and_year');
  }

  /**
   * Sets field_graduation_month_and_year
   *
   * @param $value
   *
   * @return $this
   */
  public function setGraduationMonthAndYear($value) {
    $this->set('field_graduation_month_and_year', $value);
    return $this;
  }

  /**
   * Retrieves field_aud_exam_date
   *
   * @return mixed
   */
  public function getAudExamDate($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_aud_exam_date', $format, $markup_format);
  }

  /**
   * Sets field_aud_exam_date
   *
   * @param $value
   *
   * @return $this
   */
  public function setAudExamDate($value, $format = NULL) {
    $this->setText('field_aud_exam_date', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_reg_exam_date
   *
   * @return mixed
   */
  public function getRegExamDate($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_reg_exam_date', $format, $markup_format);
  }

  /**
   * Sets field_reg_exam_date
   *
   * @param $value
   *
   * @return $this
   */
  public function setRegExamDate($value, $format = NULL) {
    $this->setText('field_reg_exam_date', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_far_exam_date
   *
   * @return mixed
   */
  public function getFarExamDate($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_far_exam_date', $format, $markup_format);
  }

  /**
   * Sets field_far_exam_date
   *
   * @param $value
   *
   * @return $this
   */
  public function setFarExamDate($value, $format = NULL) {
    $this->setText('field_far_exam_date', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_bec_exam_date
   *
   * @return mixed
   */
  public function getBecExamDate($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_bec_exam_date', $format, $markup_format);
  }

  /**
   * Sets field_bec_exam_date
   *
   * @param $value
   *
   * @return $this
   */
  public function setBecExamDate($value, $format = NULL) {
    $this->setText('field_bec_exam_date', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_archive_uid
   *
   * @return mixed
   */
  public function getArchiveUid() {
    return $this->get('field_archive_uid');
  }

  /**
   * Sets field_archive_uid
   *
   * @param $value
   *
   * @return $this
   */
  public function setArchiveUid($value) {
    $this->set('field_archive_uid', $value);
    return $this;
  }

  /**
   * Retrieves field_archive_synced
   *
   * @return mixed
   */
  public function getArchiveSynced() {
    return $this->get('field_archive_synced');
  }

  /**
   * Sets field_archive_synced
   *
   * @param $value
   *
   * @return $this
   */
  public function setArchiveSynced($value) {
    $this->set('field_archive_synced', $value);
    return $this;
  }

  /**
   * Retrieves field_first_name
   *
   * @return mixed
   */
  public function getFirstName($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_first_name', $format, $markup_format);
  }

  /**
   * Sets field_first_name
   *
   * @param $value
   *
   * @return $this
   */
  public function setFirstName($value, $format = NULL) {
    $this->setText('field_first_name', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_last_name
   *
   * @return mixed
   */
  public function getLastName($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_last_name', $format, $markup_format);
  }

  /**
   * Sets field_last_name
   *
   * @param $value
   *
   * @return $this
   */
  public function setLastName($value, $format = NULL) {
    $this->setText('field_last_name', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_campus_rep
   *
   * @return mixed
   */
  public function getCampusRep() {
    return $this->get('field_campus_rep');
  }

  /**
   * Sets field_campus_rep
   *
   * @param $value
   *
   * @return $this
   */
  public function setCampusRep($value) {
    $this->set('field_campus_rep', $value);
    return $this;
  }

  /**
   * Retrieves field_sync_stage
   *
   * @return mixed
   */
  public function getSyncStage() {
    return $this->get('field_sync_stage');
  }

  /**
   * Sets field_sync_stage
   *
   * @param $value
   *
   * @return $this
   */
  public function setSyncStage($value) {
    $this->set('field_sync_stage', $value);
    return $this;
  }

  /**
   * Retrieves field_college_state_list
   *
   * @return mixed
   */
  public function getCollegeStateList() {
    return $this->get('field_college_state_list');
  }

  /**
   * Sets field_college_state_list
   *
   * @param $value
   *
   * @return $this
   */
  public function setCollegeStateList($value) {
    $this->set('field_college_state_list', $value);
    return $this;
  }

  /**
   * Retrieves field_college_name
   *
   * @return mixed
   */
  protected function getCollegeName($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_college_name', $format, $markup_format);
  }

  /**
   * Sets field_college_name
   *
   * @param $value
   *
   * @return $this
   */
  public function setCollegeName($value, $format = NULL) {
    $this->setText('field_college_name', $value, $format);
    return $this;
  }


  /**
   * Retrieves field_country
   *
   * @return mixed
   */
  public function getCountry() {
    return $this->get('field_country');
  }

  /**
   * Sets field_country
   *
   * @param $value
   *
   * @return $this
   */
  public function setCountry($value) {
    $this->set('field_country', $value);
    return $this;
  }

  /**
   * Retrieves field_when_do_you_plan_to_start
   *
   * @return mixed
   */
  public function getWhenDoYouPlanToStart() {
    return $this->get('field_when_do_you_plan_to_start');
  }

  /**
   * Sets field_when_do_you_plan_to_start
   *
   * @param $value
   *
   * @return $this
   */
  public function setWhenDoYouPlanToStart($value) {
    $this->set('field_when_do_you_plan_to_start', $value);
    return $this;
  }

  /**
   * Retrieves field_uwuid
   *
   * @return int
   */
  public function getUwuid() {
    return $this->get('field_uwuid');
  }

  /**
   * Sets field_uwuid
   *
   * @param int $value
   *
   * @return $this
   */
  public function setUwuid($value) {
    $this->set('field_uwuid', $value);
    return $this;
  }

  /**
   * Retrieves field_college_sf_account
   *
   * @return WdEntityWrapper
   */
  public function getCollegeSfAccount() {
    $value = $this->get('field_college_sf_account');
    if (!empty($value)) {
      $value = new WdEntityWrapper($value);
    }
    return $value;
  }

  /**
   * Sets field_college_sf_account
   *
   * @param $value
   *
   * @return $this
   */
  public function setCollegeSfAccount($value) {
    if (is_array($value)) {
      foreach ($value as $i => $v) {
        if ($v instanceof WdEntityWrapper) {
          $value[$i] = $v->value();
        }
      }
    }
    else {
      if ($value instanceof WdEntityWrapper) {
        $value = $value->value();
      }
    }

    $this->set('field_college_sf_account', $value);
    return $this;
  }

  /**
   * Set the password hash for the user
   * @param string $value
   * @return $this
   */
  public function setPassHash($value) {
    $user = $this->getEntityMetadataWrapper()->value();
    $user->pass = $value;
    return $this;
  }
  
  
  public function setPartnerSfId($partnerSfId) {
    $user = $this->getEntityMetadataWrapper()->value();
    $user->data['partnerSfId'] = $partnerSfId ;
  }

}
