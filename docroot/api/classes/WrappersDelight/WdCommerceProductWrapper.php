<?php
/**
 * @file
 * class WdCommerceProductWrapper
 */

namespace RCPAR\WrappersDelight;

class WdCommerceProductWrapper extends WdEntityWrapper {

  protected $entity_type = 'commerce_product';

  /**
   * Create a new commerce_product.
   *
   * @param array $values
   * @param string $language
   *
   * @return WdCommerceProductWrapper
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    $values += array('entity_type' => 'commerce_product');
    $entity_wrapper = parent::create($values, $language);
    return new WdCommerceProductWrapper($entity_wrapper->value());
  }

  /**
   * Retrieves product_id
   *
   * @return int
   */
  public function getProductId() {
    return $this->getIdentifier();
  }

  /**
   * Retrieves sku
   *
   * @return string
   */
  public function getSku($format = WdEntityWrapper::FORMAT_PLAIN) {
    return $this->getText('sku', $format);
  }

  /**
   * Sets sku
   *
   * @param string $value
   *
   * @return $this
   */
  public function setSku($value) {
    $this->set('sku', $value);
    return $this;
  }

  /**
   * Retrieves type
   *
   * @return string
   */
  public function getType() {
    return $this->getBundle();
  }

  /**
   * Sets type
   *
   * @param string $value
   *
   * @return $this
   */
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  /**
   * Retrieves title
   *
   * @return string
   */
  public function getTitle($format = WdEntityWrapper::FORMAT_PLAIN) {
    return $this->getText('title', $format);
  }

  /**
   * Sets title
   *
   * @param string $value
   *
   * @return $this
   */
  public function setTitle($value) {
    $this->set('title', $value);
    return $this;
  }

  /**
   * Retrieves status
   *
   * @return bool
   */
  public function getStatus() {
    return $this->get('status');
  }

  /**
   * Sets status
   *
   * @param bool $value
   *
   * @return $this
   */
  public function setStatus($value) {
    $this->set('status', $value);
    return $this;
  }

  /**
   * Retrieves created
   *
   * @return int|string
   */
  public function getCreated($format = WdEntityWrapper::DATE_UNIX, $custom_format = '') {
    return $this->getDate('created', $format, $custom_format);
  }


  /**
   * Sets created
   *
   * @param int $value
   *
   * @return $this
   */
  public function setCreated($value) {
    $this->set('created', $value);
    return $this;
  }

  /**
   * Retrieves changed
   *
   * @return int|string
   */
  public function getChanged($format = WdEntityWrapper::DATE_UNIX, $custom_format = '') {
    return $this->getDate('changed', $format, $custom_format);
  }


  /**
   * Sets changed
   *
   * @param int $value
   *
   * @return $this
   */
  public function setChanged($value) {
    $this->set('changed', $value);
    return $this;
  }

  /**
   * Retrieves uid
   *
   * @return int
   */
  public function getUid() {
    return $this->get('uid');
  }

  /**
   * Sets uid
   *
   * @param int $value
   *
   * @return $this
   */
  public function setUid($value) {
    $this->set('uid', $value);
    return $this;
  }

  /**
   * Retrieves creator
   *
   * @return WdUserWrapper
   */
  public function getCreator() {
    $value = $this->get('creator');
    if (!empty($value)) {
      return new WdUserWrapper($value);
    }
    return NULL;
  }

  /**
   * Retrieves commerce_price
   *
   * @return mixed
   */
  public function getCommercePrice() {
    return $this->get('commerce_price');
  }

  /**
   * Sets commerce_price
   *
   * @param mixed $value
   *
   * @return $this
   */
  public function setCommercePrice($value) {
    $this->set('commerce_price', $value);
    return $this;
  }

  /**
   * Retrieves edit_url
   *
   * @return string
   */
  public function getEditUrl() {
    return $this->get('edit_url');
  }

  /**
   * Sets edit_url
   *
   * @param mixed $value
   *
   * @return $this
   */
  public function setEditUrl($value) {
    $this->set('edit_url', $value);
    return $this;
  }

}
