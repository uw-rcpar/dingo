<?php
/**
 * @file
 * class UpsellCommerceProductWrapper
 */

namespace RCPAR\WrappersDelight;

class UpsellCommerceProductWrapper extends WdCommerceProductWrapper {

  protected $entity_type = 'commerce_product';
  private static $bundle = 'upsell';

  /**
   * Create a new upsell commerce_product.
   *
   * @param array $values
   * @param string $language
   * @return UpsellCommerceProductWrapper
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    $values += array('entity_type' => 'commerce_product', 'bundle' => self::$bundle, 'type' => self::$bundle);
    $entity_wrapper = parent::create($values, $language);
    return new UpsellCommerceProductWrapper($entity_wrapper->value());
  }

  /**
   * Retrieves commerce_price
   *
   * @return mixed
   */
  public function getCommercePrice() {
    return $this->get('commerce_price');
  }

  /**
   * Sets commerce_price
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommercePrice($value) {
    $this->set('commerce_price', $value);
    return $this;
  }

  /**
   * Retrieves field_description
   *
   * @return mixed
   */
  public function getDescription($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('field_description', $format, $markup_format);
  }

  /**
   * Sets field_description
   *
   * @param $value
   *
   * @return $this
   */
  public function setDescription($value, $format = NULL) {
    $this->setText('field_description', $value, $format);
    return $this;
  }

  /**
   * Retrieves field_dimensions
   *
   * @return mixed
   */
  public function getDimensions() {
    return $this->get('field_dimensions');
  }

  /**
   * Sets field_dimensions
   *
   * @param $value
   *
   * @return $this
   */
  public function setDimensions($value) {
    $this->set('field_dimensions', $value);
    return $this;
  }

  /**
   * Retrieves field_weight
   *
   * @return mixed
   */
  public function getWeight() {
    return $this->get('field_weight');
  }

  /**
   * Sets field_weight
   *
   * @param $value
   *
   * @return $this
   */
  public function setWeight($value) {
    $this->set('field_weight', $value);
    return $this;
  }

  /**
   * Retrieves field_product_image
   *
   * @return mixed
   */
  public function getProductImage() {
    return $this->get('field_product_image');
  }

  /**
   * Sets field_product_image
   *
   * @param $value
   *
   * @return $this
   */
  public function setProductImage($value) {
    $this->set('field_product_image', $value);
    return $this;
  }

  /**
   * Retrieves field_product_image as an HTML <img> tag
   *
   * @param string $image_style
   *   (optional) Image style for the HTML
   * @param array $options
   *   (optional) options to pass to the theme function. If you want to add
   *   attributes, you can do that under the attributes key of this array.
   *
   * @return string[]
   */
  public function getProductImageHtml($image_style = NULL, $options = array()) {
    $images = $this->get('field_product_image');
    $image_html = array();
    if (!empty($images)) {
      foreach ($images as $i => $image) {
        $options += array(
          'path' => $image['uri'],
        );
        if (!is_null($image_style)) {
          $options['style_name'] = $image_style;
          $image_html[$i] = theme('image_style', $options);
        }
        else {
          $image_html[$i] = theme('image', $options);
        }
      }
    }
    return $image_html;
  }


  /**
   * Retrieves field_product_image as a URL
   *
   * @param string $image_style
   *   (optional) Image style for the URL
   * @param bool $absolute
   *   Whether to return an absolute URL or not
   *
   * @return string
   */
  public function getProductImageUrl($image_style = NULL) {
    $images = $this->get('field_product_image');
    if (!empty($images)) {
      foreach ($images as $i => $image) {
        if (!is_null($image_style)) {
          $images[$i] = image_style_url($image_style, $image['uri']);
        }
        else {
          $images[$i] = url(file_create_url($image['uri']));
        }
      }
    }
    return $images;
  }


  /**
   * Retrieves field_commerce_saleprice
   *
   * @return mixed
   */
  public function getCommerceSaleprice() {
    return $this->get('field_commerce_saleprice');
  }

  /**
   * Sets field_commerce_saleprice
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommerceSaleprice($value) {
    $this->set('field_commerce_saleprice', $value);
    return $this;
  }

  /**
   * Retrieves field_commerce_saleprice_on_sale
   *
   * @return mixed
   */
  public function getCommerceSalepriceOnSale() {
    return $this->get('field_commerce_saleprice_on_sale');
  }

  /**
   * Sets field_commerce_saleprice_on_sale
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommerceSalepriceOnSale($value) {
    $this->set('field_commerce_saleprice_on_sale', $value);
    return $this;
  }

  /**
   * Retrieves field_course_section
   *
   * @return mixed
   */
  public function getCourseSection() {
    return $this->get('field_course_section');
  }

  /**
   * Sets field_course_section
   *
   * @param $value
   *
   * @return $this
   */
  public function setCourseSection($value) {
    $this->set('field_course_section', $value);
    return $this;
  }

  /**
   * Retrieves field_course_type_ref
   *
   * @return mixed
   */
  public function getCourseTypeRef() {
    return $this->get('field_course_type_ref');
  }

  /**
   * Sets field_course_type_ref
   *
   * @param $value
   *
   * @return $this
   */
  public function setCourseTypeRef($value) {
    $this->set('field_course_type_ref', $value);
    return $this;
  }

  /**
   * Retrieves field_apply_discount_avatax
   *
   * @return mixed
   */
  public function getApplyDiscountAvatax() {
    return $this->get('field_apply_discount_avatax');
  }

  /**
   * Sets field_apply_discount_avatax
   *
   * @param $value
   *
   * @return $this
   */
  public function setApplyDiscountAvatax($value) {
    $this->set('field_apply_discount_avatax', $value);
    return $this;
  }

  /**
   * Retrieves commerce_avatax_code
   *
   * @return mixed
   */
  public function getCommerceAvataxCode() {
    return $this->get('commerce_avatax_code');
  }

  /**
   * Sets commerce_avatax_code
   *
   * @param $value
   *
   * @return $this
   */
  public function setCommerceAvataxCode($value) {
    $this->set('commerce_avatax_code', $value);
    return $this;
  }

}
