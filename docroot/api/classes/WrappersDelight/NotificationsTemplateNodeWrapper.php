<?php

namespace RCPAR\WrappersDelight;

/**
 * @file
 * class NotificationsTemplateNodeWrapper
 */

class NotificationsTemplateNodeWrapper extends WdNodeWrapper {

  protected $entity_type = 'node';
  private static $bundle = 'notifications_template';

  /**
   * Create a new notifications_template node.
   *
   * @param array $values
   * @param string $language
   * @return static
   */
  public static function create($values = array(), $language = LANGUAGE_NONE) {
    $values += array('entity_type' => 'node', 'bundle' => self::$bundle, 'type' => self::$bundle);
    $entity_wrapper = parent::create($values, $language);
    return new static($entity_wrapper->value());
  }

  /**
   * Retrieves body
   *
   * @return string
   */
  public function getBody($format = WdEntityWrapper::FORMAT_DEFAULT, $markup_format = NULL) {
    return $this->getText('body', $format, $markup_format);
  }

  /**
   * Retrieves summary
   *
   * @return string
   */
  public function getSummary($format = WdEntityWrapper::FORMAT_TRIMMED, $markup_format = NULL) {
    return $this->getText('body', $format, $markup_format);
  }

  /**
 * Retrieves teaser
 *
 * @return string
 */
  public function getTeaser($format = WdEntityWrapper::FORMAT_TRIMMED, $markup_format = NULL) {
    return $this->getText('field_teaser', $format, $markup_format);
  }

  /**
   * Retrieves title
   *
   * @return string
   */
  public function getTitle($format = WdEntityWrapper::FORMAT_PLAIN) {
    return $this->getText('title', $format);
  }

  /**
   * Sets body
   *
   * @param $value
   *
   * @return $this
   */
  public function setBody($value, $format = NULL) {
    $this->setText('body', $value, $format);
    return $this;
  }

  /**
   * Sets Title
   *
   * @param $value
   *
   * @return $this
   */
  public function setTitle($value) {
    $this->set('title', $value);
    return $this;
  }

  /**
   * Sets Teaser
   *
   * @param $value
   *
   * @return $this
   */
  public function setTeaser($value) {
    $this->set('teaser', $value);
    return $this;
  }



  /**
   * Retrieves field_expire
   *
   * @return mixed
   */
  public function getExpire() {
    return $this->get('field_expire');
  }

  /**
   * Sets field_expire
   *
   * @param $value
   *
   * @return $this
   */
  public function setExpire($value) {
    $this->set('field_expire', $value);
    return $this;
  }

  /**
   * Retrieves field_machine_name
   *
   * @return mixed
   */
  public function getMachineName() {
    return $this->get('field_machine_name');
  }

  /**
   * Sets field_machine_name
   *
   * @param $value
   *
   * @return $this
   */
  public function setMachineName($value) {
    $this->set('field_machine_name', $value);
    return $this;
  }

  /**
   * Sets field_type
   *
   * @param $value
   *
   * @return $this
   */
  public function setType($value) {
    $this->set('field_type', $value);
    return $this;
  }

  /**
   * Gets field_type
   *
   * @return string
   */
  public function getType() {
    return $this->get('field_type');
  }

  /**
   * Retrieves field_audience
   *
   * @return string[]
   */
  public function getAudience() {
    return $this->get('field_audience');
  }

  /**
   * Sets field_audience
   *
   * @param string[] $value
   * - An array of string values
   *
   * @return $this
   */
  public function setAudience($value) {
    $this->set('field_audience', $value);
    return $this;
  }

}
