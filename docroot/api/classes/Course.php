<?php

namespace RCPAR;

use RCPAR\Entities\UserEntitlement;
use RCPAR\Entities\UserEntitlementCollection;
use \Exception;

class Course {

  /**
   * Returns an array containing the last watched topic_id for all of the given user's
   * active course entitlements
   *
   * @param User $u
   * - The user to get progress for
   *
   * @return array
   * - An array of arrays containing keys 'sku' and 'topic_id'
   * Example:
   * array(
   *  ['sku' => 'AUD', 'topic_id' => '5491456'],
   *  ['sku' => 'BEC', 'topic_id' => '4044886'],
   * )
   */
  public static function getUserLastWatched(User $u) {
    $payload = array();

    // Get all of the active course entitlements (standard and CRAM)
    $courseEnts = $u
      ->getAllEntitlements()
      ->filterByActive()
      ->filterByOnlineAndCramCourse();

    /** @var UserEntitlement $e */
    foreach ($courseEnts->getEntitlements() as $e) {
      $payload[] = array(
        'sku' => $e->getProductSku(),
        'topic_id' => self::getEntitlementLastWatched($e),
      );
    }

    return $payload;
  }

  /**
   * Returns the topic_id of the last watched video for the given entitlement
   *
   * @return int|bool
   * - A topic id when progress exists, otherwise FALSE.
   */
  public static function getEntitlementLastWatched(UserEntitlement $e) {
      return db_query("
        SELECT topic_reference AS topic_id
        FROM eck_video_history vh
        INNER JOIN (
          SELECT MAX(changed) AS mch FROM eck_video_history 
          WHERE uid = :uid AND entitlement_product = :eid 
          ORDER BY changed DESC
        ) tmp ON (vh.changed = tmp.mch)
        WHERE uid = :uid",

        array(
          ':uid' => $e->getAuthorId(),
          ':eid' => $e->getNid(),
        )
      )->fetchField();
  }

  /**
   * Get user's video progress percentages
   * @todo - Note that this method duplicates work done in the procedural method rcpar_dashboard_get_percentages_totals()
   * It would be nice if the procedural function would leverage this method instead.
   *
   * @param User $u
   * - User to get progress for
   * @param UserEntitlementCollection $ents
   * - Entitlements to get progress for
   * @return array|bool
   * - False if there is no progress, otherwise an array
   * The return array looks like this:
   * array(
   *  0 => ['sku' => 'AUD', 'percentage' => 12.1],
   *  1 => ['sku' => 'BEC', 'percentage' => 29.5],
   *  2 => ['sku' => 'FAR', 'percentage' => 0.0],
   *  3 => ['sku' => 'REG', 'percentage' => 100.0],
   * )
   */
  public static function getProgressPercentageForUser(User $u, UserEntitlementCollection $ents) {
    // If entitlements are empty, nothing to see here
    if ($ents->isEmpty()) {
      return;
    }
    // Query to add up the user's video history progress
    $course_progress = db_query("
      SELECT SUM(last_position) as viewed, course_type, section
      FROM eck_video_history WHERE uid=:uid
      AND entitlement_product IN (:eids)
      GROUP BY section, course_type",
      array(':uid' => $u->getUid(), ':eids' => $ents->getIds()))
      ->fetchAll();

    $average_completed = array();
    if (!empty($course_progress)) {
      foreach ($course_progress as $course_record) {
        // Derive the SKU from course type and course section
        $sku = strstr(strtolower($course_record->course_type), "cram") ? "{$course_record->section}-CRAM" : $course_record->section;

        // Get the exam version term id from the entitlement. If not set, default to the most recent
        $exam_year_tid = $ents->getEntitlementBySku($sku)->getExamVersionTid();
        if(!$exam_year_tid) {
          $exam_year_tid = array_pop(exam_versions_years());
        }

        // Get the total video time for this course
        $course_stats = db_select('course_stats', 'cs')
          ->fields('cs', array('total_time'))
          ->condition('section', $sku)
          ->condition('course_type', $course_record->course_type)
          ->condition('type', 'section')
          ->condition('year', $exam_year_tid)
          ->execute()
          ->fetchObject();
        if (!$course_stats) {
          continue;
        }

        // Total video duration for this section
        $video_duration_ms = $course_stats->total_time;

        // Sum of 'last_position' markers in video history
        $last_position = $course_record->viewed;

        // Calculate percentage
        $percent = ($last_position * 100) / $video_duration_ms;
        if ($percent > 100) {
          $percent = 100;
        }

        // Add to results
        $average_completed[] = array(
          'sku' => $sku,
          'percentage' => round($percent),
        );
      }
    }

    return $average_completed;
  }

  /**
   * Get user's video progress percentage for a given sku.
   *
   * @param User $u
   * - User to get progress for
   * @param UserEntitlementCollection $ents
   * - Entitlements to get progress for
   * @return float|null
   * - Returns the percentage progress as whole number (e.g. 40.0 if it's 40%). If no progress is found, 
   * will default to 0.
   */
  public static function getProgressPercentageForUserSku(User $u, UserEntitlementCollection $ents, $sku) {
    $average_completed = self::getProgressPercentageForUser($u, $ents);
    foreach ((array)$average_completed as $data) {
      if($data['sku'] == $sku) {
        return $data['percentage'];
      }
    }
  }

  /**
   * Get the node id for the section of a standard or CRAM course node
   *
   * @param string $section
   * - CPA course section: 'AUD', 'BEC', etc
   * @param bool $isCram
   * - Set true for a CRAM course, false for standard online course.
   * @return int
   * - Returns the node ID of a course
   * @throws Exception
   */
  function getCourseNid($section, $isCram = FALSE) {
    $term = taxonomy_get_term_by_name($section, 'course_sections');
    if ($term) {
      // Returns an array, get the first item
      $term = reset($term);
      if ($isCram) {
        $course_type_tid = COURSE_TYPE_CRAM_COURSE;
      }
      else {
        $course_type_tid = COURSE_TYPE_ONLINE_COURSE;
      }
      $result = rcpar_dashboard_get_course_by_term($term->tid, $course_type_tid);
      if(!empty($result)) {
        $courseNid = reset(array_keys($result['node']));
        return $courseNid;
      }
    }

    // If we got here, we failed to get a course node
    throw new Exception('Invalid course section or type');
  }
}
