<?php


namespace RCPAR\Import;

use EntityMetadataWrapper;
use Exception;
use RCPAR\WrappersDelight\CommerceOrderWrapper;
use RCPAR\WrappersDelight\UserWrapper;
use RCPARCommerceBundle;
use \RCPARPartner;


class UWorldOrderProcessor {
  public $orderUpdateData; // Original JSON data

  // Basic identifiers
  public $uwUserId;
  public $uwOrderId;
  public $rogerUid;
  public $email;

  // Payment properties
  public $paymentId;
  public $transactionTypeId;
  public $paymentModeId;
  public $gatewayId;
  public $transactionId;
  public $transactionAmount;
  public $discountOrderAmount;

  // Order properties
  public $dateCreated;
  public $lineItems;
  public $salesTaxAmount;
  public $billing;
  public $shipping;
  public $doNotShip;

  // Partner stuff
  public $partnerName;
  public $partnerSfId;


  /** @var UserWrapper $u */
  public $u; // The user object that this order belongs to.
  public $existingUser = FALSE; // True if we found an existing user in the system already for the order.

  /** @var CommerceOrderWrapper $ow * */
  public $ow; // An order object representing the order.
  public $existingOrder = FALSE; // True when we are updating an existing order that was already saved.

  // A map of "packageId" field found in the JSON data to RCPAR SKUs.
  public static $productMap = array(
    284 => 'FREETRIAL-BUNDLE',  // Roger CPA Review Trial
    285 => 'FULL-PREM', // Roger CPA Review Premier
    286 => 'FULL-ELITE', // Roger CPA Review Elite
    287 => 'AUD-SEL', // Roger CPA AUD
    288 => 'BEC-SEL', // Roger CPA BEC
    289 => 'FAR-SEL', // Roger CPA FAR
    290 => 'REG-SEL', // Roger CPA REG
    298 => 'FULL-ELITE', // Roger CPA Review Elite Demo
    300 => 'FULL-ELITE', // Roger CPA Review Elite - Promo
    317 => 'FULL-PREM-PLUS', // Roger CPA Review Premier Plus
    318 => 'FULL-ELITE', // Roger CPA Review Elite - FSL Lead
    301 => 'AUD-REN-30', //AUD 30-day renewal
    302 => 'BEC-REN-30', //BEC 30-day renewal
    303 => 'FAR-REN-30', //FAR 30-day renewal
    304 => 'REG-REN-30', //REG 30-day renewal
    305 => 'AUD-REN-90', //AUD 90-day renewal
    306 => 'BEC-REN-90', //BEC 90-day renewal
    307 => 'FAR-REN-90', //FAR 90-day renewal
    308 => 'REG-REN-90', //REG 90-day renewal
    309 => 'AUD-REN-180', //AUD 180-day renewal
    310 => 'BEC-REN-180', //BEC 180-day renewal
    311 => 'FAR-REN-180', //FAR 180-day renewal
    312 => 'REG-REN-180', //REG 180-day renewal
    313 => 'AUD-REN-360', //AUD 360-day renewal
    314 => 'BEC-REN-360', //BEC 360-day renewal
    315 => 'FAR-REN-360', //FAR 360-day renewal
    316 => 'REG-REN-360', //REG 360-day renewal
    354 => 'AUD-BK-2020-E', //	AUD textbook 2020 Edition
    355 => 'BEC-BK-2020-E', // BEC Textbook 2020 Edition
    356 => 'FAR-BK-2020-E', // FAR Textbook 2020 Edition
    357 => 'REG-BK-2020-E', // REG Textbook 2020 Edition
    358 => 'FULL-BK-2020-E', // 	FULL - 2020 Course Textbook (Complete Set)
    401 => 'AUD-NB', // AUD - No Books
    402 => 'BEC-NB', // BEC - No Books
    403 => 'REG-NB', // REG - No Books
    404 => 'FAR-NB', // FAR - No Books
    405 => 'FSL-FAR', //  FAR FSL
  );

  // Mapping for UWorld's "courseId" field for textbooks
  public static $courseMap = array(
    28001 => 'AUD',
    29001 => 'BEC',
    30001 => 'FAR',
    31001 => 'REG'   
  );

  // True when there is a free trial in the order
  public $freeTrialIsPresent = FALSE;

  /*
   * Course list
   * 28000    AUD
   * 29000    BEC
   * 30000    FAR
   * 31000    REG
   */


  /**
   * UWorldOrderProcessor constructor.
   *
   * @param array $orderUpdateData
   * - JSON decoded data for an individual "payment" from the UWorld API
   * @throws Exception
   */
  public function __construct($orderUpdateData) {
    $this->orderUpdateData = $orderUpdateData;

    // Basic order identifiers
    $this->uwUserId = $orderUpdateData->uwUserId;
    $this->rogerUid = !empty($orderUpdateData->rogerUserId) ? $orderUpdateData->rogerUserId : FALSE;
    $this->email = trim($orderUpdateData->emailId);
    $this->determineUworldOrderId();

    // Payment properties
    $this->paymentId = $orderUpdateData->paymentId;
    $this->transactionTypeId = $orderUpdateData->transactionTypeId;
    $this->paymentModeId = $orderUpdateData->paymentModeId;
    $this->gatewayId = $orderUpdateData->gatewayId;
    $this->transactionId = $orderUpdateData->transactionId;
    $this->discountOrderAmount = $orderUpdateData->discountAmount;

    // Order properties
    $this->billing = $orderUpdateData->billing;
    $this->shipping = $orderUpdateData->shipping;
    $this->shippingAmount = $orderUpdateData->shippingAmount;
    $this->dateCreated = $orderUpdateData->dateCreatedInEpochTime;
    $this->lineItems = $orderUpdateData->lineItems;
    $this->salesTaxAmount = $orderUpdateData->salesTaxAmount;
    $this->transactionAmount = $orderUpdateData->transactionAmount;
    $this->doNotShip = $orderUpdateData->doNotShip;
    
    // Partner stuff
    $this->partnerName = $orderUpdateData->associatedFirm;
    $this->partnerSfId = $orderUpdateData->salesforceId;
  }

  /**
   * Import and log data from the UWorld orders API.
   *
   * @param int $from
   * - Beginning data range to query the API for. UNIX timestamp.
   * @param int $to
   * - Ending data range to query the API for. UNIX timestamp.
   * @param int $orderId
   * - Optionally filter by a specific order. This is a UWorld order ID.
   * @param int $userId
   * - Optionally filter by a specific user. This is a UWorld user ID.
   * @return UWorldImportLog
   * - The log object with details about the import status.
   */
  public static function import($from = NULL, $to = NULL, $orderId = NULL, $userId = NULL) {
    // Build a new log entry for this segment, or find an existing one.
    $log = new UWorldImportLog(UWorldImportLog::$TYPE_ORDERS, $from, $to);
    $log->newAttempt();

    // Save the log now, just in case anything else fails
    $log->save();

    // Get data from the Orders API endpoint
    $data = UWorldOrderProcessor::apiRequest($from, $to, $orderId, $userId);
    //$data = file_get_contents(__DIR__ . '/uworld.order.import.sample.json');

    // If the request fails, log issues and return.
    $jsonData = json_decode($data);
    if ($jsonData === NULL) {
      $log->set('message', 'API request failed or could not be decoded.');
      $log->set('completed', time());
      $log->setHasFailures(TRUE);
      $log->save();
      return $log;
    }

    // Sometimes the API response has errors when there is a remote problem
    if (isset($jsonData->error)) {
      $log->set('message', '<strong>Remote API error:</strong><br>' . $jsonData->error . '<br>' . $jsonData->error_description);
      $log->set('completed', time());
      $log->setHasFailures(TRUE);
      $log->save();
      return $log;
    }

    // Log the number of records available to process.
    $log->set('records_available', sizeof($jsonData->orders));

    // Process each order
    foreach ($jsonData->orders as $orderUpdateData) {
      try {
        $d = new UWorldOrderProcessor($orderUpdateData);
        $d->process();
        $log->incrementRecordsSuccessful();

        if(!$d->ow->getId()) {
          throw new Exception('No Drupal order ID available after import, something went wrong.');
        }

        $log->addMessage('Imported order: ' . l($d->ow->getId(), 'admin/commerce/orders/' . $d->ow->getId() . '/edit'));
      }
      catch (Exception $e) {
        $log->addMessage($e->getMessage() . ' | Email:  ' . $d->email . ' | Payment ID: ' . $d->uwOrderId);
        $log->setHasFailures(TRUE);
        if (is_object($d) && !empty($d->uwOrderId)) {
          $log->addFailureId($d->uwOrderId);
        }
        continue;
      }
    }

    // Log as completed and save.
    $log->set('completed', time());
    $log->save();
    return $log;
  }

  /**
   * Import a single order from UWorld, given its UWorld order ID (aka paymentId).
   * @param int $orderId
   * @return bool
   * @throws Exception
   */
  public static function importSingleOrder($orderId) {
    // Get data from the Orders API endpoint
    $data = UWorldOrderProcessor::apiRequest(NULL, NULL, $orderId, NULL);

    // If the request fails, log issues and return.
    $jsonData = json_decode($data);
    if ($jsonData === NULL) {
      throw new Exception('API request failed or could not be decoded.');
    }

    // Sometimes the API response has errors when there is a remote problem
    if (isset($jsonData->error)) {
      throw new Exception('<strong>Remote API error:</strong><br>' . $jsonData->error . '<br>' . $jsonData->error_description);
    }

    // Process each order
    foreach ($jsonData->orders as $orderUpdateData) {
      $d = new UWorldOrderProcessor($orderUpdateData);
      $d->process();

      if (!$d->ow->getId()) {
        throw new Exception('No Drupal order ID available after import, something went wrong.');
      }
    }

    // If no exceptions were thrown, return TRUE.
    return TRUE;
  }

  /**
   * Process an order record - its billing, shipping, line items, tax, discounts, and payments.
   */
  public function process() {
    // Create or update the user for this order
    $this->determineUser();
    $this->updateUserData();

    // Determine if we have an existing order object or create a new one
    $this->determineOrder();

    // Update basic order details (created time, etc)
    $this->updateOrderBasics();

    // Existing order:
    if(!empty($this->ow->getCommerceCustomerBilling()) || !empty($this->ow->getCommerceCustomerShipping())) { 
      // Create or update billing/shipping profile
      $this->updateBillingShippingProfile();
    }
    // New order:
    else {
      $this->createBillingShippingProfile();
    }

    // TransactionTypes (transactionTypeId)
    // 1 Charge
    // 2 Refund
    // 3 Void
    // 4 ChargeBack  (process like refund)
    // 5 InvoiceAdjustment (process like refund) (these won't come in)
    // 6 ChargeBack Reversal (process like charge)
    switch ($this->transactionTypeId) {
      // 1 Charge
      case 1:
        $this->ow->deleteAllLineItems(TRUE); // Start fresh. Especially important if we are re-running an import.

        // Remove existing and then create a single tax line item. Tax needs to be processed first because we need
        // to set a tax-related flag to prevent rcpar_commerce_views_order_save() breaking all kinds of stuff and
        // doing things that it shouldn't be doing.
        $this->ow->addAvataxLineItem($this->salesTaxAmount);

        // Product line items. Needs to be done prior to shipping so that the label creator can see products in the order.
        $this->addProductLineItemsToOrder();

        // Free trials will never have shipping and should just be marked completed.
        // doNotShip flag from UWorld should be marked completed.
        if ($this->freeTrialIsPresent) {
          commerce_order_status_update($this->ow->getOrder(), 'completed', TRUE, TRUE, 'UWorld API: Free trial - marked completed.');
        }
        else {
          // Get shipping method and amount from the API data.
          $shipping_method_id = $this->shipping->shippingCode;
          $shipping_amount = $this->shippingAmount;

          // UWorld didn't specify any shipping parameters, but it looks like a shippable order. This shouldn't happen,
          // but if it does, we'll add a $0 shipping line item and make a label.
          if (empty($shipping_method_id) && rcpar_partner_products_has_weight($this->ow->getOrder())) {
            $shipping_method_id = '03';  // UPS Ground Shipping
            $shipping_amount = 0;
          }

          // Make a shipping line item if we have determined any shipping.
          if ($shipping_method_id) {
            $this->ow->addShippingLineItem($shipping_amount, $shipping_method_id);
          }
          // Not shippable - mark as completed.
          else {
            commerce_order_status_update($this->ow->getOrder(), 'completed', TRUE, TRUE, 'UWorld API: imported non-shippable order. Marked completed.');
          }

          // If UWorld flags as DoNotShip the order is complete
          if ($this->doNotShip) {
            commerce_order_status_update($this->ow->getOrder(), 'completed', TRUE, TRUE, 'UWorld API: DO NOT SHIP - marked completed.');
          }

          // We'll mark the order as 'pending', unless it was already set as completed or shipped. This could happen
          // if we are reprocessing an order that's already been shipped. We won't print a shipping label for the
          // same reason.
          if ($this->ow->getStatus() != 'completed' && $this->ow->getStatus() != 'shipped' && $this->ow->getStatus() != 'cancelled') {
            commerce_order_status_update($this->ow->getOrder(), 'pending', TRUE, TRUE, 'UWorld API: shippable order imported. Marked pending.');
            rcpar_ups_create_label($this->ow->getId(), FALSE, TRUE);
          }
        }

        // Process discounts, if we have any
        $this->processDiscount();

        // Add the payment
        $this->addPayment();
        break;

      // 2 Refund
      case 2:
      // 5 InvoiceAdjustment (process like refund) - These shouldn't be sent to us, but Chandresh asked that we handle the code case anyway.
      case 5:
        // Add the payment - this will actually be a credit
        $this->addPayment();

        // If the refund equals the order amount, it's a full refund and should be cancelled.
        $totals = commerce_payment_order_balance($this->ow->getOrder());

        // When no totals come back, there is nothing on the order, so mark it canceled
        if (!$totals) {
          commerce_order_status_update($this->ow->getOrder(), 'cancelled', TRUE, TRUE, 'UWorld API: refund was processed with no balance remaining - auto cancelled.');
        }
        // If the we're refunding the cost of the order, mark it as cancelled. There seem to be occasional one cent
        // rounding issues that occur with the API because of the distribution of sales tax (I think) credit to each
        // line item. So if the difference between the order total and the order balance is one penny, we can assume
        // all charges have been reversed and cancel it.
        else {
          $orderTotal = $this->ow->getCommerceOrderTotal();
          if ($orderTotal['amount'] - $totals['amount'] <= 1) {
            commerce_order_status_update($this->ow->getOrder(), 'cancelled', TRUE, TRUE, 'UWorld API: refund was processed with no balance remaining - auto cancelled.');
          }
        }

        // It's also theoretically possible to have a partial refund, although this isn't really a business use case
        // that we currently support much, if at all. Brad Rynes said it doesn't really happen, but he would just
        // process a payment credit and manipulates entitlements if needed, and leave the line items as-is. Shipping
        // in this case could be tricky, we'll just have to rely on old fashioned human communication if it happens.
        break;

      // 3 Void
      case 3:
        $this->voidPayment();
        commerce_order_status_update($this->ow->getOrder(), 'cancelled', TRUE, TRUE, 'UWorld API: order was voided - auto cancelled.');
        break;

      // 4 ChargeBack (process like a full refund)
      case 4:
        // Add the payment - this will actually be a credit
        $this->addPayment();
        commerce_order_status_update($this->ow->getOrder(), 'cancelled', TRUE, TRUE, 'UWorld API: chargeback - auto cancelled.');
        break;

      // 6 ChargeBack Reversal (process like charge)
      // In this case, it takes 15 days for a chargeback to process, so the order has already been  shipped if it was
      // shippable, so we just need to change the status back to 'completed'.
      case 6:
        // Re-add the payment
        $this->addPayment();
        commerce_order_status_update($this->ow->getOrder(), 'completed', TRUE, TRUE, 'UWorld API: chargeback reversal - automatically moved to completed.');
        break;
    }

    // Save the order
    $this->ow->save();
  }
  

  /**
   * Determine the UWorld order ID. On their side, this is the ID in the paymentID when an order is created.
   * Subsequent references to that order in UWorld (in the case of adjustments, refunds, etc) will have a unique
   * payment ID, but refer to the original paymentID in a field called originalPaymentId. So we use originalPaymentId
   * if we have it, otherwise we use paymentId.
   * @throws Exception
   */
  protected function determineUworldOrderId() {
    $orderUpdateData = $this->orderUpdateData;

    if (!empty($orderUpdateData->originalPaymentId)) {
      $uwOrderId = $orderUpdateData->originalPaymentId;
    }
    else {
      $uwOrderId = $orderUpdateData->paymentId;
    }

    if (empty($uwOrderId)) {
      throw new Exception('Could not find a UWorld payment ID, which is required to process the order.');
    }

    $this->uwOrderId = $uwOrderId;
  }

  /**
   * Make a request to the UWorld API for order imports.
   *
   * @param int $from
   * - Beginning of the data range to request data for. UNIX timestamp.
   * @param int $to
   * - Beginning of the data range to request data for. UNIX timestamp.
   * @param int $orderId
   * - A UWorld order ID
   * @param int $userId
   * - A UWorld user ID
   * @return string
   * - If successful, returns an unencoded string of JSON data from the API. If not, returns the output from
   * curl_error().
   */
  public static function apiRequest($from = NULL, $to = NULL, $orderId = NULL, $userId = NULL) {
    // The API endpoint URL
    $url = variable_get('uworld_api_url', 'https://api.uworld.com') . '/service/GetOrders';

    // Build the request parameters
    $data = json_encode(
      array_filter( // Remove any empty parameters from the data array
        array(
          'from' => $from,
          'to' => $to,
          "orderId" => $orderId,
          "userId" => $userId,
        )
      )
    );

    // Set HTTP headers and method
    $method = 'POST';
    $headers = array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data),
      'x-api-key: ' . variable_get('uworld_api_key', '46f6a85d-80c4-4587-bf81-c48cf7a52a80'),
    );

    return UWorldImport::curlRequest($url, $headers, $method, $data);
  }


  /**
   * Tries to match to a user in the system based on email address, and if not successful will create a new user.
   */
  public function determineUser() {
    // Try to load an existing Drupal user
    $this->u = new UserWrapper($this->rogerUid);
    if ($this->u->isLoaded()) {
      $this->existingUser = TRUE;
      return;
    }

    // If an existing user was specified that doesn't exist in our system, that's a problem...
    if (!empty($this->rogerUid) && !$this->u->isLoaded()) {
      throw new Exception('Service specified a user ID that did not exist');
    }

    // Next try to match a user by the UWorld user Id
    if(!empty($this->uwUserId)) {
      $u = UserWrapper::FindUserByUworldUid($this->uwUserId);
      if($u && $u->isLoaded()) {
        $this->u = $u;
        $this->existingUser = TRUE;
        return;
      }
    }

    // If we already have the user, we're good.
    if ($this->u->isLoaded()) {
      return;
    }

    // Try to match with an existing email address just in case. This could happen especially if we are re-running an import.
    $uid = db_query("SELECT uid FROM users WHERE mail = :mail", [':mail' => $this->email])->fetchField();

    // If that didn't work, we may have a user whose username matches the email address. Since usernames are a unique DB
    // key, we want to load this user because creating a new user with that email as the username will cause a SQL error
    if (!$uid) {
      $uid = db_query("SELECT uid FROM users WHERE name = :mail", [':mail' => $this->email])->fetchField();
    }

    // We found a matching user, so load them up and bail.
    if ($uid) {
      $this->existingUser = TRUE;
      $this->u = new UserWrapper($uid);
      if ($this->u->isLoaded()) {
        return;
      }
    }

    // We need to create a new user in the system for this order.
    $this->u = UserWrapper::create()
                          ->setName($this->email)
                          ->setMail($this->email)
                          ->setUwuid($this->uwUserId);

    if ($this->partnerSfId) {
      // if contact is created for this order, we need to set the order account
      // as contact accont instead of default account (on rcpar_salesforce.tokens.inc)
      $this->u->setPartnerSfId($this->partnerSfId);
    }
    
    // If we're making a new user, we'll fill basic user info from their shipping data, if we have it.
    if (!empty($this->shipping)) {
      $this->u->setFirstName($this->shipping->firstName)
              ->setLastName($this->shipping->lastName)
              ->setPhoneNumber($this->shipping->telephone)
              ->setCountry($this->shipping->country)
              ->setStudentState($this->shipping->state);

    }
  }

  /**
   * when abandone order happend, we have lack of informacion
   * so we make sure we have that already or is not valid.
   * @return FALSE If not valid uid
   */
  public function getRogerUid() {
    if (!empty($this->uwUserId) && empty($this->rogerUid)) {
      $u = UserWrapper::FindUserByUworldUid($this->uwUserId);
      if ($u && $u->isLoaded()) {
        $this->rogerUid = $u->getId();
        $this->u = $u;
        $this->email = $u->get('mail');
        $this->existingUser = true;
        return $this->rogerUid;
      }
    }
    return false;
  }

  /**
   * If we didn't just create the user, this method updates their basic data from data in the order.
   */
  public function updateUserData() {
    $u = $this->u;

    // We need to store/update the UW user ID, and our email should always match theirs.
    $u->setUwuid($this->uwUserId)
      ->setMail($this->email)
      ->setStatus(1)
      ->save();
  }

  /**
   * Try to find an RCPAR order that matches the provided UWorld order ID.
   */
  public function determineOrder() {
    $ow = CommerceOrderWrapper::FindOrderByUworldId($this->uwOrderId);

    // We have an existing order
    if ($ow) {
      $this->existingOrder = TRUE;
    }
    // Create a new order
    else {
      $this->existingOrder = FALSE;
      // Save the order to get its ID (we need it to create line items)
      $ow = CommerceOrderWrapper::createNew($this->u->getUid(), 'pending');
    }
    $this->ow = $ow;
  }

    /**
   * Transfer basic data from the API to our order object. Basically everything but line items and payments.
   */
  public function updateOrderBasics() {
    $ow = $this->ow;

    $ow->setCreated($this->dateCreated);
    $ow->setPlaced($this->dateCreated);
    $ow->setMail($this->email);
    $ow->setUwOrderId($this->uwOrderId);

    // Set these raw partner-based values even if the partner lookup fails
    if(!is_null($this->partnerName)){
      $ow->setUwPartnerName($this->partnerName);
    }
    if(!is_null($this->partnerSfId)){
      $ow->setUwSfId($this->partnerSfId);
    }

    // Partner lookup
    // We could probably take a good guess at the partner by loading the first one that comes up matching this SF ID,
    // but we're not going to. It may not match, and it's been decided that the most important thing is attribution
    // to the correct SF account for proper invoicing.
    /*if (!empty($this->partnerSfId)) {
      if ($partner_id = RCPARPartner::searchBySfId($this->partnerSfId)) {
        $ow->setPartnerProfile($partner_id);
      }
    }*/
  }

  /**
   * Create a new, blank shipping and billing profile and attach it to the order.
   */
  public function createBillingShippingProfile() {
    if (!empty($this->billing)) {
      $this->ow->setCommerceCustomerBilling($this->createProfile('billing'));
    }

    if (!empty($this->shipping)) {
      $this->ow->setCommerceCustomerShipping($this->createProfile('shipping'));
    }
  }

  /**
   * Create a new billing or shipping profile
   *
   * @param string $type
   * - "billing" or "shipping".
   * @return object
   * - A commerce_customer_profile entity
   */
  protected function createProfile($type) {
    // Make and wrap a new profile
    $new_profile = commerce_customer_profile_new($type);
    $wrapper = entity_metadata_wrapper('commerce_customer_profile', $new_profile);

    // Transfer billing data to it and save
    $this->transferProfile($wrapper, $type);
    commerce_customer_profile_save($new_profile);
    return $new_profile;
  }

  /**
   * Transfer the data from the billing or shipping info into the given entity wrapper
   *
   * @param EntityMetadataWrapper $wrapper
   * @param string $type
   * - "billing" or "shipping"
   */
  public function transferProfile($wrapper, $type) {
    if ($type == 'shipping') {
      $data = $this->shipping;
    }
    else {
      $data = $this->billing;
    }

    $wrapper->uid = $this->u->getUid();
    $wrapper->commerce_customer_address->country = $data->country;
    $wrapper->commerce_customer_address->name_line = $data->firstName . ' ' . $data->lastName;
    $wrapper->commerce_customer_address->first_name = $data->firstName;
    $wrapper->commerce_customer_address->last_name = $data->lastName;
    $wrapper->commerce_customer_address->postal_code = $data->zipcode;
    $wrapper->commerce_customer_address->thoroughfare = $data->address1;
    $wrapper->commerce_customer_address->premise = $data->address2;
    $wrapper->commerce_customer_address->locality = $data->city;
    $wrapper->field_phone = $data->telephone;
    $wrapper->commerce_customer_address->administrative_area = $data->state;
  }

  /**
   * Update existing billing and shipping profiles with data from the order
   */
  public function updateBillingShippingProfile() {
    $ow = $this->ow;

    if (!empty($this->billing)) {
      $billingWrapper = entity_metadata_wrapper('commerce_customer_profile', $ow->getCommerceCustomerBilling());
      try {
        $this->transferProfile($billingWrapper, 'billing');
      }
      catch (Exception $e) {
        // If we fail to update the profile, it's not the end of the world.
      }
    }

    if (!empty($this->shipping)) {
      $shippingWrapper = entity_metadata_wrapper('commerce_customer_profile', $ow->getCommerceCustomerShipping());
      try {
        $this->transferProfile($shippingWrapper, 'shipping');
      }
      catch (Exception $e) {
        // If we fail to update the profile, it's not the end of the world.
      }
    }
  }

  /**
   * Create line items for the products/packages in this order and add them to the order.
   */
  public function addProductLineItemsToOrder() {
    $ow = $this->ow;

    // Iterate over each line item and mark what package it belongs to
    $packageIds = [];
    foreach ($this->lineItems as $item) {
      $packageIds[$item->packageId] = $item->packageId;
    }

    // Add each of the packages to our order
    foreach ($packageIds as $packageId) {
      $sku = self::$productMap[$packageId];

      if (is_null($sku)) {
        // No product mapping - I'm not sure if it can actually happen, but it seems theoretically possible that
        // non-CPA products could be present in the order. I have seen this in testing on the users side. If we
        // encounter one, we won't throw an error, but will skip.
        continue;
      }

      // Load the product that this package ID maps to
      $product = commerce_product_load_by_sku($sku);
      if (!$product) {
        // Missing local product
        throw new Exception("Tried to load local product: $sku, but it does not exist.");
      }

      // Add bundle
      if (RCPARCommerceBundle::isProductIdBundle($product->product_id)) {

        // Package id 318 is a special case 30-day "Elite" free trial. Unlike a real Elite course, however, the package
        // only includes a Full regular course and Full cram course as configured in the "elite_30_free" partner
        if ($packageId == 318) {
          $partner_helper = new RCPARPartner('elite_30_free');
          $partner_products = $partner_helper->getProductsInfo();
          foreach ($partner_products['bundle_parts'] as $prod_info) {
            $ow->addProductLineItem($prod_info['sku'], TRUE);
          }
          // In order for our Salesforce lead status to work (@see rcpar_salesforce_get_lead_status_partner()) we need
          // an association to the partner.
          $ow->setPartnerProfile($partner_helper->getNid());
          $ow->getEntityMetadataWrapper()->commerce_line_items[] = $partner_helper->generatePartnerDisLineItem();
        }
        else {
          $rcb = new RCPARCommerceBundle($product->product_id);
          $rcb->addToOrder($ow->getEntityMetadataWrapper());
          // Book versioning.
          $this->processTextbookVersions($rcb, $packageId);
        }

        // For these demo/promo packages, we indicate a free trial in order to prevent shipping, but we don't want the
        // free trial SKUs. These orders are $0 because they have coupons that zero the balance, but otherwise look like
        // an elite package.
        // Roger CPA Review Elite Demo
        // Roger CPA Review Elite - Promo
        if($packageId == 298 || $packageId == 300) {
          $this->freeTrialIsPresent = TRUE;
        }

        // In the case of free trials, the discount line item is not present in the bundle. This is because rcpar_asl
        // reads the partner configuration and the bundle is no longer used. I could have added it to the bundle, but
        // wasn't sure if there would be consequences, so we do what rcpar_asl_create_order() does and create it.
        if ($sku == 'FREETRIAL-BUNDLE') {
          $ow->addProductLineItem($sku, TRUE);
          // In order for our Salesforce lead status to work (@see rcpar_salesforce_get_lead_status_partner()) we need
          // an association to the partner. In the case of free trial, we know what this is and can inject it manually.
          $ow->setPartnerProfile(RCPAR_ASL_FREE_TRIAL_PARTNER_ID);
          $this->freeTrialIsPresent = TRUE;
        }
      }
      // Or if it's not a bundle, add the product
      else {
        $ow->addProductLineItem($sku, TRUE);
      }
    }
  }

  /**
   * @param object $order
   * @param int $transactionTypeId
   * @param int $paymentModeId
   * @param int $gatewayId
   * @param int $transactionId
   * @param int $amount
   * - A regular dollar amount, like $99.95
   */
  public function addPayment() {
    $paymentId = $this->paymentId;
    $transactionTypeId = $this->transactionTypeId;
    $paymentModeId = $this->paymentModeId;
    $gatewayId = $this->gatewayId;
    $transactionId = $this->transactionId;
    $amount = $this->transactionAmount;

    // If we have already processed this payment, remove it before reprocessing
    if ($txId = $this->getPaymentByUwPaymentId($this->paymentId)) {
      commerce_payment_transaction_delete($txId);
    }

    // When the transaction type is refund, void, or chargeback, we're actually giving back money, so the amount
    // should be made negative.
    if (in_array($transactionTypeId, [2, 3, 4, 5])) {
      $amount *= -1;
    }

    /* TransactionTypes (transactionTypeId) */
    $transactionTypes = array(
      1 => 'Charge',
      2 => 'Refund',
      3 => 'Void',
      4 => 'ChargeBack',
      5 => 'InvoiceAdjustment', // Shouldn't ever be present.
      6 => 'ChargeBack Reversal',
    );

    /* PaymentModes (paymentModeId) */
    $paymentModes = array(
      1 => 'Credit Card',
      2 => 'Check',
      3 => 'Wire/ACH',
      4 => 'Bank Draft',
      5 => 'Money Order',
      6 => 'Store Credit',
      7 => 'Finance',
    );

    /* GatewayId (gatewayId) */
    $gateways = array(
      1 => 'Authorize',
      2 => 'PayPal',
      3 => 'Authorize.Net Old',
      4 => 'BrainTree',
      5 => 'Affirm',
    );

    // Build a JSON encoded message to store with the payment. We can decode this later if for some reason we need to
    // do some additional processing or understand details about the payment.
    $data = [
      'paymentId' => $paymentId,
      'transactionType' => $transactionTypes[$transactionTypeId],
      'transactionTypeId' => $transactionTypeId,
      'paymentMode' => $paymentModes[$paymentModeId],
      'paymentModeId' => $paymentModeId,
      'gateway' => $gateways[$gatewayId],
      'gatewayId' => $gatewayId,
      'transactionId' => $transactionId,
      'amount' => $amount,
    ];
    $message = json_encode($data);

    // For reporting purposes, we keep a separate UWorld payment method, but we need to know what the equivalent RCPAR
    // payment would be, if this were transacted on our side. We store this data in the transaction instance_id for
    // later processing/querying in the data warehouse.
    $mappedPayment = self::getPaymentMapping($data);

    // Now, we need to create a payment
    $transaction = commerce_payment_transaction_new('enroll_flow_uworld_payment', $this->ow->getId());
    $transaction->instance_id = "enroll_flow_uworld_payment|$mappedPayment";
    $transaction->currency_code = $this->ow->getEntityMetadataWrapper()->commerce_order_total->currency_code->value();
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->amount = $amount * 100; // This needs to be a commerce amount, so we multiply by 100 to slide over the decimal point.
    $transaction->message = $message;
    $transaction->remote_id = $paymentId;
    $transaction->created = $this->dateCreated;
    commerce_payment_transaction_save($transaction);
  }

  /**
   * Given payment data from UWorld, return the equivalent payment method in our system.
   * @param array $data
   * - This is an array of data that is created in addPayment() by collecting all of the related payment information
   * returned from the API.
   * @return string
   * - The equivalent RCPAR payment method. Possible values: 'rcpar_partners_payment_freetrial', 'authnet_aim',
   * 'affirm', 'rcpar_pay_by_check'.
   */
  public static function getPaymentMapping(array $data) {
    // Zero dollar transactions typically come from free trials, but even if they don't, the same logic would apply in
    // our system where this payment type is applied.
    if($data['amount'] == 0) {
      return 'rcpar_partners_payment_freetrial';
    }

    // Although UWorld has other credit card processors besides authorize.net (like braintree, etc) we will always map
    // to auth.net since that's the only one we have.
    if($data['paymentModeId'] == 1) {
      return 'authnet_aim';
    }

    // Affirm
    if($data['gatewayId'] == 5) {
      return 'affirm';
    }

    // Check
    if($data['paymentModeId'] == 2) {
      return 'rcpar_pay_by_check';
    }

    /*
     * All available RCPAR payment methods:
     * authnet_aim
     * rcpar_partners_payment
     * affirm
     * rcpar_pay_by_check
     * rcpar_partners_payment_prepaid_credit
     * rcpar_partners_payment_prepaid_debit
     * rcpar_partners_payment_freetrial
     */

    // If we weren't able to find an appropriate mapping, we'll just return a blank.
    return '';
  }

  protected function getPaymentByUwPaymentId($uwPaymentId) {
    // If we have already processed this payment, remove it before reprocessing
    $txId = db_query("SELECT transaction_id FROM commerce_payment_transaction WHERE order_id = :oid AND remote_id = :pid", [
      ':oid' => $this->ow->getId(),
      ':pid' => $uwPaymentId,
    ])->fetchField();

    return $txId;
  }

  /**
   * Void the current payment. By marking the amount as 0, set 'status' to 'failure', 'remote_status' to 'void'.
   */
  public function voidPayment() {
    if ($txId = $this->getPaymentByUwPaymentId($this->paymentId)) {
      $transaction = commerce_payment_transaction_load($txId);
      $transaction->amount = 0;
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->remote_status = 'void';
      $transaction->message .= " \nTransaction voided via API import.";
      commerce_payment_transaction_save($transaction);
    }
  }

  /**
   * Create a discount coupon if necessary or apply an existing coupon.
   */
  public function processDiscount() {
    $discountCodes = [];

    // Search through all of the order line items to see if we have any discount codes
    foreach ($this->lineItems as $lineItem) {
      if (!empty($lineItem->discountCode)) {
        // UWorld may have the same discount code applying different amounts across different line items. They may
        // also have multiple discount codes per order. We'll store the total amount of discount yielded from each
        // code.
        $discountCodes[$lineItem->discountCode] += $lineItem->discountAmount;
      }
    }

    // Sometimes UWorld will add a discount to the entire order without any discount code
    // or amount on the line items. In these circumstances we will create a new coupon to cover the
    // difference between discount and retail price.
    if (empty($discountCodes) && $this->discountOrderAmount > 0) {

      // Often times the discountOrderAmount is either too munch or too little to cover
      // the monetary difference between the total price of the order and the amount actually paid.
      // Since we know that UWorld will never send an order with a balance due, we'll ensure
      // that the difference between the order total and the amount paid is covered by the discount.
      $line_items = $this->ow->getEntityMetadataWrapper()->commerce_line_items;
      $order_total = commerce_line_items_total($line_items);
      $order_amount = $order_total['amount']/100;
      $discount = $order_amount - $this->transactionAmount;

      // We need to generate a unique, reportable coupon code name for the discount line item we will add
      // in the next conditional statement.
      $discountCodes["UWDISCOUNT-" . $this->uwOrderId] = $discount;
    }

    // If we found discount codes, check to see if they exist in our system
    $couponRefs = [];
    if (!empty($discountCodes)) {
      foreach (array_keys($discountCodes) as $code) {
        // If we don't have the code, let's create it. In theory this shouldn't happen though.
        if (!$coupon = commerce_coupon_load_by_code($code)) {
          $coupon = commerce_coupon_create('commerce_coupon_fixed');
          $coupon->commerce_coupon_code[LANGUAGE_NONE][0]['value'] = $code;
          $coupon->commerce_coupon_number_of_uses[LANGUAGE_NONE][0]['value'] = 99999;
          $coupon->is_active = 1;
          $coupon->field_description[LANGUAGE_NONE][0]['value'] = 'Created programmatically via UWorld order importer.';
        }
        // Update the coupon price for a new or existing coupon and save.
        $coupon->commerce_coupon_fixed_amount[LANGUAGE_NONE][0] = array(
          'amount' => $discountCodes[$code] * 100,
          'currency_code' => commerce_default_currency()
        );
        commerce_coupon_save($coupon);

        // In theory we should be able to assume that our discount code amount matches theirs, but just to prevent any
        // accounting issues, this method allows us to specify the amount that we tallied from the API order data.
        $this->ow->addFixedCouponLineItem($coupon, $discountCodes[$code] * 100);

        $couponRefs[] = $coupon;
      }

      // Add entity references to the coupons in the order via our entity ref field.
      $this->ow->setCommerceCouponOrderReference($couponRefs);
    }
  }

  /**
   * Update the RCPAR bundle line items to match the textbook versions ordered on UWorld
   * @param object $rcb
   *  - RCPAR commerce bundle
   * @param int $packageId
   *  - UWorld packageId
   */
  public function processTextbookVersions($rcb, $packageId) {
    $ow = $this->ow;

    // Setup an array of UWorld version book skus (i.e. "FAR-BK-2020") keyed by the course section
    // Note that productTaxCode 81110 is UWorld's book id
    $uw_book_skus = array();
    foreach ($this->lineItems as $item) {
      if (isset($item->version)
        && $item->packageId == $packageId
        && array_key_exists($item->courseId, self::$courseMap)
        && $item->productTaxCode == 81110) {
        $uw_book_skus[self::$courseMap[$item->courseId]] = self::$courseMap[$item->courseId] .'-BK-'. $item->version;
      }
    }

    // Get an array of book skus from our RCPAR order bundle keyed by course section
    $line_items = $ow->getCommerceLineItems();
    $bundle_book_skus = array();
    foreach ($line_items as $line_item) {
      if (strpos($line_item->line_item_label, '-BK-') !== false ) {
        $section = substr($line_item->line_item_label, 0, 3);
        $bundle_book_skus[$section] = $line_item->line_item_label;
      }
    }

    // If we have books in the order, make sure they match the skus we created from UWorld's versioning
    if (!empty($bundle_book_skus) && !empty($uw_book_skus)) {
      foreach ($bundle_book_skus as $section => $bundle_book_sku) {
        // If the books in the bundle don't match what was requested by UWorld, delete the old book
        // version and add the requested book version
        //also validate that both sides have the same quantity
        if (isset($uw_book_skus[$section]) && ($uw_book_skus[$section] != $bundle_book_skus[$section])) {
          $ow->deleteProductLineItem($bundle_book_sku, TRUE);
          // If we have a bundle, get its line item metadata
          $line_item_data = [];
          if (is_object($rcb)) {
            $line_item_data = $rcb->getLineItemMetaData();
          }
          $ow->addProductLineItem($uw_book_skus[$section], TRUE, $line_item_data);
        }
      }
    }

  }
}
