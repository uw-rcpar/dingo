<?php

namespace RCPAR\Import;

class UWorldImport {

  /**
   * Import order and user data from UWorld.
   *
   * @param int $from
   * - UNIX timestamp for the beginning of the time range to perform the import
   * @param int $to
   * - UNIX timestamp for the end of the time range to perform the import
   */
  public static function import($from = NULL, $to = NULL, $updateLastTime = FALSE, $emailErrors = FALSE) {
    // Supply default values for testing purposes. Normally, this method should be used with parameters
    if (empty($from)) {
      $from = strtotime('June 1, 2019');
      $to = strtotime('June 14, 2019');
    }

    // Date format for display
    $format = 'M jS, Y - g:ia';

    // Formatted dates for later display
    $from_formatted = date($format, $from);
    $to_formatted = date($format, $to);

    // Get data from the API    
    $orderLog = UWorldOrderProcessor::import($from, $to);
    $userLog = UWorldUserProcessor::import($from, $to);
    $orderAbandonLog = UWorldOrderAbandonProcessor::importCartAbandonment($from, $to);

    // Set the time that the next import will begin. We update this even if there were errors. We don't want the importer
    // to stop running, and we can re-run batches that previously failed.
    if ($updateLastTime) {
      UWorldImport::setLastImportTime($to);
    }

    /** @var UWorldImportLog $log */
    foreach (array($orderLog, $userLog, $orderAbandonLog) as $log) {
      $type = $log->get('type');
      if ($log->getHasFailures()) {
        $msg = "Errors occurred while trying to import <em>$type</em> data for timespan: $from_formatted - $to_formatted" .
          '<strong>Messages:</strong><pre>' . $log->getMessage() . '</pre><hr>' .
          '<strong>Log id:' . $log->getId() . '</strong><br>' .
          'See more at: ' . url('admin/settings/rcpar/system_settings/enroll_flow/imports', array('absolute' => TRUE));
        drupal_set_message($msg, 'error');

        // Notify an admin of the problem via email
        if ($emailErrors) {
          $emailto = variable_get('uworld_api_email_list', 'jdodge@rogercpareview.com,patkins@rogercpareview.com,staylor@rogercpareview.com');
          $emailfrom = variable_get('site_mail', '');
          $body = $msg;
          drupal_mail('system', 'uworld_order_user_import', $emailto, language_default(), array('context' => [
            'message' => $body,
            'subject' => "UWorld Import Errors - $type | $from_formatted - $to_formatted | ID: {$log->getId()}",
          ]), $emailfrom);
        }
      }
      else {
        drupal_set_message("Sucessful import of <em>$type</em> data for timespan: $from_formatted - $to_formatted");
      }
    }
  }

  /**
   * Rerun an import based on the parameters from a recorded import log.
   * @param int $logid
   */
  public static function rerun($logid) {
    $log = UWorldImportLog::loadFromId($logid);
    UWorldImport::import($log->get('start_time'), $log->get('end_time'), FALSE, FALSE);
  }

  /**
   * The last recorded time that an import ran.
   * @return int
   * - UNIX timestamp.
   */
  public static function getLastImportTime() {
    return variable_get('uworld_order_last_import', strtotime('June 1st, 2019'));
  }

  /**
   * Set the time that the import last completed.
   * @param int $time
   * - UNIX timestamp.
   */
  public static function setLastImportTime($time) {
    return variable_set('uworld_order_last_import', $time);
  }

  /**
   * The width of time to request data from. Data will be requested from the last import time until the last import
   * time plus this range of time.
   * Defaults to 15 minutes.
   * @return int
   * - A number of seconds
   */
  public static function getImportWidth() {
    return variable_get('uworld_order_range_width', 15 * 60);
  }

  /**
   * Make a curl request.
   *
   * @param string $url
   * - The request URL
   * @param array $headers
   * - An array of request headers, like:
   *   array(
   *     'Content-Type: application/json',
   *     'Content-Length: ' . strlen($data),
   *     'x-api-key: 46f6a85d-80c4-4587-bf81-c48cf7a52a80',
   *   )
   * @param string $method
   * - HTTP method: "GET", "POST", "PUT", etc.
   * @param string $data
   * - Request data
   * @return string
   * - The returned output from the request, or the contents of curl_error if there is an error.
   */
  public static function curlRequest($url, $headers, $method, $data) {
    // create curl resource
    $ch = curl_init($url);

    // Set url, http method, and headers
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // Be permissive with SSL and return the transfer as a string
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // Log the request to watchdog
    $message = "Outbound API request to: $url |
    Method: $method  |
    Headers: " . var_export($headers, TRUE)  . "|
    Data: $data |
    Response: $output";
    watchdog('uw_api_request', $message);

    // Catch errors
    if ($error = curl_error($ch)) {
      return $error;
    }

    // Close curl resource to free up system resources
    curl_close($ch);

    return $output;
  }
}