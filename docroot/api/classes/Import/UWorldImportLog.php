<?php


namespace RCPAR\Import;

use RCPAR\Modeling\BaseModel;

class UWorldImportLog extends BaseModel {
  public static $tableName = 'enroll_flow_uw_import_log';

  public static $TYPE_USERS = 'users';
  public static $TYPE_ORDERS = 'orders';
  public static $TYPE_ABANDON_ORDERS = 'abandonOrders';


  /**
   * UWorldImportLog constructor.
   * @param string $type
   * - "orders" or "users"
   * @param int $start
   * - Timestamp start range
   * @param int $end
   * - Timestamp end range
   */
  public function __construct($type, $start, $end) {
    // See if we have an existing log record for this segment
    $id = db_query(
      "SELECT id FROM enroll_flow_uw_import_log WHERE type = :t AND start_time = :st AND end_time = :et",
      [':t' => $type, ':st' => $start, ':et' => $end]
    )->fetchField();

    // Construct with the ID if we have it, otherwise the parent will create us a blank entity.
    if ($id) {
      parent::__construct($id);
    }
    else {
      parent::__construct();
      $this->set('type', $type);
      $this->set('start_time', $start);
      $this->set('end_time', $end);
      $this->set('first_attempted', REQUEST_TIME);
      $this->set('last_attempted', REQUEST_TIME);
    }

    // Unserialize data for use
    $this->set('failed_ids', json_decode($this->get('failed_ids')));
  }

  public static function loadFromId($id) {
    $result = db_query("SELECT type, start_time, end_time FROM enroll_flow_uw_import_log WHERE id = :id", [':id' => $id])->fetchObject();
    return new self($result->type, $result->start_time, $result->end_time);
  }

  /**
   * Reset values and set attempt time for a new attempt
   * @param int $time
   * - Timestamp for the time to begin the attempt. Defaults to request time.
   */
  public function newAttempt($time = NULL) {
    if (is_null($time)) {
      $time = REQUEST_TIME;
    }

    $this->set('last_attempted', $time);
    $this->set('message', '');
    $this->incrementAttempts();
    $this->resetFailureIds();
    $this->resetRecordsSuccessful();
    $this->setHasFailures(FALSE);
  }

  protected function incrementAttempts() {
    $attempts = $this->get('attempts');

    if (empty($attempts)) {
      $attempts = 0;
    }

    $attempts++;

    $this->set('attempts', $attempts);
  }

  protected function resetRecordsSuccessful() {
    $this->set('records_successful', 0);
  }

  public function incrementRecordsSuccessful() {
    $i = $this->get('records_successful');

    if (empty($i)) {
      $i = 0;
    }

    $i++;

    $this->set('records_successful', $i);
  }

  /**
   * Set true when there are any errors in the import segment.
   * @param bool $bool
   */
  public function setHasFailures($bool) {
    if ($bool) {
      $this->set('has_failures', 1);
    }
    else {
      $this->set('has_failures', 0);
    }
  }

  protected function resetFailureIds() {
    $this->set('failed_ids', array());
  }

  public function addFailureId($id) {
    $data = $this->get('failed_ids');
    $data[] = $id;
    $data = array_unique($data);

    $this->set('failed_ids', $data);
  }

  public function addMessage($msg) {
    $data = $this->get('message');
    $data .= '<br>' . $msg;
    $this->set('message', $data);
  }

  public function getHasFailures() {
    return $this->get('has_failures');
  }
  public function getMessage() {
    return $this->get('message');
  }
  public function getRecordsAvailable() {
    return $this->get('records_available');
  }
  public function getRecordsSuccessful() {
    return $this->get('records_successful');
  }

  public function save() {
    // Serialize data prior to save
    $this->set('failed_ids', json_encode($this->get('failed_ids')));

    parent::save();

    // Unserialize again for further use
    $this->set('failed_ids', json_decode($this->get('failed_ids')));
  }

}