<?php


namespace RCPAR\Import;

use Exception;
use RCPAR\WrappersDelight\CommerceOrderWrapper;

class UWorldOrderAbandonProcessor extends UWorldOrderProcessor {
  public static $skipKey = 'skip';

  public $isDeleted;
  /**
   * UWorldOrderAbandonProcessor constructor.
   *
   * @param array $orderUpdateData
   * - JSON decoded data for an individual "payment" from the UWorld API
   * @throws Exception
   */
  public function __construct($orderUpdateData) {
    $this->orderUpdateData = $orderUpdateData;

    // Basic order identifiers
    $this->uwUserId = $orderUpdateData->userId;

    // Order properties
    $this->dateCreated = $orderUpdateData->dateCreated;
    $this->isDeleted = $orderUpdateData->isDeleted;
  }

  /**
   * Import and log data from the UWorld Abandon orders API.
   *
   * @param int $from
   * - Beginning data range to query the API for. UNIX timestamp.
   * @param int $to
   * - Ending data range to query the API for. UNIX timestamp.
   * @param int $orderId
   * - Optionally filter by a specific order. This is a UWorld order ID.
   * @param int $userId
   * - Optionally filter by a specific user. This is a UWorld user ID.
   * @return UWorldImportLog
   * - The log object with details about the import status.
   */
  public static function importCartAbandonment($from = NULL, $to = NULL, $orderId = NULL, $userId = NULL) {
    // Build a new log entry for this segment, or find an existing one.
    $log = new UWorldImportLog(UWorldImportLog::$TYPE_ABANDON_ORDERS, $from, $to);
    $log->newAttempt();

    // Save the log now, just in case anything else fails
    $log->save();

    // Get data from the Orders API endpoint
    $data = self::apiRequest($from, $to, $orderId, $userId);
    //$data = file_get_contents(__DIR__ . '/uworld.order.import.sample.json');

    // If the request fails, log issues and return.
    $jsonData = json_decode($data);
    if ($jsonData === NULL) {
      $log->set('message', 'API request failed or could not be decoded.');
      $log->set('completed', time());
      $log->setHasFailures(TRUE);
      $log->save();
      return $log;
    }

    // Sometimes the API response has errors when there is a remote problem
    if (isset($jsonData->error)) {
      $log->set('message', '<strong>Remote API error:</strong><br>' . $jsonData->error . '<br>' . $jsonData->error_description);
      $log->set('completed', time());
      $log->setHasFailures(TRUE);
      $log->save();
      return $log;
    }

    // Log the number of records available to process.
    $log->set('records_available', sizeof($jsonData->abandonOrders));

    // Process each order
    foreach ($jsonData->abandonOrders as $orderUpdateData) {
      try {        
        //we only process null payments since this will be handle by the regular order importer
        // If this cart order has been deleted on the UWorld side, we can ignore it - it will expire naturally via our own cart expiration system.
        if($orderUpdateData->paymentId === NULL && !$orderUpdateData->isDeleted){          
          $d = new UWorldOrderAbandonProcessor($orderUpdateData);
          $d->processCartAbandonment();
          $log->incrementRecordsSuccessful();
         
          if (is_null($d->ow) || !$d->ow->getId()) {
            throw new Exception('No Drupal order ID available after import, something went wrong.');
          }

          $log->addMessage('Abandoned cart: ' . l($d->ow->getId(), 'admin/commerce/orders/' . $d->ow->getId() . '/edit'));
        }
      }
      catch (Exception $e) {
        // Sometimes we intentionally skip because the API has users that don't seem to be CPA users and aren't in our system.
        if($e->getMessage() == UWorldOrderAbandonProcessor::$skipKey) {
          continue;
        }
        
        $log->addMessage($e->getMessage() . ' | Email:  ' . $d->email . ' | Payment ID: ' . $d->uwOrderId);
        $log->setHasFailures(TRUE);
        if (is_object($d) && !empty($d->uwOrderId)) {
          $log->addFailureId($d->uwOrderId);
        }
        continue;
      }
    }

    // Log as completed and save.
    $log->set('completed', time());
    $log->save();
    return $log;
  }
  
  /**
   * Process an order record - its billing, shipping, line items, tax, discounts, and payments.
   */
  public function processCartAbandonment() {
    if (!$this->getRogerUid($this->uwUserId)) {
      //throw new Exception('Abandoned orders specified a UW user ID that could not be loaded: ' . $this->uwUserId);      
      throw new Exception(self::$skipKey);
    }
    // Create or update the user for this order
    $this->updateUserData();

    // Determine if we have an existing order object or create a new one
    $this->determineAbandonOrder();

    // Update basic order details (created time, etc)
    $this->updateOrderBasics();

    // Existing order:
    if ($this->existingOrder) {
      // If the status of this order is no longer "cart", it has progressed to a completed purchase and is no longer abandoned, so we shouldn't modify it.
      if ($this->ow->getStatus() != 'cart') {
        return;
      }
    }
    
    // Start fresh. Especially important if we are re-running an import.
    $this->ow->deleteAllLineItems(TRUE); 
    
    // Remove existing and then create a single tax line item. Tax needs to be processed first because we need
    // to set a tax-related flag to prevent rcpar_commerce_views_order_save() breaking all kinds of stuff and
    // doing things that it shouldn't be doing.
    $this->ow->addAvataxLineItem($this->salesTaxAmount);

    // Product line items. Needs to be done prior to shipping so that the label creator can see products in the order.
    $this->addProductLineItemsToOrder();

    commerce_order_status_update($this->ow->getOrder(), 'cart', TRUE, TRUE, 'UWorld API: imported Abandoned order.');

    // Save the order
    $this->ow->save();
  }

    /**
   * Try to find an RCPAR order that matches the provided UWorld order ID.
   * if not creates one as a cart status
   */
  public function determineAbandonOrder() {
    $ow = CommerceOrderWrapper::FindOrderByUserId($this->rogerUid);

    // We have an existing order
    if ($ow && $ow->getOrder()->status === "cart") {
      $this->existingOrder = TRUE;
    }
    // Create a new order
    else {
      $this->existingOrder = FALSE;
      // Save the order to get its ID (we need it to create line items)
      $ow = CommerceOrderWrapper::createNew($this->u->getUid(), 'cart');
    }
    $this->ow = $ow;
  }

}