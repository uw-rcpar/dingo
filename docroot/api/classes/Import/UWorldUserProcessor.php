<?php


namespace RCPAR\Import;


use Exception;
use RCPAR\Entities\UserEntitlement;
use RCPAR\User;
use RCPAR\WrappersDelight\CommerceOrderWrapper;
use RCPARCommerceOrder;

class UWorldUserProcessor {
  public $uwUserId;
  public $uid;
  public $email;
  public $firstName;
  public $lastName;
  public $telephone;
  public $passHash;
  public $subscriptions;

  /** @var User $u */
  public $u;
  public $existingUser = FALSE;

  // Mapping for UWorld's "courseId" field
  public static $courseMap = array(
    28000 => 'AUD',
    29000 => 'BEC',
    30000 => 'FAR',
    31000 => 'REG',
  );

  public function __construct($data) {
    $this->uwUserId = $data->userId;
    $this->uid = $data->rogerUserId;
    $this->email = $data->emailId;
    $this->firstName = $data->firstName;
    $this->lastName = $data->lastName;
    $this->passHash = $data->password;
    $this->subscriptions = $data->subscriptions;
    $this->univSfId = $data->salesforceId;
    $this->univName = $data->schoolName;
    $this->associatedFirmName = $data->associatedFirm;
    $this->telephone = $data->telephone;

    $this->determineUser();

    // Coming soon: packageId, originalPaymentId
    /*
     "subscriptions": [
        {
         "subscriptionId": 5067641,
          "courseId": 28000,
          "dateCreated": 1559821704,
          "dateUpdated": null,
          "dateExpire": 1575373704,
          "dateActivated": 1559821704,
          "isActive": true,
          "paymentLineItemId": null,
          "durationInDays": 180
        }
      ]
    */
  }

  /**
   * Import and log data from the UWorld userse API.
   *
   * @param int $from
   * - Beginning data range to query the API for. UNIX timestamp.
   * @param int $to
   * - Ending data range to query the API for. UNIX timestamp.
   * @param int $userId
   * - Optionally filter by a specific user. This is a UWorld user ID.
   * @return UWorldImportLog
   * - The log object with details about the import status.
   */
  public static function import($from = NULL, $to = NULL, $userId = NULL) {
    // Build a new log entry for this segment, or find an existing one.
    $log = new UWorldImportLog(UWorldImportLog::$TYPE_USERS, $from, $to);
    $log->newAttempt();

    // Save the log now, just in case anything else fails
    $log->save();

    // Get data from the users API endpoint
    $subData = UWorldUserProcessor::apiRequest($from, $to, $userId);

    // If the request fails, log issues and return.
    $jsonData = json_decode($subData);
    if ($jsonData === NULL) {
      $log->set('message', 'API request failed or could not be decoded.');
      $log->set('completed', time());
      $log->setHasFailures(TRUE);
      $log->save();
      return $log;
    }

    // Sometimes the API response has errors when there is a remote problem
    if (isset($jsonData->error)) {
      $log->set('message', '<strong>Remote API error:</strong><br>' . $jsonData->error . '<br>' . $jsonData->error_description);
      $log->set('completed', time());
      $log->setHasFailures(TRUE);
      $log->save();
      return $log;
    }

    // Log the number of records available to process.
    $log->set('records_available', sizeof($jsonData));

    // Process each user
    foreach ($jsonData as $subDatum) {
      try {
        $p = new UWorldUserProcessor($subDatum);
        $p->updateUserData();
        $p->createUpdateEntitlements();

        if(!$p->u->getUid()) {
          throw new Exception('No Drupal user ID available after import, something went wrong.');
        }

        $log->incrementRecordsSuccessful();
        $log->addMessage('Imported user: ' . l($p->u->getUid(), 'customercare-screen/' . $p->u->getUid()));
      }
      catch (Exception $e) {
        $log->addMessage($e->getMessage() . ' | Email:  ' . $p->email);
        $log->setHasFailures(TRUE);
        if (is_object($p) && !empty($p->uwUserId)) {
          $log->addFailureId($p->uwUserId);
        }
        continue;
      }
    }

    // Log as completed and save.
    $log->set('completed', time());
    $log->save();
    return $log;
  }

  /**
   * Determine if we have an existing user specified, can match to a user in our system via email, or need to create
   * a new user.
   */
  public function determineUser() {
    // See if we can load an existing user
    if (!empty($this->uid)) {
      $u = new User($this->uid);
      if ($u->isLoaded()) {
        $this->existingUser = TRUE;
        $this->u = $u;
        return;
      }
    }

    // Next try to match a user by the UWorld user Id
    if(!empty($this->uwUserId)) {
      $u = User::FindUserByUworldUid($this->uwUserId);
      if($u && $u->isLoaded()) {
        $this->u = $u;
        $this->existingUser = TRUE;
        return;
      }
    }

    // Next, try to match with an existing email address just in case. This could happen especially if we are re-running an import.
    $uid = db_query("SELECT uid FROM users WHERE mail = :mail",[':mail' => $this->email])->fetchField();

    // If that didn't work, we may have a user whose username matches the email address. Since usernames are a unique DB
    // key, we want to load this user because creating a new user with that email as the username will cause a SQL error
    if(!$uid) {
      $uid = db_query("SELECT uid FROM users WHERE name = :mail", [':mail' => $this->email])->fetchField();
    }

    if($uid) {
      $this->u = new User($uid);
      if ($this->u->isLoaded()) {
        $this->existingUser = TRUE;
        return;
      }
    }

    // Lastly, if there is no matching user in the system, create a new, blank user to populate
    $u = User::create();
    $u->setName($this->email)
      ->setMail($this->email)
      ->setUwuid($this->uwUserId);

    $this->u = $u;
  }

  /**
   * Make a request to the UWorld API for order imports.
   *
   * @param int $from
   * - Beginning of the data range to request data for. UNIX timestamp.
   * @param int $to
   * - Beginning of the data range to request data for. UNIX timestamp.
   * @param int $userId
   * - A UWorld user ID
   * @return string
   * - If successful, returns an unencoded string of JSON data from the API. If not, returns the output from
   * curl_error().
   */
  public static function apiRequest($from = NULL, $to = NULL, $userId = NULL) {
    // The API endpoint URL
    $url = variable_get('uworld_api_url', 'https://api.uworld.com') . '/service/GetUsers';

    // Build the request parameters
    $data = json_encode(
      array_filter( // Remove any empty parameters from the data array
        array(
          'from' => $from,
          'to' => $to,
          "userId" => $userId,
        )
      )
    );

    // Set HTTP headers and method
    $method = 'POST';
    $headers = array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data),
      'x-api-key: ' . variable_get('uworld_api_key', '46f6a85d-80c4-4587-bf81-c48cf7a52a80'),
    );

    return UWorldImport::curlRequest($url, $headers, $method, $data);
  }

  /**
   * Take the JSON data from the import request and save it into a new or existing user.
   */
  public function updateUserData() {
    $this->u->setMail($this->email)
            ->setFirstName($this->firstName)
            ->setPhoneNumber($this->telephone)
            ->setLastName($this->lastName)
            ->setUwuid($this->uwUserId)
            ->setPassHash($this->passHash)
            ->setFirm($this->associatedFirmName)
            ->setStatus(1);

    // Try to set a university entity reference, if we can.
    // Commented out for now because the SF ids imported in UWorld don't match production.
    if(!empty($this->univSfId)) {
      $this->setUniversity($this->univSfId);
    }
    // If we don't have a university reference, just store the plain text name
    else {
      $this->u->setCollegeName($this->univName);
    }

    $this->u->save();
  }

  /**
   * Sets the 'did you graduate college', college sf_account reference, and college state list fields on the current
   * user account if a university matching the SalesForce ID was found.
   *
   * @param string $sfid
   * - A salesforce ID
   * @throws Exception
   */
  private function setUniversity($sfid) {
    // See if we can lookup a university based on the Salesforce ID
    if($entity = sf_account_load_by_sfid($sfid)) {
      // If the entity has an OPEID or FICE code, it's a university as opposed to a firm or some other thing.
      // In actuality, it should always have an OPEID but we check both just to be safe.
      if(!empty($entity->field_opeid[LANGUAGE_NONE][0]['value']) || !empty($entity->field_fice_code[LANGUAGE_NONE][0]['value'])) {
        $this->u->setDidYouGraduateCollege(1);
        $this->u->setCollegeSfAccount($entity->id);
        $this->u->setCollegeStateList($entity->field_college_state_list[LANGUAGE_NONE][0]['value']);
        $this->u->setCollegeName('');
      }
    }
    // If UW is specifying Salesforce IDs that we don't have, we consider that a problem.
    else {
      // Fallback on college name
      $this->u->setCollegeName($this->univName);
      watchdog('UWorld User Importer', "UWorld specified a SalesForce ID for a university that was not found. SF ID: $sfid",[], WATCHDOG_WARNING);
      //throw new Exception("UWorld specified a SalesForce ID for a university that was not found. SF ID: $sfid");
    }
  }

  /**
   * Create or update user entitlements based on subscriptions defined in the API data.
   */
  public function createUpdateEntitlements() {
    // Get all of the users existing entitlements
    $collection = $this->u->getAllEntitlements()->filterByUworld();

    foreach ((array)$this->subscriptions as $subData) {
      // Get the SKU that this data represents.
      $sku = self::$courseMap[$subData->courseId];

      // There may be non-CPA subscriptions here. If we don't have a sku to map, skip it.
      if (!$sku) {
        continue;
      }

      // See if we already have an entitlement for this subscription
      if($collection->filterBySkus([$sku])->isNotEmpty()) {
        $e = $collection->filterBySkus([$sku])->getFirst();
      }
      // This entitlement doesn't exist yet
      else {
        $e = UserEntitlement::create();
      }

      // @todo - Handle cram entitlements?
      if($subData->cram) {
        // $subData->cram is TRUE when the user has access to CRAM on the UWorld side. They don't actually get
        // an entitlement for CRAM there. We could do as they do and leave it off, or we could create a CRAM entitlements.
      }

      $this->createEntitlementFromSubData($e, $subData);

      $e->save();
    }
  }

  /**
   * @param object $subData
   * "subscriptions": [
   *  {
   *    "subscriptionId": 5067652,
   *    "courseId": 28000,
   *    "dateCreated": 1559834428,
   *    "dateUpdated": null,
   *    "dateExpire": 1607354428,
   *    "dateActivated": 1559834428,
   *    "isActive": false,
   *    "paymentId": 4180219,
   *    "packageId": 287,
   *    "durationInDays": 550,
   *    "cram": false,
   *    "rogerFlashcards": false
   *    "isTrialExtended": false
   *  }
   *]
   */
  public function createEntitlementFromSubData(UserEntitlement $e, $subData) {
    $bundled_product_sku = '_none';

    // See if we can find the related order
    $ow = CommerceOrderWrapper::FindOrderByUworldId($subData->paymentId);
    if($ow) {
      $order_id = $ow->getId();
      $o = new RCPARCommerceOrder($ow->getOrder());
      $bundled_product_sku = $o->getBundledProductSku();
    }
    else {
      // No matching order... @todo - that's a problem
      $order_id = '';
    }

    $sku = self::$courseMap[$subData->courseId];
    $course_section_tid = user_entitlements_get_course_section($sku);
    $product = commerce_product_load_by_sku($sku);
    $title = "UWorld - $sku";
    $exp_date = $subData->dateExpire;

    // If a free trial is extended update the expiration by the number of days
    if ($subData->isTrialExtended) {
      $exp_date = strtotime('+' . $subData->durationInDays . ' days', $subData->dateExpire);
    }

    // Set static properties
    $e->setProductId($product->product_id)
      ->setOrderId($order_id)
      ->setTitle($title)
      ->setProductTitle($title)
      ->setProductSku($sku)
      ->setBundledProduct($bundled_product_sku)
      ->setAuthorId($this->u->getUid())
      ->setCourseTypeRef(COURSE_TYPE_ONLINE_COURSE)
      ->setCourseSection($course_section_tid)
      ->setCreatedTime($subData->dateCreated)
      ->setExpirationDate($exp_date)
      ->setDurationExtended($subData->isTrialExtended)
      ->setActivationDelayed(!$subData->isActive)
      ->setActivationDate($subData->dateActivated)
      ->setCourseDuration(round($subData->durationInDays / 30))
      ->setUworldSubscriptionId($subData->subscriptionId)
      ->setIpqAccess(TRUE)// The RCPAR equivalent of all UW subscriptions would include IPQ/Qbank
      ->setExamVersionSingleVal(exam_version_get_default_version_tid())
      ->setPublished(FALSE); // Imported UWorld entitlements shouldn't be published.

    // The elite course should have unlimited access
    if($subData->packageId == 286) {
      $e->setUnlimitedAccess(TRUE);
    }
  }


}