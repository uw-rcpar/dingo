<?php

namespace RCPAR;

use EntityMetadataWrapperException;
use RCPAR\Entities\UserEntitlement;
use RCPAR\Entities\UserEntitlementCollection;
use RCPAR\Rest\V1\Token;
use RCPAR\WrappersDelight\UserWrapper;
use SalesFunnelWidgetCollege;

class User extends UserWrapper {
  protected $uid = 0;
  protected $user;
  protected $entityType = 'user';
  protected $isLoaded = FALSE;
  protected $wrapper;
  protected $entityObject;

  /**
   * @var UserEntitlementCollection $entitlements
   * @var UserEntitlementCollection $expiredEntitlements
   * @var UserEntitlementCollection $allEntitlements
   */
  protected $activeEntitlements;
  protected $expiredEntitlements;
  protected $allEntitlements;

  private $activeEntitlementsLoaded = FALSE;
  private $expiredEntitlementsLoaded = FALSE;
  private $allEntitlementsLoaded = FALSE;

  protected $drupalSessionId = '';

  /**
   * User constructor.
   * @param int|object|null $uid
   * - Can be a user ID, loaded Drupal user object, or NULL when working with a not yet existing user object.
   */
  public function __construct($uid = NULL) {
    // Initialize entitlement collections
    $this->activeEntitlements = new UserEntitlementCollection();
    $this->expiredEntitlements = new UserEntitlementCollection();
    $this->allEntitlements = new UserEntitlementCollection();

    // Load by user id if we have a numeric ID
    if ($uid && is_numeric($uid)) {
      $this->uid = $uid;
      $this->entityObject = user_load($uid);
      $this->setWrapper();
      if($this->getWrapper()) {
        $this->isLoaded = TRUE;
        $this->entity = $this->getWrapper(); // For WdUserWrapper
        return TRUE;
      }
    }

    // Construct from an already loaded user object. We assume the user object is valid and loaded if passed in
    // as an object.
    if(is_object($uid)) {
      parent::__construct($uid);
      $this->entityObject = $uid;
      $this->setWrapper();
      $this->isLoaded = TRUE;
      return TRUE;
    }

    $this->isLoaded = FALSE;
    return FALSE;
  }

  /**
   * Returns on instance of this class for the given uid.
   * (factory pattern in effect).
   * @param int $uid
   * @return User
   */
  public static function getInstance($uid) {
    return new self($uid);
  }

  /**
   * Any entitlement that the user has that they can use right now (not expired, doesn't need to be
   * activated, etc)
   * @return UserEntitlementCollection
   */
  function getActiveEntitlements() {
    // Get all entitlements that are valid and remove expired.
    return $this->getAllEntitlements()->filterByActive();
  }

  /**
   * Return the user's valid entitlements.
   * This is anything that they own right now. Includes CRAMs that are not yet activated.
   * @return UserEntitlementCollection
   */
  function getValidEntitlements() {
    return $this->getAllEntitlements()->filterByValid();
  }

  /**
   * Returns the entity ID of the sf_account that is stored as this user's college. If the user doesn't have
   * an associated entity (and instead manually entered a college name), a zero will be returned.
   * @return int
   */
  protected function getCollegeSfAccountId() {
    $sf_account = $this->getFieldValue('field_college_sf_account', '');
    if ($sf_account) {
      return $sf_account->id;
    }
    return 0;
  }

  /**
   * Get the value of field_college_state_list. Method is protected so that SalesFunnelWidgetCollege class can handle
   * the college logic.
   * @return string
   */
  protected function getCollegeState() {
    return $this->getFieldValue('field_college_state_list', '');
  }

  /**
   * Get the helper class that allows us properly identify college name, state, etc.
   * @return SalesFunnelWidgetCollege
   */
  public function getCollege() {
    return new SalesFunnelWidgetCollege($this->getCollegeSfAccountId(), $this->getCollegeState(), $this->getCollegeName());
  }

  /**
   * @return string
   * - Returns a concatenation of first and last name if available, otherwise defaults to username.
   */
  public function getDisplayName() {
    $first = trim($this->getFirstName());
    $last = trim($this->getLastName());

    // If there's no first name, return the username
    if(empty($first)) {
      return $this->getUser()->name;
    }

    // Otherwise, return first and last
    return "$first $last";
  }

  function getEmail() {
    return $this->getUser()->mail;
  }

  /**
   * Check if the user has an active entitlement for this product
   *
   * @param string $sku
   * - Sku of a product
   * @return bool
   */
  function hasActiveEntitlement($sku) {
    return $this->getActiveEntitlements()->getEntitlementBySku($sku);
  }

  /**
   * Check if the user has ever had an active entitlement for this product
   *
   * @param string $sku
   * - Sku of a product
   * @return bool
   */
  function hasExpiredEntitlement($sku) {
    return $this->getExpiredEntitlements()->getEntitlementBySku($sku);

    // Didn't find one
    return FALSE;
  }

  /**
   * @return UserEntitlementCollection
   */
  function getExpiredEntitlements() {
    // Get all entitlements that are valid and remove active.
    return $this->getAllEntitlements()->filterByExpired();
  }

  /**
   * Queries for all of the user's entitlement nodes and adds them to the collection
   * @param UserEntitlementCollection $val
   * @return UserEntitlementCollection
   */
  function setAllEntitlements($val = NULL) {
    // Flag as loaded
    $this->allEntitlementsLoaded = TRUE;

    if (!empty($val)) {
      $this->allEntitlements = $val;
    }
    else {
      $result = db_query("SELECT nid FROM node WHERE type = :t AND uid = :uid", array(
        ':t' => UserEntitlement::$bundle,
        ':uid' => $this->getUid()
      ));
      foreach ($result as $row) {
        $e = new UserEntitlement($row->nid);
        $this->allEntitlements->addEntitlement($e);
      }
    }

    return $this->allEntitlements;
  }

  /**
   * @return UserEntitlementCollection
   */
  function getAllEntitlements() {
    // Load on demand
    if(!$this->allEntitlementsLoaded) {
      $this->setAllEntitlements();
    }

    return $this->allEntitlements;
  }

  /**
   * Set the user's activated entitlements. Note that this includes CRAM entitlements that are valid
   * but not yet active
   * @param UserEntitlementCollection $val
   * @return UserEntitlementCollection
   */
  function setActiveEntitlements($val = NULL) {
    // Flag as loaded
    $this->activeEntitlementsLoaded = TRUE;

    if (!empty($val)) {
      $this->activeEntitlements = $val;
    }
    else {
      // Query for active entitlements and add them to the collection
      $entitlements = user_entitlements_get_users_products($this->getUser(), TRUE); // @todo - use entitlement cache?
      if (!empty($entitlements['products'])) {
        foreach ($entitlements['products'] as $sku => $ent) {
          $e = new UserEntitlement($ent['#node']);
          $this->activeEntitlements->addEntitlement($e);
        }
      }

      // Query for "delayed products" - entitlements that the user owns but have not been activated yet
      // (only applies to CRAM courses at the moment) and add them to the collection
      $unactivated_ents = user_entitlements_get_products_delayed($this->getUser());
      if (!empty($unactivated_ents)) {
        foreach ($unactivated_ents as $nid) {
          $node = node_load($nid['nid']);
          $e = new UserEntitlement($node);
          $this->activeEntitlements->addEntitlement($e);
        }
      }
    }

    return $this->activeEntitlements;
  }


  /**
   * @param UserEntitlementCollection|NULL $val
   * @return array|UserEntitlementCollection
   */
  function setExpiredEntitlements(UserEntitlementCollection $val = NULL) {
    // Flag as loaded
    $this->expiredEntitlementsLoaded= TRUE;

    // Set expired entitlements from provided value, if present
    if (!empty($val)) {
      $this->expiredEntitlements = $val;
    }
    // Query for this user's expired entitlements dynamically
    else {
      $entitlements_data = user_entitlements_get_expired_entitlements($this->getUser());

      if (!empty($entitlements_data)) {
        // Build a new entitlement collection
        $this->expiredEntitlements = new UserEntitlementCollection();
        foreach ($entitlements_data as $entitlement_nid => $ent) {
          if (is_numeric($entitlement_nid)) {
            // Add each entitlement to the collection
            $e = new UserEntitlement($ent['#node']);
            $this->expiredEntitlements->addEntitlement($e);
          }
        }
      }
    }

    return $this->expiredEntitlements;
  }

  /**
   * Initialize the user object from values in the token
   * @param Token $t
   */
  public function authenticateFromToken(Token $t) {
    $this->__construct($t->getUid());
  }

  /**
   * Validate a set of login and password credentials.
   * Currently we only allow stand end users to login using their email address, rather than their actual
   * Drupal username. This is to support the transition to UWorld, which will only support email.
   * There is an exception to this policy, which is that we will support logins with a username if their account
   * has an @rcparlabs.com email address. Doing this allows our QA test accounts to continue using their same
   * credentials.
   *
   * @param string $username
   * @param string $pass
   * @return bool|int
   * - Returns 0 or false on failure to validate, or an integer user ID (greater than 0) on success.
   */
  public function validateCredentials($username, $pass) {
    $uid = 0;

    // See if we the provided username is actually an email address
    if ($name = db_query("SELECT name FROM users WHERE LOWER(mail) = LOWER(:name)",
      array(':name' => $username))->fetchField()) {
      // Found a matching email - try to authenticate with it
      $uid = user_authenticate($name, $pass);
    }
    // Not an email address in the system, authenticate as a username
    else {
      $result = db_query('select mail from users where name = :name and mail like :mail', array(':name' => $username, ':mail'=> "%@rcparlabs.com%"));
      if($result->rowCount()){
        $uid = user_authenticate($username, $pass);
      }
    }

    return $uid;
  }

  /**
   * Check for a matching email/password hash combination in the user table.
   *
   * @param $mail
   * - A user email address
   * @param $hash
   * - A hashed password
   * @return int
   * - A user id, or FALSE if not found.
   */
  public function validateHash($mail, $hash) {
    // Try to match a password hash from the database
    return db_query("SELECT uid FROM users WHERE mail = :mail AND pass = :hash", [
      ':mail' => $mail,
      ':hash' => $hash,
    ])->fetchField();
  }

  /**
   * Log in to Drupal if the supplied credentials are valid. Sets a new Drupal
   * session ID and invokes Drupal's login hooks.
   *
   * @param string $username
   * @param string $pass
   * @return int|bool
   * - A user id or FALSE if failed
   */
  public function authenticateCredentials($username = NULL, $pass = NULL) {
    // Authenticate against a username and password, if provided
    if (!empty($username) && !empty($pass)) {
      $uid = $this->validateCredentials($username, $pass);

      // We authenticated the user, load them up
      if ($uid) {
        $this->login($uid);
      }

      // Return the result of authentication regardless of success
      return $uid;
    }

    return FALSE;
  }

  /**
   * Populate our class instance with user details and log the user into drupal
   * @param int $uid
   */
  public function login($uid) {
    $this->__construct($uid);

    // Log the user into Drupal
    self::drupalLogin($uid);

    // Store the resulting session ID
    $this->setDrupalSessionId(
      $this->getLatestSessionId()
    );
  }

  /**
   * Log in the user via Drupal core's user module. This will update the user's last access
   * in the DB and trigger session limit hooks that maintain the max number of sessions.
   * It will also set the global $user variable
   * @param $uid
   */
  public static function drupalLogin($uid) {
    $form_data = array('uid' => $uid);
    user_login_submit(array(), $form_data);
  }

  /**
   * @return int|bool
   */
  public function getUid() {
    // Bail if we don't have a user
    if(!is_object($this->getUser())) {
      return FALSE;
    }

    return $this->getUser()->uid;
  }

  /**
   * @return object
   * - Drupal user object
   */
  public function getUser() {
    return $this->entityObject;
  }

  /**
   * @return string
   */
  public function getDrupalSessionId() {
    return $this->drupalSessionId;
  }

  /**
   * @param string $drupalSessionId
   */
  public function setDrupalSessionId($drupalSessionId) {
    $this->drupalSessionId = $drupalSessionId;
  }

  /**
   * Get the most recently created session ID for this user
   * @return string
   */
  public function getLatestSessionId() {
    return db_query("SELECT sid FROM sessions WHERE uid = :uid ORDER BY timestamp DESC LIMIT 1",
      array(':uid' => $this->getUid())
    )->fetchField();
  }

  /**
   * Determine if the given session ID exists in the sessions table
   *
   * @param $sessionId
   * - Optionally match against the given session id, otherwise the currently set session ID will be used
   * @return string|bool
   */
  public function sessionIdExists($sessionId = NULL) {
    if(is_null(($sessionId))) {
      $sessionId = $this->getDrupalSessionId();
    }
    $result = db_query("SELECT sid FROM sessions WHERE uid = :uid AND sid = :sid LIMIT 1",
      array(':uid' => $this->getUid(), ':sid' => $sessionId)
    )->fetchField();

    return $result;
  }

  public function isLoaded() {
    return $this->isLoaded;
  }

  /**
   * Returns the stdclass user object (e.g. the value of user_load()).
   * @return object
   */
  function getEntity() {
    return $this->getWrapper()->value();
  }

  /**
   * Sets an entity metadata wrapper for the user
   * @return bool
   * - True if set, false if there was a problem
   */
  function setWrapper() {
    try {
      $this->wrapper = entity_metadata_wrapper($this->entityType, $this->entityObject);
      return is_object($this->wrapper);
    }
    catch (EntityMetadataWrapperException $exc) {
      // Swallow the error
    }
    return FALSE;
  }

  /**
   * Get the entity metadata wrapper for this user.
   * If the wrapper has not been set yet, this function will set it first
   * @return \EntityDrupalWrapper
   */
  function getWrapper() {
    if(empty($this->wrapper)) {
      $this->setWrapper();
    }

    return $this->wrapper;
  }

  /**
   * Helper function to get the value of a field the entity wrapper.
   * This function allows us to avoid the boilerplate of checking for the wrapper, supplying a fallback default,
   * and handling EntityMetadataWrapperException exceptions.
   *
   * @param string $fieldname
   * - A field name on the node
   * @param bool $default
   * - Default value to return if there is any problem getting the value (an EntityMetadataWrapperException for example)
   * @return mixed
   * - Will return whatever value the field stores, which could be just about anything.
   */
  protected function getFieldValue($fieldname, $default = FALSE) {
    if ($this->getWrapper()) {
      try {
        return $this->getWrapper()->{$fieldname}->value();
      }
      catch (EntityMetadataWrapperException $e) {
        // Swallow error - we'll return the default
      }
    }
    return $default;
  }

}
