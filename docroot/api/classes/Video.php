<?php

namespace RCPAR;

use Exception;

class Video {
  /** @var User $u */
  protected $u;
  protected $videoId;
  protected $entitlementId;

  public function __construct(User $u, $videoID, $entitlementId) {
    $this->u = $u;
    $this->videoId = $videoID;
    $this->entitlementId = $entitlementId;
  }

  /**
   * Get the id (primary key) for this history record
   * @return int
   */
  public function getHistoryId() {
    static $id = NULL;

    // Return static value if we have it
    if($id) {
      return $id;
    }

    // Get the eck_video_history ID of this history record
    $id = db_query("SELECT id FROM {eck_video_history} WHERE uid = :uid AND rcpa_video = :vidid AND entitlement_product = :eid", array(
      ':uid' => $this->u->getUid(), ':vidid' => $this->videoId , ':eid' => $this->entitlementId,
    ))->fetchField();

    return $id;
  }

  /**
   * Returns the timestamp for when this video was last watched.
   * @return int|FALSE
   */
  public function getTimeLastWatched() {
    return db_query("SELECT changed FROM {eck_video_history} WHERE id = :id", array(':id' => $this->getHistoryId()))->fetchField();
  }

  public function getVideoProgress() {
    $progress = db_query("SELECT last_position FROM {eck_video_history} WHERE id = :id", array(':id' => $this->getHistoryId()))->fetchField();
    if($progress) {
      return $progress;
    }
    return 0;
  }

  /**
   * Set and save the progress for a video history record.
   *
   * @param int $videoId
   * @param int $topicId
   * @param int $progressTime
   * - Video timestamp (in seconds)
   * @param $entitlementSku
   * - SKU for an entitlement (AUD, BEC-CRAM, etc)
   * @throws Exception
   */
  function setVideoProgress($videoId, $topicId, $progressTime, $entitlementSku) {
    $video = node_load($videoId);
    $video_wrapper = entity_metadata_wrapper('node', $video);

    // Calculate the percentage watched
    list($vidMin, $vidSec) = explode(':', $video_wrapper->field_video_duration->raw());
    // Converts the minutes into seconds and adds the remaining seconds to it.
    $durationSeconds = ($vidMin * 60) + $vidSec;
    // Converts video duration seconds into milliseconds and divides by current usage milliseconds and round to percentage
    $percent = round($progressTime / ($durationSeconds * 1000) * 100) ;

    module_load_include('inc', 'courseware', 'services/video_history');
    $response = _courseware_service_vh_save(array(
      'userid' => $this->u->getUid(),
      'topic' => $topicId,
      'video' => $videoId,
      'time' => $progressTime,
      'duration' => 'Populated from node',
      'percentviewed' => $percent,
      'entitlement_sku' => $entitlementSku,
      'entitlement_id' => $this->entitlementId,
    ));

    // Success
    if(isset($response['status'])) {
      return $response['status']['video_history_nid'];
    }
    // Error
    else {
      throw new \Exception($response['message']);
    }
  }
}
