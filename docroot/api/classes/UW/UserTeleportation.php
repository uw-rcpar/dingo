<?php

namespace RCPAR\UW;

use Exception;
use RCPAR\Course;
use RCPAR\Entities\UserEntitlementCollection;
use RCPAR\Import\UWorldImport;
use RCPAR\Import\UWorldOrderProcessor;
use RCPAR\Rest\V1\Token;
use RCPAR\User;

class UserTeleportation {

  /** @var User $u */
  public $u;

  protected $teleportData;
  protected $errorMsg;

  /**
   * UserTeleportation constructor.
   *
   * @param int|User $user_or_uid
   * - Drupal user ID or loaded user class
   */
  public function __construct($user_or_uid) {
    if (is_numeric($user_or_uid)) {
      $this->u = new User($user_or_uid);
    }
    else {
      $this->u = $user_or_uid;
    }

    // If we don't have a user, all bets are off
    if (!$this->u->isLoaded()) {
      throw new Exception("Invalid user.");
    }

    // Get our teleport user record, or create it if we don't have one yet
    $this->teleportData = $this->getTeleportationRecord();
    if(empty($this->teleportData)) {
      $this->createNewUserRecordIfNeeded();
    }
  }

  /**
   * Return a newly instantiated instance of this class.
   *
   * @param int|User $user_or_uid
   * - User ID or loaded user class
   * @return UserTeleportation
   */
  public static function instance($user_or_uid) {
    return new self($user_or_uid);
  }

  /**
   * Returns true if a lock is acquired and there is not a migration already in progress for this user.
   * @return bool
   */
  protected function lockAcquire() {
    if (lock_acquire('migrate-' . $this->u->getUid(), 120) && !$this->isMigrationInProgress()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Release our concurrency lock
   * return NULL
   */
  protected function lockRelease() {
    lock_release('migrate-' . $this->u->getUid());
  }

  /**
   * Set API errors encountered in the last request
   * @param string $msg
   */
  protected function setErrorMessage($msg) {
    $this->errorMsg = $msg;
  }

  /**
   * Called when the teleport is successful. This will mark all of the user's valid entitlements as unpublished.
   */
  public function disableEntitlements() {
    $ents = $this->u->getValidEntitlements();
    foreach ($ents as $e) {
      // Don't disable ACT entitlements
      if($e->isTypeACT() || $e->isOnlineCourseACT()) {
        continue;
      }

      $e->setPublished(FALSE);
    }
    $ents->saveAll();
  }

  /**
   * Get the user's entitlements that are eligible for teleportation to UWorld.
   * These are valid, B2C, standard course entitlements - with the exception of elite entitlements that do not
   * have unlimited access.
   * @return UserEntitlementCollection
   */
  public function getEntitlementsEligibleForTeleport() {
    if (variable_get('uw_phase3_teleport', FALSE)) {
      $ents = $this->u->getValidEntitlements()->filterByOnlineCourse()->removeACTFromCollection()->filterByPaid();
    }
    else {
      $ents = $this->u->getValidEntitlements()->filterByOnlineCourse()->filterByPartnerless()->removeACTFromCollection();
    }

    // Remove elite courses that don't have unlimited access. These are not eligible
    $elUnlFiltered = new UserEntitlementCollection();
    foreach ($ents as $e) {
      if($e->getBundledProduct() == 'FULL-ELITE-DIS' && !$e->getUnlimitedAccess()) {
        continue;
      }
      $elUnlFiltered->addEntitlement($e);
    }

    return $elUnlFiltered;
  }

  /**
   * The presence of certain B2B entitlements will disqualify the user from being migrated. FOR PHASE 1 and 2: Basically, having ACT,
   * PAL, or CRAM entitlements from a partner is allowed, but having a standard course entitlement from a partner
   * is not allowed, even if their other B2C entitlements meet the eligibility requirements. FOR PHASE 3: partners are allowed,
   * except for Ohara (partner id = 5968341).
   *
   * @return UserEntitlementCollection
   */
  protected function getDisqualifyingEntitlementsPhase1() {
    return $this->u->getValidEntitlements()->filterByOnlineCourse()->removeACTFromCollection()->filterByPartnered();
  }

  /**
   * In phase 2, partnered entitlements are allowed (except for OHara)
   * PAL, or CRAM entitlements from a partner is allowed, but having a standard course entitlement from a partner
   * is not allowed, even if their other B2C entitlements meet the eligibility requirements. FOR PHASE 3: partners are allowed,
   * except for Ohara (partner id = 5968341).
   *
   * @return UserEntitlementCollection
   */
  protected function getDisqualifyingEntitlementsPhase2() {

    // In phase 2, any partner entitlement is a disqualification.
    $ents = $this->u->getValidEntitlements()->filterByOnlineCourse()->removeACTFromCollection()->filterByPartnered();
    
    // If we have any FSL entitlements, the user is disqualified
    $fsl_ents = $this->u->getValidEntitlements()->filterByPartnerType(firm_partner)->filterByBillingType('freetrial');
    $ents = $ents->merge($fsl_ents);

    // Regardless of phase, if a user has any entitlement (expired or not) from this disqualifying partner list,
    // they cannot be migrated. The primary use case for this is ACT professors.
    $pids = explode("\r\n", variable_get('teleport_disq_partner', rcpar_mods_get_teleport_disq_partner_defaults()));
    return $this->u->getAllEntitlements()->filterByPartnerId($pids)->merge($ents);
  }

  /**
   * In Phase 3 partner entitlements are allowed except for Ohara. FSL and other specific partners disqualify migration
   * @return UserEntitlementCollection
   */
  protected function getDisqualifyingEntitlementsPhase3() {

    // If we have any FSL entitlements, the user is disqualified
    $ents = $this->u->getValidEntitlements()->filterByPartnerType(firm_partner)->filterByBillingType('freetrial');

    // Regardless of phase, if a user has any entitlement (expired or not) from this disqualifying partner list,
    // they cannot be migrated. The primary use case for this is ACT professors.
    $pids = explode("\r\n", variable_get('teleport_disq_partner', rcpar_mods_get_teleport_disq_partner_defaults()));
    return $this->u->getAllEntitlements()->filterByPartnerId($pids)->merge($ents);
  }

  /**
   * Gets an array of data that can be used to pass to UWorld's migration endpoints (phase 1 and 2).
   * @return array
   * @throws Exception
   */
  protected function getTeleportPayload() {
    // Get the UWorld user ID
    $userId = $this->u->getUwuid();
    if (empty($userId)) {
      throw new Exception("Missing UWorld user ID.");
    }

    // Get valid B2C online course entitlements.
    $ents = $this->getEntitlementsEligibleForTeleport();

    // If the user doesn't have any valid course entitlements, we can't teleport them. Other logic should prevent the
    // user from being directed here if that were the case, but the URLs are not access protected and can also be used
    // directly by customer care people, etc. So we still test for this.
    if ($ents->isEmpty()) {
      throw new Exception("You don't have any valid courses that can be transferred to the new platform.");
    }

    // User gets access to canned flashcards if they have an elite course.
    $hasElite = $ents->filterByBundle('FULL-ELITE-DIS')->isNotEmpty();

    // There is a list of partners who should be given elite subscriptions for phase3.
    if (!$hasElite && variable_get('uw_phase3_teleport', FALSE)) {
      if ($ents->isContainingElitePartnerPackage()) {
        $hasElite = TRUE;
      }
    }

    // Build subscription list
    $subscriptionList = array();
    foreach ($ents as $e) {
      // Determine what the single-part bundle would be for this part
      $bundle = $e->getCourseSectionName() . '-SEL';
      $packageId = array_search($bundle, UWorldOrderProcessor::$productMap);

      if ($packageId === FALSE) {
        continue;
      }

      // Per PROD-739 all Elite entitlements get active UWorld crams regardless of RCPAR cram status
      if ($hasElite) {
        $cram = TRUE;
      }
      else {
        // Determine CRAM. We'll look for individual CRAM parts
        $cram = $this->u->getValidEntitlements()
          ->filterByPartnerless()
          ->filterByCramCourse()
          ->filterBySectionName($e->getCourseSectionName())
          ->isNotEmpty();
      }

      // Add data to our subscription list, which will be converted to JSON payload for the API
      $subscriptionList[$packageId] = array(
        'packageId' => $packageId,
        'expirationDate' => date('m-d-Y H:i:s', $e->getExpirationDate()) . '.000', // We have to manually add the microseconds
        'courseFeatures' => [
          'cannedFlashCards' => $hasElite, // Canned flashcards for elite only
          'cramCourse' => $cram,
        ]
      );
    }

    $payload = [
      'userId' => $userId,
      'isEliteUser' => $hasElite,
      'subscriptionList' => array_values($subscriptionList),
    ];

    return $payload;
  }

  /**
   * Handle errors returned from the API.
   * @param array $return
   * - The return from the curl request.
   * @return bool
   * - Returns TRUE if there was an error.
   * @throws Exception
   */
  private function teleportHandleError($return) {
    // Could not decode response - server down or returning garbage, etc.
    if ($return === FALSE) {
      throw new Exception("Sorry, we couldn't connect to the migration server at this time. Please try again later.");
    }

    // An error
    if (!empty($return['error'])) {
      // Rajesh says error looks like this:
      // {
      //   "error": "Custom Exception",
      //   "error_description": "Invalid package."
      // }
      throw new Exception($return['error_description']); // Determine API error
    }
    
    // Any non-success return is probably a Cloudflare error, or some other unknown issue.
    if($return != 200) {
      throw new Exception("Sorry, we couldn't connect to the migration server at this time. Please try again later.");
    }
  }

  /**
   * Invokes the API. If successful, calls disableEntitlements() - if not, call setErrorMessage() with a message about
   * the error.
   * @return bool
   * - True if no errors were encountered.
   */
  public function teleportPhase1() {
    try {
      // If we can't acquire a lock or the migration is in progress, let the user know
      if (!$this->lockAcquire()) {
        throw new Exception("A migration request has already been initiated, please wait for it to complete.");
      }

      $payload = $this->getTeleportPayload();

      // Get the API response and decode
      $return = json_decode(self::apiRequest($payload), TRUE);

      // Check for errors.
      $this->teleportHandleError($return);
      
      // Success. Mark the user as migrated.
      $this->setUserMigrated();
      $this->disableEntitlements();
      return TRUE;
    }
    catch (Exception $e) {
      $this->setErrorMessage($e->getMessage());
      return FALSE;
    }
    finally {
      $this->lockRelease();
    }
  }

  /**
   * Invokes the API on the UWorld side to indicate an asynchronous migration is ready to begin. If errors occur
   * in the initial request, they are stored via setErrorMessage()
   * @return bool
   * - True if no errors were encountered.
   */
  public function teleportPhase2() {
    try {
      // If we can't acquire a lock or the migration is in progress, let the user know
      if (!$this->lockAcquire()) {
        throw new Exception("A migration request has already been initiated, please wait for it to complete.");
      }
      
      $payload = $this->getTeleportPayload();

      // Provide a JWT token to UWorld with a 30-day expiration so there is an excessive amount of time to finish.
      $t = new Token();
      $t->setUid($this->u->getUid());
      $t->setExpiration(time() + 86400 * 30); // 30 days
      $t->setIssued(time());
      $payload['jwtToken'] = $t->getEncodedToken();

      // Get the API response and decode.
      $return = json_decode(self::apiRequest($payload, '/service/InitiateMigration'), TRUE);

      // Check for errors.
      $this->teleportHandleError($return);

      // Success
      $this->resetAsyncMigration();
      $this->setAsyncStart();
      return TRUE;
    }
    catch (Exception $e) {
      $this->setErrorMessage($e->getMessage());
      return FALSE;
    }
    finally {
      $this->lockRelease();
    }
  }

  /**
   * Make a request to the UWorld InsertSubscription API.
   * @param array $payload
   * - The request payload. Should be an unencoded associative array.
   * Example payload:
   * {
   *   "userId":991804,
   *   "subscriptionList": [
   *     {
   *       "packageId":288,
   *       "expirationDate":"08-30-2020 23:59:59.000",
   *       "courseFeatures": {
   *         "cannedFlashCards": true,
   *         "cramCourse": false
   *       }
   *     },
   *     {
   *       "packageId":289,
   *       "expirationDate":"09-30-2020 23:59:59.000",
   *       "courseFeatures": {
   *         "cannedFlashCards": true,
   *         "cramCourse": false
   *       }
   *     }
   *   ]
   * }
   * int userId
   * - UWorld user ID
   * int packageId
   * - UWorld package ID
   * string expirationDate
   * - Expiration date for the subscription. Format: mm-dd-yyyy
   * bool cannedFlashCards
   * - True if the user should have access to UWorld's canned flashcards
   * bool cramCourse
   * - True if the user should have cram access.
   * @param string $url
   * - The phase 1 and phase 2 migrations have virtually identical payloads but use different URLs.
   * By default, the phase 1 URL is specified.
   *
   * @return string
   * - The returned output from the request, or the contents of curl_error if there is an error.
   */
  public static function apiRequest($payload, $url = '/service/InsertSubscription') {
    // The API endpoint URL
    $url = variable_get('uworld_api_url', 'https://api.uworld.com') . $url;

    // Build the request payload
    $data = json_encode($payload);

    if(!empty($_GET['debug'])) {
      drupal_set_message($data);
    }

    // Set HTTP headers and method
    $method = 'POST';
    $headers = array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data),
      'x-api-key: ' . variable_get('uworld_api_key', '46f6a85d-80c4-4587-bf81-c48cf7a52a80'),
    );

    $return = UWorldImport::curlRequest($url, $headers, $method, $data);

    if (!empty($_GET['debug'])) {
      drupal_set_message($return);
    }
    watchdog('teleport_api_response', $return);

    return $return;
  }

  /**
   * Get API errors encountered in the last request
   * @return string
   */
  public function getErrorMessage() {
    return $this->errorMsg;
  }

  /**
   * Reads the current value for the user's phase 1 or phase 2 eligibility and returns it. This function will not
   * recalculate whether the user is eligible if their status has changed since last checked.
   * Being "eligible" just means that we will actively display UI to that user prompting them
   * to perform the migration - the process is not otherwise access controlled.
   *
   * @param string|phase number
   * - Possible values: 'phase1', 'phase2'
   * @return bool|NULL
   * - True if the user is eligible, FALSE if they're not, or NULL if it has not yet been determined.
   */
  public function isUserEligible($phase = 'phase1') {

    // No log data has been created yet, we don't know if they're eligibile or not, but we return false by default.
    if (!$this->teleportData) {
      return NULL;
    }

    // If the user is already teleported, eligibility is redundant
    if ($this->teleportData['teleported']) {
      return FALSE;
    }

    // NULL value indicates we have never calculated
    if(is_null($this->teleportData[$phase.'_eligible'])) {
      return NULL;
    }

    // Check if the user has been blacklisted from migrating.
    if ($this->isUserBlacklisted()) {
      return FALSE;
    }

    // If this filter is set to true students who have exam simulator history will not be prompted to migrate
    if (variable_get('teleport_enable_examsim_filter', FALSE)) {
      if (db_query("SELECT count(*) FROM ipq_saved_sessions WHERE session_type = 'exam' and uid = :uid LIMIT 1", [':uid' => $this->u->getUid()])->fetchField() > 0) {
        return FALSE;
      }
    }

    // Return the eligibility value that we've determined. Note that the literal value here is
    // $this->teleportData['phase1_eligible'] === "0", so we do a boolean conversion for more precision.
    return $this->teleportData[$phase.'_eligible'] ? TRUE : FALSE;
  }

  /**
   * Determine if user is blacklisted from migrating. If migration is marked complete but user is not
   * marked as teleported, then an error occurred and they should be temporarily ineligible to migrate.
   * @return bool
   */
  public function isUserBlacklisted() {
    if ($this->getAsyncComplete() && !$this->isUserMigrated()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check if the user is zero-progress eligible. This only looks at the value stored, it does not do a calculation
   * if it has not already been determined.
   * @return bool|null
   * - True if the user is eligible, FALSE if they're not, or NULL if it has not yet been determined.
   */
  public function isUserZeroProgressEligible() {
    // No log data has been created yet, we don't know if they're eligibile or not, but we return false by default.
    if (!$this->teleportData) {
      return NULL;
    }

    // If the user is already teleported, eligibility is redundant
    if ($this->teleportData['teleported']) {
      return FALSE;
    }

    // NULL value indicates we have never calculated
    if(is_null($this->teleportData['zero_progress_eligible'])) {
      return NULL;
    }

    // Return the eligibility value that we've determined. Note that the literal value here is
    // $this->teleportData['zero_progress_eligible'] === "0", so we do a boolean conversion for more precision.
    return $this->teleportData['zero_progress_eligible'] ? TRUE : FALSE;
  }

  /**
   * Determine if the user is eligible for auto-migration by calculating their course and IPQ progress - if all is
   * zero, they're eligible.
   *
   * @return bool
   * - TRUE if the user is eligible, otherwise FALSE.
   */
  public function calculateZeroProgressEligibility() {
    $ents = $this->getEntitlementsEligibleForTeleport();

    // If the user has no valid, B2C (meaning not from a partner) course entitlements, we won't consider them
    // eligible for the phase 1 teleport.
    if ($ents->isEmpty()) {
      $this->teleportData['zero_progress_eligible'] = FALSE;
      $this->updateUserRecord();
      return $this->teleportData['zero_progress_eligible'];
    }

    // The presence of certain B2B entitlements will disqualify the user from being migrated. Basically, having ACT,
    // PAL, or CRAM entitlements from a partner is allowed, but having a standard course entitlement from a partner
    // is not allowed, even if their other B2C entitlements meet the eligibility requirements.
    if(variable_get('uw_phase2_teleport', FALSE)) {
      $disqEnts = $this->getDisqualifyingEntitlementsPhase2();
    }
    else {
      $disqEnts = $this->getDisqualifyingEntitlementsPhase1();
    }
    if ($disqEnts->isNotEmpty()) {
      $this->teleportData['zero_progress_eligible'] = FALSE;
      $this->updateUserRecord();
      return $this->teleportData['zero_progress_eligible'];
    }

    // Get the user's video progress
    $progress = Course::getProgressPercentageForUser($this->u, $ents);

    // If the user has more than 0% progress for any course entitlement, they are not eligible.
    $eligible = TRUE;
    foreach ((array)$progress as $item) {
      if ($item['percentage'] > 0) {
        $eligible = FALSE;
      }
    }

    // If the user has answered any questions in IPQ, they are not eligible. Only do this query if we haven't already
    // determined they're not eligible.
    if($eligible) {
      $ipq_count = db_query(
        "SELECT count(*) FROM ipq_saved_session_data WHERE uid = :uid AND status <> 2 LIMIT 1",
        [':uid' => $this->u->getUid()]
      )->fetchField();

      if($ipq_count > 0) {
        $eligible = FALSE;
      }
    }

    $this->teleportData['zero_progress_eligible'] = $eligible;
    $this->updateUserRecord();
    return $eligible;
  }

  /**
   * Determine whether the user is phase 1 or 2 eligible and update their user record.
   *
   * @param string|phase number
   * - Possible values: 'phase1', 'phase2'
   * @return bool
   */
  public function calculateEligibility($phase = 'phase1') {
    // Do a new calculation and update the user record
    if($phase == 'phase1') {
      $this->teleportData[$phase.'_eligible'] = $this->determineEligibilityPhase1($phase);
    }
    if($phase == 'phase2') {
      $this->teleportData[$phase.'_eligible'] = $this->determineEligibilityPhase2($phase);
    }
    if ($phase == 'phase3') {
      $this->teleportData['phase3_eligible'] = $this->determineEligibilityPhase3($phase);
    }

    $this->updateUserRecord();
    return $this->teleportData[$phase.'_eligible'] ? TRUE : FALSE;
  }

  /**
   * Create a new record for this user in enroll_flow_uw_teleport with default values.
   */
  protected function createNewUserRecordIfNeeded() {
    if (!$this->teleportData) {
      $record = array(
        'uid' => $this->u->getUid(),
        'phase1_eligible' => NULL,
        'phase2_eligible' => NULL,
        'teleported' => FALSE,
        'updated' => time(),
        'teleported_time' => NULL,
      );
      drupal_write_record('enroll_flow_uw_teleport', $record);
      $this->teleportData = $record;
    }
  }

  /**
   * Update the record for this user in enroll_flow_uw_teleport with the current local values.
   */
  protected function updateUserRecord() {
    $this->teleportData['updated'] = time();
    drupal_write_record('enroll_flow_uw_teleport', $this->teleportData, 'id');
  }

  /**
   * If the user was part of a batch migration process, update the batch queue record.
   */
  public function setUserMigrateBatchRecordComplete() {

    // If there is no batch queue record we can move along
    if (db_query("SELECT count(*) FROM enroll_flow_uw_migrate_batch_queue WHERE uid = :uid", [':uid' => $this->u->getUid()])->fetchField() == 0) {
      return;
    }
    $fields = array(
      'uid' => $this->u->getUid(),
      'status' => 'completed',
      'updated' => time(),
    );
    db_update('enroll_flow_uw_migrate_batch_queue')
      ->fields($fields)
      ->condition('uid', $this->u->getUid())
      ->execute();
  }

  /**
   * Determines whether the user is phase 1 or 2 eligible based on their user entitlements and course progress.
   * This is a moderately resource heavy process since it queries entitlements and video history.
   *
   * @param string|phase number
   *  - Possible values: 'phase1', 'phase2'
   * @return bool
   * - Will be TRUE if the user is eligible, FALSE otherwise.
   */
  protected function determineEligibilityPhase1() {
    $ents = $this->getEntitlementsEligibleForTeleport();

    // If the user has no valid, B2C (meaning not from a partner) course entitlements, we won't consider them
    // eligible for the phase 1 teleport.
    if ($ents->isEmpty()) {
      return FALSE;
    }

    // The presence of certain B2B entitlements will disqualify the user from being migrated. Basically, having ACT,
    // PAL, or CRAM entitlements from a partner is allowed, but having a standard course entitlement from a partner
    // is not allowed, even if their other B2C entitlements meet the eligibility requirements.
    if ($this->getDisqualifyingEntitlementsPhase1()->isNotEmpty()) {
      return FALSE;
    }

    // Get the user's video progress
    $progress = Course::getProgressPercentageForUser($this->u, $ents);

    // New method: if the user is eligible if all of their course progress is either below 5% (barely started) or above
    // 95% (basically done).
    foreach ((array)$progress as $item) {
      if ($item['percentage'] > 5 && $item['percentage'] < 95) {
        // This user is in the middle of one of their courses. They're not eligible.
        return FALSE;
      }
    }

    // If we're here, the user is either at the beginning or the end of all of their courses, making them a good candidate
    // to transition, since they're not midway through study on any given part.
    return TRUE;
  }

  /**
   * In phase 2 (migration WITH data), any non-partnered user can migrate.
   * In an update to phase 2 (called confusingly, "phase 3"), anybody can migrate except for O-hara.
   * @return bool
   */
  protected function determineEligibilityPhase2() {
    $ents = $this->getEntitlementsEligibleForTeleport();
    if ($ents->isEmpty()) {
      return FALSE;
    }

    if ($this->getDisqualifyingEntitlementsPhase2()->isNotEmpty()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * In phase 3 (migrate partners), any partnered user can migrate.
   * @return bool
   */
  protected function determineEligibilityPhase3() {
    $ents = $this->getEntitlementsEligibleForTeleport();
    if ($ents->isEmpty()) {
      return FALSE;
    }

    if ($this->getDisqualifyingEntitlementsPhase3()->isNotEmpty()) {
      return FALSE;
    }

    // Looking for partnered entitlements
    if ($ents->filterByPartnered()->isNotEmpty()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Marks the user as having been teleported to UWorld.
   */
  public function setUserMigrated() {
    $this->teleportData['teleported'] = TRUE;
    $this->teleportData['teleported_time'] = time();
    $this->updateUserRecord();
  }

  /**
   * Get the user's data from the enroll_flow_uw_teleport table
   * @return array|bool
   * - An array if data exists, FALSE if not. Returned valued is an associative array where keys are column names
   * from the enroll_flow_uw_teleport table.
   */
  protected function getTeleportationRecord() {
    return db_query("SELECT * FROM enroll_flow_uw_teleport WHERE uid = :uid", [':uid' => $this->u->getUid()])->fetchAssoc();
  }

  /**
   * @return bool|null
   */
  public function isUserMigrated() {
    return $this->teleportData['teleported'];
  }

  /**
   * @return int
   * - UNIX timestamp
   */
  public function getLastUpdated() {
    return $this->teleportData['updated'];
  }

  /**
   * @return int
   * - UNIX timestamp
   */
  public function getTeleportedTime() {
    return $this->teleportData['teleported_time'];
  }

  /**
   * Set the time the asyncronous teleportation of user's data has begun.
   */
  private function setAsyncStart() {
    $this->teleportData['async_start'] = time();
    $this->updateUserRecord();
  }

  /**
   * Get the time the asyncronous teleportation of user's data has begun.
   */
  public function getAsyncStart() {
    return $this->teleportData['async_start'];
  }

  /**
   * Set the time that courses were requested during asyncronous teleportation of user's data.
   */
  public function setCourseRequested() {
    $this->teleportData['course_requested'] = time();
    $this->updateUserRecord();
  }

  /**
   * Get the time that courses were requested during asyncronous teleportation of user's data.
   */
  public function getCourseRequested() {
    return $this->teleportData['course_requested'];
  }

  /**
   * Set the time that quizzes were requested during asyncronous teleportation of user's data.
   */
  public function setQuizzesRequested() {
    $this->teleportData['quizzes_requested'] = time();
    $this->updateUserRecord();
  }

  /**
   * Get the time that quizzes were requested during asyncronous teleportation of user's data.
   */
  public function getQuizzesRequested() {
    return $this->teleportData['quizzes_requested'];
  }

  /**
   * Set the time that quiz data was requested during asyncronous teleportation of user's data.
   */
  public function setQuizDataRequested() {
    $this->teleportData['quiz_data_requested'] = time();
    $this->updateUserRecord();
  }

  /**
   * Get the time that quizzes were requested during asyncronous teleportation of user's data.
   */
  public function getQuizDataRequested() {
    return $this->teleportData['quiz_data_requested'];
  }

  /**
   * Set the time that asyncronous teleportation of user's data was completed.
   */
  public function setAsyncComplete() {
    $this->teleportData['async_complete'] = time();
    $this->updateUserRecord();
  }

  /**
   * Get the time that asyncronous teleportation of user's data was completed.
   */
  public function getAsyncComplete() {
    return $this->teleportData['async_complete'];
  }


  /**
   * Set the time that asyncronous teleportation of user's data was completed.
   * todo: update message that is inserted
   */
  public function setAsyncErrorMessage($error) {
    $this->teleportData['error_message'] = $error;
    $this->updateUserRecord();
  }

  /**
   * Get the time that asyncronous teleportation of user's data was completed.
   */
  public function getAsyncErrorMessage() {
    return $this->teleportData['error_message'];
  }

  /**
   * Reset all status variables that are utilized during the asynchronous migration. Should be called before
   * beginning a new migration.
   */
  public function resetAsyncMigration() {
    $this->teleportData['async_start'] = NULL;
    $this->teleportData['course_requested'] = NULL;
    $this->teleportData['quizzes_requested'] = NULL;
    $this->teleportData['quiz_data_requested'] = NULL;
    $this->teleportData['async_complete'] = NULL;
    $this->teleportData['error_message'] = NULL;
    $this->teleportData['teleported'] = 0;
    $this->teleportData['teleported_time'] = NULL;
    $this->updateUserRecord();
  }



  /**
   * Return the number of users who are marked phase 1 eligible.
   * @return int
   */
  public static function countPhase1Eligible() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE phase1_eligible = 1")->fetchField();
  }

  /**
   * Return the number of users who are marked phase 2 eligible.
   * @return int
   */
  public static function countPhase2Eligible() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE phase2_eligible = 1")->fetchField();
  }

  /**
   * Return the number of users who are marked phase 3 eligible.
   * @return int
   */
  public static function countPhase3Eligible() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE phase3_eligible = 1")->fetchField();
  }

  /**
   * Return the number of users who are marked as having teleported to UWorld.
   * @return int
   */
  public static function countTeleported() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE teleported = 1")->fetchField();
  }

  /**
   * Return the number of users who are marked as having NOT teleported to UWorld.
   * @return int
   */
  public static function countNotTeleported() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE teleported = 0")->fetchField();
  }

  /**
   * Return the number of users who are marked as having teleported to UWorld with data.
   * @return int
   */
  public static function countTeleportedWithData() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE teleported = 1 AND async_complete IS NOT NULL")->fetchField();
  }

  /**
   * Return the number of users who are marked as having teleported to UWorld with data.
   * @return int
   */
  public static function countTeleportedWithoutData() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE teleported = 1 AND async_complete IS NULL")->fetchField();
  }

  /**
   * Return the number of users who are marked as zero progress eligible. It should be assumed that all of these users
   * have also been teleported, since it happens automatically.
   * @return int
   */
  public static function countZeroProgressEligible() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE zero_progress_eligible = 1")->fetchField();
  }

  /**
   * Return the total number of users who are ineligible for migration.
   * @return int
   */
  public static function countIneligibleUnmigrated() {
    return db_query("SELECT count(*) FROM enroll_flow_uw_teleport WHERE teleported = 0 AND (phase1_eligible <> 1 AND phase2_eligible <> 1 AND zero_progress_eligible <> 1)")->fetchField();
  }

  /**
   * Return the total number of users who are eligible for migration but not yet migrated.
   * @return int
   */
  public static function countEligibleUnmigrated() {
    return db_query("SELECT count(DISTINCT(uid)) FROM enroll_flow_uw_teleport WHERE teleported = 0 AND (phase1_eligible = 1 OR phase2_eligible = 1 OR zero_progress_eligible = 1)")->fetchField();
  }

  /**
   * Return an array of user ids that have been temporarily blacklisted (ie, are not able to migrate due to an error
   * during an earlier attempt).
   * @return array
   */
  public static function getBlacklistedUsers() {
    return db_query("SELECT uid FROM enroll_flow_uw_teleport WHERE async_complete IS NOT NULL AND teleported = 0")->fetchAll();
  }

  /**
   * Redirect a user with an in-progress migration to the 'migration in progress page', and a user who has completed
   * a migration to the 'migration complete' page.
   * @throws Exception
   */
  public static function handleRedirect() {
    // When user is migrating their data to Uworld, they cannot access any student pages.
    if(user_is_logged_in() && variable_get('uw_phase2_teleport', FALSE)) {
      $pages = array(
        'dashboard', 'node/242', 'dashboard/*', 'ipq', 'ipq/*', 'study/*', 'study-cram/*', 'homework-help-center',
        'homework-help-center/*', 'exam_scores', 'user*'
      );
      $block_access = false;
      global $user;
      if (drupal_match_path(current_path(), implode("\n", $pages))) {
        $block_access = true;
      }
      // We don't want to prevent them from accessing the platform migration pages
      if (drupal_match_path(current_path(), 'user/' . $user->uid . '/platform-migrate*')) {
        $block_access = false;
      }
      // Customer care admins get access to any pages (necessary so they can migrate other users)
      if (user_access('access customercare')) {
        $block_access = false;
      }

      // Redirect student to platform migrate with data and step parameters
      if($block_access) {
        $ut = new UserTeleportation($user->uid);
        $ents = $ut->u->getValidEntitlements();
        $act_student = false;
        foreach ($ents as $e) {
          // ACT entitlements are allowed to return to RCPAR even if they have migrated
          if($e->isTypeACT() || $e->isOnlineCourseACT()) {
            $act_student = true;
          }
        }
        
        // Redirect to the in progress page if the user is currently migrating.
        if($ut->isMigrationInProgress()) {
          $options = array('query' => array('data' => '1', 'step' => 'in-progress'));
          drupal_goto('/user/' . $user->uid . '/platform-migrate', $options);
        }
        
        // Redirect to the completion page if the user has completed a migration and does not have an active ACT entitlement
        if($ut->isUserMigrated() && !$act_student) {
          $options = array('query' => array('data' => '1', 'step' => 'confirmation'));
          drupal_goto('/user/' . $user->uid . '/platform-migrate', $options);
        }
      }
    }
  }

  /**
   * Returns true if the user has an active migration in progress - e.g. the migration has started, but not finished.
   * @return bool
   */
  public function isMigrationInProgress() {
    return $this->getAsyncStart() && !$this->getAsyncComplete();
  }

}
