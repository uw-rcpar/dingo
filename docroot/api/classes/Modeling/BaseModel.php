<?php

namespace RCPAR\Modeling;

class BaseModel {
  /**
   * Object with fields that represent all of the database tables
   * @var stdClass
   */
  protected $entity;

  /**
   * Extending class should define table name
   * @var string
   */
  static $tableName;

  protected $isNew = FALSE;
  protected $isChanged = FALSE;

  /**
   * BaseModel constructor.
   * @param null $id_or_object
   */
  public function __construct($id_or_object = NULL) {
    $this->entity = new \stdClass();

    // Existing entity ID or entity data
    if (!is_null($id_or_object)) {
      // If we were given an ID, we set the ID and load the entity from the DB
      if (is_numeric($id_or_object)) {
        $this->setId($id_or_object);
        $this->entity = db_query("SELECT * FROM {$this->getTableName()} WHERE id = :id",
          array(
            ':id' => $id_or_object,
          )
        )->fetchObject();
      }
      elseif($id_or_object instanceof \stdClass) {
        $entity = new \stdClass();
        // Assume we have a pre-queried entity with all fields present in the record
        // as if the user did a SELECT * FROM table WHERE id = x
        foreach ($id_or_object as $fieldName => $fieldValue) {
          $entity->{$fieldName} = $fieldValue;
        }
        $this->entity = $entity;
      }
    }
    // NULL value - new entity
    else {
      $this->setIsNew(TRUE);
      $this->entity = new \stdClass();
    }

    // Anything we set as part of the constructor should not count as changing the entity
    $this->isChanged = FALSE;
  }

  /**
   * @return static
   */
  public static function create() {
    return new static();
  }

  /**
   * Query for all records of this model type and return an array of as intantiated
   * @return static[]
   */
  public static function getAll() {
    $items = array();

    // Query the db for an array of all channels
    $result = db_select(static::$tableName, 't')
      ->fields('t') // All fields
      ->execute();
    foreach ($result as $record) {
      $items[] = new static($record);
    }

    return $items;
  }

  /**
   * Delete the entity from the database
   */
  public function delete() {
    if($this->getId()) {
      db_query("DELETE FROM {$this->getTableName()} WHERE id = :id", array(
        ':id' => $this->getId())
      );
    }
  }

  /**
   * Save the record to the database
   */
  public function save() {
    $this->setIsNew(FALSE);
    $this->isChanged = FALSE;

    // Pass primary key to the udpater if we have them
    $pk = array();
    if(!empty($this->getId())) {
      $pk[] = 'id';
    }

    drupal_write_record($this->getTableName(), $this->getEntity(), $pk);
    return $this;
  }

  /**
   * @return bool
   */
  public function isNew() {
    return $this->isNew;
  }


  /**
   * Return the primary key ID of the entity
   * Defaults to 0 if the entity does not yet have an ID
   * @return int
   */
  public function getId() {
    if(!$this->isNew()) {
      return $this->getEntity()->id;
    }

    return 0;
  }

  /**
   * @return string
   */
  public function getTableName() {
    return static::$tableName;
  }

  /**
   * @return object
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * @param bool $isNew
   */
  public function setIsNew($isNew) {
    $this->isNew = $isNew;
  }

  /**
   * Get the value of a property or field on the entity
   * @param string $name
   */
  public function get($name) {
    return $this->getEntity()->{$name};
  }

  /**
   * @param int $value
   */
  public function setId($value) {
    $this->set('id', $value);
  }


  /**
   * Set an entity value by name.
   *
   * @param string $name
   * @param mixed $value
   *
   * @return $this
   */
  public function set($name, $value) {
    // Changing any properties of the object should flag it has being changed
    $this->isChanged = TRUE;

    $this->entity->{$name} = $value;
    return $this;
  }

  /**
   * Return a formatted date string from a UNIX timestamp
   * @param int $val
   * - Timestamp
   * @param string $format
   * - Date format to pass to format_date. If NULL, format_date will not be used and the raw timestamp
   * will be returned
   * @return string
   * - A formatted date string
   */
  public function getDateFromTimestamp($val, $format = NULL) {
    if(is_null($format)) {
      return $val;
    }
    return format_date($val, 'custom', $format);
  }


  /**
   * Return all fields in an entity.
   * @return array
   */
  public function returnPayload() {
    $payload = array();
    foreach ($this->getEntity() as $fieldname => $fieldvalue) {
      $payload[$fieldname] = $fieldvalue;
    }
    return $payload;
  }

}