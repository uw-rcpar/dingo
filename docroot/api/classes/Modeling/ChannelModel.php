<?php

namespace RCPAR\Modeling;


class ChannelModel extends BaseModel {
  // Note - the channel model does not work against database storage, but hard coded array data
  static $tableName = '';

  // Not necessary, but makes it easier to see what the ID is when debugging
  protected $channelID;

  /**
   * Get the channel's machine name.
   * @return string
   */
  public function getMachineName() {
    return $this->get('machine_name');
  }

  /**
   * Get the channel's type.
   * @return string
   */
  public function getName() {
    return $this->get('name');
  }

  /**
   * Get the channel's weight.
   * @return int
   */
  public function getWeight() {
    return $this->get('weight');
  }

  /**
   * Get the channel's description.
   * @return string
   */
  public function getDescription() {
    return $this->get('description');
  }

  /**
   * Sets the channel's machine name.
   * @param $value
   * @return $this
   */
  public function setMachineName($value) {
    $this->set('machine_name', $value);
    return $this;
  }

  /**
   * Sets the channel's name.
   * @param $value
   * @return $this
   */
  public function setName($value) {
    $this->set('name', $value);
    return $this;
  }

  /**
   * Sets the channel's weight.
   * @param $value
   * @return $this
   */
  public function setWeight($value) {
    $this->set('weight', $value);
    return $this;
  }

}