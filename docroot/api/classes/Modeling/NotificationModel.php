<?php

namespace RCPAR\Modeling;


class NotificationModel extends BaseModel {
  static $tableName = 'notif_notifications';

  /**
   * Get the receipt's user ID.
   * @return int
   */
  public function getUid() {
    return $this->get('uid');
  }

  /**
   * Set the receipt's user ID.
   * @param int
   */
  public function setUid($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * Get the receipt's type
   * @return string
   */
  public function getType() {
    return $this->get('type');
  }

  /**
   * Get the receipt's title
   * @return string
   */
  public function getTitle() {
    return $this->get('title');
  }

  /**
   * Set the receipt's title
   * @param string
   */
  public function setTitle($value) {
    $this->set('title', $value);
    return $this;
  }

  /**
   * Get the receipt's teaser
   * @param $value
   * @return $this
   */
  public function getTeaser() {
    return $this->get('teaser');
  }

  /**
   * Set the receipt's teaser
   * @param $value
   * @return $this
   */
  public function setTeaser($value) {
    $this->set('teaser', $value);
    return $this;
  }

  /**
   * Set the receipt's type
   * @param string
   */
  public function setType($value) {
    $this->set('type', $value);
    return $this;
  }

  /**
   * Get the receipt's message
   * @return string
   */
  public function getMessage() {
    return $this->get('message');
  }

  /**
   * Set the receipt's message
   * @param string
   */
  public function setMessage($value) {
    $this->set('message', $value);
    return $this;
  }

  /**
   * Get the expiration date
   * @param string $format
   * - An optional format to be passed into format_date. If omitted, the raw value is returned
   * @return int|string
   */
  public function getExpire($format = NULL) {
    return $this->getDateFromTimestamp($this->get('expire'), $format);
  }

  /**
   * Set the expire
   * @param int
   */
  public function setExpire($value) {
    $this->set('expire', $value);
    return $this;
  }

  /**
   * Get the is_read
   * @return bool
   */
  public function getIsRead() {
    return $this->get('is_read');
  }

  /**
   * Set the is_read
   * @param bool
   */
  public function setIsRead($value) {
    $this->set('is_read', $value);
    return $this;
  }

  /**
   * Get the receipt's broadcast channel ID.
   * @return int
   */
  public function getBroadcastChannelId() {
    return $this->get('broadcast_channel_id');
  }

  /**
   * Set the receipt's broadcast channel ID.
   * @param int
   */
  public function setBroadcastChannelId($broadcast_channel_id) {
    $this->set('broadcast_channel_id', $broadcast_channel_id);
    return $this;
  }

  /** Get the receipt's broadcast ID.
   * @return int
   */
  public function getNotificationTemplateId() {
    return $this->get('notification_template_id');
  }

  /**
   * Set the receipt's broadcast ID.
   * @param int
   */
  public function setNotificationTemplateId($value) {
    $this->set('notification_template_id', $value);
    return $this;
  }

  /**
   * Get the receipt's created date.
   * @param string $format
   * - An optional format to be passed into format_date. If omitted, the raw value is returned
   * @return int|string
   */
  public function getCreated($format = NULL) {
    return $this->getDateFromTimestamp($this->get('created'), $format);
  }

  /**
   * Set the receipt's created date.
   * @param int
   */
  public function setCreated($timestamp = NULL) {
    if(is_null($timestamp)) {
      $timestamp = REQUEST_TIME;
    }
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * Get the read_on
   * @param string $format
   * - An optional format to be passed into format_date. If omitted, the raw value is returned
   * @return int|string
   */
  public function getReadOn($format = NULL) {
    return $this->getDateFromTimestamp($this->get('read_on'), $format);
  }

  /**
   * Set the read_on
   * @param int
   */
  public function setReadOn($value) {
    $this->set('read_on', $value);
    return $this;
  }

  /**
   * Mark the notification as read for the current request time
   */
  public function markRead() {
    $this->setIsRead(TRUE);
    $this->setReadOn(REQUEST_TIME);
  }

}