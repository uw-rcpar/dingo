<?php

namespace RCPAR\Modeling;

use RCPAR\Rest\V1\Payload;
use RCPAR\Rest\V1\Token;

class ModalModel extends BaseModel {
  static $tableName = 'rcpar_modals';

  /**
   * Get data
   * @return array
   */
  public function getData() {
    return $this->get('data');
  }

  /**
   * Get timestamp
   * @return int
   */
  public function getTimestamp() {
    return $this->get('timestamp');
  }

  /**
   * Get machinename
   * @return string
   */
  public function getMachineName() {
    return $this->get('machine_name');
  }

  /**
   * Get status
   * @return int
   */
  public function getStatus() {
    return $this->get('status');
  }

  /**
   * Set status
   * @param int $value
   * @return $this
   */
  public function setStatus($value) {
    $this->set('status', $value);
    return $this;
  }

  /**
   * Set machinename
   * @param string $value
   * @return $this
   */
  public function setMachinename($value) {
    $this->set('machine_name', $value);
    return $this;
  }

  /**
   * Set uid
   * @param int $value
   * @return $this
   */
  public function setUid($value) {
    $this->set('uid', $value);
    return $this;
  }

  /**
   * Set timestamp
   * @param int $value
   * @return $this
   */
  public function setTimestamp($value) {
    $this->set('timestamp', $value);
    return $this;
  }

  /**
   * Set data
   * @param array $value
   * @return $this
   */
  public function setData($value) {
    $this->set('data', $value);
    return $this;
  }

  /**
   * A function to get all the modals of a user, regardless of modal status.
   * @param int $uid
   * - user id
   * @return ModalModel[]
   */
  public static function getUserModals($uid) {
    $results = db_query("SELECT * FROM {rcpar_modals} WHERE uid = :uid",
      array(
        ':uid' => $uid,
      )
    )->fetchAll();
    $modals = array();
    foreach ($results as $result) {
      $modals[] = new ModalModel($result->id);
    }
    return $modals;
  }

  /**
   * A function to get all the modals a user has seen.
   * @param int $uid
   * - user id
   * @return ModalModel[]
   */
  public static function getUserModalsSeen($uid) {
    $allUserModals = self::getUserModals($uid);
    $modals = array();
    foreach ($allUserModals as $modal) {
      if ($modal->getStatus()) {
        $modals[] = $modal;
      }
    }
    return $modals;
  }

  /**
   * Get a block machine name OR box delta from it's block id
   * @param int $bid
   * - block id
   * @return FALSE|string
   * - returns false or a block machine name
   */
  public static function getModalBlockMachineName($bid) {
    $machine_name = false;

    // Look up the block via block id.
    $block = db_query("SELECT * FROM {block} WHERE bid = :bid", array(':bid' => $bid))->fetchAssoc();
    if ($block) {
      // Get the block machine name.
      $machine_name = block_machine_name_get_machine_name($block['delta']);
    }
    // We might have a box instead of a block. Get it's delta (which is a string).
    else {
      $box = db_query("SELECT * FROM {box} WHERE delta = :bid", array(':bid' => $bid))->fetchAssoc();
      $machine_name = $box['delta'];
    }
    return $machine_name;
  }
  /**
   * Get a box delta from it's block id
   * @param int $bid
   * - block id
   * @return FALSE|string
   * - returns false or a block machine name
   */
  public static function getModalBoxDelta($bid) {
    // Because we have to update modal records from user cookies,
    // we have to look up the block via block id rather than block delta.
    // (User's cookies are using block id.)
    $block = db_query("SELECT * FROM {box} WHERE delta = :bid", array(':bid' => $bid))->fetchAssoc();

    // Return the block machine name.
    return $block['delta'];
  }

  /**
   * Determine if a particular modal in a block has been logged for a specific user
   * @param int $uid
   * - user id
   * @param mixed $bid_or_machine_name
   * - block id or machine name
   * @return FALSE|int
   * - returns false or a block id
   */
  public static function modalHasBeenLogged($uid, $bid_or_machine_name) {
    if (is_numeric($bid_or_machine_name)) {
      // We have a block id
      $block_machine_name = self::getModalBlockMachineName($bid_or_machine_name);
    }
    else {
      // We have a machine name
      $block_machine_name = $bid_or_machine_name;
    }

    return db_query("SELECT id FROM {rcpar_modals} WHERE uid = :u AND machine_name = :m", [':u' => $uid, ':m' => $block_machine_name])->fetchField();

  }


  /**
   * Determine if a modal has been seen by a user within the last hour.
   * @param int $uid
   * - user id
   * @return bool
   */
  public static function modalCanBeShown($uid) {
    $within_an_hour = db_query("SELECT timestamp FROM {rcpar_modals} WHERE uid = :u AND timestamp > :time AND status = 1", [':u' => $uid, ':time' => time() - 60 * 60])->fetchAll();
    if ($within_an_hour) {
      return false;
    }
    else {
      return true;
    }
  }


  /**
   * Log a modal as seen for a particular user.
   * @param int $uid
   * - user id
   * @param string $modal_id
   * - modal machine name
   */
  public static function logUserModals($uid, $modal_id) {
    // Create a modal object and set its values.
    $modal = new ModalModel();
    $modal->setStatus('1');
    $modal->setData('');
    $modal->setTimestamp(REQUEST_TIME);
    $modal->setUid($uid);
    $modal->setMachinename($modal_id);
    $modal->save();
  }

  /**
   * Update a user's modal status
   * @param int $uid
   * - user id
   * @param string $modal_id
   * - modal machine name
   * @param bool $status
   * - whether the modal has been seen by the user
   *
   */
  public static function updateUserModals($uid, $modal_id, $status) {
    $user_modals = self::getUserModals($uid);
    foreach ($user_modals as $modal) {
      if ($modal->getMachineName() == $modal_id) {
        $modal->setStatus($status);
        $modal->save();
      }
    }
  }

  /**
   * Update a user's modal timestamp
   * @param int $uid
   * - user id
   * @param string $modal_id
   * - modal machine name
   * @param bool $timestamp
   * - a unix timestamp
   *
   */
  public static function updateUserModalTimestamp($uid, $modal_id, $timestamp) {
    $user_modals = self::getUserModals($uid);
    foreach ($user_modals as $modal) {
      if ($modal->getMachineName() == $modal_id) {
        $modal->setTimestamp($timestamp);
        $modal->save();
      }
    }
  }

  /**
   * Transfer user modal cookie information to database (for modals in blocks)
   * @param int $uid
   * - user id
   * @param mixed $bid_or_machine_name
   * - block id or machine name
   * @return bool
   */
  public static function updateUserModalsFromCookie($uid, $bid_or_machine_name) {
    // User may have some older cookies that are not relevant.
    if (!$bid_or_machine_name) {
      return false;
    }
    // Check if the user already has a db record for the modal.
    $modalLogged = self::modalHasBeenLogged($uid, $bid_or_machine_name);

    // If the modal is not logged in the db already, log it.
    if (!$modalLogged) {
      $modal = new ModalModel();
      $modal->setStatus('1');
      $modal->setData('');
      $modal->setTimestamp(REQUEST_TIME);
      $modal->setUid($uid);
      if (is_numeric($bid_or_machine_name)) {
        $modal->setMachinename(self::getModalBlockMachineName($bid_or_machine_name));
      }
      else {
        $modal->setMachinename($bid_or_machine_name);
      }
      $modal->save();
    }
  }


}