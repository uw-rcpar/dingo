<?php

namespace RCPAR;

use RCPAR\Entities\UpsellProduct;
use RCPAR\Entities\UserEntitlement;
use RCPAR\Entities\UserEntitlementCollection;
use RCPARCommerceOrder;
use RCPARPartner;

class UserEntitlementManager {
  /**
   * @var User $u
   */
  protected $u;

  /**
   * @var RCPARCommerceOrder $o
   */
  protected $o;

  /**
   * The user's existing entitlements prior to the order
   * @var UserEntitlementCollection $existingEntitlements;
   */
  protected $existingEntitlements;

  /**
   * The new entitlements that will be generated from this order
   * @var UserEntitlementCollection
   */
  protected $ents;

  /**
   * @var RCPARPartner
   */
  protected $p;

  /**
   * UserEntitlementManager constructor.
   * @param $user
   * @param RCPARCommerceOrder $o
   */
  public function __construct($user, RCPARCommerceOrder $o) {
    // Our fundamental variables needed to process: user, order, and user's entitlements
    $this->u = new User($user->uid);
    $this->existingEntitlements = $this->u->getValidEntitlements(); // Note: this may be an empty collection. That's ok.
    $this->ents = new UserEntitlementCollection();
    $this->o = $o;
  }


  /**
   * Build a new collection of entitlements based on products in the order
   */
  public function buildEntitlements() {
    // Build entitlement stubs for our new collection from line items on the order. This populates the static properties:
    // Product ID, Order ID, Title, Product Title, SKU, Bundled Product, Author, Course Type, Course Section
    $this->buildEntitlementsFromLineItems();

    // Iterate through each of the entitlements that we're building and populate additional fields
    foreach ($this->getEnts() as $e) {
      // Set duration, interval, and therefore expiration. Note that for offline lectures, expiration may be changed later
      $this->entitlementSetDurationAndExpiration($e);

      // Set exam version. May be overridden later if versioning is on.
      $e->setExamVersionSingleVal(exam_version_get_default_version_tid());

      // CRAM delayed activation
      $this->entitlementSetDelayedActivation($e);

      // IPQ Access.
      $this->entitlementSetIpq($e);

      // HHC access
      $this->entitlementSetHHC($e);

      // Offline lectures: generate serial numbers
      $this->entitlementSetOfflineSerialNumber($e);
    }

    /**
     * Apply modifications that require looking at the new entitlement collection as a whole.
     */
    // Apply expiration date to offline courses so that it matches online courses
    $this->collectionSetOfflineExpiration();

    // Handle user-selected exam versions
    $this->collectionSetExamVersion();

    // Handle course extensions
    $this->collectionSetExtensions();

    // Handle mobile offline access
    $this->collectionSetMobileOfflineAccess();

    // Handle Unlimited offline access
    $this->collectionSetUnlimitedAccess();
  }

  /**
   * Iterate through the line items on the order and set the basic, static properties that we can infer
   * from the line items themselves. We call these 'static' because no other future code should make changes
   * to them.
   * Product ID, Order ID, Title, Product Title, SKU, Bundled Product, Author, Course Type, Course Section
   */
  public function buildEntitlementsFromLineItems() {
    $o = $this->o;
    $u = $this->u;
    $existing_ents = $this->existingEntitlements;

    // Values needed from the order
    $order_id = $o->getOrderId();
    $bundled_product_sku = $o->getBundledProductSku(); // @todo - partner version?

    // Values needed from the user
    $uid = $u->getUid();

    // Iterate through each line item on the order to look for products that should have
    // entitlements created.
    foreach ($o->wrapper->commerce_line_items as $line_wrapper) {
      // Ignore line items like sales tax, coupons, etc.
      if ($line_wrapper->getBundle() != 'product') {
        continue;
      }


      // Skip this row if this product shouldn't provide an an entitlement
      $product_id = $line_wrapper->commerce_product->product_id->value();
      if (!user_entitlements_is_valid_entitlement_product($product_id)) {
        continue;
      }

      // These are all the values that we need from the line item
      $sku = $line_wrapper->commerce_product->sku->value();
      $title = $line_wrapper->commerce_product->label();
      try {
        $course_type_tid = $line_wrapper->commerce_product->field_course_type_ref->raw();
      }
      catch (\EntityMetadataWrapperException $e) {
        // Swallow errors in the rare case that we don't have a course type
      }
      $section_name = user_entitlements_get_course_section($sku);

      // If the user already has this entitlement, we'll update the existing entitlement node.
      // This is not really a use-case that we officially support, but legacy code handled this
      // situation, and it prevents us from creating duplicate entitlements in case there's an
      // unexpected scenario.
      if (!($e = $existing_ents->getEntitlementBySku($sku))) {
        // User does not have this entitlement, create a fresh one
        $e = UserEntitlement::create();
      }

      // Add this entitlement to the entitlement collection
      $this->getEnts()->add($e);
      if (!user_entitlements_is_cram($product_id)){
        // No-cram courses should set activation date when title was created
        $e->setActivationDate(time()) ;
      }
      
      // Set static properties
      $e->setProductId($product_id)
        ->setOrderId($order_id)
        ->setTitle($title)
        ->setProductTitle($title)
        ->setProductSku($sku)
        ->setBundledProduct($bundled_product_sku)
        ->setAuthorId($uid)
        ->setCourseTypeRef($course_type_tid)
        ->setCourseSection($section_name);
    }
  }

  /**
   * For CRAM products, we set a delayed activate flag and expiration date
   * @param UserEntitlement $e
   */
  function entitlementSetDelayedActivation(UserEntitlement $e) {
    // Determine if this is a delayed activation entitlement. CRAM products are not activated right away.
    if (in_array($e->getProductId(), user_entitlements_get_delayed_products_ids())) {
      $e->setActivationDelayed(1);
      $e->setExpirationDate(1262304000); // Date will be expired since 01/01/2010
    }
  }

  /**
   * For standard online courses, determine IPQ access
   * @param UserEntitlement $e
   */
  function entitlementSetIpq(UserEntitlement $e) {
    // Defaults to TRUE for non-partner all use cases - only applies to standard online course
    if ($e->isTypeOnlineCourse()) {
      $e->setIpqAccess(TRUE);
    }
  }

  /**
   * Calculate the duration, interval, and therefor expiration of a user entitlement.
   * @param UserEntitlement $e
   */
  function entitlementSetDurationAndExpiration(UserEntitlement $e) {
    $product_id = $e->getProductId();

    // Set the default duration and interval
    $e->setCourseDuration(variable_get('course_duration', ''));
    $e->setDefaultInterval(variable_get('course_interval', ''));

    // CRAM - set expiration based on system settings for CRAM courses
    if ($cram_pids = user_entitlements_is_cram($product_id)) {
      $e->setCourseDuration(variable_get('cram_duration', ''));
      $e->setDefaultInterval(variable_get('cram_interval', ''));
    }

    // Audio lectures - set expiration based on system settings for audio lectures
    if (user_entitlements_is_audio_course($product_id)) {
      $e->setCourseDuration(variable_get('audio_int_duration', ''));
      $e->setDefaultInterval(variable_get('audio_int_interval', ''));
    }

    // Set the expiration based on duration and interval
    $e->setExpirationDate(strtotime('+' . $e->getCourseDuration() . ' ' . $e->getDefaultInterval(), REQUEST_TIME));

    // Set a matching original expiration
    $e->setOriginalExpirationDate($e->getExpirationDate());
  }

  /**
   * Implements hook_save_user_entitlement_alter()
   * Determines level of HHC access (none, read or write) when a user entitlement is saved.
   *
   * @param \RCPAR\Entities\UserEntitlement $e
   * - A user entitlement object to be modified, if necessary.
   */
  protected function entitlementSetHHC(UserEntitlement $e) {
    // rules for homework center access are
    // WRITE:
    // - Elite or Premier with partner_nid == 0
    // - DB Enrollees - partner type: (any), partner billing type: Direct Bill (directbill)
    // - Firm/Univ Discount - partner type: (by def not Normal), partner billing type: Credit Card/Affirm (creditcard)  //
    // READ:
    // - Select students with partner_nid == 0
    // - Free Trial - partner type: Normal (1), partner billing type: Free Access (freetrial)
    // - FSL - partner type: Firm/Partner (1), partner billing type: Free Access (freetrial)
    // NONE:
    // - ASL - partner type: University (3), partner billing type: Free Access (freetrial)

    // By default you get nothing
    $e->setHhcAccess('_none');

    // We're only dealing with standard online course entitlements (AUD, BEC, FAR, and REG)
    if (!$e->isTypeOnlineCourse()) {
      return;
    }

    // If we're here, this some kind of standard online course, and it's not a partner-based transaction, which
    // means at minimum, 'read' access. This includes select course or single-part online courses.
    $e->setHhcAccess('read');

    // If this is an Elite or Premier course, the world is your oyster
    if (in_array($e->getBundledProduct(), ['FULL-ELITE-DIS', 'FULL-PREM-DIS'])) {
      $e->setHhcAccess('write');
      return;
    }
  }

  /**
   * For offline lectures and offline CRAM products, set a serial number
   * @param UserEntitlement $e
   */
  protected function entitlementSetOfflineSerialNumber(UserEntitlement $e) {
    $entitlement_pid = $e->getProductId();

    if (user_entitlements_is_offline_lectures($entitlement_pid, TRUE) || user_entitlements_is_offline_cram($entitlement_pid, TRUE)) {
      if(empty($e->getSerialNumber())){
        $e->setSerialNumber(_offline_serial_number_set_serial_no());
      }

      // Pass data into $_SESSION for use in bootstrap_rcpar_commerce_email_prepare_table()
      $_SESSION['serial_numbers'][$entitlement_pid] = array(
        'product_id' => $entitlement_pid,
        'installs' => $e->getInstalls(),
        'expiration_date' => $e->getExpirationDate(),
        'serial_number' => $e->getSerialNumber(),
      );
    }
  }

  /**
   * For each offline lecture product in the new entitlements, set its expiration
   * based on a matching existing entitlement, or matching entitlement in the order.
   */
  protected function collectionSetOfflineExpiration() {
    $ents = $this->ents;

    // Create a search pool from both the new and previously existing entitlements
    $search_pool = new UserEntitlementCollection();
    $search_pool
      ->merge($ents)
      ->merge($this->existingEntitlements);

    // Set expiration of offline lectures based on entitlements being obtained or already owned
    foreach ($ents->filterByOfflineLecture() as $e) {
      $offline_section = $e->getCourseSectionName();
      // Look for a matching online course from the entitlements in the current order
      if ($matching_online_ent = $search_pool
        ->filterByOnlineCourse()
        ->filterBySectionName($offline_section)
        ->getFirst())
      {
        // We found an online course that matches this offline course.
        // Set the expiration of this offline course to match the corresponding online course
        $e->setExpirationDate($matching_online_ent->getExpirationDate());
        $e->setOriginalExpirationDate($matching_online_ent->getOriginalExpirationDate());
        $e->setCourseDuration($matching_online_ent->getCourseDuration());
        $e->setDefaultInterval($matching_online_ent->getDefaultInterval());
        continue;
      }
    }

  }

  /**
   * If exam versioning is on, apply exam versions to any products in the order that the user
   * has made version selections for.
   */
  protected function collectionSetExamVersion() {
    if (exam_version_is_versioning_on()) {
      // NOTE: Exam versioning is not on and won't be for probably a long time. If it does
      // get turned on, this code (and much, much more) should be re-tested.
      foreach ($this->o->getTextbooksVersions() as $sku => $selected_year) {
        if ($e = $this->ents->getEntitlementBySku($sku)) {
          $e->setExamVersionSingleVal(exam_version_get_tid_from_year_name($selected_year));
        }
      }
    }
  }

  /**
   * Apply extensions to a users entitlements based on user and order data
   */
  protected function collectionSetExtensions() {
    $o = $this->o;
    $ents = $this->ents;

    // Extensions may be needed for expired entitlements
    $expired_ents = $this->u->getExpiredEntitlements()
      ->filterByUnlimitedAccess()
      ->filterByLatestExpirationDate();

    // Create a search pool from both the new and previously existing entitlements
    $search_pool = new UserEntitlementCollection();
    $search_pool
      ->merge($this->existingEntitlements)
      ->merge($expired_ents)
      ->merge($ents);

    // Get the extension product ids
    module_load_include('inc', 'enroll_flow', 'includes/enroll_flow.admin');
    $extenstion_products = array_keys(enroll_flow_get_products_list("course_extension"));

    // If we have a FULL extension product we'll need all the individual section product ids
    if (in_array('FULL-6MEXT', array_keys($o->getAllProductsBySKU()))) {
      $sixmoProdIdsInOrder = user_entitlements_get_full_extension_product_ids();
    }
    else {
      // Get 6 mo extension products from this order if not a FULL extension
      $sixmoProdIdsInOrder = array_intersect(
        $o->getAllProductIdsExpanded(), $extenstion_products
      );
    }

    // No extension product? We're done here
    if (empty($sixmoProdIdsInOrder)) {
      return;
    }

    // Get the user's online course entitlements
    $courseEnts = $search_pool->filterByOnlineCourse();

    // Apply extension to all of the matching course entitlements
    foreach ($sixmoProdIdsInOrder as $pid) {
      // Get the values for how much to extend from the extension product itself
      $product = commerce_product_load($pid);
      $product = entity_metadata_wrapper('commerce_product', $product);
      $course_duration = $product->field_extension_duration->value();
      $interval = $product->field_extension_interval->value();
      $sku = $product->sku->value();
      $unlimited_extension = (strpos($sku, 'UAEXT') !== FALSE) ? TRUE : FALSE;
      $sku_parts = explode('-', $sku);
      $extension_section = $sku_parts[0];

      if ($e = $courseEnts->getEntitlementBySku($extension_section)) {
        // If this is an unlimited access extension and the entitlement is already expired
        // we set the activation date to six months from today. Otherwise we just add it to
        // current expiration date
        $original_expiration = ($unlimited_extension && !$e->isActive()) ? time() : $e->getExpirationDate();
        $new_date = strtotime('+' . $course_duration . ' ' . $interval, $original_expiration);
        $e->setDurationExtended(TRUE); // Flag has having been extended
        $e->setExtendedCourseDuration($course_duration); // Store value of extension's duration
        $e->setExtendedInterval($interval); // Store value of extension's interval
        $e->setExpirationDate($new_date);
      }
    }
  }

  /**
   * Apply mobile offline access to a user's entitlements based on both their existing entitlements and their order.
   */
  protected function collectionSetMobileOfflineAccess() {
    $o = $this->o;
    $ents = $this->ents;
    $existingEnts = $this->existingEntitlements;

    // If we have a FULL Mobile offline access product we'll need all the individual section product ids
    if (in_array('FULL-MOB-OFF', array_keys($o->getAllProductsBySKU()))) {
      $offline_product_ids_in_order = user_entitlements_get_mobile_offline_access_ids();
    }
    else {
      // Get individual MOA products from this order if not a FULL MOA product
      $offline_product_ids_in_order = array_intersect(
        $o->getAllProductIdsExpanded(), user_entitlements_get_mobile_offline_access_ids()
      );
    }

    // No MOA? Bail...
    if (empty($offline_product_ids_in_order)) {
      return;
    }

    // We will apply mobile offline access to any valid course that the user owns that does not
    // already have offline access if it should be granted access by proxy of having either purchased
    // offline access for that section just now or for that section at any time in the past, so long
    // as the past course it was purchased for is still valid.
    $mobile_access_sections = array();

    foreach ($offline_product_ids_in_order as $off_prod_id) {
      // Since we know we're dealing with MOA products, we can assume this is an upsell product
      $p = new UpsellProduct($off_prod_id);

      $section = $p->getCourseSectionName();
      $mobile_access_sections[$section] = $section;
    }

    // Now we'll look for any of the user's valid entitlements that already have mobile access set.
    // Having mobile access set means that the user purchased offline access for that part, and any
    // time they purchase an additional course of a matching section, it should inherit the offline
    // access that they already purchased.
    /** @var UserEntitlement $e */
    foreach ($existingEnts as $e) {
      if ($e->getMobileOfflineAccess()) {
        $section = $e->getCourseSectionName();
        $mobile_access_sections[$section] = $section;
      }
    }

    // Finally, we'll apply the mobile access to any online or CRAM course that does not already have
    // it based on the grants we determined above.
    foreach ($ents->filterByOnlineAndCramCourse() as $e) {
      // Check if this course section should get access...
      if (in_array($e->getCourseSectionName(), $mobile_access_sections)) {

        // It should! But only apply and save if it doesn't already have access.
        if (!$e->getMobileOfflineAccess()) {
          $e->setMobileOfflineAccess(1);
        }

      }
    }
  }

  /**
   * Flag entitlements as Unlimited if included in the order
   */
  protected function collectionSetUnlimitedAccess() {
    // No unlimited access? Bail...
    if (!$this->o->includesUnlimitedAccess()) {
      return;
    }
    // Get entitlements
    $ents = $this->ents;

    // Create a search pool from both the new and previously existing entitlements
    $search_pool = new UserEntitlementCollection();
    $search_pool
      ->merge($ents)
      ->merge($this->existingEntitlements);

    // Apply Unlimited access to all of the matching course entitlements
    foreach ($search_pool->filterByOnlineCourse() as $e) {
      $e->setUnlimitedAccess(TRUE);
    }
  }

  /**
   * Save all entitlements produced from this order
   */
  public function saveEntitlements() {
    // Save entitlements created by the manager
    $this->getEnts()->filterByChanged()->saveAll();

    // Ensure that any existing entitlements that have been modified are also saved.
    $this->u->getAllEntitlements()->filterByChanged()->saveAll();
  }

  /**
   * @return UserEntitlementCollection
   */
  public function getEnts(): UserEntitlementCollection {
    return $this->ents;
  }

}