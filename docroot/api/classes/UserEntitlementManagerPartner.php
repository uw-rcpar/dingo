<?php
/**
 * This version of the entitlement manager class is intended to process entitlements for partner based transactions.
 * It inherits all functionality from UserEntitlementManager except where necessary to make partner-based
 * overrides or modifications.
 */

namespace RCPAR;

use RCPAR\Entities\UserEntitlement;
use RCPARCommerceOrder;
use RCPARPartner;

class UserEntitlementManagerPartner extends UserEntitlementManager {
  /**
   * @inheritdoc
   */
  public function __construct($user, RCPARCommerceOrder $o, RCPARPartner $p) {
    parent::__construct($user, $o);

    // Inject our partner
    $this->setPartner($p);
  }

  /**
   * @inheritdoc
   */
  public function buildEntitlements() {
    parent::buildEntitlements();

    $p = $this->getPartner();

    // If we don't have a valid partner, we can't make any partner-specific adjustments,
    // so we have to bail
    if(!$p->isLoaded()) {
      return;
    }

    foreach ($this->getEnts() as $e) {
      // Set partner ID
      $e->setPartnerId($p->getNid());

      // Set chapter/topic restrictions
      $this->entitlementSetRestrictions($e);
    }
  }

  /**
   * Set restricted flag and chapter/topic restrictions if configured by the partner
   * @param UserEntitlement $e
   */
  public function entitlementSetRestrictions(UserEntitlement $e) {
    // Get for topic restrictions and apply
    // NOTE: It shouldn't be possible from a business sense to restrict paid entitlements, but we don't check that.
    if ($restrictions = $this->getPartner()->getFlagRestrict()) {
      $e->setContentRestricted(TRUE);
      $e->setFlagRestrict($restrictions);
    }
  }

  /**
   * Make partner specific adjustments to the expiration based on partner settings, like email groups
   * or fixed expiration.
   * @param UserEntitlement $e
   */
  function entitlementSetDurationAndExpiration(UserEntitlement $e) {
    parent::entitlementSetDurationAndExpiration($e);

    // Do nothing if the partner is not valid and loaded.
    if(!$this->getPartner()->isLoaded()) {
      return;
    }

    // If UWorld configs are enabled, default partner course duration needs to go to 18 months because the default
    // will have changed to 12 months for b2c, but we need to respect 18 for b2b. This only applies to the online
    // course and not CRAM or audio.
    // See https://confluence.rogercpareview.com/display/UW/Configure+Product+Changes for more information
    if(variable_get('enable_uworld_configurations', FALSE)) {
      if ($e->isTypeOnlineCourse()) {
        $e->setCourseDuration(variable_get('partner_course_duration', 18));
        $e->setExpirationDate(strtotime('+' . $e->getCourseDuration() . ' ' . $e->getDefaultInterval(), REQUEST_TIME));
        $e->setOriginalExpirationDate($e->getExpirationDate());
      }
    }

    $user = $this->u->getUser();
    $p = $this->getPartner();

    // Entitlements granted from partners have special handling

    /*
     * 1. Defaults expirations are assumed for the course type if not overridden below.
     * 2. If email validation is on, expiration may be overridden by their email group
     * 3. Else, if there is a fixed date, expiration is set to that
     * 4. Finally, expiration is set based on duration/interval settings in the partner
     */

    // todo: it would be more performant to make this determination once rather than once per each entitlement
    // Expiration based on partner email group
    if ($p->useEmailValidation()) {
      $dates_colection = array();
      // Get all instances of email group association for this user's email to this partner
      $groups = rcpar_partners_import_select_groups_by_email($user->mail, $p->getNid());

      // Iterate through each group association and query for info about the group
      foreach ($groups as $group_id) {
        $group_result = _rcpar_partners_import_select_group(array(), $group_id);

        // If the group is configured to override the expiration date, store it in our array
        if ($group_result && $group_result->override_expiration && $group_result->end_date > REQUEST_TIME) {
          $dates_colection[$group_id] = $group_result->end_date;
        }
      }

      // In case the user is in multiple groups, we use the one that expires the soonest.
      if (!empty($dates_colection)) {
        sort($dates_colection);
        $e->setExpirationDate(reset($dates_colection))
          ->setOriginalExpirationDate(reset($dates_colection))
          ->setCourseDuration('')
          ->setDefaultInterval('');
        return;
      }
    }

    // There may be a fixed expiration, in which
    if ($date = $p->getEntitlementsFixedExpDate()) {
      $e->setExpirationDate($date)
        ->setOriginalExpirationDate($date)
        ->setCourseDuration('')
        ->setDefaultInterval('');
      return;
    }

    // Expiration is relative to the date of the purchase. Read from the partner
    if ($p->getCourseDuration() && $p->getInterval()) {
      // NOTE: Duration and interval are not visible to the admin, except for Normal Free Trial,
      // as it is not generally intended to be used anymore, however, many older partners still
      // have values here. Probably the values are set to 18 months by default, but reading these
      // values might not be the best idea.

      $e->setCourseDuration($p->getCourseDuration());
      // Interval is a dropdown field. 1 == days, 2 == months. @todo - this should probably
      // be in the partner class.
      $e->setDefaultInterval($p->getInterval() == 2 ? 'months' : 'days');

      // Set expiration based on duration/interval
      $e->setExpirationDate(strtotime('+' . $e->getCourseDuration() . ' ' . $e->getDefaultInterval(), REQUEST_TIME));
      $e->setOriginalExpirationDate($e->getExpirationDate());
      return;
    }

    // It's possible that the partner specified no expiration date whatsoever. In that case, we fall
    // back on the default values and execute the rest of the code below.
  }

  /**
   * Will disable IPQ for ACT partners, otherwise relies on the default.
   * @param UserEntitlement $e
   */
  function entitlementSetIpq(UserEntitlement $e) {
    parent::entitlementSetIpq($e);

    // Disable for ACT partners
    $p = $this->getPartner();
    if ($p->isLoaded()) {
      if ($p->isBillingFreeAccess() && $p->isPartnerTypeUniversity() && $p->isActPartner()) {
        // This is an ACT partner, in which case we will disable IPQ access for the entitlement.
        $e->setIpqAccess(FALSE);
      }
    }
  }

  /**
   * Set HHC access based on partner type.
   * @param UserEntitlement $e
   */
  protected function entitlementSetHHC(UserEntitlement $e) {
    parent::entitlementSetHHC($e);

    // We're only dealing with standard online course entitlements (AUD, BEC, FAR, and REG)
    if (!$e->isTypeOnlineCourse()) {
      return;
    }

    // Do nothing if the partner is not valid and loaded.
    if(!$this->getPartner()->isLoaded()) {
      return;
    }
    $partner_class = $this->getPartner();

    // If it's a paid partnership, they get write access (direct bill or firm/univ discount
    if (!$partner_class->isBillingFreeAccess()) {
      $e->setHhcAccess('write');
      return;
    }

    // FSL users
    if ($partner_class->isFSL()) {
      $e->setHhcAccess('read');
      return;
    }

    // Free trial users
    if ($partner_class->isFreeTrial()) {
      $e->setHhcAccess('read');
      return;
    }

    // Any other partner ends up here and gets none - ASL, ACT, or PAL.
    return;
  }


  /**
   * Get the partner associated with this order
   * @return RCPARPartner
   */
  public function getPartner(): RCPARPartner {
    return $this->p;
  }

  /**
   * Set the partner associated with this order
   * @param RCPARPartner $p
   */
  public function setPartner(RCPARPartner $p): void {
    $this->p = $p;
  }

}