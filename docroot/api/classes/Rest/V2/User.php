<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 7/16/2018
 * Time: 3:33 PM
 */

namespace RCPAR\Rest\V2;


use \Exception;
use RCPAR\Entities\UserEntitlement;
use RCPAR\Entities\UserEntitlementCollection;
use RCPAR\Rest\V1\Authenticate;
use RCPAR\Rest\V1\Token;
use RCPAR\User as ClassesUser;
use RCPAR\Utils\JSONResponse;

class User {

  /**
   * Route handler for /user/login
   */
  public static function login() {
    $json = JSONResponse::Instance();
    $auth = Authenticate::Instance();

    // If both name and password are empty, we will try to authenticate against the JWT token
    if (empty($_REQUEST['name']) && empty($_REQUEST['pass'])) {
      // Bad token - bail early
      if(!$auth->setIdentityFromRequest()->isAuthenticated()) {
        $json->setCode(401);
        $json->setError('Invalid token');
        $json->setMessage('Your session has expired, please login again.');
        return;
      }

      // Our token is valid - refresh token and sessionId if needed
      $u = $auth->getUser();
      $t = $auth->getToken();

      // Refresh our token
      $t->refreshToken($u->getLatestSessionId());
    }
    // We have a user and password, see if it's legit
    else {
      $u = new ClassesUser();
      $uid = $u->validateCredentials($_REQUEST['name'], $_REQUEST['pass']);
      $u = new ClassesUser($uid);
      $t = Token::buildNew($u->getUid(), $u->getDrupalSessionId());
      $auth->setIdentity($u, $t);
    }

    // Login succeeded
    if ($auth->isAuthenticated()) {
      // Add JWT token to the response payload
      $json->addToPayload('token', $t->getEncodedToken());
      $json->addToPayload('tokenExpiration', $t->getExpiration());

      // User profile stuff
      $json->addToPayload('profile', array(
        'first' => $u->getFirstName(),
        'last' => $u->getLastName(),
        'display' => $u->getDisplayName(),
        'mail' => $u->getEmail(),
        'uid' => $u->getUid()
      ));

      // Add entitlements to payload
      $json->addToPayload('entitlements',
        self::getEntitlementCollectionPayload(
          $u->getValidEntitlements()
        )
      );

      $json->setMessage('Authentication was successful');
    }
    // Login failure - bad credentials.
    // @todo - This error could also occur if the client hits the login endpoint with only a token,
    // and the token is expired - so this messaging is somewhat inaccurate. In theory the client
    // shouldn't ever do this because it already knows whether or not its token is expired.
    else {
      $json->setCode(401);
      $json->setError('Bad credentials');
      $json->setMessage('Sorry, unrecognized email or password.');
    }
  }

  public static function getEntitlementPayload(UserEntitlement $e) {
    return array(
      'id' => $e->getNid(),
      'sku' => $e->getProductSku(),
      'exam_version' => $e->getExamVersionName(),
      'content_restricted' => $e->getContentRestricted(),
      'restrictions' => $e->getAllowedTopicIds(),
      'expiration' => $e->getExpirationDate(),
      'section' => $e->getCourseSectionName(),
      'type' => $e->getCourseTypeName(),
      'activated' => !$e->getActivationDelayed(),
      'offline_access' => $e->getMobileOfflineAccess(),
      'bundled_product' => $e->getBundledProduct(),
    );
  }

  public static function getEntitlementCollectionPayload(UserEntitlementCollection $ec) {
    $payload = array();

    /** @var UserEntitlement $e */
    foreach ($ec->getEntitlements() as $e) {
      $payload[$e->getProductSku()] = self::getEntitlementPayload($e);
    }

    return $payload;
  }


  /**
   * Route handler for /user/password
   * @throws Exception
   */
  public static function password() {
    $json = JSONResponse::Instance();
    $auth = Authenticate::Instance();

    // Verify that we have an authenticated user
    if (!Authenticate::Instance()->isAuthenticated() && !Authenticate::Instance()->isAuthenticatedApiKey()) {
      $json->unauthorizedAndDie();
      return;
    }

    // API request parameters
    $authuser = $_REQUEST['authuser'];
    $authpass = $_REQUEST['authpass'];
    $chpassuser = $_REQUEST['chpassuser']; // Assumed to be an email address
    $newpass = $_REQUEST['newpass'];
    $newhash = $_REQUEST['newhash'];


    $allowed_to_change = FALSE;
    // Authentication via API key is like a super user, if we have it they can change stuff
    if(Authenticate::Instance()->isAuthenticatedApiKey()) {
      $allowed_to_change = TRUE;
    }
    // If we're authenticated and not specifying a different user to change passwords, that means we're changing
    // our own password and that's allowed
    elseif(empty($chpassuser)) {
      $allowed_to_change = TRUE;
    }
    // If the user that's authenticated is changing their own password, that's allowed
    elseif($auth->getUser()->getEmail() == $chpassuser) {
      $allowed_to_change = TRUE;
    }
    // If the user that's authenticated has admin permissions, they are allowed to change any password
    else {
      if(user_access('administer users', $auth->getUser()->getEntity())) {
        $allowed_to_change = TRUE;
      }
    }

    // If we don't have permission at this point, bail
    if(!$allowed_to_change) {
      $json->unauthorizedAndDie();
      return;
    }

    // We need the user id of the user who's password we're changing. If the user is specified overtly, query for it
    if(!empty($chpassuser)) {
      $user = user_load_by_mail($chpassuser);
      if(!$user) {
        throw new Exception('Could not load the user whose password you requested to change.');
      }
      $uid = $user->uid;
    }
    // Get it from the user we authenticated with
    else {
      $uid = $auth->getUser()->getUid();
      $user = $auth->getUser()->getEntity();
    }

    if (!$user) {
      throw new Exception('Could not load the user whose password you are trying to change.');
    }

    // Set a new password from plain text
    if(!empty($newpass)) {
      // Generate new password hash.
      include_once(DRUPAL_ROOT . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'password.inc');
      $password_hash = user_hash_password($newpass);
      if (!$password_hash) {
        // Password could not be hashed. Handle the error.
        throw new Exception('Could not hash the supplied password');
      }

      $user->pass = $password_hash;
      user_save($user);
    }
    // Set a password hash directly in the database
    elseif (!empty($newhash)) {
      db_query("UPDATE users SET pass = :hash WHERE uid = :uid", [':hash' => $newhash, ':uid' => $uid]);
    }
    else {
      throw new Exception('You must specify a new password');
    }

    // If we made it here without throwing an exception, we should have been successful.
    $json->setMessage('Password was updated successfully');

  }
}