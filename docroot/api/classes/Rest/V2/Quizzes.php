<?php

namespace RCPAR\Rest\V2;

use Exception;
use RCPAR\Utils\JSONResponse;
use RCPAR\Rest\V1\Authenticate;
use RCPAR\UW\UserTeleportation;

class Quizzes {
  /**
   * Get IPQ quiz sessions id.
   * @throws Exception
   */
  public static function getUserQuizzes() {

    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    $user = Authenticate::Instance()->getUser();

    // Verify we have an appropriate 'part' parameter
    $part = isset($_GET['part']) ? $_GET['part'] : NULL;
    if (!$part) {
      throw new Exception('Exam part not supplied');
    }

    // We need to ensure we only pass the data for the eligible entitlements
    $ut = UserTeleportation::instance($user->getUid());
    $ents = $ut->getEntitlementsEligibleForTeleport()->filterBySectionName($part);

    // If not entitlements than we're done
    if ($ents->isEmpty()) {
      throw new Exception('No eligible entitlements for ' . $part);
    }

    // Set quiz requested
    $ut->setQuizzesRequested();

    // Get the entitlement id
    $entitlement_ids = $ents->getIds();

    $ipq_session_ids = array();

    $result = db_query("select id from {ipq_saved_sessions} WHERE uid = :uid AND entitlement_id IN (:entitlement_ids)",
      array(":uid" => $user->getUid(), ":entitlement_ids" => $entitlement_ids))->fetchAll();
    foreach ($result as $key => $value) {
      $ipq_session_ids[] = $value->id;
    }
    $json->setCode(200);
    $json->addToPayload('session_ids', $ipq_session_ids);
    $json->setMessage("Found " . sizeof($ipq_session_ids) . " quiz session ids.");
  }

  /**
   * Get IPQ quiz data.
   * @throws Exception
   */
  public static function getUserQuizData() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    // Get the user
    $user = Authenticate::Instance()->getUser();

    // Set the user teleportation status
    UserTeleportation::instance($user->getUid())->setQuizDataRequested();

    // Do we have quiz session ids?
    $session_ids = isset($_GET["session_ids"]) ? $_GET["session_ids"] : NULL;
    if (empty($session_ids)) {
      throw new Exception('Parameter "session_ids" is required.');
    }

    // Query for all requested saved sessions
    $query_args = [":session_ids" => $session_ids];
    $sessions = db_query("
        SELECT id, name, session_type, created_on, updated_on, finished, session_config from ipq_saved_sessions s
        WHERE s.id IN (:session_ids)", $query_args
    )->fetchAll();

    // Get all session data for the requested sessions
    $session_data = db_query("
      SELECT ipq_question_id, ipq_question_type_id, is_correct, user_input, score, max_score, time, 
      is_flagged, status, ipq_saved_sessions_id as id 
      FROM ipq_saved_session_data WHERE ipq_saved_sessions_id IN (:session_ids)", $query_args
    )->fetchAll();

    // Get all of the saved session data IDs. We could have done this more efficiently above, but this is simpler for various reasons.
    $ssdids = db_query("
        SELECT id FROM ipq_saved_session_data WHERE ipq_saved_sessions_id IN (:session_ids)", $query_args
    )->fetchCol();

    // Get all notes for the requested sessions
    $notes = db_query("
      SELECT n.ipq_question_id, n.uid, n.note, n.ipq_saved_session_data_id, s.ipq_saved_sessions_id
      FROM ipq_notes n 
      INNER JOIN ipq_saved_session_data s ON n.ipq_saved_session_data_id = s.id 
      WHERE s.id IN (:ssdids)", array(':ssdids' => $ssdids))->fetchAll();

    // Iterate over each session and build our payload one quiz at a time.
    $payload = [];
    foreach ($sessions as $session) {
      $session_data_rows = self::getSessionDataForId($session_data, $session->id);
      $session_notes = self::getNotesDataForId($notes, $session->id);
      $p = new QuizPayload($session, $session_data_rows, $session_notes);
      $payload[] = $p->getPayloadRow();
    }

    // Add to payload
    $json->setCode(200);
    $json->addToPayload('quizzes', $payload);
    $json->setMessage("Found data for " . sizeof($payload) . " quiz sessions.");
  }

  /**
   * @param array $session_data
   * @param int $id
   * - IPQ Saved Session ID
   * @return array
   */
  public static function getSessionDataForId($session_data, $id) {
    $return = [];
    foreach ($session_data as $session_datum) {
      if ($session_datum->id == $id) {
        $return[] = $session_datum;
      }
    }
    return $return;
  }

  /**
   * @param array $notes
   * @param int $id
   * - IPQ Saved Session ID
   * @return array
   */
  public static function getNotesDataForId($notes, $id) {
    $return = [];
    foreach ($notes as $note) {
      if ($note->ipq_saved_sessions_id == $id) {
        $return[] = $note;
      }
    }
    return $return;
  }
}


/**
 * Class QuizPayload
 * @package RCPAR\Rest\V2
 */

class QuizPayload {
  private $session;
  private $sessionData;
  private $sessionConfig;

  /**
   * QuizPayload constructor.
   * @param array $session_row
   * @param array $session_data_rows
   * @param array $session_notes
   */
  public function __construct($session_row, $session_data_rows, $session_notes) {
    // We can start by copying the top level of the session row to our payload, minus the session config
    $this->session = $session_row;

    $this->sessionData = $session_data_rows;

    // Expand values in session config that we need to be on the top level
    $this->expandSessionConfig($session_row->session_config);

    // Add quizlet IDs to each session data row
    $this->populateQuizletIds();

    // Add question sequence number to each session data row
    $this->populateQuestionSequence();

    // Populate session data with concatenated notes
    $this->populateNotes($session_notes);

    // Make sure any field that should be defined as a boolean is done so.
    $this->makeConsistentBooleans();
  }

  private function expandSessionConfig($session_config) {
    $session_config = json_decode($session_config, TRUE);
    $this->sessionConfig = $session_config;
    unset($this->session->session_config);

    $fields = array(
      "exam_version" => "exam_version",
      "explain_answers" => "explain_answers",
      "view_solutions" => "view_solutions",
      "only_questions_unseen" => 'only_questions_unseen',
      "only_questions_incorrect" => 'only_questions_incorrect',
      "only_questions_skipped" => 'only_questions_skipped',
    );

    foreach ($fields as $sourcekey => $destKey) {
      $this->session->$destKey = $session_config[$sourcekey];
    }
  }

  /**
   * Add quizlet IDs to each session data row.
   */
  private function populateQuizletIds() {
    foreach ($this->sessionData as &$sessionDatum) {
      $sessionDatum->quizlet_id = $this->getQuizletIdFromQuestion($sessionDatum->ipq_question_id);
    }
  }

  /**
   * Given a question ID, determine what quizlet_id it should have.
   * @param $question_id
   * @return int
   */
  private function getQuizletIdFromQuestion($question_id) {
    // Build a simple map of quizlet IDs
    $quizlet_ids = array_flip(array_keys($this->sessionConfig['question_list']));

    foreach ($this->sessionConfig['question_list'] as $quizlet_name => $quizlet_questions) {
      if (in_array($question_id, $quizlet_questions)) {
        return $quizlet_ids[$quizlet_name];
      }
    }

    return 0;
  }

  /**
   * Add question sequence number to each session data row.
   */
  private function populateQuestionSequence() {
    foreach ($this->sessionData as &$sessionDatum) {
      $sessionDatum->sequence = $this->getQuestionSequenceFromQuestion($sessionDatum->ipq_question_id);
    }
  }

  /**
   * Get question sequence number in quiz.
   */
  private function getQuestionSequenceFromQuestion($question_id) {
    foreach ($this->sessionConfig["question_list"] as $quizlet => $question) {
      if (in_array($question_id,$question)) {
        return array_search($question_id, $question) + 1;
      }
    }
    return 0;
  }

  /**
   * Add note data to each session data row.
   * @param array $session_notes
   */
  private function populateNotes($session_notes) {
    foreach ($this->sessionData as &$sessionDatum) {
      $sessionDatum->notes = $this->getNotesForQuestion($sessionDatum->ipq_question_id, $session_notes);
    }
  }

  /**
   * Get notes for a specific question from the given pool of note data.
   * @param int $question_id
   * @param array $session_notes
   * @return string
   */
  private function getNotesForQuestion($question_id, $session_notes) {
    $note_text = '';
    foreach ($session_notes as $session_note) {
      if ($session_note->ipq_question_id == $question_id) {
        // UWorld requests notes be concatenated with \r\n
        $note_text .= $session_note->note . "\r\n";
      }
    }

    return $note_text;
  }

  /**
   * Ensure that a defined list o fields always returns a boolean output, since data types in the database may be
   * inconsistent.
   */
  private function makeConsistentBooleans() {
    // These are the only boolean fields this api is concerned with
    $boolean_fields_to_check = array(
      "finished",
      "explain_answers",
      "view_solutions",
      "only_questions_unseen",
      "only_questions_incorrect",
      "only_questions_skipped",
      "finished",
      "is_correct",
      "is_flagged",
    );

    // Fix the top level session booleans
    foreach ($this->session as $key => &$value) {
      if (in_array($key, $boolean_fields_to_check)) {
        $value = (bool) $value;
      }
    }

    // Fix the session data booleans
    foreach ($this->sessionData as $i => &$row) {
      foreach ($row as $key => &$value) {
        if (in_array($key, $boolean_fields_to_check)) {
          $value = (bool) $value;
        }
      }
    }
  }

  /**
   * Returns a finished row for the quiz data payload.
   * @return array
   */
  public function getPayloadRow() {
    $data = $this->session;
    $data->session_data = $this->sessionData;
    return $data;
  }

}
