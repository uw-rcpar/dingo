<?php

namespace RCPAR\Rest\V2;

use Exception;
use RCPAR\Rest\V1\Authenticate;
use RCPAR\Utils\JSONResponse;
use RCPAR\UW\UserTeleportation;
use RCPARCourseTree;

class Video {
  /**
   * Route handler for GET /api/v2/video/sync
   * @throws Exception
   */
  public static function syncGet() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    $u = Authenticate::Instance()->getUser();

    // Indicate to the teleporation manager that video progress has been requested
    $ut = new UserTeleportation($u->getUid());
    $ut->setCourseRequested();

    // Start with valid standard and CRAM course entitlements
    $ents = $u->getValidEntitlements()->filterByOnlineAndCramCourse();

    // Determine the entitlement IDs we're filtering on.
    // We expect a 'part' to be passed in
    $part = NULL;
    if (!empty($_GET['part'])) {
      $part = $_GET['part'];
      $ents = $ents->filterBySectionName($part);
    }

    // Get the IDs
    $entIds = $ents->getIds();
    $cramEntIds = $ents->filterByCramCourse()->getIds();

    // If we have no entitlement IDs, we can't query.
    if (empty($entIds)) {
      throw new Exception('No entitlements available for the requested section(s).');
    }

    // Build the base query
    $replacements = array(
      ':uid'    => $u->getUid(),
      ':entids' => $entIds, // Only active entitlements
      ':year'   => exam_version_get_tid_from_year_name('2019'),
    );
    $query = "SELECT topic_reference as topic_id, title, rcpa_video as video_id, chapter_reference, last_position, changed, created, entitlement_product as entid 
    FROM eck_video_history vh INNER JOIN course_stats cs ON vh.topic_reference = cs.nid WHERE vh.uid = :uid AND vh.entitlement_product IN (:entids) AND cs.year = :year";

    // Add the "after" parameter, if specified.
    $after = !empty($_REQUEST['after']) ? $_REQUEST['after'] : NULL;
    if (!empty($after)) {
      $replacements[':after'] = $after;
      $query .= "  AND changed > :after";
    }

    // Get results
    $result = db_query($query, $replacements)->fetchAll();

    // We need to load video notes for each row and concatentate those notes together if there is more than one per topic
    $payload = array();
    foreach ($result as $row) {
      // handle cram entitlements
      $cram = false;
      $cram_url = '';
      if (in_array($row->entid, $cramEntIds)) {
        $cram = true;
        $cram_url = 'Cram-';
      }
      $payload_row = $row;
      $row->notes = '';
      // Query for notes on this topic for the given entitlement
      $notes = db_query("
        SELECT body.body_value AS note_text
        FROM node n
        LEFT JOIN field_data_field_topic_reference topicref ON topicref.entity_id = n.nid
        LEFT JOIN field_data_body body ON body.entity_id = n.nid
        INNER JOIN field_data_field_entitlement_product ep ON ep.entity_id = n.nid
        WHERE n.uid = :uid AND n.type = 'topic_notes' AND topicref.field_topic_reference_target_id = :tid 
        AND ep.field_entitlement_product_target_id = :entid
        ORDER BY n.nid DESC",
        [':uid' => $u->getUid(), ':tid' => $row->topic_id, ':entid' => $row->entid]);

      // Concatenate all notes for this topic into the return data.
      foreach ($notes as $note_record) {
        $row->notes .= $note_record->note_text;
      }

      // Add topic slug and video file to payload.
      $ct = new RCPARCourseTree();
      $section_tid = $ct->getSectionIdFromName($part); // course section name

      $topic_id = $row->topic_id; // topic node id
      $chapter_id = $ct->getChapterIdFromTopic($section_tid, $topic_id, $cram); // chapter node id

      $topic_slug = RCPARCourseTree::getTopicSlug($section_tid, $topic_id, exam_version_get_tid_from_year_name('2019'));
      $chapter_weight = RCPARCourseTree::getChapterWeightFromId($chapter_id, exam_version_get_tid_from_year_name('2019'));
      $payload_row->slug = $chapter_weight . '.' . $topic_slug;
      $video_file = $part . '-';
      if ($cram) {
        $video_file .= $cram_url;
      }
      $video_file .= $chapter_weight . '.' . $topic_slug . '.mp4';
      $payload_row->video_file = $video_file;

      $payload[] = $payload_row;
    }

    // Get an array of position timestamps keyed by topic id
    $json->addToPayload('videos', $payload);
    $json->setMessage('Found ' . count($payload) . ' videos records. Results keyed by topic ID, last_position is video progress value in milliseconds.');
  }
}