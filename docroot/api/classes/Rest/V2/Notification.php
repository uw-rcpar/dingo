<?php

namespace RCPAR\Rest\V2;

use RCPAR\Entities\Notifications\Channel;
use RCPAR\Entities\Notifications\Notification as NotificationObj;
use RCPAR\Entities\Notifications\NotificationCollection;
use RCPAR\NotificationManager;
use RCPAR\Rest\V1\Authenticate;
use RCPAR\Utils\JSONResponse;

class Notification {
  /**
   * Get all notifications regardless of "read" status on the desktop channel.
   * @throws \Exception
   */
  public static function getAll() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    $user = Authenticate::Instance()->getUser();

    // Create notification manager - make sure we issue notifications from broadcasts by retrieving
    // before we get all

    // The default channel is 'desktop'
    $channel = Channel::getChannelByName('rogercpareview_com');

    $m = new NotificationManager($user, $channel);
    $m->issueNotifications();
    $limit = !empty($_GET['limit']) ? $_GET['limit'] : 19;
    $notifications = $m->getAll($limit);
    $no_of_notifications = $notifications->count();

    // Build JSON response and payload
    $json->setCode(200);
    $json->setMessage('Found ' . $no_of_notifications . ' notification(s).');
    $json->addToPayload('notifications', self::getNotificationsPayload($notifications));
  }

  /**
   * Mark a single notification as read
   * Params: $_POST['id']
   * - A notification ID.
   * @throws \Exception
   */
  public static function setRead() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    $user = Authenticate::Instance()->getUser();

    if(!empty($_POST['id'])) {
      $id = $_POST['id'];

      // Create notification manager - make sure we issue notifications from broadcasts by retrieving
      // before we get all
      $n = new NotificationObj($id);

      if($n->getUid() != $user->getUid()) {
        throw new \Exception("User does not have access to notification ID: $id");
      }

      $n->markRead();
      $n->save();

      // Build JSON response and payload
      $json->setCode(200);
      $json->setMessage("Marked notification: $id as read successfully.");
    }
    else {
      throw new \Exception("Missing notification ID");
    }
  }

  /**
   * Get an array to be used for a JSON payload of a collection of Notification objects
   * @param NotificationCollection $notifications
   * @return array
   */
  public static function getNotificationsPayload($notifications) {
    $data = array();
    foreach ($notifications as $notification) {
      $data[] = self::getNotificationPayload($notification);
    }
    return $data;
  }

  /**
   * Get an array to be used for a JSON payload of the notification object
   * @param NotificationObj[] $n
   * @return array
   */
  public static function getNotificationPayload(NotificationObj $n) {
    return array(
      'id' => $n->getId(),
      'notification_template_id' => $n->getNotificationTemplateId(),
      'broadcast_channel_id' => $n->getBroadcastChannelId(),
      'type' => $n->getType(),
      'title' => $n->getTitle(),
      'message' => $n->getMessage(),
      'teaser' => $n->getTeaser(),
      'expire' => $n->getExpire(),
      'uid' => $n->getUid(),
      'is_read' => $n->getIsRead(),
      // We'll use the modified date of the notification template as the created date to show on the front-end
      'created' => $n->getNotificationTemplate()->getChangedTime(),
      'created_formatted' => date("F d", $n->getNotificationTemplate()->getChangedTime()),
      'issued' => $n->getCreated(),
      'read_on' => $n->getReadOn()
    );
  }
}