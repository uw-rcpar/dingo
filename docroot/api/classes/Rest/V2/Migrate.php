<?php

namespace RCPAR\Rest\V2;
use Exception;
use RCPAR\Utils\JSONResponse;
use RCPAR\Rest\V1\Authenticate;
use RCPAR\UW\UserTeleportation;

class Migrate {

  /**
   * Set user teleportation/migration as complete.
   * @throws Exception
   */
  public static function setMigrationComplete() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    // Get the user
    $user = Authenticate::Instance()->getUser();

    // Get migration status
    $status = isset($_GET["status"]) ? $_GET["status"] : NULL;
    // Get error message
    $message = isset($_GET["message"]) ? $_GET["message"] : NULL;

    // Set the user teleportation status
    $ut = new UserTeleportation($user->getUid());
    $ut->setAsyncComplete();

    if ($status == 'success') {
      $ut->setUserMigrated();
      // Disable the user's Drupal entitlements
      $ut->disableEntitlements();
      // Set the batch queue to complete
      $ut->setUserMigrateBatchRecordComplete();
      // Email user
      $emailto = $user->getEmail();
      $emailfrom = variable_get('site_mail', '');
      $link = rcpar_saml_add_link('', NULL , ['token' => rcpar_mods_get_jwt_token(), 'migrate' => 1]);
      $body = "Hi {$user->getFirstName()}, \n\n";
      $body .= "We’re happy to inform you that you’ve been successfully upgraded to the UWorld Roger CPA Review course platform!\n\n";
      $body .= "To help you navigate the new platform and find all of your favorite course features and tools, we recommend you checkout the ";
      $body .= "<b>Into to Your Course</b> video located in the top right corner of your course.\n\n";
      $body .= "We also encourage you to download the new mobile app as a companion to your online program. Find links to the Apple, Android, and Amazon mobile apps  <a  href=\"https://www.rogercpareview.com/cpa-courses\" target=\"_blank\">here.</a>.\n\n";
      $body .= "Happy Studying!\n\n";
      $body .= "<a href=\"{$link}\">Take me to the new platform</a>\n\n";
      $body .= "Sincerely,\n\n";
      $body .= "The UWorld Roger CPA Review Team";

      drupal_mail('system', 'uworld_teleportation', $emailto, language_default(), array('context' => [
        'message' => $body,
        'subject' => "Upgrade complete! Get the most out of your new course platform.",
      ]), $emailfrom);
    }
    else {
      $ut->setAsyncErrorMessage($message);
      // Notify an admin of the problem via email
      $emailtodefault = 'jdodge@rogercpareview.com,patkins@rogercpareview.com,staylor@rogercpareview.com,rvemulapalli@uworld.com,vbhutada@uworld.com';
      $emailto = variable_get('teleportation_error_emails', $emailtodefault);
      $emailfrom = variable_get('site_mail', '');
      $body = $message;
      drupal_mail('system', 'uworld_teleportation', $emailto, language_default(), array(
        'context' => [
          'message' => $body,
          'subject' => "UWorld Teleportation Errors",
        ]
      ), $emailfrom);

      // Notify user of the problem via email
      if(variable_get('teleport_email_user_on_error', TRUE)) {
        $emailto = $user->getEmail();
        $emailfrom = variable_get('site_mail', '');
        $body = "Hi {$user->getFirstName()} \n\n";
        $body .= "We wanted to let you know we've encountered a minor error while attempting to upgrade—but not to worry! We're working on resolving the problem and will be back in touch soon with more details.\n\n";
        $body .= "In the meantime, you can continue using your course as usual. Be on the lookout for an email from us with further instructions in the next 1 - 2 business days—quite possibly sooner. You’ll be up and studying with the new platform in no time!\n\n";
        $body .= "Our best,\n\n";
        $body .= "The Roger CPA Review Team";
        drupal_mail('system', 'uworld_teleportation', $emailto, language_default(), array(
          'context' => [
            'message' => $body,
            'subject' => "Slight delay to your course upgrade",
          ]
        ), $emailfrom);
      }

    }

    // Add to payload
    $json->setCode(200);
    $json->addToPayload('status', $status);
    $json->setMessage("Registered status: [$status] for user: {$user->getMail()}");
  }

  /**
   * Get status of user teleportation migration complete.
   */
  public static function getMigrationCompleteStatus() {

    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    // Get the user
    $user = Authenticate::Instance()->getUser();

    $ut = new UserTeleportation($user->getUid());

    // Set the status of migration complete to true or false and get error if there is one.
    $status = $ut->getAsyncComplete() == NULL ? FALSE : TRUE;
    $error = $ut->getAsyncErrorMessage() == NULL ? FALSE : $ut->getAsyncErrorMessage();

    // Generate message
    $message = $status == TRUE ? 'The user\'s migration is complete.' : 'The user\'s migration is not complete.';

    // Add to payload
    $json->setCode(200);
    $json->addToPayload('status', $status);
    $json->setError($error);
    $json->setMessage($message);
  }
}