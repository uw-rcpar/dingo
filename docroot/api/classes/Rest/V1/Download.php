<?php

namespace RCPAR\Rest\V1;


use Exception;
use RCPAR\User;
use RCPAR\Utils\JSONResponse;

class Download {

  /**
   * Route handler for POST /download/sync
   *
   * @throws Exception
   */
  public static function syncPost() {
    $json = JSONResponse::Instance();

    /************************
     * $payloadExample = [
     *  'downloads' => [
     *    0 => [
     *      'nid'        => 5673467,
     *      'entitlement_id'   => 634456,
     *      'download_status' => 1,
     *      'section' => 'AUD',
     *      'topic' => '0.01',
     *      'file_type' => 'review_video',
     *      'file_name' => 'AUD_0.01.mp4',
     *      'created_on' => TIMESTAMP,
     *      'modified_on' => null,
     *    ],
     *    1 => [],
     *    2 => [],
     *    // etc
     *   ],
     * ];
     *************************/

    $payload = $_POST;

    if ($payload) {
      // Authenticate the user
      if(!Authenticate::Instance()->isAuthenticated()) {
        $json->unauthorizedAndDie();
        return;
      }
      $u = Authenticate::Instance()->getUser();

      // Update each download, tally successes and failures
      $successes = [];
      $fails = [];

      foreach ($payload['downloads'] as $dl) {
        $ret = self::createOrUpdate($dl, $u);
        if ($ret) {
          $successes[] = $dl['nid'];
        }
        else {
          $fails[] = $dl['nid'];
        }
      }

      // Set JSON response
      $json->addToPayload('downloadsUpdated', $successes);
      $json->addToPayload('downloadsUpdateFailed', $fails);
      $json->setMessage('Completed download history update. ' . count($successes) . ' were updated successfully. ' . count($fails) . ' failed to update.');
    }
    else {
      throw new Exception('Request payload malformed');
    }
  }

  /**
   * Create or update a download history record for the given user based on a JSON payload
   * @param array $dl
   * - Decoded JSON payload
   * @param User $u
   * - The user to create the record for
   * @return \InsertQuery|\UpdateQuery
   * @throws Exception
   */
  public static function createOrUpdate($dl, User $u) {
    // Determine if we already have an entry for this user.
    $match = db_query("SELECT nid FROM {mb_download_history} WHERE uid = :uid AND nid = :nid AND entitlement_id = :eid", [
      ':uid' => $u->getUid(),
      ':nid' => $dl['nid'],
      ':eid' => $dl['entitlement_id'],
    ])->fetchField();

    // If there is not match in the db, insert a new record.
    if ($match === NULL || $match === FALSE) {
      $fields = array(
        'uid' => $u->getUid(),
        'nid' => $dl['nid'],
        'entitlement_id' => $dl['entitlement_id'],
        'download_status' => $dl['download_status'],
        'section' => $dl['section'],
        'topic' => $dl['topic'],
        'file_type' => $dl['file_type'],
        'file_name' => $dl['file_name'],
        'created_on' => $dl['created_on'],
        'modified_on' => $dl['modified_on'],
      );
      $resp = db_insert('mb_download_history')->fields($fields)->execute();
    }
    // If there is a match, update the record.
    else {
      $fields = array(
        'download_status' => $dl['download_status'],
        'modified_on' => REQUEST_TIME,
      );
      $resp = db_update('mb_download_history')
        ->fields($fields)
        ->condition('uid', $u->getUid())
        ->condition('nid', $dl['nid'])
        ->execute();
    }
    // Success
    if (isset($resp)) {
      return $resp;
    }
    // Error
    else {
      throw new \Exception($resp['message']);
    }
  }
}