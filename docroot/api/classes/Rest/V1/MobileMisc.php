<?php

namespace RCPAR\Rest\V1;

class MobileMisc {

  /**
   * Return information about content interrupt pages used by the mobile application
   */
  public static function getInterrupts() {
    global $base_url;

    // Build $base_root (everything until first slash after "scheme://").
    // This is necessary because the base URL will otherwise end up with '/api' at the end
    $parts = parse_url($base_url);
    $base_root = substr($base_url, 0, strlen($base_url) - strlen($parts['path']));

    // Pattern template for URL parameters
    $params = '?section=<SECTION>&uid=<UID>';

    return array(
      'review' => "$base_root/mb/review-course$params",
      'cram' => "$base_root/mb/cram-course$params",
      'cram-activate' => "$base_root/mb/cram-course-activate$params",
      'download' => "$base_root/mb/downloads$params",
      'restricted' => "$base_root/mb/restricted$params",
      'flashcards' => "$base_root/mb/flashcards$params",
      'audio' => "$base_root/mb/audio$params"
    );
  }

}
