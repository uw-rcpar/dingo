<?php

namespace RCPAR\Rest\V1;

use EntityFieldQuery;
use Exception;
use RCPAR\WrappersDelight\CommerceOrderWrapper;
use RCPAR\Utils\JSONResponse;
use RCPAR\WrappersDelight\WdCommerceLineItemWrapper;

class Order {

  public static function getShipping() {
    $json = JSONResponse::Instance();

    // Authenticate via API key.
    if (!Authenticate::Instance()->isAuthenticatedApiKey()) {
      $json->unauthorizedAndDie();
      return;
    }

    // Expecting UNIX timestamps, but we'll attempt strtotime() for developer convenience
    $from = isset($_GET['from']) ? $_GET['from'] : NULL;
    $to = isset($_GET['to']) ? $_GET['to'] : NULL;

    // Verify we have an appropriate 'from' parameter
    if(!is_numeric($from)) {
      $from = strtotime($from);
      if (!$from) {
        throw new Exception('Non-numeric date encountered, but could not parse "from" param with strtotime()');
      }
    }

    // Verify we have an appropriate 'to' parameter
    if(!is_numeric($to)) {
      $to = strtotime($to);
      if (!$to) {
        throw new Exception('Non-numeric date encountered, but could not parse "to" param with strtotime()');
      }
    }


    // Verify we neither param is empty after conversion.
    if (empty($from) || empty($to)) {
      throw new Exception('One or more required parameters are missing. Please provide "from" and "to" dates.');
    }

    // Initialize our response data
    $response = array();

    // Database query - get UWorld-only orders changed between the given range.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_order')
          ->entityCondition('bundle', 'commerce_order')
          ->fieldCondition('field_uw_order_id', 'value', '', '!=')
          ->propertyCondition('changed', array($from, $to), 'BETWEEN')
          ->addMetaData('account', user_load(1)); // Run the query as user 1.
    $result = $query->execute();

    // Build response data if we have any
    if (!(empty($result)) && isset($result['commerce_order'])) {
      $ids = array_keys($result['commerce_order']);

      // If we found a matching order...
      if (!empty($ids)) {
        foreach ($ids as $order_id) {
          $o = new CommerceOrderWrapper($order_id);
          $response[] = [
            'rcpar_order_id' => $order_id,
            'uw_payment_id' => $o->getUwOrderId(),
            'status' => $o->getStatus(),
            'updated' => $o->getChangedDate(),
            'tracking' => CommerceOrderWrapper::getUpsTrackingId($order_id)
          ];
        }
      }
    }

    // Build JSON response and payload
    $date_format = 'M jS, Y g:ia';
    $json->setCode(200);
    $json->addToPayload('orders', $response);
    $json->setMessage("Found " . sizeof($response) . " orders, from: " . date($date_format, $from) . " to " . date($date_format, $to));
  }

  /**
   * Get order summary information for a given user.
   * @throws Exception
   */
  public static function getUserOrders() {

    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    $user = Authenticate::Instance()->getUser();

    // Initialize our response data
    $response = array();

    // Get all the user's orders
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_order')
      ->propertyCondition('uid', $user->getUid(), '=')
      ->addMetaData('account', user_load(1)); // Run the query as user 1.

    $results = $query->execute();

    // Build response data if we have any
    if (!(empty($results)) && isset($results['commerce_order'])) {
      $ids = array_keys($results['commerce_order']);

      // If we have orders
      if (!empty($ids)) {
        foreach ($ids as $order_id) {
          $o = new CommerceOrderWrapper($order_id);
          $line_items = $o->getCommerceLineItems();

          $response[$order_id] = [
            'rcpar_order_id' => $order_id,
            'uw_order_id' => $o->getUwOrderId(),
            'status' => $o->getStatus(),
            'order_date' => $o->getChangedDate(),
            'order_total' => commerce_currency_format($o->getCommerceOrderTotal()['amount'], $o->getCommerceOrderTotal()['currency_code']),
            'shipping_address' => $o->getCustomerShippingAddress(),
          ];

          // Line items including tax and shipping
          foreach ($line_items as $line_item) {
            $item = new WdCommerceLineItemWrapper($line_item);
            $type = $line_item->type;
            // For product get the readable label
            $product = ($type == 'product') ? $item->getProduct()->getLabel() : $line_item->line_item_label;
            $display_price = commerce_currency_format($item->getUnitPrice()['amount'], $item->getUnitPrice()['currency_code']);
            $qty = $item->getQuantity();
            $display_total = commerce_currency_format($item->getTotal()['amount'], $item->getTotal()['currency_code']);
            $response[$order_id]['line_items'][] = [
              'product' => $product,
              'price' => $display_price,
              'quantity' => $qty,
              'sub_total' => $display_total,
            ];
          }

        }
      }
    }

    // Build JSON response and payload
    $date_format = 'M jS, Y g:ia';
    $json->setCode(200);
    $json->addToPayload('orders', $response);
    $json->setMessage("Found " . sizeof($response) . " orders.");

  }
}


