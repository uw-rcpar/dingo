<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 7/16/2018
 * Time: 3:33 PM
 */

namespace RCPAR\Rest\V1;


use RCPAR\Entities\UserEntitlement;
use RCPAR\Entities\UserEntitlementCollection;
use RCPAR\User as ClassesUser;
use RCPAR\Utils\JSONResponse;

class User {

  /**
   * Route handler for /user/login
   */
  public static function login() {
    $json = JSONResponse::Instance();
    $auth = Authenticate::Instance();
    
    // If both name and password are empty, we will try to authenticate against the JWT token
    if (empty($_REQUEST['name']) && empty($_REQUEST['pass'])) {
      // Bad token - bail early
      if(!$auth->setIdentityFromRequest()->isAuthenticated()) {
        $json->setCode(401);
        $json->setError('Invalid token');
        $json->setMessage('Your session has expired, please login again.');
        return;
      }

      // Our token is valid - refresh token and sessionId if needed
      $u = $auth->getUser();
      $t = $auth->getToken();
      
      // Generate a new Drupal sessionId *if needed*. We'll only do this if there is no longer
      // a sessionId in Drupal that matches this token
      if (!$u->sessionIdExists($t->getDrupalSessionId())) {
        // Drupal no longer has a session matching the session from the token.
        // We need to regenerate a new one.
        ClassesUser::drupalLogin($u->getUid());
        $u->setDrupalSessionId($u->getLatestSessionId());
        $t->setDrupalSessionId($u->getDrupalSessionId());
      }

      // Refresh our token
      $t->refreshToken($u->getDrupalSessionId());
    }
    // We have a user and password, see if it's legit
    else {
      $u = new ClassesUser();
      $u->authenticateCredentials($_REQUEST['name'], $_REQUEST['pass']);
      $t = Token::buildNew($u->getUid(), $u->getDrupalSessionId());
      $auth->setIdentity($u, $t);
    }

    // Login succeeded
    if ($auth->isAuthenticated()) {
      // Temporary: check if the user should be denied because of no entitlements or free trial
      if(self::tempDenialCheck($u)) {
        // Wipe the identity
        $auth->setIdentity(new ClassesUser(), new Token());
        $message = self::uWorldCheck($u) ? 'UWorld offers multiple apps targeting specific products (USMLE, BOARDS, NCLEX). Please make sure you are using the correct app to access the desired resource' : 'Access to the full online course is required to use this app.';
        // Set error payload and bail
        $json->setCode(401);
        $json->setError('Sorry!');
        $json->setMessage($message);
        return;
      }

      // Add JWT token to the response payload
      $json->addToPayload('token', $t->getEncodedToken());
      $json->addToPayload('tokenExpiration', $t->getExpiration());

      // User profile stuff
      $json->addToPayload('profile', array(
        'first' => $u->getFirstName(),
        'last' => $u->getLastName(),
        'display' => $u->getDisplayName(),
        'mail' => $u->getEmail(),
        'uid' => $u->getUid()
      ));

      // Mobile Interface Access Interrupt URLs
      // @todo - it's weird to include this here, but convenient. Make new endpoint or option to merge payloads
      $json->addToPayload('interrupts', MobileMisc::getInterrupts());

      // Add entitlements to payload
      $json->addToPayload('entitlements',
        self::getEntitlementCollectionPayload(
          $u->getValidEntitlements()
            ->removeACTFromCollection()
        )
      );

      $json->setMessage('Authentication was successful');
    }
    // Login failure - bad credentials.
    // @todo - This error could also occur if the client hits the login endpoint with only a token,
    // and the token is expired - so this messaging is somewhat inaccurate. In theory the client
    // shouldn't ever do this because it already knows whether or not its token is expired.
    else {
      $json->setCode(401);
      $json->setError('Bad credentials');
      $json->setMessage('Sorry, unrecognized email or password.');
    }
  }

  /**
   * @todo - Temporary: deny access to free trial users and those with no entitlements
   * @param ClassesUser $u
   * @return bool
   */
  public static function tempDenialCheck(ClassesUser $u) {
    // No entitlements at all? Denial
    if($u->getValidEntitlements()->isEmpty()) {
      return TRUE;
    }

    // If we made it here, the user will not be dnied
    return FALSE;
  }
  
  /**
   *  Check if the user has any entitlements that originate from UWorld, active or otherwise.
   * @param ClassesUser $u
   * @return bool 
   * - TRUE if the user has at least one UWorld entitlement.
   */
  public static function uWorldCheck(ClassesUser $u) {    
    return count($u->getAllEntitlements()->filterByUworld()) > 0;
  }

  /**
   * Route handler for /user/productaccess
   */
  public static function productaccess() {
    $section = $_REQUEST['section'];
    $uid = $_REQUEST['uid'];
    $type = $_REQUEST['type'];

    $json = JSONResponse::Instance();

    $u = new ClassesUser($uid);

    $status = '';

    // Assume the sku matches the section, may be overridden for cram
    $sku = $section;

    // Get the users regular course entitlements
    $regular_ent_skus = $u->getValidEntitlements()->filterByOnlineCourse()->getSkus();


    // Check product type
    switch ($type) {
      case 'cram-course':
      case 'cram-course-activate':
        $sku = "$section-CRAM";

      case 'review-course':

        // Check if we have an active entitlement
        if ($u->hasActiveEntitlement($sku)) {
          $status = 'active';
          break;
        }

        // Check if we ever had an expired entitlement
        if ($u->hasExpiredEntitlement($sku)) {
          $status = 'expired';
          break;
        }
        // If we're here, the user must never have owned this product
        else {
          $status = 'unowned';
        }
        break;

      // @todo - We don't have a property for download access on the entitlement yet...
      case 'downloads':
        // Check if we have an active entitlement
        if (!$u->hasActiveEntitlement($section)) {
          $status = 'unowned';
        }
        else {
          $status = 'active';
          // @todo - check flag
        }
        break;

      // Unhandled.. @todo
      default:
      case 'flashcards':
      case 'audio':
        $json->setCode(500);
        $json->setError('Unknown product type');
        $json->setMessage('Unknown product type');
        return;
        break;
    }

    $json->addToPayload('status', $status);
    $json->addToPayload('entitlements', $regular_ent_skus);
  }


  public static function getEntitlementPayload(UserEntitlement $e) {
    return array(
      'id' => $e->getNid(),
      'sku' => $e->getProductSku(),
      'exam_version' => $e->getExamVersionName(),
      'content_restricted' => $e->getContentRestricted(),
      'restrictions' => $e->getAllowedTopicIds(),
      'expiration' => $e->getExpirationDate(),
      'section' => $e->getCourseSectionName(),
      'type' => $e->getCourseTypeName(),
      'activated' => !$e->getActivationDelayed(),
      'offline_access' => $e->getMobileOfflineAccess(),
    );
  }

  public static function getEntitlementCollectionPayload(UserEntitlementCollection $ec) {
    $payload = array();

    /** @var UserEntitlement $e */
    foreach ($ec->getEntitlements() as $e) {
      $payload[$e->getProductSku()] = self::getEntitlementPayload($e);
    }

    return $payload;
  }
}
