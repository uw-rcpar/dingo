<?php

namespace RCPAR\Rest\V1;

use RCPAR\Utils\JSONResponse;

class MobilePage {

  /**
   * Route handler for /mb mobile pages
   */
  public static function displayContent($alias) {
    $node_loaded = self::getNodeIdFromPathAlias($alias);
    $json = JSONResponse::Instance();
    if ($node_loaded === FALSE) {
      $json
        ->setCode(404) // Error code
        ->setMessage('The requested content was not found.') // Error messaging
        ->setError('Not found'); // The type of error
      return;
    }
    $json->setPayload(array(
      'title' => $node_loaded->title,
      'body' => $node_loaded->body['und'][0]['safe_value'],
      'regionTop' => $node_loaded->field_top_region['und'][0]['safe_value'],
      'regionMiddle' => $node_loaded->field_middle_region['und'][0]['safe_value'],
      'regionBottom' => $node_loaded->field_bottom_region['und'][0]['safe_value'],
    ));
  }

  public static function getNodeIdFromPathAlias($alias) {
    $path = drupal_lookup_path('source', 'mb/' . $alias);
    $nid = explode('/', $path, 2);
    return node_load($nid['1']);
  }

}