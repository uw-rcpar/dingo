<?php

namespace RCPAR\Rest\V1;

use \JWT\JWT;
use \Exception;

define('TOKEN_SECRET', 'RCPARsecretK3Y2018!@#');

// JWT token time to live - 24 hours
define('TOKEN_TTL', 86400);

class Token {
  // User id that the token was issued to
  protected $uid;

  // Timestamp for when token expires
  protected $expiration;

  // Timestamp the token was issued at
  protected $issued;

  // Boolean - set when a decode is attempted
  protected $decodeStatus = NULL;

  /** @var Exception $error - Any exception thrown while decoding */
  protected $error;

  // Stores the encoded token string
  protected $encodedToken = '';

  // A drupal session ID
  protected $drupalSessionId = '';

  /**
   * Initializes the object by decoding the supplied token and setting class properties
   * with the corresponding token values
   *
   * @param string $encodedToken
   * - An encoded JWT token string
   */
  public function __construct($encodedToken = '') {
    $this->decode($encodedToken);
  }

  /**
   * Attempt to decode an encoded token string and populate our object with its
   * decoded properties. Returns true on success.
   *
   * @param string $encodedToken
   * @return bool
   */
  public function decode($encodedToken) {
    try {
      // Store the encoded string for future reference
      $this->encodedToken = $encodedToken;

      // Decode the token and store the extracted properties in our object
      $jwt = JWT::decode($encodedToken, TOKEN_SECRET);
      $this->setUid($jwt->uid);
      $this->setExpiration($jwt->exp);
      $this->setIssued($jwt->iat);
      $this->setDrupalSessionId($jwt->sid);

      // Note that we succeeded in decoding
      $this->decodeStatus = TRUE;
    }
    catch (Exception $e) {
      // Note in the object that we failed to decode
      $this->decodeStatus = FALSE;

      // Store the exception but don't re-throw it
      $this->error = $e;
    }

    return $this->decodeStatus;
  }

  /**
   * Generates a new token from the existing token data.
   * This essentially just updates your token with a new expiration
   * @param $drupalSessionId
   * - Optionally provided a session ID to update as well. This is just for
   * convenience and could be done via the setDrupalSessionId() method as well.
   */
  public function refreshToken($drupalSessionId = '') {
    if (!empty($drupalSessionId)) {
      $this->setDrupalSessionId($drupalSessionId);
    }

    // Update expiration and issue time
    $time = time();
    $this->setExpiration($time + TOKEN_TTL);
    $this->setIssued($time);
  }

  /**
   * Get the user id who was issued this token
   * @return int
   */
  public function getUid() {
    return $this->uid;
  }

  /**
   * Check if the token is expired
   * @return bool
   */
  public function isExpired() {
    return $this->getExpiration() < time();
  }

  /**
   * True when the token has been decoded AND it is not expired
   * @return bool
   */
  public function isValid() {
    return $this->isDecoded() && !$this->isExpired();
  }

  /**
   * True if the token has been successfully decoded. Also true when the token has
   * been generated programmatically via buildNew()
   * @return bool
   */
  public function isDecoded() {
    return $this->decodeStatus ? TRUE : FALSE;
  }

  /**
   * Get the token's expiration
   * @return int
   * - A UNIX timestamp
   */
  public function getExpiration() {
    return $this->expiration;
  }

  /**
   * If an exception was encountered while trying to decode the token, it will be stored here.
   * @return Exception
   */
  public function getError() {
    return $this->error;
  }

  /**
   * The timestamp at which the token was issued at.
   * @return int
   * - UNIX timestamp
   */
  public function getIssued() {
    return $this->issued;
  }

  /**
   * Build a new Token
   * @param int $uid
   * @param string $sesionId
   * @return Token
   * - Token object
   */
  public static function buildNew($uid, $drupalSessionId) {
    $t = new self();
    $t->setUid($uid);
    $t->setDrupalSessionId($drupalSessionId);
    $t->refreshToken();

    $t->decodeStatus = TRUE;
    return $t;
  }

  /**
   * Get an encoded token string based on all of the current token values
   * @return string
   */
  public function getEncodedToken() {
    $token_data = array(
      'uid' => $this->getUid(),
      'exp' => $this->getExpiration(),
      'iat' => $this->getIssued(),
      'sid' => $this->getDrupalSessionId(),
    );

    return JWT::encode($token_data, TOKEN_SECRET);
  }

  /**
   * @param int $expiration
   */
  public function setExpiration($expiration = NULL) {
    $this->expiration = $expiration;
  }

  /**
   * @return string
   */
  public function getDrupalSessionId() {
    return $this->drupalSessionId;
  }

  /**
   * @param string $drupalSessionId
   */
  public function setDrupalSessionId($drupalSessionId) {
    $this->drupalSessionId = $drupalSessionId;
  }

  /**
   * Set the time the token was issued at
   * @param int $issued
   */
  public function setIssued($issued) {
    $this->issued = $issued;
  }

  /**
   * Set the user id of the recipient the token is issued to
   * @param int $uid
   */
  public function setUid($uid) {
    $this->uid = $uid;
  }
}