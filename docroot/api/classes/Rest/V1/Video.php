<?php

namespace RCPAR\Rest\V1;

use RCPAR\Utils\JSONResponse;
use RCPAR\Video as RCPARVideo;
use Exception;

class Video {

  /**
   * Route handler for POST /video/sync
   *
   * @throws Exception
   */
  public static function syncPost() {
    $json = JSONResponse::Instance();

    /************************
     * $payloadExample = array(
     * 'videos' => array(
     * 0 => array(
     * 'videoId' => 123,
     * 'topicId' => 123,
     * 'progressTime' => 12345,
     * 'entitlementSku' => 'AUD',
     * 'lastWatched' => TIMESTAMP,
     * ),
     * 1 => [],
     * 2 => [],
     * // etc
     * ),
     * );
     *************************/
    $payload = $_POST; //json_decode(file_get_contents('php://input'), TRUE);

    if ($payload) {
      // Authenticate the user
      if (!Authenticate::Instance()->isAuthenticated()) {
        $json->unauthorizedAndDie();
        return;
      }
      $u = Authenticate::Instance()->getUser();

      // Update each video, tally successes and failures
      $successes = array();
      $fails = array();
      $outdated = array();
      $ents = $u->getActiveEntitlements();
      foreach ($payload['videos'] as $video) {
        // Get the entitlement for this video
        $ent = $ents->getEntitlementBySku($video['entitlementSku']);
        if ($ent) {
          // Get the entitlement ID for this video
          $eid = $ent->getNid();
          // Only update if this is the user's most recent occurrence of watching the video
          $v = new RCPARVideo($u, $video['videoId'], $eid);
          if ($video['lastWatched'] > $v->getTimeLastWatched()) {
            //if($video['progressTime'] > $v->getVideoProgress()) {
            $ret = $v->setVideoProgress($video['videoId'], $video['topicId'], $video['progressTime'], $video['entitlementSku']);
            if ($ret) {
              $successes[] = $video['videoId'];
            }
            else {
              $fails[] = $video['videoId'];
            }
          }
          else {
            $outdated[] = $video['videoId'];
          }
        }
        else {
          try{
            // Validate if newrelic exists
            if(function_exists('newrelic_add_custom_parameter')){
              // Send a custom parameter to include payload
              newrelic_add_custom_parameter("payload", json_encode($payload));
            }
          } catch (Exception $ex) {
            watchdog(basename(__FILE__), 'ERROR ON FUNCTION ' . __FUNCTION__ . ' MODULE ' . basename(__FILE__) . " ERROR: " . $ex->getMessage());
          }
        }
      }

      // Set JSON response
      $json->addToPayload('videosUpdated', $successes);
      $json->addToPayload('videosUpdateFailed', $fails);
      $json->addToPayload('videosOutdated', $outdated);
      $json->setMessage('Completed video history update. ' . count($successes) .
        ' were updated successfully. ' . count($fails) . ' failed to update. ' .
        count($outdated) . ' have been watched more recently than this update.');
    }
    else {
      throw new Exception('Request payload malformed');
    }
  }

  /**
   * Route handler for POST /video/sync
   * @throws Exception
   */
  public static function syncGet() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    $u = Authenticate::Instance()->getUser();

    $after = $_REQUEST['after'];

    $result = db_query("SELECT rcpa_video, last_position FROM {eck_video_history} WHERE uid = :uid AND changed > :after", array(
      ':uid' => $u->getUid(),
      ':after' => $after
    ))->fetchAllKeyed();

    // Get an array of position timestamps keyed by video id
    $json->addToPayload('videos', $result);
    $json->setMessage('Found ' . count($result) . ' videos watched after ' . date('r', $after) . '. Results keyed by video ID, values are video progress values in milliseconds');
  }
}