<?php

namespace RCPAR\Rest\V1;

use Exception;
use RCPAR\Course as RCPARCourse;
use RCPAR\Utils\JSONResponse;

class Course {

  /**
   * Route handler for /course/outline
   * @throws Exception
   */
  public static function outline() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if(!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    $form = $form_state = array();

    // Set the exam version of the course that we're displaying
    $evy = exam_versions_years();
    if (!empty($_REQUEST['exam_version'])) {
      if (isset($evy[$_REQUEST['exam_version']])) {
        $form_state['values']['version'] = $evy[$_REQUEST['exam_version']];
      }
      else {
        throw new Exception('The specified exam version does not exist');
      }
    }
    else {
      $form_state['values']['version'] = $evy[exam_version_get_default_version()];
    }

    // Get the whole outline in one fell swoop and put it into the JSON payload
    $json->setPayload(rcpar_dashboard_course_stats_rebuild_json($form, $form_state, FALSE));
  }

  /**
   * Route handler for /course/progress
   * Get's the percentage of video progress for the user for the given entitlement SKUs, or all of the
   * user's entitlements if none is specified.
   * @throws Exception
   */
  public static function progress() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if(!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    $u = Authenticate::Instance()->getUser();

    // The SKUs for progress to check
    $skus = array('AUD', 'BEC', 'FAR', 'REG', 'AUD-CRAM', 'BEC-CRAM', 'FAR-CRAM', 'REG-CRAM');

    // Allow the requester to ask for specific SKUs
    if (!empty($_REQUEST['sku']) && is_array($_REQUEST['sku'])) {
      $skus = $_REQUEST['sku'];
    }

    // Get all valid course entitlements
    $ents = $u
      ->getValidEntitlements()
      ->filterBySkus($skus);

    // Get progress
    $average_completed = RCPARCourse::getProgressPercentageForUser($u, $ents);

    // Set an informational API message
    if (empty($average_completed)) {
      $json->setMessage('User has no video progress yet');
    }
    else {
      $json->setMessage('Found some video progress!');
    }

    $json->addToPayload('progress_percentages', $average_completed);

    // Add the "You left of at:" data to the payload.
    $json->addToPayload('last_watched', RCPARCourse::getUserLastWatched($u));
  }
}