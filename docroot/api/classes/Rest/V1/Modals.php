<?php

namespace RCPAR\Rest\V1;

use RCPAR\Modeling\ModalModel;
use RCPAR\Utils\JSONResponse;
use RCPAR\Rest\V1\Authenticate;

class Modals {


  /**
   * Route handler for /modals/set
   */
  public static function setData() {

    // Authenticate the user
    $user = Authenticate::Instance()->getUser();
    $json = JSONResponse::Instance();
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    // Determine if this modal has already been recorded for this user
    $modal_already_logged = ModalModel::modalHasBeenLogged($user->getUid(), $_POST['machinename']);

    // Log it if not
    if (!$modal_already_logged) {
      ModalModel::logUserModals($user->getUid(), $_POST['machinename']);
    }
    // Update the modal status if there's already a record.
    else {
      ModalModel::updateUserModals($user->getUid(), $_POST['machinename'], 1);
    }

  }


  /**
   * Route handler for /modals/update
   */
  public static function updateData() {

    // Authenticate the user
    $user = Authenticate::Instance()->getUser();
    $json = JSONResponse::Instance();
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    ModalModel::updateUserModals($user->getUid(), $_POST['machinename'], $_POST['status']);

  }

  /**
   * Route handler for /modals/update/time
   */
  public static function updateTimestamp() {

    // Authenticate the user
    $user = Authenticate::Instance()->getUser();
    $json = JSONResponse::Instance();
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    ModalModel::updateUserModalTimestamp($user->getUid(), $_POST['machinename'], $_POST['timestamp']);

  }

}