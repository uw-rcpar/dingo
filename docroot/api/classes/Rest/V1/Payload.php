<?php

namespace RCPAR\Rest\V1;

use RCPAR\Utils\JSONResponse;

class Payload {

  /**
   * Manipulate the JSON response before the request is handled
   * @param JSONResponse $json
   */
  static function prepare(JSONResponse $json) {
    // Try to authenticate against the request
    Authenticate::Instance()->setIdentityFromRequest();
  }

  /**
   * Manipulate the JSON response before it's sent
   * @param JSONResponse $json
   */
  static function preSend(JSONResponse $json) {
    $refresh = FALSE;

    // If we have an authenticated user, check if we need to tell the client to
    // refresh entitlements.
    $auth = Authenticate::Instance();
    if($auth->isAuthenticated()) {
      // Intervene against blocked user
      if(!$auth->getUser()->getUser()->status) {
        $json->setPayload(array(), FALSE);
        $json->setCode(401);
        $json->setError('Inactive account');
        $json->setMessage('Sorry, this account is not activated.');
      }

      // Token issued time
      $iat = $auth->getToken()->getIssued();

      // Check ALL entitlements to see when the last change was. We check all because
      // we need to know if an entitlement was manually expired or set to unpublished
      $entLastUpdate = $auth->getUser()->getAllEntitlements()->getLastChanged();

      // If the token was issued before the latest entitlement change, the client needs
      // to refresh
      if ($iat < $entLastUpdate) {
        $refresh = TRUE;
      }

      $json->addToPayload('entitlementsLastChanged', $entLastUpdate);
    }

    $json->addToPayload('entitlementNeedsRefresh', $refresh);
  }

  public static function postSend(JSONResponse $json) {
    /********** Logging ***** ****************/
    // Log request and response to Drupal
    $logging = variable_get('mobile_api_logging', FALSE);
    if($logging) {
      watchdog('MobileAPI',
        "<h3>Request</h3>
    <strong>Method:</strong> !method
    <strong>URI:</strong> !uri <br> <strong>Params:</strong> <pre>!params</pre>
    <hr>
    <h3>Response</h3>
    <pre>!resp</pre>",
        array(
          '!method' => $_SERVER['REQUEST_METHOD'],
          '!uri' => $_SERVER['REQUEST_URI'],
          '!params' => print_r($_REQUEST, TRUE),
          '!resp' => print_r($json->send(TRUE), TRUE),
        )
      );
    }
  }
}