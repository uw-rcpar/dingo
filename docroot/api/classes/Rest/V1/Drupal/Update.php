<?php

namespace RCPAR\Rest\V1\Drupal;

use RCPAR\Rest\V1\Authenticate;

class Update {

  /**
   * @todo - This is a work in progress
   * This is a callback through the rest API (done so that we can leverage authentication)
   * that will eventually allow us to run Drupal updates by hand.
   */
  public static function update() {
    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      print 'Unauthorized';
      die();
    }
    $u = Authenticate::Instance()->getUser();

    // Only allow access to user 1
    if ($u->getUid() != 1) {
      print 'Unauthorized';
      die();
    }

    // Include necessary Drupal files
    require_once DRUPAL_ROOT . '/includes/update.inc';
    require_once DRUPAL_ROOT . '/includes/install.inc';

    // Load all install files so schema update functions can be found
    drupal_load_updates();

    // We want to see all possible update functions, so we'll pretend we're starting at 0
    $starting_updates = array();
    $modules = drupal_get_installed_schema_version(NULL, TRUE, TRUE);
    foreach ($modules as $module => $schema_version) {
      $starting_updates[$module] = 0;
    }

    // Remove functions with no update hooks
    $update_functions = update_get_update_function_list($starting_updates);
    foreach ($update_functions as $key => $val) {
      if (empty($val)) {
        unset($update_functions[$key]);
      }
    }

    // We now have an array keyed by module name of arrays of update functions keyed by update number
    $form = array();
    foreach ($update_functions as $module => $updates) {
      $options = array();
      foreach ($updates as $update_function) {
        $options[$update_function] = $update_function;
      }
      $form[$module] = array(
        '#title' => $module,
        '#type' => 'select',
        '#options' => $options,
      );
    }

    print drupal_render($form);

    die();
  }
}