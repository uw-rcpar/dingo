<?php

namespace RCPAR\Rest\V1;
use RCPAR\User;
use RCPAR\Utils\Singleton;

class Authenticate extends Singleton {
  /** @var Token */
  protected $token;

  /** @var User */
  protected $user;

  /**
   * Get a JWT token from the request and set the objects user and token
   * from it if possible
   * @return $this
   */
  public function setIdentityFromRequest() {
    // Initialize user and token from the request if possible
    $t = self::getTokenFromRequest();
    if($t->getUid()) {
      $u = new User($t->getUid());
      $this->setUser($u);
      $this->setToken($t);
    }
    // We don't have a token, we'll allow any request to authenticate with a username and password
    else if(!empty($_REQUEST['authuser']) && (!empty($_REQUEST['authpass']) || !empty($_REQUEST['authhash']) )) {
      $u = new User();

      // If we have a password, authenticate against that
      if(!empty($_REQUEST['authpass'])) {
        $uid = $u->authenticateCredentials($_REQUEST['authuser'], $_REQUEST['authpass']);
      }
      // Otherwise we have a hash - authenticate against that
      elseif(!empty($_REQUEST['authhash'])) {
        $uid = $u->validateHash($_REQUEST['authuser'], $_REQUEST['authhash']);
        if($uid) {
          $u->login($uid);
        }
      }

      // If either method was successful in authenticating a user, populate the our Authenticate instance.
      if ($uid) {
        $t = Token::buildNew($u->getUid(), $u->getDrupalSessionId());
        $this->setUser($u);
        $this->setToken($t);
      }
    }

    return $this;
  }

  /**
   * If a token exists
   * @return Token
   */
  public static function getTokenFromRequest() {
    // Try to get a token from the HTTP headers
    $headers = getallheaders();
    if (!empty($headers['Jwt'])) {
      return new Token($headers['Jwt']);
    }

    // Lowercase
    if (!empty($headers['jwt'])) {
      return new Token($headers['jwt']);
    }

    // Try to get a token from the request parameters
    if(!empty($_REQUEST['Jwt'])) {
      return new Token($_REQUEST['Jwt']);
    }

    // Lowercase
    if (!empty($_REQUEST['jwt'])) {
      return new Token($_REQUEST['jwt']);
    }

    // Couldn't find a token, return a new blank token
    return new Token();
  }

  /**
   * Call this method to get singleton
   * @return self
   */
  public static function Instance() {
    static $inst = null;
    if ($inst === null) {
      $inst = new Authenticate();
      $inst->setUser(new User());
      $inst->setToken(new Token(''));
    }
    return $inst;
  }

  /**
   * @return Token
   */
  public function getToken() {
    return $this->token;
  }

  /**
   * @param Token $token
   */
  private function setToken(Token $token) {
    $this->token = $token;
  }

  /**
   * @return User
   */
  public function getUser() {
    return $this->user;
  }

  /**
   * @param User $user
   */
  private function setUser(User $user) {
    $this->user = $user;
  }

  /**
   * @param User $user
   * @param Token $token
   * @return $this
   */
  public function setIdentity(User $user, Token $token) {
    $this->setUser($user);
    $this->setToken($token);

    return $this;
  }

  /**
   * True if there is a loaded user and a valid, unexpired token
   * @return bool
   */
  public function isAuthenticated() {
    return $this->getUser()->isLoaded() && $this->getToken()->isValid();
  }

  /**
   * Looks for the presence of an HTTP header or GET/POST parameter that contains a static API key.
   * This is a less secure authentication method that is currently only used for the shipping API
   * (\RCPAR\Rest\V1\Order::getShipping). This method of authentication is simpler, quicker, and
   * acceptable when providing access to data that is not sensitive.
   * It's kind of a workaround to provide access to UWorld and not really architectually masterminded,
   * but it should do for now.
   *
   * @return bool
   * - True when the matching key is found.
   */
  public function isAuthenticatedApiKey() {
    $headers = getallheaders();
    if (!empty($headers['x-api-key'])) {
      $key = $headers['x-api-key'];
    }
    if (!empty($headers['X-Api-Key'])) {
      $key = $headers['X-Api-Key'];
    }

    if (!empty($_REQUEST['x-api-key'])) {
      $key = $_REQUEST['x-api-key'];
    }

    // This key is "UWORLD-" with the MD5 hash of "UWORLD" concatenated onto it. I decided that
    // totally randomly.
    if ($key == 'UWORLD-2B34751BE1FA22C37E6C654B55AA6FF6') {
      return TRUE;
    }

    return FALSE;
  }

}
