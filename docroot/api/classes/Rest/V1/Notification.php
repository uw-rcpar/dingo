<?php

namespace RCPAR\Rest\V1;

use RCPAR\Entities\Notifications\Channel;
use RCPAR\Entities\Notifications\Notification as NotificationObj;
use RCPAR\Entities\Notifications\NotificationCollection;
use RCPAR\NotificationManager;
use RCPAR\Utils\JSONResponse;

class Notification {
  /**
   * Route handler for /user/notifications/get/unread
   * Get all of the user's unread notifications
   *
   * @throws \Exception
   */
  public static function getUnread() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    $user = Authenticate::Instance()->getUser();

    // Create notification manager
    $channel = Channel::getChannelByName('mobile_app');
    $m = new NotificationManager($user, $channel);
    $m->issueNotifications();
    $notifications = $m->getUnreadNotifications();

    // Also mark as read if requested
    // @todo, we don't really want clients using this workflow. They should specificy which notifications to mark read
    // in a separate request
    if (!empty($_GET['mark_read'])) {
      $notifications->markAllRead();
    }

    // Build JSON response and payload
    $json->setCode(200);
    $json->setMessage('Found ' . ($notifications->count() + 1) . ' notification(s).'); // We add one here to account for the default message hardcoded in getNotificationsPayload
    $json->addToPayload('notifications', self::getNotificationsPayload($notifications));
  }

  /**
   * Route handler for /user/notifications/get/all
   * Get all of the user's unread notifications regardless of 'read' status
   *
   * @throws \Exception
   */
  public static function getAll() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if (!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    $user = Authenticate::Instance()->getUser();

    // Create notification manager - make sure we issue notifications from broadcasts by retrieving
    // before we get all
    $channel = Channel::getChannelByName('mobile_app');
    $m = new NotificationManager($user, $channel);
    $m->issueNotifications();
    $limit = !empty($_GET['limit']) ? $_GET['limit'] : 19;
    $notifications = $m->getAll($limit);
    // We add one here to account for the default message hardcoded in getNotificationsPayload
    $no_of_notifications = $notifications->count() + 1;

    // Build JSON response and payload
    $json->setCode(200);
    $json->setMessage('Found ' . $no_of_notifications . ' notification(s).');
    $json->addToPayload('notifications', self::getNotificationsPayload($notifications));
  }

  /**
   * Route handler for POST /user/notifications/create
   * Creates a notification for a user
   * @throws \Exception
   */
  public static function create() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if(!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }
    $user = Authenticate::Instance()->getUser();

    // Message payload
    $title      = $_POST['title'];
    $teaser     = $_POST['teaser'];
    $message    = $_POST['message'];
    $type       = $_POST['type'];
    $expiration = $_POST['expiration'];

    // Valid required params
    if(empty($title) || empty($teaser) || empty($message) || empty($type) || empty($expiration)) {
      throw new \Exception('One or more required parameters was missing');
    }

    // Build JSON response and payload
    $n = NotificationObj::createPrivateFromMessage($user, $title, $teaser, $message, $type, $expiration)->save();
    $json->addToPayload('notifications', self::getNotificationPayload($n));

    // Set code and message after creating in case of any exceptions
    $json->setCode(200);
    $json->setMessage('Created a notification');
  }

  /**
   * Route handler for /user/notifications/read
   * Mark an array of notification IDs as read
   * @throws \Exception
   */
  public static function setRead() {
    $json = JSONResponse::Instance();

    // Authenticate the user
    if(!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    // Validate required params
    $mark_read_ids = !empty($_POST['ids']) ? $_POST['ids'] : array();
    if(empty($mark_read_ids)) {
      throw new \Exception('Missing required parameter: ids');
    }

    // Set as read for each id
    $notifications = array();
    foreach ($mark_read_ids as $mark_read_id) {
      $n = new NotificationObj($mark_read_id);
      if($n->getUid() != Authenticate::Instance()->getUser()->getUid()) {
        throw new \Exception("User does not have access to notification ID: $mark_read_id");
      }
      $notifications[] = $n;
    }

    // If we're here, we have access to all notifications - mark read and save
    foreach ($notifications as $n) {
      $n->markRead();
      $n->save();
    }

    // Set JSON response and code
    $json->setMessage(count($notifications) . ' notification(s) marked as read.');
    $json->setCode(200);
    $json->addToPayload('notifications', self::getNotificationsPayload($notifications));
  }

  /**
   * Get an array to be used for a JSON payload of a collection of Notification objects
   * @param NotificationCollection $notifications
   * @return array
   */
  public static function getNotificationsPayload($notifications) {
    $data = array();
    foreach ($notifications as $notification) {
      $data[] = self::getNotificationPayload($notification);
    }
    // Backward compatibility hack attack: this notification was hardcoded in the first iteration of this API
    // and needs to remain
    $default_notification = array(
      'nid' => '124437',
      'type' => 'normal', // normal, warning, urgent
      'title' => 'Welcome to Roger CPA Review',
      'teaser' => 'Welcome to the Roger CPA Review Mobile App!',
      'body' => '<b>Welcome to the Roger CPA Review Mobile App!</b>
      <p>You\'ve just taken a major step forward in your path towards a CPA license by enabling easy access to your Roger CPA Review course materials no matter where you are.
      <p>Be on the lookout for upcoming communications from us here in your notifications inbox, as well as future updates to this app that will further maximize your study potential on-the-go. Here\'s to getting you one step closer to your dream of becoming a CPA!
      <p>Happy studies,<br>
      The Roger CPA Review Team',
      'created_date' => '1527813788'
    );
    array_unshift($data, $default_notification);

    return $data;
  }

  /**
   * Get an array to be used for a JSON payload of the notification object
   * @param NotificationObj[] $n
   * @return array
   */
  public static function getNotificationPayload(NotificationObj $n) {
    return array(
      'nid' => $n->getId(),
      'type' => $n->getType(),
      'title' => $n->getTitle(),
      'teaser' => $n->getTeaser(),
      'body' => $n->getMessage(),
      'created_date' => $n->getCreated(),

      /*
      'id' => $n->getId(),
      'notification_template_id' => $n->getNotificationTemplateId(),
      'broadcast_channel_id' => $n->getBroadcastChannelId(),
      'type' => $n->getType(),
      'title' => $n->getTitle(),
      'message' => $n->getMessage(),
      'expire' => $n->getExpire(),
      'uid' => $n->getUid(),
      'is_read' => $n->getIsRead(),
      'created' => $n->getCreated(),
      'read_on' => $n->getReadOn()
      */
    );
  }

  /**
   * Route handler for /user/notifications
   * Legacy handler - with hard coded data
   * @throws \Exception
   */
  public static function notifications() {

    self::getAll();

    /*
    $json = JSONResponse::Instance();

    // Authenticate the user
    if(!Authenticate::Instance()->isAuthenticated()) {
      $json->unauthorizedAndDie();
      return;
    }

    $json->setCode(200);
    $json->setMessage('Found 1 notification(s).');


    $json->addToPayload('notifications', array(
        0 => array(
          'nid' => '124437',
          'type' => 'normal', // normal, warning, urgent
          'title' => 'Welcome to Roger CPA Review',
          'teaser' => 'Welcome to the Roger CPA Review Mobile App!',
          'body' => '<b>Welcome to the Roger CPA Review Mobile App!</b>
<p>You\'ve just taken a major step forward in your path towards a CPA license by enabling easy access to your Roger CPA Review course materials no matter where you are.
<p>Be on the lookout for upcoming communications from us here in your notifications inbox, as well as future updates to this app that will further maximize your study potential on-the-go. Here\'s to getting you one step closer to your dream of becoming a CPA!
<p>Happy studies,<br>
The Roger CPA Review Team',
          'created_date' => '1527813788'
        ),
      )
    );*/
  }

}