<?php

/**
 * @file
 * REST API main file
 */

// A global state variable to indicate this request is being handled by our API controller.
$GLOBALS['rcpar_api'] = TRUE;

// Bootstrap Drupal
require_once('bootstrap.php');

// Register our autoloader and class directories
require_once ('registerautoload.php');

// Initialize our micro-framework.
// Note that Flight comes with a basic PSR-0 (I think, may be PSR-4) autoloader as well.
// This covers the vendor dir.
require 'vendor/flight/Flight.php';

use RCPAR\Utils\JSONResponse;


/********** Define HTTP Routes *************/
Flight::route('POST|GET /user/login', array('RCPAR\Rest\V1\User', 'login'));
Flight::route('POST|GET /v2/user/password', array('RCPAR\Rest\V2\User', 'password'));
Flight::route('POST|GET /v2/user/login', array('RCPAR\Rest\V2\User', 'login'));
Flight::route('POST|GET /user/productaccess', array('RCPAR\Rest\V1\User', 'productaccess'));
Flight::route('POST|GET /user/notifications', array('RCPAR\Rest\V1\Notification', 'notifications'));
Flight::route('POST /user/notifications/create', array('RCPAR\Rest\V1\Notification', 'create'));
Flight::route('GET /user/notifications/get/unread', array('RCPAR\Rest\V1\Notification', 'getUnread'));
Flight::route('GET /user/notifications/get/all', array('RCPAR\Rest\V1\Notification', 'getAll'));
Flight::route('GET /v2/user/notifications/get/all', array('RCPAR\Rest\V2\Notification', 'getAll'));
Flight::route('POST /v2/user/notifications/read', array('RCPAR\Rest\V2\Notification', 'setRead'));
Flight::route('POST /user/notifications/read', array('RCPAR\Rest\V1\Notification', 'setRead'));
Flight::route('GET /course/outline', array('RCPAR\Rest\V1\Course', 'outline'));
Flight::route('GET /course/progress', array('RCPAR\Rest\V1\Course', 'progress'));
Flight::route('POST /video/sync', array('RCPAR\Rest\V1\Video', 'syncPost'));
Flight::route('GET /video/sync', array('RCPAR\Rest\V1\Video', 'syncGet'));
Flight::route('POST /download/sync', array('RCPAR\Rest\V1\Download', 'syncPost'));
Flight::route('POST /mb/@alias:.*', array('RCPAR\Rest\V1\MobilePage', 'displayContent'));
Flight::route('GET /drupal/update', array('RCPAR\Rest\V1\Drupal\Update', 'update'));
Flight::route('POST|GET /modals/set', array('RCPAR\Rest\V1\Modals', 'setData'));
Flight::route('POST|GET /modals/update', array('RCPAR\Rest\V1\Modals', 'updateData'));
Flight::route('POST|GET /modals/update/time', array('RCPAR\Rest\V1\Modals', 'updateTimestamp'));
Flight::route('GET /order/get/shipping', array('RCPAR\Rest\V1\Order', 'getShipping'));
Flight::route('POST|GET /order/get/orders', array('RCPAR\Rest\V1\Order', 'getUserOrders'));
Flight::route('GET /ipq/quizzes', array('RCPAR\Rest\V2\Quizzes', 'getUserQuizzes'));
Flight::route('GET|POST /ipq/quizzes/data', array('RCPAR\Rest\V2\Quizzes', 'getUserQuizData'));
Flight::route('POST /migrate', array('RCPAR\Rest\V2\Migrate', 'setMigrationComplete'));
Flight::route('GET /migrate/status', array('RCPAR\Rest\V2\Migrate', 'getMigrationCompleteStatus'));
Flight::route('GET /v2/video/sync', array('RCPAR\Rest\V2\Video', 'syncGet'));

// Handle a request that does not match a route. Note that this would not execute
// until Flight executes the route handler.
Flight::map('notFound', function () {
  JSONResponse::Instance()
    ->setCode(404)
    ->setMessage('The requested endpoint was not found.')
    ->setError('Not found');
});

/********** Handle request ****************/
$json = JSONResponse::Instance();

// Pre-request tasks - authentication
RCPAR\Rest\V1\Payload::prepare($json);

try {
  // Execute the appropriate route handler
  Flight::start();
}
// Route handlers may intentionally throw an exception in common error scenarios
catch (Exception $e) {
  // If an error status is not already set in the response, this is an unexpected error
  if(!$json->getError()) {
    $json
      ->setCode(500)
      ->setMessage($e->getMessage())
      ->setError('Unexpected error');
  }
}

// Global adjustments to the response - tell the client if they need to refresh entitlements
RCPAR\Rest\V1\Payload::preSend($json);

// Output the JSON
$json->send();

// Post execution tasks - e.g. logging
RCPAR\Rest\V1\Payload::postSend($json);