<?php

// Note - requires DRUPAL_ROOT to have been defined first!
define('API_ROOT', DRUPAL_ROOT . '/api');

require_once(API_ROOT . '/autoload.php');
$loader = new \RCPARAPI\Psr4AutoloaderClass;
$loader->register();

// register the base directories for the namespace prefix
$baseDir = API_ROOT;
$vendorDir = "$baseDir/vendor";
$classesDir = "$baseDir/classes";

$loader->addNamespace("JWT", "$vendorDir/JWT");
$loader->addNamespace("RCPAR", $classesDir);