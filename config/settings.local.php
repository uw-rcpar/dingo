<?php

$databases = array();

/**
 * This is the main database, uncomment and up date to your local dev settings
 */

// $databases['default']['default'] = array(
//   'driver' => 'mysql',
//   'database' => 'dingo_dev',
//   'username' => 'root',
//   'password' => '',
//   'host' => 'localhost',
//   'prefix' => '',
//   'collation' => 'utf8_general_ci',
// );

/**
 * Uncomment this to add the 'rcpar_history' database to your install
 * freetds.conf defines this host
 */

// $databases['rcpar_history']['default'] = array (
//     'database' => 'rogercpareviewdb_dev',
//     'username' => 'root',
//     'password' => '',
//     'host' => 'rcpar_history',
//     'port' => '',
//     'driver' => 'dblib',
//     'prefix' => '',
// );

/**
 * Uncomment this to add the 'testcenter' database to your install
 */

// $databases['testcenter']['default'] = array (
//     'database' => 'testcenter_dev',
//     'username' => 'root',
//     'password' => '',
//     'host' => 'localhost',
//     'port' => '3306',
//     'driver' => 'mysql',
//     'prefix' => '',
// );
