#!/bin/sh
#
# Cloud Hook: post-code-deploy
#
# The post-code-deploy hook is run whenever you use the Workflow page to 
# deploy new code to an environment, either via drag-drop or by selecting
# an existing branch or tag from the Code drop-down list. See 
# ../README.md for details.
#
# Usage: post-code-deploy site target-env source-branch deployed-tag repo-url 
#                         repo-type

site="$1"
target_env="$2"
source_branch="$3"
deployed_tag="$4"
repo_url="$5"
repo_type="$6"

# Load the Slack webhook URL
SLACK_WEBHOOK_URL=https://hooks.slack.com/services/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

if [ "$source_branch" != "$deployed_tag" ]; then
	if [ "$target_env" != 'prod' ]; then
		curl -X POST --data-urlencode "payload={\"channel\": \"#webdev-notifications\", \"username\": \"Acquia Cloud\", \"text\": \"Acquia cloud: Code deployed - *$site.$target_env:* Deployed branch *$source_branch* as *$deployed_tag.*\"}" $SLACK_WEBHOOK_URL		
	else
		# PROD DEPLOYMENT!
	
		# Slack notice
		curl -X POST --data-urlencode "payload={\"channel\": \"#deployment\", \"username\": \"Acquia Cloud\", \"text\": \"Acquia cloud: @here - Code has been deployed on production! *$site.$target_env:* Deployed branch *$source_branch* as *$deployed_tag.*\"}" $SLACK_WEBHOOK_URL
	fi
else
	curl -X POST --data-urlencode "payload={\"channel\": \"#webdev-notifications\", \"username\": \"Acquia Cloud\", \"text\": \"Acquia cloud: Code deployed - *$site.$target_env:* Deployed *$deployed_tag.*\"}" $SLACK_WEBHOOK_URL		
fi
