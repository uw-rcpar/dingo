#!/bin/sh
#
# Cloud Hook: post-code-update
#
# The post-code-update hook runs in response to code commits.
# When you push commits to a Git branch, the post-code-update hooks runs for
# each environment that is currently running that branch. See
# ../README.md for details.
#
# Usage: post-code-update site target-env source-branch deployed-tag repo-url
#                         repo-type

site="$1"
target_env="$2"
source_branch="$3"
deployed_tag="$4"
repo_url="$5"
repo_type="$6"

# Load the Slack webhook URL
SLACK_WEBHOOK_URL=https://hooks.slack.com/services/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

if [ "$target_env" != 'prod' ]; then
	# Post code update notice to Slack
	curl -X POST --data-urlencode "payload={\"channel\": \"#webdev-notifications\", \"username\": \"Acquia Cloud\", \"text\": \"Acquia cloud: *$site.$target_env:* The *$source_branch* branch has been updated on *$target_env.*\"}" $SLACK_WEBHOOK_URL
else
	# Post prod code update on Slack
	curl -X POST --data-urlencode "payload={\"channel\": \"#deployment\", \"username\": \"Acquia Cloud\", \"text\": \"Acquia cloud: @here - Code has been updated on production! *$site.$target_env:* The *$source_branch* branch has been updated on *$target_env.*\"}" $SLACK_WEBHOOK_URL
fi
