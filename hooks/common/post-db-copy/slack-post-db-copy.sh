#!/bin/sh
#
# Cloud Hook: post-db-copy
#
# The post-db-copy hook is run whenever you use the Workflow page to copy a
# database from one environment to another. See ../README.md for
# details.
#
# Usage: post-db-copy site target-env db-name source-env

site="$1"
target_env="$2"
db_name="$3"
source_env="$4"

# Load the Slack webhook URL
SLACK_WEBHOOK_URL=https://hooks.slack.com/services/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

# Post DB update notice to Slack
curl -X POST --data-urlencode "payload={\"channel\": \"#webdev-notifications\", \"username\": \"Acquia Cloud\", \"text\": \"Acquia cloud database named *$db_name* was copied from *$site.$source_env* to *$site.$target_env.*\"}" $SLACK_WEBHOOK_URL
