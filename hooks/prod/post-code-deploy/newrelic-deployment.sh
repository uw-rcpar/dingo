#!/bin/sh
#
# Cloud Hook: post-code-deploy
#
# The post-code-deploy hook is run whenever you use the Workflow page to 
# deploy new code to an environment, either via drag-drop or by selecting
# an existing branch or tag from the Code drop-down list. See 
# ../README.md for details.
#
# Usage: post-code-deploy site target-env source-branch deployed-tag repo-url 
#                         repo-type

site="$1"
target_env="$2"
source_branch="$3"
deployed_tag="$4"
repo_url="$5"
repo_type="$6"

# New Relic API keys
APP_ID="18103032"
API_KEY="b19b88983027a882bcc14dd49869bfc3b918961e1f19049"

# Flag deployment in New Relic
curl -X POST "https://api.newrelic.com/v2/applications/${APP_ID}/deployments.json" \
	 -H "X-Api-Key:${API_KEY}" -i \
	 -H 'Content-Type: application/json' \
	 -d \
"{
  \"deployment\": {
	\"revision\": \"$deployed_tag\",
	\"changelog\": \"\",
	\"description\": \"*$site.$target_env:* Deployed branch *$source_branch* as *$deployed_tag.*\",
	\"user\": \"acquiacloud@notarealaddress.com\"
  }
}"